﻿using FPCS.Core.Unity;
using FPCS.Data;
using FPCS.Data.Entities;
using FPCS.Data.Enums;
using FPCS.Data.Repo;
using FPCS.Import.BetweenYears.Models.Years;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Import.BetweenYears.Handlers
{
    public static class YearsHandler
    {
        public static YearsModel Import(Int32 oldSchoolYearId, Int32 newSchoolYearId)
        {
            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                YearsModel result = new YearsModel();

                var repoStudent = uow.GetRepo<IStudentRepo>();
                var repoTeacher = uow.GetRepo<ITeacherRepo>();
                var repoGuardian = uow.GetRepo<IGuardianRepo>();
                var repoFamily = uow.GetRepo<IFamilyRepo>();
                var repoASDCourse = uow.GetRepo<IASDCourseRepo>();
                var repoFPCSCourse = uow.GetRepo<IFPCSCourseRepo>();
                var repoSchoolYear = uow.GetRepo<ISchoolYearRepo>();
                var repoFamilyRelationship = uow.GetRepo<IFamilyRelationshipRepo>();
                var repoVendor = uow.GetRepo<IVendorRepo>();

                List<Student> students = repoStudent.GetAll(oldSchoolYearId).ToList();
                List<Teacher> teachers = repoTeacher.GetAll(oldSchoolYearId).ToList();
                List<Guardian> guardians = repoGuardian.GetAll(oldSchoolYearId).ToList();
                List<Family> families = repoFamily.GetAll(oldSchoolYearId).ToList();
                List<ASDCourse> asdCourses = repoASDCourse.GetAll(oldSchoolYearId).ToList();
                List<FPCSCourse> fpcsCourses = repoFPCSCourse.GetAll(oldSchoolYearId).ToList();
                List<Vendor> vendors = repoVendor.GetAll(oldSchoolYearId).ToList();

                List<Student> studentsNY = repoStudent.GetAll(newSchoolYearId).ToList();
                List<Teacher> teachersNY = repoTeacher.GetAll(newSchoolYearId).ToList();
                List<Guardian> guardiansNY = repoGuardian.GetAll(newSchoolYearId).ToList();
                List<Family> familiesNY = repoFamily.GetAll(newSchoolYearId).ToList();
                List<ASDCourse> asdCoursesNY = repoASDCourse.GetAll(newSchoolYearId).ToList();
                List<FPCSCourse> fpcsCoursesNY = repoFPCSCourse.GetAll(newSchoolYearId).ToList();
                List<Vendor> vendorsNY = repoVendor.GetAll(newSchoolYearId).ToList();

                SchoolYear oldSchoolYear = repoSchoolYear.Get(oldSchoolYearId);
                SchoolYear newSchoolYear = repoSchoolYear.Get(newSchoolYearId);

                students = students
                    .Where(x => !x.IsDeleted &&
                                NextGrade(x.Grade).HasValue &&
                                ((x.WithdrawalDate.HasValue && x.WithdrawalDate >= newSchoolYear.StartDate) ||
                                (!x.WithdrawalDate.HasValue)))
                    .ToList();

                List<Student> studentsNewYear = new List<Student>();
                foreach (var item in students)
                {
                    if (!studentsNY.Select(x => x.UnqDbUserId).Contains(item.UnqDbUserId))
                    {
                        var newStudent = repoStudent.Add(CreateNewStudent(item, newSchoolYearId));
                        studentsNewYear.Add(newStudent);
                        result.CountStudent++;
                    }
                }

                uow.Commit();

                teachers = teachers
                    .Where(x => x.IsActive && !x.IsDeleted)
                    .ToList();

                List<Teacher> teachersNewYear = new List<Teacher>();
                foreach (var item in teachers)
                {
                    if (!teachersNY.Select(x => x.UnqDbUserId).Contains(item.UnqDbUserId))
                    {
                        var newTeacher = repoTeacher.Add(CreateNewTeacher(item, newSchoolYearId));
                        teachersNewYear.Add(newTeacher);
                        result.CountTeacher++;
                    }
                }

                uow.Commit();

                List<Guardian> guardiansNewYear = new List<Guardian>();
                foreach (var item in guardians.Where(x => !x.IsDeleted).ToList())
                {
                    var studentsGuardian = item.Families.FirstOrDefault(x => !x.IsDeleted) != null ?
                        item.Families.FirstOrDefault(x => !x.IsDeleted).Students.Select(x => x.DbUserId).ToList() :
                        new List<Guid>();

                    foreach (var item2 in studentsGuardian)
                    {
                        if (students.Select(x => x.DbUserId).Contains(item2) &&
                            !guardiansNewYear.Select(x => x.UnqDbUserId).Contains(item.UnqDbUserId) &&
                            !guardiansNY.Select(x => x.UnqDbUserId).Contains(item.UnqDbUserId))
                        {
                            var newGuardian = repoGuardian.Add(CreateNewGuardian(item, newSchoolYearId));
                            guardiansNewYear.Add(newGuardian);
                            result.CountGuardian++;
                        }
                    }
                }

                uow.Commit();

                //Family Relationship
                foreach (var item in guardians.Where(x => !x.IsDeleted).ToList())
                {
                    var studentsGuardian = item.Families.FirstOrDefault(x => !x.IsDeleted) != null ?
                        item.Families.FirstOrDefault(x => !x.IsDeleted).Students.Select(x => x.DbUserId).ToList() :
                        new List<Guid>();

                    foreach (var item2 in studentsGuardian)
                    {
                        if (students.Select(x => x.DbUserId).Contains(item2) &&
                            (guardiansNY.Select(x => x.UnqDbUserId).Contains(item.UnqDbUserId) ||
                             guardiansNewYear.Select(x => x.UnqDbUserId).Contains(item.UnqDbUserId)))
                        {
                            FamilyRelationship familyRelationship = repoFamilyRelationship.Get(item.DbUserId, item2);

                            if (familyRelationship != null)
                            {
                                Guardian newGuardian = guardiansNY.FirstOrDefault(x => x.UnqDbUserId == item.UnqDbUserId);

                                if (newGuardian == null)
                                {
                                    newGuardian = guardiansNewYear.FirstOrDefault(x => x.UnqDbUserId == item.UnqDbUserId);
                                }

                                Guid unqDbUserIdStudent = students.FirstOrDefault(x => x.DbUserId == item2).UnqDbUserId;
                                Guid studentId = studentsNewYear.FirstOrDefault(x => x.UnqDbUserId == unqDbUserIdStudent) != null ?
                                    studentsNewYear.FirstOrDefault(x => x.UnqDbUserId == unqDbUserIdStudent).DbUserId :
                                    studentsNY.FirstOrDefault(x => x.UnqDbUserId == unqDbUserIdStudent).DbUserId;

                                FamilyRelationship newFamilyRelationship = repoFamilyRelationship.Get(newGuardian.DbUserId, studentId);

                                if (newFamilyRelationship == null)
                                {
                                    newFamilyRelationship = new FamilyRelationship();

                                    newFamilyRelationship.FamilyRelationshipType = familyRelationship.FamilyRelationshipType;
                                    newFamilyRelationship.GuardianId = newGuardian.DbUserId;
                                    newFamilyRelationship.StudentId = studentId;

                                    repoFamilyRelationship.Add(newFamilyRelationship);
                                }
                                else
                                {
                                    newFamilyRelationship.FamilyRelationshipType = familyRelationship.FamilyRelationshipType;
                                    newFamilyRelationship.GuardianId = newGuardian.DbUserId;
                                    newFamilyRelationship.StudentId = studentId;

                                    repoFamilyRelationship.Update(newFamilyRelationship);
                                }
                            }
                        }
                    }
                }

                uow.Commit();

                foreach (var item in families.Where(x => !x.IsDeleted).ToList())
                {
                    var guardiansFamily = item.Guardians.Select(x => x.UnqDbUserId);
                    var studentsFamily = item.Students.Select(x => x.UnqDbUserId);

                    if (guardiansFamily.Any(x => guardiansNewYear.Select(t => t.UnqDbUserId).Contains(x)) &&
                        studentsFamily.Any(x => studentsNewYear.Select(t => t.UnqDbUserId).Contains(x)) &&
                        !guardiansFamily.Any(x => guardiansNY.Select(t => t.UnqDbUserId).Contains(x)) &&
                        !studentsFamily.Any(x => studentsNY.Select(t => t.UnqDbUserId).Contains(x)))
                    {
                        var newFamily = repoFamily.Add(CreateNewFamily(item, newSchoolYearId));
                        newFamily.Students = new List<Student>();
                        newFamily.Guardians = new List<Guardian>();

                        uow.Commit();

                        result.CountFamily++;

                        var newGuardians = guardiansNewYear.Where(x => guardiansFamily.Any(t => t == x.UnqDbUserId)).Select(x => x.DbUserId).ToList();
                        var newStudents = studentsNewYear.Where(x => studentsFamily.Any(t => t == x.UnqDbUserId)).Select(x => x.DbUserId).ToList();

                        repoFamily.UpdateGuardians(newFamily, newGuardians);
                        repoFamily.UpdateStudents(newFamily, newStudents);

                        uow.Commit();
                    }
                }

                asdCourses = asdCourses
                                .Where(x => !x.IsDeleted)
                                .ToList();

                List<ASDCourse> asdCoursesNewYear = new List<ASDCourse>();
                foreach (var item in asdCourses)
                {
                    if (!asdCoursesNY.Select(x => x.UnqASDCourseId).Contains(item.UnqASDCourseId))
                    {
                        var newASDCourse = repoASDCourse.Add(CreateNewASDCourse(item, newSchoolYearId));
                        asdCoursesNewYear.Add(newASDCourse);
                        result.CountASDCourse++;
                    }
                }

                uow.Commit();

                List<Vendor> vendorsNewYear = new List<Vendor>();
                foreach (var item in vendors)
                {
                    if (!vendorsNY.Select(x => x.UnqVendorID).Contains(item.UnqVendorID))
                    {
                        var newVendor = repoVendor.Add(CreateNewVendor(item, newSchoolYearId));
                        vendorsNewYear.Add(newVendor);
                        result.CountVendor++;
                    }
                }

                uow.Commit();

                return result;
            }
        }

        private static Grade? NextGrade(Grade grade)
        {
            Grade? result = null;

            switch (grade)
            {
                case Grade.K : 
                    result = Grade.G1;
                    break;
                case Grade.G1 : 
                    result = Grade.G2;
                    break;
                case Grade.G2 :
                    result = Grade.G3;
                    break;
                case Grade.G3 :
                    result = Grade.G4;
                    break;
                case Grade.G4 :
                    result = Grade.G5;
                    break;
                case Grade.G5 :
                    result = Grade.G6;
                    break;
                case Grade.G6 :
                    result = Grade.G7;
                    break;
                case Grade.G7 :
                    result = Grade.G8;
                    break;
                case Grade.G8 :
                    result = Grade.G9;
                    break;
                case Grade.G9 :
                    result = Grade.G10;
                    break;
                case Grade.G10 :
                    result = Grade.G11;
                    break;
                case Grade.G11 :
                    result = Grade.G12;
                    break;
                case Grade.G12 :
                    result = Grade.G;
                    break;
                case Grade.G :
                    result = null;
                    break;
                default : 
                    throw new Exception("FATAL ERROR!");
            }

            return result;
        }

        private static Student CreateNewStudent(Student student, Int32 schoolYearId)
        {
            var nextGrade = NextGrade(student.Grade);

            Student newStudent = new Student();

            newStudent.DateOfBirth = student.DateOfBirth;
            newStudent.Email = student.Email;
            newStudent.EnrollmentStatus = student.EnrollmentStatus;
            newStudent.FirstName = student.FirstName;
            newStudent.FullName = student.FullName;
            newStudent.Grade = nextGrade.Value;
            newStudent.GradYear = student.GradYear + 1;
            newStudent.IEPAutismAsbergerSyndrome = student.IEPAutismAsbergerSyndrome;
            newStudent.IEPDeafBlindness = student.IEPDeafBlindness;
            newStudent.IEPDeafness = student.IEPDeafness;
            newStudent.IEPEarlyChildhoodDevelopmentalDelay = student.IEPEarlyChildhoodDevelopmentalDelay;
            newStudent.IEPEmotionalDisturbance = student.IEPEmotionalDisturbance;
            newStudent.IEPGiftedTalented = student.IEPGiftedTalented;
            newStudent.IEPHasChildCurrentlyReceivingServicesBilingualProgram = student.IEPHasChildCurrentlyReceivingServicesBilingualProgram;
            newStudent.IEPHasChildEligible = student.IEPHasChildEligible;
            newStudent.IEPHasChildExperiencedLearned = student.IEPHasChildExperiencedLearned;
            newStudent.IEPHasChildFormally = student.IEPHasChildFormally;
            newStudent.IEPHasChildLearnedAnotherLanguage = student.IEPHasChildLearnedAnotherLanguage;
            newStudent.IEPHasChildReceiving = student.IEPHasChildReceiving;
            newStudent.IEPHasChildReceivingServicesMigrantEducation = student.IEPHasChildReceivingServicesMigrantEducation;
            newStudent.IEPHearingImpairment = student.IEPHearingImpairment;
            newStudent.IEPIsChildEligibleBilingualProgram = student.IEPIsChildEligibleBilingualProgram;
            newStudent.IEPIsExpirationDate = student.IEPIsExpirationDate;
            newStudent.IEPIsNextEligibilityEvaluation = student.IEPIsNextEligibilityEvaluation;
            newStudent.IEPMentalRetardation = student.IEPMentalRetardation;
            newStudent.IEPMultipleDisability = student.IEPMultipleDisability;
            newStudent.IEPOrthopedicImpairment = student.IEPOrthopedicImpairment;
            newStudent.IEPOtherHealthImpairment = student.IEPOtherHealthImpairment;
            newStudent.IEPSpecificLearningDisability = student.IEPSpecificLearningDisability;
            newStudent.IEPSpeechLanguageImpairment = student.IEPSpeechLanguageImpairment;
            newStudent.IEPTraumaticBrainInjury = student.IEPTraumaticBrainInjury;
            newStudent.IEPVisualImpairment = student.IEPVisualImpairment;
            newStudent.ILPPhilosophy = student.ILPPhilosophy;
            newStudent.IsASDContractHoursExemption = student.IsASDContractHoursExemption;
            newStudent.IsBirthCertificate = student.IsBirthCertificate;
            newStudent.IsDeleted = student.IsDeleted;
            newStudent.IsDoYouPlanGraduate = student.IsDoYouPlanGraduate;
            newStudent.IsGradesNotSubmitted = student.IsGradesNotSubmitted;
            newStudent.IsGraduateFromFPCS = student.IsGraduateFromFPCS;
            newStudent.IsILPPhilosophy = student.IsILPPhilosophy;
            newStudent.IsLocked = student.IsLocked;
            newStudent.IsMedicalRelease = student.IsMedicalRelease;
            newStudent.IsOther = student.IsOther;
            newStudent.IsProgressReportSignature = student.IsProgressReportSignature;
            newStudent.IsShotRecords = student.IsShotRecords;
            newStudent.IsTestingAgreement = student.IsTestingAgreement;
            newStudent.LastName = student.LastName;
            newStudent.Login = student.Login;
            newStudent.MiddleInitial = student.MiddleInitial;
            newStudent.Password = student.Password;
            newStudent.PercentagePlanningEnroll = student.PercentagePlanningEnroll;
            newStudent.PercentInSchoolDistrict = student.PercentInSchoolDistrict;
            newStudent.PrivateSchoolName = student.PrivateSchoolName;
            newStudent.ReasonASDContractHoursExemption = student.ReasonASDContractHoursExemption;
            newStudent.Role = student.Role;
            newStudent.SchoolYearId = schoolYearId;
            newStudent.Sex = student.Sex;
            newStudent.UnqDbUserId = student.UnqDbUserId;
            newStudent.CreatedDate = DateTime.Now;
            newStudent.UpdatedDate = DateTime.Now;
            newStudent.WithdrawalDate = student.WithdrawalDate;

            return newStudent;
        }

        private static Teacher CreateNewTeacher(Teacher teacher, Int32 schooYearId)
        {
            Teacher newTeacher = new Teacher();

            newTeacher.AlaskaCertificationSubjectGrades = teacher.AlaskaCertificationSubjectGrades;
            newTeacher.ASDFTE = teacher.ASDFTE;
            newTeacher.BasePayHour = teacher.BasePayHour;
            newTeacher.BusPhone = teacher.BusPhone;
            newTeacher.CellPhone = teacher.CellPhone;
            newTeacher.City = teacher.City;
            newTeacher.CreatedDate = DateTime.Now;
            newTeacher.DateBirth = teacher.DateBirth;
            newTeacher.DistrictCode = teacher.DistrictCode;
            newTeacher.Email = teacher.Email;
            newTeacher.Ext = teacher.Ext;
            newTeacher.FirstName = teacher.FirstName;
            newTeacher.FlatRateHour = teacher.FlatRateHour;
            newTeacher.FPCSFTE = teacher.FPCSFTE;
            newTeacher.FTCSFTE = teacher.FTCSFTE;
            newTeacher.FullName = teacher.FullName;
            newTeacher.HomePhone = teacher.HomePhone;
            newTeacher.IsActive = teacher.IsActive;
            newTeacher.IsAlaskaCertificationK12 = teacher.IsAlaskaCertificationK12;
            newTeacher.IsAlaskaCertificationK8 = teacher.IsAlaskaCertificationK8;
            newTeacher.IsAlaskaCertificationSecondary = teacher.IsAlaskaCertificationSecondary;
            newTeacher.IsAlaskaCertificationSpecialEducation = teacher.IsAlaskaCertificationSpecialEducation;
            newTeacher.IsAsStudentsHome = teacher.IsAsStudentsHome;
            newTeacher.IsAtFPCSClassroom = teacher.IsAtFPCSClassroom;
            newTeacher.IsAtMyHome = teacher.IsAtMyHome;
            newTeacher.IsBenefitPaidASDSchool = teacher.IsBenefitPaidASDSchool;
            newTeacher.IsDeleted = teacher.IsDeleted;
            newTeacher.IsGroupInstruction = teacher.IsGroupInstruction;
            newTeacher.IsGuardian = teacher.IsGuardian;
            newTeacher.IsIndividualInstruction = teacher.IsIndividualInstruction;
            newTeacher.IsInMyClassroom = teacher.IsInMyClassroom;
            newTeacher.IsLeaveASD = teacher.IsLeaveASD;
            newTeacher.IsLocked = teacher.IsLocked;
            newTeacher.IsNotIncludeBenefits = teacher.IsNotIncludeBenefits;
            newTeacher.IsOnASDEligibleToHire = teacher.IsOnASDEligibleToHire;
            newTeacher.IsOtherASDEmployee = teacher.IsOtherASDEmployee;
            newTeacher.IsOtherAvailableTeach = teacher.IsOtherAvailableTeach;
            newTeacher.IsRetiredASDTeacher = teacher.IsRetiredASDTeacher;
            newTeacher.IsSubtitleTeacher = teacher.IsSubtitleTeacher;
            newTeacher.IsSummers = teacher.IsSummers;
            newTeacher.IsWeekdayAfternoons = teacher.IsWeekdayAfternoons;
            newTeacher.IsWeekdayEvenings = teacher.IsWeekdayEvenings;
            newTeacher.IsWeekdays = teacher.IsWeekdays;
            newTeacher.IsWeekends = teacher.IsWeekends;
            newTeacher.LastName = teacher.LastName;
            newTeacher.Login = teacher.Login;
            newTeacher.MailingAddress = teacher.MailingAddress;
            newTeacher.MastersDegree = teacher.MastersDegree;
            newTeacher.MiddleInitial = teacher.MiddleInitial;
            newTeacher.NameSchool = teacher.NameSchool;
            newTeacher.OtherAvailableTeach = teacher.OtherAvailableTeach;
            newTeacher.Password = teacher.Password;
            newTeacher.PayHourwBenefits = teacher.PayHourwBenefits;
            newTeacher.PayType = teacher.PayType;
            newTeacher.PerDeimRate = teacher.PerDeimRate;
            newTeacher.Role = teacher.Role;
            newTeacher.SchoolYearId = schooYearId;
            newTeacher.SecondEMailAddress = teacher.SecondEMailAddress;
            newTeacher.SSN = teacher.SSN;
            newTeacher.StateId = teacher.StateId;
            newTeacher.TeachingCertificateExpirationDate = teacher.TeachingCertificateExpirationDate;
            newTeacher.TeachingSalaryPlacement = teacher.TeachingSalaryPlacement;
            newTeacher.Title = teacher.Title;
            newTeacher.UnqDbUserId = teacher.UnqDbUserId;
            newTeacher.UpdatedDate = DateTime.Now;
            newTeacher.YearsTeachingExperience = teacher.YearsTeachingExperience + 1;
            newTeacher.ZipCode = teacher.ZipCode;

            return newTeacher;
        }

        private static Guardian CreateNewGuardian(Guardian guardian, Int32 schoolYearId)
        {
            Guardian newGuardian = new Guardian();

            newGuardian.Address = guardian.Address;
            newGuardian.BusinessPhone = guardian.BusinessPhone;
            newGuardian.CellPhone = guardian.CellPhone;
            newGuardian.City = guardian.City;
            newGuardian.Country = guardian.Country;
            newGuardian.CreatedDate = DateTime.Now;
            newGuardian.Email = guardian.Email;
            newGuardian.Employer = guardian.Employer;
            newGuardian.Ext = guardian.Ext;
            newGuardian.FirstName = guardian.FirstName;
            newGuardian.FullName = guardian.FullName;
            newGuardian.IsActiveMilitary = guardian.IsActiveMilitary;
            newGuardian.IsDeleted = guardian.IsDeleted;
            newGuardian.IsLocked = guardian.IsLocked;
            newGuardian.LastName = guardian.LastName;
            newGuardian.Login = guardian.Login;
            newGuardian.MiddleInitial = guardian.MiddleInitial;
            newGuardian.Pager = guardian.Pager;
            newGuardian.Password = guardian.Password;
            newGuardian.Rank = guardian.Rank;
            newGuardian.Role = guardian.Role;
            newGuardian.SchoolYearId = schoolYearId;
            newGuardian.StateId = guardian.StateId;
            newGuardian.UnqDbUserId = guardian.UnqDbUserId;
            newGuardian.UpdatedDate = DateTime.Now;
            newGuardian.Zip = guardian.Zip;

            return newGuardian;
        }

        private static Family CreateNewFamily(Family family, Int32 schoolYearId)
        {
            Family newFamily = new Family();

            newFamily.Address = family.Address;
            newFamily.City = family.City;
            newFamily.Country = family.Country;
            newFamily.CreatedDate = DateTime.Now;
            newFamily.Description = family.Description;
            newFamily.Email = family.Email;
            newFamily.IsDeleted = family.IsDeleted;
            newFamily.IsUseDirectory = family.IsUseDirectory;
            newFamily.Name = family.Name;
            newFamily.SchoolYearId = schoolYearId;
            newFamily.StateId = family.StateId;
            newFamily.Telephone = family.Telephone;
            newFamily.UpdatedDate = DateTime.Now;
            newFamily.Zip = family.Zip;

            return newFamily;
        }

        private static ASDCourse CreateNewASDCourse(ASDCourse asdCourse, Int32 schoolYearId)
        {
            ASDCourse newASDCourse = new ASDCourse();

            newASDCourse.CreatedDate = DateTime.Now;
            newASDCourse.Description = asdCourse.Description;
            newASDCourse.ExternalASDCourseId = asdCourse.ExternalASDCourseId;
            newASDCourse.GradCredit = asdCourse.GradCredit;
            newASDCourse.IsActivated = asdCourse.IsActivated;
            newASDCourse.IsDeleted = asdCourse.IsDeleted;
            newASDCourse.Name = asdCourse.Name;
            newASDCourse.SchoolYearId = schoolYearId;
            newASDCourse.SubjectId = asdCourse.SubjectId;
            newASDCourse.UnqASDCourseId = asdCourse.UnqASDCourseId;
            newASDCourse.UpdatedDate = DateTime.Now;

            return newASDCourse;
        }

        private static Vendor CreateNewVendor(Vendor vendor, Int32 schoolYearId)
        {
            Vendor newVendor = new Vendor();

            newVendor.AKBusinessLicense = vendor.AKBusinessLicense;
            newVendor.BusinessName = vendor.BusinessName;
            newVendor.BusinessWebsite = vendor.BusinessWebsite;
            newVendor.CommentsAboutServices = vendor.CommentsAboutServices;
            newVendor.ContractStartingDate = vendor.ContractStartingDate;
            newVendor.Email = vendor.Email;
            newVendor.EmployerIdentification = vendor.EmployerIdentification;
            newVendor.Fax = vendor.Fax;
            newVendor.FirstName = vendor.FirstName;
            newVendor.InsuranceExpiration = vendor.InsuranceExpiration;
            newVendor.IsASDEmployeeEligibleHireList = vendor.IsASDEmployeeEligibleHireList;
            newVendor.IsBackgroundCheckFingerprinting = vendor.IsBackgroundCheckFingerprinting;
            newVendor.IsCurrentlyAvailableProvideServices = vendor.IsCurrentlyAvailableProvideServices;
            newVendor.IsHaveChildrenCurrentlyEnrolled = vendor.IsHaveChildrenCurrentlyEnrolled;
            newVendor.IsMisdemeanorOrFelony = vendor.IsMisdemeanorOrFelony;
            newVendor.IsNonProfit = vendor.IsNonProfit;
            newVendor.IsProvideOnlineCurriculumServices = vendor.IsProvideOnlineCurriculumServices;
            newVendor.IsRetiredCertificatedASDTeacher = vendor.IsRetiredCertificatedASDTeacher;
            newVendor.LastName = vendor.LastName;
            newVendor.LicenseExpiration = vendor.LicenseExpiration;
            newVendor.Location = vendor.Location;
            newVendor.MailingAddress = vendor.MailingAddress;
            newVendor.MailingAddressCity = vendor.MailingAddressCity;
            newVendor.MailingAddressStateId = vendor.MailingAddressStateId;
            newVendor.MailingAddressZipCode = vendor.MailingAddressZipCode;
            newVendor.OtherChargeMethod = vendor.OtherChargeMethod;
            newVendor.Phone = vendor.Phone;
            newVendor.Price = vendor.Price;
            newVendor.SchoolYearId = schoolYearId;
            newVendor.Status = vendor.Status;
            newVendor.StatusComments = vendor.StatusComments;
            newVendor.StreetAddress = vendor.StreetAddress;
            newVendor.StreetAddressCity = vendor.StreetAddressCity;
            newVendor.StreetAddressStateId = vendor.StreetAddressStateId;
            newVendor.StreetAddressZipCode = vendor.StreetAddressZipCode;
            newVendor.TrainingEducationExperience = vendor.TrainingEducationExperience;
            newVendor.Type = vendor.Type;
            newVendor.UnqVendorID = vendor.UnqVendorID;
            newVendor.VendorUnitType = vendor.VendorUnitType;

            return newVendor;
        }
    }
}
