﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Import.BetweenYears.Models.Years
{
    public class YearsModel
    {
        public Int32 CountStudent { get; set; }
        public Int32 CountTeacher { get; set; }
        public Int32 CountGuardian { get; set; }
        public Int32 CountFamily { get; set; }
        public Int32 CountASDCourse { get; set; }
        public Int32 CountVendor { get; set; }
    }
}
