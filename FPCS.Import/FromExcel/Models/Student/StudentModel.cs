﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Import.FromExcel.Models.Student
{
    public class StudentModel
    {
        public String StudentName { get; set; }
        public String StudentLastName
        {
            get
            {
                return StudentName.Split(new String[] { ", " }, StringSplitOptions.RemoveEmptyEntries)[1];
            }
        }
        public String StudentFirstName
        {
            get
            {
                return StudentName.Split(new String[] { ", " }, StringSplitOptions.RemoveEmptyEntries)[0];
            }
        }
        public String Grade { get; set; }
        public String Zangle { get; set; }
        public String SponsorTeacher { get; set; }
        public String SponsorTeacherLastName
        {
            get
            {
                return SponsorTeacher.Split(' ')[0];
            }
        }
        public String SponsorTeacherFirstName
        {
            get
            {
                return SponsorTeacher.Split(' ').Count() > 1 ? SponsorTeacher.Split(' ')[1] : String.Empty;
            }
        }
        public String HomeAddress { get; set; }
        public String City { get; set; }
        public String State { get; set; }
        public String Zip { get; set; }
        public String MailingAddress { get; set; }
        public String City2 { get; set; }
        public String State2 { get; set; }
        public String Zip2 { get; set; }
        public String Contact { get; set; }
        public String GuardianFirstName { get; set; }
        public String GuardianLastName { get; set; }
        public String GuardianPhone { get; set; }
        public String GuardianTypePhone { get; set; }
    }
}
