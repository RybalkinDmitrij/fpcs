﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Import.FromExcel.Models.ILP
{
    public class ILPPhilosophyModel
    {
        public String FirstName { get; set; }

        public String LastName { get; set; }

        public String ILPPhilosophy { get; set; }
    }
}
