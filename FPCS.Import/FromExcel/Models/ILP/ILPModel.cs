﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Import.FromExcel.Models.ILP
{
    public class ILPModel
    {
        public Decimal CourseHours { get; set; }
        public String CurriculumDesc { get; set; }
        public String Goals { get; set; }
        public String Requirements { get; set; }
        public String TeacherRole { get; set; }
        public String StudentRole { get; set; }
        public String ParentRole { get; set; }
        public String Evaluation { get; set; }
        public Boolean IsPassFail { get; set; }
        public Boolean IsOtherGrading { get; set; }
        public String OtherGrading { get; set; }
        public String ILPName { get; set; }
        public String ILPAdditions { get; set; }
        public Boolean IsGradingScale { get; set; }
        public String InstLastName { get; set; }
        public String InstFirstName { get; set; }
        public String GuarLastName { get; set; }
        public String GuarFirstName { get; set; }
        public String ASDCourseID { get; set; }
        public Boolean IsPublic { get; set; }
    }
}
