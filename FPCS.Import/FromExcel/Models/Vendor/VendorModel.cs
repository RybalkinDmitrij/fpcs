﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Import.FromExcel.Models.Vendor
{
    public class VendorModel
    {
        public String VendorId { get; set; }
        public String IsGood { get; set; }
        public String IsService { get; set; }
        public String VendorName { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String Status { get; set; }
        public String Comments { get; set; }
        public String Address { get; set; }
        public String City { get; set; }
        public String State { get; set; }
        public String Zip { get; set; }
        public String MailAddress { get; set; }
        public String MailCity { get; set; }
        public String MailState { get; set; }
        public String MailZip { get; set; }
        public String Phone { get; set; }
        public String Fax { get; set; }
        public String Email { get; set; }
        public String LocationId { get; set; }
        public String Website { get; set; }
        public String SSN { get; set; }
        public String BusinessLicense { get; set; }
        public String LicenseExper { get; set; }
        public String InsureExper { get; set; }
        public String PrevExper { get; set; }
        public String ChargeType { get; set; }
        public String OtherChargeMethod { get; set; }
        public String ChargeAmount { get; set; }
        public String ContractStart { get; set; }
        public String IsNonProfit { get; set; }
        public String IsForPay { get; set; }
        public String IsChrime { get; set; }
        public String IsConsent { get; set; }
        public String IsKidsFPCS { get; set; }
        public String IsApproved { get; set; }
        public String IsCertASDTeacher { get; set; }
        public String IsCurrentASDEmp { get; set; }
        public String IsEligibleHire { get; set; }
        public String IsConflictIntWaiver { get; set; }
        public String IsConflictIntWaiverRecvd { get; set; }
        public String IsIncludeDir { get; set; }
        public String IsNonSectarian { get; set; }
        public String IsOnlineService { get; set; }
        public String IsActive { get; set; }
    }
}
