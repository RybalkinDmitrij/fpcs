﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Import.FromExcel.Models.Teacher
{
    public class TeacherModel
    {
        public String InstructorID { get; set; }
        public String ContractASD { get; set; }
        public String Title { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String MidInitial { get; set; }
        public String SSN { get; set; }
        public String MailingAddr { get; set; }
        public String City { get; set; }
        public String State { get; set; }
        public String ZipCode { get; set; }
        public String HomePhone { get; set; }
        public String BusinessPhone { get; set; }
        public String BusinessExt { get; set; }
        public String CellPhone { get; set; }
        public String Email { get; set; }
        public String Email2 { get; set; }
        public String DateBirth { get; set; }
        public String MastersDegree { get; set; }
        public String DistCode { get; set; }
        public String Location { get; set; }
        public String ASDLeave { get; set; }
        public String Substitute { get; set; }
        public String ASDEmployee { get; set; }
        public String ASDEligibleForHire { get; set; }
        public String ASDRetired { get; set; }
        public String GroupInstruction { get; set; }
        public String IndividualInstruction { get; set; }
        public String YearsExperience { get; set; }
        public String K8 { get; set; }
        public String K12 { get; set; }
        public String SpecialEd { get; set; }
        public String Secondary { get; set; }
        public String SecondaryList { get; set; }
        public String MyClassroom { get; set; }
        public String MyHome { get; set; }
        public String StudentsHome { get; set; }
        public String FPCSClassroom { get; set; }
        public String Other { get; set; }
        public String OtherDesc { get; set; }
        public String AvailWeekdays { get; set; }
        public String AvailAfternoon { get; set; }
        public String AvailEvening { get; set; }
        public String AvailEnds { get; set; }
        public String AvailSummers { get; set; }
        public String ASDSchool { get; set; }
        public String dtCertExper { get; set; }
        public String SalaryPlacement { get; set; }
        public String CurPayRate { get; set; }
        public String PayTypeId { get; set; }
        public String ASDFullTime { get; set; }
        public String ASDFullTimePercent { get; set; }
        public String ASDPartTime { get; set; }
        public String ASDPartTimePercent { get; set; }
        public String FPCSHoursGoal { get; set; }
        public String IsActive { get; set; }
    }
}
