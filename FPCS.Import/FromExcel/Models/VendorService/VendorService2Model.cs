﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Import.FromExcel.Models.VendorService
{
    public class VendorService2Model
    {
        public String VendorName { get; set; }
        public String VendorServiceName { get; set; }
        public String SubjectName { get; set; }
    }
}
