﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Import.FromExcel.Models.VendorService
{
    public class VendorServiceModel
    {
        public String VendorServiceId { get; set; }
        public String VendorServiceName { get; set; }
        public String SubjectName { get; set; }
        public String IsActive { get; set; }
    }
}
