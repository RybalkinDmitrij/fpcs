﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FPCS.Import.FromExcel.Models.ASDCourse
{
    public class ASDCourseModel
    {
        public String CourseCode { get; set; }
        public String CourseDescription { get; set; }
        public String Subject { get; set; }
        public String Department { get; set; }
        public Decimal CardCredit { get; set; }
    }
}
