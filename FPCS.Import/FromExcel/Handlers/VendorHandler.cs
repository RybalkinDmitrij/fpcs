﻿using ClosedXML.Excel;
using FPCS.Core.Unity;
using FPCS.Data;
using FPCS.Data.Entities;
using FPCS.Data.Enums;
using FPCS.Data.Repo;
using FPCS.Import.Enum;
using FPCS.Import.FromExcel.Models.Vendor;
using FPCS.Import.State;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Import.FromExcel.Handlers
{
    public static class VendorHandler
    {
        public static Status ImportData(Stream stream, Int32 schoolYearId)
        {
            Status answer = new Status();

            try
            {
                var wb = new XLWorkbook(stream);

                IXLWorksheet ws = wb.Worksheet(1);

                List<VendorModel> xlsxData = new List<VendorModel>();

                foreach (IXLRow item in ws.Rows(2, ws.Column(1).Cells().Count()))
                {
                    VendorModel importLine = new VendorModel();

                    importLine.VendorId = item.Cell(1).Value.ToString();
                    importLine.IsGood = item.Cell(2).Value.ToString();
                    importLine.IsService = item.Cell(3).Value.ToString();
                    importLine.VendorName = item.Cell(4).Value.ToString();
                    importLine.FirstName = item.Cell(5).Value.ToString();
                    importLine.LastName = item.Cell(6).Value.ToString();
                    importLine.Status = item.Cell(7).Value.ToString();
                    importLine.Comments = item.Cell(8).Value.ToString();
                    importLine.Address = item.Cell(9).Value.ToString();
                    importLine.City = item.Cell(10).Value.ToString();
                    importLine.State = item.Cell(11).Value.ToString();
                    importLine.Zip = item.Cell(12).Value.ToString();
                    importLine.MailAddress = item.Cell(13).Value.ToString();
                    importLine.MailCity = item.Cell(14).Value.ToString();
                    importLine.MailState = item.Cell(15).Value.ToString();
                    importLine.MailZip = item.Cell(16).Value.ToString();
                    importLine.Phone = item.Cell(17).Value.ToString();
                    importLine.Fax = item.Cell(18).Value.ToString();
                    importLine.Email = item.Cell(19).Value.ToString();
                    importLine.LocationId = item.Cell(20).Value.ToString();
                    importLine.Website = item.Cell(21).Value.ToString();
                    importLine.SSN = item.Cell(22).Value.ToString();
                    importLine.BusinessLicense = item.Cell(23).Value.ToString();
                    importLine.LicenseExper = item.Cell(24).Value.ToString();
                    importLine.InsureExper = item.Cell(25).Value.ToString();
                    importLine.PrevExper = item.Cell(26).Value.ToString();
                    importLine.ChargeType = item.Cell(27).Value.ToString();
                    importLine.OtherChargeMethod = item.Cell(28).Value.ToString();
                    importLine.ChargeAmount = item.Cell(29).Value.ToString();
                    importLine.ContractStart = item.Cell(30).Value.ToString();
                    importLine.IsNonProfit = item.Cell(31).Value.ToString();
                    importLine.IsForPay = item.Cell(32).Value.ToString();
                    importLine.IsChrime = item.Cell(33).Value.ToString();
                    importLine.IsConsent = item.Cell(34).Value.ToString();
                    importLine.IsKidsFPCS = item.Cell(35).Value.ToString();
                    importLine.IsApproved = item.Cell(36).Value.ToString();
                    importLine.IsCertASDTeacher = item.Cell(37).Value.ToString();
                    importLine.IsCurrentASDEmp = item.Cell(38).Value.ToString();
                    importLine.IsEligibleHire = item.Cell(39).Value.ToString();
                    importLine.IsConflictIntWaiver = item.Cell(40).Value.ToString();
                    importLine.IsConflictIntWaiverRecvd = item.Cell(41).Value.ToString();
                    importLine.IsIncludeDir = item.Cell(42).Value.ToString();
                    importLine.IsNonSectarian = item.Cell(43).Value.ToString();
                    importLine.IsOnlineService = item.Cell(44).Value.ToString();
                    importLine.IsActive = item.Cell(45).Value.ToString();
                    
                    xlsxData.Add(importLine);
                }

                using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
                {
                    var repo = uow.GetRepo<IVendorRepo>();
                    var repoState = uow.GetRepo<IStateRepo>();

                    foreach (var item in xlsxData)
                    {
                        Vendor data = new Vendor();

                        data.SchoolYearId = schoolYearId;
                        data.Type = item.IsGood == "1" && item.IsService == "1" ? VendorType.Both :
                            item.IsGood == "1" && item.IsService == "0" ? VendorType.Goods :
                            item.IsGood == "0" && item.IsService == "1" ? VendorType.Service :
                            VendorType.NonProfit;

                        data.BusinessName = item.VendorName != "NULL" ? item.VendorName : String.Empty;
                        data.FirstName = item.FirstName != "NULL" ? item.FirstName : String.Empty;
                        data.LastName = item.LastName != "NULL" ? item.LastName : String.Empty;
                        data.Status = item.Status == "APPR" ? VendorStatus.Approved :
                            item.Status == "PEND" ? VendorStatus.Pending :
                            item.Status == "REJC" ? VendorStatus.Rejected :
                            item.Status == "REMV" ? VendorStatus.Remove :
                            VendorStatus.Remove;
                        data.IsActive = true;
                        data.StatusComments = item.Comments != "NULL" ? item.Comments : String.Empty;
                        data.StreetAddress = item.Address != "NULL" ? item.Address : String.Empty;
                        data.StreetAddressCity = item.City != "NULL" ? item.City : String.Empty;
                        data.StreetAddressState = repoState.GetAll().FirstOrDefault(x => x.Code == item.State) != null ?
                            repoState.GetAll().FirstOrDefault(x => x.Code == item.State) :
                            repoState.GetAll().FirstOrDefault(x => x.Code == "AK");
                        data.StreetAddressZipCode = item.Zip != "NULL" ? item.Zip : String.Empty;
                        data.MailingAddress = item.MailAddress != "NULL" ? item.MailAddress : String.Empty;
                        data.MailingAddressCity = item.MailCity != "NULL" ? item.MailCity : String.Empty;
                        data.MailingAddressState = repoState.GetAll().FirstOrDefault(x => x.Code == item.MailState) != null ?
                            repoState.GetAll().FirstOrDefault(x => x.Code == item.MailState) :
                            repoState.GetAll().FirstOrDefault(x => x.Code == "AK");
                        data.MailingAddressZipCode = item.MailZip != "NULL" ? item.MailZip : String.Empty;
                        data.Phone = item.Phone != "NULL" ? item.Phone : String.Empty;
                        data.Fax = item.Fax != "NULL" ? item.Fax : String.Empty;
                        data.Email = item.Email != "NULL" ? item.Email : String.Empty;

                        if (item.LocationId == "1") { data.Location = Location.Eastside; }
                        else if (item.LocationId == "2") { data.Location = Location.Westside; }
                        else if (item.LocationId == "3") { data.Location = Location.Downtown; }
                        else if (item.LocationId == "4") { data.Location = Location.Midtown; }
                        else if (item.LocationId == "5") { data.Location = Location.Hillside; }
                        else if (item.LocationId == "6") { data.Location = Location.Southside; }
                        else if (item.LocationId == "7") { data.Location = Location.EagleRiver; }

                        data.BusinessWebsite = item.Website != "NULL" ? item.Website : String.Empty;
                        data.EmployerIdentification = item.SSN != "NULL" ? item.SSN : String.Empty;
                        data.AKBusinessLicense = item.BusinessLicense != "NULL" ? item.BusinessLicense : String.Empty;

                        DateTime tempDT = DateTime.MinValue;
                        DateTime.TryParse(item.LicenseExper, out tempDT);

                        data.LicenseExpiration = tempDT != DateTime.MinValue ? (DateTime?)tempDT : null;

                        tempDT = DateTime.MinValue;
                        DateTime.TryParse(item.InsureExper, out tempDT);

                        data.InsuranceExpiration = tempDT != DateTime.MinValue ? (DateTime?)tempDT : null;
                        data.TrainingEducationExperience = item.PrevExper != "NULL" ? item.PrevExper : String.Empty;
                        data.VendorUnitType = item.ChargeType == "1" ? VendorUnitType.Hour :
                            item.ChargeType == "2" ? VendorUnitType.Lesson :
                            item.ChargeType == "3" ? VendorUnitType.Month :
                            item.ChargeType == "4" ? VendorUnitType.Semester :
                            item.ChargeType == "5" ? VendorUnitType.Other :
                            item.ChargeType == "6" ? VendorUnitType.Course :
                            VendorUnitType.Hour;

                        data.OtherChargeMethod = item.OtherChargeMethod != "NULL" ? item.OtherChargeMethod : String.Empty;

                        Decimal temp = 0;
                        Decimal.TryParse(item.ChargeAmount, out temp);

                        data.Price = temp;

                        tempDT = DateTime.MinValue;
                        DateTime.TryParse(item.ContractStart, out tempDT);

                        data.ContractStartingDate = tempDT != DateTime.MinValue ? (DateTime?)tempDT : null;

                        Boolean isLog = false;
                        isLog = item.IsNonProfit.Trim() == "1" ? true : false;
                        data.IsNonProfit = isLog;

                        isLog = false;
                        isLog = item.IsChrime.Trim() == "1" ? true : false;
                        data.IsMisdemeanorOrFelony = isLog;
                        
                        isLog = false;
                        isLog = item.IsConsent.Trim() == "1" ? true : false;
                        data.IsBackgroundCheckFingerprinting = isLog;

                        isLog = false;
                        isLog = item.IsKidsFPCS.Trim() == "1" ? true : false;
                        data.IsHaveChildrenCurrentlyEnrolled = isLog;

                        isLog = false;
                        isLog = item.IsCertASDTeacher.Trim() == "1" ? true : false;
                        data.IsRetiredCertificatedASDTeacher = isLog;

                        isLog = false;
                        isLog = item.IsCurrentASDEmp.Trim() == "1" ? true : false;
                        data.IsASDEmployeeEligibleHireList = isLog;

                        isLog = false;
                        isLog = item.IsActive.Trim() == "1" ? true : false;
                        data.IsCurrentlyAvailableProvideServices = isLog;

                        isLog = false;
                        isLog = item.IsOnlineService.Trim() == "1" ? true : false;
                        data.IsProvideOnlineCurriculumServices = isLog;

                        repo.Add(data);
                        
                        uow.Commit();
                    }
                }

                answer.StateImport = StateImport.Successed;
                answer.Title = "Data has been successfully loaded";
            }
            catch (Exception ex)
            {
                answer.StateImport = StateImport.Failed;
                answer.Title = "Failed import data";
            }

            return answer;
        }

        public static Status ImportData2(Int32 schoolYearId)
        {
            Status answer = new Status();
            DataTable table = new DataTable();

            try
            {
                using (SqlConnection cnn = new SqlConnection("Server=sql2k804.discountasp.net;Database=SQL2008R2_894208_fpcs;User ID=SQL2008R2_894208_fpcs_user;Password=Password1;Connection Timeout=30;"))
                {
                    var sql = @"
                        SELECT dt.[intVendor_ID] as VendorId
                          ,dt.[szVendor_Name] as VendorName
                          ,dt.[szVendor_Phone] as Phone
                          ,dt.[szVendor_Fax] as Fax
                          ,dt.[szVendor_Contact] as ContractStart
                          ,dt.[szContact_First_Name] as FirstName
                          ,dt.[szContact_Last_Name] as LastName
                          ,dt.[szVendor_Addr] as Address
                          ,dt.[szVendor_City] as City
                          ,dt.[sVendor_State] as State
                          ,dt.[szVendor_Zip_Code] as Zip
                          ,dt.[szMail_Addr] as MailAddress
                          ,dt.[szMail_City] as MailCity
                          ,dt.[sMail_State] as MailState
                          ,dt.[szMail_Zip_Code] as MailZip
                          ,dt.[szVendor_Tax_ID]
                          ,dt.[szSSN] as SSN
                          ,dt.[szBusiness_License] as BusinessLicense
                          ,dt.[dtLicense_Expires] as LicenseExper
                          ,dt.[dtInsurance_Expires] as InsureExper
                          ,dt.[szVendor_Email] as Email
                          ,dt.[szVendor_Website] as Website
                          ,dt.[szVendor_Service]
                          ,dt.[szPrev_Experience] as PrevExper
                          ,dt.[bolFor_Pay] as IsForPay
                          ,dt.[intCharge_Type_ID] as ChargeType
                          ,dt.[szOther_Charge_Method] as OtherChargeMethod
                          ,dt.[curCharge_Amount] as ChargeAmount
                          ,dt.[bolCrime] as IsChrime
                          ,dt.[bolConsent] as IsConsent
                          ,dt.[bolKids_FPCS] as IsKidsFPCS
                          ,dt.[bolApproved] as IsApproved
                          ,dt.[bolNonProfit] as IsNonProfit
                          ,dt.[bolCertASDTeacher] as IsCertASDTeacher
                          ,dt.[bolCurrentASDEmp] as IsCurrentASDEmp
                          ,dt.[bolEligibleHire] as IsEligibleHire
                          ,dt.[bolConflictIntWaiver] as IsConflictIntWaiver
                          ,dt.[bolConflictIntWaiverRecvd] as IsConflictIntWaiverRecvd
                          ,dt.[bolIncludeDir] as IsIncludeDir
                          ,dt.[szVendor_Comments] as Comments
                          ,dt.[bolGoods_Vendor] as IsGood
                          ,dt.[bolService_Vendor] as IsService
                          ,dt.[bolNonSectarian] as IsNonSectarian
                          ,dt.[bolOnline_Services] as IsOnlineService
                          ,dt.[bolIsActive] as IsActive
                          ,dt.[intLocation_ID] as LocationId
	                      ,vs.[szVendor_Status_CD] as Status
                      FROM [SQL2008R2_894208_fpcs].[dbo].[tblVendors] dt
                      INNER JOIN [SQL2008R2_894208_fpcs].[dbo].[tblVendor_Status] vs ON dt.intVendor_ID = vs.intVendor_ID
                      WHERE [bolIsActive] = 1 AND vs.[intSchool_Year] = 2015";

                    var sqlCmd = new SqlCommand(sql, cnn);

                    cnn.Open();
                    
                    using (SqlDataAdapter da = new SqlDataAdapter(sqlCmd))
                    {
                        da.Fill(table);


                    }
                }

                List<VendorModel> xlsxData = new List<VendorModel>();

                foreach (DataRow item in table.Rows)
                {
                    VendorModel importLine = new VendorModel();

                    importLine.VendorId = item["VendorId"].ToString();
                    importLine.IsGood = item["IsGood"].ToString();
                    importLine.IsService = item["IsService"].ToString();
                    importLine.VendorName = item["VendorName"].ToString();
                    importLine.FirstName = item["FirstName"].ToString();
                    importLine.LastName = item["LastName"].ToString();
                    importLine.Status = item["Status"].ToString();
                    importLine.Comments = item["Comments"].ToString();
                    importLine.Address = item["Address"].ToString();
                    importLine.City = item["City"].ToString();
                    importLine.State = item["State"].ToString();
                    importLine.Zip = item["Zip"].ToString();
                    importLine.MailAddress = item["MailAddress"].ToString();
                    importLine.MailCity = item["MailCity"].ToString();
                    importLine.MailState = item["MailState"].ToString();
                    importLine.MailZip = item["MailZip"].ToString();
                    importLine.Phone = item["Phone"].ToString();
                    importLine.Fax = item["Fax"].ToString();
                    importLine.Email = item["Email"].ToString();
                    importLine.LocationId = item["LocationId"].ToString();
                    importLine.Website = item["Website"].ToString();
                    importLine.SSN = item["SSN"].ToString();
                    importLine.BusinessLicense = item["BusinessLicense"].ToString();
                    importLine.LicenseExper = item["LicenseExper"].ToString();
                    importLine.InsureExper = item["InsureExper"].ToString();
                    importLine.PrevExper = item["PrevExper"].ToString();
                    importLine.ChargeType = item["ChargeType"].ToString();
                    importLine.OtherChargeMethod = item["OtherChargeMethod"].ToString();
                    importLine.ChargeAmount = item["ChargeAmount"].ToString();
                    importLine.ContractStart = item["ContractStart"].ToString();
                    importLine.IsNonProfit = item["IsNonProfit"].ToString();
                    importLine.IsForPay = item["IsForPay"].ToString();
                    importLine.IsChrime = item["IsChrime"].ToString();
                    importLine.IsConsent = item["IsConsent"].ToString();
                    importLine.IsKidsFPCS = item["IsKidsFPCS"].ToString();
                    importLine.IsApproved = item["IsApproved"].ToString();
                    importLine.IsCertASDTeacher = item["IsCertASDTeacher"].ToString();
                    importLine.IsCurrentASDEmp = item["IsCurrentASDEmp"].ToString();
                    importLine.IsEligibleHire = item["IsEligibleHire"].ToString();
                    importLine.IsConflictIntWaiver = item["IsConflictIntWaiver"].ToString();
                    importLine.IsConflictIntWaiverRecvd = item["IsConflictIntWaiverRecvd"].ToString();
                    importLine.IsIncludeDir = item["IsIncludeDir"].ToString();
                    importLine.IsNonSectarian = item["IsNonSectarian"].ToString();
                    importLine.IsOnlineService = item["IsOnlineService"].ToString();
                    importLine.IsActive = item["IsActive"].ToString();

                    xlsxData.Add(importLine);
                }

                using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
                {
                    var repo = uow.GetRepo<IVendorRepo>();
                    var repoState = uow.GetRepo<IStateRepo>();

                    foreach (var item in xlsxData)
                    {
                        Vendor data = new Vendor();

                        data.SchoolYearId = 1;
                        data.Type = item.IsGood == "1" && item.IsService == "1" ? VendorType.Both :
                            item.IsGood == "1" && item.IsService == "0" ? VendorType.Goods :
                            item.IsGood == "0" && item.IsService == "1" ? VendorType.Service :
                            VendorType.NonProfit;

                        data.BusinessName = item.VendorName != "NULL" ? item.VendorName : String.Empty;
                        data.FirstName = item.FirstName != "NULL" ? item.FirstName : String.Empty;
                        data.LastName = item.LastName != "NULL" ? item.LastName : String.Empty;
                        data.Status = item.Status == "APPR" ? VendorStatus.Approved :
                            item.Status == "PEND" ? VendorStatus.Pending :
                            item.Status == "REJC" ? VendorStatus.Rejected :
                            item.Status == "REMV" ? VendorStatus.Remove :
                            VendorStatus.Remove;
                        data.IsActive = true;
                        data.StatusComments = item.Comments != "NULL" ? item.Comments : String.Empty;
                        data.StreetAddress = item.Address != "NULL" ? item.Address : String.Empty;
                        data.StreetAddressCity = item.City != "NULL" ? item.City : String.Empty;
                        data.StreetAddressState = repoState.GetAll().FirstOrDefault(x => x.Code == item.State) != null ?
                            repoState.GetAll().FirstOrDefault(x => x.Code == item.State) :
                            repoState.GetAll().FirstOrDefault(x => x.Code == "AK");
                        data.StreetAddressZipCode = item.Zip != "NULL" ? item.Zip : String.Empty;
                        data.MailingAddress = item.MailAddress != "NULL" ? item.MailAddress : String.Empty;
                        data.MailingAddressCity = item.MailCity != "NULL" ? item.MailCity : String.Empty;
                        data.MailingAddressState = repoState.GetAll().FirstOrDefault(x => x.Code == item.MailState) != null ?
                            repoState.GetAll().FirstOrDefault(x => x.Code == item.MailState) :
                            repoState.GetAll().FirstOrDefault(x => x.Code == "AK");
                        data.MailingAddressZipCode = item.MailZip != "NULL" ? item.MailZip : String.Empty;
                        data.Phone = item.Phone != "NULL" ? item.Phone : String.Empty;
                        data.Fax = item.Fax != "NULL" ? item.Fax : String.Empty;
                        data.Email = item.Email != "NULL" ? item.Email : String.Empty;

                        if (item.LocationId == "1") { data.Location = Location.Eastside; }
                        else if (item.LocationId == "2") { data.Location = Location.Westside; }
                        else if (item.LocationId == "3") { data.Location = Location.Downtown; }
                        else if (item.LocationId == "4") { data.Location = Location.Midtown; }
                        else if (item.LocationId == "5") { data.Location = Location.Hillside; }
                        else if (item.LocationId == "6") { data.Location = Location.Southside; }
                        else if (item.LocationId == "7") { data.Location = Location.EagleRiver; }

                        data.BusinessWebsite = item.Website != "NULL" ? item.Website : String.Empty;
                        data.EmployerIdentification = item.SSN != "NULL" ? item.SSN : String.Empty;
                        data.AKBusinessLicense = item.BusinessLicense != "NULL" ? item.BusinessLicense : String.Empty;

                        DateTime tempDT = DateTime.MinValue;
                        DateTime.TryParse(item.LicenseExper, out tempDT);

                        data.LicenseExpiration = tempDT != DateTime.MinValue ? (DateTime?)tempDT : null;

                        tempDT = DateTime.MinValue;
                        DateTime.TryParse(item.InsureExper, out tempDT);

                        data.InsuranceExpiration = tempDT != DateTime.MinValue ? (DateTime?)tempDT : null;
                        data.TrainingEducationExperience = item.PrevExper != "NULL" ? item.PrevExper : String.Empty;
                        data.VendorUnitType = item.ChargeType == "1" ? VendorUnitType.Hour :
                            item.ChargeType == "2" ? VendorUnitType.Lesson :
                            item.ChargeType == "3" ? VendorUnitType.Month :
                            item.ChargeType == "4" ? VendorUnitType.Semester :
                            item.ChargeType == "5" ? VendorUnitType.Other :
                            item.ChargeType == "6" ? VendorUnitType.Course :
                            VendorUnitType.Hour;

                        data.OtherChargeMethod = item.OtherChargeMethod != "NULL" ? item.OtherChargeMethod : String.Empty;

                        Decimal temp = 0;
                        Decimal.TryParse(item.ChargeAmount, out temp);

                        data.Price = temp;

                        tempDT = DateTime.MinValue;
                        DateTime.TryParse(item.ContractStart, out tempDT);

                        data.ContractStartingDate = tempDT != DateTime.MinValue ? (DateTime?)tempDT : null;

                        Boolean isLog = false;
                        isLog = item.IsNonProfit.Trim() == "1" ? true : false;
                        data.IsNonProfit = isLog;

                        isLog = false;
                        isLog = item.IsChrime.Trim() == "1" ? true : false;
                        data.IsMisdemeanorOrFelony = isLog;

                        isLog = false;
                        isLog = item.IsConsent.Trim() == "1" ? true : false;
                        data.IsBackgroundCheckFingerprinting = isLog;

                        isLog = false;
                        isLog = item.IsKidsFPCS.Trim() == "1" ? true : false;
                        data.IsHaveChildrenCurrentlyEnrolled = isLog;

                        isLog = false;
                        isLog = item.IsCertASDTeacher.Trim() == "1" ? true : false;
                        data.IsRetiredCertificatedASDTeacher = isLog;

                        isLog = false;
                        isLog = item.IsCurrentASDEmp.Trim() == "1" ? true : false;
                        data.IsASDEmployeeEligibleHireList = isLog;

                        isLog = false;
                        isLog = item.IsActive.Trim() == "1" ? true : false;
                        data.IsCurrentlyAvailableProvideServices = isLog;

                        isLog = false;
                        isLog = item.IsOnlineService.Trim() == "1" ? true : false;
                        data.IsProvideOnlineCurriculumServices = isLog;

                        repo.Add(data);

                        uow.Commit();
                    }
                }

                answer.StateImport = StateImport.Successed;
                answer.Title = "Data has been successfully loaded";
            }
            catch (Exception ex)
            {
                answer.StateImport = StateImport.Failed;
                answer.Title = "Failed import data";
            }

            return answer;
        }

        public static Status ImportData3(Int32 schoolYearId)
        {
            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IVendorRepo>();
                var repoState = uow.GetRepo<IStateRepo>();

                Vendor data = new Vendor();

                data.SchoolYearId = 1;
                data.Type = VendorType.Both;

                data.BusinessName = "FPCS Service Vendor";
                data.FirstName = "FPCS Service Vendor";
                data.LastName = "FPCS Service Vendor";
                data.Status = VendorStatus.Approved;
                data.IsActive = true;
                data.StatusComments = String.Empty;
                data.StreetAddress = String.Empty;
                data.StreetAddressCity = String.Empty;
                data.StreetAddressState = repoState.GetAll().FirstOrDefault(x => x.Code == "AK");
                data.StreetAddressZipCode = String.Empty;
                data.MailingAddress = String.Empty;
                data.MailingAddressCity = String.Empty;
                data.MailingAddressState = repoState.GetAll().FirstOrDefault(x => x.Code == "AK");
                data.MailingAddressZipCode = String.Empty;
                data.Phone = String.Empty;
                data.Fax = String.Empty;
                data.Email = String.Empty;

                data.Location = Location.Eastside;
                data.BusinessWebsite = String.Empty;
                data.EmployerIdentification = String.Empty;
                data.AKBusinessLicense = String.Empty;

                DateTime tempDT = DateTime.MinValue;

                data.LicenseExpiration = tempDT != DateTime.MinValue ? (DateTime?)tempDT : null;

                tempDT = DateTime.MinValue;

                data.InsuranceExpiration = tempDT != DateTime.MinValue ? (DateTime?)tempDT : null;
                data.TrainingEducationExperience = String.Empty;
                data.VendorUnitType = VendorUnitType.Hour;

                data.OtherChargeMethod = String.Empty;

                Decimal temp = 0;

                data.Price = temp;

                tempDT = DateTime.MinValue;

                data.ContractStartingDate = tempDT != DateTime.MinValue ? (DateTime?)tempDT : null;

                Boolean isLog = false;
                data.IsNonProfit = isLog;

                isLog = false;
                data.IsMisdemeanorOrFelony = isLog;

                isLog = false;
                data.IsBackgroundCheckFingerprinting = isLog;

                isLog = false;
                data.IsHaveChildrenCurrentlyEnrolled = isLog;

                isLog = false;
                data.IsRetiredCertificatedASDTeacher = isLog;

                isLog = false;
                data.IsASDEmployeeEligibleHireList = isLog;

                isLog = false;
                data.IsCurrentlyAvailableProvideServices = isLog;

                isLog = false;
                data.IsProvideOnlineCurriculumServices = isLog;

                repo.Add(data);

                uow.Commit();

                Status answer = new Status();

                answer.StateImport = StateImport.Successed;
                answer.Title = "Data has been successfully loaded";

                return answer;
            }
        }
    }
}
