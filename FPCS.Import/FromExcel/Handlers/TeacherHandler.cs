﻿using ClosedXML.Excel;
using FPCS.Core.Unity;
using FPCS.Data;
using FPCS.Data.Entities;
using FPCS.Data.Enums;
using FPCS.Data.Repo;
using FPCS.Import.FromExcel.Models.Teacher;
using FPCS.Import.State;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Import.FromExcel.Handlers
{
    public static class TeacherHandler
    {
        public static void ImportData(Stream stream, Int32 schoolYearId)
        {
            Status answer = new Status();

            var wb = new XLWorkbook(stream);

            IXLWorksheet ws = wb.Worksheet(1);

            List<TeacherModel> xlsxData = new List<TeacherModel>();

            foreach (IXLRow item in ws.Rows(2, 27))
            //foreach (IXLRow item in ws.Rows(2, 60))
            {
                TeacherModel importLine = new TeacherModel();

                importLine.InstructorID = item.Cell(1).Value.ToString().Trim() != "NULL" ? item.Cell(1).Value.ToString().Trim() : String.Empty;
                importLine.ContractASD = item.Cell(2).Value.ToString().Trim() != "NULL" ? item.Cell(2).Value.ToString().Trim() : String.Empty; ;
                importLine.Title = item.Cell(3).Value.ToString().Trim() != "NULL" ? item.Cell(3).Value.ToString().Trim() : String.Empty; ;
                importLine.FirstName = item.Cell(4).Value.ToString().Trim() != "NULL" ? item.Cell(4).Value.ToString().Trim() : String.Empty; ;
                importLine.LastName = item.Cell(5).Value.ToString().Trim() != "NULL" ? item.Cell(5).Value.ToString().Trim() : String.Empty; ;
                importLine.MidInitial = item.Cell(6).Value.ToString().Trim() != "NULL" ? item.Cell(6).Value.ToString().Trim() : String.Empty; ;
                importLine.SSN = item.Cell(7).Value.ToString().Trim() != "NULL" ? item.Cell(7).Value.ToString().Trim() : String.Empty; ;
                importLine.MailingAddr = item.Cell(8).Value.ToString().Trim() != "NULL" ? item.Cell(8).Value.ToString().Trim() : String.Empty; ;
                importLine.City = item.Cell(9).Value.ToString().Trim() != "NULL" ? item.Cell(9).Value.ToString().Trim() : String.Empty; ;
                importLine.State = item.Cell(10).Value.ToString().Trim() != "NULL" ? item.Cell(10).Value.ToString().Trim() : String.Empty; ;
                importLine.ZipCode = item.Cell(11).Value.ToString().Trim() != "NULL" ? item.Cell(11).Value.ToString().Trim() : String.Empty; ;
                importLine.HomePhone = item.Cell(12).Value.ToString().Trim() != "NULL" ? item.Cell(12).Value.ToString().Trim() : String.Empty; ;
                importLine.BusinessPhone = item.Cell(13).Value.ToString().Trim() != "NULL" ? item.Cell(13).Value.ToString().Trim() : String.Empty; ;
                importLine.BusinessExt = item.Cell(14).Value.ToString().Trim() != "NULL" ? item.Cell(14).Value.ToString().Trim() : String.Empty; ;
                importLine.CellPhone = item.Cell(15).Value.ToString().Trim() != "NULL" ? item.Cell(15).Value.ToString().Trim() : String.Empty; ;
                importLine.Email = item.Cell(16).Value.ToString().Trim() != "NULL" ? item.Cell(16).Value.ToString().Trim() : String.Empty; ;
                importLine.Email2 = item.Cell(17).Value.ToString().Trim() != "NULL" ? item.Cell(17).Value.ToString().Trim() : String.Empty; ;
                importLine.DateBirth = item.Cell(18).Value.ToString().Trim() != "NULL" ? item.Cell(18).Value.ToString().Trim() : String.Empty; ;
                importLine.MastersDegree = item.Cell(19).Value.ToString().Trim() != "NULL" ? item.Cell(19).Value.ToString().Trim() : String.Empty; ;
                importLine.DistCode = item.Cell(20).Value.ToString().Trim() != "NULL" ? item.Cell(20).Value.ToString().Trim() : String.Empty; ;
                importLine.Location = item.Cell(21).Value.ToString().Trim() != "NULL" ? item.Cell(21).Value.ToString().Trim() : String.Empty; ;
                importLine.ASDLeave = item.Cell(22).Value.ToString().Trim() != "NULL" ? item.Cell(22).Value.ToString().Trim() : String.Empty; ;
                importLine.Substitute = item.Cell(23).Value.ToString().Trim() != "NULL" ? item.Cell(23).Value.ToString().Trim() : String.Empty; ;
                importLine.ASDEmployee = item.Cell(24).Value.ToString().Trim() != "NULL" ? item.Cell(24).Value.ToString().Trim() : String.Empty; ;
                importLine.ASDEligibleForHire = item.Cell(25).Value.ToString().Trim() != "NULL" ? item.Cell(25).Value.ToString().Trim() : String.Empty; ;
                importLine.ASDRetired = item.Cell(26).Value.ToString().Trim() != "NULL" ? item.Cell(26).Value.ToString().Trim() : String.Empty; ;
                importLine.GroupInstruction = item.Cell(27).Value.ToString().Trim() != "NULL" ? item.Cell(27).Value.ToString().Trim() : String.Empty; ;
                importLine.IndividualInstruction = item.Cell(28).Value.ToString().Trim() != "NULL" ? item.Cell(28).Value.ToString().Trim() : String.Empty; ;
                importLine.YearsExperience = item.Cell(29).Value.ToString().Trim() != "NULL" ? item.Cell(29).Value.ToString().Trim() : String.Empty; ;
                importLine.K8 = item.Cell(30).Value.ToString().Trim() != "NULL" ? item.Cell(30).Value.ToString().Trim() : String.Empty; ;
                importLine.K12 = item.Cell(31).Value.ToString().Trim() != "NULL" ? item.Cell(31).Value.ToString().Trim() : String.Empty; ;
                importLine.SpecialEd = item.Cell(32).Value.ToString().Trim() != "NULL" ? item.Cell(32).Value.ToString().Trim() : String.Empty; ;
                importLine.Secondary = item.Cell(33).Value.ToString().Trim() != "NULL" ? item.Cell(33).Value.ToString().Trim() : String.Empty; ;
                importLine.SecondaryList = item.Cell(34).Value.ToString().Trim() != "NULL" ? item.Cell(34).Value.ToString().Trim() : String.Empty; ;
                importLine.MyClassroom = item.Cell(35).Value.ToString().Trim() != "NULL" ? item.Cell(35).Value.ToString().Trim() : String.Empty; ;
                importLine.MyHome = item.Cell(36).Value.ToString().Trim() != "NULL" ? item.Cell(36).Value.ToString().Trim() : String.Empty; ;
                importLine.StudentsHome = item.Cell(37).Value.ToString().Trim() != "NULL" ? item.Cell(37).Value.ToString().Trim() : String.Empty; ;
                importLine.FPCSClassroom = item.Cell(38).Value.ToString().Trim() != "NULL" ? item.Cell(38).Value.ToString().Trim() : String.Empty; ;
                importLine.Other = item.Cell(39).Value.ToString().Trim() != "NULL" ? item.Cell(39).Value.ToString().Trim() : String.Empty; ;
                importLine.OtherDesc = item.Cell(40).Value.ToString().Trim() != "NULL" ? item.Cell(40).Value.ToString().Trim() : String.Empty; ;
                importLine.AvailWeekdays = item.Cell(41).Value.ToString().Trim() != "NULL" ? item.Cell(41).Value.ToString().Trim() : String.Empty; ;
                importLine.AvailAfternoon = item.Cell(42).Value.ToString().Trim() != "NULL" ? item.Cell(42).Value.ToString().Trim() : String.Empty; ;
                importLine.AvailEvening = item.Cell(43).Value.ToString().Trim() != "NULL" ? item.Cell(43).Value.ToString().Trim() : String.Empty; ;
                importLine.AvailEnds = item.Cell(44).Value.ToString().Trim() != "NULL" ? item.Cell(44).Value.ToString().Trim() : String.Empty; ;
                importLine.AvailSummers = item.Cell(45).Value.ToString().Trim() != "NULL" ? item.Cell(45).Value.ToString().Trim() : String.Empty; ;
                importLine.ASDSchool = item.Cell(46).Value.ToString().Trim() != "NULL" ? item.Cell(46).Value.ToString().Trim() : String.Empty; ;
                importLine.dtCertExper = item.Cell(47).Value.ToString().Trim() != "NULL" ? item.Cell(47).Value.ToString().Trim() : String.Empty; ;
                importLine.SalaryPlacement = item.Cell(48).Value.ToString().Trim() != "NULL" ? item.Cell(48).Value.ToString().Trim() : String.Empty; ;
                importLine.CurPayRate = item.Cell(49).Value.ToString().Trim() != "NULL" ? item.Cell(49).Value.ToString().Trim() : String.Empty; ;
                importLine.PayTypeId = item.Cell(50).Value.ToString().Trim() != "NULL" ? item.Cell(50).Value.ToString().Trim() : String.Empty; ;
                importLine.ASDFullTime = item.Cell(51).Value.ToString().Trim() != "NULL" ? item.Cell(51).Value.ToString().Trim() : String.Empty; ;
                importLine.ASDFullTimePercent = item.Cell(52).Value.ToString().Trim() != "NULL" ? item.Cell(52).Value.ToString().Trim() : String.Empty; ;
                importLine.ASDPartTime = item.Cell(53).Value.ToString().Trim() != "NULL" ? item.Cell(53).Value.ToString().Trim() : String.Empty; ;
                importLine.ASDPartTimePercent = item.Cell(54).Value.ToString().Trim() != "NULL" ? item.Cell(54).Value.ToString().Trim() : String.Empty; ;
                importLine.FPCSHoursGoal = item.Cell(55).Value.ToString().Trim() != "NULL" ? item.Cell(55).Value.ToString().Trim() : String.Empty; ;
                importLine.IsActive = item.Cell(56).Value.ToString().Trim() != "NULL" ? item.Cell(56).Value.ToString().Trim() : String.Empty; ;

                xlsxData.Add(importLine);
            }

            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<ITeacherRepo>();
                var repoState = uow.GetRepo<IStateRepo>();

                List<Teacher> oldTeachers = repo.GetAll(schoolYearId).ToList();

                foreach (var item in xlsxData)
                {
                    Teacher teacher = oldTeachers.FirstOrDefault(x => x.LastName == item.LastName &&
                                                                      x.FirstName == item.FirstName);

                    if (teacher != null) { continue; }
                    else { teacher = new Teacher(); }

                    teacher.Title = item.Title == "Mr." ? TitlePerson.Mr :
                        item.Title == "Mrs." ? TitlePerson.Mrs :
                        item.Title == "Dr." ? TitlePerson.Dr :
                        TitlePerson.Ms;
                    teacher.FirstName = item.FirstName;
                    teacher.LastName = item.LastName;
                    teacher.FullName = item.LastName + " " + item.FirstName;
                    teacher.MiddleInitial = item.MidInitial;
                    teacher.SSN = item.SSN;
                    teacher.MailingAddress = item.MailingAddr;
                    teacher.City = item.City;
                    teacher.State = repoState.GetAll().FirstOrDefault(x => x.Name == item.State);
                    teacher.ZipCode = item.ZipCode;
                    teacher.HomePhone = item.HomePhone;
                    teacher.BusPhone = item.BusinessPhone;
                    teacher.Ext = Convert.ToInt32(item.BusinessExt);
                    teacher.CellPhone = item.CellPhone;
                    teacher.Email = item.Email;
                    teacher.SecondEMailAddress = item.Email2;
                    teacher.DateBirth = Convert.ToDateTime(item.DateBirth);
                    teacher.MastersDegree = item.MastersDegree == "1" ? true : false;
                    teacher.DistrictCode = item.DistCode;
                    teacher.IsLeaveASD = item.ASDLeave == "1" ? true : false;
                    teacher.IsSubtitleTeacher = item.Substitute == "1" ? true : false;
                    teacher.IsOtherASDEmployee = item.ASDEmployee == "1" ? true : false;
                    teacher.IsOnASDEligibleToHire = item.ASDEligibleForHire == "1" ? true : false;
                    teacher.IsRetiredASDTeacher = item.ASDRetired == "1" ? true : false;
                    teacher.IsGroupInstruction = item.GroupInstruction == "1" ? true : false;
                    teacher.IsIndividualInstruction = item.IndividualInstruction == "1" ? true : false;
                    teacher.YearsTeachingExperience = Convert.ToInt32(Convert.ToDecimal(item.YearsExperience));
                    teacher.IsAlaskaCertificationK8 = item.K8 == "1" ? true : false;
                    teacher.IsAlaskaCertificationK12 = item.K12 == "1" ? true : false;
                    teacher.IsAlaskaCertificationSpecialEducation = item.SpecialEd == "1" ? true : false;
                    teacher.IsAlaskaCertificationSecondary = item.Secondary == "1" ? true : false;
                    teacher.AlaskaCertificationSubjectGrades = item.SecondaryList;
                    teacher.IsInMyClassroom = item.MyClassroom == "1" ? true : false;
                    teacher.IsAtMyHome = item.MyHome == "1" ? true : false;
                    teacher.IsAsStudentsHome = item.StudentsHome == "1" ? true : false;
                    teacher.IsAtFPCSClassroom = item.FPCSClassroom == "1" ? true : false;
                    teacher.IsOtherAvailableTeach = item.Other == "1" ? true : false;
                    teacher.OtherAvailableTeach = item.OtherDesc;
                    teacher.IsWeekdays = item.AvailWeekdays == "1" ? true : false;
                    teacher.IsWeekdayAfternoons = item.AvailAfternoon == "1" ? true : false;
                    teacher.IsWeekdayEvenings = item.AvailEvening == "1" ? true : false;
                    teacher.IsWeekends = item.AvailEnds == "1" ? true : false;
                    teacher.IsSummers = item.AvailSummers == "1" ? true : false;
                    teacher.NameSchool = item.ASDSchool;
                    teacher.TeachingCertificateExpirationDate = !String.IsNullOrEmpty(item.dtCertExper) ? Convert.ToDateTime(item.dtCertExper) : DateTime.Now;
                    teacher.TeachingSalaryPlacement = item.SalaryPlacement;

                    teacher.PerDeimRate = Convert.ToDecimal(item.CurPayRate);
                    teacher.PayType = item.PayTypeId == "1" ? PayType.ADD :
                        item.PayTypeId == "2" ? PayType.SAA :
                        item.PayTypeId == "3" ? PayType.Contract :
                        item.PayTypeId == "4" ? PayType.ADDContract :
                        item.PayTypeId == "5" ? PayType.Inactive : PayType.ADD;
                    teacher.IsBenefitPaidASDSchool = item.ASDFullTime == "1" ? true : false;
                    teacher.ASDFTE = Convert.ToInt32(item.ASDFullTimePercent);
                    teacher.IsNotIncludeBenefits = item.ASDPartTime == "1" ? true : false;
                    teacher.FTCSFTE = Convert.ToInt32(item.ASDPartTimePercent);
                    teacher.FPCSFTE = Convert.ToInt32(item.FPCSHoursGoal);
                    teacher.IsActive = item.IsActive == "1" ? true : false;

                    teacher.SchoolYearId = schoolYearId;
                    teacher.IsGuardian = false;

                    teacher = repo.Add(teacher);

                    uow.Commit();

                    teacher.UnqDbUserId = teacher.DbUserId;

                    uow.Commit();
                }
            }

            var t = 0;
        }
    }
}
