﻿using ClosedXML.Excel;
using FPCS.Core.Unity;
using FPCS.Data;
using FPCS.Data.Entities;
using FPCS.Data.Repo;
using FPCS.Import.Enum;
using FPCS.Import.FromExcel.Models.VendorService;
using FPCS.Import.State;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Import.FromExcel.Handlers
{
    public static class VendorServiceHandler
    {
        public static Status ImportData(Stream stream, Int32 schoolYearId)
        {
            Status answer = new Status();

            try
            {
                var wb = new XLWorkbook(stream);

                IXLWorksheet ws = wb.Worksheet(1);

                List<VendorServiceModel> xlsxData = new List<VendorServiceModel>();

                foreach (IXLRow item in ws.Rows(2, ws.Column(1).Cells().Count()))
                {
                    VendorServiceModel importLine = new VendorServiceModel();

                    importLine.VendorServiceId = item.Cell(1).Value.ToString();
                    importLine.VendorServiceName = item.Cell(2).Value.ToString();
                    importLine.SubjectName = item.Cell(3).Value.ToString();
                    importLine.IsActive = item.Cell(4).Value.ToString();
                    
                    xlsxData.Add(importLine);
                }

                using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
                {
                    var repo = uow.GetRepo<IServiceVendorRepo>();
                    var repoSubject = uow.GetRepo<ISubjectRepo>();

                    foreach (var item in xlsxData)
                    {
                        Subject subject = repoSubject.GetAll().FirstOrDefault(x => x.Name.Trim().ToLower() == item.SubjectName.Trim().ToLower());

                        if (subject != null)
                        {
                            ServiceVendor data = new ServiceVendor();
                            data.Name = item.VendorServiceName;
                            data.SubjectId = subject.SubjectId;

                            Boolean isLog = false;
                            isLog = item.IsActive.Trim() == "1" ? true : false;
                            data.IsActive = isLog;

                            repo.Add(data);

                            uow.Commit();
                        }
                    }
                }

                answer.StateImport = StateImport.Successed;
                answer.Title = "Data has been successfully loaded";
            }
            catch (Exception ex)
            {
                answer.StateImport = StateImport.Failed;
                answer.Title = "Failed import data";
            }

            return answer;
        }
    }
}
