﻿using ClosedXML.Excel;
using FPCS.Core.Unity;
using FPCS.Data;
using FPCS.Data.Entities;
using FPCS.Data.Repo;
using FPCS.Import.Enum;
using FPCS.Import.FromExcel.Models.ASDCourse;
using FPCS.Import.State;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using FPCS.Data.Enums;

namespace FPCS.Import.FromExcel.Handlers
{
	public static class ASDCourseHandler
	{
		public static Status ImportData(Stream stream, Int32 schoolYearId, Boolean withDelete = false)
		{
			Status answer = new Status();

			try
			{
				var wb = new XLWorkbook(stream);

				IXLWorksheet ws = wb.Worksheet(2);

				List<ASDCourseModel> xlsxData = new List<ASDCourseModel>();

				foreach (IXLRow item in ws.Rows(1, 83))
				{
					if (item.Cell(1).Value.ToString() != "")
					{
						ASDCourseModel importLine = new ASDCourseModel();

						importLine.CourseCode = item.Cell(1).Value.ToString();
						importLine.CourseDescription = item.Cell(2).Value.ToString();
						importLine.Subject = item.Cell(4).Value.ToString();

						Decimal temp = 0;
						Decimal.TryParse(item.Cell(5).Value.ToString(), out temp);

						importLine.CardCredit = temp;

						xlsxData.Add(importLine);
					}
				}

				using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
				{
					var repo = uow.GetRepo<IASDCourseRepo>();
					var repoSubject = uow.GetRepo<ISubjectRepo>();

					foreach (var item in xlsxData)
					{
						Subject subject = repoSubject.GetByName(item.Subject);

						if (subject == null)
						{
							subject = new Subject();
							subject.Name = item.Subject;
							subject.IsDeleted = false;
							subject.Code = 0;
							subject.Description = null;

							subject = repoSubject.Add(subject);
							uow.Commit();
						}

						ASDCourse asdCourse = repo.GetByExternalASDCourseId(item.CourseCode, schoolYearId);

						if (asdCourse == null)
						{
							asdCourse = new ASDCourse();

							asdCourse.SchoolYearId = schoolYearId;
							asdCourse.ExternalASDCourseId = item.CourseCode;
							asdCourse.Description = item.CourseDescription;
							asdCourse.GradCredit = item.CardCredit;
							asdCourse.Subject = subject;
							asdCourse.Name = "Middle";
							asdCourse.TypeGrade = TypeGrade.High;
							asdCourse.IsActivated = true;
							asdCourse.IsDeleted = false;
							asdCourse.CreatedDate = DateTime.Now;
							asdCourse.UpdatedDate = DateTime.Now;

							asdCourse = repo.Add(asdCourse);
							uow.Commit();
						}
					}
				}

				answer.StateImport = StateImport.Successed;
				answer.Title = "Data has been successfully loaded";
			}
			catch (Exception ex)
			{
				answer.StateImport = StateImport.Failed;
				answer.Title = "Failed import data";
			}

			return answer;
		}
	}
}
