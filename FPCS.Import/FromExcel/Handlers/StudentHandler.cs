﻿using ClosedXML.Excel;
using FPCS.Core.Unity;
using FPCS.Data;
using FPCS.Data.Entities;
using FPCS.Data.Enums;
using FPCS.Data.Repo;
using FPCS.Import.FromExcel.Models.Student;
using FPCS.Import.State;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Import.FromExcel.Handlers
{
    public static class StudentHandler
    {
        public static void ImportData(Stream stream, Int32 schoolYearId)
        {
            Status answer = new Status();

            var wb = new XLWorkbook(stream);

            IXLWorksheet ws = wb.Worksheet(1);

            List<StudentModel> xlsxData = new List<StudentModel>();

            StudentModel prevImportLine = new StudentModel();

            foreach (IXLRow item in ws.Rows(2, 1051))
            {
                StudentModel importLine = new StudentModel();
                importLine.StudentName = 
                    item.Cell(1).Value.ToString() != String.Empty ?
                        item.Cell(1).Value.ToString().Trim() != "(blank)" ?
                            item.Cell(1).Value.ToString() :
                            String.Empty :
                        prevImportLine.StudentName;

                importLine.Grade = 
                    item.Cell(2).Value.ToString() != String.Empty ?
                        item.Cell(2).Value.ToString().Trim() != "(blank)" ?
                            item.Cell(2).Value.ToString() :
                            String.Empty :
                        prevImportLine.Grade;

                importLine.Zangle = 
                    item.Cell(3).Value.ToString() != String.Empty ?
                        item.Cell(3).Value.ToString().Trim() != "(blank)" ?
                            item.Cell(3).Value.ToString() :
                            String.Empty :
                        prevImportLine.Zangle;

                importLine.SponsorTeacher = 
                    item.Cell(4).Value.ToString() != String.Empty ?
                        item.Cell(4).Value.ToString().Trim() != "(blank)" ?
                            item.Cell(4).Value.ToString() :
                            String.Empty :
                        prevImportLine.SponsorTeacher;

                importLine.HomeAddress = 
                    item.Cell(5).Value.ToString() != String.Empty ?
                        item.Cell(5).Value.ToString().Trim() != "(blank)" ?
                            item.Cell(5).Value.ToString() :
                            String.Empty :
                        prevImportLine.HomeAddress;

                importLine.City = 
                    item.Cell(6).Value.ToString() != String.Empty ?
                        item.Cell(6).Value.ToString().Trim() != "(blank)" ?
                            item.Cell(6).Value.ToString() :
                            String.Empty :
                        prevImportLine.City;

                importLine.State = 
                    item.Cell(7).Value.ToString() != String.Empty ?
                        item.Cell(7).Value.ToString().Trim() != "(blank)" ?
                            item.Cell(7).Value.ToString() :
                            String.Empty :
                        prevImportLine.State;

                importLine.Zip = 
                    item.Cell(8).Value.ToString() != String.Empty ?
                        item.Cell(8).Value.ToString().Trim() != "(blank)" ?
                            item.Cell(8).Value.ToString() :
                            String.Empty :
                        prevImportLine.Zip;

                importLine.MailingAddress = 
                    item.Cell(9).Value.ToString() != String.Empty ?
                        item.Cell(9).Value.ToString().Trim() != "(blank)" ?
                            item.Cell(9).Value.ToString() :
                            String.Empty :
                        prevImportLine.MailingAddress;

                importLine.City2 = 
                    item.Cell(10).Value.ToString() != String.Empty ?
                        item.Cell(10).Value.ToString().Trim() != "(blank)" ?
                            item.Cell(10).Value.ToString() :
                            String.Empty :
                        prevImportLine.City2;

                importLine.State2 = 
                    item.Cell(11).Value.ToString() != String.Empty ?
                        item.Cell(11).Value.ToString().Trim() != "(blank)" ?
                            item.Cell(11).Value.ToString() :
                            String.Empty :
                        prevImportLine.State2;

                importLine.Zip2 = 
                    item.Cell(12).Value.ToString() != String.Empty ?
                        item.Cell(12).Value.ToString().Trim() != "(blank)" ?
                            item.Cell(12).Value.ToString() :
                            String.Empty :
                        prevImportLine.Zip2;

                importLine.Contact = 
                    item.Cell(13).Value.ToString() != String.Empty ?
                        item.Cell(13).Value.ToString().Trim() != "(blank)" ?
                            item.Cell(13).Value.ToString() :
                            String.Empty :
                        prevImportLine.Contact;

                importLine.GuardianFirstName = 
                    item.Cell(14).Value.ToString() != String.Empty ?
                        item.Cell(14).Value.ToString().Trim() != "(blank)" ?
                            item.Cell(14).Value.ToString() :
                            String.Empty :
                        prevImportLine.GuardianFirstName;

                importLine.GuardianLastName = 
                    item.Cell(15).Value.ToString() != String.Empty ?
                        item.Cell(15).Value.ToString().Trim() != "(blank)" ?
                            item.Cell(15).Value.ToString() :
                            String.Empty :
                        prevImportLine.GuardianLastName;

                importLine.GuardianPhone = 
                    item.Cell(16).Value.ToString() != String.Empty ?
                        item.Cell(16).Value.ToString().Trim() != "(blank)" ?
                            item.Cell(16).Value.ToString() :
                            String.Empty :
                        prevImportLine.GuardianPhone;

                importLine.GuardianTypePhone = 
                    item.Cell(17).Value.ToString() != String.Empty ?
                        item.Cell(17).Value.ToString().Trim() != "(blank)" ?
                            item.Cell(17).Value.ToString() :
                            String.Empty :
                        prevImportLine.GuardianTypePhone;

                xlsxData.Add(importLine);

                prevImportLine = importLine;
            }

            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                var repoGuardian = uow.GetRepo<IGuardianRepo>();
                var repoFamily = uow.GetRepo<IFamilyRepo>();
                var repoFamilyRelationship = uow.GetRepo<IFamilyRelationshipRepo>();
                var repoStudent = uow.GetRepo<IStudentRepo>();
                var repoStudentPacket = uow.GetRepo<IStudentPacketRepo>();
                var repoTeacher = uow.GetRepo<ITeacherRepo>();

                Int32 i = 0;
                foreach (var item in xlsxData)
                {
                    i++;

                    // Guardian
                    Guardian guardian = repoGuardian.GetAll().FirstOrDefault(x => x.LastName == item.GuardianLastName && x.FirstName == item.GuardianFirstName);
                    if (guardian == null)
                    {
                        guardian = repoGuardian.Add(item.GuardianFirstName, item.GuardianLastName, "", "", null, "", "", item.GuardianPhone, "", "", "", "", "", "", "", null, schoolYearId);
                        uow.Commit();
                    }

                    // Student
                    Student student = repoStudent.GetAll().FirstOrDefault(x => x.LastName == item.StudentLastName && x.FirstName == item.StudentFirstName);
                    StudentPacket studentPacket = null;
                    if (student == null)
                    {
                        student = repoStudent.Add(item.StudentFirstName, item.StudentLastName, "", "", Convert.ToInt32(item.Grade), DateTimeOffset.Now, null, null, "", 0, false, false, "", Sex.Male, (Grade)Convert.ToInt32(item.Grade), null, item.Zangle, null, null, "", null, "", false, false, false, false, false, false, false, false, schoolYearId);
                        uow.Commit();

                        studentPacket = new StudentPacket();
                        studentPacket = repoStudentPacket.AddIsNotExist(student.DbUserId, schoolYearId);
                        uow.Commit();
                    }
                    else
                    {
                        studentPacket = repoStudentPacket.Get(student.DbUserId, schoolYearId);
                    }

                    // Sponsor teacher
                    Teacher teacher = repoTeacher.GetAll().FirstOrDefault(x => x.LastName == item.SponsorTeacherLastName && x.FirstName == item.SponsorTeacherFirstName);
                    if (teacher == null && item.SponsorTeacher != "Unassigned")
                    {
                        teacher = new Teacher();
                        teacher.SchoolYearId = schoolYearId;
                        teacher.Email = "";
                        teacher.FirstName = item.SponsorTeacherFirstName;
                        teacher.LastName = item.SponsorTeacherLastName;
                        teacher.MiddleInitial = "";
                        teacher.FullName = String.Format(@"{0} {1}", item.SponsorTeacherLastName, item.SponsorTeacherFirstName);
                        teacher.Title = TitlePerson.Mr;
                        teacher.DateBirth = DateTime.Now;
                        teacher.IsActive = true;
                        teacher.MailingAddress = item.HomeAddress;
                        teacher.City = item.City;
                        teacher.StateId = 1;
                        teacher.ZipCode = item.Zip;
                        teacher.HomePhone = "";
                        teacher.BusPhone = "";
                        teacher.Ext = 0;
                        teacher.CellPhone = "";
                        teacher.MastersDegree = false;
                        teacher.SSN = "";
                        teacher.SecondEMailAddress = "";
                        teacher.DistrictCode = "";
                        teacher.IsBenefitPaidASDSchool = false;
                        teacher.ASDFTE = 0;
                        teacher.IsNotIncludeBenefits = false;
                        teacher.FTCSFTE = 0;
                        teacher.NameSchool = "";
                        teacher.IsLeaveASD = false;
                        teacher.IsSubtitleTeacher = false;
                        teacher.IsOtherASDEmployee = false;
                        teacher.IsOnASDEligibleToHire = false;
                        teacher.IsRetiredASDTeacher = false;
                        teacher.FPCSFTE = 0;
                        teacher.IsGroupInstruction = false;
                        teacher.IsIndividualInstruction = false;
                        teacher.YearsTeachingExperience = 0;
                        teacher.TeachingCertificateExpirationDate = DateTime.Now;
                        teacher.TeachingSalaryPlacement = "";
                        teacher.IsAlaskaCertificationK8 = false;
                        teacher.IsAlaskaCertificationK12 = false;
                        teacher.IsAlaskaCertificationSpecialEducation = false;
                        teacher.IsAlaskaCertificationSecondary = false;
                        teacher.AlaskaCertificationSubjectGrades = "";
                        teacher.IsInMyClassroom = false;
                        teacher.IsAtMyHome = false;
                        teacher.IsAsStudentsHome = false;
                        teacher.IsAtFPCSClassroom = false;
                        teacher.IsOtherAvailableTeach = false;
                        teacher.OtherAvailableTeach = "";
                        teacher.IsWeekdays = false;
                        teacher.IsWeekdayAfternoons = false;
                        teacher.IsWeekdayEvenings = false;
                        teacher.IsWeekends = false;
                        teacher.IsSummers = false;
                        teacher.PayType = PayType.Inactive;
                        teacher.PerDeimRate = 0;
                        teacher.IsGuardian = false;

                        teacher = repoTeacher.Add(teacher);
                        uow.Commit();
                    }

                    if (teacher != null)
                    {
                        studentPacket.SponsorTeacherId = teacher.DbUserId;
                        repoStudentPacket.Update(studentPacket);
                        uow.Commit();
                    }

                    // Family
                    Family family = repoFamily.GetFamiliesByGuardian(guardian.DbUserId).FirstOrDefault();
                    if (family == null)
                        family = repoFamily.GetFamiliesByStudent(student.DbUserId).FirstOrDefault();

                    if (family == null)
                    {
                        family = repoFamily.Add("", "", "", item.MailingAddress, item.GuardianPhone, "", item.City2, "", item.Zip2, "", "", false, 1, schoolYearId);
                        uow.Commit();
                    }

                    var guardianIds = family.Guardians.Select(x => x.DbUserId).ToList();
                    if (!guardianIds.Contains(guardian.DbUserId))
                    {
                        guardianIds.Add(guardian.DbUserId);
                        repoFamily.UpdateGuardians(family, guardianIds);
                        uow.Commit();
                    }

                    var studentIds = family.Students.Select(x => x.DbUserId).ToList();
                    if (!studentIds.Contains(student.DbUserId))
                    {
                        studentIds.Add(student.DbUserId);
                        repoFamily.UpdateStudents(family, studentIds);
                        uow.Commit();
                    }

                    // Family relationship
                    //FamilyRelationship relationship = repoFamilyRelationship.Get(guardian.DbUserId, student.DbUserId);
                    //if (relationship == null)
                    //{
                    //    relationship = repoFamilyRelationship.Add(guardian, student, FamilyRelationshipType.NotSpecified);
                    //    uow.Commit();
                    //}
                }
            }

            var t = 0;
        }
    }
}
