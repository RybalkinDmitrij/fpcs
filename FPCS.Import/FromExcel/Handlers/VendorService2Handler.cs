﻿using ClosedXML.Excel;
using FPCS.Core.Unity;
using FPCS.Data;
using FPCS.Data.Entities;
using FPCS.Data.Repo;
using FPCS.Import.Enum;
using FPCS.Import.FromExcel.Models.VendorService;
using FPCS.Import.State;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Import.FromExcel.Handlers
{
    public static class VendorService2Handler
    {
        public static Status ImportData(Stream stream, Int32 schoolYearId)
        {
            Status answer = new Status();

            try
            {
                var wb = new XLWorkbook(stream);

                IXLWorksheet ws = wb.Worksheet(1);

                List<VendorService2Model> xlsxData = new List<VendorService2Model>();

                foreach (IXLRow item in ws.Rows(2, ws.Column(1).Cells().Count()))
                {
                    VendorService2Model importLine = new VendorService2Model();

                    importLine.VendorName = item.Cell(1).Value.ToString();
                    importLine.VendorServiceName = item.Cell(2).Value.ToString();
                    importLine.SubjectName = item.Cell(3).Value.ToString();
                    
                    xlsxData.Add(importLine);
                }

                using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
                {
                    var repoServiceVendor = uow.GetRepo<IServiceVendorRepo>();
                    var repoSubject = uow.GetRepo<ISubjectRepo>();
                    var repoVendor = uow.GetRepo<IVendorRepo>();

                    Int32 oldVendor = 0;
                    List<Int32> serviceVendors = new List<Int32>();

                    foreach (var item in xlsxData)
                    {
                        Vendor vendor = repoVendor.GetAll().FirstOrDefault(x => x.BusinessName.Trim().ToLower() == item.VendorName.Trim().ToLower());
                        Subject subject = repoSubject.GetAll().FirstOrDefault(x => x.Name.Trim().ToLower() == item.SubjectName.Trim().ToLower());

                        if (subject == null) { continue; }

                        ServiceVendor serviceVendor = repoServiceVendor
                                                            .GetAll()
                                                            .FirstOrDefault(x => x.SubjectId == subject.SubjectId &&
                                                                                 x.Name.Trim().ToLower() == item.VendorServiceName);

                        if (vendor != null && subject != null && serviceVendor != null)
                        {
                            if (vendor.VendorID != oldVendor && oldVendor != 0)
                            {
                                Vendor oldVendorEn = repoVendor.Get(oldVendor);

                                oldVendor = vendor.VendorID;

                                repoVendor.UpdateServices(oldVendorEn, serviceVendors);

                                uow.Commit();

                                serviceVendors = new List<Int32>();
                            }
                            else if (vendor.VendorID == oldVendor)
                            {
                                serviceVendors.Add(serviceVendor.ServiceVendorId);
                            }
                            else if (oldVendor == 0)
                            {
                                oldVendor = vendor.VendorID;

                                serviceVendors.Add(serviceVendor.ServiceVendorId);
                            }
                        }
                    }
                }

                answer.StateImport = StateImport.Successed;
                answer.Title = "Data has been successfully loaded";
            }
            catch (Exception ex)
            {
                answer.StateImport = StateImport.Failed;
                answer.Title = "Failed import data";
            }

            return answer;
        }
    }
}
