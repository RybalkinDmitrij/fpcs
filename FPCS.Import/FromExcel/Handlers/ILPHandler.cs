﻿using ClosedXML.Excel;
using FPCS.Core.Unity;
using FPCS.Data;
using FPCS.Data.Entities;
using FPCS.Data.Enums;
using FPCS.Data.Repo;
using FPCS.Import.FromExcel.Models.ILP;
using FPCS.Import.State;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Import.FromExcel.Handlers
{
    public static class ILPHandler
    {
        public static void ImportData(Int32 schoolYearId)
        {
            Status answer = new Status();

            //try
            //{
            SqlConnection conn = new SqlConnection("Server=tcp:sql2k804.discountasp.net;Database=SQL2008R2_894208_fpcs;User ID=SQL2008R2_894208_fpcs_user;Password=Password1;Trusted_Connection=False;Encrypt=False;Connection Timeout=30;");

            conn.Open();

            DataTable dt = new DataTable();

                
            // create a SqlCommand object for this connection
            SqlCommand command = conn.CreateCommand();
            command.CommandText = @"
                SELECT [intILP_ID]
                        ,[intStudent_ID]
                        ,dt.[intClass_ID]
                        ,[sintSchool_Year]
                        ,[decCourse_Hours]
                        ,[szCurriculum_Desc]
                        ,[szGoals]
                        ,[szRequirements]
                        ,[szTeacher_Role]
                        ,[szStudent_Role]
                        ,[szParent_Role]
                        ,[szEvaluation]
                        ,[bolPass_Fail]
	                    ,case 
		                when [szOther_Grading] <> '' then 1
		                when [szOther_Grading] = '' then 0
	                    end as bolOther_Grading
                        ,[szOther_Grading]
                        ,[szILP_Name]
                        ,[bolILP_Bank]
                        ,[szILP_Additions]
                        ,[bolGradingScale]
                        ,[isPublic]
	                    ,cls.intInstructor_ID
	                    ,inst.szLast_Name as instLastName
	                    ,inst.szFirst_Name as instFirstName
	                    ,cls.intGuardian_ID
	                    ,guar.szLast_Name as guarLastName
	                    ,guar.szFirst_Name as guarFirstName
	                    ,cls.szASD_Course_ID
                    FROM [SQL2008R2_894208_fpcs].[dbo].[tblILP] dt
                    INNER JOIN tblClasses cls ON dt.intClass_ID = cls.intClass_ID
                    LEFT JOIN tblINSTRUCTOR inst ON inst.intINSTRUCTOR_ID = cls.intInstructor_ID
                    LEFT JOIN tblGUARDIAN guar ON guar.intGUARDIAN_ID = cls.intGuardian_ID
                    WHERE sintSchool_Year = 2015 AND [bolILP_Bank] = 1 AND inst.szLast_Name = 'Alley' AND inst.szFirst_Name = 'Kay'";

            SqlDataAdapter da = new SqlDataAdapter(command);
            da.Fill(dt);

            conn.Close();

            List<ILPModel> xlsxData = new List<ILPModel>();

            foreach (DataRow item in dt.Rows)
            {
                ILPModel ilpModel = new ILPModel();
                ilpModel.CourseHours = Convert.ToDecimal(item["decCourse_Hours"]);
                ilpModel.CurriculumDesc = item["szCurriculum_Desc"].ToString();
                ilpModel.Goals = item["szGoals"].ToString();
                ilpModel.Requirements = item["szRequirements"].ToString();
                ilpModel.TeacherRole = item["szTeacher_Role"].ToString();
                ilpModel.StudentRole = item["szStudent_Role"].ToString();
                ilpModel.ParentRole = item["szParent_Role"].ToString();
                ilpModel.Evaluation = item["szEvaluation"].ToString();
                ilpModel.IsPassFail = Convert.ToBoolean(item["bolPass_Fail"]);
                ilpModel.IsOtherGrading = Convert.ToBoolean(item["bolOther_Grading"]);
                ilpModel.OtherGrading = item["szOther_Grading"].ToString();
                ilpModel.ILPName = item["szILP_Name"].ToString();
                ilpModel.ILPAdditions = item["szILP_Additions"].ToString();
                ilpModel.IsGradingScale = Convert.ToBoolean(item["bolGradingScale"]);
                ilpModel.InstLastName = item["instLastName"].ToString();
                ilpModel.InstFirstName = item["instFirstName"].ToString();
                ilpModel.GuarLastName = item["guarLastName"].ToString();
                ilpModel.GuarFirstName = item["guarFirstName"].ToString();
                ilpModel.ASDCourseID = item["szASD_Course_ID"].ToString();
                ilpModel.IsPublic = Convert.ToBoolean(item["isPublic"]);

                xlsxData.Add(ilpModel);
            }

            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                var repoILPBank = uow.GetRepo<IILPBankRepo>();
                var repoASD = uow.GetRepo<IASDCourseRepo>();
                var repoTeacher = uow.GetRepo<ITeacherRepo>();
                var repoGuardian = uow.GetRepo<IGuardianRepo>();

                Int32 countNotExistASDCourse = 0;
                Int32 countNotExistGuardian = 0;
                Int32 countNotExistTeacher = 0;

                List<String> notExistASDCourse = new List<String>();
                List<String> notExistGuardian = new List<String>();
                List<String> notExistTeacher = new List<String>();

                for (Int32 i = 0; i < xlsxData.Count; i++)
                {
                    try
                    {
                        //if (i < 675) continue;

                        var item = xlsxData[i];

                        ILPBank ilpBankEntity = new ILPBank();

                        ilpBankEntity.CourseHrs = Convert.ToInt32(item.CourseHours);
                        ilpBankEntity.CourseSyllabus = item.Goals;
                        ilpBankEntity.DescriptionCourse = item.CurriculumDesc;
                        ilpBankEntity.EvaluatedMeasurableOutcomes = item.Evaluation;
                        ilpBankEntity.EvaluationGradingGradingScale = item.IsGradingScale;
                        ilpBankEntity.EvaluationGradingOther = item.IsOtherGrading;
                        ilpBankEntity.EvaluationGradingOtherExplain = item.OtherGrading;
                        ilpBankEntity.EvaluationGradingPassFail = item.IsPassFail;
                        ilpBankEntity.GuardianILPModifications =
                            !String.IsNullOrEmpty(item.GuarFirstName) && !String.IsNullOrEmpty(item.GuarLastName) ?
                            item.ILPAdditions :
                            String.Empty;
                        ilpBankEntity.InstructorILPModifications =
                            !String.IsNullOrEmpty(item.InstFirstName) && !String.IsNullOrEmpty(item.InstLastName) ?
                            item.ILPAdditions :
                            String.Empty;
                        ilpBankEntity.ILPName = item.ILPName;
                        ilpBankEntity.IsPublic = item.IsPublic;
                        ilpBankEntity.MaterialsResources = item.StudentRole;
                        ilpBankEntity.RoleAnyPeople = item.ParentRole;
                        ilpBankEntity.SchoolYearId = schoolYearId;
                        ilpBankEntity.Standards = item.TeacherRole.Length > 18000 ? item.TeacherRole.Substring(0, 17999) : item.TeacherRole;
                        ilpBankEntity.StudentActivities = item.Requirements;

                        var asdCourse = repoASD.GetAll(schoolYearId).FirstOrDefault(x => x.ExternalASDCourseId.ToLower() == item.ASDCourseID.ToLower());

                        if (asdCourse == null)
                        {
                            countNotExistASDCourse++;
                            notExistASDCourse.Add(item.ASDCourseID);
                        }
                        else
                        {
                            ilpBankEntity.SubjectId = asdCourse.SubjectId;
                        }

                        var guardian = repoGuardian.GetAll(schoolYearId).FirstOrDefault(x => x.LastName.ToLower() == item.GuarLastName.ToLower() && x.FirstName.ToLower() == item.GuarFirstName.ToLower());

                        if (!String.IsNullOrEmpty(item.GuarLastName) &&
                            !String.IsNullOrEmpty(item.GuarFirstName) &&
                            guardian == null)
                        {
                            countNotExistGuardian++;
                            notExistGuardian.Add(item.GuarLastName + ", " + item.GuarFirstName);
                            continue;
                        }

                        if (guardian != null)
                        {
                            ilpBankEntity.GuardianId = guardian.DbUserId;
                        }
                        else
                        {
                            ilpBankEntity.GuardianId = null;
                        }

                        var teacher = repoTeacher.GetAll(schoolYearId).FirstOrDefault(x => x.LastName.ToLower() == item.InstLastName.ToLower() && x.FirstName.ToLower() == item.InstFirstName.ToLower());

                        if (!String.IsNullOrEmpty(item.InstLastName) &&
                            !String.IsNullOrEmpty(item.InstFirstName) &&
                            teacher == null)
                        {
                            countNotExistTeacher++;
                            notExistTeacher.Add(item.InstLastName + ", " + item.InstFirstName);
                            continue;
                        }

                        if (teacher != null)
                        {
                            ilpBankEntity.TeacherId = teacher.DbUserId;
                        }
                        else
                        {
                            ilpBankEntity.TeacherId = null;
                        }

                        repoILPBank.Add(ilpBankEntity);

                        uow.Commit();
                    }
                    catch (Exception ex)
                    {
                        var g = 0;
                    }
                }
            }

            var t = 0;
            //}
            //catch (Exception ex)
            //{
            //    //throw ex;
            //    var g = 0;
            //}
        }

        public static void ImportILPPhilosophy(Int32 schoolYearId)
        {
            Status answer = new Status();

            try
            {
                SqlConnection conn = new SqlConnection("Server=tcp:sql2k804.discountasp.net;Database=SQL2008R2_894208_fpcs;User ID=SQL2008R2_894208_fpcs_user;Password=Password1;Trusted_Connection=False;Encrypt=False;Connection Timeout=30;");

                conn.Open();

                DataTable dt = new DataTable();


                // create a SqlCommand object for this connection
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"
                    SELECT ph.szPhilosophy, st.szFirst_Name, st.szLast_Name
                      FROM [dbo].[tblEnroll_Info] dt
                      INNER JOIN tblPhilosophy ph ON ph.intPhilosophy_ID = dt.intPhilosophy_ID
                      INNER JOIN tblStudent st ON st.intStudent_ID = dt.intStudent_ID
                      WHERE dt.sintSCHOOL_YEAR = 2014";

                SqlDataAdapter da = new SqlDataAdapter(command);
                da.Fill(dt);

                conn.Close();

                List<ILPPhilosophyModel> xlsxData = new List<ILPPhilosophyModel>();

                foreach (DataRow item in dt.Rows)
                {
                    ILPPhilosophyModel ilpModel = new ILPPhilosophyModel();
                    ilpModel.FirstName = item["szFirst_Name"].ToString();
                    ilpModel.LastName = item["szLast_Name"].ToString();
                    ilpModel.ILPPhilosophy = item["szPhilosophy"].ToString();
                    
                    xlsxData.Add(ilpModel);
                }

                Int32 r = 0;

                using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
                {
                    var repoStudent = uow.GetRepo<IStudentRepo>();

                    Int32 i = 0;
                    foreach (var item in xlsxData)
                    {
                        i++;

                        Student student = repoStudent.GetAll()
                                                     .Where(x => x.FirstName.Trim().ToLower() == item.FirstName.Trim().ToLower() &&
                                                                 x.LastName.Trim().ToLower() == item.LastName.Trim().ToLower() &&
                                                                 x.SchoolYearId == schoolYearId)
                                                     .FirstOrDefault();

                        if (student == null) continue;

                        student.ILPPhilosophy = item.ILPPhilosophy;

                        repoStudent.Update(student);
                        r++;
                    }

                    uow.Commit();
                }

                var t = 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
