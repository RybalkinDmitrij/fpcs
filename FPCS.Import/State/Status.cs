﻿using FPCS.Import.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FPCS.Import.State
{
    public class Status
    {
        public StateImport StateImport { get; set; }

        public String Title { get; set; }

        public String Message { get; set; }

        public Int32 CountInsert { get; set; }

        public Int32 CountUpdate { get; set; }

        public Int32 CountDelete { get; set; }
    }
}
