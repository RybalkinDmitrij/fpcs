﻿using System;
using System.Web.Mvc;
using System.Linq;
using FPCS.Web.Admin.Code;
using FPCS.Data.Enums;
using FPCS.Web.Admin.Models.Home;
using FPCS.Data;
using FPCS.Data.Repo;

namespace FPCS.Web.Admin.Controllers
{
	[FPCSAuthorize()]
	public class HomeController : BaseController
	{
		public ActionResult Index()
		{
			if (User.Role == Role.Guardian)
			{
				return RedirectToAction("IndexGuardian");
			}

			if (User.Role == Role.Admin || User.Role == Role.Teacher)
			{
				return RedirectToAction("Index", "Student");
			}

			return View();
		}

		public ActionResult IndexGuardian()
		{
			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				var repo = uow.GetRepo<IStudentRepo>();
				var repoFamily = uow.GetRepo<IFamilyRepo>();

				IndexGuardianModel model = new IndexGuardianModel();

				model.FamilyId = repoFamily.GetFamiliesByGuardian(User.UserId).Select(x => x.FamilyId).FirstOrDefault();
				model.Students = repo.GetByUser(User.UserId, User.Role, SchoolYear.SchoolYearId)
					.Select(x => new StudentModel()
					{
						DbUserId = x.DbUserId,
						FullName = x.LastName + ", " + x.FirstName
					})
					.ToList();

				//if (model.Students != null && model.Students.Count == 1)
				//{
				//    return RedirectToAction("Index", "StudentPacket2", new { id = model.Students.Select(x => x.DbUserId).FirstOrDefault() });
				//}

				return View(model);
			}
		}
	}
}
