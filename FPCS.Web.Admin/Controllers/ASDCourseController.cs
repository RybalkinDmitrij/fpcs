﻿using System;
using System.Linq;
using System.Web.Mvc;

using FPCS.Core.jqGrid;
using FPCS.Data;
using FPCS.Data.Repo;
using FPCS.Data.Enums;
using FPCS.Data.Exceptions;
using FPCS.Web.Admin.Code;
using FPCS.Web.Admin.Models.ASDCourse;
using System.Net;

namespace FPCS.Web.Admin.Controllers
{
    public class ASDCourseController : BaseController
    {
        [FPCSAuthorize(Role.Admin)]
        public ActionResult Index()
        {
            return View();
        }

        [FPCSAuthorize(Role.Admin)]
        public JsonResult _Index(GridOptions options, ASDCoursesListOptions coursesListOptions)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IASDCourseRepo>();
                var dbList = repo.GetBySchoolYear(SchoolYear.SchoolYearId);

                var engine = new GridDynamicEngine(options, coursesListOptions);
                var result = engine.ApplyAll(dbList, x => new ASDCourseMiniModel
                {
                    ASDCourseId = x.ASDCourseId,
                    ExternalASDCourseId = x.ExternalASDCourseId + " (" + x.Description + ")",
                    Subject = x.Subject.Name,
                    Name = x.Name,
                    GradCredit = x.GradCredit,
                    IsActivated = x.IsActivated ? "Activated" : "Not Activated",
                    IsFree = x.IsFree ? "Free" : "Not Free"
                });

                return Json(result);
            }
        }


        [HttpGet]
        [FPCSAuthorize(Role.Admin, Role.Teacher)]
        public PartialViewResult _Details(Int32 id)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IASDCourseRepo>();
                var dbEntity = repo.Get(id);
                if (dbEntity == null) return ErrorPartial("ASD Course {0} not found", id);

                var model = new ASDCourseDetailsModel
                {
                    ASDCourseId = dbEntity.ASDCourseId,
                    ExternalASDCourseId = dbEntity.ExternalASDCourseId,
                    IsActivated = dbEntity.IsActivated,
                    Subject = dbEntity.Subject.Name,
                    Description = dbEntity.Description,
                    Name = dbEntity.Name,
                    GradCredit = dbEntity.GradCredit,
                    IsFree = dbEntity.IsFree
                };

                return PartialView(model);
            }
        }


        [HttpGet]
        [FPCSAuthorize(Role.Admin)]
        public PartialViewResult _Create()
        {
            var model = new ASDCourseCreateModel();
            model.Init();
            return PartialView(model);
        }

        [HttpPost]
        [FPCSAuthorize(Role.Admin)]
        [ValidateAntiForgeryToken]
        public ActionResult _Create(ASDCourseCreateModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var uow = UnityManager.Resolve<IUnitOfWork>())
                    {
                        var repo = uow.GetRepo<IASDCourseRepo>();
                        var dbEntity = repo.Add(model.ExternalASDCourseId, model.Description, model.Name, model.GradCredit, model.SubjectId, SchoolYear.SchoolYearId, model.IsActivated, model.IsFree);
                        uow.Commit();

                        return JsonRes(dbEntity.ASDCourseId.ToString());
                    }
                }
                catch (DuplicateEntityException ex)
                {
                    ModelState.AddModelError("ExternalASDCourseId", ex.Message);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }
            }

            model.Init();
            return PartialView(model);
        }


        [HttpGet]
        [FPCSAuthorize(Role.Admin)]
        public PartialViewResult _Edit(Int32 id)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IASDCourseRepo>();
                var dbEntity = repo.Get(id);
                if (dbEntity == null) return ErrorPartial("ASD Course {0} not found", id);

                var model = new ASDCourseEditModel
                {
                    ASDCourseId = dbEntity.ASDCourseId,
                    ExternalASDCourseId = dbEntity.ExternalASDCourseId,
                    SubjectId = dbEntity.SubjectId,
                    IsActivated = dbEntity.IsActivated,
                    IsFree = dbEntity.IsFree,
                    IsCreatedFPCSCourses = uow.GetRepo<IFPCSCourseRepo>().GetByASDCourse(id).Count() > 0,
                    Description = dbEntity.Description,
                    Name = dbEntity.Name,
                    GradCredit = dbEntity.GradCredit
                };

                model.Init();
                return PartialView(model);
            }
        }

        [HttpPost]
        [FPCSAuthorize(Role.Admin)]
        [ValidateAntiForgeryToken]
        public ActionResult _Edit(ASDCourseEditModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var uow = UnityManager.Resolve<IUnitOfWork>())
                    {
                        var repo = uow.GetRepo<IASDCourseRepo>();
                        var dbEntity = repo.Update(model.ASDCourseId, model.ExternalASDCourseId, model.Description, model.Name, model.GradCredit, model.SubjectId, model.IsActivated, SchoolYear.SchoolYearId, model.IsFree);
                        uow.Commit();

                        return JsonRes(dbEntity.ASDCourseId.ToString());
                    }
                }
                catch (DuplicateEntityException ex)
                {
                    ModelState.AddModelError("ExternalASDCourseId", ex.Message);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }
            }

            model.Init();
            return PartialView(model);
        }


        [HttpPost]
        [FPCSAuthorize(Role.Admin)]
        public ActionResult Delete(Int32 id)
        {
            try
            {
                using (var uow = UnityManager.Resolve<IUnitOfWork>())
                {
                    var repo = uow.GetRepo<IASDCourseRepo>();
                    repo.Remove(id);

                    uow.Commit();
                    return JsonRes();
                }
            }
            catch (Exception ex)
            {
                return JsonRes(Status.Error, ex.Message);
            }
        }

        [HttpPost]
        [FPCSAuthorize(Role.Admin)]
        public ActionResult DeleteAll(String id)
        {
            try
            {
                if (String.IsNullOrEmpty(id)) return JsonRes(Status.Error, "Not correct ids");

                var ids = id.Split(',');
                using (var uow = UnityManager.Resolve<IUnitOfWork>())
                {
                    Int32 errorsCount = 0;
                    var repo = uow.GetRepo<IASDCourseRepo>();
                    foreach (var item in ids)
                    {
                        try
                        {
                            repo.Remove(Int32.Parse(item));
                        }
                        catch (Exception)
                        {
                            errorsCount++;
                        }
                    }

                    uow.Commit();
                    if (errorsCount == 0) return JsonRes(); 
                    else return JsonRes(Status.Error, "Were deleted not all entities");
                }
            }
            catch (Exception ex)
            {
                return JsonRes(Status.Error, ex.Message);
            }
        }
    }
}
