﻿using System;
using System.Web.Mvc;
using System.Linq;
using System.Collections.Generic;
using FPCS.Core.Unity;
using FPCS.Data;
using FPCS.Data.Exceptions;
using FPCS.Data.Repo;
using FPCS.Data.Enums;
using FPCS.Data.Entities;
using FPCS.Web.Admin.Code;
using FPCS.Web.Admin.Models.SchoolYear;
using FPCS.Data.Helpers.HelperModels;
using FPCS.Data.Helpers;

namespace FPCS.Web.Admin.Controllers
{
	[FPCSAuthorize(Role.Admin)]
	public class SchoolYearController : BaseController
	{
		public ActionResult Index(Int32? year)
		{
			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				var repo = uow.GetRepo<ISchoolYearRepo>();
				var dbEntity = year.HasValue ? repo.GetByYear(year.Value) : repo.Get(SchoolYear.SchoolYearId);

				if (dbEntity == null) return RedirectToAction("Create", new {year = year});

				var model = new SchoolYearDetailsModel
				{
					SchoolYearId = dbEntity.SchoolYearId,
					Year = dbEntity.Year,
					Name = dbEntity.Name,
					StartDate = dbEntity.StartDate,
					EndDate = dbEntity.EndDate,
					HalfDate = dbEntity.HalfDate,
					UpdatedDate = DateTimeOffset.Now,
					ILPHoursLimit = dbEntity.ILPHoursLimit,
					CountHoursWorkDay = dbEntity.CountHoursWorkDay,
					PlusBudgetLimit = dbEntity.PlusBudgetLimit,
					BookMaterialFee = dbEntity.BookMaterialFee,
					GroupLifeInsurance = dbEntity.GroupLifeInsurance,
					GroupMedicalInsurance = dbEntity.GroupMedicalInsurance,
					WorkersComp = dbEntity.WorkersComp,
					Unemployment = dbEntity.Unemployment,
					FICA = dbEntity.FICA,
					FICACap = dbEntity.FICACap,
					Medicare = dbEntity.Medicare,
					TERSBaseContract = dbEntity.TERSBaseContract,
					PERSBaseContract = dbEntity.PERSBaseContract,
					TERSRetirement = dbEntity.TERSRetirement,
					PERSRetirement = dbEntity.PERSRetirement,
					GeneralSupplies = dbEntity.GeneralSupplies,
					FundingAmountGradeK = dbEntity.FundingAmountGradeK,
					FundingAmountGrade1 = dbEntity.FundingAmountGrade1,
					FundingAmountGrade2 = dbEntity.FundingAmountGrade2,
					FundingAmountGrade3 = dbEntity.FundingAmountGrade3,
					FundingAmountGrade4 = dbEntity.FundingAmountGrade4,
					FundingAmountGrade5 = dbEntity.FundingAmountGrade5,
					FundingAmountGrade6 = dbEntity.FundingAmountGrade6,
					FundingAmountGrade7 = dbEntity.FundingAmountGrade7,
					FundingAmountGrade8 = dbEntity.FundingAmountGrade8,
					FundingAmountGrade9 = dbEntity.FundingAmountGrade9,
					FundingAmountGrade10 = dbEntity.FundingAmountGrade10,
					FundingAmountGrade11 = dbEntity.FundingAmountGrade11,
					FundingAmountGrade12 = dbEntity.FundingAmountGrade12,
					FundingAmountGradeG = dbEntity.FundingAmountGradeG,
					CoreUnitGradeK = dbEntity.CoreUnitGradeK,
					CoreUnitGrade1 = dbEntity.CoreUnitGrade1,
					CoreUnitGrade2 = dbEntity.CoreUnitGrade2,
					CoreUnitGrade3 = dbEntity.CoreUnitGrade3,
					CoreUnitGrade4 = dbEntity.CoreUnitGrade4,
					CoreUnitGrade5 = dbEntity.CoreUnitGrade5,
					CoreUnitGrade6 = dbEntity.CoreUnitGrade6,
					CoreUnitGrade7 = dbEntity.CoreUnitGrade7,
					CoreUnitGrade8 = dbEntity.CoreUnitGrade8,
					CoreUnitGrade9 = dbEntity.CoreUnitGrade9,
					CoreUnitGrade10 = dbEntity.CoreUnitGrade10,
					CoreUnitGrade11 = dbEntity.CoreUnitGrade11,
					CoreUnitGrade12 = dbEntity.CoreUnitGrade12,
					CoreUnitGradeG = dbEntity.CoreUnitGradeG,
					ElectiveUnitGradeK = dbEntity.ElectiveUnitGradeK,
					ElectiveUnitGrade1 = dbEntity.ElectiveUnitGrade1,
					ElectiveUnitGrade2 = dbEntity.ElectiveUnitGrade2,
					ElectiveUnitGrade3 = dbEntity.ElectiveUnitGrade3,
					ElectiveUnitGrade4 = dbEntity.ElectiveUnitGrade4,
					ElectiveUnitGrade5 = dbEntity.ElectiveUnitGrade5,
					ElectiveUnitGrade6 = dbEntity.ElectiveUnitGrade6,
					ElectiveUnitGrade7 = dbEntity.ElectiveUnitGrade7,
					ElectiveUnitGrade8 = dbEntity.ElectiveUnitGrade8,
					ElectiveUnitGrade9 = dbEntity.ElectiveUnitGrade9,
					ElectiveUnitGrade10 = dbEntity.ElectiveUnitGrade10,
					ElectiveUnitGrade11 = dbEntity.ElectiveUnitGrade11,
					ElectiveUnitGrade12 = dbEntity.ElectiveUnitGrade12,
					ElectiveUnitGradeG = dbEntity.ElectiveUnitGradeG,
					DisableAccessForParents = dbEntity.DisableAccessForParents,
					DisableAccessForTeachers = dbEntity.DisableAccessForTeachers,
					DisableAccessMessage = dbEntity.DisableMessage
				};
				model.Init();

				return View(model);
			}
		}

		[HttpGet]
		public ActionResult Create(Int32? year)
		{
			if (!year.HasValue) year = DateTime.Today.Year + 1;

			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				var dbEntity = uow.GetRepo<ISchoolYearRepo>().GetByYear(year.Value);
				if (dbEntity != null) return RedirectToAction("Edit", new {year = dbEntity.Year});
			}
			var model = new SchoolYearCreateModel();
			model.Year = year.Value;
			model.StartDate = new DateTimeOffset(model.Year - 1, 6, 1, 0, 0, 0, TimeSpan.Zero);
			model.EndDate = new DateTimeOffset(model.Year, 5, 31, 23, 59, 59, TimeSpan.Zero);
			model.HalfDate = new DateTimeOffset(model.Year, 1, 1, 0, 0, 0, TimeSpan.Zero);
			model.Name = String.Format(@"{0} - {1}", model.StartDate.Year.ToString(), model.EndDate.Year.ToString());

			return View(model);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create(SchoolYearCreateModel model)
		{
			if (model.StartDate.Year != model.Year - 1)
				ModelState.AddModelError("StartDate", "Year should be " + (model.Year - 1).ToString());
			if (model.EndDate.Year != model.Year) ModelState.AddModelError("EndDate", "Year should be " + model.Year.ToString());
			if (model.HalfDate <= model.StartDate || model.HalfDate >= model.EndDate)
				ModelState.AddModelError("HalfDate", "Date should be greater start date and less end date");

			if (ModelState.IsValid)
			{
				using (var uow = UnityManager.Resolve<IUnitOfWork>())
				{
					try
					{
						var repo = uow.GetRepo<ISchoolYearRepo>();
						var dbEntity = repo.Add(
							model.StartDate,
							model.EndDate,
							model.HalfDate,
							model.FundingAmountGradeK.Value,
							model.FundingAmountGrade1.Value,
							model.FundingAmountGrade2.Value,
							model.FundingAmountGrade3.Value,
							model.FundingAmountGrade4.Value,
							model.FundingAmountGrade5.Value,
							model.FundingAmountGrade6.Value,
							model.FundingAmountGrade7.Value,
							model.FundingAmountGrade8.Value,
							model.FundingAmountGrade9.Value,
							model.FundingAmountGrade10.Value,
							model.FundingAmountGrade11.Value,
							model.FundingAmountGrade12.Value,
							model.FundingAmountGradeG.Value,
							model.CoreUnitGradeK.Value,
							model.CoreUnitGrade1.Value,
							model.CoreUnitGrade2.Value,
							model.CoreUnitGrade3.Value,
							model.CoreUnitGrade4.Value,
							model.CoreUnitGrade5.Value,
							model.CoreUnitGrade6.Value,
							model.CoreUnitGrade7.Value,
							model.CoreUnitGrade8.Value,
							model.CoreUnitGrade9.Value,
							model.CoreUnitGrade10.Value,
							model.CoreUnitGrade11.Value,
							model.CoreUnitGrade12.Value,
							model.CoreUnitGradeG.Value,
							model.ElectiveUnitGradeK.Value,
							model.ElectiveUnitGrade1.Value,
							model.ElectiveUnitGrade2.Value,
							model.ElectiveUnitGrade3.Value,
							model.ElectiveUnitGrade4.Value,
							model.ElectiveUnitGrade5.Value,
							model.ElectiveUnitGrade6.Value,
							model.ElectiveUnitGrade7.Value,
							model.ElectiveUnitGrade8.Value,
							model.ElectiveUnitGrade9.Value,
							model.ElectiveUnitGrade10.Value,
							model.ElectiveUnitGrade11.Value,
							model.ElectiveUnitGrade12.Value,
							model.ElectiveUnitGradeG.Value,
							model.ILPHoursLimit,
							model.CountHoursWorkDay,
							model.PlusBudgetLimit,
							model.BookMaterialFee,
							model.GroupLifeInsurance,
							model.GroupMedicalInsurance,
							model.WorkersComp,
							model.Unemployment,
							model.FICA,
							model.FICACap,
							model.Medicare,
							model.TERSBaseContract,
							model.PERSBaseContract,
							model.TERSRetirement,
							model.PERSRetirement,
							model.GeneralSupplies,
							model.DisableAccessForParents,
							model.DisableAccessForTeachers,
							model.DisableAccessMessage);

						uow.Commit();

						//UpdateRateTeachers(dbEntity.SchoolYearId);

						HttpContext.Session.SetSchoolYear(dbEntity.Year);
						return RedirectToAction("Index", new {year = dbEntity.Year});
					}
					catch (DuplicateEntityException ex)
					{
						ModelState.AddModelError("Year", ex.Message);
					}
					catch (InvalidRangeException ex)
					{
						ModelState.AddModelError("StartDate", ex.Message);
						ModelState.AddModelError("EndDate", ex.Message);
					}
				}
			}

			return View(model);
		}


		[HttpGet]
		public ActionResult Edit(Int32? year)
		{
			if (!year.HasValue) return RedirectToAction("Index");

			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				var dbEntity = uow.GetRepo<ISchoolYearRepo>().GetByYear(year.Value);
				if (dbEntity == null) return RedirectToAction("Create", new {year = year});

				var model = new SchoolYearEditModel
				{
					SchoolYearId = dbEntity.SchoolYearId,
					Year = dbEntity.Year,
					Name = dbEntity.Name,
					StartDate = dbEntity.StartDate,
					EndDate = dbEntity.EndDate,
					HalfDate = dbEntity.HalfDate,
					GeneralSupplies = dbEntity.GeneralSupplies,
					FundingAmountGradeK = dbEntity.FundingAmountGradeK,
					FundingAmountGrade1 = dbEntity.FundingAmountGrade1,
					FundingAmountGrade2 = dbEntity.FundingAmountGrade2,
					FundingAmountGrade3 = dbEntity.FundingAmountGrade3,
					FundingAmountGrade4 = dbEntity.FundingAmountGrade4,
					FundingAmountGrade5 = dbEntity.FundingAmountGrade5,
					FundingAmountGrade6 = dbEntity.FundingAmountGrade6,
					FundingAmountGrade7 = dbEntity.FundingAmountGrade7,
					FundingAmountGrade8 = dbEntity.FundingAmountGrade8,
					FundingAmountGrade9 = dbEntity.FundingAmountGrade9,
					FundingAmountGrade10 = dbEntity.FundingAmountGrade10,
					FundingAmountGrade11 = dbEntity.FundingAmountGrade11,
					FundingAmountGrade12 = dbEntity.FundingAmountGrade12,
					FundingAmountGradeG = dbEntity.FundingAmountGradeG,
					CoreUnitGradeK = dbEntity.CoreUnitGradeK,
					CoreUnitGrade1 = dbEntity.CoreUnitGrade1,
					CoreUnitGrade2 = dbEntity.CoreUnitGrade2,
					CoreUnitGrade3 = dbEntity.CoreUnitGrade3,
					CoreUnitGrade4 = dbEntity.CoreUnitGrade4,
					CoreUnitGrade5 = dbEntity.CoreUnitGrade5,
					CoreUnitGrade6 = dbEntity.CoreUnitGrade6,
					CoreUnitGrade7 = dbEntity.CoreUnitGrade7,
					CoreUnitGrade8 = dbEntity.CoreUnitGrade8,
					CoreUnitGrade9 = dbEntity.CoreUnitGrade9,
					CoreUnitGrade10 = dbEntity.CoreUnitGrade10,
					CoreUnitGrade11 = dbEntity.CoreUnitGrade11,
					CoreUnitGrade12 = dbEntity.CoreUnitGrade12,
					CoreUnitGradeG = dbEntity.CoreUnitGradeG,
					ElectiveUnitGradeK = dbEntity.ElectiveUnitGradeK,
					ElectiveUnitGrade1 = dbEntity.ElectiveUnitGrade1,
					ElectiveUnitGrade2 = dbEntity.ElectiveUnitGrade2,
					ElectiveUnitGrade3 = dbEntity.ElectiveUnitGrade3,
					ElectiveUnitGrade4 = dbEntity.ElectiveUnitGrade4,
					ElectiveUnitGrade5 = dbEntity.ElectiveUnitGrade5,
					ElectiveUnitGrade6 = dbEntity.ElectiveUnitGrade6,
					ElectiveUnitGrade7 = dbEntity.ElectiveUnitGrade7,
					ElectiveUnitGrade8 = dbEntity.ElectiveUnitGrade8,
					ElectiveUnitGrade9 = dbEntity.ElectiveUnitGrade9,
					ElectiveUnitGrade10 = dbEntity.ElectiveUnitGrade10,
					ElectiveUnitGrade11 = dbEntity.ElectiveUnitGrade11,
					ElectiveUnitGrade12 = dbEntity.ElectiveUnitGrade12,
					ElectiveUnitGradeG = dbEntity.ElectiveUnitGradeG,
					ILPHoursLimit = dbEntity.ILPHoursLimit,
					CountHoursWorkDay = dbEntity.CountHoursWorkDay,
					PlusBudgetLimit = dbEntity.PlusBudgetLimit,
					BookMaterialFee = dbEntity.BookMaterialFee,
					GroupLifeInsurance = dbEntity.GroupLifeInsurance,
					GroupMedicalInsurance = dbEntity.GroupMedicalInsurance,
					WorkersComp = dbEntity.WorkersComp,
					Unemployment = dbEntity.Unemployment,
					FICA = dbEntity.FICA,
					FICACap = dbEntity.FICACap,
					Medicare = dbEntity.Medicare,
					TERSBaseContract = dbEntity.TERSBaseContract,
					PERSBaseContract = dbEntity.PERSBaseContract,
					TERSRetirement = dbEntity.TERSRetirement,
					PERSRetirement = dbEntity.PERSRetirement,
					DisableAccessForParents = dbEntity.DisableAccessForParents,
					DisableAccessForTeachers = dbEntity.DisableAccessForTeachers,
					DisableAccessMessage = dbEntity.DisableMessage
				};
				return View(model);
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit(SchoolYearEditModel model)
		{
			if (model.StartDate.Year != model.Year - 1)
				ModelState.AddModelError("StartDate", "Year should be " + (model.Year - 1).ToString());
			if (model.EndDate.Year != model.Year) ModelState.AddModelError("EndDate", "Year should be " + model.Year.ToString());
			if (model.HalfDate <= model.StartDate || model.HalfDate >= model.EndDate)
				ModelState.AddModelError("HalfDate", "Date should be greater start date and less end date");

			if (ModelState.IsValid)
			{
				using (var uow = UnityManager.Resolve<IUnitOfWork>())
				{
					try
					{
						var repo = uow.GetRepo<ISchoolYearRepo>();

						var dbEntity = repo.Update(model.SchoolYearId, model.StartDate, model.EndDate, model.HalfDate,
							model.FundingAmountGradeK.Value, model.FundingAmountGrade1.Value,
							model.FundingAmountGrade2.Value, model.FundingAmountGrade3.Value, model.FundingAmountGrade4.Value,
							model.FundingAmountGrade5.Value,
							model.FundingAmountGrade6.Value, model.FundingAmountGrade7.Value, model.FundingAmountGrade8.Value,
							model.FundingAmountGrade9.Value,
							model.FundingAmountGrade10.Value, model.FundingAmountGrade11.Value, model.FundingAmountGrade12.Value,
							model.FundingAmountGradeG.Value,
							model.CoreUnitGradeK.Value, model.CoreUnitGrade1.Value,
							model.CoreUnitGrade2.Value, model.CoreUnitGrade3.Value, model.CoreUnitGrade4.Value, model.CoreUnitGrade5.Value,
							model.CoreUnitGrade6.Value, model.CoreUnitGrade7.Value, model.CoreUnitGrade8.Value, model.CoreUnitGrade9.Value,
							model.CoreUnitGrade10.Value, model.CoreUnitGrade11.Value, model.CoreUnitGrade12.Value, model.CoreUnitGradeG.Value,
							model.ElectiveUnitGradeK.Value, model.ElectiveUnitGrade1.Value,
							model.ElectiveUnitGrade2.Value, model.ElectiveUnitGrade3.Value, model.ElectiveUnitGrade4.Value,
							model.ElectiveUnitGrade5.Value,
							model.ElectiveUnitGrade6.Value, model.ElectiveUnitGrade7.Value, model.ElectiveUnitGrade8.Value,
							model.ElectiveUnitGrade9.Value,
							model.ElectiveUnitGrade10.Value, model.ElectiveUnitGrade11.Value, model.ElectiveUnitGrade12.Value,
							model.ElectiveUnitGradeG.Value,
							model.ILPHoursLimit, model.CountHoursWorkDay, model.PlusBudgetLimit, model.BookMaterialFee,
							model.GroupLifeInsurance, model.GroupMedicalInsurance, model.WorkersComp,
							model.Unemployment, model.FICA, model.FICACap, model.Medicare, model.TERSBaseContract, model.PERSBaseContract,
							model.TERSRetirement, model.PERSRetirement, model.GeneralSupplies, model.DisableAccessForParents, model.DisableAccessForTeachers,
							model.DisableAccessMessage);

						uow.Commit();

						UpdateRateTeachers(dbEntity.SchoolYearId);

						return RedirectToAction("Index", new {year = dbEntity.Year});
					}
					catch (DuplicateEntityException ex)
					{
						ModelState.AddModelError("Year", ex.Message);
					}
					catch (InvalidRangeException ex)
					{
						ModelState.AddModelError("StartDate", ex.Message);
						ModelState.AddModelError("EndDate", ex.Message);
					}
				}
			}

			return View(model);
		}

		private void UpdateRateTeachers(Int32 schoolYearId)
		{
			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				var repo = uow.GetRepo<ITeacherRepo>();

				List<Teacher> teachers = repo.GetAll(schoolYearId).ToList();

				foreach (var item in teachers)
				{
					TaxRate taxRate = TeacherRateHelper.CalcTaxRate(item);
					item.BasePayHour = taxRate.BaseHourlyRate;
					item.PayHourwBenefits = taxRate.HourlyRateTaxesBenefits;

					repo.Update(item);

					uow.Commit();
				}
			}
		}
	}
}