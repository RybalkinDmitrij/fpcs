﻿using FPCS.Core.jqGrid;
using FPCS.Core.Extensions;
using FPCS.Data;
using FPCS.Data.Enums;
using FPCS.Data.Repo;
using FPCS.Web.Admin.Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FPCS.Data.Entities;
using FPCS.Web.Admin.Models.FPCSStudentPacket;
using FPCS.Web.Admin.Models.GoodAndService;
using FPCS.Web.Admin.Models.StudentPacketCourseAlert;
using FPCS.Web.Admin.Models.SessionModels;
using FPCS.Web.Admin.Helpers.GoodAndService;
using FPCS.Web.Admin.Helpers.Access.Models;
using FPCS.Web.Admin.Helpers.Access;
using System.Configuration;

namespace FPCS.Web.Admin.Controllers
{
    /// <summary>
    /// Controller Student Packet Course
    /// </summary>
    public class FPCSStudentPacketController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult _Details(Int32? studentPacketCourseId, Int32? courseId)
        {
            DetailsModel model = new DetailsModel(studentPacketCourseId, courseId);

            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IGoodServiceRepo>();

                var balances = uow.GetRepo<IGoodServiceBalanceRepo>()
                                  .GetByStudentPacketCourseOrFPCSCourse(model.StudentPacketCourseId, model.CourseId)
                                  .ToList();

                model.TableGoodAndServiceModel.GoodsAndServices = repo.GetByStudentPacketCourseOrFPCSCourse(model.StudentPacketCourseId, model.CourseId)
                                                                      .ToList()
                                                                      .Select(x => new GoodAndServiceModel(x, balances))
                                                                      .ToList();

                StudentPacketCourse spc = uow.GetRepo<IStudentPacketCourseRepo>().Get(studentPacketCourseId.GetValueOrDefault(0));

                if (spc != null)
                {
                    Guid userId = spc.FPCSCourse.TeacherId.HasValue ?
                        spc.FPCSCourse.Teacher.DbUserId :
                        spc.FPCSCourse.GuardianId .HasValue ?
                            spc.FPCSCourse.Guardian.DbUserId :
                            Guid.Empty;

                    model.SponsorSignatureSem1 = spc.StudentPacket.SponsorSignatureSem1;
                    model.SponsorSignatureSem2 = spc.StudentPacket.SponsorSignatureSem2;

                    model.IsCanEditILP = User.Role != Role.Guardian ||
                                        (User.Role == Role.Guardian && userId == User.UserId);

                    model.TableGoodAndServiceModel.GoodsAndServices.AddRange(balances.Select(x => new GoodAndServiceModel(x)).ToList());
                    model.TableGoodAndServiceModel.GoodsAndServices.Add(new GoodAndServiceModel(GoodAndServiceHelper.GetInstructorFPCSCourse(spc)));

                    model.TableGoodAndServiceModel
                         .GoodsAndServices = model.TableGoodAndServiceModel
                                                  .GoodsAndServices
                                                  .Where(x => x.IsShow)
                                                  .OrderByDescending(x => x.IsInstructor)
                                                  .ThenBy(x => x.GoodAndServiceID)
                                                  .ThenBy(x => x.GoodAndServiceBalanceID)
                                                  .ToList();

                    model.SignModel = new SignModel(spc, "/StudentPacket/Index/" + spc.StudentPacket.StudentId.ToString(), Session.GetUser());

                    var repoAlerts = uow.GetRepo<IStudentPacketCourseAlertRepo>();

                    model.TableAlertModel.Alerts = repoAlerts
                                                        .GetAll()
                                                        .Where(x => x.StudentPacketCourseId == model.StudentPacketCourseId)
                                                        .ToList()
                                                        .Select(x => new AlertModel(x))
                                                        .ToList();
                }
                else
                {
                    FPCSCourse fpcsCourse = uow.GetRepo<IFPCSCourseRepo>().Get(courseId.GetValueOrDefault(0));

                    Guid userId = fpcsCourse.TeacherId.HasValue ?
                        fpcsCourse.Teacher.DbUserId :
                        fpcsCourse.GuardianId.HasValue ?
                            fpcsCourse.Guardian.DbUserId :
                            Guid.Empty;

                    model.IsCanEditILP = User.Role != Role.Guardian ||
                                        (User.Role == Role.Guardian && userId == User.UserId);
                }
            }

            return PartialView(model);
        }

        [HttpGet]
        public ActionResult _Reviews(Int32? studentPacketCourseId, Int32? courseId)
        {
            ReviewsModel model = new ReviewsModel(studentPacketCourseId, courseId);

            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IGoodServiceRepo>();

                var balances = uow.GetRepo<IGoodServiceBalanceRepo>()
                                  .GetByStudentPacketCourseOrFPCSCourse(model.StudentPacketCourseId, model.CourseId)
                                  .ToList();

                model.TableGoodAndServiceModel.GoodsAndServices = repo.GetByStudentPacketCourseOrFPCSCourse(model.StudentPacketCourseId, model.CourseId)
                                                                      .ToList()
                                                                      .Select(x => new GoodAndServiceModel(x, balances))
                                                                      .ToList();

                StudentPacketCourse spc = uow.GetRepo<IStudentPacketCourseRepo>().Get(studentPacketCourseId.GetValueOrDefault(0));

                if (spc != null)
                {
                    Guid userId = spc.FPCSCourse.TeacherId.HasValue ?
                        spc.FPCSCourse.Teacher.DbUserId :
                        spc.FPCSCourse.Guardian.DbUserId;

                    model.IsCanEditILP = (User.Role != Role.Guardian ||
                                          (User.Role == Role.Guardian && userId == User.UserId)) &&
                                         (User.Role != Role.Teacher ||
                                          (User.Role == Role.Teacher && (userId == User.UserId || spc.StudentPacket.SponsorTeacherId == User.UserId)));

                    model.IsCanEditGoodService = (User.Role != Role.Guardian ||
                                          (User.Role == Role.Guardian && userId == User.UserId)) &&
                                         (User.Role != Role.Teacher ||
                                          (User.Role == Role.Teacher && (userId == User.UserId || spc.StudentPacket.SponsorTeacherId == User.UserId)));

                    model.TableGoodAndServiceModel.GoodsAndServices.AddRange(balances.Select(x => new GoodAndServiceModel(x)).ToList());
                    model.TableGoodAndServiceModel.GoodsAndServices.Add(new GoodAndServiceModel(GoodAndServiceHelper.GetInstructorFPCSCourse(spc)));

                    model.TableGoodAndServiceModel
                         .GoodsAndServices = model.TableGoodAndServiceModel
                                                  .GoodsAndServices
                                                  .Where(x => x.IsShow)
                                                  .OrderByDescending(x => x.IsInstructor)
                                                  .ThenBy(x => x.GoodAndServiceID)
                                                  .ThenBy(x => x.GoodAndServiceBalanceID)
                                                  .ToList();

                    model.SignModel = new SignModel(spc, "/StudentPacket/Index/" + spc.StudentPacket.StudentId.ToString(), Session.GetUser());

                    var repoAlerts = uow.GetRepo<IStudentPacketCourseAlertRepo>();

                    model.TableAlertModel.Alerts = repoAlerts
                                                        .GetAll()
                                                        .Where(x => x.StudentPacketCourseId == model.StudentPacketCourseId)
                                                        .ToList()
                                                        .Select(x => new AlertModel(x))
                                                        .ToList();
                }
                else
                {
                    FPCSCourse fpcsCourse = uow.GetRepo<IFPCSCourseRepo>().Get(courseId.GetValueOrDefault(0));

                    Guid userId = fpcsCourse.TeacherId.HasValue ?
                        fpcsCourse.Teacher.DbUserId :
                        fpcsCourse.Guardian.DbUserId;

                    model.IsCanEditILP = User.Role != Role.Guardian ||
                                        (User.Role == Role.Guardian && userId == User.UserId);

                    model.IsCanEditGoodService = false;
                }
            }

            return PartialView(model);
        }

        public ActionResult SendEmailEnrollmentList(String[] emails)
        {
            String to = String.Join(";", emails);
            String subject = String.Empty;
            String body = String.Empty;

            var href = new
            {
                To = to,
                Subject = subject,
                Body = body
            };

            return JsonRes(Status.OK, "Ok", href);
        }

        [HttpGet]
        public ActionResult _Sign(Int64 studentPacketCourseId, String returnHref)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                StudentPacketCourse spc = uow.GetRepo<IStudentPacketCourseRepo>().Get(studentPacketCourseId);

                SignModel model = new SignModel(spc, returnHref, Session.GetUser());

                return PartialView(model);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _Sign(SignModel model)
        {
            if (ModelState.IsValid)
            {
                using (var uow = UnityManager.Resolve<IUnitOfWork>())
                {
                    var repo = uow.GetRepo<IStudentPacketCourseRepo>();

                    StudentPacketCourse dbEntity = repo.Get(model.StudentPacketCourseId);

                    AccessActionModel access = AccessAction.IsNotLock(dbEntity.StudentPacket, User.Role);
                    if (!access.IsResolve)
                    {
                        throw new Exception(access.Message);
                    }

                    SessionUserModel currentUser = Session.GetUser();
                    //DbUser currentDbUser = uow.GetRepo<IDbUserRepo>().Get(currentUser.UserId);

                    Boolean isCanChangeGuardiansSign = dbEntity.StudentPacket.Student.Families.FirstOrDefault() != null ?
                                                        dbEntity.StudentPacket.Student.Families.FirstOrDefault().Guardians.Select(x => x.DbUserId).ToList().Contains(currentUser.UserId) :
                                                        false;
                    Boolean isCanChangeSponsorSign = dbEntity.StudentPacket.SponsorTeacherId == currentUser.UserId;
                    Boolean isCanChangeInstructorSign = dbEntity.FPCSCourse.TeacherId == currentUser.UserId;
                    Boolean isCanChangeAdminSign = currentUser.Role == Role.Admin;
                    Boolean isSponsorInstructor = dbEntity.StudentPacket.SponsorTeacherId.HasValue && dbEntity.FPCSCourse.TeacherId.HasValue && dbEntity.StudentPacket.SponsorTeacherId == dbEntity.FPCSCourse.TeacherId;
                    Boolean isGuardianInstructor = dbEntity.StudentPacket.Student.Families.FirstOrDefault() != null && dbEntity.FPCSCourse.GuardianId.HasValue ?
                        dbEntity.StudentPacket.Student.Families.FirstOrDefault().Guardians.Select(x => x.DbUserId).Contains(dbEntity.FPCSCourse.GuardianId.Value) :
                        false;

                    if (isCanChangeGuardiansSign)
                    {
                        dbEntity.DateGuardianSignature = DateTime.Now;
                        dbEntity.GuardianSignature = model.GuardiansSign;
                        dbEntity.GuardianId = currentUser.UserId;
                        dbEntity.GuardianComment = model.GuardiansComment;
                    }

                    if (isCanChangeSponsorSign)
                    {
                        dbEntity.DateSponsorSignature = DateTime.Now;
                        dbEntity.SponsorSignature = model.SponsorSign;
                        dbEntity.SponsorId = currentUser.UserId;
                        dbEntity.SponsorComment = model.SponsorComment;
                    }

                    if (isCanChangeInstructorSign || (isSponsorInstructor && isCanChangeSponsorSign))
                    {
                        dbEntity.DateInstructorSignature = DateTime.Now;

                        if (isSponsorInstructor)
                        {
                            dbEntity.InstructorSignature = model.SponsorSign;
                            dbEntity.InstructorComment = model.SponsorComment;
                        }
                        else
                        {
                            dbEntity.InstructorSignature = model.InstructorSign;
                            dbEntity.InstructorComment = model.InstructorComment;
                        }

                        dbEntity.InstructorId = currentUser.UserId;
                    }

                    if (isCanChangeAdminSign)
                    {
                        dbEntity.DateAdminSignature = DateTime.Now;
                        dbEntity.AdminSignature = model.AdminSign;
                        dbEntity.AdminId = currentUser.UserId;
                        dbEntity.AdminComment = model.AdminComment;
                    }

                    uow.Commit();

                    return JsonRes(model.ReturnHref);
                }
            }

            model.Init();
            return PartialView(model);
        }

        public ActionResult _PrintEnrollmentList(Int32 studentPacketCourseId)
        {
            PrintEnrollmentListModel model = new PrintEnrollmentListModel();
            
            model.EnrollmentListModel = new EnrollmentListModel(studentPacketCourseId);

            model.PrintAddress = (ConfigurationManager.AppSettings["PrintAddress"]).ToString();
            model.PrintState = (ConfigurationManager.AppSettings["PrintState"]).ToString();
            model.PrintPhone = (ConfigurationManager.AppSettings["PrintPhone"]).ToString();
            model.PrintFax = (ConfigurationManager.AppSettings["PrintFax"]).ToString();
            model.PrintDateNow = DateTime.Now.ToString("MM/dd/yyyy");

            return PartialView(model);
        }
    }
}
