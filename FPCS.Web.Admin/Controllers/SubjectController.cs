﻿using System;
using System.Linq;
using System.Web.Mvc;

using FPCS.Core.jqGrid;
using FPCS.Core.Extensions;

using FPCS.Data;
using FPCS.Data.Repo;
using FPCS.Data.Enums;
using FPCS.Data.Exceptions;
using FPCS.Web.Admin.Code;
using FPCS.Data.Entities;
using FPCS.Web.Admin.Models.Subject;

namespace FPCS.Web.Admin.Controllers
{
    public class SubjectController : BaseController
    {
        [FPCSAuthorize(Role.Admin)]
        public ActionResult Index()
        {
            return View();
        }

        [FPCSAuthorize(Role.Admin)]
        public JsonResult _Index(GridOptions options, SubjectListOptions coursesListOptions)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<ISubjectRepo>();
                var dbList = repo.GetAll();

                var engine = new GridDynamicEngine(options, coursesListOptions);
                var result = engine.ApplyAll(dbList, x => new SubjectMiniModel
                {
                    SubjectId = x.SubjectId,
                    Name = x.Name,
                    IsElective = x.IsElective ? "Elective" : "Core"
                });

                return Json(result);
            }
        }


        [HttpGet]
        [FPCSAuthorize(Role.Admin, Role.Teacher)]
        public PartialViewResult _Details(Int32 id)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<ISubjectRepo>();
                var dbEntity = repo.Get(id);
                if (dbEntity == null) return ErrorPartial("Subject {0} not found", id);

                var model = new SubjectDetailsModel
                {
                    SubjectId = dbEntity.SubjectId,
                    Name = dbEntity.Name,
                    IsElective = dbEntity.IsElective,
                    Description = dbEntity.Description
                };

                return PartialView(model);
            }
        }


        [HttpGet]
        [FPCSAuthorize(Role.Admin)]
        public PartialViewResult _Create()
        {
            var model = new SubjectCreateModel();
            model.Init();
            return PartialView(model);
        }

        [HttpPost]
        [FPCSAuthorize(Role.Admin)]
        [ValidateAntiForgeryToken]
        public ActionResult _Create(SubjectCreateModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var uow = UnityManager.Resolve<IUnitOfWork>())
                    {
                        var repo = uow.GetRepo<ISubjectRepo>();
                        Subject dbEntity = new Subject();

                        dbEntity.Name = model.Name.TrimAndReduce();
                        dbEntity.Description = model.Description.TrimAndReduce();
                        dbEntity.IsElective = model.IsElective;
                        dbEntity.IsDeleted = false;

                        uow.Commit();

                        return JsonRes(dbEntity.SubjectId.ToString());
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }
            }

            model.Init();
            return PartialView(model);
        }


        [HttpGet]
        [FPCSAuthorize(Role.Admin)]
        public PartialViewResult _Edit(Int32 id)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<ISubjectRepo>();
                var dbEntity = repo.Get(id);
                if (dbEntity == null) return ErrorPartial("Subject {0} not found", id);

                var model = new SubjectEditModel
                {
                    SubjectId = dbEntity.SubjectId,
                    Name = dbEntity.Name,
                    Description = dbEntity.Description,
                    IsElective = dbEntity.IsElective
                };

                model.Init();
                return PartialView(model);
            }
        }

        [HttpPost]
        [FPCSAuthorize(Role.Admin)]
        [ValidateAntiForgeryToken]
        public ActionResult _Edit(SubjectEditModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var uow = UnityManager.Resolve<IUnitOfWork>())
                    {
                        var repo = uow.GetRepo<ISubjectRepo>();

                        Subject dbEntity = repo.Get(model.SubjectId);

                        if (dbEntity == null) return ErrorPartial("Subject {0} not found", model.SubjectId);

                        dbEntity.Name = model.Name.TrimAndReduce();
                        dbEntity.Description = model.Description.TrimAndReduce();
                        dbEntity.IsElective = model.IsElective;
                        dbEntity.IsDeleted = false;

                        uow.Commit();

                        return JsonRes(dbEntity.SubjectId.ToString());
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }
            }

            model.Init();
            return PartialView(model);
        }


        [HttpPost]
        [FPCSAuthorize(Role.Admin)]
        public ActionResult Delete(Int32 id)
        {
            try
            {
                using (var uow = UnityManager.Resolve<IUnitOfWork>())
                {
                    var repo = uow.GetRepo<ISubjectRepo>();
                    repo.Remove(id);

                    uow.Commit();
                    return JsonRes();
                }
            }
            catch (Exception ex)
            {
                return JsonRes(Status.Error, ex.Message);
            }
        }

        [HttpPost]
        [FPCSAuthorize(Role.Admin)]
        public ActionResult DeleteAll(String id)
        {
            try
            {
                if (String.IsNullOrEmpty(id)) return JsonRes(Status.Error, "Not correct ids");

                var ids = id.Split(',');
                using (var uow = UnityManager.Resolve<IUnitOfWork>())
                {
                    Int32 errorsCount = 0;
                    var repo = uow.GetRepo<ISubjectRepo>();
                    foreach (var item in ids)
                    {
                        try
                        {
                            repo.Remove(Int32.Parse(item));
                        }
                        catch (Exception)
                        {
                            errorsCount++;
                        }
                    }

                    uow.Commit();
                    if (errorsCount == 0) return JsonRes(); 
                    else return JsonRes(Status.Error, "Were deleted not all entities");
                }
            }
            catch (Exception ex)
            {
                return JsonRes(Status.Error, ex.Message);
            }
        }
    }
}
