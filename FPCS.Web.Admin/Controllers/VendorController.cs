﻿using FPCS.Core.jqGrid;
using FPCS.Data;
using FPCS.Core.Extensions;
using FPCS.Data.Enums;
using FPCS.Data.Repo;
using FPCS.Web.Admin.Code;
using FPCS.Web.Admin.Models.Vendor;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.IO;
using FPCS.Web.Admin.Models.StoredFile;
using System.Threading.Tasks;
using System.Web.UI;
using FPCS.Data.Entities;

namespace FPCS.Web.Admin.Controllers
{
	public class VendorController : BaseController
	{
        [FPCSAuthorize(Role.Admin, Role.Guardian, Role.Teacher)]
		public ActionResult Index()
		{
			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				VendorIndexModel model = new VendorIndexModel();

				model.Services = String.Join(@";",
					uow.GetRepo<IServiceVendorRepo>()
						.GetAll()
						.Select(x => new {Type = x.Subject.Name, Name = x.Name})
						.ToList()
						.Select(x => String.Format(@"{0}- {1}:{0}- {1}", x.Type, x.Name))
						.ToList());

				return View(model);
			}
		}

        [FPCSAuthorize(Role.Admin, Role.Guardian, Role.Teacher)]
		public JsonResult _Index(GridOptions options, VendorListOptions vendorListOptions)
		{
			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				Int32? status = vendorListOptions.Status;
				Int32? type = vendorListOptions.VendorType;
				String services = vendorListOptions.Services != null ? vendorListOptions.Services.Replace('-', ':') : null;

				var repo = uow.GetRepo<IVendorRepo>();
				var dbList = repo.GetAll(Session.GetSchoolYear().SchoolYearId)
					.Where(x => (status != null && status.Value == 10 && x.Status == VendorStatus.Approved) ||
					            (status != null && status.Value == 20 && x.Status == VendorStatus.Pending) ||
					            (status != null && status.Value == 30 && x.Status == VendorStatus.Rejected) ||
					            (status != null && status.Value == 40 && x.Status == VendorStatus.Remove) ||
					            (status != null && status.Value == 50 && (x.Status == VendorStatus.Approved ||
					                                                      x.Status == VendorStatus.Pending ||
					                                                      x.Status == VendorStatus.Remove)) ||
					            (status == null || status == -1))
					.Where(x => (type != null && type.Value == 10 && !x.IsNonProfit) ||
					            (type != null && type.Value == 20 && x.IsNonProfit) ||
					            (type == null || type == -1))
					.Select(x => new
					{
						VendorId = x.VendorID,
						IsNonProfit = x.IsNonProfit ? 10 : 20,
						Name = x.BusinessName,
						ContactName = x.LastName + ", " + x.FirstName,
						Status = x.Status,
						StreetAddress = x.StreetAddress,
						StreetAddressCity = x.StreetAddressCity,
						StreetAddressStateName = x.StreetAddressState.Name,
						Phone = x.Phone,
						Email = x.Email,
						Website = x.BusinessWebsite,
                        FileStoreId = x.FileStoreId,
						Services = x.Services
							.Where(t => t.IsActive)
                                .Select(t => t.Subject.Name + ": " + t.Name)
					})
					.Where(x => (services != null && services != String.Empty && x.Services.Contains(services)) ||
					            (services == null) ||
					            (services == String.Empty));

				vendorListOptions.Status = null;
				vendorListOptions.VendorType = null;
				vendorListOptions.Services = null;

				var engine = new GridDynamicEngine(options, vendorListOptions);
				var result = engine.ApplyAll2(dbList, x => new VendorMiniModel()
				{
					VendorId = x.VendorId,
					VendorType = x.IsNonProfit == 10 ? "Non-profit" : "For-profit",
					Name = x.Name,
					ContactName = x.ContactName,
					Status = x.Status.HasValue ? x.Status.Value.GetDescription() : "",
					Address = String.Format(@"{0} {1} {2}", x.StreetAddress, x.StreetAddressCity, x.StreetAddressStateName),
					Phone = x.Phone,
					Email = x.Email,
					Website = x.Website,
					Services = String.Join(@"<br />", x.Services),
                        FileStoreId = x.FileStoreId
				});

				return Json(result);
			}
		}

		[FPCSAuthorize(Role.Admin)]
		[HttpGet]
		public ActionResult Create()
		{
			var model = new VendorCreateModel();
			model.Init();
			model.Services = new List<LightServiceModel>();
			return PartialView("_Create", model);
		}

		[FPCSAuthorize(Role.Admin)]
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create(VendorCreateModel model)
		{
			if (model.Services == null)
			{
				model.Services = new List<LightServiceModel>();
			}

			if (ModelState.IsValid)
			{
				try
				{
					using (var uow = UnityManager.Resolve<IUnitOfWork>())
					{
						var repo = uow.GetRepo<IVendorRepo>();
						var vendor = repo.Add(
							VendorType.Both,
							model.BusinessName.TrimAndReduce(),
							model.FirstName.TrimAndReduce(),
							model.LastName.TrimAndReduce(),
							model.Status,
							model.StatusComments.TrimAndReduce(),
							model.StreetAddress.TrimAndReduce(),
							model.StreetAddressCity.TrimAndReduce(),
							model.StreetAddressStateId,
							model.StreetAddressZipCode.TrimAndReduce(),
							model.MailingAddress.TrimAndReduce(),
							model.MailingAddressCity.TrimAndReduce(),
							model.MailingAddressStateId,
							model.MailingAddressZipCode.TrimAndReduce(),
							model.Phone.TrimAndReduce(),
							model.Fax.TrimAndReduce(),
							model.Email.TrimAndReduce(),
							model.Location,
							model.BusinessWebsite.TrimAndReduce(),
							model.EmployerIdentification.TrimAndReduce(),
							model.AKBusinessLicense.TrimAndReduce(),
							model.LicenseExpiration,
							model.InsuranceExpiration,
							model.TrainingEducationExperience.TrimAndReduce(),
							model.CommentsAboutServices.TrimAndReduce(),
							model.VendorUnitType,
							model.OtherChargeMethod.TrimAndReduce(),
							model.Price,
							model.ContractStartingDate,
							model.IsNonProfit,
							model.IsMisdemeanorOrFelony,
							model.IsBackgroundCheckFingerprinting,
							model.IsHaveChildrenCurrentlyEnrolled,
							model.IsRetiredCertificatedASDTeacher,
							model.IsASDEmployeeEligibleHireList,
							model.IsCurrentlyAvailableProvideServices,
							model.IsProvideOnlineCurriculumServices,
							model.IsCourseProvider,
							model.CostCourseProvider,
							Session.GetSchoolYear().SchoolYearId);

						vendor = repo.UpdateServices(vendor, model.Services.Select(x => x.ServiceId));

						uow.Commit();

						return
							JsonRes(
								new {id = vendor.VendorID.ToString(), text = String.Format("{0}, {1}", vendor.LastName, vendor.FirstName)});
					}
				}
				catch (Exception ex)
				{
					ModelState.AddModelError("", ex.Message);
				}
			}

			model.Init();
			return PartialView("_Create", model);
		}

		[FPCSAuthorize(Role.Admin)]
		[HttpGet]
		public ActionResult Edit(Int64 id)
		{
			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				var model = uow.GetRepo<IVendorRepo>().GetAsQuery(id)
					.Select(x => new VendorEditModel
					{
						VendorId = x.VendorID,
						BusinessName = x.BusinessName,
						FirstName = x.FirstName,
						LastName = x.LastName,
						Status = x.Status,
						StatusComments = x.StatusComments,
						StreetAddress = x.StreetAddress,
						StreetAddressCity = x.StreetAddressCity,
						StreetAddressStateId = x.StreetAddressStateId,
						StreetAddressZipCode = x.StreetAddressZipCode,
						MailingAddress = x.MailingAddress,
						MailingAddressCity = x.MailingAddressCity,
						MailingAddressStateId = x.MailingAddressStateId,
						MailingAddressZipCode = x.MailingAddressZipCode,
						Phone = x.Phone,
						Fax = x.Fax,
						Email = x.Email,
						Location = x.Location,
						BusinessWebsite = x.BusinessWebsite,
						EmployerIdentification = x.EmployerIdentification,
						AKBusinessLicense = x.AKBusinessLicense,
						LicenseExpiration = x.LicenseExpiration,
						InsuranceExpiration = x.InsuranceExpiration,
						TrainingEducationExperience = x.TrainingEducationExperience,
						CommentsAboutServices = x.CommentsAboutServices,
						VendorUnitType = x.VendorUnitType,
						OtherChargeMethod = x.OtherChargeMethod,
						Price = x.Price,
						ContractStartingDate = x.ContractStartingDate,
						IsNonProfit = x.IsNonProfit,
						IsMisdemeanorOrFelony = x.IsMisdemeanorOrFelony,
						IsBackgroundCheckFingerprinting = x.IsBackgroundCheckFingerprinting,
						IsHaveChildrenCurrentlyEnrolled = x.IsHaveChildrenCurrentlyEnrolled,
						IsRetiredCertificatedASDTeacher = x.IsRetiredCertificatedASDTeacher,
						IsASDEmployeeEligibleHireList = x.IsASDEmployeeEligibleHireList,
						IsCurrentlyAvailableProvideServices = x.IsCurrentlyAvailableProvideServices,
						IsProvideOnlineCurriculumServices = x.IsProvideOnlineCurriculumServices,
						IsCourseProvider = x.IsCourseProvider,
						CostCourseProvider = x.CostCourseProvider,
						Services = x.Services
							.Select(x2 => new LightServiceModel {ServiceId = x2.ServiceVendorId, Name = x2.Name, Type = x2.Subject.Name})
					}).FirstOrDefault();

				if (model == null) return Error("Vendor {0} not found", id);

				model.Init();
				return PartialView("_Edit", model);
			}
		}

		[FPCSAuthorize(Role.Admin)]
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit(VendorEditModel model)
		{
			if (model.Services == null)
			{
				model.Services = new List<LightServiceModel>();
			}

			if (ModelState.IsValid)
			{
				try
				{
					using (var uow = UnityManager.Resolve<IUnitOfWork>())
					{
						var repo = uow.GetRepo<IVendorRepo>();
						var vendor = repo.Update(
							model.VendorId,
							VendorType.Both,
							model.BusinessName.TrimAndReduce(),
							model.FirstName.TrimAndReduce(),
							model.LastName.TrimAndReduce(),
							model.Status,
							model.StatusComments.TrimAndReduce(),
							model.StreetAddress.TrimAndReduce(),
							model.StreetAddressCity.TrimAndReduce(),
							model.StreetAddressStateId,
							model.StreetAddressZipCode.TrimAndReduce(),
							model.MailingAddress.TrimAndReduce(),
							model.MailingAddressCity.TrimAndReduce(),
							model.MailingAddressStateId,
							model.MailingAddressZipCode.TrimAndReduce(),
							model.Phone.TrimAndReduce(),
							model.Fax.TrimAndReduce(),
							model.Email.TrimAndReduce(),
							model.Location,
							model.BusinessWebsite.TrimAndReduce(),
							model.EmployerIdentification.TrimAndReduce(),
							model.AKBusinessLicense.TrimAndReduce(),
							model.LicenseExpiration,
							model.InsuranceExpiration,
							model.TrainingEducationExperience.TrimAndReduce(),
							model.CommentsAboutServices.TrimAndReduce(),
							model.VendorUnitType,
							model.OtherChargeMethod.TrimAndReduce(),
							model.Price,
							model.ContractStartingDate,
							model.IsNonProfit,
							model.IsMisdemeanorOrFelony,
							model.IsBackgroundCheckFingerprinting,
							model.IsHaveChildrenCurrentlyEnrolled,
							model.IsRetiredCertificatedASDTeacher,
							model.IsASDEmployeeEligibleHireList,
							model.IsCurrentlyAvailableProvideServices,
							model.IsProvideOnlineCurriculumServices,
							model.IsCourseProvider,
							model.CostCourseProvider,
							Session.GetSchoolYear().SchoolYearId);

						vendor = repo.UpdateServices(vendor, model.Services.Select(x => x.ServiceId));
						uow.Commit();

						return
							JsonRes(
								new {id = vendor.VendorID.ToString(), text = String.Format("{0}, {1}", vendor.LastName, vendor.FirstName)});
					}
				}
				catch (Exception ex)
				{
					ModelState.AddModelError("", ex.Message);
				}
			}

			model.Init();
			return PartialView("_Edit", model);
		}

		[FPCSAuthorize(Role.Admin)]
		[HttpGet]
		public PartialViewResult _VendorServicePartial(Int32 id)
		{
			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				var dbEntity = uow.GetRepo<IServiceVendorRepo>().Get(id);
				if (dbEntity == null) return ErrorPartial("Guardian {0} not found", id);

				var dbSubject = uow.GetRepo<ISubjectRepo>().Get(dbEntity.SubjectId);

				var model = new LightServiceModel
				{
					ServiceId = dbEntity.ServiceVendorId,
					Name = dbSubject.Name + ":" + dbEntity.Name
				};
				return PartialView(model);
			}
		}

		[FPCSAuthorize(Role.Admin)]
		[HttpPost]
		public ActionResult Delete(Int32 id)
		{
			try
			{
				using (var uow = UnityManager.Resolve<IUnitOfWork>())
				{
					var repo = uow.GetRepo<IVendorRepo>();
					repo.Remove(id);

					uow.Commit();
					return JsonRes();
				}
			}
			catch (Exception ex)
			{
				return JsonRes(Status.Error, ex.Message);
			}
		}

		[FPCSAuthorize(Role.Admin)]
		[HttpPost]
		public ActionResult DeleteAll(String id)
		{
			try
			{
				if (String.IsNullOrEmpty(id)) return JsonRes(Status.Error, "Not correct ids");

				var ids = id.Split(',');
				using (var uow = UnityManager.Resolve<IUnitOfWork>())
				{
					Int32 errorsCount = 0;
					var repo = uow.GetRepo<IVendorRepo>();
					foreach (var item in ids)
					{
						try
						{
							repo.Remove(Int64.Parse(item));
						}
						catch (Exception)
						{
							errorsCount++;
						}
					}

					uow.Commit();
					if (errorsCount == 0) return JsonRes();
					else return JsonRes(Status.Error, "Were deleted not all entities");
				}
			}
			catch (Exception ex)
			{
				return JsonRes(Status.Error, ex.Message);
			}
		}

        [FPCSAuthorize(Role.Admin)]
        [HttpGet]
        public ActionResult Print(Int64 id)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IGoodServiceRepo>();
                var repoFamily = uow.GetRepo<IFamilyRepo>();
                var repoVendor = uow.GetRepo<IVendorRepo>();
                var repoBalance = uow.GetRepo<IGoodServiceBalanceRepo>();

                var data = repo.GetByVendorHasStudentPacketCourse(id)
                    .ToList()
                    .Select(x => new GoodServiceVendorPrintModel()
                    {
                        Title = String.Format("{0} #{1}: {2}", x.RequisitionOrReimbursement.GetDescription(), x.GoodServiceId.ToString(), x.Title),
                        Name = x.StudentPacketCourse.StudentPacket.Student.LastName + ", " + x.StudentPacketCourse.StudentPacket.Student.FirstName,
                        GuardianName = x.StudentPacketCourse.StudentPacket.Student.LastName + " " + String.Join(", ", repoFamily.GetGuardiansByStudent(x.StudentPacketCourse.StudentPacket.StudentId).Select(t => t.FirstName)),
                        Email = repoFamily.GetFamiliesByStudent(x.StudentPacketCourse.StudentPacket.StudentId).FirstOrDefault() != null ?
                            repoFamily.GetFamiliesByStudent(x.StudentPacketCourse.StudentPacket.StudentId).FirstOrDefault().Email : String.Empty,
                        Phone = repoFamily.GetFamiliesByStudent(x.StudentPacketCourse.StudentPacket.StudentId).FirstOrDefault() != null ?
                            repoFamily.GetFamiliesByStudent(x.StudentPacketCourse.StudentPacket.StudentId).FirstOrDefault().CellPhone : String.Empty,
                        SponsorTeacher = x.StudentPacketCourse.StudentPacket.SponsorTeacher.LastName + ", " + x.StudentPacketCourse.StudentPacket.SponsorTeacher.FirstName,
                        CourseName = x.StudentPacketCourse.FPCSCourse.Name,
                        Budget = x.NumberOfUnits * x.UnitPrice + x.ShippingHandlingFees,
                        Balance = repoBalance.GetByGoodService(x.GoodServiceId).ToList().Sum(t => t.UnitPrice * t.QTY + t.Shipping)
                    })
                    .ToList();

                var model = new TableGoodServiceVendorPrintModel();
                model.Data = data;
                model.VendorName = repoVendor.Get(id).BusinessName;
                model.PrintAddress = (ConfigurationManager.AppSettings["PrintAddress"]);
                model.PrintState = (ConfigurationManager.AppSettings["PrintState"]);
                model.PrintPhone = (ConfigurationManager.AppSettings["PrintPhone"]);
                model.PrintFax = (ConfigurationManager.AppSettings["PrintFax"]);
                model.PrintDateNow = DateTime.Now.ToString("MM/dd/yyyy");

                return PartialView("_Print", model);
            }
        }

		[FPCSAuthorize(Role.Admin)]
		[HttpGet]
        public ActionResult LoadFile(Int64 id)
		{
            var model = id;
            return PartialView("_VendorFileLoad", model);
        }

        [FPCSAuthorize(Role.Admin)]
        [HttpPost]
        public ActionResult DeleteFile(Int64 fileStoreId, Int64 vendorId)
        {
            try
            {
			    using (var uow = UnityManager.Resolve<IUnitOfWork>())
			    {
				    var repoVendor = uow.GetRepo<IVendorRepo>();
                    var vendor = repoVendor.Get(vendorId);
                    vendor.FileStoreId = null;
                    repoVendor.Update(vendor);
                    uow.Commit();

                    var repo = uow.GetRepo<IFileStoreRepo>();
			        var file = repo.Get(fileStoreId);

			        DeleteFile("vendor", file.URL);
                    repo.Remove(fileStoreId);

                    uow.Commit();
                    return JsonRes();
                }
            }
            catch (Exception ex)
            {
                return JsonRes(Status.Error, ex.Message);
            }
        }

        [FPCSAuthorize(Role.Admin)]
        [HttpPost]
        public ActionResult SaveVendorFile(Int64 vendorId)
		{
            var file = Request.Files[0];
            if (file != null && file.ContentLength > 0 && file.ContentType.Contains("pdf"))
            {
                var serverFile = SavePostedFile(file, "Vendor");
                using (var uow = UnityManager.Resolve<IUnitOfWork>())
                {
                    var repoFileStore = uow.GetRepo<IFileStoreRepo>();
                    var fileStore = repoFileStore.Add(
                        serverFile.Name,
                        serverFile.URL, 
                        serverFile.ContentSize, 
                        serverFile.DateCreated, 
                        serverFile.MimeType);
                    uow.Commit();

                    var repoVendor = uow.GetRepo<IVendorRepo>();
                    var vendor = repoVendor.Get(vendorId);
                    vendor.FileStoreId = fileStore.FileStoreId;
                    repoVendor.Update(vendor);
                    uow.Commit();
                }
            }
			
            return RedirectToAction("Index");
        }

        /// <remarks>Данный метод вызывает SubmitChanges так что нужно использовать его с осторожностью</remarks>
        /// <summary>
        /// Сохраняет указанный файд в сервером хранилище файлов
        /// </summary>
        /// <param name="postedFile">Отправленный на сервер файл</param>
        /// <param name="folder">Опционально - подпапка в которой сохранить</param>
        /// <returns>Объект сохраненного файла</returns>
        private AddFileModel SavePostedFile(HttpPostedFileBase postedFile, string folder = "")
        {
            var folderPath = folder;
            folder = folder.ToLower();
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse((ConfigurationManager.AppSettings["StorageConnectionString"]).ToString());
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            CloudBlobContainer container = null;

            // ФИзически сохраняем файл на сервере
            //var filesPath = ConfigurationManager.AppSettings["FilesStoragePath"];
            var ext = Path.GetExtension(postedFile.FileName);
            var serverFilename = string.Format("{0}{1}", Path.GetRandomFileName(), ext);
            serverFilename = Path.ChangeExtension(serverFilename, ".pdf");
            if (!String.IsNullOrEmpty(folder))
            {
                //serverFilename = String.Format("{0}\\{1}", folder, serverFilename);
                container = blobClient.GetContainerReference(folder);
                container.CreateIfNotExists();
            }

            CloudBlockBlob blockBlob = container.GetBlockBlobReference(serverFilename);

            var stream = postedFile.InputStream;

            using (var memoryStream = new MemoryStream())
            {
                stream.CopyTo(memoryStream);
                var bytes = memoryStream.ToArray();

                blockBlob.UploadFromByteArray(bytes, 0, bytes.Length);
            };
            
            var storedFile = new AddFileModel()
            {
                ContentSize = postedFile.InputStream.Length,
                DateCreated = DateTime.Now,
                MimeType = postedFile.ContentType,
                Name = postedFile.FileName,
                URL = String.Format(serverFilename)
            };

            return storedFile;
        }

        private void DeleteFile(string folder, string uri)
        {
            // Retrieve storage account from connection string.
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse((ConfigurationManager.AppSettings["StorageConnectionString"]).ToString());

            // Create the blob client.
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            // Retrieve reference to a previously created container.
            CloudBlobContainer container = blobClient.GetContainerReference(folder);

            // Retrieve reference to a blob named "myblob.txt".
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(uri);

            // Delete the blob.
            blockBlob.DeleteIfExists();
        }

        [FPCSAuthorize(Role.Admin)]
        [HttpPost]
        public ActionResult DownloadFileFromBlob(Int64 fileStoreId)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repoFileStore = uow.GetRepo<IFileStoreRepo>();
                var file = repoFileStore.Get(fileStoreId);
                CloudStorageAccount account =
                    CloudStorageAccount.Parse((ConfigurationManager.AppSettings["StorageConnectionString"]).ToString());
                CloudBlobClient blobClient = account.CreateCloudBlobClient();
                CloudBlobContainer container = blobClient.GetContainerReference("vendor");
                CloudBlob blob = container.GetBlobReference(file.URL);
                MemoryStream memStream = new MemoryStream();
                blob.DownloadToStream(memStream);

                byte[] pdfBytes = memStream.ToArray();
                String guid = Guid.NewGuid().ToString();
                Session[guid] = pdfBytes;

                var printModel = new PrintFileStoreModel()
                {
                    Guid = guid,
                    Name = file.Name
                };

                return Json(printModel, JsonRequestBehavior.AllowGet);
            }
        }

        [FPCSAuthorize(Role.Admin)]
        [HttpGet]
        public void ShowFileFromBlob(Int64 vendorId)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IVendorRepo>();
                var model = repo.GetVendorFileStoreId(vendorId);
                if (model.HasValue)
                {
                    var repoFileStore = uow.GetRepo<IFileStoreRepo>();
                    var file = repoFileStore.Get(model.Value);
                    CloudStorageAccount account =
                        CloudStorageAccount.Parse((ConfigurationManager.AppSettings["StorageConnectionString"]).ToString());
                    CloudBlobClient blobClient = account.CreateCloudBlobClient();
                    CloudBlobContainer container = blobClient.GetContainerReference("vendor");

                    try
                    {
                        CloudBlob blob = container.GetBlobReference(file.URL);
                        MemoryStream memStream = new MemoryStream();
                        blob.DownloadToStream(memStream);
                        byte[] pdfBytes = memStream.ToArray();
                        Response.Clear();
                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.ContentType = "application/pdf";
                        Response.AddHeader("content-length", pdfBytes.Length.ToString());
                        Response.BinaryWrite(pdfBytes);
                        Response.Flush();
                        Response.End();
                    }
                    catch
                    {
                        
                    }
                }
                
            }
        }
	}
}