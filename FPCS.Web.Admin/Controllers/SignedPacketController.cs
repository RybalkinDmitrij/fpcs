﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using FPCS.Core.Extensions;
using FPCS.Core.jqGrid;
using FPCS.Data;
using FPCS.Data.Enums;
using FPCS.Data.Repo;
using FPCS.Web.Admin.Code;
using FPCS.Web.Admin.Models.SignedPacket;
using System;
using System.Web.Script.Serialization;
using System.Configuration;

namespace FPCS.Web.Admin.Controllers
{
	public class SignedPacketController : BaseController
	{
		public ActionResult Index()
		{
			return View();
		}

		public JsonResult _Index(GridOptions options, SignedPacketListOptions listOptions)
		{
			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				var dbList = uow.GetRepo<IStudentPacketRepo>()
						.GetSignedPackets(SchoolYear.SchoolYearId)
						.Select(x => new
						{
							StudentGuid = x.Student.DbUserId,
							x.StudentPacketId,
							StudentLastName = x.Student.LastName,
							StudentFirstName = x.Student.FirstName,
							x.Student.Grade,
							HomePhone = x.Student.Families.FirstOrDefault() != null
								? x.Student.Families.FirstOrDefault().Telephone
								: string.Empty,
							EmailFamily = x.Student.Families.FirstOrDefault() != null
								? x.Student.Families.FirstOrDefault().Email
								: string.Empty,
                            DateGuardianSignature = x.DateGuardianSignatureSem1,
                            DateSponsorSignature = x.DateSponsorSignatureSem1,
                            IsWithdrawal = x.Student.WithdrawalDate.HasValue ? true : false
						})
						.ToList()
						.Select(x => new SignedPacketMiniModel
						{
							StudentGuid = x.StudentGuid,
							StudentPacketId = x.StudentPacketId,
							StudentName = string.Format(@"{0}, {1}", x.StudentLastName, x.StudentFirstName),
							Grade = x.Grade.GetDescription(),
							HomePhone = x.HomePhone,
							EmailFamily = x.EmailFamily,
                            DateSponsorSignature = x.DateSponsorSignature.HasValue ? x.DateSponsorSignature.Value.ToString("MM/dd/yyyy") : String.Empty,
                            DateGuardianSignature = x.DateGuardianSignature.HasValue ? x.DateGuardianSignature.Value.ToString("MM/dd/yyyy") : String.Empty,
                            IsWithdrawal = x.IsWithdrawal
						})
						.OrderBy(x => x.StudentName)
						.ToList();
				
				var engine = new GridDynamicEngine(options, listOptions);
				var result = engine.ApplyAll(dbList.AsQueryable());
				return Json(result);
			}
		}

        [HttpPost]
        public ActionResult _Print(String paramsIds, Int64? studentPacketId, GridOptions options, SignedPacketListOptions listOptions)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                Guid[] ids;
                if (!String.IsNullOrEmpty(paramsIds))
                {
                    ids = new JavaScriptSerializer().Deserialize<Guid[]>(paramsIds);
                }
                else
                {
                    Int64 _studentPacketId = studentPacketId.GetValueOrDefault(0);
                    ids = uow.GetRepo<IStudentPacketRepo>().GetAsQuery(_studentPacketId).Select(x => x.Student.DbUserId).ToArray();
                }

                List<SignedPacketMiniModel> data = GetData(uow, options, listOptions)
                    .Where(x => ids.Contains(x.StudentGuid))
                    .ToList();

                SignedPacketPrintModel model = new SignedPacketPrintModel();
                model.Data = data;

                model.PrintAddress = (ConfigurationManager.AppSettings["PrintAddress"]).ToString();
                model.PrintState = (ConfigurationManager.AppSettings["PrintState"]).ToString();
                model.PrintPhone = (ConfigurationManager.AppSettings["PrintPhone"]).ToString();
                model.PrintFax = (ConfigurationManager.AppSettings["PrintFax"]).ToString();
                model.PrintDateNow = DateTime.Now.ToString("MM/dd/yyyy");

                return PartialView(model);
            }
        }

        private IEnumerable<SignedPacketMiniModel> GetData(IUnitOfWork uow, GridOptions options,
            SignedPacketListOptions listOptions)
        {
            Int32 currentSchoolYearId = Session.GetSchoolYear().SchoolYearId;

            var repo = uow.GetRepo<IStudentPacketRepo>();

            var dbList = uow.GetRepo<IStudentPacketRepo>()
                        .GetSignedPackets(SchoolYear.SchoolYearId)
                        .Select(x => new
                        {
                            StudentGuid = x.Student.DbUserId,
                            x.StudentPacketId,
                            StudentLastName = x.Student.LastName,
                            StudentFirstName = x.Student.FirstName,
                            x.Student.Grade,
                            x.Student.DateOfBirth,
                            HomePhone = x.Student.Families.FirstOrDefault() != null
                                ? x.Student.Families.FirstOrDefault().Telephone
                                : string.Empty,
                            EmailFamily = x.Student.Families.FirstOrDefault() != null
                                ? x.Student.Families.FirstOrDefault().Email
                                : string.Empty,
                            DateGuardianSignature = x.DateGuardianSignatureSem1,
                            DateSponsorSignature = x.DateSponsorSignatureSem1,
                            IsWithdrawl = x.Student.WithdrawalDate.HasValue ? true : false
                        })
                        .ToList()
                        .Select(x => new SignedPacketMiniModel
                        {
                            StudentGuid = x.StudentGuid,
                            StudentPacketId = x.StudentPacketId,
                            StudentName = string.Format(@"{0}, {1}", x.StudentLastName, x.StudentFirstName),
                            Grade = x.Grade.GetDescription(),
                            DateOfBirth = x.DateOfBirth != DateTimeOffset.MinValue ? x.DateOfBirth.ToString("yyyy/MM/dd") : "",
                            HomePhone = x.HomePhone,
                            EmailFamily = x.EmailFamily,
                            DateSponsorSignature = x.DateSponsorSignature.HasValue ? x.DateSponsorSignature.Value.ToString("MM/dd/yyyy") : String.Empty,
                            DateGuardianSignature = x.DateGuardianSignature.HasValue ? x.DateGuardianSignature.Value.ToString("MM/dd/yyyy") : String.Empty,
                            IsWithdrawal = x.IsWithdrawl
                        });

            return dbList;
        }
	}
}