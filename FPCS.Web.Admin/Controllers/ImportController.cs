﻿using Imp = FPCS.Import.FromExcel.Handlers;
using ImpState = FPCS.Import.State;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FPCS.Import.Enum;
using FPCS.Web.Admin.Code;
using FPCS.Web.Admin.Models.Import;
using FPCS.Import.BetweenYears.Handlers;
using FPCS.Import.BetweenYears.Models.Years;
using FPCS.Data;
using FPCS.Data.Repo;

namespace FPCS.Web.Admin.Controllers
{
    public class ImportController : BaseController
    {
        public ActionResult Index()
        {
            ImportDetailsModel model = new ImportDetailsModel();
            model.Init();

            return View(model);
        }

        public ActionResult Ready(String message, String title, Int32 countInsert, Int32 countUpdate, Int32 countDelete)
        {
            return View(new AnswerASDCourseModel(message, title, countInsert, countUpdate, countDelete));
        }

        [HttpPost]
        public ActionResult UploadASDCourses(HttpPostedFileBase file)
        {
            String message = String.Empty;

            ImpState.Status answer = new ImpState.Status();

            if (file != null && file.ContentLength > 0 && Path.GetExtension(file.FileName) == ".xlsx")
            {
                answer = Imp.ASDCourseHandler.ImportData(file.InputStream, Session.GetSchoolYear().SchoolYearId);
            }
            else
            {
                answer.StateImport = StateImport.Failed;
                answer.Message = "File not found or format of extension isn't xlsx";
            }

            return RedirectToAction("Ready",
                new 
                {
                    message = answer.Message,
                    title = answer.Title,
                    countInsert = answer.CountInsert,
                    countUpdate = answer.CountUpdate,
                    countDelete = answer.CountDelete
                });
        }

        public FileResult SampleASDCourses()
        {
            return File(Url.Content("/Content/sample_import/Elementary.xlsx"), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "SampleASDCourses");
        }

        public ActionResult ReadyYears(String message, String title, Int32 countStudent, Int32 countTeacher, Int32 countGuardian, Int32 countFamily, Int32 countASDCourse, Int32 countVendor)
        {
            return View(new YearsDetailModel(message, title, countStudent, countTeacher, countGuardian, countFamily, countASDCourse, countVendor));
        }

        [HttpPost]
        public ActionResult BetweenYears(Int32? fromSchoolYearId, Int32? toSchoolYearId)
        {
            String message = String.Empty;

            if (!fromSchoolYearId.HasValue || !toSchoolYearId.HasValue)
            {
                return JsonRes(
                new
                {
                    message = "School year is undefined",
                    title = "Error",
                    countStudent = 0,
                    countTeacher = 0,
                    countGuardian = 0,
                    countFamily = 0,
                    countASDCourse = 0,
                    countVendor = 0
                });
            }

            if (fromSchoolYearId.Value == toSchoolYearId.Value)
            {
                return JsonRes(
                new
                {
                    message = "From year equal to year!",
                    title = "Error",
                    countStudent = 0,
                    countTeacher = 0,
                    countGuardian = 0,
                    countFamily = 0,
                    countASDCourse = 0,
                    countVendor = 0
                });
            }

            YearsModel result = YearsHandler.Import(fromSchoolYearId.Value, toSchoolYearId.Value);

            return JsonRes(
                new
                {
                    message = "Success import data between years",
                    title = "Success import data between years",
                    countStudent = result.CountStudent,
                    countTeacher = result.CountTeacher,
                    countGuardian = result.CountGuardian,
                    countFamily = result.CountFamily,
                    countASDCourse = result.CountASDCourse,
                    countVendor = result.CountVendor
                });
        }

        //[HttpPost]
        //public ActionResult UploadStudents(HttpPostedFileBase file)
        //{
        //    String message = String.Empty;

        //    ImpState.Status answer = new ImpState.Status();

        //    if (file != null && file.ContentLength > 0 && Path.GetExtension(file.FileName) == ".xlsx")
        //    {
        //        Imp.StudentHandler.ImportData(file.InputStream, Session.GetSchoolYear().SchoolYearId);
        //    }
        //    else
        //    {
        //        answer.StateImport = StateImport.Failed;
        //        answer.Message = "File not found or format of extension isn't xlsx";
        //    }

        //    return RedirectToAction("Ready",
        //        new
        //        {
        //            message = answer.Message,
        //            title = answer.Title,
        //            countInsert = answer.CountInsert,
        //            countUpdate = answer.CountUpdate,
        //            countDelete = answer.CountDelete
        //        });
        //}

        //[HttpPost]
        //public ActionResult UploadVendors(HttpPostedFileBase file)
        //{
        //    String message = String.Empty;

        //    ImpState.Status answer = new ImpState.Status();

        //    if (file != null && file.ContentLength > 0 && Path.GetExtension(file.FileName) == ".xlsx")
        //    {
        //        Imp.VendorHandler.ImportData(file.InputStream, Session.GetSchoolYear().SchoolYearId);
        //    }
        //    else
        //    {
        //        answer.StateImport = StateImport.Failed;
        //        answer.Message = "File not found or format of extension isn't xlsx";
        //    }

        //    return RedirectToAction("Ready",
        //        new
        //        {
        //            message = answer.Message,
        //            title = answer.Title,
        //            countInsert = answer.CountInsert,
        //            countUpdate = answer.CountUpdate,
        //            countDelete = answer.CountDelete
        //        });
        //}

        [HttpGet]
        public ActionResult UploadVendors2()
        {
            Imp.VendorHandler.ImportData3(2015);
            
            return RedirectToAction("Ready",
                new
                {
                    message = "",
                    title = "",
                    countInsert = 0,
                    countUpdate = 0,
                    countDelete = 0
                });
        }

        //[HttpPost]
        //public ActionResult UploadVendorServices(HttpPostedFileBase file)
        //{
        //    String message = String.Empty;

        //    ImpState.Status answer = new ImpState.Status();

        //    if (file != null && file.ContentLength > 0 && Path.GetExtension(file.FileName) == ".xlsx")
        //    {
        //        Imp.VendorServiceHandler.ImportData(file.InputStream, Session.GetSchoolYear().SchoolYearId);
        //    }
        //    else
        //    {
        //        answer.StateImport = StateImport.Failed;
        //        answer.Message = "File not found or format of extension isn't xlsx";
        //    }

        //    return RedirectToAction("Ready",
        //        new
        //        {
        //            message = answer.Message,
        //            title = answer.Title,
        //            countInsert = answer.CountInsert,
        //            countUpdate = answer.CountUpdate,
        //            countDelete = answer.CountDelete
        //        });
        //}

        //[HttpPost]
        //public ActionResult UploadVendorServices2(HttpPostedFileBase file)
        //{
        //    String message = String.Empty;

        //    ImpState.Status answer = new ImpState.Status();

        //    if (file != null && file.ContentLength > 0 && Path.GetExtension(file.FileName) == ".xlsx")
        //    {
        //        Imp.VendorService2Handler.ImportData(file.InputStream, Session.GetSchoolYear().SchoolYearId);
        //    }
        //    else
        //    {
        //        answer.StateImport = StateImport.Failed;
        //        answer.Message = "File not found or format of extension isn't xlsx";
        //    }

        //    return RedirectToAction("Ready",
        //        new
        //        {
        //            message = answer.Message,
        //            title = answer.Title,
        //            countInsert = answer.CountInsert,
        //            countUpdate = answer.CountUpdate,
        //            countDelete = answer.CountDelete
        //        });
        //}

        //[HttpPost]
        //public ActionResult UploadTeachers(HttpPostedFileBase file)
        //{
        //    String message = String.Empty;

        //    ImpState.Status answer = new ImpState.Status();

        //    if (file != null && file.ContentLength > 0 && Path.GetExtension(file.FileName) == ".xlsx")
        //    {
        //        Imp.TeacherHandler.ImportData(file.InputStream, Session.GetSchoolYear().SchoolYearId);
        //    }
        //    else
        //    {
        //        answer.StateImport = StateImport.Failed;
        //        answer.Message = "File not found or format of extension isn't xlsx";
        //    }

        //    return RedirectToAction("Ready",
        //        new
        //        {
        //            message = answer.Message,
        //            title = answer.Title,
        //            countInsert = answer.CountInsert,
        //            countUpdate = answer.CountUpdate,
        //            countDelete = answer.CountDelete
        //        });
        //}

        [HttpGet]
        public ActionResult UploadILPs()
        {
            String message = String.Empty;

            ImpState.Status answer = new ImpState.Status();

            Imp.ILPHandler.ImportData(Session.GetSchoolYear().SchoolYearId);

            return RedirectToAction("Ready",
                new
                {
                    message = answer.Message,
                    title = answer.Title,
                    countInsert = answer.CountInsert,
                    countUpdate = answer.CountUpdate,
                    countDelete = answer.CountDelete
                });
        }

        //[HttpPost]
        //public ActionResult UploadILPPhilosophies()
        //{
        //    String message = String.Empty;

        //    ImpState.Status answer = new ImpState.Status();

        //    Imp.ILPHandler.ImportILPPhilosophy(Session.GetSchoolYear().SchoolYearId);

        //    return RedirectToAction("Ready",
        //        new
        //        {
        //            message = answer.Message,
        //            title = answer.Title,
        //            countInsert = answer.CountInsert,
        //            countUpdate = answer.CountUpdate,
        //            countDelete = answer.CountDelete
        //        });
        //}

        [HttpPost]
        public ActionResult UploadStudents(HttpPostedFileBase file)
        {
            String message = String.Empty;

            ImpState.Status answer = new ImpState.Status();

            if (file != null && file.ContentLength > 0 && Path.GetExtension(file.FileName) == ".xlsx")
            {
                Imp.StudentHandler.ImportData(file.InputStream, Session.GetSchoolYear().SchoolYearId);
            }
            else
            {
                answer.StateImport = StateImport.Failed;
                answer.Message = "File not found or format of extension isn't xlsx";
            }

            return RedirectToAction("Ready",
                new
                {
                    message = answer.Message,
                    title = answer.Title,
                    countInsert = answer.CountInsert,
                    countUpdate = answer.CountUpdate,
                    countDelete = answer.CountDelete
                });
        }

        [HttpPost]
        public ActionResult UploadCourses(HttpPostedFileBase file)
        {
            String message = String.Empty;

            ImpState.Status answer = new ImpState.Status();

            if (file != null && file.ContentLength > 0 && Path.GetExtension(file.FileName) == ".xlsx")
            {
                Imp.ASDCourseHandler.ImportData(file.InputStream, Session.GetSchoolYear().SchoolYearId);
            }
            else
            {
                answer.StateImport = StateImport.Failed;
                answer.Message = "File not found or format of extension isn't xlsx";
            }

            return RedirectToAction("Ready",
                new
                {
                    message = answer.Message,
                    title = answer.Title,
                    countInsert = answer.CountInsert,
                    countUpdate = answer.CountUpdate,
                    countDelete = answer.CountDelete
                });
        }

        public ActionResult TotalCleanDB()
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IAdminRepo>();
                //repo.ExecuteSql("DELETE FROM [dbo].[GoodServiceBalance]");
                //repo.ExecuteSql("DELETE FROM [dbo].[GoodService]");
                //repo.ExecuteSql("DELETE FROM [dbo].[CourseScheduleOnDay]");
                //repo.ExecuteSql("DELETE FROM [dbo].[Transfer]");
                //repo.ExecuteSql("DELETE FROM [dbo].[ILPBank]");
                //repo.ExecuteSql("DELETE FROM [dbo].[ILP]");
                //repo.ExecuteSql("DELETE FROM [dbo].[StudentPacketCourseAlert]");
                //repo.ExecuteSql("DELETE FROM [dbo].[StudentPacketCourse]");
                //repo.ExecuteSql("DELETE FROM [dbo].[StudentPacket]");
                //repo.ExecuteSql("DELETE FROM [dbo].[FPCSCoursesRestrictions]");
                //repo.ExecuteSql("DELETE FROM [dbo].[FPCSCourse]");
                repo.ExecuteSql("DELETE FROM [dbo].[ASDCourse]");
                //repo.ExecuteSql("DELETE FROM [dbo].[ServiceBelongVendor]");
                //repo.ExecuteSql("DELETE FROM [dbo].[ServiceVendor]");
                repo.ExecuteSql("DELETE FROM [dbo].[Subject]");
                //repo.ExecuteSql("DELETE FROM [dbo].[MonthlyContactLog]");
                //repo.ExecuteSql("DELETE FROM [dbo].[FamilyRelationship]");
                //repo.ExecuteSql("DELETE FROM [dbo].[FamilyGuardian]");
                //repo.ExecuteSql("DELETE FROM [dbo].[FamilyStudent]");
                //repo.ExecuteSql("DELETE FROM [dbo].[Family]");
                //repo.ExecuteSql("DELETE FROM [dbo].[Vendor]");
                //repo.ExecuteSql("DELETE FROM [dbo].[Teacher]");
                //repo.ExecuteSql("DELETE FROM [dbo].[Student]");
                //repo.ExecuteSql("DELETE FROM [dbo].[Guardian]");
            }

            return Json("success", JsonRequestBehavior.AllowGet);
        }
    }
}
