﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FPCS.Web.Admin.Code;

namespace FPCS.Web.Admin.Controllers
{
	[Authorize]
	public class PdfController : Controller
	{
		public ActionResult GenerateFromHtml(String htmlContent)
		{
			var urlFpcsLogo = "/Content/images/fpcsLogo.png";
			htmlContent = htmlContent.Replace(urlFpcsLogo, VirtualPathHandler.ToAbsoluteUrl(urlFpcsLogo));

			var pdfBytes = (new NReco.PdfGenerator.HtmlToPdfConverter()).GeneratePdf(htmlContent);
			String guid = Guid.NewGuid().ToString();
			Session[guid] = pdfBytes;
			return Json(guid, JsonRequestBehavior.AllowGet);
		}

        public ActionResult Get(String guid, String fileName = "Report.pdf")
		{
			if (Session[guid] == null) return null;

			var bytes = (Byte[])Session[guid];
            return File(bytes, "application/pdf", fileName);
		}
	}
}
