﻿using FPCS.Core.jqGrid;
using FPCS.Data;
using FPCS.Data.Entities;
using FPCS.Data.Enums;
using FPCS.Data.Exceptions;
using FPCS.Data.Repo;
using FPCS.Web.Admin.Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FPCS.Core.Extensions;
using FPCS.Web.Admin.Models.Transfers;
using FPCS.Web.Admin.Helpers.Access.Models;
using FPCS.Web.Admin.Helpers.Access;
using FPCS.Web.Admin.Helpers.BudgetStudent;

namespace FPCS.Web.Admin.Controllers
{
    public class TransferController : BaseController
    {
        [FPCSAuthorize(Role.Admin, Role.Guardian)]
        public ActionResult Index(Guid id)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var packetRepo = uow.GetRepo<IStudentPacketRepo>();

                Student student = uow.GetRepo<IStudentRepo>().Get(id);

                TransferIndexModel model = new TransferIndexModel();
                model.StudentId = student.DbUserId;
                model.StudentName = String.Format(@"{0}, {1}", student.LastName, student.FirstName);

                var repo = uow.GetRepo<IStudentPacketRepo>();

                var studentPacket = repo.Get(id, SchoolYear.SchoolYearId);
                var dbEntity = packetRepo.AddIsNotExist(id, SchoolYear.SchoolYearId);

                if (dbEntity != null)
                {
                    uow.Commit();
                    studentPacket = dbEntity;
                }

                model.IsLocked = studentPacket.IsLocked;

                return View(model);
            }
        }

        [FPCSAuthorize(Role.Admin, Role.Guardian)]
        public JsonResult _Index(GridOptions options, Guid studentId, TransferListOptions transferListOptions)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<ITransferRepo>();
                var query = repo.GetByStudent(studentId);

                var engine = new GridDynamicEngine(options, transferListOptions);
                var result = engine.ApplyAll(query, x => new TransferMiniModel
                {
                    TransferId = x.TransferId,
                    DateCreate = x.DateCreate,
                    Comments = x.Comment,
                    CreatedBy = x.UserCreateName,
                    FromStudent = x.FromStudent != null ? 
                                        x.FromStudent.LastName + ", " + x.FromStudent.FirstName :
                                        "FPCS Account",
                    ToStudent = x.ToStudent != null ?
                                        x.ToStudent.LastName + ", " + x.ToStudent.FirstName :
                                        "FPCS Account",
                    Amount = x.TransferAmount
                });

                return Json(result);
            }
        }

        [HttpGet]
        [FPCSAuthorize(Role.Admin, Role.Guardian)]
        public ActionResult _Details(Int32 id)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<ITransferRepo>();
                var dbEntity = repo.Get(id);
                if (dbEntity == null) return JsonRes(Status.Error, "Transfer not found");

                var model = new DetailTransferModel(dbEntity, Session.GetSchoolYear().SchoolYearId);

                return PartialView(model);
            }
        }

        [HttpGet]
        [FPCSAuthorize(Role.Admin, Role.Guardian)]
        public ActionResult _Create(Guid id)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repoTransfer = uow.GetRepo<ITransferRepo>();
                var repoStudent = uow.GetRepo<IStudentRepo>();

                var dbEntity = repoStudent.Get(id);
                if (dbEntity == null)
                {
                    return ErrorPartial("Student {0} not found", id);
                }

                var model = new CreateTransferModel(dbEntity, Session.GetSchoolYear().SchoolYearId);

                return PartialView(model);
            }
        }

        [HttpPost]
        [FPCSAuthorize(Role.Admin, Role.Guardian)]
        [ValidateAntiForgeryToken]
        public ActionResult _Create(CreateTransferModel model)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repoStudent = uow.GetRepo<IStudentRepo>();
                var repoStudentPacket = uow.GetRepo<IStudentPacketRepo>();

                if (model.FromStudentId == Guid.Empty && model.ToStudentId != model.StudentId)
                {
                    throw new NotFoundEntityException("Need student not choose"); ;
                }
                else if (model.FromStudentId == model.ToStudentId)
                {
                    throw new NotFoundEntityException("From student equal To student"); ;
                }

                Student dbStudent = repoStudent.Get(model.StudentId);
                Student fromStudent = repoStudent.Get(model.FromStudentId);
                if (dbStudent == null) { /*throw new NotFoundEntityException("Student not found");*/ }

                if (ModelState.IsValid)
                {
                    try
                    {
                        var studentPacket = repoStudentPacket.Get(dbStudent.DbUserId, SchoolYear.SchoolYearId);

                        if (studentPacket != null)
                        {
                            AccessActionModel access = AccessAction.IsNotLock(studentPacket, User.Role);

                            if (!access.IsResolve)
                            {
                                throw new Exception(access.Message);
                            }
                        }
                        
                        if (model.ToStudentId != dbStudent.DbUserId && !BudgetStudentHelper.IsResolveTransfer(studentPacket.StudentId, SchoolYear.SchoolYearId, model.TransferAmount))
                        {
                            return JsonRes(Status.NoEnoughBudget, BudgetStudentHelper.MessageNoEnoughBudget);
                        }

                        var repoTransfer = uow.GetRepo<ITransferRepo>();

                        Transfer transfer = new Transfer();
                        transfer.DateCreate = DateTime.Now;
                        transfer.Comment = model.Comment.TrimAndReduce();
                        transfer.UserCreateName = String.Format(@"{0}, {1}", Session.GetUser().LastName, Session.GetUser().FirstName);
                        transfer.FromStudent = fromStudent;
                        transfer.ToStudent = repoStudent.Get(model.ToStudentId);
                        transfer.TransferAmount = model.TransferAmount;

                        repoTransfer.Add(transfer);

                        uow.Commit();

                        return JsonRes(transfer.TransferId.ToString());
                    }
                    catch (Exception ex)
                    {
                        ModelState.AddModelError("", ex.Message);
                    }
                }

                model.Init(dbStudent, Session.GetSchoolYear().SchoolYearId);
                return PartialView(model);
            }
        }

        [HttpPost]
        [FPCSAuthorize(Role.Admin, Role.Guardian)]
        public ActionResult Delete(Int32 id)
        {
            try
            {
                using (var uow = UnityManager.Resolve<IUnitOfWork>())
                {
                    var repo = uow.GetRepo<ITransferRepo>();
                    var repoStudentPacket = uow.GetRepo<IStudentPacketRepo>();

                    var transfer = repo.Get(id);

                    var studentPacket = repoStudentPacket.Get(transfer.FromStudentId.HasValue ? 
                                                                transfer.FromStudentId.Value : 
                                                                transfer.ToStudentId.Value,
                                                              SchoolYear.SchoolYearId);

                    if (studentPacket != null)
                    {
                        AccessActionModel access = AccessAction.IsNotLock(studentPacket, User.Role);

                        if (!access.IsResolve)
                        {
                            throw new Exception(access.Message);
                        }
                    }

                    repo.Remove(transfer);
                    
                    uow.Commit();
                    return JsonRes();
                }
            }
            catch (Exception ex)
            {
                return JsonRes(Status.Error, ex.Message);
            }
        }
    }
}
