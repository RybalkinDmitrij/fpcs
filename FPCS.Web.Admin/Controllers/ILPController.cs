﻿using FPCS.Data;
using FPCS.Data.Entities;
using FPCS.Data.Repo;
using FPCS.Web.Admin.Code;
using FPCS.Web.Admin.Helpers.Access;
using FPCS.Web.Admin.Helpers.Access.Models;
using FPCS.Web.Admin.Models.ILP;
using FPCS.Core.Extensions;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FPCS.Web.Admin.Controllers
{
	public class ILPController : BaseController
	{
		[HttpGet]
		public ActionResult _ILPDetails(Int32? studentPacketCourseId, Int32? courseId)
		{
			ILPModel model = new ILPModel(studentPacketCourseId, courseId);

			return PartialView(model);
		}

		[HttpGet]
		public ActionResult _ILPInfo(Int32 id)
		{
			ILPInfoModel model = new ILPInfoModel(id);

			return PartialView(model);
		}

		[HttpGet]
		public ActionResult _ILP(Int32? studentPacketCourseId, Int32? courseId)
		{
			ILPModel model = new ILPModel(studentPacketCourseId, courseId);

			return PartialView(model);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult _ILP(ILPModel model)
		{
			if (ModelState.IsValid)
			{
				try
				{
					using (var uow = UnityManager.Resolve<IUnitOfWork>())
					{
						var repo = uow.GetRepo<IILPRepo>();
						var repoBank = uow.GetRepo<IILPBankRepo>();
						var repoSchoolYear = uow.GetRepo<ISchoolYearRepo>();

						// if it is edit student packet course then check IsLocked
						if (model.StudentPacketCourseId != 0)
						{
							var studentPacketCourse = uow.GetRepo<IStudentPacketCourseRepo>().Get(model.StudentPacketCourseId);

							if (studentPacketCourse == null)
							{
								throw new Exception("Student packet course does not exist");
							}

							AccessActionModel access = AccessAction.IsNotLock(studentPacketCourse.StudentPacket, User.Role);
							if (!access.IsResolve)
							{
								throw new Exception(access.Message);
							}
						}

						ILP dbEntity = repo.Get(model.ILPId);
						if (dbEntity == null) dbEntity = new ILP();

						dbEntity.StudentPacketCourseId = model.StudentPacketCourseId != 0 ? (Int32?) model.StudentPacketCourseId : null;
						dbEntity.FPCSCourseId = model.FPCSCourseId != 0 ? (Int32?) model.FPCSCourseId : null;

						dbEntity.CourseHrs = model.CourseHrs;
						dbEntity.Instructor = model.Instructor != null ? model.Instructor : "";
                        dbEntity.ILPName = "";
						dbEntity.DescriptionCourse = model.DescriptionCourse.TrimAndReduce();
						dbEntity.Standards = model.Standards.TrimAndReduce();
						dbEntity.StudentActivities = model.StudentActivities.TrimAndReduce();
						dbEntity.MaterialsResources = model.MaterialsResources.TrimAndReduce();
						dbEntity.RoleAnyPeople = model.RoleAnyPeople.TrimAndReduce();
						dbEntity.EvaluationGradingPassFail = model.EvaluationGradingPassFail;
						dbEntity.EvaluationGradingGradingScale = model.EvaluationGradingGradingScale;
						dbEntity.EvaluationGradingOSN = model.EvaluationGradingOSN;
						dbEntity.EvaluationGradingOther = model.EvaluationGradingOther;
						dbEntity.EvaluationGradingOtherExplain = model.EvaluationGradingOtherExplain.TrimAndReduce();
						dbEntity.EvaluatedMeasurableOutcomes = model.EvaluatedMeasurableOutcomes.TrimAndReduce();
						dbEntity.CourseSyllabus = model.CourseSyllabus.TrimAndReduce();
						dbEntity.GuardianILPModifications = model.GuardianILPModifications.TrimAndReduce();
						dbEntity.InstructorILPModifications = model.InstructorILPModifications.TrimAndReduce();

						if (model.ILPId == 0)
						{
							dbEntity = repo.Add(dbEntity);
						}
						else
						{
							repo.Update(dbEntity);
						}

						uow.Commit();

						StudentPacketCourse spc = dbEntity.StudentPacketCourseId.HasValue
							? uow.GetRepo<IStudentPacketCourseRepo>().Get(dbEntity.StudentPacketCourseId.Value)
							: null;

						FPCSCourse fpcsCourse = dbEntity.FPCSCourseId.HasValue
							? uow.GetRepo<IFPCSCourseRepo>().Get(dbEntity.FPCSCourseId.Value)
							: null;

						var teacherGuid = spc != null ? spc.FPCSCourse.TeacherId : fpcsCourse.TeacherId;

						if (model.AddILPToBank && teacherGuid.HasValue)
						{
							//ILPBank ilpBank = repoBank.GetByILP(dbEntity.ILPId);
							ILPBank ilpBank = null;

							if (ilpBank == null)
							{
								ilpBank = new ILPBank();
							}

							//SchoolYear schoolYear = studentPacketCourse != null ?
							//    repoSchoolYear.GetByYear(studentPacketCourse.FPCSCourse.ClassStartDate.Year + 1) :
							//    repoSchoolYear.GetByYear(fpcsCourse.ClassStartDate.Year + 1);

							SchoolYear schoolYear = repoSchoolYear.Get(SchoolYear.SchoolYearId);

							ilpBank.SchoolYearId = schoolYear.SchoolYearId;
							ilpBank.SubjectId = spc != null
								? spc.FPCSCourse.ASDCourse.SubjectId
								: fpcsCourse.ASDCourse.SubjectId;

							ilpBank.TeacherId = spc != null
								? spc.FPCSCourse.TeacherId
								: fpcsCourse.TeacherId;

							//ilpBank.GuardianId = spc != null ?
							//    spc.FPCSCourse.GuardianId :
							//    fpcsCourse.GuardianId;

							ilpBank.GuardianId = null;

							ilpBank.ILPId = dbEntity.ILPId;
							ilpBank.ILPName = dbEntity.ILPName.TrimAndReduce();
							ilpBank.IsPublic = false;

							ilpBank.CourseHrs = dbEntity.CourseHrs;
							ilpBank.DescriptionCourse = dbEntity.DescriptionCourse.TrimAndReduce();
							ilpBank.Standards = dbEntity.Standards.TrimAndReduce();
							ilpBank.StudentActivities = dbEntity.StudentActivities.TrimAndReduce();
							ilpBank.MaterialsResources = dbEntity.MaterialsResources.TrimAndReduce();
							ilpBank.RoleAnyPeople = dbEntity.RoleAnyPeople.TrimAndReduce();
							ilpBank.EvaluationGradingPassFail = dbEntity.EvaluationGradingPassFail;
							ilpBank.EvaluationGradingGradingScale = dbEntity.EvaluationGradingGradingScale;
							ilpBank.EvaluationGradingOSN = dbEntity.EvaluationGradingOSN;
							ilpBank.EvaluationGradingOther = dbEntity.EvaluationGradingOther;
							ilpBank.EvaluationGradingOtherExplain = dbEntity.EvaluationGradingOtherExplain.TrimAndReduce();
							ilpBank.EvaluatedMeasurableOutcomes = dbEntity.EvaluatedMeasurableOutcomes.TrimAndReduce();
							ilpBank.CourseSyllabus = dbEntity.CourseSyllabus.TrimAndReduce();
							ilpBank.GuardianILPModifications = dbEntity.GuardianILPModifications.TrimAndReduce();
							ilpBank.InstructorILPModifications = dbEntity.InstructorILPModifications.TrimAndReduce();

							if (ilpBank.ILPBankId == 0)
							{
								ilpBank = repoBank.Add(ilpBank);
							}
							else
							{
								repoBank.Update(ilpBank);
							}

							uow.Commit();
						}

						return JsonRes(dbEntity.ILPId.ToString());
					}
				}
				catch (Exception ex)
				{
					return JsonRes(Status.Error, ex.Message);
				}
			}

			return PartialView(model);
		}

		[HttpGet]
		public ActionResult _ILPPrint(Int32 id)
		{
			ILPPrintModel model = new ILPPrintModel(id);

			model.PrintAddress = (ConfigurationManager.AppSettings["PrintAddress"]).ToString();
			model.PrintState = (ConfigurationManager.AppSettings["PrintState"]).ToString();
			model.PrintPhone = (ConfigurationManager.AppSettings["PrintPhone"]).ToString();
			model.PrintFax = (ConfigurationManager.AppSettings["PrintFax"]).ToString();
			model.PrintDateNow = DateTime.Now.ToString("MM/dd/yyyy");

			return PartialView(model);
		}
	}
}
