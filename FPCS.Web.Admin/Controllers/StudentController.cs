﻿using System;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Linq;

using FPCS.Core.Extensions;
using FPCS.Data;
using FPCS.Data.Repo;
using FPCS.Data.Enums;
using FPCS.Web.Admin.Code;
using FPCS.Web.Admin.Models.Student;
using FPCS.Core.jqGrid;
using FPCS.Data.Entities;
using FPCS.Web.Admin.Helpers.Access.Models;
using FPCS.Web.Admin.Helpers.Access;
using FPCS.Data.Exceptions;
using System.Configuration;
using FPCS.Core;

namespace FPCS.Web.Admin.Controllers
{
    public class StudentController : BaseController
    {
        [FPCSAuthorize(Role.Admin, Role.Teacher, Role.Guardian)]
        public ActionResult Index()
        {
            StudentIndexModel model = new StudentIndexModel();

            model.Grade = String.Join(@";", FPCS.Data.Enums.Grade.G.ToSelectListUsingDesc().Select(x => String.Format(@"{0}: {1}", x.Value, x.Value)));
            model.Sex = String.Join(@";", FPCS.Data.Enums.Sex.Male.ToSelectListUsingDesc().Select(x => String.Format(@"{0}: {1}", x.Value, x.Text)));
            model.EnrollmentStatus = String.Join(@";", FPCS.Data.Enums.EnrollmentStatus.Enrolled.ToSelectListUsingDesc().Select(x => String.Format(@"{0}: {1}", x.Value, x.Text)));

            model.Init(Session.GetSchoolYear().SchoolYearId);

            return View(model);
        }

        [FPCSAuthorize(Role.Admin, Role.Teacher, Role.Guardian)]
        public JsonResult _Index(GridOptions options, StudentsListOptions studentsListOptions, Guid? studentId, Guid? teacherId, Int64? fpcsCourseId)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var students = GetData(uow, options, studentsListOptions, studentId, teacherId, fpcsCourseId);
                Int32 totalCount = students.Count();

                var engine = new GridDynamicEngine(options, studentsListOptions);
                var result = engine.CreateGridResult2(engine.ApplyPaging(students.AsQueryable()), totalCount, x => new StudentMiniModel
                {
                    DbUserId = x.DbUserId,
                    FullName = x.FullName,
                    Email = x.Email,
                    Sex = x.Sex,
                    Grade = x.Grade,
                    ZangleID = x.ZangleID,
                    EnrollmentStatus = x.EnrollmentStatus,
                    EnrollmentDate = x.EnrollmentDate,
                    WithdrawalDate = x.WithdrawalDate,
                    IsLocked = x.IsLocked,
                    Guardians = x.Guardians,
                    Phone = x.Phone
                });

                return Json(result);
            }
        }

        [FPCSAuthorize(Role.Admin, Role.Teacher, Role.Guardian)]
        [HttpGet]
        public ActionResult _Details(Guid id)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IStudentRepo>();
                var dbEntity = repo.Get(id);
                if (dbEntity == null) return JsonRes(Status.Error, "Student not found");

                var model = new StudentDetailsModel
                {
                    DbUserId = dbEntity.DbUserId,
                    FirstName = dbEntity.FirstName,
                    LastName = dbEntity.LastName,
                    MiddleInitial = dbEntity.MiddleInitial,
                    Email = dbEntity.Email,
                    GradYear = dbEntity.GradYear,
                    DateOfBirth = dbEntity.DateOfBirth,
                    EnrollmentDate = dbEntity.EnrollmentDate,
                    WithdrawalDate = dbEntity.WithdrawalDate,
                    PrivateSchoolName = dbEntity.PrivateSchoolName,
                    PercentInSchoolDistrict = dbEntity.PercentInSchoolDistrict,
                    IsGraduateFromFPCS = dbEntity.IsGraduateFromFPCS,
                    IsASDContractHoursExemption = dbEntity.IsASDContractHoursExemption,
                    ReasonASDContractHoursExemption = dbEntity.ReasonASDContractHoursExemption,
                    Sex = dbEntity.Sex,
                    Grade = dbEntity.Grade,
                    EnrollmentStatus = dbEntity.EnrollmentStatus,
                    ZangleID = dbEntity.ZangleID,
                    PercentagePlanningEnroll = dbEntity.PercentagePlanningEnroll.GetValueOrDefault(100),
                    CoreCreditExemption = dbEntity.CoreCreditExemption.GetValueOrDefault(0),
                    CoreCreditExemptionReason = dbEntity.CoreCreditExemptionReason,
                    ElectiveCreditExemption = dbEntity.ElectiveCreditExemption.GetValueOrDefault(0),
                    ElectiveCreditExemptionReason = dbEntity.ElectiveCreditExemptionReason,
                    IsBirthCertificate = dbEntity.IsBirthCertificate.GetValueOrDefault(false),
                    IsGradesNotSubmitted = dbEntity.IsGradesNotSubmitted.GetValueOrDefault(false),
                    IsILPPhilosophy = dbEntity.IsILPPhilosophy.GetValueOrDefault(false),
                    IsMedicalRelease = dbEntity.IsMedicalRelease.GetValueOrDefault(false),
                    IsProgressReportSignature = dbEntity.IsProgressReportSignature.GetValueOrDefault(false),
                    IsShotRecords = dbEntity.IsShotRecords.GetValueOrDefault(false),
                    IsTestingAgreement = dbEntity.IsTestingAgreement.GetValueOrDefault(false),
                    IsOther = dbEntity.IsOther.GetValueOrDefault(false)
                };

                model.Init();
                return PartialView(model);
            }
        }

        [FPCSAuthorize(Role.Admin)]
        [HttpGet]
        public PartialViewResult _Create()
        {
            var model = new StudentCreateModel();
            model.Init(SchoolYear.SchoolYearId);
            model.GradYear = DateTime.Today.Year + 12;
            return PartialView(model);
        }

        [FPCSAuthorize(Role.Admin)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _Create(StudentCreateModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var uow = UnityManager.Resolve<IUnitOfWork>())
                    {
                        var repo = uow.GetRepo<IStudentRepo>();
                        var repoPacket = uow.GetRepo<IStudentPacketRepo>();

                        var dbEntity = repo.Add(model.FirstName, model.LastName, model.MiddleInitial, model.Email, model.GradYear, model.DateOfBirth.Value,
                            model.WithdrawalDate, model.EnrollmentDate, model.PrivateSchoolName, model.PercentInSchoolDistrict, model.IsGraduateFromFPCS, model.IsASDContractHoursExemption,
                            model.ReasonASDContractHoursExemption, model.Sex, model.Grade, model.EnrollmentStatus, model.ZangleID, model.PercentagePlanningEnroll, model.CoreCreditExemption,
                            model.CoreCreditExemptionReason, model.ElectiveCreditExemption, model.ElectiveCreditExemptionReason,
                            model.IsBirthCertificate, model.IsGradesNotSubmitted, model.IsILPPhilosophy, model.IsMedicalRelease, model.IsProgressReportSignature, model.IsShotRecords,
                            model.IsTestingAgreement, model.IsOther, SchoolYear.SchoolYearId);

                        uow.Commit();

                        dbEntity.UnqDbUserId = dbEntity.DbUserId;
                        
                        uow.Commit();

                        var dbPacket = repoPacket.GetOrAddIsNotExist(dbEntity.DbUserId, SchoolYear.SchoolYearId);
                        if (dbPacket != null) { uow.Commit(); }

                        dbPacket.SponsorTeacherId = model.SponsorTeacherId;
                        repoPacket.Update(dbPacket);
                        uow.Commit();

                        return JsonRes(dbEntity.DbUserId.ToString());
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }
            }

            model.Init(SchoolYear.SchoolYearId);
            return PartialView(model);
        }

        [FPCSAuthorize(Role.Admin, Role.Guardian)]
        [HttpGet]
        public PartialViewResult _Edit(Guid id)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var dbEntity = uow.GetRepo<IStudentRepo>().Get(id);
                if (dbEntity == null) return ErrorPartial("Student {0} not found", id);

                var dbStudentPacket = uow.GetRepo<IStudentPacketRepo>().Get(dbEntity.DbUserId, SchoolYear.SchoolYearId);

                var model = new StudentEditModel
                {
                    DbUserId = dbEntity.DbUserId,
                    FirstName = dbEntity.FirstName,
                    LastName = dbEntity.LastName,
                    MiddleInitial = dbEntity.MiddleInitial,
                    Email = dbEntity.Email,
                    GradYear = dbEntity.GradYear,
                    DateOfBirth = dbEntity.DateOfBirth,
                    WithdrawalDate = dbEntity.WithdrawalDate,
                    EnrollmentDate = dbEntity.EnrollmentDate,
                    PrivateSchoolName = dbEntity.PrivateSchoolName,
                    PercentInSchoolDistrict = dbEntity.PercentInSchoolDistrict,
                    IsGraduateFromFPCS = dbEntity.IsGraduateFromFPCS,
                    IsASDContractHoursExemption = dbEntity.IsASDContractHoursExemption,
                    ReasonASDContractHoursExemption = dbEntity.ReasonASDContractHoursExemption,
                    Sex = dbEntity.Sex,
                    Grade = dbEntity.Grade,
                    EnrollmentStatus = dbEntity.EnrollmentStatus,
                    ZangleID = dbEntity.ZangleID,
                    PercentagePlanningEnroll = dbEntity.PercentagePlanningEnroll.GetValueOrDefault(100),
                    CoreCreditExemption = dbEntity.CoreCreditExemption.GetValueOrDefault(0),
                    CoreCreditExemptionReason = dbEntity.CoreCreditExemptionReason,
                    ElectiveCreditExemption = dbEntity.ElectiveCreditExemption.GetValueOrDefault(0),
                    ElectiveCreditExemptionReason = dbEntity.ElectiveCreditExemptionReason,
                    IsBirthCertificate = dbEntity.IsBirthCertificate.GetValueOrDefault(false),
                    IsGradesNotSubmitted = dbEntity.IsGradesNotSubmitted.GetValueOrDefault(false),
                    IsILPPhilosophy = dbEntity.IsILPPhilosophy.GetValueOrDefault(false),
                    IsMedicalRelease = dbEntity.IsMedicalRelease.GetValueOrDefault(false),
                    IsProgressReportSignature = dbEntity.IsProgressReportSignature.GetValueOrDefault(false),
                    IsShotRecords = dbEntity.IsShotRecords.GetValueOrDefault(false),
                    IsTestingAgreement = dbEntity.IsTestingAgreement.GetValueOrDefault(false),
                    IsOther = dbEntity.IsOther.GetValueOrDefault(false),
                    IsLockedStudentPacket = dbStudentPacket != null ? dbStudentPacket.IsLocked : false,
                    SponsorTeacherId = dbStudentPacket != null ? dbStudentPacket.SponsorTeacherId : null
                };

                model.Init(SchoolYear.SchoolYearId);
                return PartialView(model);
            }
        }

        [FPCSAuthorize(Role.Admin, Role.Guardian)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _Edit(StudentEditModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var uow = UnityManager.Resolve<IUnitOfWork>())
                    {
                        var repo = uow.GetRepo<IStudentRepo>();
                        var repoStudentPacket = uow.GetRepo<IStudentPacketRepo>();

                        Student dbEntity = repo.Get(model.DbUserId);
                        if (dbEntity == null) throw new NotFoundEntityException("Student not found");

                        var studentPacket = repoStudentPacket.GetOrAddIsNotExist(dbEntity.DbUserId, SchoolYear.SchoolYearId);
                        if (studentPacket != null) { uow.Commit(); }

                        if (studentPacket != null)
                        {
                            studentPacket.SponsorTeacherId = model.SponsorTeacherId;
                            repoStudentPacket.Update(studentPacket);
                            uow.Commit();

                            AccessActionModel access = AccessAction.IsNotLock(studentPacket, User.Role);

                            if (!access.IsResolve || User.Role != Role.Admin)
                            {
                                model.PercentagePlanningEnroll = dbEntity.PercentagePlanningEnroll.GetValueOrDefault(0);
                            }
                        }

                        if (dbEntity.WithdrawalDate == null && model.WithdrawalDate != null)
                        {
                            var repoUser = uow.GetRepo<IDbUserRepo>();
                            var users = repoUser.GetByRoles(SchoolYear.SchoolYearId, Role.Admin)
                                                    .Where(x => x.FullName == "Zimmerman Joanne" || 
                                                                x.FullName == "Corbin Dru" ||
                                                                x.FullName == "Caldwell Rebecca" ||
                                                                x.FullName == "Mastro Debbie" ||
                                                                x.FullName == "Roehl Debra" ||
                                                                x.FullName == "Shiroma-Carroll Deanne");
                            foreach (var user in users)
                            {
                                if (user.Email != "")
                                    EmailSender.Instance.Send(user.Email, "Notice: Student Withdrawal - " + model.LastName.TrimAndReduce() + " " + model.FirstName.TrimAndReduce(), String.Empty);
                            }
                            if (model.SponsorTeacherId != null)
                            {
                                var repoTeacher = uow.GetRepo<ITeacherRepo>();
                                var sponsorTeacher = repoTeacher.Get(model.SponsorTeacherId);
                                if (sponsorTeacher.Email != "")
                                    EmailSender.Instance.Send(sponsorTeacher.Email, "Notice: Student Withdrawal - " + model.LastName.TrimAndReduce() + " " + model.FirstName.TrimAndReduce(), String.Empty);
                            }
                        }

                        dbEntity.FirstName = model.FirstName.TrimAndReduce();
                        dbEntity.LastName = model.LastName.TrimAndReduce();
                        dbEntity.FullName = model.LastName.TrimAndReduce() + " " + model.FirstName.TrimAndReduce();
                        dbEntity.MiddleInitial = model.MiddleInitial.TrimAndReduce();
                        dbEntity.Email = model.Email.TrimAndReduce();
                        dbEntity.GradYear = model.GradYear;
                        dbEntity.DateOfBirth = model.DateOfBirth.Value;
                        dbEntity.WithdrawalDate = model.WithdrawalDate;
                        dbEntity.EnrollmentDate = model.EnrollmentDate;
                        dbEntity.PrivateSchoolName = model.PrivateSchoolName.TrimAndReduce();
                        dbEntity.PercentInSchoolDistrict = model.PercentInSchoolDistrict;
                        dbEntity.IsGraduateFromFPCS = model.IsGraduateFromFPCS;
                        dbEntity.IsASDContractHoursExemption = model.IsASDContractHoursExemption;
                        dbEntity.ReasonASDContractHoursExemption = model.ReasonASDContractHoursExemption;
                        dbEntity.UpdatedDate = DateTimeOffset.Now;
                        dbEntity.Sex = model.Sex;
                        dbEntity.Grade = model.Grade;
                        dbEntity.EnrollmentStatus = model.EnrollmentStatus;
                        dbEntity.ZangleID = model.ZangleID;
                        dbEntity.PercentagePlanningEnroll = model.PercentagePlanningEnroll;
                        dbEntity.CoreCreditExemption = model.CoreCreditExemption;
                        dbEntity.CoreCreditExemptionReason = model.CoreCreditExemptionReason.TrimAndReduce();
                        dbEntity.ElectiveCreditExemption = model.ElectiveCreditExemption;
                        dbEntity.ElectiveCreditExemptionReason = model.ElectiveCreditExemptionReason.TrimAndReduce();
                        dbEntity.IsBirthCertificate = model.IsBirthCertificate;
                        dbEntity.IsGradesNotSubmitted = model.IsGradesNotSubmitted;
                        dbEntity.IsILPPhilosophy = model.IsILPPhilosophy;
                        dbEntity.IsMedicalRelease = model.IsMedicalRelease;
                        dbEntity.IsProgressReportSignature = model.IsProgressReportSignature;
                        dbEntity.IsShotRecords = model.IsShotRecords;
                        dbEntity.IsTestingAgreement = model.IsTestingAgreement;
                        dbEntity.IsOther = model.IsOther;
                        dbEntity.SchoolYearId = SchoolYear.SchoolYearId;

                        repo.Update(dbEntity);

                        uow.Commit();

                        return JsonRes(new { DbUserId = dbEntity.DbUserId, FullName = dbEntity.FullName, GradYear = dbEntity.GradYear });
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }
            }

            model.Init(SchoolYear.SchoolYearId);
            return PartialView(model);
        }

        [FPCSAuthorize(Role.Admin, Role.Guardian)]
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            try
            {
                using (var uow = UnityManager.Resolve<IUnitOfWork>())
                {
                    var repo = uow.GetRepo<IStudentRepo>();
                    repo.Remove(id);

                    uow.Commit();
                    return JsonRes();
                }
            }
            catch (Exception ex)
            {
                return JsonRes(Status.Error, ex.Message);
            }
        }

        [FPCSAuthorize(Role.Admin, Role.Guardian)]
        [HttpPost]
        public ActionResult DeleteAll(String id)
        {
            try
            {
                if (String.IsNullOrEmpty(id)) return JsonRes(Status.Error, "Not correct ids");

                var ids = id.Split(',');
                using (var uow = UnityManager.Resolve<IUnitOfWork>())
                {
                    Int32 errorsCount = 0;
                    var repo = uow.GetRepo<IStudentRepo>();
                    foreach (var item in ids)
                    {
                        try
                        {
                            repo.Remove(new Guid(item));
                        }
                        catch (Exception)
                        {
                            errorsCount++;
                        }
                    }

                    uow.Commit();
                    if (errorsCount == 0) return JsonRes();
                    else return JsonRes(Status.Error, "Were deleted not all entities");
                }
            }
            catch (Exception ex)
            {
                return JsonRes(Status.Error, ex.Message);
            }
        }

        [FPCSAuthorize(Role.Admin, Role.Teacher, Role.Guardian)]
        [HttpGet]
        public ActionResult _DetailsIEP(Guid id)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IStudentRepo>();
                var dbEntity = repo.Get(id);
                if (dbEntity == null) return JsonRes(Status.Error, "Student not found");

                var model = new StudentIEPDetailsModel(dbEntity);

                model.Init();
                return PartialView(model);
            }
        }

        [FPCSAuthorize(Role.Admin, Role.Guardian)]
        [HttpGet]
        public PartialViewResult _EditIEP(Guid id)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var dbEntity = uow.GetRepo<IStudentRepo>().Get(id);
                if (dbEntity == null) return ErrorPartial("Student {0} not found", id);

                var model = new StudentIEPEditModel(dbEntity);

                model.Init();
                return PartialView(model);
            }
        }

        [FPCSAuthorize(Role.Admin, Role.Guardian)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _EditIEP(StudentIEPEditModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var uow = UnityManager.Resolve<IUnitOfWork>())
                    {
                        var repo = uow.GetRepo<IStudentRepo>();

                        var dbEntity = repo.Get(model.DbUserId);

                        dbEntity.IEPHasChildEligible = model.IEPHasChildEligible;
                        dbEntity.IEPGiftedTalented = model.IEPGiftedTalented;
                        dbEntity.IEPDeafBlindness = model.IEPDeafBlindness;
                        dbEntity.IEPHearingImpairment = model.IEPHearingImpairment;
                        dbEntity.IEPMultipleDisability = model.IEPMultipleDisability;
                        dbEntity.IEPTraumaticBrainInjury = model.IEPTraumaticBrainInjury;
                        dbEntity.IEPAutismAsbergerSyndrome = model.IEPAutismAsbergerSyndrome;
                        dbEntity.IEPEarlyChildhoodDevelopmentalDelay = model.IEPEarlyChildhoodDevelopmentalDelay;
                        dbEntity.IEPSpecificLearningDisability = model.IEPSpecificLearningDisability;
                        dbEntity.IEPOrthopedicImpairment = model.IEPOrthopedicImpairment;
                        dbEntity.IEPSpeechLanguageImpairment = model.IEPSpeechLanguageImpairment;
                        dbEntity.IEPDeafness = model.IEPDeafness;
                        dbEntity.IEPEmotionalDisturbance = model.IEPEmotionalDisturbance;
                        dbEntity.IEPMentalRetardation = model.IEPMentalRetardation;
                        dbEntity.IEPOtherHealthImpairment = model.IEPOtherHealthImpairment;
                        dbEntity.IEPVisualImpairment = model.IEPVisualImpairment;
                        dbEntity.IEPHasChildReceiving = model.IEPHasChildReceiving;
                        dbEntity.IEPHasChildFormally = model.IEPHasChildFormally;
                        dbEntity.IEPIsExpirationDate = model.IEPIsExpirationDate;
                        dbEntity.IEPIsNextEligibilityEvaluation = model.IEPIsNextEligibilityEvaluation;
                        dbEntity.IEPHasChildLearnedAnotherLanguage = model.IEPHasChildLearnedAnotherLanguage;
                        dbEntity.IEPHasChildExperiencedLearned = model.OtherLanguage;
                        dbEntity.IEPIsChildEligibleBilingualProgram = model.IEPIsChildEligibleBilingualProgram;
                        dbEntity.IEPHasChildCurrentlyReceivingServicesBilingualProgram = model.IEPHasChildCurrentlyReceivingServicesBilingualProgram;
                        dbEntity.IEPHasChildReceivingServicesMigrantEducation = model.IEPHasChildReceivingServicesMigrantEducation;
                        
                        uow.Commit();
                        return JsonRes(new { DbUserId = dbEntity.DbUserId, FullName = dbEntity.FullName, GradYear = dbEntity.GradYear });
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }
            }

            model.Init();
            return PartialView(model);
        }

        [FPCSAuthorize(Role.Admin, Role.Teacher, Role.Guardian)]
        [HttpGet]
        public ActionResult _DetailsILPPhilosophy(Guid id)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IStudentRepo>();
                var dbEntity = repo.Get(id);
                if (dbEntity == null) return JsonRes(Status.Error, "Student not found");

                var model = new StudentILPPhilosophyDetailModel(dbEntity);

                return PartialView(model);
            }
        }

        [FPCSAuthorize(Role.Admin, Role.Guardian, Role.Teacher)]
        [HttpGet]
        public PartialViewResult _EditILPPhilosophy(Guid id)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var dbEntity = uow.GetRepo<IStudentRepo>().Get(id);
                if (dbEntity == null) return ErrorPartial("Student {0} not found", id);

                var model = new StudentILPPhilosophyEditModel(dbEntity);

                return PartialView(model);
            }
        }

        [FPCSAuthorize(Role.Admin, Role.Guardian)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _EditILPPhilosophy(StudentILPPhilosophyEditModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var uow = UnityManager.Resolve<IUnitOfWork>())
                    {
                        var repo = uow.GetRepo<IStudentRepo>();

                        var dbEntity = repo.Get(model.DbUserId);

                        dbEntity.ILPPhilosophy = model.Value;

                        model.Students = model.Students != null ? model.Students.Where(x => x.IsChecked).ToList() : 
                                                                  new List<StudentILPPhilosophyFamilyModel>();

                        foreach (var item in model.Students)
                        {
                            dbEntity = repo.Get(item.StudentId);

                            dbEntity.ILPPhilosophy = model.Value;

                            uow.Commit();
                        }

                        uow.Commit();
                        return JsonRes(new { DbUserId = dbEntity.DbUserId });
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }
            }

            model.Init();
            return PartialView(model);
        }

        public ActionResult _Print(GridOptions options, StudentsListOptions studentsListOptions, Guid? studentId, Guid? teacherId, Int64? fpcsCourseId)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                List<StudentPrintModel> data = GetData(uow, options, studentsListOptions, studentId, teacherId, fpcsCourseId)
                    .Select(x => new StudentPrintModel()
                    {
                        DbUserId = x.DbUserId,
                        Email = x.Email,
                        EnrollmentStatus = x.EnrollmentStatus,
                        EnrollmentDate = x.EnrollmentDate,
                        FullName = x.FullName,
                        Sex = x.Sex,
                        Grade = x.Grade,
                        WithdrawalDate = x.WithdrawalDate,
                        IsLocked = x.IsLocked,
                        Guardians = x.Guardians,
                        ZangleID = x.ZangleID
                    })
                    .ToList();

                TableStudentPrintModel model = new TableStudentPrintModel();
                model.Data = data;

                model.PrintAddress = (ConfigurationManager.AppSettings["PrintAddress"]).ToString();
                model.PrintState = (ConfigurationManager.AppSettings["PrintState"]).ToString();
                model.PrintPhone = (ConfigurationManager.AppSettings["PrintPhone"]).ToString();
                model.PrintFax = (ConfigurationManager.AppSettings["PrintFax"]).ToString();
                model.PrintDateNow = DateTime.Now.ToString("MM/dd/yyyy");

                return PartialView(model);
            }
        }

        private List<StudentMiniModel> GetData(IUnitOfWork uow, GridOptions options, StudentsListOptions studentsListOptions, Guid? studentId, Guid? teacherId, Int64? fpcsCourseId)
        {
            var fltSex = studentsListOptions.Sex;
            var fltGrade = studentsListOptions.Grade;
            var fltEnrollmentStatus = studentsListOptions.EnrollmentStatus;

            var repo = uow.GetRepo<IStudentRepo>();
            var repoGuardian = uow.GetRepo<IGuardianRepo>();
            var repoFamily = uow.GetRepo<IFamilyRepo>();
            var guardians = repoGuardian.GetAll().Where(x => !x.IsDeleted && x.LastName.StartsWith(studentsListOptions.Guardians)).Select(t => t.DbUserId).ToList();
            var families = repoFamily.GetAll().Where(x => !x.IsDeleted && x.Guardians.Any(t => guardians.Contains(t.DbUserId))).Select(t => t.FamilyId).ToList();

            Boolean isDdlFilter = studentId.HasValue || teacherId.HasValue || fpcsCourseId.HasValue && fpcsCourseId.Value != 0;

            List<Guid> studentsFilterIds = new List<Guid>();
            if (isDdlFilter)
            {
                if (studentId.HasValue)
                {
                    studentsFilterIds = new List<Guid>();
                    studentsFilterIds.Add(studentId.Value);
                }
                else
                {
                    var repoStudentPacketCourse = uow.GetRepo<IStudentPacketCourseRepo>();
                    studentsFilterIds = repoStudentPacketCourse.GetStudentsByInstructorAndFPCSCourse(teacherId, fpcsCourseId, Session.GetSchoolYear().SchoolYearId)
                        .Select(x => x.DbUserId)
                        .ToList();
                }
            }

            var dbList = repo.GetByUser(User.UserId, User.Role, SchoolYear.SchoolYearId)
                                .Where(x => ((fltSex.HasValue && x.Sex == fltSex.Value) || !fltSex.HasValue) &&
                                            ((fltGrade.HasValue && x.Grade == fltGrade.Value) || !fltGrade.HasValue) &&
                                            ((fltEnrollmentStatus.HasValue && x.EnrollmentStatus == fltEnrollmentStatus.Value) || !fltEnrollmentStatus.HasValue) &&
                                            ((families.Count == 0 && studentsListOptions.Guardians == null) || (families.Count != 0 && (families.Contains(x.Families.FirstOrDefault().FamilyId)))) &&
                                            ((studentsFilterIds.Count == 0 && !isDdlFilter) || (studentsFilterIds.Count != 0 && studentsFilterIds.Contains(x.DbUserId))))
                                .Select(x => new
                                {
                                    DbUserId = x.DbUserId,
                                    FullName = x.LastName + ", " + x.FirstName,
                                    Email = x.Email,
                                    Sex = x.Sex,
                                    Grade = x.Grade,
                                    ZangleID = x.ZangleID,
                                    EnrollmentStatus = x.EnrollmentStatus,
                                    EnrollmentDate = x.EnrollmentDate,
                                    WithdrawalDate = x.WithdrawalDate,
                                    IsLocked = x.StudentPackets.FirstOrDefault() != null &&
                                                x.StudentPackets.FirstOrDefault().IsLocked ? "Locked" : "Unlocked",
                                    Guardians = x.Families.FirstOrDefault().Guardians.Where(t => !t.IsDeleted).Select(t => t.LastName).Distinct(),
                                    Phone = x.Families.FirstOrDefault().Guardians.Where(t => !t.IsDeleted).Select(t => t.CellPhone).Distinct()
                                })
                                .ToList();
                
            studentsListOptions.Sex = null;
            studentsListOptions.Grade = null;
            studentsListOptions.Guardians = null;
            studentsListOptions.EnrollmentStatus = null;

            var engine = new GridDynamicEngine(options, studentsListOptions);
            var result = engine.ApplySort(engine.ApplyFilter(dbList.AsQueryable())).Select(x => new StudentMiniModel
            {
                DbUserId = x.DbUserId,
                FullName = x.FullName,
                Email = x.Email,
                Sex = x.Sex.GetDescription(),
                Grade = x.Grade.GetDescription(),
                ZangleID = x.ZangleID,
                EnrollmentStatus = x.EnrollmentStatus.HasValue ? x.EnrollmentStatus.Value.GetDescription() : String.Empty,
                WithdrawalDate = x.WithdrawalDate.HasValue ? x.WithdrawalDate.Value.ToString("yyyy/MM/dd") : String.Empty,
                EnrollmentDate = x.EnrollmentDate.HasValue ? x.EnrollmentDate.Value.ToString("yyyy/MM/dd") : String.Empty,
                IsLocked = x.IsLocked,
                Guardians = String.Join(", ", x.Guardians),
                Phone = String.Join(", ", x.Phone)
            })
            .ToList();

            return result;
        }
    }
}
