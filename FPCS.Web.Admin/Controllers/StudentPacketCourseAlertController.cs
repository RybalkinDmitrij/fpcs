﻿using FPCS.Core.jqGrid;
using FPCS.Core.Extensions;
using FPCS.Data;
using FPCS.Data.Enums;
using FPCS.Data.Repo;
using FPCS.Web.Admin.Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FPCS.Data.Entities;
using FPCS.Web.Admin.Models.FPCSStudentPacket;
using FPCS.Web.Admin.Models.GoodAndService;
using FPCS.Web.Admin.Models.StudentPacketCourseAlert;
using FPCS.Web.Admin.Helpers.Access.Models;
using FPCS.Web.Admin.Helpers.Access;

namespace FPCS.Web.Admin.Controllers
{
    /// <summary>
    /// Controller Student Packet Course Alert
    /// </summary>
    public class StudentPacketCourseAlertController : BaseController
    {
        public ActionResult _TableAlert(Int32 studentPacketCourseId)
        {
            TableAlertModel model = new TableAlertModel();

            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IStudentPacketCourseAlertRepo>();

                model.Alerts = repo.GetAll()
                                 .Where(x => x.StudentPacketCourseId == studentPacketCourseId)
                                 .ToList()
                                 .Select(x => new AlertModel(x))
                                 .ToList();
            }

            return PartialView(model);
        }

        public ActionResult _TableAlertDetails(Int32? studentPacketCourseId)
        {
            TableAlertModel model = new TableAlertModel();

            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IStudentPacketCourseAlertRepo>();

                model.Alerts = repo.GetAll()
                                 .Where(x => x.StudentPacketCourseId == studentPacketCourseId)
                                 .ToList()
                                 .Select(x => new AlertModel(x))
                                 .ToList();
            }

            return PartialView(model);
        }

        [HttpGet]
        public ActionResult _DetailAlert(Int64 studentPacketCourseAlertId)
        {
            StudentPacketCourseAlert dbEntity = new StudentPacketCourseAlert();

            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IStudentPacketCourseAlertRepo>();

                dbEntity = repo.Get(studentPacketCourseAlertId);

                if (dbEntity == null) { throw new Exception("Alert not found!"); }
            }

            var model = new DetailAlertModel(dbEntity);

            return PartialView(model);
        }

        [HttpGet]
        public ActionResult _CreateAlert(Int64 studentPacketCourseId)
        {
            var model = new CreateAlertModel(studentPacketCourseId);

            return PartialView(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _CreateAlert(CreateAlertModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var uow = UnityManager.Resolve<IUnitOfWork>())
                    {
                        var repo = uow.GetRepo<IStudentPacketCourseAlertRepo>();

                        var studentPacketCourse = uow.GetRepo<IStudentPacketCourseRepo>().Get(model.StudentPacketCourseId);

                        AccessActionModel access = AccessAction.IsNotLock(studentPacketCourse.StudentPacket, User.Role);
                        if (!access.IsResolve)
                        {
                            throw new Exception(access.Message);
                        }

                        StudentPacketCourseAlert dbEntity = new StudentPacketCourseAlert();
                        dbEntity.StudentPacketCourseId = model.StudentPacketCourseId;
                        dbEntity.DbUserId = Session.GetUser().UserId;
                        dbEntity.Value = model.Value.TrimAndReduce();

                        dbEntity = repo.Add(dbEntity);
                        uow.Commit();

                        return JsonRes(dbEntity.StudentPacketCourseAlertId.ToString());
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }
            }

            return PartialView(model);
        }

        [HttpPost]
        public ActionResult Delete(Int64 id)
        {
            try
            {
                using (var uow = UnityManager.Resolve<IUnitOfWork>())
                {
                    var repo = uow.GetRepo<IStudentPacketCourseAlertRepo>();

                    var alert = repo.Get(id);

                    AccessActionModel access = AccessAction.IsNotLock(alert.StudentPacketCourse.StudentPacket, User.Role);
                    if (!access.IsResolve)
                    {
                        throw new Exception(access.Message);
                    }
                    
                    repo.Remove(id);

                    uow.Commit();
                    return JsonRes();
                }
            }
            catch (Exception ex)
            {
                return JsonRes(Status.Error, ex.Message);
            }
        }
    }
}
