﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Data.Entity;
using System.Collections.Generic;
using FPCS.Core.Extensions;
using FPCS.Core.jqGrid;
using FPCS.Data;
using FPCS.Data.Enums;
using FPCS.Data.Repo;
using FPCS.Data.Entities;
using FPCS.Web.Admin.Code;
using FPCS.Web.Admin.Models.FPCSCourse;
using FPCS.Web.Admin.Models.GoodAndService;
using System.Configuration;
using FPCS.Web.Admin.Helpers.Access;
using FPCS.Web.Admin.Helpers.Access.Models;
using FPCS.Web.Admin.Helpers.FPCSCourses;
using FPCS.Web.Admin.Helpers.BudgetStudent;

namespace FPCS.Web.Admin.Controllers
{
	[FPCSAuthorize(Role.Admin, Role.Teacher, Role.Guardian)]
	public class FPCSCourseController : BaseController
	{
		public ActionResult Index()
		{
			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				IndexModel model = new IndexModel();
				model.SubjectSearchOptions = ":All";

				var repo = uow.GetRepo<ISubjectRepo>();
				var data = repo.GetAll().ToList();

				foreach (var item in data)
				{
					model.SubjectSearchOptions += String.Format(@";{0}:{1}", item.SubjectId, item.Name);
				}

				return View(model);
			}
		}

		public JsonResult _Index(GridOptions options, FPCSCoursesListOptions coursesListOptions)
		{
			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				Int32? scheduleRepetition = coursesListOptions.ScheduleRepetition;
				coursesListOptions.ScheduleRepetition = null;

				Int32? subject = coursesListOptions.Subject;
				coursesListOptions.Subject = null;

				var repo = uow.GetRepo<IFPCSCourseRepo>();
				var dbList = repo.GetBySchoolYearAndUser(SchoolYear.SchoolYearId, User.UserId)
					.Include(x => x.ASDCourse)
					.Include(x => x.ASDCourse.Subject)
					.Where(x => ((scheduleRepetition.HasValue && (Int32) x.ScheduleRepetition == scheduleRepetition.Value) ||
					             (!scheduleRepetition.HasValue)) &&
					            ((subject.HasValue && (Int32) x.ASDCourse.SubjectId == subject.Value) ||
					             (!subject.HasValue)));

				var dbStudentPacketCourses = uow.GetRepo<IStudentPacketCourseRepo>().GetAll(SchoolYear.SchoolYearId).ToList();

				var engine = new GridDynamicEngine(options, coursesListOptions);
				var result = engine.ApplyAll2(dbList, x => new FPCSCourseMiniModel
				{
					FPCSCourseId = x.FPCSCourseId,
					Name = x.Name,
					ClassType = x.ClassType.HasValue ? x.ClassType.Value.GetDescription2Additional() : String.Empty,
					Subject = x.ASDCourse.Subject.Name,
					AvailableSpots =
						dbStudentPacketCourses.Where(t => t.FPCSCourseId == x.FPCSCourseId).Count().ToString() + " of " +
						x.MaxStudentsCount.ToString(),
					GradCredit = x.ASDCourse.GradCredit,
					IsActivated = x.IsActivated ? "Activated" : "Not Activated"
				});

				return Json(result);
			}
		}

		public ActionResult _PreDetails(Int32 id)
		{
			PreDetailsModel model = new PreDetailsModel(id);

			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				var balances = uow.GetRepo<IGoodServiceBalanceRepo>()
					.GetByStudentPacketCourseOrFPCSCourse(0, model.CourseId)
					.ToList();

				model.TableGoodAndServiceModel.GoodsAndServices = uow.GetRepo<IGoodServiceRepo>()
					.GetByFPCSCourse(model.CourseId)
					.ToList()
					.Select(x => new GoodAndServiceModel(x, balances))
					.ToList();

				var repo = uow.GetRepo<IFPCSCourseRepo>();
				var query = repo.GetAsQuery(id);

				var detailModel = query.Select(x => new FPCSCourseDetailsModel
				{
					FPCSCourseId = x.FPCSCourseId,
					ASDCourse = x.ASDCourse.Subject.Name,
					ASDCourseCredit = x.ASDCourse.GradCredit,
					ASDCourseDescription = x.ASDCourse.Description,
					Teacher = x.Teacher.FullName,
					Guardian = x.Guardian.FullName,
					Vendor = x.Vendor.LastName + ", " + x.Vendor.FirstName,
					Name = x.Name,
					Location = x.Location,
					ClassType = x.ClassType,
					ClassStartDate = x.ClassStartDate,
					ClassEndDate = x.ClassEndDate,
					Semester = x.Semester,
					IsActivated = x.IsActivated
				}).FirstOrDefault();

				if (detailModel == null) return ErrorPartial("FPCS Course {0} not found", id);

				detailModel.ClassTypeText = detailModel.ClassType.HasValue
					? detailModel.ClassType.Value.GetDescription()
					: String.Empty;

				detailModel.ASDCourse =
					String.Format(@"{0} - {1} - {2}",
						detailModel.ASDCourse,
						detailModel.ASDCourseCredit,
						detailModel.ASDCourseDescription.Length > 50
							? detailModel.ASDCourseDescription.Substring(0, 50)
							: detailModel.ASDCourseDescription);

				model.FPCSCourseDetailsModel = detailModel;

				return PartialView(model);
			}
		}

		[HttpGet]
		public ActionResult _Details(Int32 id)
		{
			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				var repo = uow.GetRepo<IFPCSCourseRepo>();
				var query = repo.GetAsQuery(id);

				var model = query.Select(x => new FPCSCourseDetailsModel
				{
					FPCSCourseId = x.FPCSCourseId,
					ASDCourse = x.ASDCourse.Subject.Name,
					ASDCourseCredit = x.ASDCourse.GradCredit,
					ASDCourseDescription = x.ASDCourse.Description,
					Teacher = x.Teacher.FullName,
					Guardian = x.Guardian.FullName,
					Vendor = x.Vendor.LastName + ", " + x.Vendor.FirstName,
					Name = x.Name,
					Location = x.Location,
					ClassType = x.ClassType,
					ClassStartDate = x.ClassStartDate,
					ClassEndDate = x.ClassEndDate,
					Semester = x.Semester,
					IsActivated = x.IsActivated
				}).FirstOrDefault();

				if (model == null) return ErrorPartial("FPCS Course {0} not found", id);

				model.ClassTypeText = model.ClassType.HasValue ? model.ClassType.Value.GetDescription() : String.Empty;

				model.ASDCourse =
					String.Format(@"{0} - {1} - {2}",
						model.ASDCourse,
						model.ASDCourseCredit,
						model.ASDCourseDescription.Length > 50
							? model.ASDCourseDescription.Substring(0, 50)
							: model.ASDCourseDescription);

				return PartialView(model);
			}
		}

		[HttpGet]
		public ActionResult _MegaCreate(Int64? studentPacketId, Semester? semester)
		{
			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				var repo = uow.GetRepo<IStudentPacketRepo>();
				StudentPacket dbStudentPacket = studentPacketId.HasValue ? repo.Get(studentPacketId.Value) : null;

				Guid? studentId = dbStudentPacket != null ? (Guid?) dbStudentPacket.StudentId : null;

				var model = new MegaCreateModel(uow, studentPacketId, studentId, semester, User, SchoolYear);

				return PartialView(model);
			}
		}

		[HttpPost]
		public ActionResult _MegaCreate(PostMegaCreateModel model)
		{
			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				//if (model.TeacherId == null && model.GuardianId == null && model.VendorId == null)
				//{
				//	ModelState.AddModelError("", "Must be select teacher or guardian or vendor");
				//}

				if (String.IsNullOrEmpty(model.Location))
				{
					model.Location = String.Empty;
				}

				if (ModelState.IsValid)
				{
					try
					{
						Boolean isFirst = true;
						Int64 fpcsCourseId = 0;
						Semester semester = Semester.Semester1;

						model.IsActivated = true;

						foreach (var item in model.SemesterNumbers)
						{
							// Step 1: Create INFO course
							var repo = uow.GetRepo<IFPCSCourseRepo>();

							var dbEntity = repo.Add(model.ASDCourseId, model.TeacherId, model.GuardianId, model.VendorId, model.Name,
								model.GradeLevel, model.CourseSubjectId, model.Location, model.ClassType, model.SubjectId, model.IsActivated,
								(Semester) item);

							uow.Commit();

							if (isFirst)
							{
								fpcsCourseId = dbEntity.FPCSCourseId;
								semester = (Semester) item;
								isFirst = false;
							}

							// Step 2: Create ILP
							var repoILP = uow.GetRepo<IILPRepo>();
							var repoBank = uow.GetRepo<IILPBankRepo>();
							var repoSchoolYear = uow.GetRepo<ISchoolYearRepo>();

							ILP dbEntityILP = new ILP();

							dbEntityILP.ILPId = model.ILPId;
							dbEntityILP.ILPName = model.ILPName.TrimAndReduce();
							dbEntityILP.Instructor = model.Instructor != null ? model.Instructor : "";
							dbEntityILP.StudentPacketCourseId = null;
							dbEntityILP.FPCSCourseId = dbEntity.FPCSCourseId;

							dbEntityILP.DescriptionCourse = model.DescriptionCourse.TrimAndReduce();
							dbEntityILP.EvaluationGradingPassFail = model.EvaluationGradingPassFail;
							dbEntityILP.EvaluationGradingGradingScale = model.EvaluationGradingGradingScale;
							dbEntityILP.EvaluationGradingOSN = model.EvaluationGradingOSN;
							dbEntityILP.EvaluationGradingOther = model.EvaluationGradingOther;
							dbEntityILP.EvaluationGradingOtherExplain = model.EvaluationGradingOtherExplain.TrimAndReduce();
							dbEntityILP.CourseHrs = 0;

							if (model.ILPId == 0)
							{
								dbEntityILP = repoILP.Add(dbEntityILP);
							}
							else
							{
								repoILP.Update(dbEntityILP);
							}

							uow.Commit();

							FPCSCourse fpcsCourse = dbEntityILP.FPCSCourseId.HasValue
								? uow.GetRepo<IFPCSCourseRepo>().Get(dbEntityILP.FPCSCourseId.Value)
								: null;

							var teacherGuid = fpcsCourse.TeacherId;

							if (model.AddILPToBank && teacherGuid.HasValue)
							{
								ILPBank ilpBank = null;

								if (ilpBank == null)
								{
									ilpBank = new ILPBank();
								}

								SchoolYear schoolYear = repoSchoolYear.Get(SchoolYear.SchoolYearId);

								ilpBank.SchoolYearId = schoolYear.SchoolYearId;
								ilpBank.SubjectId = fpcsCourse.ASDCourse.SubjectId;
								ilpBank.TeacherId = fpcsCourse.TeacherId;
								ilpBank.GuardianId = null;

								ilpBank.ILPId = dbEntityILP.ILPId;
								ilpBank.ILPName = dbEntityILP.ILPName.TrimAndReduce();
								ilpBank.IsPublic = false;

								ilpBank.CourseHrs = dbEntityILP.CourseHrs;
								ilpBank.DescriptionCourse = dbEntityILP.DescriptionCourse.TrimAndReduce();
								ilpBank.Standards = dbEntityILP.Standards.TrimAndReduce();
								ilpBank.StudentActivities = dbEntityILP.StudentActivities.TrimAndReduce();
								ilpBank.MaterialsResources = dbEntityILP.MaterialsResources.TrimAndReduce();
								ilpBank.RoleAnyPeople = dbEntityILP.RoleAnyPeople.TrimAndReduce();
								ilpBank.EvaluationGradingPassFail = dbEntityILP.EvaluationGradingPassFail;
								ilpBank.EvaluationGradingGradingScale = dbEntityILP.EvaluationGradingGradingScale;
								ilpBank.EvaluationGradingOSN = dbEntityILP.EvaluationGradingOSN;
								ilpBank.EvaluationGradingOther = dbEntityILP.EvaluationGradingOther;
								ilpBank.EvaluationGradingOtherExplain = dbEntityILP.EvaluationGradingOtherExplain.TrimAndReduce();
								ilpBank.EvaluatedMeasurableOutcomes = dbEntityILP.EvaluatedMeasurableOutcomes.TrimAndReduce();
								ilpBank.CourseSyllabus = dbEntityILP.CourseSyllabus.TrimAndReduce();
								ilpBank.GuardianILPModifications = dbEntityILP.GuardianILPModifications.TrimAndReduce();
								ilpBank.InstructorILPModifications = dbEntityILP.InstructorILPModifications.TrimAndReduce();

								if (ilpBank.ILPBankId == 0)
								{
									ilpBank = repoBank.Add(ilpBank);
								}
								else
								{
									repoBank.Update(ilpBank);
								}

								uow.Commit();
							}

							// Step 3: Create good/service
							var repoGS = uow.GetRepo<IGoodServiceRepo>();
							var gsData = model.tempId.HasValue ? repoGS.GetByTemp(model.tempId.Value).ToList() : new List<GoodService>();

							foreach (var itemGS in gsData)
							{
								GoodService newGS = new GoodService();
								newGS.IsDeleted = dbEntity.IsDeleted;
								newGS.CreatedDate = DateTimeOffset.Now;
								newGS.UpdatedDate = DateTimeOffset.Now;
								newGS.StudentPacketCourseId = null;
								newGS.FPCSCourseId = dbEntity.FPCSCourseId;
								newGS.StudentPacketId = null;
								newGS.TempId = "";
								newGS.GoodOrService = itemGS.GoodOrService;
								newGS.RequisitionOrReimbursement = itemGS.RequisitionOrReimbursement;
								newGS.TypeRequisitionReimbursement = itemGS.TypeRequisitionReimbursement;
								newGS.VendorId = itemGS.VendorId;

								newGS.AdminComments = itemGS.AdminComments;
								newGS.ApprovalGivenBy = itemGS.ApprovalGivenBy;
								newGS.NumberOfUnits = itemGS.NumberOfUnits;
								newGS.UnitPrice = itemGS.UnitPrice;
								newGS.ShippingHandlingFees = itemGS.ShippingHandlingFees;
								newGS.LineItemName = itemGS.LineItemName;
								newGS.LineItemDesc = itemGS.LineItemDesc;
								newGS.SupplyType = itemGS.SupplyType;
								newGS.Count = itemGS.Count;
								newGS.CatalogMFG = itemGS.CatalogMFG;
								newGS.CatalogIssuePG = itemGS.CatalogIssuePG;
								newGS.UnitType = itemGS.UnitType;
								newGS.BarCode = itemGS.BarCode;
								newGS.Consumable = itemGS.Consumable;
								newGS.TypeTextbookCurriculum = itemGS.TypeTextbookCurriculum;
								newGS.Title = itemGS.Title;
								newGS.Author = itemGS.Author;
								newGS.PublisherISBN = itemGS.PublisherISBN;
								newGS.PublisherName = itemGS.PublisherName;
								newGS.Edition = itemGS.Edition;
								newGS.HighSchool = itemGS.HighSchool;
								newGS.NumberSemesters = itemGS.NumberSemesters;
								newGS.CostPerSemester = itemGS.CostPerSemester;
								newGS.ClassType = itemGS.ClassType;
								newGS.CourseTitle = itemGS.CourseTitle;
								newGS.CourseNumber = itemGS.CourseNumber;
								newGS.DateBirth = itemGS.DateBirth;
								newGS.Credits = itemGS.Credits;
								newGS.Semester = itemGS.Semester;
								newGS.TypeService = itemGS.TypeService;
								newGS.Description = itemGS.Description;
								newGS.BeginDate = itemGS.BeginDate;
								newGS.EndDate = itemGS.EndDate;
								newGS.VendorUnitType = itemGS.VendorUnitType;
								newGS.ComputerLeaseItemType = itemGS.ComputerLeaseItemType;
								newGS.ComputerLeaseItemDesc = itemGS.ComputerLeaseItemDesc;
								newGS.DetailedItemDesc = itemGS.DetailedItemDesc;
								newGS.InternetItemType = itemGS.InternetItemType;
								newGS.GoodOrServiceApproved = itemGS.GoodOrServiceApproved;
								newGS.CommentsRejectReason = itemGS.CommentsRejectReason;
								newGS.IsClosed = itemGS.IsClosed;
								newGS.UserRequestId = itemGS.UserRequestId;
								newGS.DateRequest = itemGS.DateRequest;

								repoGS.Add(newGS);
							}

							uow.Commit();

							// Step 4: adding to student packet
							// if student packet id not equal null or 0, then check must be course into student packet
							if (model.StudentPacketId.HasValue)
							{
								var repoStudentPacket = uow.GetRepo<IStudentPacketRepo>();
								var dbStudentPacket = repoStudentPacket.Get(model.StudentPacketId);

								if (
									!BudgetStudentHelper.IsResolveFPCSCourse(dbStudentPacket.StudentId, SchoolYear.SchoolYearId,
										dbEntity.FPCSCourseId))
								{
									repo.Remove(dbEntity);
									uow.Commit();

									return JsonRes(Status.NoEnoughBudget, BudgetStudentHelper.MessageNoEnoughBudget);
								}

								var fpcsCourseIds = uow.GetRepo<IFPCSCourseRepo>()
									.GetNotSelectedInPacketCourses(dbStudentPacket.StudentId, dbStudentPacket.StudentPacketId,
										SchoolYear.SchoolYearId)
									.Select(x => x.FPCSCourseId)
									.ToList();

								if (fpcsCourseIds.Contains(dbEntity.FPCSCourseId))
								{
									FPCSCourseHandler.AddFPCSStudentPacketCourse(dbEntity.FPCSCourseId, dbStudentPacket.StudentPacketId,
										Session.GetUser().Role);
								}
							}
						}

						return JsonRes(new {FPCSCourseId = fpcsCourseId.ToString(), Semester = semester});
					}
					catch (Exception ex)
					{
						ModelState.AddModelError("", ex.Message);
					}
				}

				var returnModel = new MegaCreateModel(
					uow,
					model.StudentPacketId,
					model.StudentId,
					(Semester) model.SemesterNumbers.FirstOrDefault(),
					User,
					SchoolYear);

				return PartialView(returnModel);
			}
		}

		[HttpGet]
		public ActionResult _Create(Int64? studentPacketId, Semester? semester)
		{
			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				var repo = uow.GetRepo<IStudentPacketRepo>();
				StudentPacket dbStudentPacket = studentPacketId.HasValue ? repo.Get(studentPacketId.Value) : null;

				Guid? studentId = dbStudentPacket != null ? (Guid?) dbStudentPacket.StudentId : null;

				var model = new FPCSCourseCreateModel(uow, studentPacketId, studentId, semester, User);
				model.Init(SchoolYear.SchoolYearId, User.UserId, User.Role, studentId);
				return PartialView(model);
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult _Create(FPCSCourseCreateModel model)
		{
			if (model.TeacherId == null && model.GuardianId == null && model.VendorId == null)
			{
				ModelState.AddModelError("", "Must be select teacher or guardian or vendor");
			}

			if (model.ASDCourseId == 0)
			{
				ModelState.AddModelError("", "Must be select ASD course");
			}

			if (String.IsNullOrEmpty(model.Location))
			{
				model.Location = String.Empty;
			}

			if (ModelState.IsValid)
			{
				try
				{
					using (var uow = UnityManager.Resolve<IUnitOfWork>())
					{
						Boolean isFirst = true;
						Int64 fpcsCourseId = 0;
						Semester semester = Semester.Semester1;

						foreach (var item in model.SemesterNumbers)
						{
							var repo = uow.GetRepo<IFPCSCourseRepo>();

							var dbEntity = repo.Add(model.ASDCourseId, model.TeacherId, model.GuardianId, model.VendorId, model.Name,
								model.GradeLevel, model.CourseSubjectId, model.Location, model.ClassType, model.SubjectId,
								model.IsActivated, (Semester) item);

							uow.Commit();

							if (isFirst)
							{
								fpcsCourseId = dbEntity.FPCSCourseId;
								semester = (Semester) item;
								isFirst = false;
							}

							// if student packet id not equal null or 0, then check must be course into student packet
							if (model.StudentPacketId.HasValue)
							{
								var repoStudentPacket = uow.GetRepo<IStudentPacketRepo>();
								var dbStudentPacket = repoStudentPacket.Get(model.StudentPacketId);

								if (
									!BudgetStudentHelper.IsResolveFPCSCourse(dbStudentPacket.StudentId, SchoolYear.SchoolYearId,
										dbEntity.FPCSCourseId))
								{
									repo.Remove(dbEntity);
									uow.Commit();

									return JsonRes(Status.NoEnoughBudget, BudgetStudentHelper.MessageNoEnoughBudget);
								}

								var fpcsCourseIds = uow.GetRepo<IFPCSCourseRepo>()
									.GetNotSelectedInPacketCourses(dbStudentPacket.StudentId, dbStudentPacket.StudentPacketId,
										SchoolYear.SchoolYearId)
									.Select(x => x.FPCSCourseId)
									.ToList();

								if (fpcsCourseIds.Contains(dbEntity.FPCSCourseId))
								{
									FPCSCourseHandler.AddFPCSStudentPacketCourse(dbEntity.FPCSCourseId, dbStudentPacket.StudentPacketId,
										Session.GetUser().Role);
								}
							}
						}

						return JsonRes(new {FPCSCourseId = fpcsCourseId.ToString(), Semester = semester});
					}
				}
				catch (Exception ex)
				{
					ModelState.AddModelError("", ex.Message);
				}
			}

			model.Init(SchoolYear.SchoolYearId, User.UserId, User.Role, model.StudentId);
			return PartialView(model);
		}

		public ActionResult _PreEdit(Int32 id)
		{
			PreEditModel model = new PreEditModel(id);

			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				var balances = uow.GetRepo<IGoodServiceBalanceRepo>()
					.GetByStudentPacketCourseOrFPCSCourse(0, model.CourseId)
					.ToList();

				model.TableGoodAndServiceModel.GoodsAndServices = uow.GetRepo<IGoodServiceRepo>()
					.GetByFPCSCourse(model.CourseId)
					.ToList()
					.Select(x => new GoodAndServiceModel(x, balances))
					.ToList();

				var repo = uow.GetRepo<IFPCSCourseRepo>();
				var query = repo.GetAsQuery(id);

				var editModel = query.Select(x => new FPCSCourseEditModel
				{
					FPCSCourseId = x.FPCSCourseId,
					CourseSubjectId = x.CourseSubjectId,
					GradeLevel = x.GradeLevel,
					ASDCourseId = x.ASDCourseId,
					ASDCourseCredit = x.ASDCourse.GradCredit,
					ASDCourseDescription = x.ASDCourse.Description,
					TeacherId = x.TeacherId,
					GuardianId = x.GuardianId,
					VendorId = x.VendorId,
					TeacherName = x.TeacherId.HasValue ? x.Teacher.LastName + ", " + x.Teacher.FirstName : String.Empty,
					GuardianName = x.GuardianId.HasValue ? x.Guardian.LastName + ", " + x.Guardian.FirstName : String.Empty,
					VendorName = x.VendorId.HasValue ? x.Vendor.LastName + ", " + x.Vendor.FirstName : String.Empty,
					Name = x.Name,
					Location = x.Location,
					ClassType = x.ClassType,
					ClassStartDate = x.ClassStartDate,
					ClassEndDate = x.ClassEndDate,
					SemesterNumber = x.Semester,
					SubjectId = x.SubjectId,
					IsActivated = x.IsActivated
				}).FirstOrDefault();

				if (editModel == null) return ErrorPartial("FPCS Course {0} not found", id);
				editModel.Init(SchoolYear.SchoolYearId, User.UserId, User.Role);

				var repoILP = uow.GetRepo<IILPRepo>();
				var dbILP = repoILP.GetByFPCSCourse(editModel.FPCSCourseId);
				editModel.ILPName = dbILP != null ? dbILP.ILPName : String.Empty;

				model.FPCSCourseEditModel = editModel;

				return PartialView(model);
			}
		}

		[HttpGet]
		public ActionResult _Edit(Int32 id)
		{
			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				var repo = uow.GetRepo<IFPCSCourseRepo>();
				var query = repo.GetAsQuery(id);

				var model = query.Select(x => new FPCSCourseEditModel
				{
					FPCSCourseId = x.FPCSCourseId,
					CourseSubjectId = x.CourseSubjectId,
					GradeLevel = x.GradeLevel,
					ASDCourseId = x.ASDCourseId,
					ASDCourseCredit = x.ASDCourse.GradCredit,
					ASDCourseDescription = x.ASDCourse.Description,
					TeacherId = x.TeacherId,
					GuardianId = x.GuardianId,
					VendorId = x.VendorId,
					TeacherName = x.TeacherId.HasValue ? x.Teacher.LastName + ", " + x.Teacher.FirstName : String.Empty,
					GuardianName = x.GuardianId.HasValue ? x.Guardian.LastName + ", " + x.Guardian.FirstName : String.Empty,
					VendorName = x.VendorId.HasValue ? x.Vendor.LastName + ", " + x.Vendor.FirstName : String.Empty,
					Name = x.Name,
					Location = x.Location,
					ClassType = x.ClassType,
					ClassStartDate = x.ClassStartDate,
					ClassEndDate = x.ClassEndDate,
					SemesterNumber = x.Semester,
					SubjectId = x.SubjectId,
					IsActivated = x.IsActivated
				}).FirstOrDefault();

				if (model == null) return ErrorPartial("FPCS Course {0} not found", id);
				model.Init(SchoolYear.SchoolYearId, User.UserId, User.Role);

				var repoILP = uow.GetRepo<IILPRepo>();
				var dbILP = repoILP.GetByFPCSCourse(model.FPCSCourseId);
				model.ILPName = dbILP != null ? dbILP.ILPName : String.Empty;
				
				return PartialView(model);
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult _Edit(FPCSCourseEditModel model)
		{
			if (String.IsNullOrEmpty(model.Location))
			{
				model.Location = String.Empty;
			}

			if (ModelState.IsValid)
			{
				try
				{
					using (var uow = UnityManager.Resolve<IUnitOfWork>())
					{
						var repo = uow.GetRepo<IFPCSCourseRepo>();
						var repoILP = uow.GetRepo<IILPRepo>();

						var dbEntity = repo.Update(model.FPCSCourseId, model.ASDCourseId, model.TeacherId, model.GuardianId,
							model.VendorId, model.Name, model.GradeLevel, model.CourseSubjectId, model.Location, model.ClassType,
							model.SubjectId, model.IsActivated, model.SemesterNumber);

						uow.Commit();

						var dbILP = repoILP.GetByFPCSCourse(dbEntity.FPCSCourseId);
                        if (dbILP == null)
                        {
                            dbILP = new ILP("");
                        }
						dbILP.StudentPacketCourseId = null;
						dbILP.FPCSCourseId = dbEntity.FPCSCourseId;
						dbILP.ILPName = model.ILPName != null ? model.ILPName.TrimAndReduce() : "";
						
						if (dbILP.ILPId == 0) repoILP.Add(dbILP);
						else repoILP.Update(dbILP);

						uow.Commit();

						return JsonRes(new {FPCSCourseId = dbEntity.FPCSCourseId.ToString(), Semester = dbEntity.Semester});
					}
				}
				catch (Exception ex)
				{
					ModelState.AddModelError("", ex.Message);
				}
			}

			model.Init(SchoolYear.SchoolYearId, User.UserId, User.Role);
			return PartialView(model);
		}

		[HttpPost]
		public ActionResult Delete(Int32 id)
		{
			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				var repo = uow.GetRepo<IFPCSCourseRepo>();
				repo.Remove(id);

				uow.Commit();
				return JsonRes();
			}
		}

		[HttpPost]
		public ActionResult DeleteAll(String id)
		{
			if (!String.IsNullOrEmpty(id))
			{
				var ids = id.Split(',');
				using (var uow = UnityManager.Resolve<IUnitOfWork>())
				{
					var repo = uow.GetRepo<IFPCSCourseRepo>();
					foreach (var item in ids)
					{
						repo.Remove(Int32.Parse(item));
					}

					uow.Commit();
					return JsonRes();
				}
			}
			return JsonRes(Status.Error, "Error");
		}

		[HttpGet]
		public ActionResult _Print(Int32 id)
		{
			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				var repo = uow.GetRepo<IFPCSCourseRepo>();
				var query = repo.GetAsQuery(id);

				var model = query.Select(x => new FPCSCoursePrintModel
				{
					FPCSCourseId = x.FPCSCourseId,
					ASDCourse = x.ASDCourse.Subject.Name,
					ASDCourseID = x.ASDCourse.ExternalASDCourseId,
					ASDCourseCredit = x.ASDCourse.GradCredit,
					ASDCourseDescription = x.ASDCourse.Description,
					ASDCourseSubject = x.ASDCourse.Subject.Name,
					Name = x.Name,
					Location = x.Location,
					ClassStartDate = x.ClassStartDate,
					ClassEndDate = x.ClassEndDate,
					Semester = x.Semester,
					IsActivated = x.IsActivated
				}).FirstOrDefault();

				if (model == null) return ErrorPartial("FPCS Course {0} not found", id);

				model.ASDCourse =
					String.Format(@"{0} - {1} - {2}",
						model.ASDCourse,
						model.ASDCourseCredit,
						model.ASDCourseDescription.Length > 50
							? model.ASDCourseDescription.Substring(0, 50)
							: model.ASDCourseDescription);

				model.PrintAddress = (ConfigurationManager.AppSettings["PrintAddress"]).ToString();
				model.PrintState = (ConfigurationManager.AppSettings["PrintState"]).ToString();
				model.PrintPhone = (ConfigurationManager.AppSettings["PrintPhone"]).ToString();
				model.PrintFax = (ConfigurationManager.AppSettings["PrintFax"]).ToString();
				model.PrintDateNow = DateTime.Now.ToString("MM/dd/yyyy");
				model.Name = model.Name;

				return PartialView(model);
			}
		}
	}
}