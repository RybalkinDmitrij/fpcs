﻿using FPCS.Core.jqGrid;
using FPCS.Data;
using FPCS.Data.Enums;
using FPCS.Data.Repo;
using FPCS.Web.Admin.Code;
using FPCS.Web.Admin.Models.Instructor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FPCS.Core.Extensions;

namespace FPCS.Web.Admin.Controllers
{
    public class InstructorController : BaseController
    {
        [FPCSAuthorize(Role.Admin, Role.Teacher)]
        public ActionResult Index()
        {
            return View();
        }

        [FPCSAuthorize(Role.Admin, Role.Teacher)]
        public JsonResult _Index(GridOptions options, InstructorsListOptions instructorsListOptions)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IStudentPacketCourseRepo>();

                var dbList = repo.GetByInstructor(Session.GetUser().Role == Role.Teacher ? Session.GetUser().UserId : Guid.Empty, SchoolYear.SchoolYearId)
                                 .Select(x => new
                {
                    StudentPacketCourseId = x.StudentPacketCourseId,
                    ClassName = x.FPCSCourse.Name,
                    StudentId = x.StudentPacket.Student.DbUserId,
                    LastName = x.StudentPacket.Student.LastName,
                    FirstName = x.StudentPacket.Student.FirstName,
                    GuardSign = x.GuardianSignature,
                    GuardComment = x.GuardianComment,
                    GuardName = x.Guardian != null ? x.Guardian.FirstName : String.Empty,
                    SponsorSign = x.SponsorSignature,
                    SponsorComment = x.SponsorComment,
                    SponsorName = x.Sponsor != null ? x.Sponsor.LastName : String.Empty,
                    InstructSign = x.InstructorSignature,
                    InstructComment = x.InstructorComment,
                    InstructName = x.Instructor != null ? x.Instructor.LastName : String.Empty,
                    AdminSign = x.AdminSignature,
                    AdminComment = x.AdminComment,
                    AdminName = x.Admin != null ? x.Admin.LastName : String.Empty,
                    FamilyId = x.StudentPacket.Student.Families.FirstOrDefault() != null ? 
                                x.StudentPacket.Student.Families.FirstOrDefault().FamilyId : 0,
                    FamilyEmail = x.StudentPacket.Student.Families.FirstOrDefault() != null ? 
                                    x.StudentPacket.Student.Families.FirstOrDefault().Email : String.Empty,
                    FamilyPhone = x.StudentPacket.Student.Families.FirstOrDefault() != null ? 
                                    x.StudentPacket.Student.Families.FirstOrDefault().Telephone : String.Empty,
                })
                .ToList();

                var dbListGuardian = uow.GetRepo<IGuardianRepo>()
                    .GetAll()
                    .Where(x => x.Families.FirstOrDefault(t => !t.IsDeleted) != null)
                    .Select(x => new
                    {
                        Name = x.FirstName,
                        FamilyId = x.Families.FirstOrDefault(t => !t.IsDeleted).FamilyId
                    })
                    .ToList();

                var result = dbList.Select(x => new InstructorMiniModel
                {
                    StudentPacketCourseId = x.StudentPacketCourseId,
                    ClassName = x.ClassName,
                    StudentName = String.Format(@"{0}, {1}", x.LastName, x.FirstName),
                    Guardians =
                        String.Join(@", ",
                            dbListGuardian.Where(t => t.FamilyId == x.FamilyId)
                                .Select(t => t.Name)),
                    Email = x.FamilyEmail,
                    FamilyPhone = x.FamilyPhone,
                    Comments = String.Format(@"Guardians: {0}<br />Sponsor: {1}<br />Instructor: {2}<br />Admin: {3}", x.GuardComment, x.SponsorComment, x.InstructComment, x.AdminComment),
                    GuardSign = x.GuardSign.HasValue ? String.Format(@"{0}<br />{1}", x.GuardSign.Value.GetDescription(), x.GuardName) : "not signed",
                    SponsorSign = x.SponsorSign.HasValue ? String.Format(@"{0}<br />{1}", x.SponsorSign.Value.GetDescription(), x.SponsorName) : "not signed",
                    InstructSign = x.InstructSign.HasValue ? String.Format(@"{0}<br />{1}", x.InstructSign.Value.GetDescription(), x.InstructName) : "not signed",
                    AdminSign = x.AdminSign.HasValue ? String.Format(@"{0}<br />{1}", x.AdminSign.Value.GetDescription(), x.AdminName) : "not signed"
                });

                return Json(result);
            }
        }
    }
}
