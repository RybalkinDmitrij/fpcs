﻿using FPCS.Core.jqGrid;
using FPCS.Data;
using FPCS.Data.Entities;
using FPCS.Data.Enums;
using FPCS.Data.Repo;
using FPCS.Web.Admin.Code;
using FPCS.Web.Admin.Helpers.Access;
using FPCS.Web.Admin.Helpers.Access.Models;
using FPCS.Web.Admin.Models.ILPBank;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FPCS.Web.Admin.Controllers
{
    [FPCSAuthorize(Role.Admin, Role.Teacher, Role.Guardian)]
    public class ILPBankController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult _Index(GridOptions options, ILPBankListOptions ilpBankListOptions)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IILPBankRepo>();
                var dbList = repo.GetAll();

                if (ilpBankListOptions.IsPublic.HasValue)
                {
                    dbList = dbList.Where(x => x.IsPublic == ilpBankListOptions.IsPublic.Value);
                }

                var engine = new GridDynamicEngine(options, ilpBankListOptions);
                var result = engine.ApplyAll(dbList, x => new ILPBankMiniModel
                {
                    ILPBankId = x.ILPBankId,
                    Name = x.ILPName,
                    TeacherId = x.TeacherId.HasValue ? x.TeacherId : null,
                    Teacher = x.Teacher != null ?
                        x.Teacher.LastName + ", " + x.Teacher.FirstName :
                        String.Empty,
                    GuardianId = x.GuardianId.HasValue ? x.GuardianId : null,
                    Guardian = x.Guardian != null ?
                        x.Guardian.LastName + ", " + x.Guardian.FirstName :
                        String.Empty,
                    Subject = x.SubjectId.HasValue ? 
                                x.Subject.Name :
                                String.Empty,
                    Year = x.SchoolYear.Year,
                    IsPublic = x.IsPublic ? "Public" : "Private"
                });

                return Json(result);
            }
        }

        [HttpGet]
        public ActionResult _Details(Int32 id)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IILPBankRepo>();
                var model = repo.GetAsQuery(id)
                                .Select(x => new ILPBankDetailsModel()
                                {
                                    ILPBankId = x.ILPBankId,
                                    ILPId = x.ILPId,
                                    ILPName = x.ILPName,
                                    TeacherName = x.Teacher != null ?
                                        x.Teacher.LastName + ", " + x.Teacher.FirstName :
                                        String.Empty,
                                    GuardianName = x.Guardian != null ?
                                        x.Guardian.LastName + ", " + x.Guardian.FirstName :
                                        String.Empty,
                                    Instructor = x.Teacher != null ?
                                        x.Teacher.LastName + ", " + x.Teacher.FirstName :
                                        x.Guardian.LastName + ", " + x.Guardian.FirstName,
                                    PublicOrPrivate = x.IsPublic ? "Public" : "Private",
                                    SchoolYear = x.SchoolYear != null ?
                                        x.SchoolYear.Year :
                                        0,
                                    CourseHrs = x.CourseHrs,
                                    DescriptionCourse = x.DescriptionCourse,
                                    Standards = x.Standards,
                                    StudentActivities = x.StudentActivities,
                                    MaterialsResources = x.MaterialsResources,
                                    RoleAnyPeople = x.RoleAnyPeople,
                                    EvaluationGradingPassFail = x.EvaluationGradingPassFail,
                                    EvaluationGradingGradingScale = x.EvaluationGradingGradingScale,
                                    EvaluationGradingOSN = x.EvaluationGradingOSN,
                                    EvaluationGradingOther = x.EvaluationGradingOther,
                                    EvaluationGradingOtherExplain = x.EvaluationGradingOtherExplain,
                                    EvaluatedMeasurableOutcomes = x.EvaluatedMeasurableOutcomes,
                                    CourseSyllabus = x.CourseSyllabus,
                                    GuardianILPModifications = x.GuardianILPModifications,
                                    InstructorILPModifications = x.InstructorILPModifications
                                })
                                .FirstOrDefault();
                
                return PartialView(model);
            }
        }

        [HttpGet]
        public ActionResult _Edit(Int32 id)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IILPBankRepo>();
                var model = repo.GetAsQuery(id)
                                .Select(x => new ILPBankEditModel()
                                {
                                    ILPBankId = x.ILPBankId,
                                    ILPId = x.ILPId,
                                    ILPName = x.ILPName,
                                    TeacherName = x.Teacher != null ?
                                        x.Teacher.LastName + ", " + x.Teacher.FirstName :
                                        String.Empty,
                                    GuardianName = x.Guardian != null ?
                                        x.Guardian.LastName + ", " + x.Guardian.FirstName :
                                        String.Empty,
                                    Instructor = x.Teacher != null ?
                                        x.Teacher.LastName + ", " + x.Teacher.FirstName :
                                        x.Guardian.LastName + ", " + x.Guardian.FirstName,
                                    IsPublic = x.IsPublic,
                                    SchoolYear = x.SchoolYear != null ?
                                        x.SchoolYear.Year :
                                        0,
                                    CourseHrs = x.CourseHrs,
                                    DescriptionCourse = x.DescriptionCourse,
                                    Standards = x.Standards,
                                    StudentActivities = x.StudentActivities,
                                    MaterialsResources = x.MaterialsResources,
                                    RoleAnyPeople = x.RoleAnyPeople,
                                    EvaluationGradingPassFail = x.EvaluationGradingPassFail,
                                    EvaluationGradingGradingScale = x.EvaluationGradingGradingScale,
                                    EvaluationGradingOSN = x.EvaluationGradingOSN,
                                    EvaluationGradingOther = x.EvaluationGradingOther,
                                    EvaluationGradingOtherExplain = x.EvaluationGradingOtherExplain,
                                    EvaluatedMeasurableOutcomes = x.EvaluatedMeasurableOutcomes,
                                    CourseSyllabus = x.CourseSyllabus,
                                    GuardianILPModifications = x.GuardianILPModifications,
                                    InstructorILPModifications = x.InstructorILPModifications
                                })
                                .FirstOrDefault();
                
                return PartialView(model);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _Edit(ILPBankEditModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var uow = UnityManager.Resolve<IUnitOfWork>())
                    {
                        var repo = uow.GetRepo<IILPBankRepo>();

                        ILPBank dbEntity = repo.Get(model.ILPBankId);

                        dbEntity.IsPublic = model.IsPublic;

                        repo.Update(dbEntity);

                        uow.Commit();
                        
                        return JsonRes(dbEntity.ILPBankId.ToString());
                    }
                }
                catch (Exception ex)
                {
                    return JsonRes(Status.Error, ex.Message);
                }
            }

            return PartialView(model);
        }

        [HttpPost]
        public ActionResult Delete(Int32 id)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IILPBankRepo>();
                repo.Remove(id);

                uow.Commit();
                return JsonRes();
            }
        }

        [HttpPost]
        public ActionResult DeleteAll(String id)
        {
            if (!String.IsNullOrEmpty(id))
            {
                var ids = id.Split(',');
                using (var uow = UnityManager.Resolve<IUnitOfWork>())
                {
                    var repo = uow.GetRepo<IILPBankRepo>();
                    foreach (var item in ids)
                    {
                        repo.Remove(Int32.Parse(item));
                    }

                    uow.Commit();
                    return JsonRes();
                }
            }
            return JsonRes(Status.Error, "Error");
        }

        public JsonResult _Table(String name, String teacher, String guardian, String subject, String year)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                Int32 intYear = 0;
                Int32.TryParse(year, out intYear);

                var repo = uow.GetRepo<IILPBankRepo>();
                var result = repo.GetAll()
                    //.Where(x => x.TeacherId.HasValue &&
                    //            ((x.TeacherId != User.UserId &&
                    //            x.GuardianId != User.UserId &&
                    //            x.IsPublic) ||
                    //            (x.TeacherId == User.UserId ||
                    //            x.GuardianId == User.UserId) ||
                    //            User.Role == Role.Admin) &&
                    //            (((name != "" && x.ILPName.Contains(name)) ||
                    //            (name == "")) &&
                    //            ((teacher != "" && x.TeacherId.HasValue && x.Teacher.FullName.Contains(teacher)) ||
                    //            (teacher == "")) &&
                    //            ((guardian != "" && x.GuardianId.HasValue && x.Guardian.FullName.Contains(guardian)) ||
                    //            (guardian == "")) &&
                    //            ((subject != "" && x.SubjectId.HasValue && x.Subject.Name.Contains(subject)) ||
                    //            (subject == "")) &&
                    //            ((year != "" && x.SchoolYear.Year == intYear) ||
                    //            (year == ""))))
                    .Where(x => (((name != "" && x.ILPName.Contains(name)) ||
                                (name == "")) &&
                                ((teacher != "" && x.TeacherId.HasValue && x.Teacher.FullName.Contains(teacher)) ||
                                (teacher == "")) &&
                                ((guardian != "" && x.GuardianId.HasValue && x.Guardian.FullName.Contains(guardian)) ||
                                (guardian == "")) &&
                                ((subject != "" && x.SubjectId.HasValue && x.Subject.Name.Contains(subject)) ||
                                (subject == "")) &&
                                ((year != "" && x.SchoolYear.Year == intYear) ||
                                (year == ""))))
                    .Select(x => new ILPBankMiniModel
                    {
                        ILPBankId = x.ILPBankId,
                        Name = x.ILPName,
                        TeacherId = x.TeacherId.HasValue ? x.TeacherId : null,
                        Teacher = x.Teacher != null ?
                            x.Teacher.LastName + ", " + x.Teacher.FirstName :
                            String.Empty,
                        GuardianId = x.GuardianId.HasValue ? x.GuardianId : null,
                        Guardian = x.Guardian != null ?
                            x.Guardian.LastName + ", " + x.Guardian.FirstName :
                            String.Empty,
                        Subject = x.SubjectId.HasValue ?
                            x.Subject.Name :
                            String.Empty,
                        Year = x.SchoolYear.Year
                    })
                    .OrderBy(x => x.Name)
                    .ToList();

                return Json(result);
            }
        }

        public JsonResult GetILP(Int32 id)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IILPBankRepo>();
                var result = repo.GetAsQuery(id).Select(x => new {
                    CourseHrs = x.CourseHrs,
                    DescriptionCourse = x.DescriptionCourse,
                    Standards = x.Standards,
                    StudentActivities = x.StudentActivities,
                    MaterialsResources = x.MaterialsResources,
                    RoleAnyPeople = x.RoleAnyPeople,
                    EvaluationGradingPassFail = x.EvaluationGradingPassFail,
                    EvaluationGradingGradingScale = x.EvaluationGradingGradingScale,
                    EvaluationGradingOSN = x.EvaluationGradingOSN,
                    EvaluationGradingOther = x.EvaluationGradingOther,
                    EvaluationGradingOtherExplain = x.EvaluationGradingOtherExplain,
                    EvaluatedMeasurableOutcomes = x.EvaluatedMeasurableOutcomes,
                    CourseSyllabus = x.CourseSyllabus,
                    GuardianILPModifications = x.GuardianILPModifications,
                    InstructorILPModifications = x.InstructorILPModifications
                })
                .FirstOrDefault();

                return Json(result);
            }
        }
    }
}
