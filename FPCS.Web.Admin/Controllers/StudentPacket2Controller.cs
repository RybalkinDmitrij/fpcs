﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Objects.SqlClient;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using FPCS.Core.Extensions;
using FPCS.Core.jqGrid;
using FPCS.Data;
using FPCS.Data.Enums;
using FPCS.Data.Repo;
using FPCS.Web.Admin.Code;
using FPCS.Web.Admin.Helpers.Access;
using FPCS.Web.Admin.Helpers.BudgetStudent;
using FPCS.Web.Admin.Helpers.BudgetStudent.Models;
using FPCS.Web.Admin.Helpers.FPCSCourses;
using FPCS.Web.Admin.Helpers.GoodAndService;
using FPCS.Web.Admin.Models.GoodAndService;
using FPCS.Web.Admin.Models.StudentPacket2;

namespace FPCS.Web.Admin.Controllers
{
	[FPCSAuthorize(Role.Admin, Role.Teacher, Role.Guardian)]
	public class StudentPacket2Controller : BaseController
	{
		public ActionResult Index(Guid id, Semester? semester)
		{
			try
			{
				var model = GetStudentPacket(id);
				model.Semester = semester;

				return View(model);
			}
			catch (Exception ex)
			{
				return Error(ex.Message);
			}
		}

		public ActionResult _Print(Guid id)
		{
			try
			{
				var model = new PrintStudentPacketModel();

				model.StudentPacketModel = GetStudentPacket(id);

				model.PrintAddress = (ConfigurationManager.AppSettings["PrintAddress"]);
				model.PrintState = (ConfigurationManager.AppSettings["PrintState"]);
				model.PrintPhone = (ConfigurationManager.AppSettings["PrintPhone"]);
				model.PrintFax = (ConfigurationManager.AppSettings["PrintFax"]);
				model.PrintDateNow = DateTime.Now.ToString("MM/dd/yyyy");

				model.Title = "Student Packet : " + model.StudentPacketModel.StudentName + " - Grade: " +
				              model.StudentPacketModel.StudentGradeStr;

				return PartialView(model);
			}
			catch (Exception ex)
			{
				return Error(ex.Message);
			}
		}

		public ActionResult _PrintReimbursement(Guid id)
		{
			try
			{
				var printAddress = (ConfigurationManager.AppSettings["PrintAddress"]);
				var printState = (ConfigurationManager.AppSettings["PrintState"]);
				var printPhone = (ConfigurationManager.AppSettings["PrintPhone"]);
				var printFax = (ConfigurationManager.AppSettings["PrintFax"]);
				var printDateNow = DateTime.Now.ToString("MM/dd/yyyy");

				var model = new ListPrintReimbursementModel();
				model.PrintReimbursements = new List<PrintReimbursementModel>();

				using (var uow = UnityManager.Resolve<IUnitOfWork>())
				{
					var guardians = uow.GetRepo<IFamilyRepo>().GetGuardiansByStudent(id).ToList();

                    var studentPacket = uow.GetRepo<IStudentPacketRepo>().Get(id, SchoolYear.SchoolYearId);

					var data = uow.GetRepo<IGoodServiceRepo>()
						.GetReimbursements(id, studentPacket.StudentPacketId)
						.Select(x => new
						{
							x.GoodServiceId,
							VendorName = x.Vendor.BusinessName,
							Item = x.Title,
							BudgetedAmount = x.UnitPrice*x.NumberOfUnits + x.ShippingHandlingFees,
							RequestedAmount = 0,
							Course = x.StudentPacketCourse.FPCSCourse.Name,
							StudentName =
								x.StudentPacketCourseId.HasValue ?
									x.StudentPacketCourse.StudentPacket.Student.LastName + ", " +
									x.StudentPacketCourse.StudentPacket.Student.FirstName + " ( " +
									(
										x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.K ? "K" :
										x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.G1 ? "G1" :
										x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.G2 ? "G2" :
										x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.G3 ? "G3" :
										x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.G4 ? "G4" :
										x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.G5 ? "G5" :
										x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.G6 ? "G6" :
										x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.G7 ? "G7" :
										x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.G8 ? "G8" :
										x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.G9 ? "G9" :
										x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.G10 ? "G10" :
										x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.G11 ? "G11" :
										x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.G12 ? "G12" :
										x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.G ? "G" : ""
									)
									+ " ) " :
								x.StudentPacketId.HasValue ?
									x.StudentPacket.Student.LastName + ", " +
									x.StudentPacket.Student.FirstName + " ( " +
									(
										x.StudentPacket.Student.Grade == Grade.K ? "K" :
										x.StudentPacket.Student.Grade == Grade.G1 ? "G1" :
										x.StudentPacket.Student.Grade == Grade.G2 ? "G2" :
										x.StudentPacket.Student.Grade == Grade.G3 ? "G3" :
										x.StudentPacket.Student.Grade == Grade.G4 ? "G4" :
										x.StudentPacket.Student.Grade == Grade.G5 ? "G5" :
										x.StudentPacket.Student.Grade == Grade.G6 ? "G6" :
										x.StudentPacket.Student.Grade == Grade.G7 ? "G7" :
										x.StudentPacket.Student.Grade == Grade.G8 ? "G8" :
										x.StudentPacket.Student.Grade == Grade.G9 ? "G9" :
										x.StudentPacket.Student.Grade == Grade.G10 ? "G10" :
										x.StudentPacket.Student.Grade == Grade.G11 ? "G11" :
										x.StudentPacket.Student.Grade == Grade.G12 ? "G12" :
										x.StudentPacket.Student.Grade == Grade.G ? "G" : ""
									)
									+ " ) " :
								"",
                            IsWithdrawal = x.StudentPacketCourseId.HasValue
                                            ? x.StudentPacketCourse.StudentPacket.Student.WithdrawalDate.HasValue ? true : false
                                            : x.StudentPacketId.HasValue ? x.StudentPacket.Student.WithdrawalDate.HasValue ? true : false : false,
							ObjCD =
								x.GoodOrService == GoodOrService.Good &&
								x.TypeRequisitionReimbursement == TypeRequisitionReimbursement.VendorService
									? "3030"
									: x.GoodOrService == GoodOrService.Service &&
									  x.TypeRequisitionReimbursement == TypeRequisitionReimbursement.TextbookCurriculum
										? "4020"
										: string.Empty
						})
						.OrderBy(x => x.StudentName)
						.ToList();

					var groupVendor = data.Select(x => x.VendorName).Distinct().OrderBy(x => x).ToList();

					foreach (var item in groupVendor)
					{
						var itemModel = new PrintReimbursementModel();

						itemModel.Reimbursements = data.Where(x => x.VendorName == item)
							.Select(x => new ReimbursementModel
							{
								GoodServiceId = x.GoodServiceId,
								Item = x.Item,
								BudgetedAmount = x.BudgetedAmount,
								RequestedAmount = x.RequestedAmount,
								Course = x.Course,
								StudentName = x.StudentName,
								ObjCD = x.ObjCD,
                                IsWithdrawal = x.IsWithdrawal
							})
							.ToList();

						itemModel.VendorName = item;
						itemModel.PrintAddress = printAddress;
						itemModel.PrintState = printState;
						itemModel.PrintPhone = printPhone;
						itemModel.PrintFax = printFax;
						itemModel.PrintDateNow = printDateNow;

						itemModel.Total = itemModel.Reimbursements.Sum(x => x.BudgetedAmount);

						if (guardians != null && guardians.Count >= 2)
						{
							itemModel.Title = string.Format(@"{0} : {1} & {2}", guardians.FirstOrDefault().LastName,
								guardians.FirstOrDefault().FirstName, guardians.LastOrDefault().FirstName);
						}
						else if (guardians != null && guardians.Count == 1)
						{
							itemModel.Title = string.Format(@"{0}, {1}", guardians.FirstOrDefault().LastName,
								guardians.FirstOrDefault().FirstName);
						}
						else
						{
							itemModel.Title = string.Empty;
						}

						model.PrintReimbursements.Add(itemModel);
					}
				}

				return PartialView(model);
			}
			catch (Exception ex)
			{
				return Error(ex.Message);
			}
		}

		public ActionResult _PrintReimbursementGeneralExpenses(Int64 id)
		{
			try
			{
				var printAddress = (ConfigurationManager.AppSettings["PrintAddress"]);
				var printState = (ConfigurationManager.AppSettings["PrintState"]);
				var printPhone = (ConfigurationManager.AppSettings["PrintPhone"]);
				var printFax = (ConfigurationManager.AppSettings["PrintFax"]);
				var printDateNow = DateTime.Now.ToString("MM/dd/yyyy");

				var model = new ListPrintReimbursementModel();
				model.PrintReimbursements = new List<PrintReimbursementModel>();

				using (var uow = UnityManager.Resolve<IUnitOfWork>())
				{
					var studentPacket = uow.GetRepo<IStudentPacketRepo>().Get(id);
					var guardians = uow.GetRepo<IFamilyRepo>().GetGuardiansByStudent(studentPacket.StudentId).ToList();

					var data = uow.GetRepo<IGoodServiceRepo>()
						.GetByGeneralExpenses(studentPacket.StudentPacketId)
						.Select(x => new
						{
							x.GoodServiceId,
							VendorName = x.Vendor.BusinessName,
							Item = x.Title,
							BudgetedAmount = x.UnitPrice * x.NumberOfUnits + x.ShippingHandlingFees,
							RequestedAmount = 0,
							Course = x.StudentPacketCourse.FPCSCourse.Name,
							StudentName =
								x.StudentPacketCourseId.HasValue ?
									x.StudentPacketCourse.StudentPacket.Student.LastName + ", " +
									x.StudentPacketCourse.StudentPacket.Student.FirstName + " ( " +
									(
										x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.K ? "K" :
										x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.G1 ? "G1" :
										x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.G2 ? "G2" :
										x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.G3 ? "G3" :
										x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.G4 ? "G4" :
										x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.G5 ? "G5" :
										x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.G6 ? "G6" :
										x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.G7 ? "G7" :
										x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.G8 ? "G8" :
										x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.G9 ? "G9" :
										x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.G10 ? "G10" :
										x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.G11 ? "G11" :
										x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.G12 ? "G12" :
										x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.G ? "G" : ""
									)
									+ " ) " :
								x.StudentPacketId.HasValue ?
									x.StudentPacket.Student.LastName + ", " +
									x.StudentPacket.Student.FirstName + " ( " +
									(
										x.StudentPacket.Student.Grade == Grade.K ? "K" :
										x.StudentPacket.Student.Grade == Grade.G1 ? "G1" :
										x.StudentPacket.Student.Grade == Grade.G2 ? "G2" :
										x.StudentPacket.Student.Grade == Grade.G3 ? "G3" :
										x.StudentPacket.Student.Grade == Grade.G4 ? "G4" :
										x.StudentPacket.Student.Grade == Grade.G5 ? "G5" :
										x.StudentPacket.Student.Grade == Grade.G6 ? "G6" :
										x.StudentPacket.Student.Grade == Grade.G7 ? "G7" :
										x.StudentPacket.Student.Grade == Grade.G8 ? "G8" :
										x.StudentPacket.Student.Grade == Grade.G9 ? "G9" :
										x.StudentPacket.Student.Grade == Grade.G10 ? "G10" :
										x.StudentPacket.Student.Grade == Grade.G11 ? "G11" :
										x.StudentPacket.Student.Grade == Grade.G12 ? "G12" :
										x.StudentPacket.Student.Grade == Grade.G ? "G" : ""
									)
									+ " ) " :
								"",
                            IsWithdrawal = x.StudentPacketCourseId.HasValue
                                            ? x.StudentPacketCourse.StudentPacket.Student.WithdrawalDate.HasValue ? true : false
                                            : x.StudentPacketId.HasValue ? x.StudentPacket.Student.WithdrawalDate.HasValue ? true : false : false,
							ObjCD =
								x.GoodOrService == GoodOrService.Good &&
								x.TypeRequisitionReimbursement == TypeRequisitionReimbursement.VendorService
									? "3030"
									: x.GoodOrService == GoodOrService.Service &&
									  x.TypeRequisitionReimbursement == TypeRequisitionReimbursement.TextbookCurriculum
										? "4020"
										: string.Empty
						})
						.OrderBy(x => x.StudentName)
						.ToList();

					var groupVendor = data.Select(x => x.VendorName).Distinct().OrderBy(x => x).ToList();

					foreach (var item in groupVendor)
					{
						var itemModel = new PrintReimbursementModel();

						itemModel.Reimbursements = data.Where(x => x.VendorName == item)
							.Select(x => new ReimbursementModel
							{
								GoodServiceId = x.GoodServiceId,
								Item = x.Item,
								BudgetedAmount = x.BudgetedAmount,
								RequestedAmount = x.RequestedAmount,
								Course = x.Course,
								StudentName = x.StudentName,
								ObjCD = x.ObjCD,
                                IsWithdrawal = x.IsWithdrawal
							})
							.ToList();

						itemModel.VendorName = item;
						itemModel.PrintAddress = printAddress;
						itemModel.PrintState = printState;
						itemModel.PrintPhone = printPhone;
						itemModel.PrintFax = printFax;
						itemModel.PrintDateNow = printDateNow;

						itemModel.Total = itemModel.Reimbursements.Sum(x => x.BudgetedAmount);

						if (guardians != null && guardians.Count >= 2)
						{
							itemModel.Title = string.Format(@"{0} : {1} & {2}", guardians.FirstOrDefault().LastName,
								guardians.FirstOrDefault().FirstName, guardians.LastOrDefault().FirstName);
						}
						else if (guardians != null && guardians.Count == 1)
						{
							itemModel.Title = string.Format(@"{0}, {1}", guardians.FirstOrDefault().LastName,
								guardians.FirstOrDefault().FirstName);
						}
						else
						{
							itemModel.Title = string.Empty;
						}

						model.PrintReimbursements.Add(itemModel);
					}
				}

				return PartialView("_PrintReimbursement", model);
			}
			catch (Exception ex)
			{
				return Error(ex.Message);
			}
		}

		[HttpGet]
		public PartialViewResult _SponsorTeacherEdit(long id)
		{
			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				var query = uow.GetRepo<IStudentPacketRepo>().GetAsQuery(id);
				var model = query.Select(x => new SponsorTeacherModel
				{
					StudentPacketId = x.StudentPacketId,
					TeacherId = x.SponsorTeacherId,
					Name = x.SponsorTeacher.FullName
				}).FirstOrDefault();

				if (model == null) return ErrorPartial("Teacher {0} not found", id);

				model.Init();
				return PartialView(model);
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult _SponsorTeacherEdit(SponsorTeacherModel model)
		{
			if (ModelState.IsValid)
			{
				try
				{
					using (var uow = UnityManager.Resolve<IUnitOfWork>())
					{
						var access =
							AccessAction.IsNotLock(model.StudentPacketId, User.Role);

						if (!access.IsResolve)
						{
							throw new Exception(access.Message);
						}

						var repo = uow.GetRepo<IStudentPacketRepo>();
						var dbEntity = repo.UpdateSponsorTeacher(model.StudentPacketId, model.TeacherId.Value);
						uow.Commit();

						model.Name = dbEntity.SponsorTeacher.FullName;
						return PartialView("_SponsorTeacher", model);
					}
				}
				catch (Exception ex)
				{
					ModelState.AddModelError("", ex.Message);
				}
			}

			model.Init();
			return PartialView(model);
		}

		[HttpGet]
		public PartialViewResult _SponsorSignatureEdit(long id)
		{
			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				var query = uow.GetRepo<IStudentPacketRepo>().GetAsQuery(id);
				var model = query.Select(x => new SponsorSignatureModel
				{
					StudentPacketId = x.StudentPacketId,
					TeacherId = x.SponsorTeacherId,
					Signature = x.SponsorSignatureSem1
				}).FirstOrDefault();

				if (model == null) return ErrorPartial("Teacher {0} not found", id);

				model.Init();
				return PartialView(model);
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult _SponsorSignatureEdit(SponsorSignatureModel model)
		{
			if (ModelState.IsValid)
			{
				try
				{
					using (var uow = UnityManager.Resolve<IUnitOfWork>())
					{
						var access =
							AccessAction.IsNotLock(model.StudentPacketId, User.Role);

						if (!access.IsResolve)
						{
							throw new Exception(access.Message);
						}

						var repo = uow.GetRepo<IStudentPacketRepo>();
						var dbEntity = repo.UpdateSponsorSignature(model.StudentPacketId, model.Signature);
						uow.Commit();

						return PartialView("_SponsorSignature", model);
					}
				}
				catch (Exception ex)
				{
					ModelState.AddModelError("", ex.Message);
				}
			}

			model.Init();
			return PartialView(model);
		}

		[HttpGet]
		public PartialViewResult _ZangleEdit(long id)
		{
			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				var query = uow.GetRepo<IStudentPacketRepo>().GetAsQuery(id);
				var model = query.Select(x => new ZangleModel
				{
					StudentPacketId = x.StudentPacketId,
					StudentId = x.StudentId,
					Name = x.Student.ZangleID
				}).FirstOrDefault();

				if (model == null) return ErrorPartial("Student packet {0} not found", id);

				model.Init();
				return PartialView(model);
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult _ZangleEdit(ZangleModel model)
		{
			if (ModelState.IsValid)
			{
				try
				{
					using (var uow = UnityManager.Resolve<IUnitOfWork>())
					{
						var access =
							AccessAction.IsNotLock(model.StudentPacketId, User.Role);

						if (!access.IsResolve)
						{
							throw new Exception(access.Message);
						}

						var repo = uow.GetRepo<IStudentRepo>();
						var student = repo.Get(model.StudentId);
						student.ZangleID = model.Name;
						uow.Commit();

						return PartialView("_Zangle", model);
					}
				}
				catch (Exception ex)
				{
					ModelState.AddModelError("", ex.Message);
				}
			}

			model.Init();
			return PartialView(model);
		}

		[HttpGet]
		public PartialViewResult _AmpElaEdit(long id)
		{
			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				var query = uow.GetRepo<IStudentPacketRepo>().GetAsQuery(id);
				var model = query.Select(x => new AmpElaModel
				{
					StudentPacketId = x.StudentPacketId,
					StudentId = x.StudentId,
					AmpEla = x.AmpELA
				}).FirstOrDefault();

				if (model == null) return ErrorPartial("Student packet {0} not found", id);

				model.Init();
				return PartialView(model);
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult _AmpElaEdit(AmpElaModel model)
		{
			if (ModelState.IsValid)
			{
				try
				{
					using (var uow = UnityManager.Resolve<IUnitOfWork>())
					{
						var access =
							AccessAction.IsNotLock(model.StudentPacketId, User.Role);

						if (!access.IsResolve)
						{
							throw new Exception(access.Message);
						}

						var repo = uow.GetRepo<IStudentPacketRepo>();
						var studentPacket = repo.Get(model.StudentPacketId);
						studentPacket.AmpELA = model.AmpEla;
						uow.Commit();

						return PartialView("_AmpEla", model);
					}
				}
				catch (Exception ex)
				{
					ModelState.AddModelError("", ex.Message);
				}
			}

			model.Init();
			return PartialView(model);
		}

		[HttpGet]
		public PartialViewResult _AmpMathEdit(long id)
		{
			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				var query = uow.GetRepo<IStudentPacketRepo>().GetAsQuery(id);
				var model = query.Select(x => new AmpMathModel
				{
					StudentPacketId = x.StudentPacketId,
					StudentId = x.StudentId,
					AmpMath = x.AmpMath
				}).FirstOrDefault();

				if (model == null) return ErrorPartial("Student packet {0} not found", id);

				model.Init();
				return PartialView(model);
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult _AmpMathEdit(AmpMathModel model)
		{
			if (ModelState.IsValid)
			{
				try
				{
					using (var uow = UnityManager.Resolve<IUnitOfWork>())
					{
						var access =
							AccessAction.IsNotLock(model.StudentPacketId, User.Role);

						if (!access.IsResolve)
						{
							throw new Exception(access.Message);
						}

						var repo = uow.GetRepo<IStudentPacketRepo>();
						var studentPacket = repo.Get(model.StudentPacketId);
						studentPacket.AmpMath = model.AmpMath;
						uow.Commit();

						return PartialView("_AmpMath", model);
					}
				}
				catch (Exception ex)
				{
					ModelState.AddModelError("", ex.Message);
				}
			}

			model.Init();
			return PartialView(model);
		}

		[HttpPost]
		public JsonResult _FPCSCourses(Guid studentId, Guid? sponsorId, long studentPacketId, Semester semester,
			GridOptions options, FPCSCoursesListOptions coursesListOptions)
		{
			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				var subject = coursesListOptions.Subject;
				coursesListOptions.Subject = null;

				var query = uow.GetRepo<IFPCSCourseRepo>()
					.GetNotSelectedInPacketCourses(studentId, studentPacketId, SchoolYear.SchoolYearId, semester)
					.Where(x => (subject.HasValue && x.ASDCourse.SubjectId == subject.Value) ||
					            (!subject.HasValue));

				// if current user is teacher and isn't sponsor, then to show his courses
				if (sponsorId.HasValue &&
				    User.Role == Role.Teacher &&
				    User.UserId != sponsorId.Value)
				{
					query = query.Where(x => x.TeacherId == User.UserId);
				}

				var fpcsCourseIds = query.Select(x => x.FPCSCourseId).ToList();

				var goodService = GoodAndServiceHelper.GetListGoodAndService(fpcsCourseIds, Session.GetSchoolYear().SchoolYearId)
					.Select(x => new
					{
						FPCSCourseId = x.FPCSCourseID,
						Cost = x.BudgetTotal
					})
					.ToList();

				var engine = new GridDynamicEngine(options, coursesListOptions);
				var result = engine.ApplyAll(query, x => new FPCSCourseMiniModel
				{
					FPCSCourseId = x.FPCSCourseId,
					Name = x.Name,
					Teacher = x.Teacher != null ? x.Teacher.FullName : string.Empty,
					Guardian = x.Guardian != null ? x.Guardian.FullName : string.Empty,
					Vendor = x.Vendor != null ? x.Vendor.LastName + ", " + x.Vendor.FirstName : string.Empty,
					Subject = x.ASDCourse.Subject.Name,
					Semester = x.Semester
				});

				foreach (var item in result.rows)
				{
					item.Cost = "$" + goodService.Where(t => t.FPCSCourseId == item.FPCSCourseId).Sum(t => t.Cost);
				}

				return Json(result);
			}
		}

		[HttpPost]
		public JsonResult AddFPCSStudentPacketCourse(long fpcsCourseId, long studentPacketId)
		{
			try
			{
				using (var uow = UnityManager.Resolve<IUnitOfWork>())
				{
					var repo = uow.GetRepo<IStudentPacketRepo>();
					var studentPacket = repo.Get(studentPacketId);

					if (!BudgetStudentHelper.IsResolveFPCSCourse(studentPacket.StudentId, SchoolYear.SchoolYearId, fpcsCourseId))
					{
						return JsonRes(Status.NoEnoughBudget, BudgetStudentHelper.MessageNoEnoughBudget);
					}

					FPCSCourseHandler.AddFPCSStudentPacketCourse(fpcsCourseId, studentPacketId, Session.GetUser().Role);

					return JsonRes();
				}
			}
			catch (Exception ex)
			{
				return JsonRes(Status.Error, ex.Message);
			}
		}

		[HttpPost]
		public JsonResult DeleteStudentPacketCourse(long studentPacketCourseId)
		{
			try
			{
				using (var uow = UnityManager.Resolve<IUnitOfWork>())
				{
					var repo = uow.GetRepo<IStudentPacketCourseRepo>();

					var access = AccessAction.IsCanDeleteStudentPacketCourse(studentPacketCourseId, Session.GetUser().Role);
					if (!access.IsResolve)
					{
						throw new Exception(access.Message);
					}

					repo.Remove(studentPacketCourseId);
					uow.Commit();
					return JsonRes();
				}
			}
			catch (Exception ex)
			{
				return JsonRes(Status.Error, ex.Message);
			}
		}

		[HttpPost]
		public ActionResult ChangeGoalEnrollment(Guid studentId, GoalEnrollmentPercent percentagePlanningEnroll)
		{
			try
			{
				using (var uow = UnityManager.Resolve<IUnitOfWork>())
				{
					var repo = uow.GetRepo<IStudentRepo>();

					var access =
						AccessAction.IsNotLock(uow.GetRepo<IStudentPacketRepo>().Get(studentId, SchoolYear.SchoolYearId), User.Role);

					if (!access.IsResolve)
					{
						throw new Exception(access.Message);
					}

					var student = repo.Get(studentId);

					if (studentId == null)
					{
						throw new Exception("Student not found!");
					}

					student.PercentagePlanningEnroll = percentagePlanningEnroll == GoalEnrollmentPercent._0Percent
						? 0
						: percentagePlanningEnroll == GoalEnrollmentPercent._25Percent
							? 25
							: percentagePlanningEnroll == GoalEnrollmentPercent._50Percent
								? 50
								: percentagePlanningEnroll == GoalEnrollmentPercent._75Percent
									? 75
									: percentagePlanningEnroll == GoalEnrollmentPercent._100Percent ? 100 : 0;

					repo.Update(student);

					uow.Commit();

					return JsonRes(student.DbUserId.ToString());
				}
			}
			catch (Exception ex)
			{
				return JsonRes(Status.Error, ex.Message);
			}
		}

		public ActionResult Locked(Guid id)
		{
			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				var packetRepo = uow.GetRepo<IStudentPacketRepo>();

				var dbEntity = packetRepo.Get(id, SchoolYear.SchoolYearId);

				dbEntity.IsLocked = true;

				packetRepo.Update(dbEntity);

				uow.Commit();

				return RedirectToAction("Index", new {id});
			}
		}

		public ActionResult Unlocked(Guid id)
		{
			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				var packetRepo = uow.GetRepo<IStudentPacketRepo>();

				var dbEntity = packetRepo.Get(id, SchoolYear.SchoolYearId);

				dbEntity.IsLocked = false;

				packetRepo.Update(dbEntity);

				uow.Commit();

				return RedirectToAction("Index", new {id});
			}
		}

		public ActionResult _ASDTestingAgreement(long id)
		{
			var model = new AgreementModel();
			model.Init(id);

			return PartialView(model);
		}

		public ActionResult SignASDTestingAgreement(Guid studentId)
		{
			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				var packetRepo = uow.GetRepo<IStudentPacketRepo>();

				var dbEntity = packetRepo.Get(studentId, SchoolYear.SchoolYearId);

				dbEntity.IsASDTASigned = true;
				dbEntity.ASDTAGuardian = uow.GetRepo<IGuardianRepo>().Get(User.UserId);

				packetRepo.Update(dbEntity);

				uow.Commit();

				return JsonRes(Status.OK, "OK");
			}
		}

		public ActionResult _ProgressReportAgreement(long id)
		{
			var model = new AgreementModel();
			model.Init(id);

			return PartialView(model);
		}

		public ActionResult SignProgressReportAgreement(Guid studentId)
		{
			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				var packetRepo = uow.GetRepo<IStudentPacketRepo>();

				var dbEntity = packetRepo.Get(studentId, SchoolYear.SchoolYearId);

				dbEntity.IsPRASigned = true;
				dbEntity.PRAGuardian = uow.GetRepo<IGuardianRepo>().Get(User.UserId);

				packetRepo.Update(dbEntity);

				uow.Commit();

				return JsonRes(Status.OK, "OK");
			}
		}

		public ActionResult GetStudentPacketCourse(long studentPacketCourseId)
		{
			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				var packetRepo = uow.GetRepo<IStudentPacketRepo>();
				var packetCourseRepo = uow.GetRepo<IStudentPacketCourseRepo>();

				var spc = packetCourseRepo.Get(studentPacketCourseId);
				var goodService = GoodAndServiceHelper.GetListGoodAndService(spc.StudentPacketId)
					.Where(x => x.StudentPacketCourseID == studentPacketCourseId)
					.Select(x => new GoodAndServiceModel(x))
					.ToList();

				var model = new StudentPacketCourseModel();
				model.StudentPacketCourseId = studentPacketCourseId;
				model.TableGoodAndService = new TableGoodAndServiceModel
				{
					GoodsAndServices = goodService.ToList(),
					PlanningBudgetTotal = goodService.Sum(t => t.TotalPlanningBudget),
					ActualSpendingTotal = goodService.Sum(t => t.TotalActualSpending)
				};

				return PartialView("_StudentPacketCourseGoodService", model);
			}
		}

		public ActionResult UpdateField(int pk, string name, string value)
		{
			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				var repo = uow.GetRepo<IStudentPacketCourseRepo>();
				var dbEntity = repo.Get(pk);

				try
				{
					switch (name)
					{
						case "Repeat":
							dbEntity.Repeat = (Repeated) (Convert.ToInt32(value));
							break;
						case "IsInSystem":
							dbEntity.IsInSystem = Convert.ToInt32(value) == 1;
							break;
						case "Qtr11":
							dbEntity.Q11 = value;
							break;
						case "Qtr12":
							dbEntity.Q12 = value;
							break;
						case "Qtr13":
							dbEntity.Q13 = value;
							break;
						case "Q1":
							dbEntity.Q1 = value;
							break;
						case "Qtr21":
							dbEntity.Q21 = value;
							break;
						case "Qtr22":
							dbEntity.Q22 = value;
							break;
						case "Qtr23":
							dbEntity.Q23 = value;
							break;
						case "Q2":
							dbEntity.Q2 = value;
							break;
						case "Sem1":
							dbEntity.Sem1 = value;
							break;
						case "Qtr31":
							dbEntity.Q31 = value;
							break;
						case "Qtr32":
							dbEntity.Q32 = value;
							break;
						case "Qtr33":
							dbEntity.Q33 = value;
							break;
						case "Q3":
							dbEntity.Q3 = value;
							break;
						case "Qtr41":
							dbEntity.Q41 = value;
							break;
						case "Qtr42":
							dbEntity.Q42 = value;
							break;
						case "Qtr43":
							dbEntity.Q43 = value;
							break;
						case "Q4":
							dbEntity.Q4 = value;
							break;
						case "Sem2":
							dbEntity.Sem2 = value;
							break;
						case "Qtr51":
							dbEntity.Q51 = value;
							break;
						case "Qtr52":
							dbEntity.Q52 = value;
							break;
						case "Qtr53":
							dbEntity.Q53 = value;
							break;
						case "Q5":
							dbEntity.Q5 = value;
							break;
						case "Qtr61":
							dbEntity.Q61 = value;
							break;
						case "Qtr62":
							dbEntity.Q62 = value;
							break;
						case "Qtr63":
							dbEntity.Q63 = value;
							break;
						case "Q6":
							dbEntity.Q6 = value;
							break;
						case "Sem3":
							dbEntity.Sem3 = value;
							break;
						default:
							break;
					}

					dbEntity = repo.Update(dbEntity);

					uow.Commit();
				}
				catch (Exception)
				{
					return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
				}
			}

			return new HttpStatusCodeResult(HttpStatusCode.OK);
		}

		public ActionResult UpdateFieldAmp(int pk, string name)
		{
			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				var dbEntity = uow.GetRepo<IStudentPacketRepo>().Get(pk);
				try
				{
					var markValAmpEla = !string.IsNullOrEmpty(Request[@"value[markVal]"])
						? (AmpEla) Convert.ToInt32(Request[@"value[markVal]"])
						: AmpEla.NA;
					var markValAmpMath = !string.IsNullOrEmpty(Request[@"value[markVal]"])
						? (AmpMath) Convert.ToInt32(Request[@"value[markVal]"])
						: AmpMath.NA;
					var commentVal = !string.IsNullOrEmpty(Request[@"value[commentVal]"])
						? Request[@"value[commentVal]"]
						: string.Empty;

					switch (name)
					{
						case "AmpEla":
							dbEntity.AmpELA = markValAmpEla;
							dbEntity.AmpELAComment = commentVal;
							break;
						case "AmpMath":
							dbEntity.AmpMath = markValAmpMath;
							dbEntity.AmpMathComment = commentVal;
							break;
						default:
							break;
					}

					uow.Commit();
				}
				catch (Exception)
				{
					return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
				}
			}
			return new HttpStatusCodeResult(HttpStatusCode.OK); // OK = 200
		}

		public ActionResult UpdateFieldQ(int pk, string name)
		{
			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				var dbEntity = uow.GetRepo<IStudentPacketCourseRepo>().Get(pk);
				try
				{
					var markVal = !string.IsNullOrEmpty(Request[@"value[markVal]"]) ? Request[@"value[markVal]"] : string.Empty;
					var commentVal = !string.IsNullOrEmpty(Request[@"value[commentVal]"])
						? Request[@"value[commentVal]"]
						: string.Empty;
					var lockVal = !string.IsNullOrEmpty(Request[@"value[lockVal]"])
						? Convert.ToBoolean(Request[@"value[lockVal]"])
						: false;

					switch (name)
					{
						case "Q1":
							dbEntity.Q1 = markVal;
							dbEntity.Q1Comment = commentVal;
							dbEntity.Q1IsLock = lockVal;
							break;
						case "Q2":
							dbEntity.Q2 = markVal;
							dbEntity.Q2Comment = commentVal;
							dbEntity.Q2IsLock = lockVal;
							break;
						case "Q3":
							dbEntity.Q3 = markVal;
							dbEntity.Q3Comment = commentVal;
							dbEntity.Q3IsLock = lockVal;
							break;
						case "Q4":
							dbEntity.Q4 = markVal;
							dbEntity.Q4Comment = commentVal;
							dbEntity.Q4IsLock = lockVal;
							break;
						case "Q5":
							dbEntity.Q5 = markVal;
							dbEntity.Q5Comment = commentVal;
							dbEntity.Q5IsLock = lockVal;
							break;
						case "Q6":
							dbEntity.Q6 = markVal;
							dbEntity.Q6Comment = commentVal;
							dbEntity.Q6IsLock = lockVal;
							break;
						default:
							break;
					}

					uow.Commit();
				}
				catch (Exception)
				{
					return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
				}
			}
			return new HttpStatusCodeResult(HttpStatusCode.OK); // OK = 200
		}

		public ActionResult UpdateFieldS(int pk, string name)
		{
			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				var dbEntity = uow.GetRepo<IStudentPacketCourseRepo>().Get(pk);
				try
				{
					var markVal = !string.IsNullOrEmpty(Request[@"value[markVal]"]) ? Request[@"value[markVal]"] : string.Empty;
					var lockVal = !string.IsNullOrEmpty(Request[@"value[lockVal]"])
						? Convert.ToBoolean(Request[@"value[lockVal]"])
						: false;
					var creditVal = !string.IsNullOrEmpty(Request[@"value[creditVal]"])
						? Convert.ToDecimal(Request[@"value[creditVal]"])
						: 0;
					var commentVal = !string.IsNullOrEmpty(Request[@"value[commentVal]"])
						? Request[@"value[commentVal]"]
						: string.Empty;

					switch (name)
					{
						case "Sem1":
							dbEntity.Sem1 = markVal;
							dbEntity.Sem1IsLock = lockVal;
							dbEntity.Sem1Credit = creditVal;
							dbEntity.Sem1Comment = commentVal;
							break;
						case "Sem2":
							dbEntity.Sem2 = markVal;
							dbEntity.Sem2IsLock = lockVal;
							dbEntity.Sem2Credit = creditVal;
							dbEntity.Sem2Comment = commentVal;
							break;
						case "Sem3":
							dbEntity.Sem3 = markVal;
							dbEntity.Sem3IsLock = lockVal;
							dbEntity.Sem3Credit = creditVal;
							dbEntity.Sem3Comment = commentVal;
							break;
						default:
							break;
					}

					uow.Commit();
				}
				catch (Exception)
				{
					return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
				}
			}
			return new HttpStatusCodeResult(HttpStatusCode.OK); // OK = 200
		}

		public ActionResult UpdateFieldPacket(int pk, string name, string value)
		{
			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				var repo = uow.GetRepo<IStudentPacketRepo>();
				var dbEntity = repo.Get(pk);

				try
				{
					switch (name)
					{
						case "SponsorTeacher":
							dbEntity.SponsorTeacherId = Guid.Parse(value);
							dbEntity = repo.Update(dbEntity);
							uow.Commit();
							break;
						case "SponsorSignatureSem1":
							dbEntity.SponsorSignatureSem1 = (Sign) (Convert.ToInt32(value));
							dbEntity.DateSponsorSignatureSem1 = DateTime.Now;
							dbEntity = repo.Update(dbEntity);
							uow.Commit();

							CalcPercentageEnrollment(dbEntity.StudentId);
							break;
						case "GuardianSignatureSem1":
							dbEntity.GuardianSignatureSem1 = (Sign) (Convert.ToInt32(value));
							dbEntity.DateGuardianSignatureSem1 = DateTime.Now;

							dbEntity = repo.Update(dbEntity);
							uow.Commit();

							CalcPercentageEnrollment(dbEntity.StudentId);
							break;
						case "SponsorSignatureSem2":
							dbEntity.SponsorSignatureSem2 = (Sign) (Convert.ToInt32(value));
							dbEntity.DateSponsorSignatureSem2 = DateTime.Now;
							dbEntity = repo.Update(dbEntity);
							uow.Commit();
							break;
						case "GuardianSignatureSem2":
							dbEntity.GuardianSignatureSem2 = (Sign) (Convert.ToInt32(value));
							dbEntity.DateGuardianSignatureSem2 = DateTime.Now;
							dbEntity = repo.Update(dbEntity);
							uow.Commit();
							break;
						case "SponsorSignatureSummer":
							dbEntity.SponsorSignatureSummer = (Sign) (Convert.ToInt32(value));
							dbEntity.DateSponsorSignatureSummer = DateTime.Now;
							dbEntity = repo.Update(dbEntity);
							uow.Commit();
							break;
						case "GuardianSignatureSummer":
							dbEntity.GuardianSignatureSummer = (Sign) (Convert.ToInt32(value));
							dbEntity.DateGuardianSignatureSummer = DateTime.Now;
							dbEntity = repo.Update(dbEntity);
							uow.Commit();
							break;
						case "A504":
							dbEntity.A504 = Convert.ToInt32(value) == 1;
							dbEntity = repo.Update(dbEntity);
							uow.Commit();
							break;
						case "SWD":
							dbEntity.SWD = Convert.ToInt32(value) == 1;
							dbEntity = repo.Update(dbEntity);
							uow.Commit();
							break;
						default:
							break;
					}
				}
				catch (Exception)
				{
					return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
				}
			}

			return new HttpStatusCodeResult(HttpStatusCode.OK);
		}

		public ActionResult UpdateFieldStudent(Guid pk, string name, string value)
		{
			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				var repo = uow.GetRepo<IStudentRepo>();
				var dbEntity = repo.Get(pk);

				try
				{
					switch (name)
					{
						case "Zangle":
							dbEntity.ZangleID = value;
							break;
						case "State":
							dbEntity.StateID = value;
							break;
						default:
							break;
					}

					dbEntity = repo.Update(dbEntity);

					uow.Commit();
				}
				catch (Exception)
				{
					return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
				}
			}

			return new HttpStatusCodeResult(HttpStatusCode.OK);
		}

		private StudentPacketModel GetStudentPacket(Guid id)
		{
			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				var packetRepo = uow.GetRepo<IStudentPacketRepo>();
				var packetCourseRepo = uow.GetRepo<IStudentPacketCourseRepo>();
				var subjectRepo = uow.GetRepo<ISubjectRepo>();
				var goodServiceRepo = uow.GetRepo<IGoodServiceRepo>();
				var schoolYearRepo = uow.GetRepo<ISchoolYearRepo>();
				var guardiansRepo = uow.GetRepo<IGuardianRepo>();

				var schoolYear = schoolYearRepo.Get(SchoolYear.SchoolYearId);

				var dbEntity = packetRepo.AddIsNotExist(id, SchoolYear.SchoolYearId);
				if (dbEntity != null)
				{
					uow.Commit();
				}

				var budgetStudentPacket = BudgetStudentHelper.GetBudgetStudentPacket(id, SchoolYear.SchoolYearId);

				var query = packetRepo.GetAsQuery(id, SchoolYear.SchoolYearId);
				var model = query.Select(x => new StudentPacketModel
				{
					CurrentUserId = User.UserId,
					StudentPacketId = x.StudentPacketId,
					IsLocked = x.IsLocked,
					StudentId = x.StudentId,
					StudentName = x.Student.FullName,
					FamilyId =
						x.Student.Families.FirstOrDefault(t => !t.IsDeleted) != null
							? x.Student.Families.FirstOrDefault(t => !t.IsDeleted).FamilyId
							: 0,
					FamilyName = string.Empty,
					//x.Student.Families.FirstOrDefault(t => !t.IsDeleted) != null ? x.Student.Families.FirstOrDefault(t => !t.IsDeleted).Name : String.Empty,
					ZangleId = x.Student.ZangleID,
					StateId = x.Student.StateID,
					AmpEla = x.AmpELA,
					AmpElaComment = x.AmpELAComment,
					AmpMath = x.AmpMath,
					AmpMathComment = x.AmpMathComment,
					A504 = x.A504 ? "Yes" : "No",
					SWD = x.SWD ? "Yes" : "No",
					StudentGrade = x.Student.Grade,
					SponsorTeacherId = x.SponsorTeacherId,
					SponsorTeacherName = x.SponsorTeacher.FullName,
					SponsorSignatureSem1 = x.SponsorSignatureSem1,
					DateSponsorSignatureSem1 = x.DateSponsorSignatureSem1,
					GuardianSignatureSem1 = x.GuardianSignatureSem1,
					DateGuardianSignatureSem1 = x.DateGuardianSignatureSem1,
					SponsorSignatureSem2 = x.SponsorSignatureSem2,
					DateSponsorSignatureSem2 = x.DateSponsorSignatureSem2,
					GuardianSignatureSem2 = x.GuardianSignatureSem2,
					DateGuardianSignatureSem2 = x.DateGuardianSignatureSem2,
					SponsorSignatureSummer = x.SponsorSignatureSummer,
					DateSponsorSignatureSummer = x.DateSponsorSignatureSummer,
					GuardianSignatureSummer = x.GuardianSignatureSummer,
					DateGuardianSignatureSummer = x.DateGuardianSignatureSummer,
					IsASDTASigned = x.IsASDTASigned,
					IsPRASigned = x.IsPRASigned,
					ILPPhilosophy = x.Student.ILPPhilosophy,
					PBBeginningBalance = budgetStudentPacket.PBBeginningBalance,
					PBBudgetTransferDeposits = budgetStudentPacket.PBBudgetTransferDeposits,
					PBBudgetTransferWithdrawals = budgetStudentPacket.PBBudgetTransferWithdrawals,
					PBAvailableRemainingFunds = budgetStudentPacket.PBAvailableRemainingFunds,
					PBAvailableRemainingFundsTotal = budgetStudentPacket.PBAvailableRemainingFundsTotal,
					ASBeginningBalance = budgetStudentPacket.ASBeginningBalance,
					ASBudgetTransferDeposits = budgetStudentPacket.ASBudgetTransferDeposits,
					ASBudgetTransferWithdrawals = budgetStudentPacket.ASBudgetTransferWithdrawals,
					ASAvailableRemainingFunds = budgetStudentPacket.ASAvailableRemainingFunds,
					ASAvailableRemainingFundsTotal = budgetStudentPacket.ASAvailableRemainingFundsTotal,
					GoalEnrollmentPercent = budgetStudentPacket.GoalEnrollmentPercent,
					GoalCoreUnits = budgetStudentPacket.GoalCoreUnits,
					GoalElectiveUnits = budgetStudentPacket.GoalElectiveUnits,
					GoalTotalUnits = budgetStudentPacket.GoalTotalUnits,
					GoalILPHrs = budgetStudentPacket.GoalILPHrs,
					GoalContractHrs = budgetStudentPacket.GoalContractHrs,
					AchievedEnrollmentPercent = budgetStudentPacket.AchievedEnrollmentPercent,
					AchievedCoreUnits = budgetStudentPacket.AchievedCoreUnits,
					AchievedElectiveUnits = budgetStudentPacket.AchievedElectiveUnits,
					AchievedTotalUnits = budgetStudentPacket.AchievedTotalUnits,
					AchievedILPHrs = budgetStudentPacket.AchievedILPHrs,
					AchievedContractHrs = budgetStudentPacket.AchievedContractHrs,
					IndividualBudgetBudgetLimit = budgetStudentPacket.IBBudgetLimit,
					//IndividualBudgetAmountBudgeted = budgetStudentPacket.IBAmountBudgeted,
					//IndividualBudgetElectiveBalance = budgetStudentPacket.IBElectiveBalance,
					IndividualBudgetGlobalReserve = budgetStudentPacket.IBGlobalReserve,
					Sponsorship = budgetStudentPacket.Sponsorship,
					SponsorshipOut = schoolYear.BookMaterialFee,
                    FamilyEmail = x.Student != null 
                            ? x.Student
                                .Families
								.Where(t => !t.IsDeleted)
								.Select(t => t.Email)
								.FirstOrDefault()
                            : String.Empty,
                    SponsorTeacherEmail = x.StudentPacketCourses.FirstOrDefault(t => !t.IsDeleted) != null
                            ? x.StudentPacketCourses.FirstOrDefault(t => !t.IsDeleted)
                                .StudentPacket
                                .SponsorTeacher
                                .Email
                            : String.Empty,
                    IsWithdrawal = x.Student.WithdrawalDate.HasValue ? true : false
				})
					.FirstOrDefault();

				var guardians = guardiansRepo.GetByFamilies(SchoolYear.SchoolYearId, new List<long> {model.FamilyId})
					.Select(x => new
					{
						GuardianId = x.DbUserId,
						GuardianFirstName = x.FirstName,
						GuardianLastName = x.LastName,
						FamilyIds = x.Families.Where(t => !t.IsDeleted).Select(t => t.FamilyId),
                        FamilyPhone = x.CellPhone
					})
					.ToList();

				var names =
					guardians.Where(x => x.FamilyIds.Contains(model.FamilyId))
						.Select(x => new {x.GuardianLastName, x.GuardianFirstName, x.FamilyPhone})
						.ToList();
				var lastNames = names.Select(x => x.GuardianLastName).Distinct();

                var phones = names.Select(x => x.FamilyPhone).Distinct();

				if (lastNames.Count() == 1)
				{
					model.FamilyName = names[0].GuardianLastName + ", " + string.Join(" & ", names.Select(x => x.GuardianFirstName));
				}
				else
				{
					model.FamilyName = string.Join(" - ", lastNames) + ", " +
					                   string.Join(" & ", names.Select(x => x.GuardianFirstName));
				}

                if (phones.Count() == 1)
                {
                    model.FamilyPhone = names[0].FamilyPhone;
                }
                else
                {
                    model.FamilyPhone = string.Join(" , ", phones);
                }

				model.StudentGradeStr = model.StudentGrade.GetDescription();
				model.SubjectSearchOptions = ":All";
				var subjects = subjectRepo.GetAll().ToList();

				foreach (var item in subjects)
				{
					model.SubjectSearchOptions += string.Format(@";{0}:{1}", item.SubjectId, item.Name);
				}

				var goodService = GoodAndServiceHelper.GetListGoodAndService(model.StudentPacketId)
					.Select(x => new GoodAndServiceModel(x))
					.ToList();

				model.StudentPacketCourseModels = packetCourseRepo.GetByStudentPacket(model.StudentPacketId)
					.Select(x => new
					{
						x.StudentPacketCourseId,
						x.FPCSCourseId,
                        FPCSCourseName = x.FPCSCourse.Name + " - " + (!String.IsNullOrEmpty(x.FPCSCourse.ASDCourse.Description)
                            ? x.FPCSCourse.ASDCourse.Description.Length > 50
                                ? x.FPCSCourse.ASDCourse.Description.Substring(0, 50)
                                : x.FPCSCourse.ASDCourse.Description
                            : String.Empty),
						ILPName =
							x.FPCSCourse.ILPs.FirstOrDefault(y => y.FPCSCourseId == x.FPCSCourseId) != null
								? x.FPCSCourse.ILPs.FirstOrDefault(y => y.FPCSCourseId == x.FPCSCourseId).ILPName
								: String.Empty,
						ASDCourseCode = x.FPCSCourse.ASDCourse.ExternalASDCourseId,
						GradeCredit = x.FPCSCourse.ASDCourse.GradCredit,
						x.FPCSCourse.ScheduleRepetition,
						x.FPCSCourse.CourseScheduleOnDays,
						x.FPCSCourse.TeacherId,
						TeacherName = x.FPCSCourse.Teacher.FullName,
						x.FPCSCourse.GuardianId,
						GuardianName = x.FPCSCourse.Guardian.FullName,
						x.FPCSCourse.VendorId,
						VendorName = x.FPCSCourse.Vendor.LastName + ", " + x.FPCSCourse.Vendor.FirstName,
						x.FPCSCourse.ASDCourse.SubjectId,
						Subject = x.FPCSCourse.ASDCourse.Subject.Name,
						SubjectIsElective = x.FPCSCourse.ASDCourse.Subject.IsElective,
						x.FPCSCourse.Semester,
						x.Repeat,
						Qtr11 = x.Q11,
						Qtr12 = x.Q12,
						Qtr13 = x.Q13,
						x.Q1,
						x.Q1Comment,
						x.Q1IsLock,
						Qtr21 = x.Q21,
						Qtr22 = x.Q22,
						Qtr23 = x.Q23,
						x.Q2,
						x.Q2Comment,
						x.Q2IsLock,
						x.Sem1,
						x.Sem1Credit,
						x.Sem1Comment,
						x.Sem1IsLock,
						Qtr31 = x.Q31,
						Qtr32 = x.Q32,
						Qtr33 = x.Q33,
						x.Q3,
						x.Q3Comment,
						x.Q3IsLock,
						Qtr41 = x.Q41,
						Qtr42 = x.Q42,
						Qtr43 = x.Q43,
						x.Q4,
						x.Q4Comment,
						x.Q4IsLock,
						x.Sem2,
						x.Sem2Credit,
						x.Sem2Comment,
						x.Sem2IsLock,
						Qtr51 = x.Q51,
						Qtr52 = x.Q52,
						Qtr53 = x.Q53,
						x.Q5,
						x.Q5Comment,
						x.Q5IsLock,
						Qtr61 = x.Q61,
						Qtr62 = x.Q62,
						Qtr63 = x.Q63,
						x.Q6,
						x.Q6Comment,
						x.Q6IsLock,
						x.Sem3,
						x.Sem3Credit,
						x.Sem3Comment,
						x.Sem3IsLock,
						x.IsInSystem,
						SponsorSignatureGuid = x.StudentPacket.SponsorTeacherId,
						x.SponsorSignature,
						InstructorSignatureGuid = x.FPCSCourse.TeacherId,
						x.InstructorSignature,
						x.AdminSignature,
						x.GuardianSignature,
						ContractHrs = x.FPCSCourse.TotalHours,
						ILPHrs = x.ILPs.FirstOrDefault() != null ? x.ILPs.FirstOrDefault().CourseHrs : 0,
						HasSponsorAlert = x.StudentPacketCourseAlerts.Count(t => t.User.Role == Role.Teacher) != 0,
						HasParentAlert = x.StudentPacketCourseAlerts.Count(t => t.User.Role == Role.Guardian) != 0
					})
					.ToList()
					.Select(x => new StudentPacketCourseModel
					{
						StudentPacketCourseId = x.StudentPacketCourseId,
						FPCSCourseId = x.FPCSCourseId,
						FPCSCourseName = x.FPCSCourseName,
						ILPName = x.ILPName,
						StudentPacketCourseName = string.Format(@"{0}, {1}, [{2}]",
							x.FPCSCourseName,
							x.TeacherId.HasValue ? x.TeacherName : x.VendorId.HasValue ? x.VendorName : x.GuardianName,
							x.ASDCourseCode),
						GradeCredit = x.GradeCredit,
						TeacherId = x.TeacherId,
						TeacherName = x.TeacherName,
						GuardianId = x.GuardianId,
						GuardianName = x.GuardianName,
						VendorId = x.VendorId,
						VendorName = x.VendorName,
						SubjectId = x.SubjectId,
						Subject = x.Subject,
						SubjectIsElective = x.SubjectIsElective,
						Semester = x.Semester,
						Repeat = x.Repeat,
						Qtr11 = x.Qtr11,
						Qtr12 = x.Qtr12,
						Qtr13 = x.Qtr13,
						Q1 = x.Q1,
						Q1Comment = x.Q1Comment,
						Q1IsLock = x.Q1IsLock,
						Qtr21 = x.Qtr21,
						Qtr22 = x.Qtr22,
						Qtr23 = x.Qtr23,
						Q2 = x.Q2,
						Q2Comment = x.Q2Comment,
						Q2IsLock = x.Q2IsLock,
						Sem1 = x.Sem1,
						Sem1Credit = x.Sem1Credit,
						Sem1Comment = x.Sem1Comment,
						Sem1IsLock = x.Sem1IsLock,
						Qtr31 = x.Qtr31,
						Qtr32 = x.Qtr32,
						Qtr33 = x.Qtr33,
						Q3 = x.Q3,
						Q3Comment = x.Q3Comment,
						Q3IsLock = x.Q3IsLock,
						Qtr41 = x.Qtr41,
						Qtr42 = x.Qtr42,
						Qtr43 = x.Qtr43,
						Q4 = x.Q4,
						Q4Comment = x.Q4Comment,
						Q4IsLock = x.Q4IsLock,
						Sem2 = x.Sem2,
						Sem2Credit = x.Sem2Credit,
						Sem2Comment = x.Sem2Comment,
						Sem2IsLock = x.Sem2IsLock,
						Qtr51 = x.Qtr51,
						Qtr52 = x.Qtr52,
						Qtr53 = x.Qtr53,
						Q5 = x.Q5,
						Q5Comment = x.Q5Comment,
						Q5IsLock = x.Q5IsLock,
						Qtr61 = x.Qtr61,
						Qtr62 = x.Qtr62,
						Qtr63 = x.Qtr63,
						Q6 = x.Q6,
						Q6Comment = x.Q6Comment,
						Q6IsLock = x.Q6IsLock,
						Sem3 = x.Sem3,
						Sem3Credit = x.Sem3Credit,
						Sem3Comment = x.Sem3Comment,
						Sem3IsLock = x.Sem3IsLock,
						IsInSystem = x.IsInSystem ? "Yes" : "No",
						TableGoodAndService = new TableGoodAndServiceModel
						{
							GoodsAndServices = goodService.Where(t => t.StudentPacketCourseID == x.StudentPacketCourseId)
								.ToList(),
							PlanningBudgetTotal = goodService.Where(t => t.StudentPacketCourseID == x.StudentPacketCourseId && t.Status != "rejc")
								.Sum(t => t.TotalPlanningBudget)*-1,
							ActualSpendingTotal = goodService.Where(t => t.StudentPacketCourseID == x.StudentPacketCourseId)
								.Sum(t => t.TotalActualSpending)*-1
						}
					})
					.ToList();

				var achievedStudentPacket =
					BudgetStudentHelper.GetAchievedStudentPacket(
						model.GoalTotalUnits,
						model.StudentPacketCourseModels
							.Select(x => new StudentPacketCourseHelperModel
							{
								IsElective = x.SubjectIsElective,
								ContractHrs = x.ContractHrs
							})
                            .ToList()
						);

				model.AchievedILPHrs = achievedStudentPacket.AchievedILPHrs;
				model.AchievedContractHrs = achievedStudentPacket.AchievedContractHrs;
				model.AchievedCoreUnits = achievedStudentPacket.AchievedCoreUnits;
				model.AchievedElectiveUnits = achievedStudentPacket.AchievedElectiveUnits;
				model.AchievedTotalUnits = achievedStudentPacket.AchievedTotalUnits;
				model.AchievedEnrollmentPercent = achievedStudentPacket.AchievedEnrollmentPercent;

				var individualBudgetStudentPacket =
					BudgetStudentHelper.GetIndividualBudgetStudentPacket(
						model.IndividualBudgetBudgetLimit,
						model.IndividualBudgetGlobalReserve,
						model.StudentPacketCourseModels
							.Select(x => new StudentPacketCourseHelperModel
							{
								SubjectId = x.SubjectId,
								IsElective = x.SubjectIsElective,
								PlanningBudgetTotal = x.TableGoodAndService.PlanningBudgetTotal,
								ActualSpendingTotal = x.TableGoodAndService.ActualSpendingTotal
							})
							.ToList()
						);

				model.IndividualBudgetBudgetLimit = individualBudgetStudentPacket.IBBudgetLimit;
				model.IndividualBudgetAmountBudgeted = individualBudgetStudentPacket.IBAmountBudgeted;
				model.IndividualBudgetElectiveBalance = individualBudgetStudentPacket.IBElectiveBalance;
				model.IndividualBudgetGlobalReserve = individualBudgetStudentPacket.IBGlobalReserve;

				// Items still needed to complete this packet
				model.IsCompletePacketCourse = true;

				model.ItemsNeedComplete = new List<string>();

				if (model.AchievedCoreUnits < model.GoalCoreUnits)
				{
					model.ItemsNeedComplete.Add(string.Format(@"{0} more Core Units",
						Math.Round(model.GoalCoreUnits - model.AchievedCoreUnits, 1)));
				}
				else
				{
					model.ItemsNeedComplete.Add(string.Empty);
				}

				if (model.AchievedCoreUnits < model.GoalCoreUnits ||
				    (model.AchievedElectiveUnits + model.AchievedCoreUnits) < (model.GoalCoreUnits + model.GoalElectiveUnits))
				{
					model.ItemsNeedComplete.Add(string.Format(@"{0} more Units overall",
						Math.Round(
							(model.GoalCoreUnits + model.GoalElectiveUnits) - (model.AchievedCoreUnits + model.AchievedElectiveUnits), 1)));
				}
				else
				{
					model.ItemsNeedComplete.Add(string.Empty);
				}

				if (model.AchievedContractHrs < model.GoalContractHrs)
				{
					model.ItemsNeedComplete.Add(string.Format(@"{0} more Contract Hours",
						Math.Round(model.GoalContractHrs - model.AchievedContractHrs, 1)));
				}
				else
				{
					model.ItemsNeedComplete.Add(string.Empty);
				}

				if (!model.IsASDTASigned && !model.IsPRASigned)
				{
					model.ItemsNeedComplete.Add("ASD Testing Agreement must be signed and Progress Report Agreement must be signed");
				}
				else if (!model.IsASDTASigned)
				{
					model.ItemsNeedComplete.Add("ASD Testing Agreement must be signed");
				}
				else if (!model.IsPRASigned)
				{
					model.ItemsNeedComplete.Add("Progress Report Agreement must be signed");
				}
				else
				{
					model.ItemsNeedComplete.Add(string.Empty);
				}

				// TODO: Task from 14/01/2015

				//if (String.IsNullOrEmpty(model.ILPPhilosophy))
				//{
				//    model.ItemsNeedComplete.Add("Must provide an ILP Philosophy");
				//}
				//else
				//{
				//    model.ItemsNeedComplete.Add(String.Empty);
				//}

				//if (model.StudentPacketCourseModels.Select(x => x.IsInstructorSponsor).All(x => x == false))
				//{
				//    model.ItemsNeedComplete.Add("Packet must include an ASD Sponsor/Oversight class with at least 1 contract hour.");
				//}
				//else
				//{
				//    model.ItemsNeedComplete.Add(String.Empty);
				//}

				var repoDbUser = uow.GetRepo<IDbUserRepo>();
				var sponsorTeachers = repoDbUser.GetByRoles(SchoolYear.SchoolYearId, Role.Teacher)
					.Select(x => new
					{
						value = x.DbUserId,
						text = x.FullName
					})
					.ToList();

				model.JsonSponsorTeachers = (new JavaScriptSerializer()).Serialize(sponsorTeachers);

				var sponsorSignatures = Sign.MustAmend
					.ToSelectListUsingDescFromIds()
					.Select(x => new
					{
						value = x.Value,
						text = x.Text
					})
					.ToList();

				model.JsonSponsorSignatures = (new JavaScriptSerializer()).Serialize(sponsorSignatures);

				var ampElas = AmpEla.Advanced
					.ToSelectListUsingDescFromIds()
					.Select(x => new
					{
						value = x.Value,
						text = x.Text
					})
					.ToList();

				model.JsonAmpElas = (new JavaScriptSerializer()).Serialize(ampElas);

				var ampMaths = AmpMath.Advanced
					.ToSelectListUsingDescFromIds()
					.Select(x => new
					{
						value = x.Value,
						text = x.Text
					})
					.ToList();

				model.JsonAmpMaths = (new JavaScriptSerializer()).Serialize(ampMaths);

				return model;
			}
		}

		private void CalcPercentageEnrollment(Guid studentId)
		{
			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				var studentRepo = uow.GetRepo<IStudentRepo>();
				var studentPacketRepo = uow.GetRepo<IStudentPacketRepo>();
				var studentPacketCourseRepo = uow.GetRepo<IStudentPacketCourseRepo>();

				var studentPacket = studentPacketRepo.GetAsQuery(studentId, SchoolYear.SchoolYearId).FirstOrDefault();
				var data = studentPacketCourseRepo.GetByStudent(studentId, SchoolYear.SchoolYearId).ToList();

				// WARNING: WTF???
				data = data.Where(x => x.FPCSCourse.Semester == Semester.Semester1).ToList();

				if (studentPacket != null && studentPacket.GuardianSignatureSem1 == Sign.Sign && studentPacket.SponsorSignatureSem1 == Sign.Sign)
				{
					var sumGradeCredit = data.Sum(x => x.FPCSCourse.ASDCourse.GradCredit);

					var student = studentRepo.Get(studentId);

                    //if (sumGradeCredit >= 2m)
                    //    student.PercentagePlanningEnroll = 100;
                    //else if (sumGradeCredit >= 1.5m && sumGradeCredit < 2m)
                    //    student.PercentagePlanningEnroll = 75;
                    //else if (sumGradeCredit < 1.5m)
                    //    student.PercentagePlanningEnroll = 50;

                    if (sumGradeCredit >= 4m)
                        student.PercentagePlanningEnroll = 100;
                    else if (sumGradeCredit >= 3m && sumGradeCredit < 4m)
                        student.PercentagePlanningEnroll = 75;
                    else if (sumGradeCredit >= 2m && sumGradeCredit < 3m)
                        student.PercentagePlanningEnroll = 50;
                    else if (sumGradeCredit >= 1m && sumGradeCredit < 2m)
                        student.PercentagePlanningEnroll = 25;
                    else
                        student.PercentagePlanningEnroll = 0;

					studentRepo.Update(student);
					uow.Commit();
				}
			}
		}
	}
}
