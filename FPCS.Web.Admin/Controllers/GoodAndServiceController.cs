﻿using FPCS.Core.jqGrid;
using FPCS.Data;
using FPCS.Data.Entities;
using FPCS.Data.Enums;
using FPCS.Data.Repo;
using FPCS.Web.Admin.Code;
using FPCS.Web.Admin.Models.GoodAndService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FPCS.Core.Extensions;
using FPCS.Web.Admin.Helpers.GoodAndService;
using FPCS.Web.Admin.Helpers.Access.Models;
using FPCS.Web.Admin.Helpers.Access;
using System.Configuration;
using FPCS.Web.Admin.Helpers.BudgetStudent;
using System.Transactions;

namespace FPCS.Web.Admin.Controllers
{
	[FPCSAuthorize(Role.Admin, Role.Teacher, Role.Guardian)]
	public class GoodAndServiceController : BaseController
	{
		public ActionResult _PreTableGoodAndService(Int64? studentPacketId)
		{
			TableGoodAndServiceModel model = GetTableGoodAndService(null, null, studentPacketId, null, null);
			return PartialView(model);
		}

		public ActionResult _TableGoodAndService(Int32? studentPacketCourseId, Int32? courseId, Int64? studentPacketId, Guid? tempId, Int32? typeCall)
		{
			TableGoodAndServiceModel model = GetTableGoodAndService(studentPacketCourseId, courseId, studentPacketId, tempId, typeCall);
			return PartialView(model);
		}

		public ActionResult _TableGoodAndServiceDetails(Int32? studentPacketCourseId, Int32? courseId, Int64? studentPacketId, Guid? tempId, Int32? typeCall)
		{
			TableGoodAndServiceModel model = GetTableGoodAndService(studentPacketCourseId, courseId, studentPacketId, tempId, typeCall);
			return PartialView(model);
		}

        [HttpGet]
        public Decimal _GetCourseCost(Int64 vendorId)
        {
            var uow = UnityManager.Resolve<IUnitOfWork>();
            var repo = uow.GetRepo<IVendorRepo>();
            var model = repo.GetCourseCost(vendorId);
            return model;
        }

        [HttpGet]
        public Int64 _GetVendorFileStoreId(Int64 vendorId)
        {
            var uow = UnityManager.Resolve<IUnitOfWork>();
            var repo = uow.GetRepo<IVendorRepo>();
            var model = repo.GetVendorFileStoreId(vendorId);
            return model.HasValue ? model.Value : 0;
        }

        [HttpGet]
        public Int32 _GetNumberUnits(Int64 vendorId)
        {
            var uow = UnityManager.Resolve<IUnitOfWork>();
            var repo = uow.GetRepo<IGoodServiceRepo>();
            var model = repo.GetByVendorHasStudentPacketCourse(vendorId).Count();
            return model;
        }
		
		public ActionResult _PrintGoodAndService(Int32? studentPacketCourseId, Int32? courseId, Int32? studentPacketId)
		{
			PrintGoodAndServiceModel model = new PrintGoodAndServiceModel();

			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				var repo = uow.GetRepo<IGoodServiceRepo>();

				Int32 _studentPacketCourseId = studentPacketCourseId.GetValueOrDefault(0);
				Int32 _courseId = courseId.GetValueOrDefault(0);
				Int64 _studentPacketId = studentPacketId.GetValueOrDefault(0);

				var balances = uow.GetRepo<IGoodServiceBalanceRepo>()
					.GetByStudentPacketCourseOrFPCSCourseOrStudentPacket(_studentPacketCourseId, _courseId, _studentPacketId, Guid.Empty)
					.ToList();

				model.GoodsAndServices = repo.GetByStudentPacketCourseOrFPCSCourseOrStudentPacket(_studentPacketCourseId, _courseId, _studentPacketId, Guid.Empty)
					.ToList()
					.Select(x => new GoodAndServiceModel(x, balances))
					.ToList();

				StudentPacketCourse spc = uow.GetRepo<IStudentPacketCourseRepo>().Get(_studentPacketCourseId);

				if (spc != null)
				{
					model.GoodsAndServices.AddRange(balances.Select(x => new GoodAndServiceModel(x)).ToList());
					model.GoodsAndServices.Add(new GoodAndServiceModel(GoodAndServiceHelper.GetInstructorFPCSCourse(spc)));

					model.GoodsAndServices = model.GoodsAndServices
						.Where(x => x.IsShow)
						.OrderByDescending(x => x.IsInstructor)
						.ThenBy(x => x.GoodAndServiceID)
						.ThenBy(x => x.GoodAndServiceBalanceID)
						.ToList();

					model.ClassName = String.Format(@"{0}, {1}, [{2}]",
						spc.FPCSCourse.Name,
						spc.FPCSCourse.TeacherId.HasValue
							? spc.FPCSCourse.Teacher.LastName + ", " + spc.FPCSCourse.Teacher.FirstName
							: spc.FPCSCourse.GuardianId.HasValue
								? spc.FPCSCourse.Guardian.LastName + ", " + spc.FPCSCourse.Guardian.FirstName
								: spc.FPCSCourse.Vendor.BusinessName,
						spc.FPCSCourse.ASDCourse.ExternalASDCourseId);

					model.StudentName = spc.StudentPacket.Student.LastName + ", " + spc.StudentPacket.Student.FirstName;
				}
				else if (_studentPacketId != 0)
				{
					model.GoodsAndServices.AddRange(balances.Select(x => new GoodAndServiceModel(x)).ToList());
					model.GoodsAndServices = model.GoodsAndServices
						.Where(x => x.IsShow)
						.OrderByDescending(x => x.IsInstructor)
						.ThenBy(x => x.GoodAndServiceID)
						.ThenBy(x => x.GoodAndServiceBalanceID)
						.ToList();
				}
				else
				{
					FPCSCourse fpcsCourse = uow.GetRepo<IFPCSCourseRepo>().Get(_courseId);

					model.ClassName = String.Format(@"{0}, {1}, [{2}]",
						fpcsCourse.Name,
						fpcsCourse.TeacherId.HasValue
							? fpcsCourse.Teacher.LastName + ", " + fpcsCourse.Teacher.FirstName
							: fpcsCourse.Guardian.LastName + ", " + fpcsCourse.Guardian.FirstName,
						fpcsCourse.ASDCourse.ExternalASDCourseId);
				}

				model.PrintAddress = (ConfigurationManager.AppSettings["PrintAddress"]).ToString();
				model.PrintState = (ConfigurationManager.AppSettings["PrintState"]).ToString();
				model.PrintPhone = (ConfigurationManager.AppSettings["PrintPhone"]).ToString();
				model.PrintFax = (ConfigurationManager.AppSettings["PrintFax"]).ToString();
				model.PrintDateNow = DateTime.Now.ToString("MM/dd/yyyy");

				model.GrandTotal = model.GoodsAndServices.Where(x => x.Status != "b-rejc").Sum(x => x.BudgetTotal);
			}

			return PartialView(model);
		}

		[HttpGet]
		public ActionResult _DetailsGoodService(Int32 id)
		{
			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				var repo = uow.GetRepo<IGoodServiceRepo>();
				GoodService dbEntity = repo.Get(id);

				if (dbEntity == null)
				{
					throw new Exception("Not found entity!");
				}

				return RedirectToAction("ChoosePartialForm", "GoodAndService", new
				{
					goodOrService = dbEntity.GoodOrService,
					requisitionOrReimbursement = dbEntity.RequisitionOrReimbursement,
					typeRequisitionReimbursement = dbEntity.TypeRequisitionReimbursement,
					studentPacketCourseId = dbEntity.StudentPacketCourseId.GetValueOrDefault(0),
					courseId = dbEntity.FPCSCourseId.GetValueOrDefault(0),
					vendor = dbEntity.Vendor.VendorID,
					isReadOnly = true,
					id = dbEntity.GoodServiceId
				});
			}
		}

		[HttpPost]
		public ActionResult Delete(Int64 id)
		{
			try
			{
				using (var uow = UnityManager.Resolve<IUnitOfWork>())
				{
					var repo = uow.GetRepo<IGoodServiceRepo>();

					var entity = repo.Get(id);
					var studentPacketCourseId = entity.StudentPacketCourseId.GetValueOrDefault(0);
					var fpcsCourseId = entity.FPCSCourseId.GetValueOrDefault(0);
					var studentPacketId = entity.StudentPacketId.GetValueOrDefault(0);

					IsCheckLock(entity.StudentPacketCourseId);

					repo.Remove(id);

					uow.Commit();

					if (studentPacketCourseId == 0 && fpcsCourseId == 0)
					{
						var lstGoodService = repo.GetByPacket(studentPacketId)
							.Select(x => new
							{
								NumberOfUnits = x.NumberOfUnits,
								UnitPrice = x.UnitPrice,
								ShippingHandlingFees = x.ShippingHandlingFees
							})
							.ToList();

						var sponsorship = lstGoodService != null
							? Math.Round(lstGoodService.Sum(t => t.NumberOfUnits*t.UnitPrice + t.ShippingHandlingFees), 2)
							: 0;

						return JsonRes(new {Sponsorship = sponsorship});
					}

					return JsonRes();
				}
			}
			catch (Exception ex)
			{
				return JsonRes(Status.Error, ex.Message);
			}
		}

		[HttpGet]
		public ActionResult __UniversalGoodService(Int64 studentPacketId, Int32 studentPacketCourseId, Int32 courseId,
			String tempId, Int32? goodServiceId, Int32 typeCall = 0)
		{
			var tempGuid = Guid.Empty;
			Guid.TryParse(tempId, out tempGuid);

			var isGeneralExpenses = studentPacketId != 0 && studentPacketCourseId == 0 && courseId == 0 &&
			                        (String.IsNullOrEmpty(tempId) || tempId == Guid.Empty.ToString());

			var model = new UniversalGoodServiceModel(RequisitionOrReimbursement.Requisition,
				isGeneralExpenses ? TypeRequisitionReimbursement.ASDClass : 0, studentPacketId, studentPacketCourseId, courseId,
				tempGuid, false,
				goodServiceId, typeCall);

			model.Init(Session.GetSchoolYear().SchoolYearId, isGeneralExpenses);

			return PartialView(model);
		}

		[HttpPost]
		public ActionResult __UniversalGoodService(UniversalGoodServiceModel model)
		{
			if (String.IsNullOrEmpty(model.TempId))
			{
				model.TempId = Guid.Empty.ToString();
			}

			if (ModelState.IsValid)
			{
				try
				{
					using (var uow = UnityManager.Resolve<IUnitOfWork>())
					{
						if (model.TempId != Guid.Empty.ToString())
						{
							model.StudentPacketId = 0;
							model.StudentPacketCourseId = 0;
							model.CourseId = 0;
						}

						if (model.TypeCall == 1)
						{
						    model.RequisitionOrReimbursement = RequisitionOrReimbursement.Reimbursement;
						}

						var repo = uow.GetRepo<IGoodServiceRepo>();

						IsCheckLock(model.StudentPacketCourseId);

						GoodService dbEntity = repo.Get(model.Id);
						if (dbEntity == null) dbEntity = new GoodService();

						dbEntity.StudentPacketId = model.StudentPacketId != 0 && model.StudentPacketCourseId == 0 && model.CourseId == 0
							? (Int64?) model.StudentPacketId
							: null;
						dbEntity.StudentPacketCourseId = model.StudentPacketCourseId != 0 ? (Int32?) model.StudentPacketCourseId : null;
						dbEntity.FPCSCourseId = model.CourseId != 0 ? (Int32?) model.CourseId : null;
						dbEntity.TempId = model.StudentPacketId == 0 && model.StudentPacketCourseId == 0 && model.CourseId == 0
							? model.TempId
							: null;

						dbEntity.Title = model.Title.TrimAndReduce();
						dbEntity.Description = model.Description.TrimAndReduce();
						dbEntity.AdminComments = model.Comments.TrimAndReduce();
						dbEntity.PublisherISBN = model.PublisherISBN.TrimAndReduce();

						dbEntity.NumberOfUnits = model.NumberOfUnits;
						dbEntity.UnitPrice = model.UnitPrice;
						dbEntity.ShippingHandlingFees = model.ShippingHandlingFees;

						dbEntity.GoodOrService = model.GoodOrService;
						dbEntity.RequisitionOrReimbursement = model.RequisitionOrReimbursement;
						dbEntity.TypeRequisitionReimbursement = model.TypeRequisitionReimbursement;
						dbEntity.VendorId = model.VendorId;

						dbEntity.IsDeleted = false;
						dbEntity.CreatedDate = DateTime.Now;
						dbEntity.UpdatedDate = DateTime.Now;

						dbEntity.UserRequestId = User.UserId;
						dbEntity.DateRequest = DateTime.Now;

						using (var scope = new TransactionScope(TransactionScopeOption.Required))
						{
							if (model.Id == 0)
							{
								dbEntity.GoodOrServiceApproved = GoodOrServiceApproved.b_pend;
								dbEntity = repo.Add(dbEntity);
							}
							else
							{
								repo.Update(dbEntity);
							}

							uow.Commit();

							if (model.StudentPacketCourseId != 0)
							{
								var repoSPC = uow.GetRepo<IStudentPacketCourseRepo>();
								var spc = repoSPC.Get(model.StudentPacketCourseId);

								if (!BudgetStudentHelper.IsResolveGoodService(spc.StudentPacket.StudentId, SchoolYear.SchoolYearId))
								{
									return JsonRes(Status.NoEnoughBudget, BudgetStudentHelper.MessageNoEnoughBudget);
								}
							}
							else if (model.CourseId != 0)
							{
							}
							else if (model.StudentPacketId != 0)
							{
								var repoSP = uow.GetRepo<IStudentPacketRepo>();
								var sp = repoSP.Get(model.StudentPacketId);

								//if (!BudgetStudentHelper.IsResolveSponsorshipGoodService(model.StudentPacketId, SchoolYear.SchoolYearId))
								//{
								//    return JsonRes(Status.NoEnoughBudget, BudgetStudentHelper.MessageNoEnoughSponsorship);
								//}

								if (!BudgetStudentHelper.IsResolveSpecificGoodService(model.VendorId, model.UnitPrice, model.NumberOfUnits, model.ShippingHandlingFees))
								{
									return JsonRes(Status.TotalLess, String.Format(BudgetStudentHelper.TotalLessBudget, BudgetStudentHelper.GetTotalLessSpecificVendor(model.VendorId)));
								}

								if (!BudgetStudentHelper.IsResolveGoodService(sp.StudentId, SchoolYear.SchoolYearId))
								{
									return JsonRes(Status.NoEnoughBudget, BudgetStudentHelper.MessageNoEnoughBudget);
								}
							}

							scope.Complete();
						}

						if (model.StudentPacketCourseId == 0 && model.CourseId == 0 && model.StudentPacketId != 0)
						{
							var lstGoodService = repo.GetByPacket(model.StudentPacketId)
								.Select(x => new
								{
									NumberOfUnits = x.NumberOfUnits,
									UnitPrice = x.UnitPrice,
									ShippingHandlingFees = x.ShippingHandlingFees
								})
								.ToList();

							var sponsorship = lstGoodService != null
								? Math.Round(lstGoodService.Sum(t => t.NumberOfUnits*t.UnitPrice + t.ShippingHandlingFees), 2)
								: 0;

							return JsonRes(new {Sponsorship = sponsorship});
						}

						return JsonRes(dbEntity.GoodServiceId.ToString());
					}
				}
				catch (Exception ex)
				{
					return JsonRes(Status.Error, ex.Message);
				}
			}

			model.Init(Session.GetSchoolYear().SchoolYearId);
			return PartialView(model);
		}

		[HttpGet]
		public ActionResult _CreateGoodAndService()
		{
			var model = new GoodAndServiceCreateModel();

			model.Init(Session.GetSchoolYear().SchoolYearId);

			return PartialView(model);
		}

		public JsonResult GetValueLookup(
			GoodOrService? goodOrService,
			RequisitionOrReimbursement? requisitionOrReimbursement,
			TypeRequisitionReimbursement? typeRequisitionReimbursement,
			Int32? vendor
			)
		{
			List<Object> result = new List<Object>();

			if (goodOrService == null &&
			    requisitionOrReimbursement == null &&
			    typeRequisitionReimbursement == null &&
			    vendor == null)
			{
				// No action
			}
			else if (goodOrService != null &&
			         requisitionOrReimbursement == null &&
			         typeRequisitionReimbursement == null &&
			         vendor == null)
			{
				result.Add(new {Value = "", Text = "Select an Option"});
				result.Add(
					new
					{
						Value = RequisitionOrReimbursement.Requisition,
						Text = RequisitionOrReimbursement.Requisition.GetDescription()
					});
				result.Add(
					new
					{
						Value = RequisitionOrReimbursement.Reimbursement,
						Text = RequisitionOrReimbursement.Reimbursement.GetDescription()
					});
			}
			else if (goodOrService != null &&
			         requisitionOrReimbursement != null &&
			         typeRequisitionReimbursement == null &&
			         vendor == null)
			{
				if (goodOrService == GoodOrService.Good &&
				    (requisitionOrReimbursement == RequisitionOrReimbursement.Requisition ||
				     requisitionOrReimbursement == RequisitionOrReimbursement.Reimbursement))
				{
					result.Add(new {Value = "", Text = "Select an Option"});
					//result.Add(
					//	new
					//	{
					//		Value = TypeRequisitionReimbursement.Administrative,
					//		Text = TypeRequisitionReimbursement.Administrative.GetDescription()
					//	});
					result.Add(
						new {Value = TypeRequisitionReimbursement.Supplies, Text = TypeRequisitionReimbursement.Supplies.GetDescription()});
					result.Add(
						new
						{
							Value = TypeRequisitionReimbursement.TextbookCurriculum,
							Text = TypeRequisitionReimbursement.TextbookCurriculum.GetDescription()
						});
				}
				else if (goodOrService == GoodOrService.Service && requisitionOrReimbursement == RequisitionOrReimbursement.Requisition)
				{
					result.Add(new {Value = "", Text = "Select an Option"});
					result.Add(
						new
						{
							Value = TypeRequisitionReimbursement.ASDClass,
							Text = TypeRequisitionReimbursement.ASDClass.GetDescription()
						});
					result.Add(
						new
						{
							Value = TypeRequisitionReimbursement.UniversityCourseIndStudyClass,
							Text = TypeRequisitionReimbursement.UniversityCourseIndStudyClass.GetDescription()
						});
					result.Add(
						new
						{
							Value = TypeRequisitionReimbursement.VendorService,
							Text = TypeRequisitionReimbursement.VendorService.GetDescription()
						});
				}
				else if (goodOrService == GoodOrService.Service &&
				         requisitionOrReimbursement == RequisitionOrReimbursement.Reimbursement)
				{
					result.Add(new {Value = "", Text = "Select an Option"});
					result.Add(
						new
						{
							Value = TypeRequisitionReimbursement.UniversityCourseIndStudyClass,
							Text = TypeRequisitionReimbursement.UniversityCourseIndStudyClass.GetDescription()
						});
					result.Add(
						new
						{
							Value = TypeRequisitionReimbursement.VendorService,
							Text = TypeRequisitionReimbursement.VendorService.GetDescription()
						});
				}
			}

			return JsonRes(result);
		}

		public ActionResult ChoosePartialForm(GoodOrService goodOrService,
			RequisitionOrReimbursement requisitionOrReimbursement, TypeRequisitionReimbursement typeRequisitionReimbursement,
			Int32 vendor, Int32 studentPacketCourseId, Int32 courseId, Boolean isReadOnly, Int32? id)
		{
			//if (goodOrService == GoodOrService.Good &&
			//    typeRequisitionReimbursement == TypeRequisitionReimbursement.Administrative)
			//{
			//	return RedirectToAction("__GoodAdministrative", "GoodAndService",
			//		new
			//		{
			//			goodOrService = goodOrService,
			//			requisitionOrReimbursement = requisitionOrReimbursement,
			//			typeRequisitionReimbursement = typeRequisitionReimbursement,
			//			studentPacketCourseId = studentPacketCourseId,
			//			courseId = courseId,
			//			vendor = vendor,
			//			isReadOnly = isReadOnly,
			//			id = id
			//		});
			//}
			//else 
				if (goodOrService == GoodOrService.Good && typeRequisitionReimbursement == TypeRequisitionReimbursement.Supplies)
			{
				return RedirectToAction("__GoodSupplies", "GoodAndService",
					new
					{
						goodOrService = goodOrService,
						requisitionOrReimbursement = requisitionOrReimbursement,
						typeRequisitionReimbursement = typeRequisitionReimbursement,
						studentPacketCourseId = studentPacketCourseId,
						courseId = courseId,
						vendor = vendor,
						isReadOnly = isReadOnly,
						id = id
					});
			}
			else if (goodOrService == GoodOrService.Good &&
			         typeRequisitionReimbursement == TypeRequisitionReimbursement.TextbookCurriculum)
			{
				return RedirectToAction("__GoodTextbook", "GoodAndService",
					new
					{
						goodOrService = goodOrService,
						requisitionOrReimbursement = requisitionOrReimbursement,
						typeRequisitionReimbursement = typeRequisitionReimbursement,
						studentPacketCourseId = studentPacketCourseId,
						courseId = courseId,
						vendor = vendor,
						isReadOnly = isReadOnly,
						id = id
					});
			}
				if (goodOrService == GoodOrService.Service &&
			         typeRequisitionReimbursement == TypeRequisitionReimbursement.ASDClass)
			{
				return RedirectToAction("__ServiceASDClass", "GoodAndService",
					new
					{
						goodOrService = goodOrService,
						requisitionOrReimbursement = requisitionOrReimbursement,
						typeRequisitionReimbursement = typeRequisitionReimbursement,
						studentPacketCourseId = studentPacketCourseId,
						courseId = courseId,
						vendor = vendor,
						isReadOnly = isReadOnly,
						id = id
					});
			}
			else if (goodOrService == GoodOrService.Service &&
			         typeRequisitionReimbursement == TypeRequisitionReimbursement.UniversityCourseIndStudyClass)
			{
				return RedirectToAction("__ServiceUniversityCourse", "GoodAndService",
					new
					{
						goodOrService = goodOrService,
						requisitionOrReimbursement = requisitionOrReimbursement,
						typeRequisitionReimbursement = typeRequisitionReimbursement,
						studentPacketCourseId = studentPacketCourseId,
						courseId = courseId,
						vendor = vendor,
						isReadOnly = isReadOnly,
						id = id
					});
			}
			else
			//if (goodOrService == GoodOrService.Service && typeRequisitionReimbursement == TypeRequisitionReimbursement.VendorService)
			{
				return RedirectToAction("__ServiceVendorService", "GoodAndService",
					new
					{
						goodOrService = goodOrService,
						requisitionOrReimbursement = requisitionOrReimbursement,
						typeRequisitionReimbursement = typeRequisitionReimbursement,
						studentPacketCourseId = studentPacketCourseId,
						courseId = courseId,
						vendor = vendor,
						isReadOnly = isReadOnly,
						id = id
					});
			}
		}

		#region partial view goods and services

		[HttpGet]
		public ActionResult __GoodAdministrative(GoodOrService goodOrService,
			RequisitionOrReimbursement requisitionOrReimbursement, TypeRequisitionReimbursement typeRequisitionReimbursement,
			Int32 vendor, Int32 studentPacketCourseId, Int32 courseId, Boolean isReadOnly, Int32? id)
		{
			var model = new GoodAdministrativeModel(goodOrService, requisitionOrReimbursement, typeRequisitionReimbursement,
				vendor, studentPacketCourseId, courseId, isReadOnly, id);
			return PartialView(model);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult __GoodAdministrative(GoodAdministrativeModel model)
		{
			if (ModelState.IsValid)
			{
				try
				{
					using (var uow = UnityManager.Resolve<IUnitOfWork>())
					{
						var repo = uow.GetRepo<IGoodServiceRepo>();

						IsCheckLock(model.StudentPacketCourseId);

						GoodService dbEntity = new GoodService();

						dbEntity.GoodServiceId = model.Id;
						dbEntity.StudentPacketCourseId = model.StudentPacketCourseId != 0 ? (Int32?) model.StudentPacketCourseId : null;
						dbEntity.FPCSCourseId = model.CourseId != 0 ? (Int32?) model.CourseId : null;

						dbEntity.LineItemName = model.LineItemName.TrimAndReduce();
						dbEntity.LineItemDesc = model.LineItemDesc.TrimAndReduce();
						dbEntity.AdminComments = model.AdminComments.TrimAndReduce();
						dbEntity.NumberOfUnits = model.NumberOfUnits;
						dbEntity.UnitPrice = model.UnitPrice;
						dbEntity.ApprovalGivenBy = model.ApprovalGivenBy;
						dbEntity.ShippingHandlingFees = model.ShippingHandlingFees;

						dbEntity.GoodOrService = model.GoodOrService;
						dbEntity.RequisitionOrReimbursement = model.RequisitionOrReimbursement;
						dbEntity.TypeRequisitionReimbursement = model.TypeRequisitionReimbursement;
						dbEntity.VendorId = model.VendorId;

						dbEntity.IsDeleted = false;
						dbEntity.CreatedDate = DateTime.Now;
						dbEntity.UpdatedDate = DateTime.Now;

						SetClosedAndApproved(uow, dbEntity, model.Id, model.IsClosed, model.Aprv);

						if (model.Id == 0)
						{
							dbEntity.GoodOrServiceApproved = GoodOrServiceApproved.b_pend;
							dbEntity = repo.Add(dbEntity);
						}
						else
						{
							repo.Update(dbEntity);
						}

						uow.Commit();

						return JsonRes(dbEntity.GoodServiceId.ToString());
					}
				}
				catch (Exception ex)
				{
					return JsonRes(Status.Error, ex.Message);
				}
			}

			model.Init();
			return PartialView(model);
		}

		[HttpGet]
		public ActionResult __GoodSupplies(GoodOrService goodOrService, RequisitionOrReimbursement requisitionOrReimbursement,
			TypeRequisitionReimbursement typeRequisitionReimbursement, Int32 vendor, Int32 studentPacketCourseId, Int32 courseId,
			Boolean isReadOnly, Int32? id)
		{
			var model = new GoodSuppliesModel(goodOrService, requisitionOrReimbursement, typeRequisitionReimbursement, vendor,
				studentPacketCourseId, courseId, isReadOnly, id);
			return PartialView(model);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult __GoodSupplies(GoodSuppliesModel model)
		{
			if (ModelState.IsValid)
			{
				try
				{
					using (var uow = UnityManager.Resolve<IUnitOfWork>())
					{
						var repo = uow.GetRepo<IGoodServiceRepo>();

						IsCheckLock(model.StudentPacketCourseId);

						GoodService dbEntity = new GoodService();

						dbEntity.GoodServiceId = model.Id;
						dbEntity.StudentPacketCourseId = model.StudentPacketCourseId != 0 ? (Int32?) model.StudentPacketCourseId : null;
						dbEntity.FPCSCourseId = model.CourseId != 0 ? (Int32?) model.CourseId : null;

						dbEntity.SupplyType = model.SupplyType;
						dbEntity.LineItemName = model.ItemName.TrimAndReduce();
						dbEntity.Description = model.Description.TrimAndReduce();
						dbEntity.Count = model.Count;
						dbEntity.CatalogMFG = model.CatalogMFG.TrimAndReduce();
						dbEntity.CatalogIssuePG = model.CatalogIssuePG.TrimAndReduce();
						dbEntity.UnitType = model.UnitType;
						dbEntity.BarCode = model.BarCode.TrimAndReduce();
						dbEntity.Consumable = model.Consumable;
						dbEntity.AdminComments = model.AdminComments.TrimAndReduce();
						dbEntity.ApprovalGivenBy = model.ApprovalGivenBy;
						dbEntity.NumberOfUnits = model.NumberOfUnits;
						dbEntity.UnitPrice = model.UnitPrice;
						dbEntity.ShippingHandlingFees = model.ShippingHandlingFees;

						dbEntity.GoodOrService = model.GoodOrService;
						dbEntity.RequisitionOrReimbursement = model.RequisitionOrReimbursement;
						dbEntity.TypeRequisitionReimbursement = model.TypeRequisitionReimbursement;
						dbEntity.VendorId = model.VendorId;

						dbEntity.IsDeleted = false;
						dbEntity.CreatedDate = DateTime.Now;
						dbEntity.UpdatedDate = DateTime.Now;

						SetClosedAndApproved(uow, dbEntity, model.Id, model.IsClosed, model.Aprv);

						if (model.Id == 0)
						{
							dbEntity.GoodOrServiceApproved = GoodOrServiceApproved.b_pend;
							dbEntity = repo.Add(dbEntity);
						}
						else
						{
							repo.Update(dbEntity);
						}

						uow.Commit();

						return JsonRes(dbEntity.GoodServiceId.ToString());
					}
				}
				catch (Exception ex)
				{
					return JsonRes(Status.Error, ex.Message);
				}
			}

			model.Init();
			return PartialView(model);
		}

		[HttpGet]
		public ActionResult __GoodTextbook(GoodOrService goodOrService, RequisitionOrReimbursement requisitionOrReimbursement,
			TypeRequisitionReimbursement typeRequisitionReimbursement, Int32 vendor, Int32 studentPacketCourseId, Int32 courseId,
			Boolean isReadOnly, Int32? id)
		{
			var model = new GoodTextbookCurriculumModel(goodOrService, requisitionOrReimbursement, typeRequisitionReimbursement,
				vendor, studentPacketCourseId, courseId, isReadOnly, id);
			return PartialView(model);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult __GoodTextbook(GoodTextbookCurriculumModel model)
		{
			if (ModelState.IsValid)
			{
				try
				{
					using (var uow = UnityManager.Resolve<IUnitOfWork>())
					{
						var repo = uow.GetRepo<IGoodServiceRepo>();

						IsCheckLock(model.StudentPacketCourseId);

						GoodService dbEntity = new GoodService();

						dbEntity.GoodServiceId = model.Id;
						dbEntity.StudentPacketCourseId = model.StudentPacketCourseId != 0 ? (Int32?) model.StudentPacketCourseId : null;
						dbEntity.FPCSCourseId = model.CourseId != 0 ? (Int32?) model.CourseId : null;

						dbEntity.TypeTextbookCurriculum = model.TypeTextbookCurriculum;
						dbEntity.Title = model.Title.TrimAndReduce();
						dbEntity.Author = model.Author.TrimAndReduce();
						dbEntity.PublisherISBN = model.PublisherISBN.TrimAndReduce();
						dbEntity.PublisherName = model.PublisherName.TrimAndReduce();
						dbEntity.Edition = model.Edition.TrimAndReduce();
						dbEntity.NumberOfUnits = model.NumberOfUnits;
						dbEntity.UnitPrice = model.UnitPrice;
						dbEntity.ShippingHandlingFees = model.ShippingHandlingFees;

						dbEntity.GoodOrService = model.GoodOrService;
						dbEntity.RequisitionOrReimbursement = model.RequisitionOrReimbursement;
						dbEntity.TypeRequisitionReimbursement = model.TypeRequisitionReimbursement;
						dbEntity.VendorId = model.VendorId;

						dbEntity.IsDeleted = false;
						dbEntity.CreatedDate = DateTime.Now;
						dbEntity.UpdatedDate = DateTime.Now;

						SetClosedAndApproved(uow, dbEntity, model.Id, model.IsClosed, model.Aprv);

						if (model.Id == 0)
						{
							dbEntity.GoodOrServiceApproved = GoodOrServiceApproved.b_pend;
							dbEntity = repo.Add(dbEntity);
						}
						else
						{
							repo.Update(dbEntity);
						}

						uow.Commit();

						return JsonRes(dbEntity.GoodServiceId.ToString());
					}
				}
				catch (Exception ex)
				{
					return JsonRes(Status.Error, ex.Message);
				}
			}

			model.Init();
			return PartialView(model);
		}

		[HttpGet]
		public ActionResult __ServiceAdministrative(GoodOrService goodOrService,
			RequisitionOrReimbursement requisitionOrReimbursement, TypeRequisitionReimbursement typeRequisitionReimbursement,
			Int32 vendor, Int32 studentPacketCourseId, Int32 courseId, Boolean isReadOnly, Int32? id)
		{
			var model = new ServiceAdministrativeModel(goodOrService, requisitionOrReimbursement, typeRequisitionReimbursement,
				vendor, studentPacketCourseId, courseId, isReadOnly, id);
			return PartialView(model);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult __ServiceAdministrative(ServiceAdministrativeModel model)
		{
			if (ModelState.IsValid)
			{
				try
				{
					using (var uow = UnityManager.Resolve<IUnitOfWork>())
					{
						var repo = uow.GetRepo<IGoodServiceRepo>();

						IsCheckLock(model.StudentPacketCourseId);

						GoodService dbEntity = new GoodService();

						dbEntity.GoodServiceId = model.Id;
						dbEntity.StudentPacketCourseId = model.StudentPacketCourseId != 0 ? (Int32?) model.StudentPacketCourseId : null;
						dbEntity.FPCSCourseId = model.CourseId != 0 ? (Int32?) model.CourseId : null;

						dbEntity.LineItemName = model.LineItemName.TrimAndReduce();
						dbEntity.LineItemDesc = model.LineItemDesc.TrimAndReduce();
						dbEntity.AdminComments = model.AdminComments.TrimAndReduce();
						dbEntity.NumberOfUnits = model.NumberOfUnits;
						dbEntity.UnitPrice = model.UnitPrice;
						dbEntity.ShippingHandlingFees = model.ShippingHandlingFees;

						dbEntity.GoodOrService = model.GoodOrService;
						dbEntity.RequisitionOrReimbursement = model.RequisitionOrReimbursement;
						dbEntity.TypeRequisitionReimbursement = model.TypeRequisitionReimbursement;
						dbEntity.VendorId = model.VendorId;

						dbEntity.IsDeleted = false;
						dbEntity.CreatedDate = DateTime.Now;
						dbEntity.UpdatedDate = DateTime.Now;

						SetClosedAndApproved(uow, dbEntity, model.Id, model.IsClosed, model.Aprv);

						if (model.Id == 0)
						{
							dbEntity.GoodOrServiceApproved = GoodOrServiceApproved.b_pend;
							dbEntity = repo.Add(dbEntity);
						}
						else
						{
							repo.Update(dbEntity);
						}

						uow.Commit();

						return JsonRes(dbEntity.GoodServiceId.ToString());
					}
				}
				catch (Exception ex)
				{
					return JsonRes(Status.Error, ex.Message);
				}
			}

			model.Init();
			return PartialView(model);
		}

		[HttpGet]
		public ActionResult __ServiceASDClass(GoodOrService goodOrService,
			RequisitionOrReimbursement requisitionOrReimbursement, TypeRequisitionReimbursement typeRequisitionReimbursement,
			Int32 vendor, Int32 studentPacketCourseId, Int32 courseId, Boolean isReadOnly, Int32? id)
		{
			var model = new ServiceASDClassModel(goodOrService, requisitionOrReimbursement, typeRequisitionReimbursement, vendor,
				studentPacketCourseId, courseId, isReadOnly, id);
			return PartialView(model);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult __ServiceASDClass(ServiceASDClassModel model)
		{
			if (ModelState.IsValid)
			{
				try
				{
					using (var uow = UnityManager.Resolve<IUnitOfWork>())
					{
						var repo = uow.GetRepo<IGoodServiceRepo>();

						IsCheckLock(model.StudentPacketCourseId);

						GoodService dbEntity = new GoodService();

						dbEntity.GoodServiceId = model.Id;
						dbEntity.StudentPacketCourseId = model.StudentPacketCourseId != 0 ? (Int32?) model.StudentPacketCourseId : null;
						dbEntity.FPCSCourseId = model.CourseId != 0 ? (Int32?) model.CourseId : null;

						dbEntity.HighSchool = model.HighSchool;
						dbEntity.AdminComments = model.AdminComments.TrimAndReduce();
						dbEntity.ApprovalGivenBy = model.ApprovalGivenBy;
						dbEntity.NumberSemesters = model.NumberOfSemesters;
						dbEntity.CostPerSemester = model.CostPerSemester;

						dbEntity.GoodOrService = model.GoodOrService;
						dbEntity.RequisitionOrReimbursement = model.RequisitionOrReimbursement;
						dbEntity.TypeRequisitionReimbursement = model.TypeRequisitionReimbursement;
						dbEntity.VendorId = model.VendorId;

						dbEntity.IsDeleted = false;
						dbEntity.CreatedDate = DateTime.Now;
						dbEntity.UpdatedDate = DateTime.Now;

						SetClosedAndApproved(uow, dbEntity, model.Id, model.IsClosed, model.Aprv);

						if (model.Id == 0)
						{
							dbEntity.GoodOrServiceApproved = GoodOrServiceApproved.b_pend;
							dbEntity = repo.Add(dbEntity);
						}
						else
						{
							repo.Update(dbEntity);
						}

						uow.Commit();

						return JsonRes(dbEntity.GoodServiceId.ToString());
					}
				}
				catch (Exception ex)
				{
					return JsonRes(Status.Error, ex.Message);
				}
			}

			model.Init();
			return PartialView(model);
		}

		[HttpGet]
		public ActionResult __ServiceComputerLease(GoodOrService goodOrService,
			RequisitionOrReimbursement requisitionOrReimbursement, TypeRequisitionReimbursement typeRequisitionReimbursement,
			Int32 vendor, Int32 studentPacketCourseId, Int32 courseId, Boolean isReadOnly, Int32? id)
		{
			var model = new ServiceComputerLeaseModel(goodOrService, requisitionOrReimbursement, typeRequisitionReimbursement,
				vendor, studentPacketCourseId, courseId, isReadOnly, id);
			return PartialView(model);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult __ServiceComputerLease(ServiceComputerLeaseModel model)
		{
			if (ModelState.IsValid)
			{
				try
				{
					using (var uow = UnityManager.Resolve<IUnitOfWork>())
					{
						var repo = uow.GetRepo<IGoodServiceRepo>();

						IsCheckLock(model.StudentPacketCourseId);

						GoodService dbEntity = new GoodService();

						dbEntity.GoodServiceId = model.Id;
						dbEntity.StudentPacketCourseId = model.StudentPacketCourseId != 0 ? (Int32?) model.StudentPacketCourseId : null;
						dbEntity.FPCSCourseId = model.CourseId != 0 ? (Int32?) model.CourseId : null;

						dbEntity.ComputerLeaseItemType = model.ComputerLeaseItemType;
						dbEntity.ComputerLeaseItemDesc = model.ComputerLeaseItemDesc;
						dbEntity.UnitType = model.UnitType;
						dbEntity.BarCode = model.BarCode.TrimAndReduce();
						dbEntity.AdminComments = model.AdminComments.TrimAndReduce();
						dbEntity.ApprovalGivenBy = model.ApprovalGivenBy;
						dbEntity.DetailedItemDesc = model.DetailedItemDesc.TrimAndReduce();
						dbEntity.NumberOfUnits = model.NumberOfUnits;
						dbEntity.UnitPrice = model.UnitPrice;

						dbEntity.GoodOrService = model.GoodOrService;
						dbEntity.RequisitionOrReimbursement = model.RequisitionOrReimbursement;
						dbEntity.TypeRequisitionReimbursement = model.TypeRequisitionReimbursement;
						dbEntity.VendorId = model.VendorId;

						dbEntity.IsDeleted = false;
						dbEntity.CreatedDate = DateTime.Now;
						dbEntity.UpdatedDate = DateTime.Now;

						SetClosedAndApproved(uow, dbEntity, model.Id, model.IsClosed, model.Aprv);

						if (model.Id == 0)
						{
							dbEntity.GoodOrServiceApproved = GoodOrServiceApproved.b_pend;
							dbEntity = repo.Add(dbEntity);
						}
						else
						{
							repo.Update(dbEntity);
						}

						uow.Commit();

						return JsonRes(dbEntity.GoodServiceId.ToString());
					}
				}
				catch (Exception ex)
				{
					return JsonRes(Status.Error, ex.Message);
				}
			}

			model.Init();
			return PartialView(model);
		}

		[HttpGet]
		public ActionResult __ServiceInternet(GoodOrService goodOrService,
			RequisitionOrReimbursement requisitionOrReimbursement, TypeRequisitionReimbursement typeRequisitionReimbursement,
			Int32 vendor, Int32 studentPacketCourseId, Int32 courseId, Boolean isReadOnly, Int32? id)
		{
			var model = new ServiceInternetModel(goodOrService, requisitionOrReimbursement, typeRequisitionReimbursement, vendor,
				studentPacketCourseId, courseId, isReadOnly, id);
			return PartialView(model);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult __ServiceInternet(ServiceInternetModel model)
		{
			if (ModelState.IsValid)
			{
				try
				{
					using (var uow = UnityManager.Resolve<IUnitOfWork>())
					{
						var repo = uow.GetRepo<IGoodServiceRepo>();

						IsCheckLock(model.StudentPacketCourseId);

						GoodService dbEntity = new GoodService();

						dbEntity.GoodServiceId = model.Id;
						dbEntity.StudentPacketCourseId = model.StudentPacketCourseId != 0 ? (Int32?) model.StudentPacketCourseId : null;
						dbEntity.FPCSCourseId = model.CourseId != 0 ? (Int32?) model.CourseId : null;

						dbEntity.InternetItemType = model.InternetItemType;
						dbEntity.Description = model.Description.TrimAndReduce();
						dbEntity.NumberOfUnits = model.NumberOfUnits;
						dbEntity.UnitPrice = model.UnitPrice;

						dbEntity.GoodOrService = model.GoodOrService;
						dbEntity.RequisitionOrReimbursement = model.RequisitionOrReimbursement;
						dbEntity.TypeRequisitionReimbursement = model.TypeRequisitionReimbursement;
						dbEntity.VendorId = model.VendorId;

						dbEntity.IsDeleted = false;
						dbEntity.CreatedDate = DateTime.Now;
						dbEntity.UpdatedDate = DateTime.Now;

						SetClosedAndApproved(uow, dbEntity, model.Id, model.IsClosed, model.Aprv);

						if (model.Id == 0)
						{
							dbEntity.GoodOrServiceApproved = GoodOrServiceApproved.b_pend;
							dbEntity = repo.Add(dbEntity);
						}
						else
						{
							repo.Update(dbEntity);
						}

						uow.Commit();

						return JsonRes(dbEntity.GoodServiceId.ToString());
					}
				}
				catch (Exception ex)
				{
					return JsonRes(Status.Error, ex.Message);
				}
			}

			model.Init();
			return PartialView(model);
		}

		[HttpGet]
		public ActionResult __ServiceUniversityCourse(GoodOrService goodOrService,
			RequisitionOrReimbursement requisitionOrReimbursement, TypeRequisitionReimbursement typeRequisitionReimbursement,
			Int32 vendor, Int32 studentPacketCourseId, Int32 courseId, Boolean isReadOnly, Int32? id)
		{
			var model = new ServiceUniversityCourseModel(goodOrService, requisitionOrReimbursement, typeRequisitionReimbursement,
				vendor, studentPacketCourseId, courseId, isReadOnly, id);
			return PartialView(model);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult __ServiceUniversityCourse(ServiceUniversityCourseModel model)
		{
			if (ModelState.IsValid)
			{
				try
				{
					using (var uow = UnityManager.Resolve<IUnitOfWork>())
					{
						var repo = uow.GetRepo<IGoodServiceRepo>();

						IsCheckLock(model.StudentPacketCourseId);

						GoodService dbEntity = new GoodService();

						dbEntity.GoodServiceId = model.Id;
						dbEntity.StudentPacketCourseId = model.StudentPacketCourseId != 0 ? (Int32?) model.StudentPacketCourseId : null;
						dbEntity.FPCSCourseId = model.CourseId != 0 ? (Int32?) model.CourseId : null;

						dbEntity.ClassType = model.ClassType;
						dbEntity.CourseTitle = model.CourseTitle.TrimAndReduce();
						dbEntity.CourseNumber = model.CourseNumber.TrimAndReduce();
						dbEntity.DateBirth = model.DateBirth;
						dbEntity.Credits = model.Credits;
						dbEntity.Semester = model.Semester;
						dbEntity.AdminComments = model.AdminComments.TrimAndReduce();
						dbEntity.ApprovalGivenBy = model.ApprovalGivenBy;
						dbEntity.NumberOfUnits = model.NumberOfUnits;
						dbEntity.UnitPrice = model.UnitPrice;

						dbEntity.GoodOrService = model.GoodOrService;
						dbEntity.RequisitionOrReimbursement = model.RequisitionOrReimbursement;
						dbEntity.TypeRequisitionReimbursement = model.TypeRequisitionReimbursement;
						dbEntity.VendorId = model.VendorId;

						dbEntity.IsDeleted = false;
						dbEntity.CreatedDate = DateTime.Now;
						dbEntity.UpdatedDate = DateTime.Now;

						SetClosedAndApproved(uow, dbEntity, model.Id, model.IsClosed, model.Aprv);

						if (model.Id == 0)
						{
							dbEntity.GoodOrServiceApproved = GoodOrServiceApproved.b_pend;
							dbEntity = repo.Add(dbEntity);
						}
						else
						{
							repo.Update(dbEntity);
						}

						uow.Commit();

						return JsonRes(dbEntity.GoodServiceId.ToString());
					}
				}
				catch (Exception ex)
				{
					return JsonRes(Status.Error, ex.Message);
				}
			}

			model.Init();
			return PartialView(model);
		}

		[HttpGet]
		public ActionResult __ServiceVendorService(GoodOrService goodOrService,
			RequisitionOrReimbursement requisitionOrReimbursement, TypeRequisitionReimbursement typeRequisitionReimbursement,
			Int32 vendor, Int32 studentPacketCourseId, Int32 courseId, Boolean isReadOnly, Int32? id)
		{
			var model = new ServiceVendorServiceModel(goodOrService, requisitionOrReimbursement, typeRequisitionReimbursement,
				vendor, studentPacketCourseId, courseId, isReadOnly, id);
			return PartialView(model);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult __ServiceVendorService(ServiceVendorServiceModel model)
		{
			if (ModelState.IsValid)
			{
				try
				{
					using (var uow = UnityManager.Resolve<IUnitOfWork>())
					{
						var repo = uow.GetRepo<IGoodServiceRepo>();

						IsCheckLock(model.StudentPacketCourseId);

						GoodService dbEntity = new GoodService();

						dbEntity.GoodServiceId = model.Id;
						dbEntity.StudentPacketCourseId = model.StudentPacketCourseId != 0 ? (Int32?) model.StudentPacketCourseId : null;
						dbEntity.FPCSCourseId = model.CourseId != 0 ? (Int32?) model.CourseId : null;

						dbEntity.TypeService = model.TypeService;
						dbEntity.Description = model.Description.TrimAndReduce();
						dbEntity.BeginDate = model.BeginDate;
						dbEntity.EndDate = model.EndDate;
						dbEntity.VendorUnitType = model.VendorUnitType;
						dbEntity.AdminComments = model.AdminComments.TrimAndReduce();
						dbEntity.ApprovalGivenBy = model.ApprovalGivenBy;
						dbEntity.NumberOfUnits = model.NumberOfUnits;
						dbEntity.UnitPrice = model.UnitPrice;

						dbEntity.GoodOrService = model.GoodOrService;
						dbEntity.RequisitionOrReimbursement = model.RequisitionOrReimbursement;
						dbEntity.TypeRequisitionReimbursement = model.TypeRequisitionReimbursement;
						dbEntity.VendorId = model.VendorId;

						dbEntity.IsDeleted = false;
						dbEntity.CreatedDate = DateTime.Now;
						dbEntity.UpdatedDate = DateTime.Now;

						SetClosedAndApproved(uow, dbEntity, model.Id, model.IsClosed, model.Aprv);

						if (model.Id == 0)
						{
							dbEntity.GoodOrServiceApproved = GoodOrServiceApproved.b_pend;
							dbEntity = repo.Add(dbEntity);
						}
						else
						{
							repo.Update(dbEntity);
						}

						uow.Commit();

						return JsonRes(dbEntity.GoodServiceId.ToString());
					}
				}
				catch (Exception ex)
				{
					return JsonRes(Status.Error, ex.Message);
				}
			}

			model.Init();
			return PartialView(model);
		}

		#endregion partial view goods and services

		private TableGoodAndServiceModel GetTableGoodAndService(Int32? studentPacketCourseId, Int32? courseId,
			Int64? studentPacketId, Guid? tempId, Int32? typeCall)
		{
			TableGoodAndServiceModel model = new TableGoodAndServiceModel();

			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				var repo = uow.GetRepo<IGoodServiceRepo>();

				Int32 _studentPacketCourseId = studentPacketCourseId.GetValueOrDefault(0);
				Int32 _courseId = courseId.GetValueOrDefault(0);
				Int64 _studentPacketId = studentPacketId.GetValueOrDefault(0);
				Guid _tempId = tempId.GetValueOrDefault(Guid.Empty);
                Int32 _typeCall = typeCall.GetValueOrDefault(0);

				if (_tempId != Guid.Empty)
				{
					_studentPacketCourseId = 0;
					_courseId = 0;
					_studentPacketId = 0;
                    _typeCall = 0;
				}

				var balances = uow.GetRepo<IGoodServiceBalanceRepo>()
					.GetByStudentPacketCourseOrFPCSCourseOrStudentPacket(_studentPacketCourseId, _courseId, _studentPacketId, _tempId)
					.ToList();

				var temp =
					repo.GetByStudentPacketCourseOrFPCSCourseOrStudentPacket(_studentPacketCourseId, _courseId, _studentPacketId,
						_tempId)
						.ToList();

				model.GoodsAndServices =
					repo.GetByStudentPacketCourseOrFPCSCourseOrStudentPacket(_studentPacketCourseId, _courseId, _studentPacketId, _tempId)
						.ToList()
						.Select(x => new GoodAndServiceModel(x, balances))
						.ToList();

				StudentPacketCourse spc = uow.GetRepo<IStudentPacketCourseRepo>().Get(_studentPacketCourseId);

				if (spc != null)
				{
					model.GoodsAndServices.AddRange(balances.Select(x => new GoodAndServiceModel(x)).ToList());
					model.GoodsAndServices.Add(new GoodAndServiceModel(GoodAndServiceHelper.GetInstructorFPCSCourse(spc)));
					model.GoodsAndServices = model.GoodsAndServices
						.Where(x => x.IsShow)
						.OrderByDescending(x => x.IsInstructor)
						.ThenBy(x => x.GoodAndServiceID)
						.ThenBy(x => x.GoodAndServiceBalanceID)
						.ToList();
				}
				else if (_studentPacketId != 0)
				{
					model.GoodsAndServices.AddRange(balances.Select(x => new GoodAndServiceModel(x)).ToList());
					model.GoodsAndServices = model.GoodsAndServices
						.Where(x => x.IsShow)
						.OrderByDescending(x => x.IsInstructor)
						.ThenBy(x => x.GoodAndServiceID)
						.ThenBy(x => x.GoodAndServiceBalanceID)
						.ToList();
				}
			}

			return model;
		}

		private void IsCheckLock(Int64? studentPacketCourseId)
		{
			if (!studentPacketCourseId.HasValue || (studentPacketCourseId.HasValue && studentPacketCourseId.Value == 0))
			{
				return;
			}

			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				// if it is edit student packet course then check IsLocked
				if (studentPacketCourseId.HasValue && studentPacketCourseId.Value != 0)
				{
					var studentPacketCourse = uow.GetRepo<IStudentPacketCourseRepo>().Get(studentPacketCourseId);

					if (studentPacketCourse == null)
					{
						throw new Exception("Student packet course does not exist");
					}

					AccessActionModel access = AccessAction.IsNotLock(studentPacketCourse.StudentPacket, User.Role);
					if (!access.IsResolve)
					{
						throw new Exception(access.Message);
					}
				}
			}
		}

		private void SetClosedAndApproved(IUnitOfWork uow, GoodService dbEntity, Int64 id, Boolean isClosed,
			GoodOrServiceApproved? aprv)
		{
			if (!aprv.HasValue)
			{
				aprv = GoodOrServiceApproved.b_pend;
			}

			if ((isClosed || aprv == GoodOrServiceApproved.b_rejc) && id != 0)
			{
				var balances = uow.GetRepo<IGoodServiceBalanceRepo>().GetByGoodService(dbEntity.GoodServiceId);

				foreach (var item in balances)
				{
					uow.GetRepo<IGoodServiceBalanceRepo>().Remove(item.GoodServiceBalanceId);
				}

				uow.Commit();

				dbEntity.IsClosed = true;
				dbEntity.GoodOrServiceApproved = GoodOrServiceApproved.b_rejc;
			}
			else
			{
				dbEntity.IsClosed = (Boolean?) isClosed;
				dbEntity.GoodOrServiceApproved = (GoodOrServiceApproved?) aprv;
			}
		}
	}
}