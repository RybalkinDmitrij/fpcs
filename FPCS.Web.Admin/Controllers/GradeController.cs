﻿using FPCS.Core.jqGrid;
using FPCS.Data;
using FPCS.Data.Entities;
using FPCS.Data.Enums;
using FPCS.Data.Exceptions;
using FPCS.Data.Repo;
using FPCS.Web.Admin.Code;
using FPCS.Web.Admin.Models.Instructor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FPCS.Core.Extensions;
using FPCS.Web.Admin.Models.Grades;
using System.IO;
using FPCS.Web.Admin.Code.ReadWriteCsv;
using System.Text;
using FPCS.Data.Helpers;

namespace FPCS.Web.Admin.Controllers
{
    public class GradeController : BaseController
    {
        [FPCSAuthorize(Role.Admin, Role.Teacher, Role.Guardian)]
        public ActionResult Index()
        {
            return View();
        }

        [FPCSAuthorize(Role.Admin, Role.Teacher, Role.Guardian)]
        public JsonResult _Index(GridOptions options, GradeListOptions gradeListOptions)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var dbList = GetData(uow);

                var engine = new GridDynamicEngine(options, gradeListOptions);
                var result = engine.ApplyAll2(dbList, x => new GradeMiniModel
                {
                    StudentPacketCourseId = x.StudentPacketCourseId,
                    StudentName = String.Format(@"{0}, {1}", x.StudentLastName, x.StudentFirstName),
                    ASDCode = x.ASDCode,
                    CourseTitle = x.CourseTitle,
                    ClassType = x.ClassType.HasValue ? x.ClassType.Value.GetDescription2Additional() : String.Empty,
                    TeacherId = x.TeacherId,
                    TeacherName = !String.IsNullOrEmpty(x.TeacherLastName) ? 
                                    String.Format(@"{0}, {1}", x.TeacherLastName, x.TeacherFirstName) :
                                    String.Empty,
                    GuardianId = x.GuardianId,
                    GuardianName = !String.IsNullOrEmpty(x.GuardianLastName) ?
                                    String.Format(@"{0}, {1}", x.GuardianLastName, x.GuardianFirstName) :
                                    String.Empty,
                    Q11 = x.Q11,
                    Q12 = x.Q12,
                    Q13 = x.Q13,
                    Q1 = x.Q1,
                    Q21 = x.Q21,
                    Q22 = x.Q22,
                    Q23 = x.Q23,
                    Q2 = x.Q2,
                    Sem1 = x.Sem1,
                    Q31 = x.Q31,
                    Q32 = x.Q32,
                    Q33 = x.Q33,
                    Q3 = x.Q3,
                    Q41 = x.Q41,
                    Q42 = x.Q42,
                    Q43 = x.Q43,
                    Q4 = x.Q4,
                    Sem2 = x.Sem2,
                    Credits = x.Credit,
                    Comment = x.Comment,
                    IsInSystem = x.IsInSystem,
                    IsLock = x.IsLock
                });

                return Json(result);
            }
        }

        [HttpGet]
        [FPCSAuthorize(Role.Admin, Role.Teacher, Role.Guardian)]
        public ActionResult _Details(Int64 id)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IStudentPacketCourseRepo>();
                var dbEntity = repo.Get(id);
                if (dbEntity == null) return JsonRes(Status.Error, "Student Packet Course not found");

                var model = new DetailGradeModel(dbEntity);

                return PartialView(model);
            }
        }

        [HttpGet]
        [FPCSAuthorize(Role.Admin, Role.Teacher, Role.Guardian)]
        public ActionResult _Edit(Int64 id)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IStudentPacketCourseRepo>();

                var dbEntity = repo.Get(id);
                if (dbEntity == null)
                {
                    return ErrorPartial("Student Packet Course {0} not found", id);
                }

                if (Session.GetUser().Role == Role.Teacher && dbEntity.IsLock)
                {
                    return ErrorPartial("Student Packet Course is locked!");
                }

                var model = new EditGradeModel(dbEntity);

                return PartialView(model);
            }
        }

        [HttpPost]
        [FPCSAuthorize(Role.Admin, Role.Teacher, Role.Guardian)]
        [ValidateAntiForgeryToken]
        public ActionResult _Edit(EditGradeModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var uow = UnityManager.Resolve<IUnitOfWork>())
                    {
                        var repo = uow.GetRepo<IStudentPacketCourseRepo>();

                        StudentPacketCourse dbStudentPacketCourse = repo.Get(model.StudentPacketCourseId);
                        if (dbStudentPacketCourse == null) { throw new NotFoundEntityException("Student Packet Course not found"); }

                        if (Session.GetUser().Role == Role.Teacher && dbStudentPacketCourse.IsLock)
                        {
                            throw new NotFoundEntityException("Student Packet Course is locked!");
                        }

                        dbStudentPacketCourse.Q11 = model.Q11;
                        dbStudentPacketCourse.Q12 = model.Q12;
                        dbStudentPacketCourse.Q13 = model.Q13;
                        dbStudentPacketCourse.Q1 = model.Q1;
                        dbStudentPacketCourse.Q21 = model.Q21;
                        dbStudentPacketCourse.Q22 = model.Q22;
                        dbStudentPacketCourse.Q23 = model.Q23;
                        dbStudentPacketCourse.Q2 = model.Q2;
                        dbStudentPacketCourse.Sem1 = model.Sem1;
                        dbStudentPacketCourse.Q31 = model.Q31;
                        dbStudentPacketCourse.Q32 = model.Q32;
                        dbStudentPacketCourse.Q33 = model.Q33;
                        dbStudentPacketCourse.Q3 = model.Q3;
                        dbStudentPacketCourse.Q41 = model.Q41;
                        dbStudentPacketCourse.Q42 = model.Q42;
                        dbStudentPacketCourse.Q43 = model.Q43;
                        dbStudentPacketCourse.Q4 = model.Q4;
                        dbStudentPacketCourse.Sem2 = model.Sem2;
                        dbStudentPacketCourse.Credit = model.Credits == GradeCredits.n000
                            ? 0m
                            : model.Credits == GradeCredits.n025
                                ? 0.25m
                                : model.Credits == GradeCredits.n050
                                    ? 0.50m
                                    : model.Credits == GradeCredits.n100 ? 1.00m : 0m;
                        dbStudentPacketCourse.Comment = model.Comment.TrimAndReduce();
                        dbStudentPacketCourse.IsInSystem = model.IsInSystem;
                        dbStudentPacketCourse.IsLock = model.IsLock;

                        repo.Update(dbStudentPacketCourse);

                        uow.Commit();

                        return JsonRes(dbStudentPacketCourse.StudentPacketCourseId.ToString());
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }
            }

            model.Init();
            return PartialView(model);
        }

        [FPCSAuthorize(Role.Admin, Role.Teacher, Role.Guardian)]
        public JsonResult _CSV(GridOptions options, GradeListOptions gradeListOptions)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var dbList = GetData(uow);

                var engine = new GridDynamicEngine(options, gradeListOptions);
                dbList = engine.ApplyFilter(dbList);
                dbList = engine.ApplySort(dbList);
                
                var result = dbList
                    .ToList()
                    .Select(x => new GradeMiniModel
                    {
                        StudentPacketCourseId = x.StudentPacketCourseId,
                        StudentName = String.Format(@"{0}, {1}", x.StudentLastName, x.StudentFirstName),
                        ASDCode = x.ASDCode,
                        CourseTitle = x.CourseTitle,
                        TeacherId = x.TeacherId,
                        TeacherName = !String.IsNullOrEmpty(x.TeacherLastName) ?
                                        String.Format(@"{0}, {1}", x.TeacherLastName, x.TeacherFirstName) :
                                        String.Empty,
                        GuardianId = x.GuardianId,
                        GuardianName = !String.IsNullOrEmpty(x.GuardianLastName) ?
                                        String.Format(@"{0}, {1}", x.GuardianLastName, x.GuardianFirstName) :
                                        String.Empty,
                        Q1 = x.Q1,
                        Q2 = x.Q2,
                        Sem1 = x.Sem1,
                        Q3 = x.Q3,
                        Q4 = x.Q4,
                        Sem2 = x.Sem2,
                        Credits = x.Credit,
                        Comment = x.Comment,
                        IsInSystem = x.IsInSystem,
                        IsLock = x.IsLock
                    })
                    .ToList();

                var csv = new StringBuilder();

                CsvFileWriter writer = new CsvFileWriter();

                CsvRow rowTitle = new CsvRow();
                rowTitle.Add("Student Name");
                rowTitle.Add("ASD Code");
                rowTitle.Add("Course Title");
                rowTitle.Add("Teacher Name");
                rowTitle.Add("Guardian Name");
                rowTitle.Add("Q1");
                rowTitle.Add("Q2");
                rowTitle.Add("Sem1");
                rowTitle.Add("Q3");
                rowTitle.Add("Q4");
                rowTitle.Add("Sem2");
                rowTitle.Add("Credits");
                rowTitle.Add("Comment");
                rowTitle.Add("In Zangle");
                rowTitle.Add("Lock");

                csv.AppendLine(writer.WriteRow(rowTitle));

                foreach (var item in result)
                {
                    CsvRow row = new CsvRow();

                    row.Add(item.StudentName);
                    row.Add(item.ASDCode);
                    row.Add(item.CourseTitle);
                    row.Add(item.TeacherName);
                    row.Add(item.GuardianName);
                    row.Add(item.Q1);
                    row.Add(item.Q2);
                    row.Add(item.Sem1);
                    row.Add(item.Q3);
                    row.Add(item.Q4);
                    row.Add(item.Sem2);
                    row.Add(item.Credits.ToString());
                    row.Add(item.Comment);
                    row.Add(item.IsInSystem ? "Yes" : "No");
                    row.Add(item.IsLock ? "Yes" : "No");

                    csv.AppendLine(writer.WriteRow(row));
                }

                String guid = Guid.NewGuid().ToString();
                Session[guid] = csv.ToString();

                return Json(guid);
            }
        }

        [FPCSAuthorize(Role.Admin, Role.Teacher, Role.Guardian)]
        public ActionResult Download(String guid)
        {
            var str = (String)Session[guid];

            Byte[] bytes = new Byte[str.Length * sizeof(Char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);

            return File(bytes, "application/CSV", "Grade.csv");
        }

        public ActionResult DownloadFile(Int64 id, String q)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IStudentPacketCourseRepo>();

                var dbEntity = repo.Get(id);
                if (dbEntity == null)
                {
                    return ErrorPartial("Student Packet Course {0} not found", id);
                }

                if (Session.GetUser().Role == Role.Teacher && dbEntity.IsLock)
                {
                    return ErrorPartial("Student Packet Course is locked!");
                }

                var fileName = String.Empty;
                if (q == "Q11") fileName = dbEntity.Q11GuidFile;
                else if (q == "Q12") fileName = dbEntity.Q12GuidFile;
                else if (q == "Q13") fileName = dbEntity.Q13GuidFile;
                else if (q == "Q21") fileName = dbEntity.Q21GuidFile;
                else if (q == "Q22") fileName = dbEntity.Q22GuidFile;
                else if (q == "Q23") fileName = dbEntity.Q23GuidFile;
                else if (q == "Q31") fileName = dbEntity.Q31GuidFile;
                else if (q == "Q32") fileName = dbEntity.Q32GuidFile;
                else if (q == "Q33") fileName = dbEntity.Q33GuidFile;
                else if (q == "Q41") fileName = dbEntity.Q41GuidFile;
                else if (q == "Q42") fileName = dbEntity.Q42GuidFile;
                else if (q == "Q43") fileName = dbEntity.Q43GuidFile;

                if (String.IsNullOrEmpty(fileName))
                {
                    return ErrorPartial("File not exist");
                }

                Byte[] arrayFile;
                using (FileStream fs = new FileStream(Path.Combine(Server.MapPath("~/FileStore/Grade/"), fileName), FileMode.Open))
                {
                    Byte[] buffer = new Byte[16 * 1024];
                    using (MemoryStream ms = new MemoryStream())
                    {
                        Int32 read;
                        while ((read = fs.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            ms.Write(buffer, 0, read);
                        }

                        arrayFile = ms.ToArray();
                    }
                }

                String guid = Guid.NewGuid().ToString();
                Session[guid] = arrayFile;
                Session[guid + "_fileName"] = "G" + dbEntity.StudentPacketCourseId.ToString() + Path.GetExtension(fileName);

                return Json(guid, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetDownloadFile(String guid)
        {
            String fileName = Session[guid + "_fileName"].ToString();
            String contentType = MimeTypeHelper.GetMIMEType(fileName);

            Byte[] data = (Byte[])Session[guid];
            return File(data, contentType, fileName);
        }

        public ActionResult RemoveFile(Int64 id, String q)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IStudentPacketCourseRepo>();

                var dbEntity = repo.Get(id);
                if (dbEntity == null)
                {
                    return ErrorPartial("Student Packet Course {0} not found", id);
                }

                if (Session.GetUser().Role == Role.Teacher && dbEntity.IsLock)
                {
                    return ErrorPartial("Student Packet Course is locked!");
                }

                var fileName = String.Empty;
                if (q == "Q11")
                {
                    fileName = dbEntity.Q11GuidFile;
                    dbEntity.Q11GuidFile = String.Empty;
                }
                else if (q == "Q12")
                {
                    fileName = dbEntity.Q12GuidFile;
                    dbEntity.Q12GuidFile = String.Empty;
                }
                else if (q == "Q13")
                {
                    fileName = dbEntity.Q13GuidFile;
                    dbEntity.Q13GuidFile = String.Empty;
                }
                else if (q == "Q21")
                {
                    fileName = dbEntity.Q21GuidFile;
                    dbEntity.Q21GuidFile = String.Empty;
                }
                else if (q == "Q22")
                {
                    fileName = dbEntity.Q22GuidFile;
                    dbEntity.Q22GuidFile = String.Empty;
                }
                else if (q == "Q23")
                {
                    fileName = dbEntity.Q23GuidFile;
                    dbEntity.Q23GuidFile = String.Empty;
                }
                else if (q == "Q31")
                {
                    fileName = dbEntity.Q31GuidFile;
                    dbEntity.Q31GuidFile = String.Empty;
                }
                else if (q == "Q32")
                {
                    fileName = dbEntity.Q32GuidFile;
                    dbEntity.Q32GuidFile = String.Empty;
                }
                else if (q == "Q33")
                {
                    fileName = dbEntity.Q33GuidFile;
                    dbEntity.Q33GuidFile = String.Empty;
                }
                else if (q == "Q41")
                {
                    fileName = dbEntity.Q41GuidFile;
                    dbEntity.Q41GuidFile = String.Empty;
                }
                else if (q == "Q42")
                {
                    fileName = dbEntity.Q42GuidFile;
                    dbEntity.Q42GuidFile = String.Empty;
                }
                else if (q == "Q43")
                {
                    fileName = dbEntity.Q43GuidFile;
                    dbEntity.Q43GuidFile = String.Empty;
                }

                if (String.IsNullOrEmpty(fileName))
                {
                    return ErrorPartial("File not exist");
                }

                System.IO.File.Delete(Path.Combine(Server.MapPath("~/FileStore/Grade/"), fileName));

                repo.Update(dbEntity);
                uow.Commit();

                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [FPCSAuthorize(Role.Admin, Role.Teacher, Role.Guardian)]
        public ActionResult UploadFile(Int64 id, String q)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IStudentPacketCourseRepo>();

                var dbEntity = repo.Get(id);
                if (dbEntity == null)
                {
                    return ErrorPartial("Student Packet Course {0} not found", id);
                }

                if (Session.GetUser().Role == Role.Teacher && dbEntity.IsLock)
                {
                    return ErrorPartial("Student Packet Course is locked!");
                }

                var model = new UploadFileGradeModel(dbEntity, q);

                return PartialView(model);
            }
        }

        [HttpPost]
        [FPCSAuthorize(Role.Admin, Role.Teacher, Role.Guardian)]
        public ActionResult UploadFile(UploadFileGradeModel model, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var uow = UnityManager.Resolve<IUnitOfWork>())
                    {
                        var repo = uow.GetRepo<IStudentPacketCourseRepo>();

                        StudentPacketCourse dbStudentPacketCourse = repo.Get(model.StudentPacketCourseId);
                        if (dbStudentPacketCourse == null) { throw new NotFoundEntityException("Student Packet Course not found"); }

                        if (Session.GetUser().Role == Role.Teacher && dbStudentPacketCourse.IsLock)
                        {
                            throw new NotFoundEntityException("Student Packet Course is locked!");
                        }

                        if (file == null)
                            throw new NotFoundEntityException("Attached file not found");

                        String newFileName = Guid.NewGuid().ToString() + Path.GetExtension(file.FileName);
                        String storeName = Server.MapPath("~/FileStore/Grade/" + newFileName);
                        file.SaveAs(storeName);

                        if (model.QNumber == "Q11") dbStudentPacketCourse.Q11GuidFile = storeName;
                        else if (model.QNumber == "Q12") dbStudentPacketCourse.Q12GuidFile = storeName;
                        else if (model.QNumber == "Q13") dbStudentPacketCourse.Q13GuidFile = storeName;
                        else if (model.QNumber == "Q21") dbStudentPacketCourse.Q21GuidFile = storeName;
                        else if (model.QNumber == "Q22") dbStudentPacketCourse.Q22GuidFile = storeName;
                        else if (model.QNumber == "Q23") dbStudentPacketCourse.Q23GuidFile = storeName;
                        else if (model.QNumber == "Q31") dbStudentPacketCourse.Q31GuidFile = storeName;
                        else if (model.QNumber == "Q32") dbStudentPacketCourse.Q32GuidFile = storeName;
                        else if (model.QNumber == "Q33") dbStudentPacketCourse.Q33GuidFile = storeName;
                        else if (model.QNumber == "Q41") dbStudentPacketCourse.Q41GuidFile = storeName;
                        else if (model.QNumber == "Q42") dbStudentPacketCourse.Q42GuidFile = storeName;
                        else if (model.QNumber == "Q43") dbStudentPacketCourse.Q43GuidFile = storeName;

                        repo.Update(dbStudentPacketCourse);

                        uow.Commit();

                        return JsonRes(dbStudentPacketCourse.StudentPacketCourseId.ToString());
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }
            }

            model.Init();
            return PartialView(model);
        }

        private IQueryable<GradePrevModel> GetData(IUnitOfWork uow)
        {
            var repo = uow.GetRepo<IStudentPacketCourseRepo>();
            var dbList = repo.GetByUser(Session.GetUser().UserId, Session.GetUser().Role, SchoolYear.SchoolYearId)
                .Select(x => new GradePrevModel
                {
                    StudentPacketCourseId = x.StudentPacketCourseId,
                    StudentLastName = x.StudentPacket.Student.LastName,
                    StudentFirstName = x.StudentPacket.Student.FirstName,
                    ASDCode = x.FPCSCourse.ASDCourse.ExternalASDCourseId,
                    CourseTitle = x.FPCSCourse.Name,
                    ClassType = x.FPCSCourse.ClassType,
                    TeacherId = x.FPCSCourse.TeacherId,
                    TeacherLastName = x.FPCSCourse.Teacher != null ?
                                        x.FPCSCourse.Teacher.LastName :
                                        String.Empty,
                    TeacherFirstName = x.FPCSCourse.Teacher != null ?
                                        x.FPCSCourse.Teacher.FirstName :
                                        String.Empty,
                    GuardianId = x.FPCSCourse.GuardianId,
                    GuardianLastName = x.FPCSCourse.Guardian != null ?
                                        x.FPCSCourse.Guardian.LastName :
                                        String.Empty,
                    GuardianFirstName = x.FPCSCourse.Guardian != null ?
                                        x.FPCSCourse.Guardian.FirstName :
                                        String.Empty,
                    Q11 = x.Q11,
                    Q12 = x.Q12,
                    Q13 = x.Q13,
                    Q1 = x.Q1,
                    Q21 = x.Q21,
                    Q22 = x.Q22,
                    Q23 = x.Q23,
                    Q2 = x.Q2,
                    Sem1 = x.Sem1,
                    Q31 = x.Q31,
                    Q32 = x.Q32,
                    Q33 = x.Q33,
                    Q3 = x.Q3,
                    Q41 = x.Q41,
                    Q42 = x.Q42,
                    Q43 = x.Q43,
                    Q4 = x.Q4,
                    Sem2 = x.Sem2,
                    Credit = x.Credit.HasValue ? x.Credit.Value : 0,
                    Comment = x.Comment,
                    IsInSystem = x.IsInSystem,
                    IsLock = x.IsLock
                });

            return dbList;
        }
    }
}
