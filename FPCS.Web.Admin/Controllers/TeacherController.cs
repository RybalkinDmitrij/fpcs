﻿using System;
using System.Web.Mvc;

using FPCS.Core.jqGrid;
using FPCS.Core.Extensions;

using FPCS.Data;
using FPCS.Data.Repo;
using FPCS.Data.Enums;
using FPCS.Web.Admin.Code;
using FPCS.Web.Admin.Models.Teacher;
using FPCS.Data.Entities;
using FPCS.Data.Exceptions;

namespace FPCS.Web.Admin.Controllers
{
    public class TeacherController : BaseController
    {
        [FPCSAuthorize(Role.Admin, Role.Teacher, Role.Guardian)]
        public ActionResult Index()
        {
            return View();
        }

        [FPCSAuthorize(Role.Admin, Role.Teacher, Role.Guardian)]
        public JsonResult _Index(GridOptions options, TeachersListOptions teachersListOptions)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<ITeacherRepo>();
                var dbList = repo.GetAll(Session.GetSchoolYear().SchoolYearId);

                var engine = new GridDynamicEngine(options, teachersListOptions);
                var result = engine.ApplyAll(dbList, x => new TeacherMiniModel
                {
                    DbUserId = x.DbUserId,
                    FullName = x.FullName,
                    Email = x.Email,
                    IsActive = x.IsActive
                });

                return Json(result);
            }
        }


        [HttpGet]
        [FPCSAuthorize(Role.Admin, Role.Teacher)]
        public ActionResult Details(Guid id)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<ITeacherRepo>();
                var dbEntity = repo.Get(id);
                if (dbEntity == null) return JsonRes(Status.Error, "Teacher not found");

                var model = new TeacherDetailsModel(dbEntity);

                //return View(model);
                return PartialView("_Details", model);
            }
        }


        [HttpGet]
        [FPCSAuthorize(Role.Admin)]
        public ActionResult Create()
        {
            var model = new TeacherCreateModel();
            //return View(model);
            return PartialView("_Create", model);
        }

        [HttpPost]
        [FPCSAuthorize(Role.Admin)]
        [ValidateAntiForgeryToken]
        public ActionResult Create(TeacherCreateModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var uow = UnityManager.Resolve<IUnitOfWork>())
                    {
                        var repo = uow.GetRepo<ITeacherRepo>();

                        DbUser user = uow.GetRepo<IDbUserRepo>().GetByEmail(model.Email, SchoolYear.SchoolYearId);
                        if (user != null)
                        {
                            throw new NotFoundEntityException("This email is already registered");
                        }

                        Teacher dbTeacher = new Teacher()
                        {
                            SchoolYearId = Session.GetSchoolYear().SchoolYearId,
                            Email = model.Email.TrimAndReduce(),
                            FirstName = model.FirstName.TrimAndReduce(),
                            LastName = model.LastName.TrimAndReduce(),
                            MiddleInitial = model.MiddleInitial.TrimAndReduce(),
                            FullName = String.Format(@"{0} {1}", model.LastName.TrimAndReduce(), model.FirstName.TrimAndReduce()),
                            Title = model.TitlePerson,
                            DateBirth = model.DateBirth,
                            IsActive = model.IsActive,
                            MailingAddress = model.MailingAddress.TrimAndReduce(),
                            City = model.City.TrimAndReduce(),
                            StateId = model.StateId,
                            ZipCode = model.ZipCode.TrimAndReduce(),
                            HomePhone = model.HomePhone.TrimAndReduce(),
                            BusPhone = model.BusPhone.TrimAndReduce(),
                            Ext = model.Ext,
                            CellPhone = model.CellPhone.TrimAndReduce(),
                            MastersDegree = model.MastersDegree,
                            SSN = model.SSN.TrimAndReduce(),
                            SecondEMailAddress = model.SecondEMailAddress.TrimAndReduce(),
                            DistrictCode = model.DistrictCode.TrimAndReduce(),
                            IsBenefitPaidASDSchool = model.IsBenefitPaidASDSchool,
                            ASDFTE = model.ASDFTE,
                            IsNotIncludeBenefits = model.IsNotIncludeBenefits,
                            FTCSFTE = model.FTCSFTE,
                            NameSchool = model.NameSchool.TrimAndReduce(),
                            IsLeaveASD = model.IsLeaveASD,
                            IsSubtitleTeacher = model.IsSubtitleTeacher,
                            IsOtherASDEmployee = model.IsOtherASDEmployee,
                            IsOnASDEligibleToHire = model.IsOnASDEligibleToHire,
                            IsRetiredASDTeacher = model.IsRetiredASDTeacher,
                            FPCSFTE = model.FPCSFTE,
                            IsGroupInstruction = model.IsGroupInstruction,
                            IsIndividualInstruction = model.IsIndividualInstruction,
                            YearsTeachingExperience = model.YearsTeachingExperience,
                            TeachingCertificateExpirationDate = model.TeachingCertificateExpirationDate,
                            TeachingSalaryPlacement = model.TeachingSalaryPlacement.TrimAndReduce(),
                            IsAlaskaCertificationK8 = model.IsAlaskaCertificationK8,
                            IsAlaskaCertificationK12 = model.IsAlaskaCertificationK12,
                            IsAlaskaCertificationSpecialEducation = model.IsAlaskaCertificationSpecialEducation,
                            IsAlaskaCertificationSecondary = model.IsAlaskaCertificationSecondary,
                            AlaskaCertificationSubjectGrades = model.AlaskaCertificationSubjectGrades,
                            IsInMyClassroom = model.IsInMyClassroom,
                            IsAtMyHome = model.IsAtMyHome,
                            IsAsStudentsHome = model.IsAsStudentsHome,
                            IsAtFPCSClassroom = model.IsAtFPCSClassroom,
                            IsOtherAvailableTeach = model.IsOtherAvailableTeach,
                            OtherAvailableTeach = model.OtherAvailableTeach.TrimAndReduce(),
                            IsWeekdays = model.IsWeekdays,
                            IsWeekdayAfternoons = model.IsWeekdayAfternoons,
                            IsWeekdayEvenings = model.IsWeekdayEvenings,
                            IsWeekends = model.IsWeekends,
                            IsSummers = model.IsSummers,
                            //FlatRateHour = model.FlatRateHour,
                            //BasePayHour = model.BasePayHour,
                            //PayHourwBenefits = model.PayHourwBenefits,
                            PayType = model.PayType,
                            PerDeimRate = model.PerDeimRate,
                            IsGuardian = false
                        };

                        dbTeacher = repo.Add(dbTeacher);
                        uow.Commit();

                        dbTeacher.UnqDbUserId = dbTeacher.DbUserId;

                        uow.Commit();

                        return JsonRes(new { id = dbTeacher.DbUserId.ToString(), text = String.Format("{0}, {1}", dbTeacher.LastName, dbTeacher.FirstName) });
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }
            }

            model.Init();
            //return View(model);
            return PartialView("_Create", model);
        }


        [HttpGet]
        [FPCSAuthorize(Role.Admin)]
        public ActionResult Edit(Guid id)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<ITeacherRepo>();
                var dbEntity = repo.Get(id);
                if (dbEntity == null)
                {
                    return ErrorPartial("Teacher {0} not found", id);
                }

                var model = new TeacherEditModel(dbEntity);
                
                //return View(model);
                return PartialView("_Edit", model);
            }
        }

        [HttpPost]
        [FPCSAuthorize(Role.Admin)]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(TeacherEditModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var uow = UnityManager.Resolve<IUnitOfWork>())
                    {
                        var repo = uow.GetRepo<ITeacherRepo>();

                        Teacher dbTeacher = repo.Get(model.DbUserId);
                        if (dbTeacher == null) { throw new NotFoundEntityException("Teacher not found"); }

                        DbUser user = uow.GetRepo<IDbUserRepo>().GetByEmail(model.Email, SchoolYear.SchoolYearId);
                        if (user != null && user.DbUserId != dbTeacher.DbUserId)
                        {
                            throw new NotFoundEntityException("This email is already registered");
                        }

                        dbTeacher.Email = model.Email.TrimAndReduce();
                        dbTeacher.FirstName = model.FirstName.TrimAndReduce();
                        dbTeacher.LastName = model.LastName.TrimAndReduce();
                        dbTeacher.MiddleInitial = model.MiddleInitial.TrimAndReduce();
                        dbTeacher.FullName = String.Format(@"{0} {1}", model.LastName.TrimAndReduce(), model.FirstName.TrimAndReduce());

                        dbTeacher.SchoolYearId = Session.GetSchoolYear().SchoolYearId;
                        dbTeacher.Title = model.TitlePerson;
                        dbTeacher.DateBirth = model.DateBirth;
                        dbTeacher.IsActive = model.IsActive;
                        dbTeacher.MailingAddress = model.MailingAddress.TrimAndReduce();
                        dbTeacher.City = model.City.TrimAndReduce();
                        dbTeacher.StateId = model.StateId;
                        dbTeacher.ZipCode = model.ZipCode.TrimAndReduce();
                        dbTeacher.HomePhone = model.HomePhone.TrimAndReduce();
                        dbTeacher.BusPhone = model.BusPhone.TrimAndReduce();
                        dbTeacher.Ext = model.Ext;
                        dbTeacher.CellPhone = model.CellPhone.TrimAndReduce();
                        dbTeacher.MastersDegree = model.MastersDegree;
                        dbTeacher.SSN = model.SSN.TrimAndReduce();
                        dbTeacher.SecondEMailAddress = model.SecondEMailAddress.TrimAndReduce();
                        dbTeacher.DistrictCode = model.DistrictCode.TrimAndReduce();
                        dbTeacher.IsBenefitPaidASDSchool = model.IsBenefitPaidASDSchool;
                        dbTeacher.ASDFTE = model.ASDFTE;
                        dbTeacher.IsNotIncludeBenefits = model.IsNotIncludeBenefits;
                        dbTeacher.FTCSFTE = model.FTCSFTE;
                        dbTeacher.NameSchool = model.NameSchool.TrimAndReduce();
                        dbTeacher.IsLeaveASD = model.IsLeaveASD;
                        dbTeacher.IsSubtitleTeacher = model.IsSubtitleTeacher;
                        dbTeacher.IsOtherASDEmployee = model.IsOtherASDEmployee;
                        dbTeacher.IsOnASDEligibleToHire = model.IsOnASDEligibleToHire;
                        dbTeacher.IsRetiredASDTeacher = model.IsRetiredASDTeacher;
                        dbTeacher.FPCSFTE = model.FPCSFTE;
                        dbTeacher.IsGroupInstruction = model.IsGroupInstruction;
                        dbTeacher.IsIndividualInstruction = model.IsIndividualInstruction;
                        dbTeacher.YearsTeachingExperience = model.YearsTeachingExperience;
                        dbTeacher.TeachingCertificateExpirationDate = model.TeachingCertificateExpirationDate;
                        dbTeacher.TeachingSalaryPlacement = model.TeachingSalaryPlacement.TrimAndReduce();
                        dbTeacher.IsAlaskaCertificationK8 = model.IsAlaskaCertificationK8;
                        dbTeacher.IsAlaskaCertificationK12 = model.IsAlaskaCertificationK12;
                        dbTeacher.IsAlaskaCertificationSpecialEducation = model.IsAlaskaCertificationSpecialEducation;
                        dbTeacher.IsAlaskaCertificationSecondary = model.IsAlaskaCertificationSecondary;
                        dbTeacher.AlaskaCertificationSubjectGrades = model.AlaskaCertificationSubjectGrades;
                        dbTeacher.IsInMyClassroom = model.IsInMyClassroom;
                        dbTeacher.IsAtMyHome = model.IsAtMyHome;
                        dbTeacher.IsAsStudentsHome = model.IsAsStudentsHome;
                        dbTeacher.IsAtFPCSClassroom = model.IsAtFPCSClassroom;
                        dbTeacher.IsOtherAvailableTeach = model.IsOtherAvailableTeach;
                        dbTeacher.OtherAvailableTeach = model.OtherAvailableTeach.TrimAndReduce();
                        dbTeacher.IsWeekdays = model.IsWeekdays;
                        dbTeacher.IsWeekdayAfternoons = model.IsWeekdayAfternoons;
                        dbTeacher.IsWeekdayEvenings = model.IsWeekdayEvenings;
                        dbTeacher.IsWeekends = model.IsWeekends;
                        dbTeacher.IsSummers = model.IsSummers;
                        //dbTeacher.FlatRateHour = model.FlatRateHour;
                        //dbTeacher.BasePayHour = model.BasePayHour;
                        //dbTeacher.PayHourwBenefits = model.PayHourwBenefits;
                        dbTeacher.PayType = model.PayType;
                        dbTeacher.PerDeimRate = model.PerDeimRate;
                        dbTeacher.IsGuardian = false;

                        dbTeacher = repo.Update(dbTeacher);

                        uow.Commit();

                        //return RedirectToAction("Index");
                        return JsonRes(dbTeacher.DbUserId.ToString());
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }
            }

            model.Init();
            //return View(model);
            return PartialView("_Edit", model);
        }
        
        [HttpPost]
        [FPCSAuthorize(Role.Admin)]
        public ActionResult Delete(Guid id)
        {
            try
            {
                using (var uow = UnityManager.Resolve<IUnitOfWork>())
                {
                    var repo = uow.GetRepo<ITeacherRepo>();
                    repo.Remove(id);

                    uow.Commit();
                    return JsonRes();
                }
            }
            catch (Exception ex)
            {
                return JsonRes(Status.Error, ex.Message);
            }
        }

        [HttpPost]
        [FPCSAuthorize(Role.Admin)]
        public ActionResult DeleteAll(String id)
        {
            try
            {
                if (String.IsNullOrEmpty(id)) return JsonRes(Status.Error, "Not correct ids");

                var ids = id.Split(',');
                using (var uow = UnityManager.Resolve<IUnitOfWork>())
                {
                    Int32 errorsCount = 0;
                    var repo = uow.GetRepo<ITeacherRepo>();
                    foreach (var item in ids)
                    {
                        try
                        {
                            repo.Remove(new Guid(item));
                        }
                        catch (Exception)
                        {
                            errorsCount++;
                        }
                    }

                    uow.Commit();
                    if (errorsCount == 0) return JsonRes();
                    else return JsonRes(Status.Error, "Were deleted not all entities");
                }
            }
            catch (Exception ex)
            {
                return JsonRes(Status.Error, ex.Message);
            }
        }
    }
}
