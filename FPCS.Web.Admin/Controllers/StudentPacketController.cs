﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Collections.Generic;

using FPCS.Data;
using FPCS.Data.Enums;
using FPCS.Data.Repo;
using FPCS.Web.Admin.Code;
using FPCS.Web.Admin.Models.StudentPacket;
using FPCS.Core.jqGrid;
using FPCS.Data.Entities;
using FPCS.Core.Extensions;
using FPCS.Web.Admin.Helpers.GoodAndService;
using FPCS.Web.Admin.Models.GoodAndService;
using FPCS.Web.Admin.Helpers.BudgetStudent;
using FPCS.Web.Admin.Helpers.BudgetStudent.Models;
using FPCS.Web.Admin.Helpers.Access;
using FPCS.Web.Admin.Helpers.Access.Models;
using System.Configuration;

namespace FPCS.Web.Admin.Controllers
{
    [FPCSAuthorize(Role.Admin, Role.Teacher, Role.Guardian)]
    public class StudentPacketController : BaseController
    {
        public ActionResult Index(Guid id)
        {
            try
            {
                StudentPacketModel model = GetStudentPacket(id);

                return View(model);
            }
            catch (Exception ex)
            {
                return Error(ex.Message);
            }
        }

        public ActionResult _Print(Guid id)
        {
            try
            {
                PrintStudentPacketModel model = new PrintStudentPacketModel();

                model.StudentPacketModel = GetStudentPacket(id);

                model.PrintAddress = (ConfigurationManager.AppSettings["PrintAddress"]).ToString();
                model.PrintState = (ConfigurationManager.AppSettings["PrintState"]).ToString();
                model.PrintPhone = (ConfigurationManager.AppSettings["PrintPhone"]).ToString();
                model.PrintFax = (ConfigurationManager.AppSettings["PrintFax"]).ToString();
                model.PrintDateNow = DateTime.Now.ToString("MM/dd/yyyy");

                model.Title = "Student Packet : " + model.StudentPacketModel.StudentName + " - Grade: " + model.StudentPacketModel.StudentGradeStr;

                return PartialView(model);
            }
            catch (Exception ex)
            {
                return Error(ex.Message);
            }
        }

        public ActionResult _PrintReimbursement(Guid id)
        {
            try
            {
                PrintReimbursementModel model = new PrintReimbursementModel();

                using (var uow = UnityManager.Resolve<IUnitOfWork>())
                {
                    model.Reimbursements = uow.GetRepo<IGoodServiceRepo>()
                                              .GetReimbursements(id, SchoolYear.SchoolYearId)
                                              .Select(x => new ReimbursementModel
                                              {
                                                  GoodServiceId = x.GoodServiceId,
                                                  VendorName = x.Vendor.BusinessName,
                                                  Item = x.LineItemName,
                                                  Cons = x.Consumable.HasValue && x.Consumable.Value == Consumable.Yes ? "Y" : "N",
                                                  BudgetedAmount = x.UnitPrice * x.NumberOfUnits + x.ShippingHandlingFees,
                                                  RequestedAmount = 0,
                                                  Course = x.StudentPacketCourse.FPCSCourse.Name,
                                                  StudentName = x.StudentPacketCourse.StudentPacket.Student.LastName + ", " + x.StudentPacketCourse.StudentPacket.Student.FirstName,
                                                  ObjCD = x.GoodOrService == GoodOrService.Good && x.TypeRequisitionReimbursement == TypeRequisitionReimbursement.VendorService ? "3030" :
                                                            x.GoodOrService == GoodOrService.Service && x.TypeRequisitionReimbursement == TypeRequisitionReimbursement.TextbookCurriculum ? "4020" :
                                                            String.Empty,
                                                  IsWithdrawal = x.StudentPacketCourse.StudentPacket.Student.WithdrawalDate.HasValue ? true : false
                                              })
                                              .ToList();

                    model.PrintAddress = (ConfigurationManager.AppSettings["PrintAddress"]).ToString();
                    model.PrintState = (ConfigurationManager.AppSettings["PrintState"]).ToString();
                    model.PrintPhone = (ConfigurationManager.AppSettings["PrintPhone"]).ToString();
                    model.PrintFax = (ConfigurationManager.AppSettings["PrintFax"]).ToString();
                    model.PrintDateNow = DateTime.Now.ToString("MM/dd/yyyy");

                    model.Total = model.Reimbursements.Sum(x => x.BudgetedAmount);

                    List<Guardian> guardians = uow.GetRepo<IFamilyRepo>().GetGuardiansByStudent(id).ToList();

                    if (guardians != null && guardians.Count >= 2)
                    {
                        model.Title = String.Format(@"{0} : {1} & {2}", guardians.FirstOrDefault().LastName, guardians.FirstOrDefault().FirstName, guardians.LastOrDefault().FirstName); 
                    }
                    else if (guardians != null && guardians.Count == 1)
                    {
                        model.Title = String.Format(@"{0}, {1}", guardians.FirstOrDefault().LastName, guardians.FirstOrDefault().FirstName);
                    }
                    else
                    {
                        model.Title = String.Empty;
                    }
                }

                return PartialView(model);
            }
            catch (Exception ex)
            {
                return Error(ex.Message);
            }
        }

        [HttpGet]
        public PartialViewResult _SponsorTeacherEdit(Int64 id)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var query = uow.GetRepo<IStudentPacketRepo>().GetAsQuery(id);
                var model = query.Select(x => new SponsorTeacherModel
                {
                    StudentPacketId = x.StudentPacketId,
                    TeacherId = x.SponsorTeacherId,
                    Name = x.SponsorTeacher.FullName
                }).FirstOrDefault();

                if (model == null) return ErrorPartial("Teacher {0} not found", id);

                model.Init();
                return PartialView(model);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _SponsorTeacherEdit(SponsorTeacherModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var uow = UnityManager.Resolve<IUnitOfWork>())
                    {
                        AccessActionModel access =
                            AccessAction.IsNotLock(model.StudentPacketId, User.Role);

                        if (!access.IsResolve)
                        {
                            throw new Exception(access.Message);
                        }

                        var repo = uow.GetRepo<IStudentPacketRepo>();
                        var dbEntity = repo.UpdateSponsorTeacher(model.StudentPacketId, model.TeacherId.Value);
                        uow.Commit();

                        model.Name = dbEntity.SponsorTeacher.FullName;
                        return PartialView("_SponsorTeacher", model);
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }
            }

            model.Init();
            return PartialView(model);
        }


        [HttpPost]
        public JsonResult _FPCSCourses(Guid studentId, Guid? sponsorId, Int64 studentPacketId, GridOptions options, FPCSCoursesListOptions coursesListOptions)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                Int32? subject = coursesListOptions.Subject;
                coursesListOptions.Subject = null;

                var query = uow.GetRepo<IFPCSCourseRepo>().GetNotSelectedInPacketCourses(studentId, studentPacketId, SchoolYear.SchoolYearId)
                    .Where(x => (subject.HasValue && (Int32)x.ASDCourse.SubjectId == subject.Value) ||
                                             (!subject.HasValue));

                // if current user is teacher and isn't sponsor, then to show his courses
                if (sponsorId.HasValue &&
                    User.Role == Role.Teacher &&
                    User.UserId != sponsorId.Value)
                {
                    query = query.Where(x => x.TeacherId == User.UserId);
                }

                var engine = new GridDynamicEngine(options, coursesListOptions);
                var result = engine.ApplyAll(query, x => new FPCSCourseMiniModel
                {
                    FPCSCourseId = x.FPCSCourseId,
                    Name = x.Name,
                    Teacher = x.Teacher != null ? x.Teacher.FullName : String.Empty,
                    Guardian = x.Guardian != null ? x.Guardian.FullName : String.Empty,
                    Subject = x.ASDCourse.Subject.Name,
                    Semester = x.Semester
                });

                return Json(result);
            }
        }

        [HttpPost]
        public JsonResult AddFPCSStudentPacketCourse(Int64 fpcsCourseId, Int64 studentPacketId)
        {
            try
            {
                using (var uow = UnityManager.Resolve<IUnitOfWork>())
                {
                    AccessActionModel access = AccessAction.IsCanAddNewCourseByStudentPacket(studentPacketId, Session.GetUser().Role);

                    if (!access.IsResolve)
                    {
                        throw new Exception(access.Message);
                    }

                    var dbEntity = uow.GetRepo<IStudentPacketCourseRepo>().Add(studentPacketId, fpcsCourseId);
                    uow.Commit();

                    ILP dbGetILP = uow.GetRepo<IILPRepo>().GetByFPCSCourse(fpcsCourseId);

                    if (dbGetILP != null)
                    {
                        ILP saveILP = new ILP();
                        saveILP.ILPId = 0;
                        saveILP.ILPName = dbGetILP.ILPName;
                        saveILP.StudentPacketCourseId = dbEntity.StudentPacketCourseId;
                        saveILP.FPCSCourseId = null;
                        saveILP.CourseHrs = dbGetILP.CourseHrs;
                        saveILP.DescriptionCourse = dbGetILP.DescriptionCourse;
                        saveILP.Standards = dbGetILP.Standards;
                        saveILP.StudentActivities = dbGetILP.StudentActivities;
                        saveILP.MaterialsResources = dbGetILP.MaterialsResources;
                        saveILP.RoleAnyPeople = dbGetILP.RoleAnyPeople;
                        saveILP.EvaluationGradingPassFail = dbGetILP.EvaluationGradingPassFail;
                        saveILP.EvaluationGradingGradingScale = dbGetILP.EvaluationGradingGradingScale;
                        saveILP.EvaluationGradingOther = dbGetILP.EvaluationGradingOther;
                        saveILP.EvaluationGradingOtherExplain = dbGetILP.EvaluationGradingOtherExplain;
                        saveILP.EvaluatedMeasurableOutcomes = dbGetILP.EvaluatedMeasurableOutcomes;
                        saveILP.CourseSyllabus = dbGetILP.CourseSyllabus;
                        saveILP.GuardianILPModifications = dbGetILP.GuardianILPModifications;
                        saveILP.InstructorILPModifications = dbGetILP.InstructorILPModifications;

                        saveILP = uow.GetRepo<IILPRepo>().Add(saveILP);
                    }

                    List<GoodService> goodAndServices = uow.GetRepo<IGoodServiceRepo>()
                                                           .GetByFPCSCourse(fpcsCourseId)
                                                           .ToList();

                    foreach (GoodService item in goodAndServices)
                    {
                        GoodService dbSaveGoodService = new GoodService();
                        dbSaveGoodService.GoodServiceId = 0;
                        dbSaveGoodService.CreatedDate = DateTime.Now;
                        dbSaveGoodService.UpdatedDate = DateTime.Now;
                        dbSaveGoodService.StudentPacketCourseId = dbEntity.StudentPacketCourseId;
                        dbSaveGoodService.FPCSCourseId = null;
                        dbSaveGoodService.GoodOrService = item.GoodOrService;
                        dbSaveGoodService.RequisitionOrReimbursement = item.RequisitionOrReimbursement;
                        dbSaveGoodService.TypeRequisitionReimbursement = item.TypeRequisitionReimbursement;
                        dbSaveGoodService.VendorId = item.VendorId;
                        dbSaveGoodService.AdminComments = item.AdminComments;
                        dbSaveGoodService.ApprovalGivenBy = item.ApprovalGivenBy;
                        dbSaveGoodService.NumberOfUnits = item.NumberOfUnits;
                        dbSaveGoodService.UnitPrice = item.UnitPrice;
                        dbSaveGoodService.ShippingHandlingFees = item.ShippingHandlingFees;
                        dbSaveGoodService.LineItemName = item.LineItemName;
                        dbSaveGoodService.LineItemDesc = item.LineItemDesc;
                        dbSaveGoodService.SupplyType = item.SupplyType;
                        dbSaveGoodService.Count = item.Count;
                        dbSaveGoodService.CatalogMFG = item.CatalogMFG;
                        dbSaveGoodService.CatalogIssuePG = item.CatalogIssuePG;
                        dbSaveGoodService.UnitType = item.UnitType;
                        dbSaveGoodService.BarCode = item.BarCode;
                        dbSaveGoodService.Consumable = item.Consumable;
                        dbSaveGoodService.TypeTextbookCurriculum = item.TypeTextbookCurriculum;
                        dbSaveGoodService.Title = item.Title;
                        dbSaveGoodService.Author = item.Author;
                        dbSaveGoodService.PublisherISBN = item.PublisherISBN;
                        dbSaveGoodService.PublisherName = item.PublisherName;
                        dbSaveGoodService.Edition = item.Edition;
                        dbSaveGoodService.HighSchool = item.HighSchool;
                        dbSaveGoodService.NumberSemesters = item.NumberSemesters;
                        dbSaveGoodService.CostPerSemester = item.CostPerSemester;
                        dbSaveGoodService.ClassType = item.ClassType;
                        dbSaveGoodService.CourseTitle = item.CourseTitle;
                        dbSaveGoodService.CourseNumber = item.CourseNumber;
                        dbSaveGoodService.DateBirth = item.DateBirth;
                        dbSaveGoodService.Credits = item.Credits;
                        dbSaveGoodService.Semester = item.Semester;
                        dbSaveGoodService.TypeService = item.TypeService;
                        dbSaveGoodService.Description = item.Description;
                        dbSaveGoodService.BeginDate = item.BeginDate;
                        dbSaveGoodService.EndDate = item.EndDate;
                        dbSaveGoodService.VendorUnitType = item.VendorUnitType;

                        dbSaveGoodService = uow.GetRepo<IGoodServiceRepo>().Add(dbSaveGoodService);
                    }

                    uow.Commit();

                    return JsonRes();
                }
            }
            catch (Exception ex)
            {
                return JsonRes(Status.Error, ex.Message);
            }
        }

        [HttpPost]
        public JsonResult DeleteStudentPacketCourse(Int64 studentPacketCourseId)
        {
            try
            {
                using (var uow = UnityManager.Resolve<IUnitOfWork>())
                {
                    var repo = uow.GetRepo<IStudentPacketCourseRepo>();

                    AccessActionModel access = AccessAction.IsCanDeleteStudentPacketCourse(studentPacketCourseId, Session.GetUser().Role);
                    if (!access.IsResolve)
                    {
                        throw new Exception(access.Message);
                    }

                    repo.Remove(studentPacketCourseId);
                    uow.Commit();
                    return JsonRes();
                }
            }
            catch (Exception ex)
            {
                return JsonRes(Status.Error, ex.Message);
            }
        }

        [HttpPost]
        public ActionResult ChangeGoalEnrollment(Guid studentId, GoalEnrollmentPercent percentagePlanningEnroll)
        {
            try
            {
                using (var uow = UnityManager.Resolve<IUnitOfWork>())
                {
                    var repo = uow.GetRepo<IStudentRepo>();

                    AccessActionModel access =
                        AccessAction.IsNotLock(uow.GetRepo<IStudentPacketRepo>().Get(studentId, SchoolYear.SchoolYearId), User.Role);

                    if (!access.IsResolve)
                    {
                        throw new Exception(access.Message);
                    }

                    Student student = repo.Get(studentId);

                    if (studentId == null) { throw new Exception("Student not found!"); }

                    student.PercentagePlanningEnroll = percentagePlanningEnroll == GoalEnrollmentPercent._0Percent ? 0 :
                                                        percentagePlanningEnroll == GoalEnrollmentPercent._25Percent ? 25 :
                                                        percentagePlanningEnroll == GoalEnrollmentPercent._50Percent ? 50 :
                                                        percentagePlanningEnroll == GoalEnrollmentPercent._75Percent ? 75 :
                                                        percentagePlanningEnroll == GoalEnrollmentPercent._100Percent ? 100 : 0;

                    repo.Update(student);

                    uow.Commit();

                    return JsonRes(student.DbUserId.ToString());
                }
            }
            catch (Exception ex)
            {
                return JsonRes(Status.Error, ex.Message);
            }
        }

        public ActionResult Locked(Guid id)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var packetRepo = uow.GetRepo<IStudentPacketRepo>();

                var dbEntity = packetRepo.Get(id, SchoolYear.SchoolYearId);

                dbEntity.IsLocked = true;

                packetRepo.Update(dbEntity);

                uow.Commit();

                return RedirectToAction("Index", new { id = id });
            }
        }

        public ActionResult Unlocked(Guid id)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var packetRepo = uow.GetRepo<IStudentPacketRepo>();

                var dbEntity = packetRepo.Get(id, SchoolYear.SchoolYearId);

                dbEntity.IsLocked = false;

                packetRepo.Update(dbEntity);

                uow.Commit();

                return RedirectToAction("Index", new { id = id });
            }
        }

        public ActionResult _ASDTestingAgreement(Int64 id)
        {
            AgreementModel model = new AgreementModel();
            model.Init(id);

            return PartialView(model);
        }

        public ActionResult SignASDTestingAgreement(Guid studentId)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var packetRepo = uow.GetRepo<IStudentPacketRepo>();

                var dbEntity = packetRepo.Get(studentId, SchoolYear.SchoolYearId);

                dbEntity.IsASDTASigned = true;
                dbEntity.ASDTAGuardian = uow.GetRepo<IGuardianRepo>().Get(User.UserId);

                packetRepo.Update(dbEntity);

                uow.Commit();

                return JsonRes(Status.OK, "OK");
            }
        }

        public ActionResult _ProgressReportAgreement(Int64 id)
        {
            AgreementModel model = new AgreementModel();
            model.Init(id);

            return PartialView(model);
        }

        public ActionResult SignProgressReportAgreement(Guid studentId)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var packetRepo = uow.GetRepo<IStudentPacketRepo>();

                var dbEntity = packetRepo.Get(studentId, SchoolYear.SchoolYearId);

                dbEntity.IsPRASigned = true;
                dbEntity.PRAGuardian = uow.GetRepo<IGuardianRepo>().Get(User.UserId);

                packetRepo.Update(dbEntity);

                uow.Commit();

                return JsonRes(Status.OK, "OK");
            }
        }

        public ActionResult GetStudentPacketCourse(Int64 studentPacketCourseId)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var packetRepo = uow.GetRepo<IStudentPacketRepo>();
                var packetCourseRepo = uow.GetRepo<IStudentPacketCourseRepo>();

                StudentPacketCourse spc = packetCourseRepo.Get(studentPacketCourseId);
                List<GoodAndServiceModel> goodService = GoodAndServiceHelper.GetListGoodAndService(spc.StudentPacketId)
                                                                            .Where(x => x.StudentPacketCourseID == studentPacketCourseId)
                                                                            .Select(x => new GoodAndServiceModel(x))
                                                                            .ToList();

                StudentPacketCourseModel model = new StudentPacketCourseModel();
                model.StudentPacketCourseId = studentPacketCourseId;
                model.TableGoodAndService = new TableGoodAndServiceModel()
                        {
                            GoodsAndServices = goodService.ToList(),
                            PlanningBudgetTotal = goodService.Sum(t => t.TotalPlanningBudget),
                            ActualSpendingTotal = goodService.Sum(t => t.TotalActualSpending)
                        };

                return PartialView("_StudentPacketCourseGoodService", model);
            }
        }

        private StudentPacketModel GetStudentPacket(Guid id)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var packetRepo = uow.GetRepo<IStudentPacketRepo>();
                var packetCourseRepo = uow.GetRepo<IStudentPacketCourseRepo>();
                var subjectRepo = uow.GetRepo<ISubjectRepo>();

                var dbEntity = packetRepo.AddIsNotExist(id, SchoolYear.SchoolYearId);
                if (dbEntity != null) { uow.Commit(); }

                var budgetStudentPacket = BudgetStudentHelper.GetBudgetStudentPacket(id, SchoolYear.SchoolYearId);

                var query = packetRepo.GetAsQuery(id, SchoolYear.SchoolYearId);
                var model = query.Select(x => new StudentPacketModel
                {
                    StudentPacketId = x.StudentPacketId,
                    IsLocked = x.IsLocked,
                    StudentId = x.StudentId,
                    StudentName = x.Student.FullName,
                    StudentGrade = x.Student.Grade,
                    SponsorTeacherId = x.SponsorTeacherId,
                    SponsorTeacherName = x.SponsorTeacher.FullName,
                    IsASDTASigned = x.IsASDTASigned,
                    IsPRASigned = x.IsPRASigned,
                    ILPPhilosophy = x.Student.ILPPhilosophy,
                    PBBeginningBalance = budgetStudentPacket.PBBeginningBalance,
                    PBBudgetTransferDeposits = budgetStudentPacket.PBBudgetTransferDeposits,
                    PBBudgetTransferWithdrawals = budgetStudentPacket.PBBudgetTransferWithdrawals,
                    PBAvailableRemainingFunds = budgetStudentPacket.PBAvailableRemainingFunds,
                    PBAvailableRemainingFundsTotal = budgetStudentPacket.PBAvailableRemainingFundsTotal,
                    ASBeginningBalance = budgetStudentPacket.ASBeginningBalance,
                    ASBudgetTransferDeposits = budgetStudentPacket.ASBudgetTransferDeposits,
                    ASBudgetTransferWithdrawals = budgetStudentPacket.ASBudgetTransferWithdrawals,
                    ASAvailableRemainingFunds = budgetStudentPacket.ASAvailableRemainingFunds,
                    ASAvailableRemainingFundsTotal = budgetStudentPacket.ASAvailableRemainingFundsTotal,
                    GoalEnrollmentPercent = budgetStudentPacket.GoalEnrollmentPercent,
                    GoalCoreUnits = budgetStudentPacket.GoalCoreUnits,
                    GoalElectiveUnits = budgetStudentPacket.GoalElectiveUnits,
                    GoalTotalUnits = budgetStudentPacket.GoalTotalUnits,
                    GoalILPHrs = budgetStudentPacket.GoalILPHrs,
                    GoalContractHrs = budgetStudentPacket.GoalContractHrs,
                    AchievedEnrollmentPercent = budgetStudentPacket.AchievedEnrollmentPercent,
                    AchievedCoreUnits = budgetStudentPacket.AchievedCoreUnits,
                    AchievedElectiveUnits = budgetStudentPacket.AchievedElectiveUnits,
                    AchievedTotalUnits = budgetStudentPacket.AchievedTotalUnits,
                    AchievedILPHrs = budgetStudentPacket.AchievedILPHrs,
                    AchievedContractHrs = budgetStudentPacket.AchievedContractHrs,
                    IndividualBudgetBudgetLimit = budgetStudentPacket.IBBudgetLimit,
                    //IndividualBudgetAmountBudgeted = budgetStudentPacket.IBAmountBudgeted,
                    //IndividualBudgetElectiveBalance = budgetStudentPacket.IBElectiveBalance,
                    IndividualBudgetGlobalReserve = budgetStudentPacket.IBGlobalReserve
                })
                .FirstOrDefault();

                model.StudentGradeStr = model.StudentGrade.GetDescription();

                model.SubjectSearchOptions = ":All";
                var subjects = subjectRepo.GetAll().ToList();

                foreach (var item in subjects)
                {
                    model.SubjectSearchOptions += String.Format(@";{0}:{1}", item.SubjectId, item.Name);
                }

                List<GoodAndServiceModel> goodService = GoodAndServiceHelper.GetListGoodAndService(model.StudentPacketId)
                                                                            .Select(x => new GoodAndServiceModel(x))
                                                                            .ToList();

                model.StudentPacketCourseModels = packetCourseRepo.GetByStudentPacket(model.StudentPacketId)
                    .Select(x => new
                    {
                        StudentPacketCourseId = x.StudentPacketCourseId,
                        FPCSCourseId = x.FPCSCourseId,
                        FPCSCourseName = x.FPCSCourse.Name,
                        ASDCourseCode = x.FPCSCourse.ASDCourse.ExternalASDCourseId,
                        ScheduleRepetition = x.FPCSCourse.ScheduleRepetition,
                        CourseScheduleOnDays = x.FPCSCourse.CourseScheduleOnDays,
                        TeacherId = x.FPCSCourse.TeacherId,
                        TeacherName = x.FPCSCourse.Teacher.FullName,
                        GuardianId = x.FPCSCourse.GuardianId,
                        GuardianName = x.FPCSCourse.Guardian.FullName,
                        SubjectId = x.FPCSCourse.ASDCourse.SubjectId,
                        Subject = x.FPCSCourse.ASDCourse.Subject.Name,
                        SubjectIsElective = x.FPCSCourse.ASDCourse.Subject.IsElective,
                        Semester = x.FPCSCourse.Semester,
                        SponsorSignatureGuid = x.StudentPacket.SponsorTeacherId,
                        SponsorSignature = x.SponsorSignature,
                        InstructorSignatureGuid = x.FPCSCourse.TeacherId,
                        InstructorSignature = x.InstructorSignature,
                        AdminSignature = x.AdminSignature,
                        GuardianSignature = x.GuardianSignature,
                        ContractHrs = x.FPCSCourse.TotalHours,
                        ILPHrs = x.ILPs.FirstOrDefault() != null ? x.ILPs.FirstOrDefault().CourseHrs : 0,
                        HasSponsorAlert = x.StudentPacketCourseAlerts.Count(t => t.User.Role == Role.Teacher) != 0,
                        HasParentAlert = x.StudentPacketCourseAlerts.Count(t => t.User.Role == Role.Guardian) != 0
                    })
                    .ToList()
                    .Select(x => new StudentPacketCourseModel
                    {
                        StudentPacketCourseId = x.StudentPacketCourseId,
                        FPCSCourseId = x.FPCSCourseId,
                        FPCSCourseName = x.FPCSCourseName,
                        StudentPacketCourseName = String.Format(@"{0}, {1}, [{2}]",
                                                    x.FPCSCourseName,
                                                    x.TeacherId.HasValue ? x.TeacherName : x.GuardianName,
                                                    x.ASDCourseCode),
                        TeacherId = x.TeacherId,
                        TeacherName = x.TeacherName,
                        GuardianId = x.GuardianId,
                        GuardianName = x.GuardianName,
                        SubjectId = x.SubjectId,
                        Subject = x.Subject,
                        SubjectIsElective = x.SubjectIsElective,
                        Semester = x.Semester,
                        IsFullSignature = x.SponsorSignature.HasValue && x.SponsorSignature.Value == Sign.Sign &&
                                            ((x.InstructorSignature.HasValue && x.InstructorSignature.Value == Sign.Sign) || x.GuardianId.HasValue) &&
                                            x.AdminSignature.HasValue && x.AdminSignature.Value == Sign.Sign &&
                                            x.GuardianSignature.HasValue && x.GuardianSignature.Value == Sign.Sign,
                        SponsorSignatureGuid = x.SponsorSignatureGuid,
                        SponsorSignature = x.SponsorSignature.HasValue ? x.SponsorSignature.Value.GetDescription() : "not signed",
                        InstructorSignatureGuid = x.InstructorSignatureGuid,
                        InstructorSignature = x.InstructorSignature.HasValue ? x.InstructorSignature.Value.GetDescription() : "not signed",
                        AdminSignature = x.AdminSignature.HasValue ? x.AdminSignature.Value.GetDescription() : "not signed",
                        AdminSignatureInt = x.AdminSignature.HasValue ? (Int32)x.AdminSignature.Value : 0,
                        GuardiansSignature = x.GuardianSignature.HasValue ? x.GuardianSignature.Value.GetDescription() : "not signed",
                        IsInstructorSponsor = x.SponsorSignatureGuid.HasValue && x.InstructorSignatureGuid.HasValue && x.SponsorSignatureGuid.Value == x.InstructorSignatureGuid.Value,
                        IsInstructorGuardian = x.GuardianId.HasValue,
                        ILPHours = x.ILPHrs,
                        TableGoodAndService = new TableGoodAndServiceModel()
                        {
                            GoodsAndServices = goodService.Where(t => t.StudentPacketCourseID == x.StudentPacketCourseId)
                                                            .ToList(),
                            PlanningBudgetTotal = goodService.Where(t => t.StudentPacketCourseID == x.StudentPacketCourseId)
                                                                .Sum(t => t.TotalPlanningBudget),
                            ActualSpendingTotal = goodService.Where(t => t.StudentPacketCourseID == x.StudentPacketCourseId)
                                                                .Sum(t => t.TotalActualSpending)
                        },
                        Status = x.HasSponsorAlert ?
                                    StudentPacketCourseStatus.SponsorAlert :
                                    x.HasParentAlert ?
                                        StudentPacketCourseStatus.ParentAlert :
                                        (x.AdminSignature.HasValue && x.AdminSignature.Value == Sign.Rejected) ||
                                        (x.SponsorSignature.HasValue && x.SponsorSignature.Value == Sign.Rejected) ?
                                            StudentPacketCourseStatus.Rejected :
                                            (x.AdminSignature.HasValue && x.AdminSignature.Value == Sign.MustAmend) ||
                                            (x.SponsorSignature.HasValue && x.SponsorSignature.Value == Sign.MustAmend) ?
                                                StudentPacketCourseStatus.MustAmend :
                                                ((!x.GuardianSignature.HasValue || (x.GuardianSignature.HasValue && x.GuardianSignature.Value != Sign.Sign)) ||
                                                    (!x.SponsorSignature.HasValue || (x.SponsorSignature.HasValue && x.SponsorSignature.Value != Sign.Sign)) ||
                                                    (!x.InstructorSignature.HasValue || (x.InstructorSignature.HasValue && x.InstructorSignature.Value != Sign.Sign))) ?
                                                    StudentPacketCourseStatus.NotFullySigned :
                                                    StudentPacketCourseStatus.FullySigned,
                        CourseHelper =
                                    (x.AdminSignature.HasValue && x.AdminSignature.Value == Sign.Rejected) ||
                                    (x.SponsorSignature.HasValue && x.SponsorSignature.Value == Sign.Rejected) ?
                                    "This course has been rejected. The Guardian or the Sponsor must delete this course. The funds budgeted by this course will not be released until the course is deleted." :
                                    (x.AdminSignature.HasValue && x.AdminSignature.Value == Sign.MustAmend) ||
                                    (x.SponsorSignature.HasValue && x.SponsorSignature.Value == Sign.MustAmend) ?
                                        "This course needs work before it can be signed off on. Please fix any problems and re-sign the contract after any issues have been resolved." :
                                        ((!x.GuardianSignature.HasValue || (x.GuardianSignature.HasValue && x.GuardianSignature.Value != Sign.Sign)) ||
                                            (!x.SponsorSignature.HasValue || (x.SponsorSignature.HasValue && x.SponsorSignature.Value != Sign.Sign)) ||
                                            (!x.InstructorSignature.HasValue || (x.InstructorSignature.HasValue && x.InstructorSignature.Value != Sign.Sign))) ?
                                            "This course has not yet been signed by all parties. In order for this course to be complete all parties must sign." :
                                            "Congratulations! This course has been approved.",
                    })
                    .ToList();

                var achievedStudentPacket =
                    BudgetStudentHelper.GetAchievedStudentPacket(
                        model.GoalTotalUnits,
                        model.StudentPacketCourseModels
                                .Select(x => new StudentPacketCourseHelperModel()
                                {
                                    IsElective = x.SubjectIsElective,
                                    ILPHours = x.ILPHours,
                                    ContractHrs = x.ContractHrs,
                                    IsFullSignature = x.IsFullSignature
                                })
                                .ToList()
                    );

                model.AchievedILPHrs = achievedStudentPacket.AchievedILPHrs;
                model.AchievedContractHrs = achievedStudentPacket.AchievedContractHrs;
                model.AchievedCoreUnits = achievedStudentPacket.AchievedCoreUnits;
                model.AchievedElectiveUnits = achievedStudentPacket.AchievedElectiveUnits;
                model.AchievedTotalUnits = achievedStudentPacket.AchievedTotalUnits;
                model.AchievedEnrollmentPercent = achievedStudentPacket.AchievedEnrollmentPercent;

                var individualBudgetStudentPacket =
                    BudgetStudentHelper.GetIndividualBudgetStudentPacket(
                        model.IndividualBudgetBudgetLimit,
                        model.IndividualBudgetGlobalReserve,
                        model.StudentPacketCourseModels
                                .Select(x => new StudentPacketCourseHelperModel()
                                {
                                    SubjectId = x.SubjectId,
                                    IsElective = x.SubjectIsElective,
                                    PlanningBudgetTotal = x.TableGoodAndService.PlanningBudgetTotal,
                                    ActualSpendingTotal = x.TableGoodAndService.ActualSpendingTotal
                                })
                                .ToList()
                    );

                model.IndividualBudgetBudgetLimit = individualBudgetStudentPacket.IBBudgetLimit;
                model.IndividualBudgetAmountBudgeted = individualBudgetStudentPacket.IBAmountBudgeted;
                model.IndividualBudgetElectiveBalance = individualBudgetStudentPacket.IBElectiveBalance;
                model.IndividualBudgetGlobalReserve = individualBudgetStudentPacket.IBGlobalReserve;

                // Items still needed to complete this packet
                model.IsCompletePacketCourse = model.StudentPacketCourseModels != null &&
                                                model.StudentPacketCourseModels.Count() != 0 &&
                                                model.StudentPacketCourseModels.Select(x => x.AdminSignature).All(x => x == Sign.Sign.GetDescription());

                model.ItemsNeedComplete = new List<String>();

                if (model.AchievedCoreUnits < model.GoalCoreUnits)
                {
                    model.ItemsNeedComplete.Add(String.Format(@"{0} more Core Units", Math.Round(model.GoalCoreUnits - model.AchievedCoreUnits, 1).ToString()));
                }
                else
                {
                    model.ItemsNeedComplete.Add(String.Empty);
                }

                if (model.AchievedCoreUnits < model.GoalCoreUnits || (model.AchievedElectiveUnits + model.AchievedCoreUnits) < (model.GoalCoreUnits + model.GoalElectiveUnits))
                {
                    model.ItemsNeedComplete.Add(String.Format(@"{0} more Units overall", Math.Round((model.GoalCoreUnits + model.GoalElectiveUnits) - (model.AchievedCoreUnits + model.AchievedElectiveUnits), 1).ToString()));
                }
                else
                {
                    model.ItemsNeedComplete.Add(String.Empty);
                }

                if (model.AchievedContractHrs < model.GoalContractHrs)
                {
                    model.ItemsNeedComplete.Add(String.Format(@"{0} more Contract Hours", Math.Round(model.GoalContractHrs - model.AchievedContractHrs, 1).ToString()));
                }
                else
                {
                    model.ItemsNeedComplete.Add(String.Empty);
                }

                if (!model.IsASDTASigned && !model.IsPRASigned)
                {
                    model.ItemsNeedComplete.Add("ASD Testing Agreement must be signed and Progress Report Agreement must be signed");
                }
                else if (!model.IsASDTASigned)
                {
                    model.ItemsNeedComplete.Add("ASD Testing Agreement must be signed");
                }
                else if (!model.IsPRASigned)
                {
                    model.ItemsNeedComplete.Add("Progress Report Agreement must be signed");
                }
                else
                {
                    model.ItemsNeedComplete.Add(String.Empty);
                }

                // TODO: Task from 14/01/2015

                //if (String.IsNullOrEmpty(model.ILPPhilosophy))
                //{
                //    model.ItemsNeedComplete.Add("Must provide an ILP Philosophy");
                //}
                //else
                //{
                //    model.ItemsNeedComplete.Add(String.Empty);
                //}

                //if (model.StudentPacketCourseModels.Select(x => x.IsInstructorSponsor).All(x => x == false))
                //{
                //    model.ItemsNeedComplete.Add("Packet must include an ASD Sponsor/Oversight class with at least 1 contract hour.");
                //}
                //else
                //{
                //    model.ItemsNeedComplete.Add(String.Empty);
                //}

                return model;
            }
        }
    }
}
