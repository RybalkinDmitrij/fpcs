﻿using FPCS.Core.jqGrid;
using FPCS.Core.Extensions;

using FPCS.Data;
using FPCS.Data.Enums;
using FPCS.Data.Repo;

using FPCS.Web.Admin.Code;
using FPCS.Web.Admin.Models.SessionModels;
using FPCS.Web.Admin.Models.SignedCourse;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FPCS.Data.Entities;

namespace FPCS.Web.Admin.Controllers
{
    public class SignedCourseController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult _Index(GridOptions options, SignedCourseListOptions listOptions)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                Int32 currentSchoolYearId = Session.GetSchoolYear().SchoolYearId;

                var repo = uow.GetRepo<IStudentPacketCourseRepo>();

                List<SignedCourseMiniModel> dbList = new List<SignedCourseMiniModel>();

                SessionUserModel currentUser = Session.GetUser();

                var schoolYearId = Session.GetSchoolYear().SchoolYearId;

                if (currentUser.Role == Role.Admin)
                {
                    dbList = uow.GetRepo<IStudentPacketRepo>()
                                 .GetAll()
                                 .Where(x => x.SchoolYearId == schoolYearId && !x.IsDeleted)
                                 .Select(x => new
                                 {
                                     StudentGuid = x.Student.DbUserId,
                                     StudentPacketId = x.StudentPacketId,
                                     StudentLastName = x.Student.LastName,
                                     StudentFirstName = x.Student.FirstName,
                                     Grade = x.Student.Grade,
                                     HomePhone = x.Student.Families.FirstOrDefault() != null ?
                                                    x.Student.Families.FirstOrDefault().Telephone : String.Empty,
                                     EmailFamily = x.Student.Families.FirstOrDefault() != null ?
                                                    x.Student.Families.FirstOrDefault().Email : String.Empty
                                 })
                                 .ToList()
                                 .Select(x => new SignedCourseMiniModel()
                                 {
                                     StudentGuid = x.StudentGuid,
                                     StudentPacketId = x.StudentPacketId,
                                     StudentName = String.Format(@"{0}, {1}", x.StudentLastName, x.StudentFirstName),
                                     Grade = x.Grade.GetDescription(),
                                     HomePhone = x.HomePhone,
                                     EmailFamily = x.EmailFamily
                                 })
                                 .ToList();
                }
                else if (currentUser.Role == Role.Guardian)
                {
                    Guardian guardian = uow.GetRepo<IGuardianRepo>()
                                           .Get(currentUser.UserId);

                    var familyId = guardian.Families.FirstOrDefault(t => !t.IsDeleted) != null ?
                        guardian.Families.FirstOrDefault(t => !t.IsDeleted).FamilyId : 0;

                    List<Guid> children = uow.GetRepo<IStudentRepo>()
                                             .GetAll()
                                             .Where(x => x.Families.FirstOrDefault().FamilyId == familyId)
                                             .Select(x => x.DbUserId)
                                             .ToList();

                    dbList = uow.GetRepo<IStudentPacketRepo>()
                                 .GetAll()
                                 .Where(x => x.SchoolYearId == schoolYearId && !x.IsDeleted &&
                                             children.Contains(x.StudentId))
                                 .Select(x => new
                                 {
                                     StudentGuid = x.Student.DbUserId,
                                     StudentPacketId = x.StudentPacketId,
                                     StudentLastName = x.Student.LastName,
                                     StudentFirstName = x.Student.FirstName,
                                     Grade = x.Student.Grade,
                                     HomePhone = x.Student.Families.FirstOrDefault() != null ?
                                                    x.Student.Families.FirstOrDefault().Telephone : String.Empty,
                                     EmailFamily = x.Student.Families.FirstOrDefault() != null ?
                                                    x.Student.Families.FirstOrDefault().Email : String.Empty
                                 })
                                 .ToList()
                                 .Select(x => new SignedCourseMiniModel()
                                 {
                                     StudentGuid = x.StudentGuid,
                                     StudentPacketId = x.StudentPacketId,
                                     StudentName = String.Format(@"{0}, {1}", x.StudentLastName, x.StudentFirstName),
                                     Grade = x.Grade.GetDescription(),
                                     HomePhone = x.HomePhone,
                                     EmailFamily = x.EmailFamily
                                 })
                                 .ToList();
                }
                else if (currentUser.Role == Role.Teacher)
                {
                    dbList = uow.GetRepo<IStudentPacketRepo>()
                                 .GetAll()
                                 .Where(x => x.SchoolYearId == schoolYearId && !x.IsDeleted &&
                                             (x.SponsorTeacherId.HasValue && x.SponsorTeacherId.Value == currentUser.UserId ||
                                             x.StudentPacketCourses.Select(t => t.FPCSCourse.TeacherId).Contains(currentUser.UserId)))
                                 .Select(x => new
                                 {
                                     StudentGuid = x.Student.DbUserId,
                                     StudentPacketId = x.StudentPacketId,
                                     StudentLastName = x.Student.LastName,
                                     StudentFirstName = x.Student.FirstName,
                                     Grade = x.Student.Grade,
                                     HomePhone = x.Student.Families.FirstOrDefault() != null ?
                                                    x.Student.Families.FirstOrDefault().Telephone : String.Empty,
                                     EmailFamily = x.Student.Families.FirstOrDefault() != null ?
                                                    x.Student.Families.FirstOrDefault().Email : String.Empty
                                 })
                                 .ToList()
                                 .Select(x => new SignedCourseMiniModel()
                                 {
                                     StudentGuid = x.StudentGuid,
                                     StudentPacketId = x.StudentPacketId,
                                     StudentName = String.Format(@"{0}, {1}", x.StudentLastName, x.StudentFirstName),
                                     Grade = x.Grade.GetDescription(),
                                     HomePhone = x.HomePhone,
                                     EmailFamily = x.EmailFamily
                                 })
                                 .ToList();
                }

                var engine = new GridDynamicEngine(options, listOptions);

                var filterData = engine.ApplyFilter(dbList.AsQueryable());

                var totalCount = filterData.Count();

                var data = engine.ApplySortAndPaging(filterData).ToList();

                var dataIds = data.Select(t => t.StudentGuid).ToList();

                var packetCourses = repo
                                       .GetAll()
                                       .Where(x => !x.IsDeleted &&
                                                   dataIds.Contains(x.StudentPacket.Student.DbUserId))
                                       .ToList();

                data = data.Select(x => new SignedCourseMiniModel()
                            {
                                StudentGuid = x.StudentGuid,
                                StudentName = x.StudentName,
                                Grade = x.Grade,
                                HomePhone = x.HomePhone,
                                EmailFamily = x.EmailFamily,
                                GNS = packetCourses.Where(t => t.StudentPacket.StudentPacketId == x.StudentPacketId &&
                                                              t.GuardianSignature != Sign.Sign)
                                                  .Count(),
                                SNS = packetCourses.Where(t => t.StudentPacket.StudentPacketId == x.StudentPacketId &&
                                                              t.SponsorSignature != Sign.Sign)
                                                  .Count(),
                                INS = packetCourses.Where(t => t.StudentPacket.StudentPacketId == x.StudentPacketId &&
                                                              t.InstructorSignature != Sign.Sign)
                                                  .Count(),
                                ANS = packetCourses.Where(t => t.StudentPacket.StudentPacketId == x.StudentPacketId &&
                                                              t.AdminSignature != Sign.Sign)
                                                  .Count(),
                                AMA = 0,
                                SA = 0,
                                GA = 0
                            })
                           .ToList();

                var result = new GridResult<SignedCourseMiniModel>(options, totalCount, data);

                return Json(result);
            }
        }
    }
}