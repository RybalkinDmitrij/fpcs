﻿using System;
using System.Linq;
using System.Web.Mvc;
using FPCS.Data;
using FPCS.Data.Repo;
using FPCS.Data.Enums;
using FPCS.Web.Admin.Code;
using FPCS.Web.Admin.Models.Guardian;
using FPCS.Core.jqGrid;
using System.Collections.Generic;
using FPCS.Data.Entities;

namespace FPCS.Web.Admin.Controllers
{
	public class GuardianController : BaseController
	{
		[FPCSAuthorize(Role.Admin, Role.Teacher, Role.Guardian)]
		public ActionResult Index()
		{
			return View();
		}

		[FPCSAuthorize(Role.Admin, Role.Teacher, Role.Guardian)]
		public JsonResult _Index(GridOptions options, GuardiansListOptions guardiansListOptions)
		{
			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				var repo = uow.GetRepo<IGuardianRepo>();
				var repoFamily = uow.GetRepo<IFamilyRepo>();
				IQueryable<Guardian> dbList;

				if (User.Role != Role.Guardian)
				{
					dbList = repo.GetAll(SchoolYear.SchoolYearId);
				}
				else
				{
					var familyIds = repoFamily.GetFamiliesByGuardian(User.UserId).Select(x => x.FamilyId).ToList();
					dbList = repo.GetByFamilies(SchoolYear.SchoolYearId, familyIds);
				} 

				var engine = new GridDynamicEngine(options, guardiansListOptions);
				var result = engine.ApplyAll(dbList, x => new GuardianMiniModel
				{
					DbUserId = x.DbUserId,
					FullName = x.LastName + ", " + x.FirstName,
					Email = x.Email,
					IsLocked = x.IsLocked ? "Locked" : "Active"
				});

				return Json(result);
			}
		}

		[FPCSAuthorize(Role.Admin, Role.Guardian, Role.Teacher)]
		[HttpGet]
		public ActionResult _Details(Guid id)
		{
			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				var repo = uow.GetRepo<IGuardianRepo>();
				var dbEntity = repo.Get(id);
				if (dbEntity == null) return JsonRes(Status.Error, "Guardian not found");

				var model = new GuardianDetailsModel
				{
					DbUserId = dbEntity.DbUserId,
					FirstName = dbEntity.FirstName,
					LastName = dbEntity.LastName,
					MiddleInitial = dbEntity.MiddleInitial,
					Employer = dbEntity.Employer,
					IsActiveMilitary = dbEntity.IsActiveMilitary,
					Rank = dbEntity.Rank,
					Pager = dbEntity.Pager,
					BusinessPhone = dbEntity.BusinessPhone,
					Ext = dbEntity.Ext,
					CellPhone = dbEntity.CellPhone,
					Email = dbEntity.Email,
					Address = dbEntity.Address,
					City = dbEntity.City,
					Country = dbEntity.Country,
					Zip = dbEntity.Zip,
					State = dbEntity.State == null ? "Same as for family" : dbEntity.State.Code
				};

				model.Init();
				return PartialView(model);
			}
		}

		[FPCSAuthorize(Role.Admin, Role.Teacher)]
		[HttpGet]
		public PartialViewResult _Create()
		{
			var model = new GuardianCreateModel();
			model.Init();
			
			return PartialView(model);
		}

		[FPCSAuthorize(Role.Admin, Role.Teacher)]
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult _Create(GuardianCreateModel model)
		{
			if (ModelState.IsValid)
			{
				try
				{
					using (var uow = UnityManager.Resolve<IUnitOfWork>())
					{
						var repo = uow.GetRepo<IGuardianRepo>();
						var dbGuardian = repo.Add(model.FirstName, model.LastName, model.MiddleInitial, model.Employer,
							model.IsActiveMilitary, model.Rank, model.Pager, model.BusinessPhone, model.Ext, model.CellPhone,
							model.Email, model.Address, model.City, model.Country, model.Zip, model.StateId, SchoolYear.SchoolYearId);

						uow.Commit();

						dbGuardian.UnqDbUserId = dbGuardian.DbUserId;

						uow.Commit();

						//var sgRepo = uow.GetRepo<IFamilyRelationshipRepo>();
						//foreach (var item in model.GuardianRelationships)
						//{
						//	sgRepo.Update(dbGuardian.DbUserId, item.DbUserId, item.FamilyRelationshipType);
						//}

						//uow.Commit();

						return
							JsonRes(
								new
								{
									id = dbGuardian.DbUserId.ToString(),
									text = String.Format("{0}, {1}", dbGuardian.LastName, dbGuardian.FirstName)
								});
					}
				}
				catch (Exception ex)
				{
					ModelState.AddModelError("", ex.Message);
				}
			}

			model.Init();
			return PartialView(model);
		}

		[FPCSAuthorize(Role.Admin, Role.Guardian, Role.Teacher)]
		[HttpGet]
		public PartialViewResult _Edit(Guid id)
		{
			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				var dbEntity = uow.GetRepo<IGuardianRepo>().Get(id);
				if (dbEntity == null) return ErrorPartial("Guardian {0} not found", id);

				var model = new GuardianEditModel
				{
					DbUserId = dbEntity.DbUserId,
					FirstName = dbEntity.FirstName,
					LastName = dbEntity.LastName,
					MiddleInitial = dbEntity.MiddleInitial,
					Employer = dbEntity.Employer,
					IsActiveMilitary = dbEntity.IsActiveMilitary,
					Rank = dbEntity.Rank,
					Pager = dbEntity.Pager,
					BusinessPhone = dbEntity.BusinessPhone,
					Ext = dbEntity.Ext,
					CellPhone = dbEntity.CellPhone,
					Email = dbEntity.Email,
					Address = dbEntity.Address,
					City = dbEntity.City,
					Country = dbEntity.Country,
					Zip = dbEntity.Zip,
					StateId = dbEntity.StateId,
					GuardianRelationships = dbEntity.Relationships.Select(x =>
						new RelationshipModel
						{
							DbUserId = x.Student.DbUserId,
							FullName = x.Student.FullName,
							FamilyRelationshipType = x.FamilyRelationshipType
						}).ToList()
				};

				model.Init();
				return PartialView(model);
			}
		}

		[FPCSAuthorize(Role.Admin, Role.Guardian, Role.Teacher)]
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult _Edit(GuardianEditModel model)
		{
			if (model.GuardianRelationships == null) model.GuardianRelationships = new List<RelationshipModel>();

			if (ModelState.IsValid)
			{
				try
				{
					using (var uow = UnityManager.Resolve<IUnitOfWork>())
					{
						var repo = uow.GetRepo<IGuardianRepo>();
						var dbEntity = repo.Update(model.DbUserId, model.FirstName, model.LastName, model.MiddleInitial, model.Employer,
							model.IsActiveMilitary, model.Rank, model.Pager, model.BusinessPhone, model.Ext, model.CellPhone,
							model.Email, model.Address, model.City, model.Country, model.Zip, model.StateId);

						var sgRepo = uow.GetRepo<IFamilyRelationshipRepo>();
						foreach (var item in model.GuardianRelationships)
						{
							sgRepo.Update(dbEntity.DbUserId, item.DbUserId, item.FamilyRelationshipType);
						}

						uow.Commit();
						return JsonRes(new {DbUserId = dbEntity.DbUserId, FullName = dbEntity.FullName, Phone = dbEntity.CellPhone});
					}
				}
				catch (Exception ex)
				{
					ModelState.AddModelError("", ex.Message);
				}
			}

			model.Init();
			return PartialView(model);
		}

		[FPCSAuthorize(Role.Admin, Role.Teacher)]
		[HttpPost]
		public ActionResult Delete(Guid id)
		{
			try
			{
				using (var uow = UnityManager.Resolve<IUnitOfWork>())
				{
					var repo = uow.GetRepo<IGuardianRepo>();
					repo.Remove(id);

					uow.Commit();
					return JsonRes();
				}
			}
			catch (Exception ex)
			{
				return JsonRes(Status.Error, ex.Message);
			}
		}

		[FPCSAuthorize(Role.Admin, Role.Teacher)]
		[HttpPost]
		public ActionResult DeleteAll(String id)
		{
			try
			{
				if (String.IsNullOrEmpty(id)) return JsonRes(Status.Error, "Not correct ids");

				var ids = id.Split(',');
				using (var uow = UnityManager.Resolve<IUnitOfWork>())
				{
					Int32 errorsCount = 0;
					var repo = uow.GetRepo<IGuardianRepo>();
					foreach (var item in ids)
					{
						try
						{
							repo.Remove(new Guid(item));
						}
						catch (Exception)
						{
							errorsCount++;
						}
					}

					uow.Commit();
					if (errorsCount == 0) return JsonRes();
					else return JsonRes(Status.Error, "Were deleted not all entities");
				}
			}
			catch (Exception ex)
			{
				return JsonRes(Status.Error, ex.Message);
			}
		}
	}
}
