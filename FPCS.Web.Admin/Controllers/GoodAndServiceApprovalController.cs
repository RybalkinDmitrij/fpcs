﻿using FPCS.Core.jqGrid;
using FPCS.Core.Extensions;
using FPCS.Data;
using FPCS.Data.Entities;
using FPCS.Data.Enums;
using FPCS.Data.Repo;
using FPCS.Web.Admin.Code;
using FPCS.Web.Admin.Models.GoodAndServiceApproval;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using System.Runtime.InteropServices.ComTypes;
using Newtonsoft.Json;
using System.Web.Script.Serialization;

namespace FPCS.Web.Admin.Controllers
{
	public class GoodAndServiceApprovalController : BaseController
	{
		[FPCSAuthorize(Role.Admin)]
		public ActionResult Index()
		{
			return View();
		}

		[FPCSAuthorize(Role.Admin)]
		public JsonResult _Index(GridOptions options, GoodAndServiceApprovalListOptions listOptions)
		{
			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				var dbList = GetData(uow, options, listOptions);

				var engine = new GridDynamicEngine(options, listOptions);
				var result = engine.ApplyAll3(dbList, x => new GoodAndServiceApprovalModel()
				{
					GoodAndServiceId = x.GoodAndServiceId,
					GoodOrService = x.GoodOrService.GetDescription(),
					RequisitionOrReimbursement = x.RequisitionOrReimbursement.GetDescription(),
					StudentsName = String.Format(@"{0} {1}", x.StudentsLastName, x.StudentsFirstName),
					VendorName = x.VendorName,
					ItemNumber = x.ItemNumber,
					UserRequest = x.UserRequest,
					DateRequest = x.DateRequest.HasValue ? x.DateRequest.Value.ToString("MM/dd/yyyy") : String.Empty,
					Budget = x.NumberOfUnits*x.UnitPrice + x.ShippingHandlingFees,
					Balance =
						(x.NumberOfUnits*x.UnitPrice + x.ShippingHandlingFees) -
						x.GoodServiceBalances.Sum(t => t.UnitPrice*t.QTY + t.Shipping),
					IsClosed = x.IsClosed,
					CourseStatus = x.CourseStatus.HasValue && x.CourseStatus.Value == Sign.Sign
						? x.DateCourseStatus.Value.ToShortDateString()
						: x.CourseStatus.HasValue && x.CourseStatus.Value != Sign.Sign
							? x.CourseStatus.Value.GetDescription()
							: "not signed",
					GoodOrServiceApprovedText = x.Aprv.GetDescription(),
					CommentsRejectReason = x.CommentsRejectReason,
					DateGuardianSignature =
						x.DateGuardianSignature.HasValue ? x.DateGuardianSignature.Value.ToString("MM/dd/yyyy") : String.Empty,
					DateSponsorSignature =
						x.DateSponsorSignature.HasValue ? x.DateSponsorSignature.Value.ToString("MM/dd/yyyy") : String.Empty,
					GuardianSignature = x.GuardianSignature.HasValue ? x.GuardianSignature.ToString() : "Not signed",
					SponsorSignature = x.SponsorSignature.HasValue ? x.SponsorSignature.ToString() : "Not signed",
					FamilyEmail = x.FamilyEmail,
					SponsorTeacherEmail = x.SponsorTeacherEmail,
                    IsWithdrawal = x.IsWithdrawal,
                    Semester = x.Semester,
                    DateCreated = x.DateCreated.ToString("MM/dd/yyyy")
				});

				return Json(result);
			}
		}

		[FPCSAuthorize(Role.Admin)]
		public ActionResult _PreEdit(Int64 id)
		{
			PreEditModel model = new PreEditModel(id);

			return PartialView(model);
		}

		[FPCSAuthorize(Role.Admin)]
		public JsonResult _TableBalanceForSubGrid(Int64 goodServiceId)
		{
			List<GoodAndServiceSubGridModel> model = new List<GoodAndServiceSubGridModel>();

			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				model = uow.GetRepo<IGoodServiceBalanceRepo>()
					.GetByGoodService(goodServiceId)
					.Select(x => new GoodAndServiceSubGridModel
					{
						GoodServiceId = x.GoodServiceId,
						GoodServiceBalanceId = x.GoodServiceBalanceId,
						PO = x.PO,
						Invoice = x.Invoice,
						Check = x.Check,
						CheckDate = x.CheckDate,
						Payee = x.Payee,
						ReceiptDate = x.ReceiptDate,
						Description = x.Description,
						UnitPrice = x.UnitPrice,
						QTY = x.QTY,
						Shipping = x.Shipping
					})
					.ToList();
			}

			return Json(model, JsonRequestBehavior.AllowGet);
		}

		[FPCSAuthorize(Role.Admin)]
		public ActionResult _TableBalance(Int64 goodServiceId)
		{
			TableGoodAndServiceBalanceModel model = new TableGoodAndServiceBalanceModel();

			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				model.Balances = uow.GetRepo<IGoodServiceBalanceRepo>()
					.GetByGoodService(goodServiceId)
					.ToList()
					.Select(x => new GoodAndServiceMiniBalanceModel(x))
					.ToList();
			}

			return PartialView(model);
		}

		[FPCSAuthorize(Role.Admin)]
		[HttpGet]
		public ActionResult _Edit(Int64 id)
		{
			EditModel model = new EditModel(id);

			return PartialView(model);
		}

		[FPCSAuthorize(Role.Admin)]
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult _Edit(EditModel model)
		{
			if (ModelState.IsValid)
			{
				using (var uow = UnityManager.Resolve<IUnitOfWork>())
				{
					var repo = uow.GetRepo<IGoodServiceRepo>();

					var dbEntity = repo.Get(model.GoodServiceId);

					if (model.IsClosed || model.Aprv == GoodOrServiceApproved.b_rejc)
					{
						var balances = uow.GetRepo<IGoodServiceBalanceRepo>().GetByGoodService(dbEntity.GoodServiceId);

						foreach (var item in balances)
						{
							uow.GetRepo<IGoodServiceBalanceRepo>().Remove(item.GoodServiceBalanceId);
						}

						uow.Commit();

						dbEntity.IsClosed = true;
						dbEntity.GoodOrServiceApproved = GoodOrServiceApproved.b_rejc;
					}
					else
					{
						dbEntity.IsClosed = (Boolean?) model.IsClosed;
						dbEntity.GoodOrServiceApproved = (GoodOrServiceApproved?) model.Aprv;
					}

					dbEntity.CommentsRejectReason = model.CommentsRejectReason;

					repo.Update(dbEntity);

					uow.Commit();

					return JsonRes(dbEntity.GoodServiceId.ToString());
				}
			}

			model.Init();
			return PartialView(model);
		}

		[FPCSAuthorize(Role.Admin)]
		[HttpGet]
		public ActionResult _EditBalance(Int64 id, Int64 goodServiceId)
		{
			GoodAndServiceBalanceModel model = new GoodAndServiceBalanceModel(id, goodServiceId);

			return PartialView(model);
		}

		[FPCSAuthorize(Role.Admin)]
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult _EditBalance(GoodAndServiceBalanceModel model)
		{
			if (ModelState.IsValid)
			{
				using (var uow = UnityManager.Resolve<IUnitOfWork>())
				{
					var repo = uow.GetRepo<IGoodServiceBalanceRepo>();

					var dbEntity = repo.Get(model.GoodServiceBalanceId);

					if (dbEntity == null)
					{
						dbEntity = new GoodServiceBalance();
					}

					dbEntity.GoodServiceId = model.GoodServiceId;
					dbEntity.PO = model.PO.TrimAndReduce();
					dbEntity.Invoice = model.Invoice.TrimAndReduce();
					dbEntity.Check = model.Check.TrimAndReduce();
					dbEntity.CheckDate = model.CheckDate;
					dbEntity.Payee = model.Payee.TrimAndReduce();
					dbEntity.ReceiptDate = model.ReceiptDate;
					dbEntity.Description = model.Description.TrimAndReduce();
					dbEntity.UnitPrice = model.UnitPrice;
					dbEntity.QTY = model.QTY;
					dbEntity.Shipping = model.Shipping;

					if (dbEntity.GoodServiceBalanceId == 0)
					{
						dbEntity.DateEntered = DateTime.Now;
						dbEntity = repo.Add(dbEntity);
					}
					else
					{
						repo.Update(dbEntity);
					}

					uow.Commit();

					return JsonRes(dbEntity.GoodServiceBalanceId.ToString());
				}
			}

			return PartialView(model);
		}

		[FPCSAuthorize(Role.Admin)]
		[HttpPost]
		public ActionResult Delete(Int64 id)
		{
			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				var repo = uow.GetRepo<IGoodServiceBalanceRepo>();
				repo.Remove(id);

				uow.Commit();
				return JsonRes();
			}
		}

		[FPCSAuthorize(Role.Admin)]
		[HttpPost]
		public ActionResult EditRow(GoodAndServiceRowModel model)
		{
			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				var repo = uow.GetRepo<IGoodServiceRepo>();

				GoodService dbEntity = repo.Get(model.GoodAndServiceId);
				dbEntity.IsClosed = model.IsClosed;
				dbEntity.CommentsRejectReason = model.CommentsRejectReason;

				repo.Update(dbEntity);
				uow.Commit();

				return JsonRes();
			}

			return JsonRes(Status.Error, "Error");
		}

		[FPCSAuthorize(Role.Admin)]
		[HttpPost]
		public ActionResult EditSubGridRow(GoodAndServiceSubGridModel model)
		{
			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				var repo = uow.GetRepo<IGoodServiceBalanceRepo>();

				GoodServiceBalance dbEntity = repo.Get(model.GoodServiceBalanceId);

				if (dbEntity == null)
				{
					dbEntity = new GoodServiceBalance();
				}
				dbEntity.Check = model.Check;
				dbEntity.CheckDate = model.CheckDate;
				dbEntity.Description = model.Description;
				dbEntity.GoodServiceId = model.GoodServiceId;
				dbEntity.Invoice = model.Invoice;
				dbEntity.Payee = model.Payee;
				dbEntity.PO = model.PO;
				dbEntity.QTY = model.QTY;
				dbEntity.ReceiptDate = model.ReceiptDate;
				dbEntity.Shipping = model.Shipping;
				dbEntity.UnitPrice = model.UnitPrice;

				if (model.GoodServiceBalanceId == 0)
					repo.Add(dbEntity);
				else
					repo.Update(dbEntity);

				uow.Commit();

				return JsonRes();
			}

			return JsonRes(Status.Error, "Error");
		}

		[FPCSAuthorize(Role.Admin)]
		[HttpPost]
		public ActionResult DeleteSubGridRow(Int32 id)
		{
			try
			{
				using (var uow = UnityManager.Resolve<IUnitOfWork>())
				{
					var repo = uow.GetRepo<IGoodServiceBalanceRepo>();
					repo.Remove(id);

					uow.Commit();
					return JsonRes();
				}
			}
			catch (Exception ex)
			{
				return JsonRes(Status.Error, ex.Message);
			}
		}

		[FPCSAuthorize(Role.Admin)]
		public ActionResult _PreDetail(Int64 id)
		{
			PreDetailModel model = new PreDetailModel(id);

			return PartialView(model);
		}

		[FPCSAuthorize(Role.Admin)]
		public ActionResult StudentPacket(Int64 id)
		{
			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				var repo = uow.GetRepo<IGoodServiceRepo>();
				GoodService dbEntity = repo.Get(id);

				return RedirectToAction("Index", "StudentPacket2", new
				{
					id = dbEntity.StudentPacketCourse != null
						? dbEntity.StudentPacketCourse.StudentPacket.StudentId
						: dbEntity.StudentPacket.StudentId
				});
			}
		}

		[FPCSAuthorize(Role.Admin)]
		public ActionResult FPCSCourse(Int64 id)
		{
			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				var repo = uow.GetRepo<IGoodServiceRepo>();
				GoodService dbEntity = repo.Get(id);

				return RedirectToAction("_PreEdit", "FPCSCourse", new {id = dbEntity.StudentPacketCourse.FPCSCourseId});
			}
		}

		[FPCSAuthorize(Role.Admin)]
		public ActionResult ILP(Int64 id)
		{
			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				var repo = uow.GetRepo<IGoodServiceRepo>();
				GoodService dbEntity = repo.Get(id);

				return RedirectToAction("_ILP", "ILP", new {studentPacketCourseId = dbEntity.StudentPacketCourseId});
			}
		}

		[FPCSAuthorize(Role.Admin)]
		public ActionResult GoodService(Int64 id)
		{
			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				var repo = uow.GetRepo<IGoodServiceRepo>();
				GoodService dbEntity = repo.Get(id);

				return RedirectToAction("__UniversalGoodService", "GoodAndService",
					new
					{
						studentPacketId = dbEntity.StudentPacketId.HasValue ? dbEntity.StudentPacketId.Value : 0,
						studentPacketCourseId = dbEntity.StudentPacketCourseId.HasValue ? dbEntity.StudentPacketCourseId.Value : 0,
						courseId = 0,
						goodServiceId = id
					});
			}
		}

		[FPCSAuthorize(Role.Admin)]
		public ActionResult _TableVendorService(Int64 vendorId)
		{
			TableVendorServiceModel model = new TableVendorServiceModel(vendorId);

			return PartialView(model);
		}

		[FPCSAuthorize(Role.Guardian, Role.Teacher, Role.Admin)]
		[HttpPost]
		public ActionResult _Print(String paramsIds, Int64? studentPacketId, GridOptions options,
			GoodAndServiceApprovalListOptions listOptions)
		{
			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				Int64[] ids;
				if (!String.IsNullOrEmpty(paramsIds))
				{
					ids = new JavaScriptSerializer().Deserialize<Int64[]>(paramsIds);
				}
				else
				{
					Int64 _studentPacketId = studentPacketId.GetValueOrDefault(0);
					ids = uow.GetRepo<IGoodServiceRepo>().GetByStudentPacket(_studentPacketId).Select(x => x.GoodServiceId).ToArray();
				}

				List<ApprovalModel> data = GetData(uow, options, listOptions)
					.Where(x => ids.Contains(x.GoodAndServiceId))
					.ToList()
					.Select(x => new ApprovalModel()
					{
						GoodAndServiceId = x.GoodAndServiceId,
						StudentName = x.StudentsLastName + ", " + x.StudentsFirstName + " ( " + x.Grade + " ) ",
						VendorName = x.VendorName,
						RequisitionOrReimbursements =
							x.RequisitionOrReimbursement == RequisitionOrReimbursement.Requisition ? "Requisition" : "Reimbursement",
						TypeRequisitionReimbursement = x.TypeRequisitionReimbursement.GetDescription(),
						Title = x.Title,
						Description = x.Description,
						PublisherISBN = x.PublisherISBN,
						Comments = x.Comments,
						FamilyEmail = x.FamilyEmail,
						FamilyPhone = x.FamilyPhone,
						DateOfBirth = x.DateOfBirth != DateTimeOffset.MinValue ? x.DateOfBirth.ToString("yyyy/MM/dd") : "",
						NumberOfUnits = x.NumberOfUnits.ToString(),
						UnitPrice = x.UnitPrice.ToString(),
						Shipping = x.ShippingHandlingFees.ToString(),
						Total = (x.NumberOfUnits*x.UnitPrice + x.ShippingHandlingFees).ToString(),
                        IsWithdrawal = x.IsWithdrawal
					})
					.ToList();

				TableGoodServicePrintModel model = new TableGoodServicePrintModel();
				model.Data = data;

				model.PrintAddress = (ConfigurationManager.AppSettings["PrintAddress"]).ToString();
				model.PrintState = (ConfigurationManager.AppSettings["PrintState"]).ToString();
				model.PrintPhone = (ConfigurationManager.AppSettings["PrintPhone"]).ToString();
				model.PrintFax = (ConfigurationManager.AppSettings["PrintFax"]).ToString();
				model.PrintDateNow = DateTime.Now.ToString("MM/dd/yyyy");

				return PartialView(model);
			}
		}

		[FPCSAuthorize(Role.Admin)]
		[HttpPost]
		public ActionResult _PrintVendor()
		{
			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				var repoGS = uow.GetRepo<IGoodServiceRepo>();
				var repoGSB = uow.GetRepo<IGoodServiceBalanceRepo>();

				List<GoodAndServiceVendorModel> data = repoGS.GetAll()
					.Select(x => new
					{
						x.VendorId,
						x.Vendor.BusinessName,
						x.Vendor.MailingAddress,
						x.NumberOfUnits,
						x.UnitPrice,
						x.ShippingHandlingFees,
						x.GoodServiceBalances
					})
					.ToList()
					.GroupBy(x => new
					{
						VendorId = x.VendorId,
						VendorName = x.BusinessName,
						Address = x.MailingAddress
					})
					.Select(x => new GoodAndServiceVendorModel()
					{
						VendorId = x.Key.VendorId,
						VendorName = x.Key.VendorName,
						Address = x.Key.Address,
						Sum = x.Sum(t => t.NumberOfUnits*t.UnitPrice + t.ShippingHandlingFees),
						SumBalance = x.Sum(t => t.GoodServiceBalances.Sum(h => h.QTY*h.UnitPrice + h.Shipping))
					})
					.ToList();

				TableGoodServiceVendorPrintModel model = new TableGoodServiceVendorPrintModel();
				model.Data = data;

				model.PrintAddress = (ConfigurationManager.AppSettings["PrintAddress"]).ToString();
				model.PrintState = (ConfigurationManager.AppSettings["PrintState"]).ToString();
				model.PrintPhone = (ConfigurationManager.AppSettings["PrintPhone"]).ToString();
				model.PrintFax = (ConfigurationManager.AppSettings["PrintFax"]).ToString();
				model.PrintDateNow = DateTime.Now.ToString("MM/dd/yyyy");

				return PartialView(model);
			}
		}

		[FPCSAuthorize(Role.Admin)]
		public ActionResult _Print2(GridOptions options, GoodAndServiceApprovalListOptions listOptions)
		{
			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				List<GoodAndServiceApprovalModel> data = GetData(uow, options, listOptions)
					.ToList()
					.Select(x => new GoodAndServiceApprovalModel()
					{
						GoodAndServiceId = x.GoodAndServiceId,
						GoodOrService = x.GoodOrService.GetDescription(),
						RequisitionOrReimbursement = x.RequisitionOrReimbursement.GetDescription(),
						StudentsName = String.Format(@"{0} {1}", x.StudentsLastName, x.StudentsFirstName),
						VendorName = x.VendorName,
						ItemNumber = x.ItemNumber,
						UserRequest = x.UserRequest,
						DateRequest = x.DateRequest.HasValue ? x.DateRequest.Value.ToString("MM/dd/yyyy") : String.Empty,
						Budget =
							//x.TypeRequisitionReimbursement == TypeRequisitionReimbursement.Administrative
							//? x.NumberOfUnits*x.UnitPrice + x.ShippingHandlingFees
							//: 
							x.TypeRequisitionReimbursement == TypeRequisitionReimbursement.ASDClass
								? x.NumberOfSemester*x.CostPerSemester
								: x.TypeRequisitionReimbursement == TypeRequisitionReimbursement.UniversityCourseIndStudyClass
									? x.NumberOfUnits*x.UnitPrice
									: x.TypeRequisitionReimbursement == TypeRequisitionReimbursement.VendorService
										? x.NumberOfUnits*x.UnitPrice
										: x.TypeRequisitionReimbursement == TypeRequisitionReimbursement.Supplies
											? x.NumberOfUnits*x.UnitPrice + x.ShippingHandlingFees
											: x.TypeRequisitionReimbursement == TypeRequisitionReimbursement.FPCSClass
												? x.NumberOfUnits*x.UnitPrice + x.ShippingHandlingFees
												: x.TypeRequisitionReimbursement == TypeRequisitionReimbursement.TextbookCurriculum
													? x.NumberOfUnits*x.UnitPrice + x.ShippingHandlingFees
													: 0,
						Balance =
							x.TypeRequisitionReimbursement == TypeRequisitionReimbursement.ASDClass
								? (x.NumberOfSemester*x.CostPerSemester) - x.GoodServiceBalances.Sum(t => t.UnitPrice*t.QTY + t.Shipping)
								: x.TypeRequisitionReimbursement == TypeRequisitionReimbursement.UniversityCourseIndStudyClass
									? (x.NumberOfUnits*x.UnitPrice) - x.GoodServiceBalances.Sum(t => t.UnitPrice*t.QTY + t.Shipping)
									: x.TypeRequisitionReimbursement == TypeRequisitionReimbursement.VendorService
										? (x.NumberOfUnits*x.UnitPrice) - x.GoodServiceBalances.Sum(t => t.UnitPrice*t.QTY + t.Shipping)
										: x.TypeRequisitionReimbursement == TypeRequisitionReimbursement.Supplies
											? (x.NumberOfUnits*x.UnitPrice + x.ShippingHandlingFees) -
											  x.GoodServiceBalances.Sum(t => t.UnitPrice*t.QTY + t.Shipping)
											: x.TypeRequisitionReimbursement == TypeRequisitionReimbursement.FPCSClass
												? (x.NumberOfUnits*x.UnitPrice + x.ShippingHandlingFees) -
												  x.GoodServiceBalances.Sum(t => t.UnitPrice*t.QTY + t.Shipping)
												: x.TypeRequisitionReimbursement == TypeRequisitionReimbursement.TextbookCurriculum
													? (x.NumberOfUnits*x.UnitPrice + x.ShippingHandlingFees) -
													  x.GoodServiceBalances.Sum(t => t.UnitPrice*t.QTY + t.Shipping)
													: 0,
						IsClosed = x.IsClosed,
						CourseStatus = x.CourseStatus.HasValue && x.CourseStatus.Value == Sign.Sign
							? x.DateCourseStatus.Value.ToShortDateString()
							: x.CourseStatus.HasValue && x.CourseStatus.Value != Sign.Sign
								? x.CourseStatus.Value.GetDescription()
								: "not signed",
						GoodOrServiceApprovedText = x.Aprv.GetDescription(),
						CommentsRejectReason = x.CommentsRejectReason
					})
					.ToList();

				TableGoodServicePrintModel model = new TableGoodServicePrintModel();
				//model.Data = data;

				model.PrintAddress = (ConfigurationManager.AppSettings["PrintAddress"]).ToString();
				model.PrintState = (ConfigurationManager.AppSettings["PrintState"]).ToString();
				model.PrintPhone = (ConfigurationManager.AppSettings["PrintPhone"]).ToString();
				model.PrintFax = (ConfigurationManager.AppSettings["PrintFax"]).ToString();
				model.PrintDateNow = DateTime.Now.ToString("MM/dd/yyyy");

				return PartialView(model);
			}
		}

		[FPCSAuthorize(Role.Admin)]
		public ActionResult _PrintReimbursement(Int64 id)
		{
			try
			{
				var printAddress = (ConfigurationManager.AppSettings["PrintAddress"]);
				var printState = (ConfigurationManager.AppSettings["PrintState"]);
				var printPhone = (ConfigurationManager.AppSettings["PrintPhone"]);
				var printFax = (ConfigurationManager.AppSettings["PrintFax"]);
				var printDateNow = DateTime.Now.ToString("MM/dd/yyyy");

				var model = new ListPrintReimbursementModel();
				model.PrintReimbursements = new List<PrintReimbursementModel>();

				using (var uow = UnityManager.Resolve<IUnitOfWork>())
				{
					var data = uow.GetRepo<IGoodServiceRepo>()
						.GetAsQuery(id, true)
						.Select(x => new
						{
							x.GoodServiceId,
							VendorName = x.Vendor.BusinessName,
							Item = x.Title,
							BudgetedAmount = x.UnitPrice*x.NumberOfUnits + x.ShippingHandlingFees,
							RequestedAmount = 0,
							Course = x.StudentPacketCourse.FPCSCourse.Name,
							StudentId = x.StudentPacketCourse.StudentPacket.StudentId,
							StudentName =
								x.StudentPacketCourse.StudentPacket.Student.LastName + ", " +
								x.StudentPacketCourse.StudentPacket.Student.FirstName,
                            IsWithdrawal = x.StudentPacketCourse.StudentPacket.Student.WithdrawalDate.HasValue ? true : false,
							ObjCD =
								x.GoodOrService == GoodOrService.Good &&
								x.TypeRequisitionReimbursement == TypeRequisitionReimbursement.VendorService
									? "3030"
									: x.GoodOrService == GoodOrService.Service &&
									  x.TypeRequisitionReimbursement == TypeRequisitionReimbursement.TextbookCurriculum
										? "4020"
										: string.Empty
						})
						.FirstOrDefault();

					if (data == null)
						return PartialView(model);

					var guardians = uow.GetRepo<IFamilyRepo>().GetGuardiansByStudent(data.StudentId).ToList();

					var groupVendor = data.VendorName;

					var itemModel = new PrintReimbursementModel();

					itemModel.Reimbursements = new List<ReimbursementModel>();
					itemModel.Reimbursements.Add(
						new ReimbursementModel
						{
							GoodServiceId = data.GoodServiceId,
							Item = data.Item,
							BudgetedAmount = data.BudgetedAmount,
							RequestedAmount = data.RequestedAmount,
							Course = data.Course,
							StudentName = data.StudentName,
							ObjCD = data.ObjCD,
                            IsWithdrawal = data.IsWithdrawal
						});

					itemModel.VendorName = groupVendor;
					itemModel.PrintAddress = printAddress;
					itemModel.PrintState = printState;
					itemModel.PrintPhone = printPhone;
					itemModel.PrintFax = printFax;
					itemModel.PrintDateNow = printDateNow;

					itemModel.Total = itemModel.Reimbursements.Sum(x => x.BudgetedAmount);

					if (guardians.Count >= 2)
					{
						itemModel.Title = string.Format(@"{0} : {1} & {2}", guardians.FirstOrDefault().LastName,
							guardians.FirstOrDefault().FirstName, guardians.LastOrDefault().FirstName);
					}
					else if (guardians != null && guardians.Count == 1)
					{
						itemModel.Title = string.Format(@"{0}, {1}", guardians.FirstOrDefault().LastName,
							guardians.FirstOrDefault().FirstName);
					}
					else
					{
						itemModel.Title = string.Empty;
					}

					model.PrintReimbursements.Add(itemModel);
				}

				return PartialView(model);
			}
			catch (Exception ex)
			{
				return Error(ex.Message);
			}
		}

		[FPCSAuthorize(Role.Admin)]
		public ActionResult DoMultiAction(String paramsIds, MultiAction action)
		{
			var ids = new JavaScriptSerializer().Deserialize<Int64[]>(paramsIds);
			using (var uow = UnityManager.Resolve<IUnitOfWork>())
			{
				var repo = uow.GetRepo<IGoodServiceRepo>();

				var data = repo.GetByIds(ids.ToList());

				foreach (var item in data)
				{
					switch (action)
					{
						case MultiAction.Approve:
							item.GoodOrServiceApproved = GoodOrServiceApproved.b_appr;
							break;
						case MultiAction.Pending:
							item.GoodOrServiceApproved = GoodOrServiceApproved.b_pend;
							break;
						case MultiAction.Reject:
							item.GoodOrServiceApproved = GoodOrServiceApproved.b_rejc;
							break;
						case MultiAction.Close:
							item.IsClosed = true;
							break;
						default:
							break;
					}

					repo.Update(item);
				}

				uow.Commit();
			}

			return Json("Ok", JsonRequestBehavior.AllowGet);
		}

		private IQueryable<GoodAndServiceDataModel> GetData(IUnitOfWork uow, GridOptions options,
			GoodAndServiceApprovalListOptions listOptions)
		{
			Int32 currentSchoolYearId = Session.GetSchoolYear().SchoolYearId;

			var repo = uow.GetRepo<IGoodServiceRepo>();

			var dbList = repo.GetAll()
				.Where(x => ((x.StudentPacketCourseId.HasValue &&
				              !x.StudentPacketCourse.IsDeleted &&
				              x.StudentPacketCourse.StudentPacket.SchoolYear.SchoolYearId == currentSchoolYearId) ||
				             (x.StudentPacketId.HasValue &&
				              !x.StudentPacket.IsDeleted &&
				              x.StudentPacket.SchoolYear.SchoolYearId == currentSchoolYearId)) &&
				            (!listOptions.GoodOrService.HasValue ||
				             (listOptions.GoodOrService.HasValue && listOptions.GoodOrService.Value == 0 &&
				              x.GoodOrService == GoodOrService.Good) ||
				             (listOptions.GoodOrService.HasValue && listOptions.GoodOrService.Value == 1 &&
				              x.GoodOrService == GoodOrService.Service)) &&
				            (!listOptions.RequisitionOrReimbursement.HasValue ||
				             (listOptions.RequisitionOrReimbursement.HasValue && listOptions.RequisitionOrReimbursement.Value == 0 &&
				              x.RequisitionOrReimbursement == RequisitionOrReimbursement.Requisition) ||
				             (listOptions.RequisitionOrReimbursement.HasValue && listOptions.RequisitionOrReimbursement.Value == 1 &&
				              x.RequisitionOrReimbursement == RequisitionOrReimbursement.Reimbursement)) &&
				            (!listOptions.IsClosed.HasValue ||
				             (listOptions.IsClosed.HasValue && listOptions.IsClosed.Value == 1 && x.IsClosed == true) ||
				             (listOptions.IsClosed.HasValue && listOptions.IsClosed.Value == 0 && (x.IsClosed == false ||
				                                                                                   !x.IsClosed.HasValue))) &&
				            (!listOptions.CourseStatus.HasValue ||
				             (listOptions.CourseStatus.HasValue && listOptions.CourseStatus.Value == 0 &&
				              x.StudentPacketCourse.AdminSignature.HasValue &&
				              x.StudentPacketCourse.AdminSignature.Value == Sign.Sign) ||
				             (listOptions.CourseStatus.HasValue && listOptions.CourseStatus.Value == 1 &&
				              !x.StudentPacketCourse.AdminSignature.HasValue) ||
				             (listOptions.CourseStatus.HasValue && listOptions.CourseStatus.Value == 2 &&
				              x.StudentPacketCourse.AdminSignature.HasValue &&
				              x.StudentPacketCourse.AdminSignature == Sign.MustAmend) ||
				             (listOptions.CourseStatus.HasValue && listOptions.CourseStatus.Value == 3 &&
				              x.StudentPacketCourse.AdminSignature.HasValue && x.StudentPacketCourse.AdminSignature == Sign.Rejected)) &&
				            (!listOptions.GoodOrServiceApprovedText.HasValue ||
				             (listOptions.GoodOrServiceApprovedText.HasValue && listOptions.GoodOrServiceApprovedText.Value == 0 &&
				              x.GoodOrServiceApproved == GoodOrServiceApproved.b_pend) ||
				             (listOptions.GoodOrServiceApprovedText.HasValue && listOptions.GoodOrServiceApprovedText.Value == 1 &&
				              (x.GoodOrServiceApproved == GoodOrServiceApproved.b_appr || x.GoodOrServiceApproved == null)) ||
				             (listOptions.GoodOrServiceApprovedText.HasValue && listOptions.GoodOrServiceApprovedText.Value == 2 &&
				              x.GoodOrServiceApproved == GoodOrServiceApproved.b_rejc)) &&
				            (listOptions.StudentsName == null ||
				             (listOptions.StudentsName != null &&
				              (x.StudentPacketCourse.StudentPacket.Student.LastName.Contains(listOptions.StudentsName) ||
				               x.StudentPacket.Student.LastName.Contains(listOptions.StudentsName) ||
				               x.StudentPacketCourse.StudentPacket.Student.FirstName.Contains(listOptions.StudentsName) ||
				               x.StudentPacket.Student.FirstName.Contains(listOptions.StudentsName)))) &&
				            (!listOptions.SponsorSignature.HasValue ||
				             (listOptions.SponsorSignature.HasValue && listOptions.SponsorSignature.Value == 0 &&
				              (x.StudentPacketCourse.StudentPacket.SponsorSignatureSem1 == Sign.MustAmend ||
				               x.StudentPacketCourse.StudentPacket.SponsorSignatureSem1 == Sign.Rejected ||
				               !x.StudentPacketCourse.StudentPacket.SponsorSignatureSem1.HasValue)) ||
				             (listOptions.SponsorSignature.HasValue && listOptions.SponsorSignature.Value == 1 &&
				              x.StudentPacketCourse.StudentPacket.SponsorSignatureSem1 == Sign.Sign)) &&
				            (!listOptions.GuardianSignature.HasValue ||
				             (listOptions.GuardianSignature.HasValue && listOptions.GuardianSignature.Value == 0 &&
				              (x.StudentPacketCourse.StudentPacket.SponsorSignatureSem1 == Sign.MustAmend ||
				               x.StudentPacketCourse.StudentPacket.SponsorSignatureSem1 == Sign.Rejected ||
				               !x.StudentPacketCourse.StudentPacket.SponsorSignatureSem1.HasValue)) ||
				             (listOptions.GuardianSignature.HasValue && listOptions.GuardianSignature.Value == 1 &&
				              x.StudentPacketCourse.StudentPacket.SponsorSignatureSem1 == Sign.Sign)) &&
				            (!listOptions.ItemNumber.HasValue ||
				             (listOptions.ItemNumber.HasValue &&
				              x.GoodServiceId.Equals(listOptions.ItemNumber.Value))) &&
				            (listOptions.VendorName == null ||
				             (listOptions.VendorName != null && x.Vendor.BusinessName.StartsWith(listOptions.VendorName)))
				)
				.Select(x => new GoodAndServiceDataModel()
				{
					GoodAndServiceId = x.GoodServiceId,
					GoodOrService = x.GoodOrService,
					RequisitionOrReimbursement = x.RequisitionOrReimbursement,
					StudentsLastName = x.StudentPacketCourseId.HasValue
						? x.StudentPacketCourse.StudentPacket.Student.LastName
						: x.StudentPacketId.HasValue ? x.StudentPacket.Student.LastName : String.Empty,
					StudentsFirstName = x.StudentPacketCourseId.HasValue
						? x.StudentPacketCourse.StudentPacket.Student.FirstName
						: x.StudentPacketId.HasValue ? x.StudentPacket.Student.FirstName : String.Empty,
					Grade = x.StudentPacketCourseId.HasValue
						? x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.K
							? "K"
							: x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.G1
								? "G1"
								: x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.G2
									? "G2"
									: x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.G3
										? "G3"
										: x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.G4
											? "G4"
											: x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.G5
												? "G5"
												: x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.G6
													? "G6"
													: x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.G7
														? "G7"
														: x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.G8
															? "G8"
															: x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.G9
																? "G9"
																: x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.G10
																	? "G10"
																	: x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.G11
																		? "G11"
																		: x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.G12
																			? "G12"
																			: x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.G ? "G" : ""
						: x.StudentPacketId.HasValue
							? x.StudentPacket.Student.Grade == Grade.K
								? "K"
								: x.StudentPacket.Student.Grade == Grade.G1
									? "G1"
									: x.StudentPacket.Student.Grade == Grade.G2
										? "G2"
										: x.StudentPacket.Student.Grade == Grade.G3
											? "G3"
											: x.StudentPacket.Student.Grade == Grade.G4
												? "G4"
												: x.StudentPacket.Student.Grade == Grade.G5
													? "G5"
													: x.StudentPacket.Student.Grade == Grade.G6
														? "G6"
														: x.StudentPacket.Student.Grade == Grade.G7
															? "G7"
															: x.StudentPacket.Student.Grade == Grade.G8
																? "G8"
																: x.StudentPacket.Student.Grade == Grade.G9
																	? "G9"
																	: x.StudentPacket.Student.Grade == Grade.G10
																		? "G10"
																		: x.StudentPacket.Student.Grade == Grade.G11
																			? "G11"
																			: x.StudentPacket.Student.Grade == Grade.G12
																				? "G12"
																				: x.StudentPacket.Student.Grade == Grade.G ? "G" : ""
							: "",
					VendorName = x.Vendor.BusinessName,
					ItemNumber = x.GoodServiceId,
					NumberOfUnits = x.NumberOfUnits,
					UnitPrice = x.UnitPrice,
					ShippingHandlingFees = x.ShippingHandlingFees,
					CostPerSemester = x.CostPerSemester,
					NumberOfSemester = x.NumberSemesters,
					TypeRequisitionReimbursement = x.TypeRequisitionReimbursement,
					Title = x.Title,
					PublisherISBN = x.PublisherISBN,
					Description = x.Description,
					Comments = x.AdminComments,
					IsClosed = x.IsClosed.HasValue ? x.IsClosed.Value : false,
					CourseStatus = x.StudentPacketCourse.AdminSignature,
					DateCourseStatus = x.StudentPacketCourse.DateAdminSignature,
					Aprv = x.GoodOrServiceApproved.HasValue ? x.GoodOrServiceApproved.Value : GoodOrServiceApproved.b_appr,
					CommentsRejectReason = x.CommentsRejectReason,
					GoodServiceBalances = x.GoodServiceBalances,
					CourseTitle = x.CourseTitle,
					Subject = x.StudentPacketCourseId.HasValue
						? x.StudentPacketCourse.FPCSCourse.Subject.Name
						: x.FPCSCourseId.HasValue
							? x.FPCSCourse.Subject.Name
							: String.Empty,
					Category = "",
					FamilyNumber = x.StudentPacketCourseId.HasValue
						? x.StudentPacketCourse
							.StudentPacket
							.Student
							.Families
							.Where(t => !t.IsDeleted)
							.Select(t => t.FamilyId)
							.FirstOrDefault()
						: 0,
					FamilyEmail = x.StudentPacketCourseId.HasValue
						? x.StudentPacketCourse
							.StudentPacket
							.Student
							.Families
							.Where(t => !t.IsDeleted)
							.Select(t => t.Email)
							.FirstOrDefault()
						: "",
					FamilyPhone = x.StudentPacketCourseId.HasValue
						? x.StudentPacketCourse
							.StudentPacket
							.Student
							.Families
							.Where(t => !t.IsDeleted)
							.Select(t => t.CellPhone)
							.FirstOrDefault()
						: "",
					ClassType = x.ClassType,
					CourseNumber = x.CourseNumber,
					DateOfBirth = x.StudentPacketCourseId.HasValue
						? x.StudentPacketCourse.StudentPacket.Student.DateOfBirth
						: DateTimeOffset.MinValue,
					Credits = x.Credits,
					UserRequest = x.UserRequestId.HasValue ? x.UserRequest.LastName + ", " + x.UserRequest.FirstName : String.Empty,
					DateRequest = x.DateRequest,
					DateGuardianSignature = x.StudentPacketCourse.StudentPacket.DateGuardianSignatureSem1,
					DateSponsorSignature = x.StudentPacketCourse.StudentPacket.DateSponsorSignatureSem1,
					GuardianSignature = x.StudentPacketCourse.StudentPacket.GuardianSignatureSem1,
					SponsorSignature = x.StudentPacketCourse.StudentPacket.SponsorSignatureSem1,
					SponsorTeacherEmail = x.StudentPacketCourse.StudentPacket.SponsorTeacher.Email,
                    IsWithdrawal = x.StudentPacketCourseId.HasValue
                        ? x.StudentPacketCourse.StudentPacket.Student.WithdrawalDate.HasValue ? true : false
                        : x.StudentPacketId.HasValue ? x.StudentPacket.Student.WithdrawalDate.HasValue ? true : false : false,
                    DateCreated = x.CreatedDate,
                    Semester = x.StudentPacketCourse.FPCSCourseId != 0
						? x.StudentPacketCourse.FPCSCourse.Semester : Semester.Summer
				});

			return dbList;
		}

		public class Params
		{
			public List<Int64> ids { get; set; }
		}

		public enum MultiAction
		{
			Approve = 1,
			Pending = 2,
			Reject = 3,
			Close = 4
		}
	}
}