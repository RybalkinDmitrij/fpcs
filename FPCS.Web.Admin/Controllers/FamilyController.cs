﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Collections.Generic;

using FPCS.Data;
using FPCS.Data.Repo;
using FPCS.Data.Enums;
using FPCS.Web.Admin.Code;
using FPCS.Web.Admin.Models.Family;
using FPCS.Core.jqGrid;
using FPCS.Data.Entities;
using System.Configuration;
using System.Web.Script.Serialization;
using FPCS.Web.Admin.Models.GoodAndServiceApproval;
using ListPrintReimbursementModel = FPCS.Web.Admin.Models.Family.ListPrintReimbursementModel;
using PrintReimbursementModel = FPCS.Web.Admin.Models.Family.PrintReimbursementModel;
using ReimbursementModel = FPCS.Web.Admin.Models.Family.ReimbursementModel;

namespace FPCS.Web.Admin.Controllers
{
    public class FamilyController : BaseController
    {
        [FPCSAuthorize(Role.Admin, Role.Teacher)]
        public ActionResult Index()
        {
            FamilyIndexModel model = new FamilyIndexModel();
            model.Init(SchoolYear.SchoolYearId);

            return View(model);
        }

        [FPCSAuthorize(Role.Admin, Role.Teacher)]
        public JsonResult _Index(GridOptions options, FamilyListOptions familyListOptions, Int64? familyId)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IFamilyRepo>();
                IQueryable<Family> dbList;

                if (familyId.HasValue && familyId.Value != 0)
                {
                    dbList = repo.GetAsQuery(familyId.Value);
                }
                else
                {
                    dbList = repo.GetAll(SchoolYear.SchoolYearId);
                }

                var engine = new GridDynamicEngine(options, familyListOptions);
                var result = engine.ApplyAll(dbList, x => new FamilyMiniModel
                {
                    FamilyId = x.FamilyId,
                    FamilyName = x.Name,
                    FirstNames = String.Empty,
                    Telephone = x.Telephone,
                    Email = x.Email,
                });

                var guardians = uow.GetRepo<IGuardianRepo>()
                                              .GetByFamilies(SchoolYear.SchoolYearId, result.rows.Select(t => t.FamilyId).ToList())
                                              .Select(x => new
                                              {
                                                  GuardianId = x.DbUserId,
                                                  GuardianFirstName = x.FirstName,
                                                  GuardianLastName = x.LastName,
                                                  FamilyIds = x.Families.Where(t => !t.IsDeleted).Select(t => t.FamilyId)
                                              })
                                              .ToList();

                foreach (var item in result.rows)
                {
                    var names = guardians.Where(x => x.FamilyIds.Contains(item.FamilyId)).Select(x => new { x.GuardianLastName, x.GuardianFirstName }).ToList();
                    var lastNames = names.Select(x => x.GuardianLastName).Distinct();

                    if (lastNames.Count() == 1)
                    {
                        item.FirstNames = names[0].GuardianLastName + ", " + String.Join(" & ", names.Select(x => x.GuardianFirstName));
                    }
                    else
                    {
                        item.FirstNames = String.Join(" - ", lastNames) + ", " + String.Join(" & ", names.Select(x => x.GuardianFirstName));
                    }
                }

                return Json(result);
            }
        }

        [FPCSAuthorize(Role.Admin, Role.Teacher)]
        [HttpGet]
        public ActionResult Create()
        {
            var model = new FamilyCreateModel();
            model.Init();
            model.FamilyGuardians = new List<LightGuardianModel>();
            model.FamilyStudents = new List<LightStudentModel>();
            return View(model);
        }

        [FPCSAuthorize(Role.Admin, Role.Teacher)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(FamilyCreateModel model)
        {
            if (model.FamilyGuardians == null) model.FamilyGuardians = new List<LightGuardianModel>();
            if (model.FamilyStudents == null) model.FamilyStudents = new List<LightStudentModel>();

            if (ModelState.IsValid)
            {
                try
                {
                    using (var uow = UnityManager.Resolve<IUnitOfWork>())
                    {
                        String familyName = String.Empty;
                        if (model.FamilyGuardians.Count() != 0)
                        {
                            var repoDbUser = uow.GetRepo<IDbUserRepo>();
                            var guardianIds = model.FamilyGuardians.Select(t => t.DbUserId).ToList();
                            var guardians = repoDbUser.GetAll().Where(x => guardianIds.Contains(x.DbUserId)).ToList();

                            var names = guardians.Select(x => new { x.LastName, x.FirstName }).ToList();
                            var lastNames = names.Select(x => x.LastName).Distinct();

                            if (lastNames.Count() == 1) familyName = names[0].LastName;
                            else familyName = String.Join(" - ", lastNames);
                        }

                        var repo = uow.GetRepo<IFamilyRepo>();
                        var family = repo.Add(familyName, model.Description, model.Address, model.MailingAddress, model.Telephone, model.CellPhone, model.City,
                             model.Country, model.Zip, model.Email, model.Password, model.IsUseDirectory, model.StateId, SchoolYear.SchoolYearId);

                        uow.Commit();

                        family = repo.UpdateGuardians(family, model.FamilyGuardians.Select(x => x.DbUserId));
                        family = repo.UpdateStudents(family, model.FamilyStudents.Select(x => x.DbUserId));
                        uow.Commit();

                        return RedirectToAction("Index");
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }
            }

            model.Init();
            return View("Create", model);
        }

        [FPCSAuthorize(Role.Admin, Role.Teacher)]
        [HttpGet]
        public ActionResult Edit(Int64 id)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var model = uow.GetRepo<IFamilyRepo>().GetAsQuery(id)
                    .Select(x => new FamilyEditModel
                    {
                        FamilyId = x.FamilyId,
                        Name = x.Name,
                        Description = x.Description,
                        Email = x.Email,
                        Password = x.Password,
                        Telephone = x.Telephone,
                        CellPhone = x.CellPhone,
                        Address = x.Address,
                        MailingAddress = x.MailingAddress,
                        City = x.City,
                        StateId = x.StateId,
                        Country = x.Country,
                        Zip = x.Zip,
                        IsUseDirectory = x.IsUseDirectory,
                        FamilyGuardians = x.Guardians
                            .Select(x2 => new LightGuardianModel { DbUserId = x2.DbUserId, FullName = x2.FullName, CellPhone = x2.CellPhone }),
                        FamilyStudents = x.Students
                            .Select(x3 => new LightStudentModel { DbUserId = x3.DbUserId, FullName = x3.FullName, GradYear = x3.GradYear, IsWithdrawal = x3.WithdrawalDate.HasValue ? true : false})
                    }).FirstOrDefault();

                if (model == null) return Error("Family {0} not found", id);

                model.Init();

                return View(model);
            }
        }

        [FPCSAuthorize(Role.Admin, Role.Teacher)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(FamilyEditModel model)
        {
            if (model.FamilyGuardians == null) model.FamilyGuardians = new List<LightGuardianModel>();
            if (model.FamilyStudents == null) model.FamilyStudents = new List<LightStudentModel>();

            if (ModelState.IsValid)
            {
                try
                {
                    using (var uow = UnityManager.Resolve<IUnitOfWork>())
                    {
                        String familyName = String.Empty;
                        if (model.FamilyGuardians.Count() != 0)
                        {
                            var repoDbUser = uow.GetRepo<IDbUserRepo>();
                            var guardianIds = model.FamilyGuardians.Select(t => t.DbUserId).ToList();
                            var guardians = repoDbUser.GetAll().Where(x => guardianIds.Contains(x.DbUserId)).ToList();

                            var names = guardians.Select(x => new { x.LastName, x.FirstName }).ToList();
                            var lastNames = names.Select(x => x.LastName).Distinct();

                            if (lastNames.Count() == 1) familyName = names[0].LastName;
                            else familyName = String.Join(" - ", lastNames);
                        }

                        var repo = uow.GetRepo<IFamilyRepo>();
                        var family = repo.Update(model.FamilyId, familyName, model.Description, model.Address, model.MailingAddress, model.Telephone, model.CellPhone,
                            model.City, model.Country, model.Zip, model.Email, model.Password, model.IsUseDirectory, model.StateId);

                        uow.Commit();

                        family = repo.UpdateGuardians(family, model.FamilyGuardians.Select(x => x.DbUserId));
                        family = repo.UpdateStudents(family, model.FamilyStudents.Select(x => x.DbUserId));
                        uow.Commit();

                        return RedirectToAction("Index");
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }
            }

            model.Init();
            return View(model);
        }

        //public ActionResult RebindFamily()
        //{
        //    using (var uow = UnityManager.Resolve<IUnitOfWork>())
        //    {
        //        var repo = uow.GetRepo<IFamilyRepo>();

        //        var data = repo.GetAll().ToList();

        //        foreach (var item in data)
        //        {
        //            var familyGuardians = repo.GetGuardiansByFamily(item.FamilyId).ToList();

        //            String familyName = String.Empty;
        //            if (familyGuardians.Count() != 0)
        //            {
        //                var repoDbUser = uow.GetRepo<IDbUserRepo>();
        //                var guardianIds = familyGuardians.Select(t => t.DbUserId).ToList();
        //                var guardians = repoDbUser.GetAll().Where(x => guardianIds.Contains(x.DbUserId)).ToList();

        //                var names = guardians.Select(x => new { x.LastName, x.FirstName }).ToList();
        //                var lastNames = names.Select(x => x.LastName).Distinct();

        //                if (lastNames.Count() == 1) familyName = names[0].LastName;
        //                else familyName = String.Join(" - ", lastNames);
        //            }

        //            var family = repo.Update(item.FamilyId, familyName, item.Description, item.Address, item.MailingAddress, item.Telephone, item.CellPhone,
        //                    item.City, item.Country, item.Zip, item.Email, item.Password, item.IsUseDirectory, item.StateId);

        //            uow.Commit();
        //        }

        //        return Json("Ok", JsonRequestBehavior.AllowGet);
        //    }
        //}

        [FPCSAuthorize(Role.Admin, Role.Teacher)]
        [HttpGet]
        public PartialViewResult _FamilyGuardianPartial(Guid id)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var dbEntity = uow.GetRepo<IGuardianRepo>().Get(id);
                if (dbEntity == null) return ErrorPartial("Guardian {0} not found", id);

                var model = new LightGuardianModel
                {
                    DbUserId = dbEntity.DbUserId,
                    FullName = dbEntity.FullName,
                    CellPhone = dbEntity.CellPhone
                };
                return PartialView(model);
            }
        }

        [FPCSAuthorize(Role.Admin, Role.Teacher)]
        [HttpGet]
        public PartialViewResult _FamilyStudentPartial(Guid id)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var dbEntity = uow.GetRepo<IStudentRepo>().Get(id);
                if (dbEntity == null) return ErrorPartial("Student {0} not found", id);

                var model = new LightStudentModel
                {
                    DbUserId = dbEntity.DbUserId,
                    FullName = dbEntity.FullName,
                    GradYear = dbEntity.GradYear,
                    IsWithdrawal = dbEntity.WithdrawalDate.HasValue ? true : false
                };
                return PartialView(model);
            }
        }

        [FPCSAuthorize(Role.Admin, Role.Teacher)]
        [HttpPost]
        public ActionResult Delete(Int64 id)
        {
            try
            {
                using (var uow = UnityManager.Resolve<IUnitOfWork>())
                {
                    var repo = uow.GetRepo<IFamilyRepo>();
                    repo.Remove(id);

                    uow.Commit();
                    return JsonRes();
                }
            }
            catch (Exception ex)
            {
                return JsonRes(Status.Error, ex.Message);
            }
        }

        [FPCSAuthorize(Role.Admin, Role.Teacher)]
        [HttpPost]
        public ActionResult DeleteAll(String id)
        {
            try
            {
                if (String.IsNullOrEmpty(id)) return JsonRes(Status.Error, "Not correct ids");

                var ids = id.Split(',');
                using (var uow = UnityManager.Resolve<IUnitOfWork>())
                {
                    Int32 errorsCount = 0;
                    var repo = uow.GetRepo<IFamilyRepo>();
                    foreach (var item in ids)
                    {
                        try
                        {
                            repo.Remove(Int64.Parse(item));
                        }
                        catch (Exception)
                        {
                            errorsCount++;
                        }
                    }

                    uow.Commit();
                    if (errorsCount == 0) return JsonRes();
                    else return JsonRes(Status.Error, "Were deleted not all entities");
                }
            }
            catch (Exception ex)
            {
                return JsonRes(Status.Error, ex.Message);
            }
        }

        [FPCSAuthorize(Role.Teacher)]
        [HttpGet]
        public ActionResult EditForTeacher(Int64 id)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var model = uow.GetRepo<IFamilyRepo>().GetAsQuery(id)
                    .Select(x => new FamilyEditModel
                    {
                        FamilyId = x.FamilyId,
                        Name = x.Name,
                        Description = x.Description,
                        Email = x.Email,
                        Password = x.Password,
                        Telephone = x.Telephone,
                        CellPhone = x.CellPhone,
                        Address = x.Address,
                        MailingAddress = x.MailingAddress,
                        City = x.City,
                        StateId = x.StateId,
                        Country = x.Country,
                        Zip = x.Zip,
                        IsUseDirectory = x.IsUseDirectory,
                        FamilyGuardians = x.Guardians
                            .Select(x2 => new LightGuardianModel { DbUserId = x2.DbUserId, FullName = x2.FullName, CellPhone = x2.CellPhone }),
                        FamilyStudents = x.Students
                            .Select(x3 => new LightStudentModel { DbUserId = x3.DbUserId, FullName = x3.FullName, GradYear = x3.GradYear, IsWithdrawal = x3.WithdrawalDate.HasValue ? true : false })
                    }).FirstOrDefault();

                if (model == null) return Error("Family {0} not found", id);

                model.Init();

                return View(model);
            }
        }

        [FPCSAuthorize(Role.Guardian)]
        [HttpGet]
        public ActionResult DetailsForGuardian()
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repoGuardian = uow.GetRepo<IGuardianRepo>();

                Guardian guardian = repoGuardian.Get(User.UserId);

                Int64 familyId = guardian.Families.Where(x => !x.IsDeleted).Select(x => x.FamilyId).FirstOrDefault();

                var model = uow.GetRepo<IFamilyRepo>().GetAsQuery(familyId)
                    .Select(x => new FamilyDetailsModel
                    {
                        FamilyId = x.FamilyId,
                        Name = x.Name,
                        Description = x.Description,
                        Email = x.Email,
                        Telephone = x.Telephone,
                        CellPhone = x.CellPhone,
                        Address = x.Address,
                        MailingAddress = x.MailingAddress,
                        City = x.City,
                        StateId = x.StateId,
                        Country = x.Country,
                        Zip = x.Zip,
                        IsUseDirectory = x.IsUseDirectory,
                        FamilyGuardians = x.Guardians
                            .Select(x2 => new LightGuardianModel { DbUserId = x2.DbUserId, FullName = x2.FullName, CellPhone = x2.CellPhone }),
                        FamilyStudents = x.Students
                            .Select(x3 => new LightStudentModel { DbUserId = x3.DbUserId, FullName = x3.FullName, GradYear = x3.GradYear, IsWithdrawal = x3.WithdrawalDate.HasValue ? true : false })
                    }).FirstOrDefault();

                if (model == null) return Error("Family {0} not found", familyId);

                model.Init();
                return View(model);
            }
        }

        [FPCSAuthorize(Role.Guardian)]
        [HttpGet]
        public ActionResult EditForGuardian()
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repoGuardian = uow.GetRepo<IGuardianRepo>();

                Guardian guardian = repoGuardian.Get(User.UserId);

                Int64 familyId = guardian.Families.Where(x => !x.IsDeleted).Select(x => x.FamilyId).FirstOrDefault();

                var model = uow.GetRepo<IFamilyRepo>().GetAsQuery(familyId)
                    .Select(x => new FamilyEditModel
                    {
                        FamilyId = x.FamilyId,
                        Name = x.Name,
                        Description = x.Description,
                        Email = x.Email,
                        Telephone = x.Telephone,
                        CellPhone = x.CellPhone,
                        Address = x.Address,
                        MailingAddress = x.MailingAddress,
                        City = x.City,
                        StateId = x.StateId,
                        Country = x.Country,
                        Zip = x.Zip,
                        IsUseDirectory = x.IsUseDirectory,
                        FamilyGuardians = x.Guardians
                            .Select(x2 => new LightGuardianModel { DbUserId = x2.DbUserId, FullName = x2.FullName, CellPhone = x2.CellPhone }),
                        FamilyStudents = x.Students
                            .Select(x3 => new LightStudentModel { DbUserId = x3.DbUserId, FullName = x3.FullName, GradYear = x3.GradYear, IsWithdrawal = x3.WithdrawalDate.HasValue ? true : false })
                    }).FirstOrDefault();

                if (model == null) return Error("Family {0} not found", familyId);

                model.Init();
                return View(model);
            }
        }

        [FPCSAuthorize(Role.Guardian)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditForGuardian(FamilyEditModel model)
        {
            if (model.FamilyGuardians == null) model.FamilyGuardians = new List<LightGuardianModel>();
            if (model.FamilyStudents == null) model.FamilyStudents = new List<LightStudentModel>();

            if (ModelState.IsValid)
            {
                try
                {
                    using (var uow = UnityManager.Resolve<IUnitOfWork>())
                    {
                        var repo = uow.GetRepo<IFamilyRepo>();
                        var family = repo.Update(model.FamilyId, model.Address, model.MailingAddress, model.Telephone, model.CellPhone,
                            model.City, model.Country, model.Zip, model.IsUseDirectory, model.StateId);

                        uow.Commit();

                        return RedirectToAction("DetailsForGuardian");
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }
            }

            model.Init();
            return View(model);
        }

        #region monthly contact logs

        [FPCSAuthorize(Role.Admin, Role.Teacher)]
        [HttpGet]
        public ActionResult MonthlyContactLog(Int64 id)
        {
            MonthlyContactLogIndexModel model = new MonthlyContactLogIndexModel();
            model.FamilyId = id;
            model.FamilyName = GetFamilyNameByFamilyId(id);
            model.StudentName = GetStudentsNameByFamilyId(id);

            return View(model);
        }

        [FPCSAuthorize(Role.Admin, Role.Teacher)]
        public JsonResult _MonthlyContactLog(GridOptions options, MonthlyContactLogListOptions monthlyContactLogListOptions, Int64 familyId)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IMonthlyContactLogRepo>();
                var dbList = repo.GetByFamily(familyId)
                    .ToList()
                    .Select(x => new MonthlyContactLogMiniModel()
                    {
                        MonthlyContactLogId = x.MonthlyContactLogId,
                        FamilyId = x.FamilyId,
                        FamilyName = x.Family.Name,
                        TeacherId = x.TeacherId,
                        TeacherName = x.Teacher.LastName + ", " + x.Teacher.FirstName,
                        Date = x.Date.ToString("MM/dd/yyyy"),
                        Comment = x.Comment
                    })
                    .AsQueryable();

                var engine = new GridDynamicEngine(options, monthlyContactLogListOptions);
                var result = engine.ApplyAll(dbList, x => new
                {
                    MonthlyContactLogId = x.MonthlyContactLogId,
                    FamilyId = x.FamilyId,
                    FamilyName = x.FamilyName,
                    TeacherId = x.TeacherId,
                    TeacherName = x.TeacherName,
                    Date = x.Date,
                    Comment = x.Comment
                });
                
                return Json(result);
            }
        }

        [HttpGet]
        public ActionResult _CreateMonthlyContactLog(Int64 familyId)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repoMCL = uow.GetRepo<IMonthlyContactLogRepo>();
                
                MonthlyContactLogCreateModel model = new MonthlyContactLogCreateModel();
                model.TeacherId = Session.GetUser().UserId;
                model.FamilyId = familyId;
                model.Date = DateTime.Now;

                model.Init();
                return PartialView(model);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _CreateMonthlyContactLog(MonthlyContactLogCreateModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var uow = UnityManager.Resolve<IUnitOfWork>())
                    {
                        var repo = uow.GetRepo<IMonthlyContactLogRepo>();
                        var dbEntity = new MonthlyContactLog();
                        dbEntity.TeacherId = model.TeacherId;
                        dbEntity.FamilyId = model.FamilyId;
                        dbEntity.Date = model.Date;
                        dbEntity.Comment = model.Comment;

                        dbEntity = repo.Add(dbEntity);

                        uow.Commit();

                        return JsonRes(new { MonthlyContactLogId = dbEntity.MonthlyContactLogId });
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }
            }

            model.Init();
            return PartialView(model);
        }

        [HttpGet]
        public ActionResult _EditMonthlyContactLog(Int32 id)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repoMCL = uow.GetRepo<IMonthlyContactLogRepo>();

                MonthlyContactLog mcl = repoMCL.Get(id);

                MonthlyContactLogEditModel model = new MonthlyContactLogEditModel();
                model.MonthlyContactLogId = mcl.MonthlyContactLogId;
                model.TeacherId = mcl.TeacherId;
                model.FamilyId = mcl.FamilyId;
                model.Date = mcl.Date;
                model.Comment = mcl.Comment;

                if (model == null) return Error("Monthly contact log {0} not found", id);

                model.Init();
                return PartialView(model);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _EditMonthlyContactLog(MonthlyContactLogEditModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var uow = UnityManager.Resolve<IUnitOfWork>())
                    {
                        var repo = uow.GetRepo<IMonthlyContactLogRepo>();
                        var dbEntity = repo.Get(model.MonthlyContactLogId);
                        dbEntity.Date = model.Date;
                        dbEntity.Comment = model.Comment;

                        repo.Update(dbEntity);

                        uow.Commit();

                        return JsonRes(new { MonthlyContactLogId = dbEntity.MonthlyContactLogId });
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }
            }

            model.Init();
            return PartialView(model);
        }

        [FPCSAuthorize(Role.Guardian, Role.Teacher, Role.Admin)]
        [HttpPost]
        public ActionResult _Print(String paramsIds, Int64? studentPacketId, GridOptions options,
            FamilyListOptions listOptions)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                Int64[] ids;
                ids = new JavaScriptSerializer().Deserialize<Int64[]>(paramsIds);

                var repo = uow.GetRepo<IMonthlyContactLogRepo>();

                var data = repo.GetAll()
                    .Where(x => ids.Contains(x.MonthlyContactLogId))
                    .OrderByDescending(x => x.Date)
                    .ToList()
                    .Select(x => new MonthlyContactLogPrintItem()
                    {
                        FamilyName = GetFamilyNameByFamilyId(x.FamilyId),
                        TeacherName = x.Teacher.LastName + ", " + x.Teacher.FirstName,
                        Date = x.Date.ToString("MM/dd/yyyy"),
                        Comment = x.Comment
                    });

                MonthlyContactLogPrintModel model = new MonthlyContactLogPrintModel();
                model.Data = data.ToList();
                model.PrintAddress = (ConfigurationManager.AppSettings["PrintAddress"]).ToString();
                model.PrintState = (ConfigurationManager.AppSettings["PrintState"]).ToString();
                model.PrintPhone = (ConfigurationManager.AppSettings["PrintPhone"]).ToString();
                model.PrintFax = (ConfigurationManager.AppSettings["PrintFax"]).ToString();
                model.PrintDateNow = DateTime.Now.ToString("MM/dd/yyyy");

                return PartialView(model);
            }
        }

        private String GetFamilyNameByFamilyId(Int64 FamilyId)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                List<Int64> family = new List<Int64>(){ FamilyId };

                var names = uow.GetRepo<IGuardianRepo>()
                    .GetByFamilies(SchoolYear.SchoolYearId, family)
                    .Select(x => new
                    {
                        GuardianId = x.DbUserId,
                        GuardianFirstName = x.FirstName,
                        GuardianLastName = x.LastName,
                        FamilyIds = x.Families.Where(t => !t.IsDeleted).Select(t => t.FamilyId)
                    })
                    .Select(x => new { x.GuardianLastName, x.GuardianFirstName })
                    .ToList();
                var lastNames = names.Select(x => x.GuardianLastName).Distinct();

                if (lastNames.Count() == 1)
                {
                    return names[0].GuardianLastName + ", " +
                                      String.Join(" & ", names.Select(x => x.GuardianFirstName));
                }
                else
                {
                    return String.Join(" - ", lastNames) + ", " +
                                      String.Join(" & ", names.Select(x => x.GuardianFirstName));
                }
            }
        }

        private String GetStudentsNameByFamilyId(Int64 FamilyId)
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var names = uow.GetRepo<IStudentRepo>()
                    .GetByFamily(FamilyId, SchoolYear.SchoolYearId)
                    .Select(x => new
                    {
                        StudentId = x.DbUserId,
                        StudentFirstName = x.FirstName,
                        StudentLastName = x.LastName,
                        FamilyIds = x.Families.Where(t => !t.IsDeleted).Select(t => t.FamilyId)
                    })
                    .Select(x => new { x.StudentLastName, x.StudentFirstName })
                    .ToList();
                var lastNames = names.Select(x => x.StudentLastName).Distinct();

                if (lastNames.Count() == 1)
                {
                    return names[0].StudentLastName + ", " +
                                      String.Join(" & ", names.Select(x => x.StudentFirstName));
                }
                else
                {
                    return String.Join(" - ", lastNames) + ", " +
                                      String.Join(" & ", names.Select(x => x.StudentFirstName));
                }
            }
        }

        [HttpPost]
        public ActionResult _DeleteMonthlyContactLog(Int32 id)
        {
            try
            {
                using (var uow = UnityManager.Resolve<IUnitOfWork>())
                {
                    var repo = uow.GetRepo<IMonthlyContactLogRepo>();
                    repo.Remove(id);

                    uow.Commit();
                    return JsonRes();
                }
            }
            catch (Exception ex)
            {
                return JsonRes(Status.Error, ex.Message);
            }
        }

        #endregion monthly contact logs

        public ActionResult ChangeFamilyName()
        {
            using (var uow = UnityManager.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IFamilyRepo>();
                var repoDbUser = uow.GetRepo<IDbUserRepo>();

                var families = repo.GetAll().ToList();
                foreach (var item in families)
                {
                    var guardianIds = item.Guardians.Select(t => t.DbUserId).ToList();
                    var guardians = repoDbUser.GetAll().Where(x => guardianIds.Contains(x.DbUserId)).ToList();

                    var names = guardians.Select(x => new { x.LastName, x.FirstName }).ToList();
                    var lastNames = names.Select(x => x.LastName).Distinct();

                    String familyName = String.Empty;
                    if (lastNames.Count() == 1) familyName = names[0].LastName;
                    else familyName = String.Join(" - ", lastNames);

                    item.Name = familyName;

                    repo.Update(item);
                    uow.Commit();
                }

                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult _PrintReimbursement(Int64 id)
        {
            try
            {
                var printAddress = (ConfigurationManager.AppSettings["PrintAddress"]).ToString();
                var printState = (ConfigurationManager.AppSettings["PrintState"]).ToString();
                var printPhone = (ConfigurationManager.AppSettings["PrintPhone"]).ToString();
                var printFax = (ConfigurationManager.AppSettings["PrintFax"]).ToString();
                var printDateNow = DateTime.Now.ToString("MM/dd/yyyy");

                ListPrintReimbursementModel model = new ListPrintReimbursementModel();
                model.PrintReimbursements = new List<PrintReimbursementModel>();

                using (var uow = UnityManager.Resolve<IUnitOfWork>())
                {
                    List<Guardian> guardians = uow.GetRepo<IFamilyRepo>().GetGuardiansByFamily(id).ToList();
                    List<Guid> studentIds = uow.GetRepo<IFamilyRepo>().GetStudentsByFamily(id).Select(x => x.DbUserId).ToList();

                    var data = uow.GetRepo<IGoodServiceRepo>()
                                              .GetReimbursements(studentIds)
                                              .Select(x => new
                                              {
                                                  GoodServiceId = x.GoodServiceId,
                                                  VendorName = x.Vendor.BusinessName,
                                                  Item = x.Title,
                                                  BudgetedAmount = x.UnitPrice * x.NumberOfUnits + x.ShippingHandlingFees,
                                                  RequestedAmount = 0,
                                                  Course = x.StudentPacketCourse.FPCSCourse.Name,
								  StudentName =
									x.StudentPacketCourseId.HasValue ?
										x.StudentPacketCourse.StudentPacket.Student.LastName + ", " +
										x.StudentPacketCourse.StudentPacket.Student.FirstName + " ( " +
										(
											x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.K ? "K" :
											x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.G1 ? "G1" :
											x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.G2 ? "G2" :
											x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.G3 ? "G3" :
											x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.G4 ? "G4" :
											x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.G5 ? "G5" :
											x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.G6 ? "G6" :
											x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.G7 ? "G7" :
											x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.G8 ? "G8" :
											x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.G9 ? "G9" :
											x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.G10 ? "G10" :
											x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.G11 ? "G11" :
											x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.G12 ? "G12" :
											x.StudentPacketCourse.StudentPacket.Student.Grade == Grade.G ? "G" : ""
										)
										+ " ) " :
									x.StudentPacketId.HasValue ?
										x.StudentPacket.Student.LastName + ", " +
										x.StudentPacket.Student.FirstName + " ( " +
										(
											x.StudentPacket.Student.Grade == Grade.K ? "K" :
											x.StudentPacket.Student.Grade == Grade.G1 ? "G1" :
											x.StudentPacket.Student.Grade == Grade.G2 ? "G2" :
											x.StudentPacket.Student.Grade == Grade.G3 ? "G3" :
											x.StudentPacket.Student.Grade == Grade.G4 ? "G4" :
											x.StudentPacket.Student.Grade == Grade.G5 ? "G5" :
											x.StudentPacket.Student.Grade == Grade.G6 ? "G6" :
											x.StudentPacket.Student.Grade == Grade.G7 ? "G7" :
											x.StudentPacket.Student.Grade == Grade.G8 ? "G8" :
											x.StudentPacket.Student.Grade == Grade.G9 ? "G9" :
											x.StudentPacket.Student.Grade == Grade.G10 ? "G10" :
											x.StudentPacket.Student.Grade == Grade.G11 ? "G11" :
											x.StudentPacket.Student.Grade == Grade.G12 ? "G12" :
											x.StudentPacket.Student.Grade == Grade.G ? "G" : ""
										)
										+ " ) " :
									"",
                                    IsWithdrawal = x.StudentPacketCourseId.HasValue
                                                    ? x.StudentPacketCourse.StudentPacket.Student.WithdrawalDate.HasValue ? true : false
                                                    : x.StudentPacketId.HasValue ? x.StudentPacket.Student.WithdrawalDate.HasValue ? true : false : false,
                                                  ObjCD = x.GoodOrService == GoodOrService.Good && x.TypeRequisitionReimbursement == TypeRequisitionReimbursement.VendorService ? "3030" :
                                                            x.GoodOrService == GoodOrService.Service && x.TypeRequisitionReimbursement == TypeRequisitionReimbursement.TextbookCurriculum ? "4020" :
                                                            String.Empty
                                              })
                                              .OrderBy(x => x.StudentName)
                                              .ToList();

                    var groupVendor = data.Select(x => x.VendorName).Distinct().OrderBy(x => x).ToList();

                    foreach (var item in groupVendor)
                    {
                        PrintReimbursementModel itemModel = new PrintReimbursementModel();

                        itemModel.Reimbursements = data.Where(x => x.VendorName == item)
                                                        .Select(x => new ReimbursementModel()
                                                        {
                                                            GoodServiceId = x.GoodServiceId,
                                                            Item = x.Item,
                                                            BudgetedAmount = x.BudgetedAmount,
                                                            RequestedAmount = x.RequestedAmount,
                                                            Course = x.Course,
                                                            StudentName = x.StudentName,
                                                            ObjCD = x.ObjCD,
                                                            IsWithdrawal = x.IsWithdrawal
                                                        })
                                                        .ToList();

                        itemModel.VendorName = item;
                        itemModel.PrintAddress = printAddress;
                        itemModel.PrintState = printState;
                        itemModel.PrintPhone = printPhone;
                        itemModel.PrintFax = printFax;
                        itemModel.PrintDateNow = printDateNow;

                        itemModel.Total = itemModel.Reimbursements.Sum(x => x.BudgetedAmount);

                        if (guardians != null && guardians.Count >= 2)
                        {
                            itemModel.Title = String.Format(@"{0} : {1} & {2}", guardians.FirstOrDefault().LastName, guardians.FirstOrDefault().FirstName, guardians.LastOrDefault().FirstName);
                        }
                        else if (guardians != null && guardians.Count == 1)
                        {
                            itemModel.Title = String.Format(@"{0}, {1}", guardians.FirstOrDefault().LastName, guardians.FirstOrDefault().FirstName);
                        }
                        else
                        {
                            itemModel.Title = String.Empty;
                        }

                        model.PrintReimbursements.Add(itemModel);
                    }
                }

                return PartialView(model);
            }
            catch (Exception ex)
            {
                return Error(ex.Message);
            }
        }
    }
}
