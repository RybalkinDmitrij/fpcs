﻿using System;
using System.Web.Mvc;

using FPCS.Data.Enums;
using FPCS.Web.Admin.Code;
using FPCS.Web.Admin.Models.Shared;

namespace FPCS.Web.Admin.Controllers
{
    public class SharedController : BaseController
    {
        public ActionResult _TopNavPartial()
        {
            var model = new TopNavModel();
            model.SessionSchoolYear = SchoolYear;
            model.SessionUser = User;
            model.Init();
            return PartialView(model);
        }

        [FPCSAuthorize()]
        public ActionResult SetSchoolYear(Int32 year)
        {
            var schoolYear = HttpContext.Session.SetSchoolYear(year);
            if (schoolYear == null)
            {
                if (User.Role == Role.Admin)
                    return RedirectToAction("Create", "SchoolYear", new { year = year });
                else
                    return Error("The year {0} wasn't initialized by administrator. Please contact with administrator or select another year.", year);
            }

            if (Request.UrlReferrer != null &&
                Request.UrlReferrer.AbsolutePath != Request.Url.AbsolutePath &&
                Request.UrlReferrer.Host == Request.Url.Host &&
                Request.UrlReferrer.AbsolutePath != "/SchoolYear/Create")
            {
                return Redirect(Request.UrlReferrer.OriginalString);
            }
            else if (Request.UrlReferrer != null &&
                Request.UrlReferrer.AbsolutePath != Request.Url.AbsolutePath &&
                Request.UrlReferrer.Host == Request.Url.Host &&
                Request.UrlReferrer.AbsolutePath == "/SchoolYear/Create")
            {
                return RedirectToAction("Index", "SchoolYear");
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
    }
}
