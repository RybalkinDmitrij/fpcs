﻿using FPCS.Data.Entities;
using FPCS.Core.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FPCS.Data.Enums;

namespace FPCS.Web.Admin.Helpers.GoodAndService.Models
{
    public class GoodAndServiceFPCSCourseHelperModel
    {
        public GoodAndServiceFPCSCourseHelperModel()
        {
        }

        public GoodAndServiceFPCSCourseHelperModel(GoodService dbEntity)
        {
            FPCSCourseID = dbEntity.FPCSCourseId;
            GoodAndServiceID = dbEntity.GoodServiceId;
            Vendor = dbEntity.Vendor.BusinessName;
            BudgetItem = dbEntity.TypeRequisitionReimbursement.GetDescription();
            Status = String.Empty;
            Description = String.Format(@"{0} #{1}: {2}", dbEntity.RequisitionOrReimbursement.GetDescription(), dbEntity.GoodServiceId.ToString(), dbEntity.Title);
            QTY = dbEntity.NumberOfUnits;
            UnitCost = Math.Round(dbEntity.UnitPrice, 2);
            SH = Math.Round(dbEntity.ShippingHandlingFees, 2);
            BudgetTotal = Math.Round(QTY * UnitCost + SH, 2);
            ActualCharges = 0;
            BudgetAdjust = dbEntity.IsClosed.HasValue && dbEntity.IsClosed.Value ? Math.Round(-1 * BudgetTotal, 2) : 0;
            BudgetBalance = Math.Round(BudgetTotal - ActualCharges + BudgetAdjust, 2);
            IsInstructor = false;
            IsShow = true;

            ItemName = dbEntity.LineItemName;
            Dates = dbEntity.BeginDate.HasValue && dbEntity.EndDate.HasValue ?
                dbEntity.BeginDate.Value.ToShortDateString() + " - " + dbEntity.EndDate.Value.ToShortDateString() :
                String.Empty;
            Consumable = dbEntity.Consumable.HasValue && dbEntity.Consumable.Value == Data.Enums.Consumable.Yes ?
                "Y" :
                "N";
        }
        
        public Int64? FPCSCourseID { get; set; }

        public Int64? GoodAndServiceID { get; set; }
        
        public String BudgetItem { get; set; }

        public String Vendor { get; set; }

        public String Status { get; set; }

        public String Description { get; set; }

        public Decimal QTY { get; set; }

        public Decimal UnitCost { get; set; }

        public Decimal SH { get; set; }

        public Decimal BudgetTotal { get; set; }

        public Decimal ActualCharges { get; set; }

        public Decimal BudgetAdjust { get; set; }

        public Decimal BudgetBalance { get; set; }

        public Decimal TotalPlanningBudget { get; set; }

        public Decimal TotalActualSpending { get; set; }

        public Boolean IsInstructor { get; set; }

        public Boolean IsShow { get; set; }

        public String ItemName { get; set; }

        public String Dates { get; set; }

        public String Consumable { get; set; }
    }
}