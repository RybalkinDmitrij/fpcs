﻿using FPCS.Data.Entities;
using FPCS.Core.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FPCS.Data.Enums;

namespace FPCS.Web.Admin.Helpers.GoodAndService.Models
{
    public class GoodAndServiceHelperModel
    {
        public GoodAndServiceHelperModel()
        {
        }
        
        public GoodAndServiceHelperModel(GoodService dbEntity, List<GoodServiceBalance> balances)
        {
            balances = balances.Where(x => x.GoodServiceId == dbEntity.GoodServiceId).ToList();

            StudentPacketCourseID = dbEntity.StudentPacketCourseId;
            GoodAndServiceID = dbEntity.GoodServiceId;
            GoodAndServiceBalanceID = 0;
            Vendor = dbEntity.Vendor.BusinessName;
            LineItems = balances.Count() != 0 ? "show" : String.Empty;
            BudgetItem = dbEntity.TypeRequisitionReimbursement.GetDescription();
            Status = balances.Count() == 0 && dbEntity.GoodOrServiceApproved.HasValue ? dbEntity.GoodOrServiceApproved.Value.GetDescription() :
                balances.Count() == 0 && !dbEntity.GoodOrServiceApproved.HasValue ? "pend" :
                balances.Count() != 0 && dbEntity.GoodOrServiceApproved.HasValue && dbEntity.GoodOrServiceApproved.Value == GoodOrServiceApproved.b_pend ? "pymt made" :
                balances.Count() != 0 && dbEntity.GoodOrServiceApproved.HasValue && dbEntity.GoodOrServiceApproved.Value == GoodOrServiceApproved.b_appr ? "processing" : "pend";
            Description = String.Format(@"{0} #{1}: {2}. ISBN: {3}", dbEntity.RequisitionOrReimbursement.GetDescription(), dbEntity.GoodServiceId.ToString(), dbEntity.Title, dbEntity.PublisherISBN);
            QTY = dbEntity.NumberOfUnits;
            UnitCost = Math.Round(dbEntity.UnitPrice, 2);
            SH = Math.Round(dbEntity.ShippingHandlingFees, 2);
            BudgetTotal = Math.Round(QTY * UnitCost + SH, 2);
            ActualCharges = Math.Round(balances.Sum(x => x.QTY * x.UnitPrice + x.Shipping), 2);
            BudgetAdjust = dbEntity.IsClosed.HasValue && dbEntity.IsClosed.Value ? Math.Round(-1 * BudgetTotal, 2) : 0;
            BudgetBalance = Math.Round(BudgetTotal - ActualCharges + BudgetAdjust, 2);
            IsInstructor = false;
            IsShow = true;
            IsClosed = dbEntity.IsClosed.HasValue ? dbEntity.IsClosed.Value : false;

            ItemName = dbEntity.LineItemName;
            Dates = dbEntity.BeginDate.HasValue && dbEntity.EndDate.HasValue ?
                dbEntity.BeginDate.Value.ToShortDateString() + " - " + dbEntity.EndDate.Value.ToShortDateString() :
                String.Empty;
            Consumable = dbEntity.Consumable.HasValue && dbEntity.Consumable.Value == Data.Enums.Consumable.Yes ?
                "Y" :
                "N";
            HasBalance = balances.Count > 0 ? true : false;
        }

        public GoodAndServiceHelperModel(GoodServiceBalance dbEntity)
        {
            StudentPacketCourseID = dbEntity.GoodService.StudentPacketCourseId;
            GoodAndServiceID = dbEntity.GoodServiceId;
            GoodAndServiceBalanceID = dbEntity.GoodServiceBalanceId;
            BudgetItem = String.Format(@"Entered: {0}",
                dbEntity.DateEntered.HasValue
                    ? dbEntity.DateEntered.Value.ToString("MM/dd/yyyy")
                    : DateTime.Now.ToString("MM/dd/yyyy"));
            Description = String.Format(@"{0}<br />Check #:{1}", dbEntity.Description, dbEntity.Check);
            QTY = Math.Truncate(dbEntity.QTY);
            UnitCost = Math.Round(dbEntity.UnitPrice, 2);
            SH = Math.Round(dbEntity.Shipping, 2);
            ActualCharges = Math.Round(dbEntity.QTY * dbEntity.UnitPrice + dbEntity.Shipping, 2);
            IsShow = true;
            IsClosed = dbEntity.GoodService.IsClosed.HasValue ? dbEntity.GoodService.IsClosed.Value : false;
        }

        public Int64? StudentPacketCourseID { get; set; }

        public Int64? GoodAndServiceID { get; set; }

        public Int64? GoodAndServiceBalanceID { get; set; }

        public String LineItems { get; set; }

        public String BudgetItem { get; set; }

        public String Vendor { get; set; }

        public String Status { get; set; }

        public String Description { get; set; }

        public Decimal QTY { get; set; }

        public Decimal UnitCost { get; set; }

        public Decimal SH { get; set; }

        public Decimal BudgetTotal { get; set; }

        public Decimal ActualCharges { get; set; }

        public Decimal BudgetAdjust { get; set; }

        public Decimal BudgetBalance { get; set; }

        public Decimal TotalPlanningBudget { get; set; }

        public Decimal TotalActualSpending { get; set; }

        public Boolean IsInstructor { get; set; }

        public Boolean IsShow { get; set; }

        public Boolean IsClosed { get; set; }

        public String ItemName { get; set; }

        public String Dates { get; set; }

        public String Consumable { get; set; }

        public Boolean HasBalance { get; set; }
    }
}