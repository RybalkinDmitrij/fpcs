﻿using FPCS.Core.Unity;
using FPCS.Data;
using FPCS.Data.Entities;
using FPCS.Data.Repo;
using FPCS.Web.Admin.Helpers.GoodAndService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FPCS.Web.Admin.Helpers.GoodAndService
{
	public static class GoodAndServiceHelper
	{
		public static GoodAndServiceHelperModel GetInstructorFPCSCourse(StudentPacketCourse spc)
		{
			if (spc.FPCSCourse.GuardianId.HasValue)
			{
				return new GoodAndServiceHelperModel()
				{
					IsShow = false,
					IsInstructor = true,
					LineItems = String.Empty,
					BudgetItem = "Guardian",
					Vendor = String.Empty,
					Status = String.Empty,
					QTY = 0,
					UnitCost = 0,
					SH = 0,
					BudgetTotal = 0,
					ActualCharges = 0,
					BudgetAdjust = 0,
					BudgetBalance = 0
				};
			}
			else if (spc.FPCSCourse.VendorId.HasValue)
			{
				var vQTY = 1m;
				return new GoodAndServiceHelperModel()
				{
					IsInstructor = true,
					LineItems = String.Empty,
					BudgetItem = "Instructor",
					Vendor = String.Empty,
					Status = String.Empty,
					QTY = Math.Round(vQTY, 2),
					UnitCost = Math.Round(spc.FPCSCourse.Vendor != null ? spc.FPCSCourse.Vendor.CostCourseProvider : 0m, 2),
					SH = 0,
					BudgetTotal = Math.Round(vQTY*(spc.FPCSCourse.Vendor != null ? spc.FPCSCourse.Vendor.CostCourseProvider : 0m), 2),
					ActualCharges = Math.Round(vQTY*(spc.FPCSCourse.Vendor != null ? spc.FPCSCourse.Vendor.CostCourseProvider : 0m), 2),
					BudgetAdjust = 0,
					BudgetBalance = 0
				};
			}
			else if (spc.FPCSCourse.TeacherId.HasValue && spc.FPCSCourse.ASDCourse.IsFree)
			{
				return new GoodAndServiceHelperModel()
				{
					IsShow = false,
					IsInstructor = true,
					LineItems = String.Empty,
					BudgetItem = "Instructor",
					Vendor = String.Empty,
					Status = String.Empty,
					QTY = 0,
					UnitCost = 0,
					SH = 0,
					BudgetTotal = 0,
					ActualCharges = 0,
					BudgetAdjust = 0,
					BudgetBalance = 0
				};
			}

			var countStudentCourse = 0;

			using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
			{
				var repo = uow.GetRepo<IStudentPacketCourseRepo>();

				countStudentCourse = repo.GetByFPCSCourse(spc.FPCSCourse.FPCSCourseId, spc.StudentPacket.SchoolYearId).Count();
			}

			var QTY = countStudentCourse != 0
				? (spc.FPCSCourse.HoursTeacherPlanning + spc.FPCSCourse.HoursWithAllStudents)/countStudentCourse
				: 0;

			return new GoodAndServiceHelperModel()
			{
				IsInstructor = true,
				LineItems = String.Empty,
				BudgetItem = "Instructor",
				Vendor = String.Empty,
				Status = String.Empty,
				QTY = QTY.HasValue ? Math.Round(QTY.Value, 2) : 0,
				UnitCost = Math.Round(spc.FPCSCourse.Teacher != null ? spc.FPCSCourse.Teacher.PayHourwBenefits : 0m, 2),
				SH = 0,
				BudgetTotal = QTY.HasValue ? Math.Round(QTY.Value*(spc.FPCSCourse.Teacher != null ? spc.FPCSCourse.Teacher.PayHourwBenefits : 0m), 2) : 0,
				ActualCharges = QTY.HasValue ? Math.Round(QTY.Value*(spc.FPCSCourse.Teacher != null ? spc.FPCSCourse.Teacher.PayHourwBenefits : 0m), 2) : 0,
				BudgetAdjust = 0,
				BudgetBalance = 0
			};
		}

		public static GoodAndServiceFPCSCourseHelperModel GetInstructor(FPCSCourse fpcs, Int32 schoolYearId)
		{
			var countStudentCourse = 0;

			using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
			{
				var repo = uow.GetRepo<IStudentPacketCourseRepo>();

				countStudentCourse = repo.GetByFPCSCourse(fpcs.FPCSCourseId, schoolYearId).Count();
			}

			var QTY = countStudentCourse != 0
				? (fpcs.HoursTeacherPlanning + fpcs.HoursWithAllStudents)/countStudentCourse
				: 0;

			if (fpcs.GuardianId.HasValue)
			{
				return new GoodAndServiceFPCSCourseHelperModel()
				{
					FPCSCourseID = fpcs.FPCSCourseId,
					IsShow = false,
					IsInstructor = true,
					BudgetItem = "Guardian",
					Vendor = String.Empty,
					Status = String.Empty,
					QTY = 0,
					UnitCost = 0,
					SH = 0,
					BudgetTotal = 0,
					ActualCharges = 0,
					BudgetAdjust = 0,
					BudgetBalance = 0
				};
			}
			else if (fpcs.VendorId.HasValue)
			{
				QTY = 1m;
				return new GoodAndServiceFPCSCourseHelperModel()
				{
					FPCSCourseID = fpcs.FPCSCourseId,
					IsShow = false,
					IsInstructor = true,
					BudgetItem = "Vendor",
					Vendor = String.Empty,
					Status = String.Empty,
					QTY = QTY.Value,
					UnitCost = Math.Round(fpcs.Vendor != null ? fpcs.Vendor.CostCourseProvider : 0m, 2),
					SH = 0,
					BudgetTotal = Math.Round(QTY.Value*(fpcs.Vendor != null ? fpcs.Vendor.CostCourseProvider : 0m), 2),
					ActualCharges = Math.Round(QTY.Value*(fpcs.Vendor != null ? fpcs.Vendor.CostCourseProvider : 0m), 2),
					BudgetAdjust = 0,
					BudgetBalance = 0
				};
			}
			else if (fpcs.TeacherId.HasValue && fpcs.ASDCourse.IsFree)
			{
				return new GoodAndServiceFPCSCourseHelperModel()
				{
					FPCSCourseID = fpcs.FPCSCourseId,
					IsShow = false,
					IsInstructor = true,
					BudgetItem = "Instructor",
					Vendor = String.Empty,
					Status = String.Empty,
					QTY = 0,
					UnitCost = 0,
					SH = 0,
					BudgetTotal = 0,
					ActualCharges = 0,
					BudgetAdjust = 0,
					BudgetBalance = 0
				};
			}

			return new GoodAndServiceFPCSCourseHelperModel()
			{
				FPCSCourseID = fpcs.FPCSCourseId,
				IsInstructor = true,
				BudgetItem = "Instructor",
				Vendor = String.Empty,
				Status = String.Empty,
				QTY = QTY.HasValue ? Math.Round(QTY.Value, 2) : 0,
				UnitCost = Math.Round(fpcs.Teacher != null ? fpcs.Teacher.PayHourwBenefits : 0m, 2),
				SH = 0,
				BudgetTotal =
					QTY.HasValue ? Math.Round(QTY.Value*(fpcs.Teacher != null ? fpcs.Teacher.PayHourwBenefits : 0m), 2) : 0,
				ActualCharges =
					QTY.HasValue ? Math.Round(QTY.Value*(fpcs.Teacher != null ? fpcs.Teacher.PayHourwBenefits : 0m), 2) : 0,
				BudgetAdjust = 0,
				BudgetBalance = 0
			};
		}

		public static List<GoodAndServiceHelperModel> GetListInstructorFPCSCourse(Int64 studentPacketId)
		{
			using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
			{
				var repo = uow.GetRepo<IStudentPacketCourseRepo>();

				List<StudentPacketCourse> studentPacketCourses = repo.GetByStudentPacket(studentPacketId)
					.ToList();

				List<StudentPacketCourse> courseStudent = repo.GetByFPCSCourse(
					studentPacketCourses.Select(x => x.FPCSCourseId).ToList(),
					studentPacketCourses.FirstOrDefault() != null
						? studentPacketCourses.FirstOrDefault().StudentPacket.SchoolYearId
						: 0)
					.ToList();

				List<GoodAndServiceHelperModel> result = new List<GoodAndServiceHelperModel>();

				foreach (var item in studentPacketCourses)
				{
					if (!item.FPCSCourse.TeacherId.HasValue)
					{
						if (item.FPCSCourse.VendorId.HasValue)
						{
							var QTY = 1m;
							result.Add(new GoodAndServiceHelperModel()
							{
								StudentPacketCourseID = item.StudentPacketCourseId,
								IsInstructor = true,
								LineItems = String.Empty,
								BudgetItem = "Instructor",
								Vendor = String.Empty,
								Status = String.Empty,
								QTY = Math.Round(QTY, 2),
								UnitCost = Math.Round(item.FPCSCourse.Vendor != null ? item.FPCSCourse.Vendor.CostCourseProvider : 0m, 2),
								SH = 0,
								BudgetTotal =
									Math.Round(QTY*(item.FPCSCourse.Vendor != null ? item.FPCSCourse.Vendor.CostCourseProvider : 0m), 2),
								ActualCharges =
									Math.Round(QTY*(item.FPCSCourse.Vendor != null ? item.FPCSCourse.Vendor.CostCourseProvider : 0m), 2),
								BudgetAdjust = 0,
								BudgetBalance = 0
							});
						}
					}
					else if (item.FPCSCourse.TeacherId.HasValue && item.FPCSCourse.ASDCourse.IsFree)
					{
						continue;
					}
					else
					{
						var countStudentCourse = 0;

						countStudentCourse = courseStudent.Where(x => x.FPCSCourseId == item.FPCSCourseId).Count();

						var QTY = countStudentCourse != 0
							? (item.FPCSCourse.HoursTeacherPlanning + item.FPCSCourse.HoursWithAllStudents)/countStudentCourse
							: 0;

						result.Add(new GoodAndServiceHelperModel()
						{
							StudentPacketCourseID = item.StudentPacketCourseId,
							IsInstructor = true,
							LineItems = String.Empty,
							BudgetItem = "Instructor",
							Vendor = String.Empty,
							Status = String.Empty,
							QTY = QTY.HasValue ? Math.Round(QTY.Value, 2) : 0,
							UnitCost = Math.Round(item.FPCSCourse.Teacher != null ? item.FPCSCourse.Teacher.PayHourwBenefits : 0m, 2),
							SH = 0,
							BudgetTotal =
								QTY.HasValue ? Math.Round(QTY.Value*(item.FPCSCourse.Teacher != null ? item.FPCSCourse.Teacher.PayHourwBenefits : 0m), 2) : 0,
							ActualCharges =
								QTY.HasValue ? Math.Round(QTY.Value*(item.FPCSCourse.Teacher != null ? item.FPCSCourse.Teacher.PayHourwBenefits : 0m), 2) : 0,
							BudgetAdjust = 0,
							BudgetBalance = 0
						});
					}
				}

				return result;
			}
		}

		public static List<GoodAndServiceFPCSCourseHelperModel> GetListInstructor(List<Int64> fpcsCourseIds,
			Int32 schoolYearId)
		{
			using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
			{
				var repo = uow.GetRepo<IFPCSCourseRepo>();

				List<FPCSCourse> fpcsCourses = repo.GetByIds(fpcsCourseIds).ToList();

				List<GoodAndServiceFPCSCourseHelperModel> result = new List<GoodAndServiceFPCSCourseHelperModel>();

				foreach (var item in fpcsCourses)
				{
					result.Add(GetInstructor(item, schoolYearId));
				}

				return result;
			}
		}

		public static GoodAndServiceHelperModel GetGoodAndService(GoodService dbEntity, List<GoodServiceBalance> balances)
		{
			return new GoodAndServiceHelperModel(dbEntity, balances);
		}

		public static GoodAndServiceHelperModel GetGoodAndService(GoodServiceBalance dbEntity)
		{
			return new GoodAndServiceHelperModel(dbEntity);
		}

		public static List<GoodAndServiceHelperModel> GetListGoodAndService(Int64 studentPacketId)
		{
			using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
			{
				var repoGoodService = uow.GetRepo<IGoodServiceRepo>();
				var repoGoodServiceBalance = uow.GetRepo<IGoodServiceBalanceRepo>();

				var dataBalance = repoGoodServiceBalance.GetByStudentPacket(studentPacketId).ToList();

				var data = repoGoodService.GetByStudentPacket(studentPacketId)
					.ToList()
					.Select(x => new GoodAndServiceHelperModel(x, dataBalance.Where(t => t.GoodServiceId == x.GoodServiceId).ToList()))
					.ToList();

				data.AddRange(dataBalance.Select(x => new GoodAndServiceHelperModel(x)).ToList());
				data.AddRange(GoodAndServiceHelper.GetListInstructorFPCSCourse(studentPacketId));

				data = data
					.OrderByDescending(x => x.IsInstructor)
					.ThenBy(x => x.GoodAndServiceID)
					.ThenBy(x => x.GoodAndServiceBalanceID)
					.ToList();

				return data;
			}
		}

		public static List<GoodAndServiceFPCSCourseHelperModel> GetListGoodAndService(List<Int64> fpcsCourseIds,
			Int32 schoolYearId)
		{
			using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
			{
				var repoGoodService = uow.GetRepo<IGoodServiceRepo>();

				var data = repoGoodService.GetByFPCSCourse(fpcsCourseIds)
					.ToList()
					.Select(x => new GoodAndServiceFPCSCourseHelperModel(x))
					.ToList();
				
				data = data
					.OrderByDescending(x => x.IsInstructor)
					.ThenBy(x => x.GoodAndServiceID)
					.ToList();

				return data;
			}
		}
	}
}