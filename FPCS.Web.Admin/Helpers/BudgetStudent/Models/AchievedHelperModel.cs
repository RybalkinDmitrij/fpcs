﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FPCS.Web.Admin.Helpers.BudgetStudent.Models
{
    public class AchievedHelperModel
    {
        public Int32 AchievedILPHrs { get; set; }

        public Decimal AchievedContractHrs { get; set; }

        public Decimal AchievedCoreUnits { get; set; }

        public Decimal AchievedElectiveUnits { get; set; }

        public Decimal AchievedTotalUnits { get; set; }

        public Decimal AchievedEnrollmentPercent { get; set; }
    }
}