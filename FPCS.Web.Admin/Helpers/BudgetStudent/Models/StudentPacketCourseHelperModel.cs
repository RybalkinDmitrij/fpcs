﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FPCS.Web.Admin.Helpers.BudgetStudent.Models
{
    public class StudentPacketCourseHelperModel
    {
        public Int32 SubjectId { get; set; }

        public Boolean IsElective { get; set; }

        public Decimal PlanningBudgetTotal { get; set; }

        public Decimal ActualSpendingTotal { get; set; }

        public Int32 ILPHours { get; set; }

        public Decimal ContractHrs { get; set; }

        public Boolean IsFullSignature { get; set; }
    }
}