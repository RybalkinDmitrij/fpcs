﻿using FPCS.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FPCS.Web.Admin.Helpers.BudgetStudent.Models
{
    public class BudgetStudentHelperModel
    {
        public GoalEnrollmentPercent GoalEnrollmentPercent { get; set; }

        public Decimal GoalCoreUnits { get; set; }

        public Decimal GoalElectiveUnits { get; set; }

        public Decimal GoalTotalUnits { get; set; }

        public Decimal GoalILPHrs { get; set; }

        public Decimal GoalContractHrs { get; set; }

        public Decimal AchievedEnrollmentPercent { get; set; }

        public Decimal AchievedCoreUnits { get; set; }

        public Decimal AchievedElectiveUnits { get; set; }

        public Decimal AchievedTotalUnits { get; set; }

        public Decimal AchievedILPHrs { get; set; }

        public Decimal AchievedContractHrs { get; set; }

        public Decimal IBBudgetLimit { get; set; }

        public Decimal IBAmountBudgeted { get; set; }

        public Decimal IBElectiveBalance { get; set; }

        public Decimal IBGlobalReserve { get; set; }

        public Decimal PBBeginningBalance { get; set; }

        public Decimal PBBudgetTransferDeposits { get; set; }

        public Decimal PBBudgetTransferWithdrawals { get; set; }

        public Decimal PBAvailableRemainingFunds { get; set; }

        public Decimal ASBeginningBalance { get; set; }

        public Decimal ASBudgetTransferDeposits { get; set; }

        public Decimal ASBudgetTransferWithdrawals { get; set; }

        public Decimal ASAvailableRemainingFunds { get; set; }

        public List<BudgetFPCSCourseHelperModel> CoursesTotals { get; set; }

        public Decimal PBAvailableRemainingFundsTotal { get; set; }

        public Decimal ASAvailableRemainingFundsTotal { get; set; }

        public Decimal Sponsorship { get; set; }
    }
}