﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FPCS.Web.Admin.Helpers.BudgetStudent.Models
{
    public class IndividualBudgetHelperModel
    {
        public Decimal IBBudgetLimit { get; set; }

        public Decimal IBAmountBudgeted { get; set; }

        public Decimal IBElectiveBalance { get; set; }

        public Decimal IBGlobalReserve { get; set; }
    }
}