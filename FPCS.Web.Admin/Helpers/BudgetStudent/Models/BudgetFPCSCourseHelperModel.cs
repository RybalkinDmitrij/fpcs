﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FPCS.Web.Admin.Helpers.BudgetStudent.Models
{
    public class BudgetFPCSCourseHelperModel
    {
        public Int64 FPCSCourseId { get; set; }

        public Decimal PBTotals { get; set; }

        public Decimal ASTotals { get; set; }
    }
}