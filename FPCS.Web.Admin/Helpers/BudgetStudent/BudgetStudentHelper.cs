﻿using FPCS.Core.Unity;
using FPCS.Data;
using FPCS.Data.Entities;
using FPCS.Data.Enums;
using FPCS.Data.Repo;
using FPCS.Web.Admin.Helpers.GoodAndService.Models;
using FPCS.Web.Admin.Helpers.BudgetStudent.Models;
using FPCS.Web.Admin.Helpers.GoodAndService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FPCS.Web.Admin.Helpers.BudgetStudent
{
	public static class BudgetStudentHelper
	{
		public static String TotalLessBudget
		{
			get { return "You cannot add more than ${0} for this vendor."; }
		}

		public static String MessageNoEnoughBudget
		{
			get { return "There's not enough money to perform this action."; }
		}

		public static String MessageNoEnoughSponsorship
		{
			get { return "There's not enough money to perform this action."; }
		}

		public static BudgetStudentHelperModel GetBudgetStudentPacket(Guid studentId, Int32 schoolYearId)
		{
			using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
			{
				BudgetStudentHelperModel result = new BudgetStudentHelperModel();

				var schoolYearRepo = uow.GetRepo<ISchoolYearRepo>();
				var studentRepo = uow.GetRepo<IStudentRepo>();
				var packetRepo = uow.GetRepo<IStudentPacketRepo>();
				var transferRepo = uow.GetRepo<ITransferRepo>();
				var goodServiceRepo = uow.GetRepo<IGoodServiceRepo>();

				SchoolYear schoolYear = schoolYearRepo.Get(schoolYearId);
				Student student = studentRepo.Get(studentId);
				StudentPacket packet = packetRepo.Get(studentId, schoolYearId);
				List<GoodService> sponsorship = goodServiceRepo.GetByPacket(packet.StudentPacketId).ToList();

				result.GoalEnrollmentPercent = student.PercentagePlanningEnroll.HasValue &&
				                               student.PercentagePlanningEnroll.Value == 0
					? GoalEnrollmentPercent._0Percent
					: student.PercentagePlanningEnroll.HasValue &&
					  student.PercentagePlanningEnroll.Value == 25
						? GoalEnrollmentPercent._25Percent
						: student.PercentagePlanningEnroll.HasValue &&
						  student.PercentagePlanningEnroll.Value == 50
							? GoalEnrollmentPercent._50Percent
							: student.PercentagePlanningEnroll.HasValue &&
							  student.PercentagePlanningEnroll.Value == 75
								? GoalEnrollmentPercent._75Percent
								: student.PercentagePlanningEnroll.HasValue &&
								  student.PercentagePlanningEnroll.Value == 100
									? GoalEnrollmentPercent._100Percent
									: GoalEnrollmentPercent._0Percent;

				result.GoalCoreUnits =
					Math.Round(
						GetCoreUnitByGrade(student.Grade, schoolYear)*student.PercentagePlanningEnroll.GetValueOrDefault(100)/100, 2);
				;
				result.GoalElectiveUnits =
					Math.Round(
						GetElectiveUnitByGrade(student.Grade, schoolYear)*student.PercentagePlanningEnroll.GetValueOrDefault(100)/100, 2);
				;
				result.GoalTotalUnits = Math.Round(result.GoalCoreUnits + result.GoalElectiveUnits);
				result.GoalILPHrs = result.GoalTotalUnits*90;

				result.Sponsorship = sponsorship.Sum(x => x.NumberOfUnits*x.UnitPrice + x.ShippingHandlingFees);
				result.PBBeginningBalance =
					Math.Round(
						GetFundingAmountByGrade(student.Grade, schoolYear)*student.PercentagePlanningEnroll.GetValueOrDefault(100)/100, 2);
				result.PBBudgetTransferDeposits = Math.Round(transferRepo.GetDeposits(studentId), 2);
				result.PBBudgetTransferWithdrawals = Math.Round(transferRepo.GetWithdrawals(studentId), 2);
				// INFO: 18.05.2014 was solved to make PBAvailableRemainingFunds = PBAvailableRemainingFundsTotal
				result.PBAvailableRemainingFunds =
					Math.Round(
						result.PBBeginningBalance + result.PBBudgetTransferDeposits - result.PBBudgetTransferWithdrawals -
						result.Sponsorship, 2);

				// INFO: 18.05.2014 was solved to make Actual Spending = Planning Budget
				result.ASBeginningBalance = result.PBBeginningBalance;
				result.ASBudgetTransferDeposits = result.PBBudgetTransferDeposits;
				result.ASBudgetTransferWithdrawals = result.PBBudgetTransferWithdrawals;
				result.ASAvailableRemainingFunds = result.PBAvailableRemainingFunds;

				// INFO: 08.02.2015 was solved to make if Amp Math and Amp E-La equal Advanced or Proficient, then IBBudgetLimit spend 100% else spend 50% with +
				if ((packet.AmpELA == AmpEla.Advanced || packet.AmpELA == AmpEla.Proficient) &&
				    (packet.AmpMath == AmpMath.Advanced || packet.AmpMath == AmpMath.Proficient))
				{
					result.IBBudgetLimit = result.PBBeginningBalance;
				}
				else
				{
					result.IBBudgetLimit = Math.Round(result.PBBeginningBalance/2, 2) + schoolYear.PlusBudgetLimit;
				}

				//result.IBAmountBudgeted = 0; // TODO: solve problem
				//result.IBElectiveBalance = Math.Round(result.IBBudgetLimit - result.IBAmountBudgeted, 2);
				result.IBGlobalReserve = GetFundingAmountByGrade(Grade.G, schoolYear);

				List<StudentPacketCourse> courses =
					packet != null
						? packet.StudentPacketCourses
							.Where(x => !x.IsDeleted &&
							            !x.FPCSCourse.IsDeleted &&
							            (x.FPCSCourse.Semester == Semester.Semester1 || x.FPCSCourse.Semester == Semester.Semester2))
							.ToList()
						: new List<StudentPacketCourse>();

				List<BudgetFPCSCourseHelperModel> budgetCourses = new List<BudgetFPCSCourseHelperModel>();

				budgetCourses = courses.Select(x => new BudgetFPCSCourseHelperModel()
				{
					FPCSCourseId = x.FPCSCourseId,
					PBTotals = x.GoodServices.Where(t => !t.IsDeleted /*&& (!t.IsClosed.HasValue ||
					                                                      (t.IsClosed.HasValue && !t.IsClosed.Value))*/)
						.Select(t => new GoodAndServiceHelperModel(t, t.GoodServiceBalances.ToList()))
						.Sum(t => t.BudgetTotal) +
					           GoodAndServiceHelper.GetInstructorFPCSCourse(x).BudgetTotal,
					ASTotals = x.GoodServices.Where(t => !t.IsDeleted && (!t.IsClosed.HasValue ||
					                                                      (t.IsClosed.HasValue && !t.IsClosed.Value)))
						.Select(t => new GoodAndServiceHelperModel(t, t.GoodServiceBalances.ToList()))
						.Sum(t => t.ActualCharges) +
					           GoodAndServiceHelper.GetInstructorFPCSCourse(x).ActualCharges,
				})
					.ToList();

				result.CoursesTotals = budgetCourses;

				result.PBAvailableRemainingFundsTotal = result.PBAvailableRemainingFunds - budgetCourses.Sum(x => x.PBTotals);
				result.PBAvailableRemainingFunds = result.PBAvailableRemainingFundsTotal;

				result.ASAvailableRemainingFundsTotal = result.ASAvailableRemainingFunds - budgetCourses.Sum(x => x.ASTotals);
				result.ASAvailableRemainingFunds = result.ASAvailableRemainingFundsTotal;

				return result;
			}
		}

		public static BudgetFPCSCourseHelperModel GetBudgetFPCSCourse(Int64 fpcsCourseId, Int32 schoolYearId)
		{
			using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
			{
				var repo = uow.GetRepo<IFPCSCourseRepo>();

				var entity = repo.Get(fpcsCourseId);

				BudgetFPCSCourseHelperModel model = new BudgetFPCSCourseHelperModel();

				model.FPCSCourseId = fpcsCourseId;
				model.PBTotals = entity.GoodServices
					.Where(t => !t.IsDeleted && (!t.IsClosed.HasValue ||
					                             (t.IsClosed.HasValue && !t.IsClosed.Value)))
					.Select(t => new GoodAndServiceHelperModel(t, t.GoodServiceBalances.ToList()))
					.Sum(t => t.BudgetTotal) +
				                 GoodAndServiceHelper.GetInstructor(entity, schoolYearId).BudgetTotal;
				model.ASTotals = entity.GoodServices.Where(t => !t.IsDeleted && (!t.IsClosed.HasValue ||
				                                                                 (t.IsClosed.HasValue && !t.IsClosed.Value)))
					.Select(t => new GoodAndServiceHelperModel(t, t.GoodServiceBalances.ToList()))
					.Sum(t => t.ActualCharges) +
				                 GoodAndServiceHelper.GetInstructor(entity, schoolYearId).ActualCharges;

				return model;
			}
		}

		public static BudgetGoodServiceHelperModel GetBudgetGoodService(Int64 goodServiceId)
		{
			using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
			{
				var repo = uow.GetRepo<IGoodServiceRepo>();
				var repoBalance = uow.GetRepo<IGoodServiceBalanceRepo>();

				var entity = repo.Get(goodServiceId);
				var balances = repoBalance.GetByGoodService(goodServiceId).ToList();

				var gsModel = new GoodAndServiceHelperModel(entity, balances);

				BudgetGoodServiceHelperModel model = new BudgetGoodServiceHelperModel();
				model.GoodServiceId = entity.GoodServiceId;
				model.PBTotals = gsModel.BudgetTotal;
				model.ASTotals = gsModel.ActualCharges;

				return model;
			}
		}

		public static Boolean IsResolveFPCSCourse(Guid studentId, Int32 schoolYearId, Int64 fpcsCourseId)
		{
			using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
			{
				var repoSP = uow.GetRepo<IStudentPacketRepo>();
				var repoFC = uow.GetRepo<IFPCSCourseRepo>();

				var studentPacket = repoSP.Get(studentId, schoolYearId);
				var fpcsCourse = repoFC.Get(fpcsCourseId);

				if ((fpcsCourse.Semester == Semester.Semester1 && studentPacket.SponsorSignatureSem1 == Sign.Sign &&
				     studentPacket.GuardianSignatureSem1 == Sign.Sign) ||
				    (fpcsCourse.Semester == Semester.Semester2 && studentPacket.SponsorSignatureSem2 == Sign.Sign &&
				     studentPacket.GuardianSignatureSem2 == Sign.Sign) ||
				    (fpcsCourse.Semester == Semester.Summer && studentPacket.SponsorSignatureSummer == Sign.Sign &&
				     studentPacket.GuardianSignatureSummer == Sign.Sign))
				{
					BudgetStudentHelperModel budgetStudentPacket = GetBudgetStudentPacket(studentId, schoolYearId);
					BudgetFPCSCourseHelperModel budgetFpcsCourse = GetBudgetFPCSCourse(fpcsCourseId, schoolYearId);

					return budgetStudentPacket.PBAvailableRemainingFundsTotal >= budgetFpcsCourse.PBTotals;
				}
				else
				{
					return true;
				}
			}
		}

		public static Boolean IsResolveGoodService(Guid studentId, Int32 schoolYearId)
		{
			BudgetStudentHelperModel budgetStudentPacket = GetBudgetStudentPacket(studentId, schoolYearId);

			return budgetStudentPacket.PBAvailableRemainingFundsTotal >= 0;
		}

		public static Boolean IsResolveSpecificGoodService(Int32 vendorId, Decimal unitPrice, Int32 numberOfUnits, Decimal shippingHandlingFees)
		{
			if (vendorId == 598) // FPCS ADDITIONAL COMPUTER
			{
				return unitPrice * numberOfUnits + shippingHandlingFees <= GetTotalLessSpecificVendor(598);
			}
			else if (vendorId == 599) // FPCS IPAD/TABLET
			{
				return unitPrice * numberOfUnits + shippingHandlingFees <= GetTotalLessSpecificVendor(599);
			}
			else if (vendorId == 600) // FPCS PRINTER
			{
				return unitPrice * numberOfUnits + shippingHandlingFees <= GetTotalLessSpecificVendor(600);
			}
			else if (vendorId == 601) // FPCS SCHOOL SUPPLIES
			{
				return unitPrice * numberOfUnits + shippingHandlingFees <= GetTotalLessSpecificVendor(601);
			}
			else if (vendorId == 602) // FPCS INTERNET
			{
				return unitPrice * numberOfUnits + shippingHandlingFees <= GetTotalLessSpecificVendor(602);
			}

			return true;
		}

		public static Int32 GetTotalLessSpecificVendor(Int32 vendorId)
		{
			if (vendorId == 598) // FPCS ADDITIONAL COMPUTER
			{
				return 200;
			}
			else if (vendorId == 599) // FPCS IPAD/TABLET
			{
				return 200;
			}
			else if (vendorId == 600) // FPCS PRINTER
			{
				return 200;
			}
			else if (vendorId == 601) // FPCS SCHOOL SUPPLIES
			{
				return 150;
			}
			else if (vendorId == 602) // FPCS INTERNET
			{
				return 840;
			}

			return 0;
		}

		public static Boolean IsResolveSponsorshipGoodService(Int64 studentPacketId, Int32 schoolYearId)
		{
			using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
			{
				var repo = uow.GetRepo<ISchoolYearRepo>();
				var repoGS = uow.GetRepo<IGoodServiceRepo>();

				var lstGoodService = repoGS.GetByPacket(studentPacketId)
					.Select(x => new
					{
						NumberOfUnits = x.NumberOfUnits,
						UnitPrice = x.UnitPrice,
						ShippingHandlingFees = x.ShippingHandlingFees
					})
					.ToList();

				var sponsorship = lstGoodService != null
					? Math.Round(lstGoodService.Sum(t => t.NumberOfUnits*t.UnitPrice + t.ShippingHandlingFees), 2)
					: 0;

				var schoolYear = repo.Get(schoolYearId);
				var sponsorshipLimit = schoolYear != null ? schoolYear.GeneralSupplies : 0m;

				return sponsorshipLimit - sponsorship >= 0;
			}
		}

		public static Boolean IsResolveTransfer(Guid studentId, Int32 schoolYearId, Decimal transfer)
		{
			BudgetStudentHelperModel budgetStudentPacket = GetBudgetStudentPacket(studentId, schoolYearId);

			return budgetStudentPacket.PBAvailableRemainingFundsTotal >= transfer;
		}

		public static IndividualBudgetHelperModel GetIndividualBudgetStudentPacket(Decimal ibBudgetLimit,
			Decimal ibGlobalReserve, List<StudentPacketCourseHelperModel> courses)
		{
			IndividualBudgetHelperModel result = new IndividualBudgetHelperModel();

			result.IBBudgetLimit = ibBudgetLimit;

			result.IBAmountBudgeted = Math.Round(courses.Where(x => x.IsElective).Sum(x => x.PlanningBudgetTotal), 2);
			result.IBElectiveBalance = Math.Round(result.IBBudgetLimit + result.IBAmountBudgeted, 2);
			result.IBGlobalReserve = ibGlobalReserve;

			return result;
		}

		public static AchievedHelperModel GetAchievedStudentPacket(Decimal goalTotalUnits,
			List<StudentPacketCourseHelperModel> courses)
		{
			courses = courses.Where(x => x.IsFullSignature)
				.ToList();

			AchievedHelperModel result = new AchievedHelperModel();

			result.AchievedILPHrs = courses.Sum(x => x.ILPHours);
			result.AchievedContractHrs = Convert.ToInt32(courses.Sum(x => x.ContractHrs));
			result.AchievedCoreUnits = Math.Round(courses.Where(x => !x.IsElective).Sum(x => x.ILPHours)*0.5m/90m, 2);
			result.AchievedElectiveUnits = Math.Round(courses.Where(x => x.IsElective).Sum(x => x.ILPHours)*0.5m/90m, 2);
			result.AchievedTotalUnits = Math.Round(result.AchievedCoreUnits + result.AchievedElectiveUnits);
			var TotalUnitsPercent = result.AchievedTotalUnits*100;
			if (goalTotalUnits > 0)
			{
				result.AchievedEnrollmentPercent = Math.Round(TotalUnitsPercent/goalTotalUnits);
			}
			else
			{
				result.AchievedEnrollmentPercent = Math.Round(TotalUnitsPercent);
			}
			return result;
		}

		private static Decimal GetFundingAmountByGrade(Grade grade, SchoolYear schoolYear)
		{
			Decimal result = 0;

			switch (grade)
			{
				case Grade.K:
					result = schoolYear.FundingAmountGradeK;
					break;
				case Grade.G1:
					result = schoolYear.FundingAmountGrade1;
					break;
				case Grade.G2:
					result = schoolYear.FundingAmountGrade2;
					break;
				case Grade.G3:
					result = schoolYear.FundingAmountGrade3;
					break;
				case Grade.G4:
					result = schoolYear.FundingAmountGrade4;
					break;
				case Grade.G5:
					result = schoolYear.FundingAmountGrade5;
					break;
				case Grade.G6:
					result = schoolYear.FundingAmountGrade6;
					break;
				case Grade.G7:
					result = schoolYear.FundingAmountGrade7;
					break;
				case Grade.G8:
					result = schoolYear.FundingAmountGrade8;
					break;
				case Grade.G9:
					result = schoolYear.FundingAmountGrade9;
					break;
				case Grade.G10:
					result = schoolYear.FundingAmountGrade10;
					break;
				case Grade.G11:
					result = schoolYear.FundingAmountGrade11;
					break;
				case Grade.G12:
					result = schoolYear.FundingAmountGrade12;
					break;
				case Grade.G:
					result = schoolYear.FundingAmountGradeG;
					break;
				default:
					result = 0;
					break;
			}

			return result;
		}

		private static Decimal GetCoreUnitByGrade(Grade grade, SchoolYear schoolYear)
		{
			Decimal result = 0;

			switch (grade)
			{
				case Grade.K:
					result = schoolYear.CoreUnitGradeK;
					break;
				case Grade.G1:
					result = schoolYear.CoreUnitGrade1;
					break;
				case Grade.G2:
					result = schoolYear.CoreUnitGrade2;
					break;
				case Grade.G3:
					result = schoolYear.CoreUnitGrade3;
					break;
				case Grade.G4:
					result = schoolYear.CoreUnitGrade4;
					break;
				case Grade.G5:
					result = schoolYear.CoreUnitGrade5;
					break;
				case Grade.G6:
					result = schoolYear.CoreUnitGrade6;
					break;
				case Grade.G7:
					result = schoolYear.CoreUnitGrade7;
					break;
				case Grade.G8:
					result = schoolYear.CoreUnitGrade8;
					break;
				case Grade.G9:
					result = schoolYear.CoreUnitGrade9;
					break;
				case Grade.G10:
					result = schoolYear.CoreUnitGrade10;
					break;
				case Grade.G11:
					result = schoolYear.CoreUnitGrade11;
					break;
				case Grade.G12:
					result = schoolYear.CoreUnitGrade12;
					break;
				case Grade.G:
					result = schoolYear.CoreUnitGradeG;
					break;
				default:
					result = 0;
					break;
			}

			return result;
		}

		private static Decimal GetElectiveUnitByGrade(Grade grade, SchoolYear schoolYear)
		{
			Decimal result = 0;

			switch (grade)
			{
				case Grade.K:
					result = schoolYear.ElectiveUnitGradeK;
					break;
				case Grade.G1:
					result = schoolYear.ElectiveUnitGrade1;
					break;
				case Grade.G2:
					result = schoolYear.ElectiveUnitGrade2;
					break;
				case Grade.G3:
					result = schoolYear.ElectiveUnitGrade3;
					break;
				case Grade.G4:
					result = schoolYear.ElectiveUnitGrade4;
					break;
				case Grade.G5:
					result = schoolYear.ElectiveUnitGrade5;
					break;
				case Grade.G6:
					result = schoolYear.ElectiveUnitGrade6;
					break;
				case Grade.G7:
					result = schoolYear.ElectiveUnitGrade7;
					break;
				case Grade.G8:
					result = schoolYear.ElectiveUnitGrade8;
					break;
				case Grade.G9:
					result = schoolYear.ElectiveUnitGrade9;
					break;
				case Grade.G10:
					result = schoolYear.ElectiveUnitGrade10;
					break;
				case Grade.G11:
					result = schoolYear.ElectiveUnitGrade11;
					break;
				case Grade.G12:
					result = schoolYear.ElectiveUnitGrade12;
					break;
				case Grade.G:
					result = schoolYear.ElectiveUnitGradeG;
					break;
				default:
					result = 0;
					break;
			}

			return result;
		}
	}
}