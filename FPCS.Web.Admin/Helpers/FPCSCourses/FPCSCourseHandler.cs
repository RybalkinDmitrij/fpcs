﻿using FPCS.Core.Unity;
using FPCS.Data;
using FPCS.Data.Entities;
using FPCS.Data.Enums;
using FPCS.Data.Repo;
using FPCS.Web.Admin.Helpers.Access;
using FPCS.Web.Admin.Helpers.Access.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FPCS.Web.Admin.Helpers.FPCSCourses
{
    public static class FPCSCourseHandler
    {
        public static void AddFPCSStudentPacketCourse(Int64 fpcsCourseId, Int64 studentPacketId, Role role)
        {
            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                AccessActionModel access = AccessAction.IsCanAddNewCourseByStudentPacket(studentPacketId, role);

                if (!access.IsResolve)
                {
                    throw new Exception(access.Message);
                }

                var dbEntity = uow.GetRepo<IStudentPacketCourseRepo>().Add(studentPacketId, fpcsCourseId);
                uow.Commit();

                ILP dbGetILP = uow.GetRepo<IILPRepo>().GetByFPCSCourse(fpcsCourseId);

                if (dbGetILP != null)
                {
                    ILP saveILP = new ILP();
                    saveILP.ILPId = 0;
                    saveILP.ILPName = dbGetILP.ILPName;
                    saveILP.Instructor = dbGetILP.Instructor != null ? dbGetILP.Instructor : "";
                    saveILP.StudentPacketCourseId = dbEntity.StudentPacketCourseId;
                    saveILP.FPCSCourseId = null;
                    saveILP.CourseHrs = dbGetILP.CourseHrs;
                    saveILP.DescriptionCourse = dbGetILP.DescriptionCourse;
                    saveILP.Standards = dbGetILP.Standards;
                    saveILP.StudentActivities = dbGetILP.StudentActivities;
                    saveILP.MaterialsResources = dbGetILP.MaterialsResources;
                    saveILP.RoleAnyPeople = dbGetILP.RoleAnyPeople;
                    saveILP.EvaluationGradingPassFail = dbGetILP.EvaluationGradingPassFail;
                    saveILP.EvaluationGradingGradingScale = dbGetILP.EvaluationGradingGradingScale;
                    saveILP.EvaluationGradingOther = dbGetILP.EvaluationGradingOther;
                    saveILP.EvaluationGradingOtherExplain = dbGetILP.EvaluationGradingOtherExplain;
                    saveILP.EvaluatedMeasurableOutcomes = dbGetILP.EvaluatedMeasurableOutcomes;
                    saveILP.CourseSyllabus = dbGetILP.CourseSyllabus;
                    saveILP.GuardianILPModifications = dbGetILP.GuardianILPModifications;
                    saveILP.InstructorILPModifications = dbGetILP.InstructorILPModifications;

                    saveILP = uow.GetRepo<IILPRepo>().Add(saveILP);
                }

                List<GoodService> goodAndServices = uow.GetRepo<IGoodServiceRepo>()
                                                       .GetByFPCSCourse(fpcsCourseId)
                                                       .ToList();

                foreach (GoodService item in goodAndServices)
                {
                    GoodService dbSaveGoodService = new GoodService();
                    dbSaveGoodService.GoodServiceId = 0;
                    dbSaveGoodService.CreatedDate = DateTime.Now;
                    dbSaveGoodService.UpdatedDate = DateTime.Now;
                    dbSaveGoodService.StudentPacketCourseId = dbEntity.StudentPacketCourseId;
                    dbSaveGoodService.FPCSCourseId = null;
                    dbSaveGoodService.GoodOrService = item.GoodOrService;
                    dbSaveGoodService.RequisitionOrReimbursement = item.RequisitionOrReimbursement;
                    dbSaveGoodService.TypeRequisitionReimbursement = item.TypeRequisitionReimbursement;
                    dbSaveGoodService.VendorId = item.VendorId;
                    dbSaveGoodService.AdminComments = item.AdminComments;
                    dbSaveGoodService.ApprovalGivenBy = item.ApprovalGivenBy;
                    dbSaveGoodService.NumberOfUnits = item.NumberOfUnits;
                    dbSaveGoodService.UnitPrice = item.UnitPrice;
                    dbSaveGoodService.ShippingHandlingFees = item.ShippingHandlingFees;
                    dbSaveGoodService.LineItemName = item.LineItemName;
                    dbSaveGoodService.LineItemDesc = item.LineItemDesc;
                    dbSaveGoodService.SupplyType = item.SupplyType;
                    dbSaveGoodService.Count = item.Count;
                    dbSaveGoodService.CatalogMFG = item.CatalogMFG;
                    dbSaveGoodService.CatalogIssuePG = item.CatalogIssuePG;
                    dbSaveGoodService.UnitType = item.UnitType;
                    dbSaveGoodService.BarCode = item.BarCode;
                    dbSaveGoodService.Consumable = item.Consumable;
                    dbSaveGoodService.TypeTextbookCurriculum = item.TypeTextbookCurriculum;
                    dbSaveGoodService.Title = item.Title;
                    dbSaveGoodService.Author = item.Author;
                    dbSaveGoodService.PublisherISBN = item.PublisherISBN;
                    dbSaveGoodService.PublisherName = item.PublisherName;
                    dbSaveGoodService.Edition = item.Edition;
                    dbSaveGoodService.HighSchool = item.HighSchool;
                    dbSaveGoodService.NumberSemesters = item.NumberSemesters;
                    dbSaveGoodService.CostPerSemester = item.CostPerSemester;
                    dbSaveGoodService.ClassType = item.ClassType;
                    dbSaveGoodService.CourseTitle = item.CourseTitle;
                    dbSaveGoodService.CourseNumber = item.CourseNumber;
                    dbSaveGoodService.DateBirth = item.DateBirth;
                    dbSaveGoodService.Credits = item.Credits;
                    dbSaveGoodService.Semester = item.Semester;
                    dbSaveGoodService.TypeService = item.TypeService;
                    dbSaveGoodService.Description = item.Description;
                    dbSaveGoodService.BeginDate = item.BeginDate;
                    dbSaveGoodService.EndDate = item.EndDate;
                    dbSaveGoodService.VendorUnitType = item.VendorUnitType;

                    dbSaveGoodService = uow.GetRepo<IGoodServiceRepo>().Add(dbSaveGoodService);
                }

                uow.Commit();
            }
        }
    }
}