﻿using FPCS.Core.Unity;
using FPCS.Data;
using FPCS.Data.Entities;
using FPCS.Data.Enums;
using FPCS.Data.Repo;
using FPCS.Web.Admin.Helpers.Access.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FPCS.Web.Admin.Helpers.Access
{
    public static class AccessAction
    {
        public static AccessActionModel IsCanDeleteStudentPacketCourse(Int64 studentPacketCourseId, Role role)
        {
            AccessActionModel result = new AccessActionModel();

            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                var courseRepo = uow.GetRepo<IStudentPacketCourseRepo>();

                StudentPacketCourse data = courseRepo.Get(studentPacketCourseId);

                if (role != Role.Admin)
                {
                    if (data.AdminSignature.HasValue && data.AdminSignature.Value == Sign.Sign)
                    {
                        result.IsResolve = false;
                        result.Message = "Only an administrator can delete them already signed courses";
                    }
                    else
                    {
                        if (data.StudentPacket.IsLocked)
                        {
                            result.IsResolve = false;
                            result.Message = "Student Packet is locked";
                        }
                        else
                        {
                            result.IsResolve = true;
                        }
                    }
                }
                else
                {
                    result.IsResolve = true;
                }
            }

            return result;
        }

        public static AccessActionModel IsCanAddNewCourseByStudentPacket(Int64 studentPacketId, Role role)
        {
            AccessActionModel result = new AccessActionModel();

            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                if (role != Role.Admin)
                {
                    var cntCourses = uow.GetRepo<IStudentPacketCourseRepo>().GetByStudentPacket(studentPacketId).Count();
                    if (cntCourses >= 14)
                    {
                        result.IsResolve = false;
                        result.Message = "Only an administrator can add more than 14 courses";
                    }
                    else
                    {
                        result = IsNotLock(studentPacketId, role);
                    }
                }
                else
                {
                    result.IsResolve = true;
                }
            }

            return result;
        }

        public static AccessActionModel IsNotLock(Int64 studentPacketId, Role role)
        {
            AccessActionModel result = new AccessActionModel();

            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                var packetRepo = uow.GetRepo<IStudentPacketRepo>();

                StudentPacket data = packetRepo.Get(studentPacketId);

                return IsNotLock(data, role);
            }
        }

        public static AccessActionModel IsNotLock(StudentPacket data, Role role)
        {
            AccessActionModel result = new AccessActionModel();

            if (role != Role.Admin)
            {
                if (data.IsLocked)
                {
                    result.IsResolve = false;
                    result.Message = "Student Packet is locked";
                }
                else
                {
                    result.IsResolve = true;
                }
            }
            else
            {
                result.IsResolve = true;
            }

            return result;
        }
    }
}