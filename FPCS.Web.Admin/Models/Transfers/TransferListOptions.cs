﻿using FPCS.Core.jqGrid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FPCS.Web.Admin.Models.Transfers
{
    public class TransferListOptions
    {
        [GridProperty(ExtensionType.All, true, "DateCreate", FilterOperation.Equal)]
        public DateTime Date { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Contains)]
        public String Comments { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Contains)]
        public String CreatedBy { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Contains)]
        public String FromStudent { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Contains)]
        public String ToStudent { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Equal)]
        public Decimal Amount { get; set; }
    }
}