﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FPCS.Web.Admin.Models.Transfers
{
    public class TransferIndexModel
    {
        public Guid StudentId { get; set; }

        public String StudentName { get; set; }

        public Boolean IsLocked { get; set; }
    }
}