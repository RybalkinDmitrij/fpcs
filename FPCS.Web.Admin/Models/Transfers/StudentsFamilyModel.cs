﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FPCS.Web.Admin.Models.Transfers
{
    public class StudentsFamilyModel
    {
        public Guid DbUserId { get; set; }

        [Display(Name = "Students in Family")]
        public String Name { get; set; }

        [Display(Name = "Available Budget")]
        public Decimal AvailableBudget { get; set; }
    }
}