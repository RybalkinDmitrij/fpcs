﻿using FPCS.Data.Entities;
using FPCS.Data.Enums;
using FPCS.Core.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DEB = FPCS.Data.Entities;
using FPCS.Core.Unity;
using FPCS.Data;
using FPCS.Data.Repo;
using FPCS.Web.Admin.Helpers.BudgetStudent;

namespace FPCS.Web.Admin.Models.Transfers
{
    public class DetailTransferModel
    {
        public DetailTransferModel()
        {
        }

        public DetailTransferModel(Transfer dbEntity, Int32 schoolYearId)
        {
            TransferId = dbEntity.TransferId;
            StudentId = dbEntity.FromStudentId.HasValue ?
                dbEntity.FromStudentId.Value :
                dbEntity.ToStudentId.Value;
            FromStudentName = dbEntity.FromStudentId != null ?
                String.Format(@"{0}, {1}", dbEntity.FromStudent.LastName, dbEntity.FromStudent.FirstName) :
                "FPCS Account";
            ToStudentName = dbEntity.ToStudentId != null ?
                String.Format(@"{0}, {1}", dbEntity.ToStudent.LastName, dbEntity.ToStudent.FirstName) :
                "FPCS Account";
            TransferAmount = dbEntity.TransferAmount;
            Comment = dbEntity.Comment;

            Init(dbEntity.FromStudent != null ?
                    dbEntity.FromStudent.Families.FirstOrDefault() :
                    dbEntity.ToStudent.Families.FirstOrDefault(),
                 schoolYearId);
        }

        public Int64 TransferId { get; set; }

        public Guid StudentId { get; set; }

        [Display(Name = "Transfer Funds From Student")]
        public String FromStudentName { get; set; }

        [Display(Name = "Transfer Funds To Student")]
        public String ToStudentName { get; set; }

        [Display(Name = "Transfer Amount")]
        public Decimal TransferAmount { get; set; }

        [Display(Name = "Comment")]
        public String Comment { get; set; }

        public List<StudentsFamilyModel> StudentsInFamily { get; set; }

        public void Init(DEB.Family family, Int32 schoolYearId)
        {
            if (family == null)
            {
                StudentsInFamily = new List<StudentsFamilyModel>();

                var data = BudgetStudentHelper.GetBudgetStudentPacket(StudentId, schoolYearId);

                StudentsFamilyModel studentFamilyBudget = new StudentsFamilyModel();
                studentFamilyBudget.DbUserId = StudentId;
                studentFamilyBudget.Name = FromStudentName;
                studentFamilyBudget.AvailableBudget = data.PBAvailableRemainingFundsTotal;

                StudentsInFamily.Add(studentFamilyBudget);

                return;
            }

            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IStudentRepo>();

                var students = repo.GetByFamily(family.FamilyId, schoolYearId)
                                    .ToList()
                                    .Select(x => new Lookup<Guid> { Value = x.DbUserId, Text = String.Format(@"{0}, {1}", x.LastName, x.FirstName) })
                                    .ToList();

                students.Add(new Lookup<Guid> { Value = Guid.Empty, Text = "FPCS Account" });

                StudentsInFamily = new List<StudentsFamilyModel>();

                foreach (var item in students)
                {
                    if (item.Value == Guid.Empty) { continue; }

                    var data = BudgetStudentHelper.GetBudgetStudentPacket(item.Value, schoolYearId);

                    StudentsFamilyModel studentFamilyBudget = new StudentsFamilyModel();
                    studentFamilyBudget.DbUserId = item.Value;
                    studentFamilyBudget.Name = item.Text;
                    studentFamilyBudget.AvailableBudget = data.PBAvailableRemainingFundsTotal;

                    StudentsInFamily.Add(studentFamilyBudget);
                }
            }
        }
    }
}