﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FPCS.Web.Admin.Models.Transfers
{
    public class TransferMiniModel
    {
        public Int64 TransferId { get; set; }

        public DateTime DateCreate { get; set; }

        public String Comments { get; set; }

        public String CreatedBy { get; set; }

        public String FromStudent { get; set; }

        public String ToStudent { get; set; }

        public Decimal Amount { get; set; }
    }
}