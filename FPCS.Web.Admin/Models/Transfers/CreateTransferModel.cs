﻿using FPCS.Data.Entities;
using FPCS.Data.Enums;
using FPCS.Core.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DEB = FPCS.Data.Entities;
using FPCS.Core.Unity;
using FPCS.Data;
using FPCS.Data.Repo;
using FPCS.Web.Admin.Helpers.BudgetStudent;

namespace FPCS.Web.Admin.Models.Transfers
{
    public class CreateTransferModel
    {
        public CreateTransferModel()
        {
        }

        public CreateTransferModel(DEB.Student student, Int32 schoolYearId)
        {
            Init(student, schoolYearId);
        }

        public Int64 TransferId { get; set; }

        public Guid StudentId { get; set; }

        [Display(Name = "Transfer Funds From Student")]
        public Guid FromStudentId { get; set; }

        [Required]
        [Display(Name = "Transfer Funds To Student")]
        public Guid ToStudentId { get; set; }

        [Required]
        [Range(0, 999999, ErrorMessage = "Value must be more 0")]
        [Display(Name = "Transfer Amount")]
        public Decimal TransferAmount { get; set; }

        [Required]
        [Display(Name = "Comment")]
        public String Comment { get; set; }

        public SelectList StudentsFrom { get; set; }

        public SelectList Students { get; set; }

        public List<StudentsFamilyModel> StudentsInFamily { get; set; }

        public void Init(DEB.Student student, Int32 schoolYearId)
        {
            StudentId = student.DbUserId;
            FromStudentId = student != null ?
                student.DbUserId :
                Guid.Empty;

            var family = student != null ? student.Families.FirstOrDefault() : null;

            if (family == null)
            {
                StudentsInFamily = new List<StudentsFamilyModel>();

                var data = BudgetStudentHelper.GetBudgetStudentPacket(StudentId, schoolYearId);

                StudentsFamilyModel studentFamilyBudget = new StudentsFamilyModel();
                studentFamilyBudget.DbUserId = StudentId;
                studentFamilyBudget.Name = String.Format(@"{0}, {1}", student.LastName, student.FirstName);
                studentFamilyBudget.AvailableBudget = data.PBAvailableRemainingFundsTotal;

                StudentsInFamily.Add(studentFamilyBudget);

                return;
            }

            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IStudentRepo>();

                var students = repo.GetByFamily(family.FamilyId, schoolYearId)
                                    .ToList()
                                    .Select(x => new Lookup<Guid> { Value = x.DbUserId, Text = String.Format(@"{0}, {1}", x.LastName, x.FirstName) })
                                    .ToList();

                students.Add(new Lookup<Guid> { Value = Guid.Empty, Text = FPCSAccount });

                Students = new SelectList(students.ToList(), "Value", "Text");

                var studentsFrom = new List<Lookup<Guid>>();
                studentsFrom.Add(new Lookup<Guid> { Value = student.DbUserId, Text = String.Format(@"{0}, {1}", student.LastName, student.FirstName) });
                studentsFrom.Add(new Lookup<Guid> { Value = Guid.Empty, Text = FPCSAccount });

                StudentsFrom = new SelectList(studentsFrom, "Value", "Text");

                StudentsInFamily = new List<StudentsFamilyModel>();

                foreach (var item in students)
                {
                    if (item.Value == Guid.Empty) { continue; }

                    var data = BudgetStudentHelper.GetBudgetStudentPacket(item.Value, schoolYearId);

                    StudentsFamilyModel studentFamilyBudget = new StudentsFamilyModel();
                    studentFamilyBudget.DbUserId = item.Value;
                    studentFamilyBudget.Name = item.Text;
                    studentFamilyBudget.AvailableBudget = data.PBAvailableRemainingFundsTotal;

                    StudentsInFamily.Add(studentFamilyBudget);
                }
            }
        }

        private const String FPCSAccount = "FPCS Account";
    }
}