﻿using FPCS.Data.Entities;
using FPCS.Web.Admin.Models.GoodAndService;
using FPCS.Web.Admin.Models.ILP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FPCS.Web.Admin.Models.FPCSCourse
{
    public class PreDetailsModel
    {
        public PreDetailsModel(Int32 courseId)
        {
            StudentPacketCourseId = 0;
            CourseId = courseId;

            FPCSCourseDetailsModel = new FPCSCourseDetailsModel();
            TableGoodAndServiceModel = new TableGoodAndServiceModel();
            ILPModel = new ILPModel(StudentPacketCourseId, CourseId);
        }

        public Int32 StudentPacketCourseId { get; set; }
        public Int32 CourseId { get; set; }

        public FPCSCourseDetailsModel FPCSCourseDetailsModel { get; set; }
        public TableGoodAndServiceModel TableGoodAndServiceModel { get; set; }
        public ILPModel ILPModel { get; set; }
    }
}