﻿using System;

using FPCS.Core.jqGrid;

namespace FPCS.Web.Admin.Models.FPCSCourse
{
    public class FPCSCoursesListOptions
    {
        [GridProperty(ExtensionType.All, true)]
        public Int64? FPCSCourseId { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Contains)]
        public String Name { get; set; }

        [GridProperty(ExtensionType.All, true, "Teacher.FullName", FilterOperation.Contains)]
        public String Teacher { get; set; }

        [GridProperty(ExtensionType.All, true, "Guardian.FullName", FilterOperation.Contains)]
        public String Guardian { get; set; }

        [GridProperty(ExtensionType.All, true, "Vendor.LastName", FilterOperation.Contains)]
        public String Vendor { get; set; }

        [GridProperty(ExtensionType.All, true)]
        public Int32? Subject { get; set; }

        [GridProperty(ExtensionType.Filter, false)]
        public String AvailableSpots { get; set; }

        [GridProperty(ExtensionType.Filter, false)]
        public String Grade { get; set; }

        [GridProperty(ExtensionType.All, true)]
        public Int32? ScheduleRepetition { get; set; }

        [GridProperty(ExtensionType.Filter, false)]
        [GridProperty(ExtensionType.All, true)]
        public String IsActivated { get; set; }
    }
}