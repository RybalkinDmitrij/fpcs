﻿using FPCS.Data.Entities;
using FPCS.Web.Admin.Models.GoodAndService;
using FPCS.Web.Admin.Models.ILP;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using FPCS.Data.Enums;
using FPCS.Web.Admin.Models.SessionModels;

namespace FPCS.Web.Admin.Models.FPCSCourse
{
	public class PostMegaCreateModel
	{
		public PostMegaCreateModel()
		{
			
		}

		public Int64? StudentPacketId { get; set; }

		public Guid? StudentId { get; set; }

		public Guid? tempId { get; set; }

		public Int32? CourseSubjectId { get; set; }

		public Int32? SubjectId { get; set; }

		[Required(ErrorMessage = "Course is required")]
		public Int32 ASDCourseId { get; set; }

		public Decimal ASDCourseCredit { get; set; }

		public String ASDCourseDescription { get; set; }

		public Guid? TeacherId { get; set; }

		public Guid? GuardianId { get; set; }

		public Int32? VendorId { get; set; }
		
		[Required(ErrorMessage = "Course name is required")]
		public String Name { get; set; }

		public String Location { get; set; }

		public CourseClassType? ClassType { get; set; }
		
		[Required(ErrorMessage = "Semester numbers is required")]
		public IEnumerable<Int64> SemesterNumbers { get; set; }

		public DateTimeOffset ClassStartDate { get; set; }

		public DateTimeOffset ClassEndDate { get; set; }

		[Required(ErrorMessage = "Is activated is required")]
		public Boolean IsActivated { get; set; }
		
		public Int32? GradeLevel { get; set; }

		public Int32 ILPId { get; set; }

		public String Instructor { get; set; }

		public String ClassName { get; set; }

		public String SchoolYear { get; set; }

		[Required(ErrorMessage = "Course name is required")]
		public String ILPName { get; set; }

		public Int64 StudentPacketCourseId { get; set; }

		public Int64 FPCSCourseId { get; set; }

		public String DescriptionCourse { get; set; }

		public Boolean EvaluationGradingPassFail { get; set; }

		public Boolean EvaluationGradingGradingScale { get; set; }

		public Boolean EvaluationGradingOSN { get; set; }

		public Boolean EvaluationGradingOther { get; set; }

		public String EvaluationGradingOtherExplain { get; set; }

		public Boolean AddILPToBank { get; set; }

		public Boolean IsCourseGuardian { get; set; }

		public Boolean IsCourseVendor { get; set; }
	}
}