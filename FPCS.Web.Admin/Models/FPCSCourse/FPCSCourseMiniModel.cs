﻿using System;

namespace FPCS.Web.Admin.Models.FPCSCourse
{
	public class FPCSCourseMiniModel
	{
		public Int64 FPCSCourseId { get; set; }

		public String Name { get; set; }

		public String ClassType { get; set; }

		public Guid? TeacherId { get; set; }

		public String Teacher { get; set; }

		public Guid? GuardianId { get; set; }

		public String Guardian { get; set; }

		public Int32? VendorId { get; set; }

		public String Vendor { get; set; }

		public String Subject { get; set; }

		public String AvailableSpots { get; set; }

		public Decimal GradCredit { get; set; }

		public String IsActivated { get; set; }
	}
}