﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using FPCS.Data.Enums;

namespace FPCS.Web.Admin.Models.FPCSCourse
{
	public class FPCSCourseDetailsModel
	{
		[Display(Name = "FPCS Course")]
		public Int64 FPCSCourseId { get; set; }

		[Display(Name = "Course")]
		public String ASDCourse { get; set; }

		[Display(Name = "Unit")]
		public Decimal ASDCourseCredit { get; set; }

		[Display(Name = "Description")]
		public String ASDCourseDescription { get; set; }

		[Display(Name = "Subject")]
		public String ASDCourseSubject { get; set; }

		[Display(Name = "Teacher")]
		public String Teacher { get; set; }

		[Display(Name = "Guardian")]
		public String Guardian { get; set; }

		[Display(Name = "Vendor")]
		public String Vendor { get; set; }

		[Display(Name = "Name")]
		public String Name { get; set; }

		[Display(Name = "Location")]
		public String Location { get; set; }

		[Display(Name = "Class Type")]
		public CourseClassType? ClassType { get; set; }

		[Display(Name = "Class Type")]
		public String ClassTypeText { get; set; }
		
		[Display(Name = "Class Start Date")]
		public DateTimeOffset ClassStartDate { get; set; }

		[Display(Name = "Class End Date")]
		public DateTimeOffset ClassEndDate { get; set; }

		[Display(Name = "Semester")]
		public Semester Semester { get; set; }

		[Display(Name = "Activate FPCS course. (Please note, FPCS course will be accessed for use by other users)")]
		public Boolean IsActivated { get; set; }
	}
}