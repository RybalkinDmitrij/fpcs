﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FPCS.Web.Admin.Models.FPCSCourse
{
    public class ASDCourseModel
    {
        public Int32 Id { get; set; }

        public String Code { get; set; }

        public Decimal Credit { get; set; }

        public String Description { get; set; }
    }
}