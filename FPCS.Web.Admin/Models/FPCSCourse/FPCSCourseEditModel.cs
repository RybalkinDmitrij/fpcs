﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using FPCS.Core.Extensions;
using FPCS.Core.Unity;
using FPCS.Data;
using FPCS.Data.Repo;
using FPCS.Data.Enums;

namespace FPCS.Web.Admin.Models.FPCSCourse
{
	public class FPCSCourseEditModel
	{
		[Required]
		[Display(Name = "FPCS Course")]
		public Int64 FPCSCourseId { get; set; }

		[Required]
		[Display(Name = "Course")]
		public Int32 ASDCourseId { get; set; }

		[Display(Name = "Course FPCS subject")]
		public Int32? CourseSubjectId { get; set; }

		[Display(Name = "Additional FPCS subject")]
		public Int32? SubjectId { get; set; }

		[Display(Name = "Unit")]
		public Decimal ASDCourseCredit { get; set; }

		[Display(Name = "Description")]
		public String ASDCourseDescription { get; set; }

		[Display(Name = "Teacher")]
		public Guid? TeacherId { get; set; }

		[Display(Name = "Parent/Guardian")]
		public Guid? GuardianId { get; set; }

		[Display(Name = "Vendor")]
		public Int32? VendorId { get; set; }

		[Display(Name = "Teacher")]
		public String TeacherName { get; set; }

		[Display(Name = "Guardian")]
		public String GuardianName { get; set; }

		[Display(Name = "Vendor")]
		public String VendorName { get; set; }

		[Display(Name = "Name")]
		public String Name { get; set; }

		[Required]
		[Display(Name = "Course Description")]
		public String ILPName { get; set; }

		[Display(Name = "Location")]
		public String Location { get; set; }

		[Display(Name = "Class type")]
		public CourseClassType? ClassType { get; set; }
		
		[Display(Name = "Class Start Date")]
		public DateTimeOffset ClassStartDate { get; set; }

		[Display(Name = "Class End Date")]
		public DateTimeOffset ClassEndDate { get; set; }

		[Required]
		[Display(Name = "Semester")]
		public Semester SemesterNumber { get; set; }
		
		[Required]
		[Display(Name = "Activate FPCS course. (Please note, FPCS course will be accessed for use by other users)")]
		public Boolean IsActivated { get; set; }
		
		[Display(Name = "Grade Level")]
		public Int32? GradeLevel { get; set; }

		public SelectList ASDCourses { get; set; }

		public SelectList ASDCourses2 { get; set; }

		public List<Lookup<Int32, String, Int32, Int32>> ASDCourses3 { get; set; }

		public SelectList Grades { get; set; }

		public SelectList ClassTypes { get; set; }
		
		public SelectList Semesters { get; set; }

		public SelectList Subjects { get; set; }

		public List<Lookup<Int32, String, String>> Subjects2 { get; set; }

		public SelectList GradeLevels { get; set; }
		
		public List<ASDCourseModel> ASDCoursesHid { get; set; }

		public void Init(Int32 schoolYear, Guid dbUserId, Role userRole)
		{
			Grades = Grade.G1.ToSelectListUsingDescWithoutActive();
			ClassTypes = CourseClassType.HD.ToSelectListUsingDescWithoutActive();
			Semesters = SemesterNumber.ToSelectListUsingDesc();
			GradeLevels = TypeGrade.Elementary.ToSelectListUsingDescFromIds();

			using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
			{
				var dbASDCoursesGrp = uow.GetRepo<IASDCourseRepo>()
					.GetActivatedBySchoolYear(schoolYear)
					.GroupBy(x => new {TypeGrade = x.TypeGrade, SubjectId = x.SubjectId})
					.Select(x => new {TypeGrade = x.Key.TypeGrade, SubjectId = x.Key.SubjectId, Count = x.Count()})
					.ToList();

				var dbASDCourses = uow.GetRepo<IASDCourseRepo>()
					.GetActivatedBySchoolYear(schoolYear)
					.Select(x => new
					{
						ASDCourseId = x.ASDCourseId,
						SubjectId = x.SubjectId,
						SubjectName = x.Subject.Name,
						ExternalASDCourseId = x.ExternalASDCourseId,
						GradCredit = x.GradCredit,
						Name = x.Name,
						TypeGrade = (Int32) x.TypeGrade,
						Description = x.Description
					})
					.ToList();

				var asdCourses = dbASDCourses
					.Select(x => new Lookup<Int32>
					{
						Value = x.ASDCourseId,
						Text = x.SubjectName + " (" + x.ExternalASDCourseId + ")" + " - " + (!String.IsNullOrEmpty(x.Description)
							? x.Description.Length > 50
								? x.Description.Substring(0, 50)
								: x.Description
							: String.Empty)
					})
					.ToList();

				var asdCourses2 = dbASDCourses
					.Select(x => new Lookup<Int32>
					{
						Value = x.ASDCourseId,
						Text = x.SubjectName + " (" + x.ExternalASDCourseId + ")"
					})
					.ToList();

				ASDCourses = new SelectList(asdCourses, "Value", "Text");
				ASDCourses2 = new SelectList(asdCourses2, "Value", "Text");
				ASDCoursesHid = dbASDCourses.Select(x => new ASDCourseModel
				{
					Id = x.ASDCourseId,
					Code = x.ExternalASDCourseId,
					Credit = x.GradCredit,
					Description = x.Description
				})
					.ToList();

				ASDCourses3 = dbASDCourses
					.Select(x => new Lookup<Int32, String, Int32, Int32>
					{
						Value = x.ASDCourseId,
						Text = x.SubjectName + " (" + x.ExternalASDCourseId + ")" + " - " + (!String.IsNullOrEmpty(x.Description)
							? x.Description.Length > 50
								? x.Description.Substring(0, 50)
								: x.Description
							: String.Empty),
						Additional = x.TypeGrade,
						Additional2 = x.SubjectId
					})
					.ToList();

				var subjects =
					uow.GetRepo<ISubjectRepo>()
						.GetAll()
						.ToList()
						.Select(x => new Lookup<Int32, String, String>
						{
							Value = x.SubjectId,
							Text = x.Name,
							Additional =
								string.Join("-",
									dbASDCoursesGrp.Where(t => t.SubjectId == x.SubjectId)
										.Select(t => "TG" + ((int) t.TypeGrade).ToString() + ":" + t.Count.ToString())
										.ToList())
						})
						.ToList();

				Subjects = new SelectList(subjects, "Value", "Text");
				Subjects2 = subjects;
			}
		}
	}
}