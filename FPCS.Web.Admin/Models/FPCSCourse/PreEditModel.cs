﻿using FPCS.Data.Entities;
using FPCS.Web.Admin.Models.GoodAndService;
using FPCS.Web.Admin.Models.ILP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FPCS.Web.Admin.Models.FPCSCourse
{
	public class PreEditModel
	{
		public PreEditModel(Int32 courseId)
		{
			StudentPacketCourseId = 0;
			CourseId = courseId;

			FPCSCourseEditModel = new FPCSCourseEditModel();
			TableGoodAndServiceModel = new TableGoodAndServiceModel();
			ILPModel = new ILPModel(StudentPacketCourseId, CourseId);
		}

		public Int32 StudentPacketCourseId { get; set; }
		public Int32 CourseId { get; set; }

		public FPCSCourseEditModel FPCSCourseEditModel { get; set; }
		public TableGoodAndServiceModel TableGoodAndServiceModel { get; set; }
		public ILPModel ILPModel { get; set; }
	}
}