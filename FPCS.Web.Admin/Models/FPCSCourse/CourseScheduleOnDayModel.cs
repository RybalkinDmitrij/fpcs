﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace FPCS.Web.Admin.Models.FPCSCourse
{
    public class CourseScheduleOnDayModel
    {
        public Int64 CourseScheduleOnDayId { get; set; }

        public String StartTime { get; set; }

        public String EndTime { get; set; }

        public TimeSpan StartTimeSpan
        {
            get
            {
                DateTime dateTime = DateTime.ParseExact(this.StartTime, "hh:mm tt", CultureInfo.GetCultureInfo("en-US"));
                return dateTime.TimeOfDay;
            }

            set
            {
                var time = value.ToString();
                this.StartTime = DateTime.ParseExact(value.ToString(), "HH:mm:ss", CultureInfo.GetCultureInfo("en-US")).ToString("hh:mm tt");
            }
        }

        public TimeSpan EndTimeSpan
        {
            get
            {
                DateTime dateTime = DateTime.ParseExact(this.EndTime, "hh:mm tt", CultureInfo.GetCultureInfo("en-US"));
                return dateTime.TimeOfDay;
            }

            set
            {
                this.EndTime = DateTime.ParseExact(value.ToString(), "HH:mm:ss", CultureInfo.GetCultureInfo("en-US")).ToString("hh:mm tt");
            }
        }

        public DayOfWeek DayOfWeek { get; set; }
    }
}