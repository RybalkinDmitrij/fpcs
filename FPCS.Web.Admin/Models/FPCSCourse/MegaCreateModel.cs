﻿using FPCS.Data.Entities;
using FPCS.Web.Admin.Models.GoodAndService;
using FPCS.Web.Admin.Models.ILP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FPCS.Data.Enums;
using FPCS.Web.Admin.Models.SessionModels;

namespace FPCS.Web.Admin.Models.FPCSCourse
{
	public class MegaCreateModel
	{
		public MegaCreateModel()
		{
			
		}

		public MegaCreateModel(FPCS.Data.IUnitOfWork uow, Int64? studentPacketId, Guid? studentId, Semester? semester, SessionUserModel currentUser, SessionSchoolYearModel schoolYear)
		{
			TempGuid = Guid.NewGuid();

			StudentPacketCourseId = 0;
			CourseId = 0;

			FPCSCourseCreateModel = new FPCSCourseCreateModel(uow, studentPacketId, studentId, semester, currentUser);
            FPCSCourseCreateModel.Init(schoolYear.SchoolYearId, currentUser.UserId, currentUser.Role, studentId, studentPacketId, semester);

			TableGoodAndServiceModel = new TableGoodAndServiceModel();
			ILPModel = new ILPModel();
		}

		public Guid TempGuid { get; private set; }

		public Int32 StudentPacketCourseId { get; set; }
		public Int32 CourseId { get; set; }

		public FPCSCourseCreateModel FPCSCourseCreateModel { get; set; }
		public TableGoodAndServiceModel TableGoodAndServiceModel { get; set; }
		public ILPModel ILPModel { get; set; }
	}
}