﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using FPCS.Data.Enums;

namespace FPCS.Web.Admin.Models.FPCSCourse
{
	public class FPCSCoursePrintModel
	{
		[Display(Name = "FPCS Course")]
		public Int64 FPCSCourseId { get; set; }

		[Display(Name = "Course")]
		public String ASDCourse { get; set; }

		public String ASDCourseID { get; set; }

		[Display(Name = "Unit")]
		public Decimal ASDCourseCredit { get; set; }

		[Display(Name = "Description")]
		public String ASDCourseDescription { get; set; }

		[Display(Name = "Subject")]
		public String ASDCourseSubject { get; set; }

		[Display(Name = "Name")]
		public String Name { get; set; }

		[Display(Name = "Location ")]
		public String Location { get; set; }

		[Display(Name = "Class Start Date")]
		public DateTimeOffset ClassStartDate { get; set; }

		[Display(Name = "Class End Date")]
		public DateTimeOffset ClassEndDate { get; set; }

		[Display(Name = "Semester")]
		public Semester Semester { get; set; }

		[Display(Name = "Activate FPCS course. (Please note, FPCS course will be accessed for use by other users)")]
		public Boolean IsActivated { get; set; }
		
		public String PrintAddress { get; set; }

		public String PrintState { get; set; }

		public String PrintPhone { get; set; }

		public String PrintFax { get; set; }

		public String PrintDateNow { get; set; }
	}
}