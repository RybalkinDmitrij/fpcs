﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FPCS.Web.Admin.Models.GoodAndServiceApproval
{
    public class GoodAndServiceVendorModel
    {
        public Int32 VendorId { get; set; }

        public String VendorName { get; set; }

        public String Address { get; set; }

        public Decimal Sum { get; set; }

        public Decimal SumBalance { get; set; }
    }
}