﻿using FPCS.Core.Unity;
using FPCS.Data;
using FPCS.Data.Repo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FPCS.Web.Admin.Models.GoodAndServiceApproval
{
    public class TableVendorServiceModel
    {
        public TableVendorServiceModel()
        {
        }

        public TableVendorServiceModel(Int64 vendorId)
        {
            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IGoodServiceRepo>();

                var dbEntity = repo.Get(vendorId);

                Data = repo.GetByVendorHasStudentPacketCourse(vendorId)
                           .Select(x => new
                           {
                               VendorName = x.Vendor.BusinessName,
                               CourseName = x.StudentPacketCourse.FPCSCourse.Name,
                               StudentLastName = x.StudentPacketCourse.StudentPacket.Student.LastName,
                               StudentFirstName = x.StudentPacketCourse.StudentPacket.Student.FirstName,
                               Budget = 0,
                               Spent = 0,
                               BudgetAdjustment = 0,
                               Balance = 0
                           })
                           .ToList()
                           .Select(x => new VendorServiceModel()
                           {
                               VendorName = x.VendorName,
                               CourseName = x.CourseName,
                               StudentName = String.Format(@"{0}, {1}", x.StudentLastName, x.StudentFirstName),
                               Budget = x.Budget,
                               Spent = x.Spent,
                               BudgetAdjustment = x.BudgetAdjustment,
                               Balance = x.Balance
                           })
                           .ToList();

                Budget = Data.Sum(x => x.Budget);
                Spent = Data.Sum(x => x.Spent);
                BudgetAdjustment = Data.Sum(x => x.BudgetAdjustment);
                Balance = Data.Sum(x => x.Balance);
            }
        }

        public List<VendorServiceModel> Data { get; set; }

        public Decimal Budget { get; set; }

        public Decimal Spent { get; set; }

        public Decimal BudgetAdjustment { get; set; }

        public Decimal Balance { get; set; }
    }
}