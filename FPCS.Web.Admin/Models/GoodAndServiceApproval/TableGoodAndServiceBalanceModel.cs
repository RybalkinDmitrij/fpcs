﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FPCS.Web.Admin.Models.GoodAndServiceApproval
{
    public class TableGoodAndServiceBalanceModel
    {
        public List<GoodAndServiceMiniBalanceModel> Balances { get; set; }
    }
}