﻿using FPCS.Core.Unity;
using FPCS.Data;
using FPCS.Data.Entities;
using FPCS.Data.Enums;
using FPCS.Data.Repo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FPCS.Web.Admin.Models.GoodAndServiceApproval
{
    public class PreDetailModel
    {
        public PreDetailModel()
        {
        }

        public PreDetailModel(Int64 goodServiceId)
        {
            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                GoodService goodService = uow.GetRepo<IGoodServiceRepo>().Get(goodServiceId);

                GoodServiceId = goodService.GoodServiceId;
                GoodServiceId2 = Convert.ToInt32(goodService.GoodServiceId);
                GoodOrService = goodService.GoodOrService;
                RequisitionOrReimbursement = goodService.RequisitionOrReimbursement;
                TypeRequisitionReimbursement = goodService.TypeRequisitionReimbursement;
                VendorID = goodService.VendorId;
                FPCSCourseId = goodService.StudentPacketCourse.FPCSCourseId;
                StudentPacketCourseId = goodService.StudentPacketCourse.StudentPacketCourseId;
            }
        }

        public Int64 GoodServiceId { get; set; }

        public Int32 GoodServiceId2 { get; set; }

        public Int64 FPCSCourseId { get; set; }

        public Int64 StudentPacketCourseId { get; set; }

        public GoodOrService GoodOrService { get; set; }

        public RequisitionOrReimbursement RequisitionOrReimbursement { get; set; }

        public TypeRequisitionReimbursement TypeRequisitionReimbursement { get; set; }

        public Int32 VendorID { get; set; }
    }
}