﻿using FPCS.Data.Enums;
using FPCS.Core.Extensions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace FPCS.Web.Admin.Models.GoodAndServiceApproval
{
    public class ListPrintReimbursementModel
    {
        public ListPrintReimbursementModel()
        {
        }

        public List<PrintReimbursementModel> PrintReimbursements { get; set; }
    }
}