﻿using FPCS.Core.Unity;
using FPCS.Data;
using FPCS.Data.Enums;
using FPCS.Data.Repo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FPCS.Web.Admin.Models.GoodAndServiceApproval
{
    public class PreEditModel
    {
        public PreEditModel()
        {
        }

        public PreEditModel(Int64 goodServiceId)
        {
            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                var goodAndService = uow.GetRepo<IGoodServiceRepo>().Get(goodServiceId);

                EditModel = new EditModel();
                EditModel.GoodServiceId = goodServiceId;
                EditModel.IsClosed = goodAndService.IsClosed.HasValue ? goodAndService.IsClosed.Value : false;
                EditModel.Aprv = goodAndService.GoodOrServiceApproved.HasValue ? goodAndService.GoodOrServiceApproved.Value : GoodOrServiceApproved.b_appr;
                EditModel.CommentsRejectReason = goodAndService.CommentsRejectReason;

                TableBalanceModel = new TableGoodAndServiceBalanceModel();
                TableBalanceModel.Balances = uow.GetRepo<IGoodServiceBalanceRepo>()
                                                .GetByGoodService(goodServiceId)
                                                .ToList()
                                                .Select(x => new GoodAndServiceMiniBalanceModel(x))
                                                .ToList();

                GoodServiceId = goodServiceId;
            }
        }

        public Int64 GoodServiceId { get; set; }

        public EditModel EditModel { get; set; }

        public TableGoodAndServiceBalanceModel TableBalanceModel { get; set; }
    }
}