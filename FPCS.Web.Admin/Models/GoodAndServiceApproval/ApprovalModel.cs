﻿using FPCS.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FPCS.Web.Admin.Models.Student.Abstract;

namespace FPCS.Web.Admin.Models.GoodAndServiceApproval
{
	public class ApprovalModel: AbstractStudent
	{
		public Int64 GoodAndServiceId { get; set; }

		public String StudentName { get; set; }

		public String VendorName { get; set; }

        public String RequisitionOrReimbursements { get; set; }

        public String TypeRequisitionReimbursement { get; set; }

        public String Title { get; set; }

        public String Description { get; set; }

        public String PublisherISBN { get; set; }

        public String Comments { get; set; }

		public String FamilyEmail { get; set; }

		public String FamilyPhone { get; set; }

		public String DateOfBirth { get; set; }

		public String NumberOfUnits { get; set; }

		public String UnitPrice { get; set; }

		public String Shipping { get; set; }

		public String Total { get; set; }
	}
}