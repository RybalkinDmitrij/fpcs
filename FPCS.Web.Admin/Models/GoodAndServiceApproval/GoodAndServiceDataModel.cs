﻿using FPCS.Data.Entities;
using FPCS.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FPCS.Web.Admin.Models.Student.Abstract;

namespace FPCS.Web.Admin.Models.GoodAndServiceApproval
{
	public class GoodAndServiceDataModel: AbstractStudent
	{
		public Int64 GoodAndServiceId { get; set; }

		public GoodOrService GoodOrService { get; set; }

		public RequisitionOrReimbursement RequisitionOrReimbursement { get; set; }

		public String StudentsLastName { get; set; }

		public String StudentsFirstName { get; set; }

		public String Grade { get; set; }

		public String VendorName { get; set; }

		public Int64 ItemNumber { get; set; }

		public String User { get; set; }

		public Int32 NumberOfUnits { get; set; }

		public Decimal UnitPrice { get; set; }

		public Decimal ShippingHandlingFees { get; set; }

		public Decimal CostPerSemester { get; set; }

		public Int32 NumberOfSemester { get; set; }

		public TypeRequisitionReimbursement TypeRequisitionReimbursement { get; set; }

		public Boolean IsClosed { get; set; }

		public Sign? CourseStatus { get; set; }

		public DateTime? DateCourseStatus { get; set; }

		public GoodOrServiceApproved Aprv { get; set; }

		public String CommentsRejectReason { get; set; }

		public String CourseTitle { get; set; }

		public String Subject { get; set; }

		public String Category { get; set; }

		public Int64 FamilyNumber { get; set; }

		public String FamilyEmail { get; set; }

		public String FamilyPhone { get; set; }

		public ClassType? ClassType { get; set; }

		public String CourseNumber { get; set; }

		public DateTimeOffset DateOfBirth { get; set; }

		public Credits? Credits { get; set; }

		public String UserRequest { get; set; }

		public DateTime? DateRequest { get; set; }

        public String Title { get; set; }

        public String Description { get; set; }

        public String PublisherISBN { get; set; }

        public String Comments { get; set; }

		public ICollection<GoodServiceBalance> GoodServiceBalances { get; set; }

        public DateTime? DateSponsorSignature { get; set; }

        public DateTime? DateGuardianSignature { get; set; }

        public Sign? SponsorSignature { get; set; }

        public Sign? GuardianSignature { get; set; }

        public String SponsorTeacherEmail { get; set; }

        public Semester Semester { get; set; }

        public DateTimeOffset DateCreated { get; set; }
	}
}