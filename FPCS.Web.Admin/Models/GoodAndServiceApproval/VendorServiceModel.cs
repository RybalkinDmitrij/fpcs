﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FPCS.Web.Admin.Models.GoodAndServiceApproval
{
    public class VendorServiceModel
    {
        [Display(Name = "Vendor Name")]
        public String VendorName { get; set; }

        [Display(Name = "Course Name")]
        public String CourseName { get; set; }

        [Display(Name = "Student Name")]
        public String StudentName { get; set; }

        [Display(Name = "Budget")]
        public Decimal Budget { get; set; }

        [Display(Name = "Spent")]
        public Decimal Spent { get; set; }

        [Display(Name = "Budget Adjustment")]
        public Decimal BudgetAdjustment { get; set; }

        [Display(Name = "Balance")]
        public Decimal Balance { get; set; }
    }
}