﻿using FPCS.Data.Enums;
using FPCS.Core.Extensions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FPCS.Core.Unity;
using FPCS.Data;
using FPCS.Data.Repo;
using System.ComponentModel.DataAnnotations;

namespace FPCS.Web.Admin.Models.GoodAndServiceApproval
{
    public class EditModel
    {
        public EditModel()
        {
            Init();
        }

        public EditModel(Int64 goodServiceId)
        {
            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                var goodAndService = uow.GetRepo<IGoodServiceRepo>().Get(goodServiceId);

                GoodServiceId = goodServiceId;
                IsClosed = goodAndService.IsClosed.HasValue ? goodAndService.IsClosed.Value : false;
                Aprv = goodAndService.GoodOrServiceApproved.HasValue ? goodAndService.GoodOrServiceApproved.Value : GoodOrServiceApproved.b_appr;
                CommentsRejectReason = goodAndService.CommentsRejectReason;
            }

            Init();
        }

        public Int64 GoodServiceId { get; set; }

        [Display(Name = "Is Closed")]
        public Boolean IsClosed { get; set; }

        [Display(Name = "Approval")]
        public GoodOrServiceApproved Aprv { get; set; }

        [Display(Name = "Comments/Reject Reason")]
        public String CommentsRejectReason { get; set; }

        public SelectList Aprvs { get; set; }

        public void Init()
        {
            Aprvs = Aprv.ToSelectListUsingDesc();
        }
    }
}