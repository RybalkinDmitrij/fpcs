﻿using FPCS.Core.Unity;
using FPCS.Data;
using FPCS.Data.Entities;
using FPCS.Data.Repo;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FPCS.Web.Admin.Models.GoodAndServiceApproval
{
    public class GoodAndServiceMiniBalanceModel
    {
        public GoodAndServiceMiniBalanceModel()
        {
        }

        public GoodAndServiceMiniBalanceModel(Int64 goodServiceBalanceId, Int64 goodServiceId)
        {
            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IGoodServiceBalanceRepo>();

                var dbEntity = repo.Get(goodServiceBalanceId);

                if (dbEntity != null)
                {
                    GoodServiceBalanceId = dbEntity.GoodServiceBalanceId;
                    GoodServiceId = dbEntity.GoodServiceId.GetValueOrDefault(0);
                    PO = dbEntity.PO;
                    Invoice = dbEntity.Invoice;
                    Check = dbEntity.Check;
                    CheckDate = dbEntity.CheckDate.HasValue ? 
                                    dbEntity.CheckDate.Value.ToShortDateString() : 
                                    String.Empty;
                    Payee = dbEntity.Payee;
                    ReceiptDate = dbEntity.ReceiptDate.HasValue ? 
                                    dbEntity.ReceiptDate.Value.ToShortDateString() : 
                                    String.Empty;
                    Description = dbEntity.Description;
                    UnitPrice = dbEntity.UnitPrice;
                    QTY = dbEntity.QTY;
                    Shipping = dbEntity.Shipping;
                }
                else
                {
                    GoodServiceId = goodServiceId;
                }
            }
        }

        public GoodAndServiceMiniBalanceModel(GoodServiceBalance dbEntity)
        {
            GoodServiceId = dbEntity.GoodServiceId.GetValueOrDefault(0);
            GoodServiceBalanceId = dbEntity.GoodServiceBalanceId;
            PO = dbEntity.PO;
            Invoice = dbEntity.Invoice;
            Check = dbEntity.Check;
            CheckDate = dbEntity.CheckDate.HasValue ? 
                            dbEntity.CheckDate.Value.ToShortDateString() : 
                            String.Empty;
            Payee = dbEntity.Payee;
            ReceiptDate = dbEntity.ReceiptDate.HasValue ? 
                            dbEntity.ReceiptDate.Value.ToShortDateString() : 
                            String.Empty;
            Description = dbEntity.Description;
            UnitPrice = Math.Round(dbEntity.UnitPrice, 2);
            QTY = Math.Round(dbEntity.QTY, 2);
            Shipping = Math.Round(dbEntity.Shipping, 2);
        }

        public Int64 GoodServiceId { get; set; }

        public Int64 GoodServiceBalanceId { get; set; }

        [Display(Name = "PR #")]
        public String PO { get; set; }

        [Display(Name = "Invoice #")]
        public String Invoice { get; set; }

        [Display(Name = "Check #")]
        public String Check { get; set; }

        [Display(Name = "Check Date")]
        public String CheckDate { get; set; }

        [Display(Name = "Payee")]
        public String Payee { get; set; }

        [Display(Name = "Receipt Date")]
        public String ReceiptDate { get; set; }

        [Display(Name = "Description")]
        public String Description { get; set; }

        [Display(Name = "Unit Price")]
        public Decimal UnitPrice { get; set; }

        [Display(Name = "QTY")]
        public Decimal QTY { get; set; }

        [Display(Name = "Shipping")]
        public Decimal Shipping { get; set; }

        [Display(Name = "Total")]
        public Decimal Total 
        { 
            get
            {
                return Math.Round(UnitPrice * QTY + Shipping, 2);
            }
        }
    }
}