﻿using FPCS.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FPCS.Web.Admin.Models.Student.Abstract;

namespace FPCS.Web.Admin.Models.GoodAndServiceApproval
{
	public class GoodAndServiceApprovalModel: AbstractStudent
	{
		public Int64 GoodAndServiceId { get; set; }

		public String GoodOrService { get; set; }

		public String RequisitionOrReimbursement { get; set; }

		public String StudentsName { get; set; }

		public String StudentsLastName { get; set; }

		public String StudentsFirstName { get; set; }

		public String VendorName { get; set; }

		public Int64 ItemNumber { get; set; }

		public String UserRequest { get; set; }

		public String DateRequest { get; set; }

		public Decimal Budget { get; set; }

		public Decimal Balance { get; set; }

		public String CourseStatus { get; set; }

		public Boolean IsClosed { get; set; }

		public GoodOrServiceApproved GoodOrServiceApproved { get; set; }

		public String GoodOrServiceApprovedText { get; set; }

		public String CommentsRejectReason { get; set; }

        public String DateSponsorSignature { get; set; }

        public String DateGuardianSignature { get; set; }

        public String SponsorSignature { get; set; }

        public String GuardianSignature { get; set; }

        public String FamilyEmail { get; set; }

        public String SponsorTeacherEmail { get; set; }

        public Semester Semester { get; set; }

        public String DateCreated { get; set; }
	}
}