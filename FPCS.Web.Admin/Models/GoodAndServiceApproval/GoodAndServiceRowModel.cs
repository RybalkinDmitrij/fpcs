﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FPCS.Web.Admin.Models.GoodAndServiceApproval
{
    public class GoodAndServiceRowModel
    {
        public Int32 GoodAndServiceId { get; set; }

        public Boolean IsClosed { get; set; }

        public String CommentsRejectReason { get; set; }
    }
}