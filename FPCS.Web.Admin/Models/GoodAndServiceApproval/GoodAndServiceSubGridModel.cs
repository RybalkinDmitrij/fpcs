﻿using FPCS.Core.Unity;
using FPCS.Data;
using FPCS.Data.Entities;
using FPCS.Data.Repo;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FPCS.Web.Admin.Models.GoodAndServiceApproval
{
    public class GoodAndServiceSubGridModel
    {
        public Int64? GoodServiceId { get; set; }

        public Int64 GoodServiceBalanceId { get; set; }

        public String PO { get; set; }

        public String Invoice { get; set; }

        public String Check { get; set; }

        public DateTime? CheckDate { get; set; }

        public String Payee { get; set; }

        public DateTime? ReceiptDate { get; set; }

        public String Description { get; set; }

        public Decimal UnitPrice { get; set; }

        public Decimal QTY { get; set; }

        public Decimal Shipping { get; set; }

        public Decimal Total
        {
            get
            {
                return Math.Round(UnitPrice * QTY + Shipping, 2);
            }
        }
    }
}