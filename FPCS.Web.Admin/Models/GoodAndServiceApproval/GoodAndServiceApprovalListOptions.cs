﻿using System;

using FPCS.Core.jqGrid;

namespace FPCS.Web.Admin.Models.GoodAndServiceApproval
{
    public class GoodAndServiceApprovalListOptions
    {
        [GridProperty(ExtensionType.Sort, true)]
        public Int64? GoodAndServiceId { get; set; }

        [GridProperty(ExtensionType.Sort, true)]
        public Int32? GoodOrService { get; set; }

        [GridProperty(ExtensionType.Sort, true)]
        public Int32? RequisitionOrReimbursement { get; set; }

        [GridProperty(ExtensionType.Sort, true)]
        public String StudentsName { get; set; }

        [GridProperty(ExtensionType.Sort, true)]
        public String VendorName { get; set; }

        [GridProperty(ExtensionType.Sort, true)]
        public String User { get; set; }

        [GridProperty(ExtensionType.Sort, true)]
        public Int32? IsClosed { get; set; }

        [GridProperty(ExtensionType.Sort, true)]
        public Int32? CourseStatus { get; set; }

        [GridProperty(ExtensionType.Sort, true)]
        public Int32? GoodOrServiceApprovedText { get; set; }

        [GridProperty(ExtensionType.Sort, true)]
        public Int64? ItemNumber { get; set; }

        [GridProperty(ExtensionType.Sort, true)]
        public Decimal? Budget { get; set; }

        [GridProperty(ExtensionType.Sort, true)]
        public Decimal? Balance { get; set; }

        [GridProperty(ExtensionType.Sort, true)]
        public Int32? SponsorSignature { get; set; }

        [GridProperty(ExtensionType.Sort, true)]
        public Int32? GuardianSignature { get; set; }
    }
}