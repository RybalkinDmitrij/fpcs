﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using FPCS.Core.Unity;
using FPCS.Data;
using FPCS.Data.Repo;
using FPCS.Data.Enums;
using FPCS.Core.Extensions;

namespace FPCS.Web.Admin.Models.Vendor
{
	public class VendorCreateModel
	{
		[Required]
		[Display(Name = "Business Name")]
		public String BusinessName { get; set; }

		[Display(Name = "First Name")]
		public String FirstName { get; set; }

		[Display(Name = "Last Name")]
		public String LastName { get; set; }

		[Display(Name = "Status")]
		public VendorStatus? Status { get; set; }

		[Display(Name = "Status Comments")]
		public String StatusComments { get; set; }

		[Display(Name = "Street Address")]
		public String StreetAddress { get; set; }

		[Display(Name = "Street Address City")]
		public String StreetAddressCity { get; set; }

		[Display(Name = "Street Address State")]
		public Int64? StreetAddressStateId { get; set; }

		[Display(Name = "Street Address Zip Code")]
		public String StreetAddressZipCode { get; set; }

		[Display(Name = "Mailing Address")]
		public String MailingAddress { get; set; }

		[Display(Name = "Mailing Address City")]
		public String MailingAddressCity { get; set; }

		[Display(Name = "Mailing Address State")]
		public Int64? MailingAddressStateId { get; set; }

		[Display(Name = "Mailing Address Zip Code")]
		public String MailingAddressZipCode { get; set; }

		[Display(Name = "Phone Number")]
		public String Phone { get; set; }

		[Display(Name = "Fax Number")]
		public String Fax { get; set; }

		[Display(Name = "Email Address")]
		public String Email { get; set; }

		[Display(Name = "Location")]
		public Location? Location { get; set; }

		[Display(Name = "Business Website")]
		public String BusinessWebsite { get; set; }

		[Display(Name = "Employer Identification or SSN #")]
		public String EmployerIdentification { get; set; }

		[Display(Name = "AK Business License #")]
		public String AKBusinessLicense { get; set; }

		[Display(Name = "License Expiration")]
		public DateTime? LicenseExpiration { get; set; }

		[Display(Name = "Insurance Expiration")]
		public DateTime? InsuranceExpiration { get; set; }

		[Display(Name = "Services Provied")]
		public IEnumerable<LightServiceModel> Services { get; set; }

		[Display(Name = "Please list any training/education/experience in this or a related field")]
		public String TrainingEducationExperience { get; set; }

		[Display(Name = "Other comments about services provided")]
		public String CommentsAboutServices { get; set; }

		[Display(Name = "How do you charge?")]
		public VendorUnitType? VendorUnitType { get; set; }

		[Display(Name = "Other Charge Method")]
		public String OtherChargeMethod { get; set; }

		[Display(Name = "How much do you charge?")]
		public Decimal? Price { get; set; }

		[Display(Name = "Contract Starting Date")]
		public DateTime? ContractStartingDate { get; set; }

		[Display(Name = "Are you Non-Profit?")]
		public Boolean IsNonProfit { get; set; }

		[Display(Name = "Have you ever been convicted of a misdemeanor or felony?")]
		public Boolean IsMisdemeanorOrFelony { get; set; }

		[Display(Name = "Would you consent to a background check and submit to fingerprinting?")]
		public Boolean IsBackgroundCheckFingerprinting { get; set; }

		[Display(Name = "Do you have children who are currently enrolled in FPCS?")]
		public Boolean IsHaveChildrenCurrentlyEnrolled { get; set; }

		[Display(Name = "Are you a Retired Certificated ASD Teacher?")]
		public Boolean IsRetiredCertificatedASDTeacher { get; set; }

		[Display(Name = "Are you, or a member of your immediate family, an ASD employee or on the ASD eligible for hire list?")]
		public Boolean IsASDEmployeeEligibleHireList { get; set; }

		[Display(Name = "Are you currently available to provide services?")]
		public Boolean IsCurrentlyAvailableProvideServices { get; set; }

		[Display(Name = "Do you provide online Curriculum services?")]
		public Boolean IsProvideOnlineCurriculumServices { get; set; }

		[Display(Name = "Course provider")]
		public Boolean IsCourseProvider { get; set; }

		[Display(Name = "Cost")]
		public Decimal CostCourseProvider { get; set; }

		public SelectList StreetAddressStates { get; set; }

		public SelectList MailingAddressStates { get; set; }

		public SelectList ServiceVendors { get; set; }
		
		public SelectList Statuses { get; set; }

		public SelectList Locations { get; set; }

		public SelectList VendorUnitTypes { get; set; }

		public void Init()
		{
			using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
			{
				var states = uow.GetRepo<IStateRepo>()
					.GetAll()
					.Select(x => new Lookup<Int64> {Value = x.StateId, Text = x.Code})
					.ToList();

				var serviceVendors = uow.GetRepo<IServiceVendorRepo>()
					.GetAll()
					.Select(x => new {Value = x.ServiceVendorId, Type = x.Subject.Name, Name = x.Name})
					.ToList()
					.Select(x => new Lookup<Int32> {Value = x.Value, Text = String.Format(@"{0}: {1}", x.Type, x.Name)})
					.ToList();

				StreetAddressStates = new SelectList(states, "Value", "Text");
				MailingAddressStates = new SelectList(states, "Value", "Text");
				ServiceVendors = new SelectList(serviceVendors, "Value", "Text");

				Statuses = VendorStatus.Approved.ToSelectListUsingDescWithoutActive();
				Locations = Data.Enums.Location.Downtown.ToSelectListUsingDescWithoutActive();
				VendorUnitTypes = Data.Enums.VendorUnitType.Semester.ToSelectListUsingDescWithoutActive();
			}
		}
	}
}