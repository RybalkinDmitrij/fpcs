﻿using FPCS.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FPCS.Web.Admin.Models.Vendor
{
    public class LightServiceModel
    {
        public Int32 ServiceId { get; set; }

        public String Name { get; set; }

        public String Type { get; set; } 
    }
}