﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FPCS.Web.Admin.Models.Vendor
{
    public class VendorMiniModel
    {
        [Display(Name = "Vendor Id")]
        public Int32 VendorId { get; set; }

        [Display(Name = "Name")]
        public String Name { get; set; }

        [Display(Name = "Name")]
        public String ContactName { get; set; }

        [Display(Name = "Status")]
        public String Status { get; set; }

        [Display(Name = "Vendor Type")]
        public String VendorType { get; set; }

        [Display(Name = "Address")]
        public String Address { get; set; }

        [Display(Name = "Phone")]
        public String Phone { get; set; }

        [Display(Name = "Email")]
        public String Email { get; set; }

        [Display(Name = "Website")]
        public String Website { get; set; }

        [Display(Name = "Services")]
        public String Services { get; set; }

        [Display(Name = "Course Description")]
        public String CourseDescription { get; set; }

        public Int64? FileStoreId { get; set; }
    }
}