﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FPCS.Web.Admin.Models.Vendor
{
	public class TableGoodServiceVendorPrintModel
	{
		public List<GoodServiceVendorPrintModel> Data { get; set; }

		public Decimal TotalBudget
		{
			get { return this.Data.Sum(x => x.Budget.HasValue ? x.Budget.Value : 0); }
		}

		public String TotalBudgetStr
		{
			get { return this.TotalBudget.ToString("C"); }
		}

		public Decimal TotalBalance
		{
			get { return this.Data.Sum(x => x.Balance.HasValue ? x.Balance.Value : 0); }
		}

		public String TotalBalanceStr
		{
			get { return this.TotalBalance.ToString("C"); }
		}

		public String VendorName { get; set; }

		public String PrintAddress { get; set; }

		public String PrintState { get; set; }

		public String PrintPhone { get; set; }

		public String PrintFax { get; set; }

		public String PrintDateNow { get; set; }
	}
}