﻿using System;

using FPCS.Core.jqGrid;
using FPCS.Data.Enums;

namespace FPCS.Web.Admin.Models.Vendor
{
    public class VendorListOptions
    {
        [GridProperty(ExtensionType.All, true)]
        public Int32? VendorId { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.StartsWith)]
        public String Name { get; set; }

        [GridProperty(ExtensionType.All, true, "LastName", FilterOperation.Contains)]
        public String ContactName { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Equal)]
        public Int32? Status { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Equal)]
        public Int32? VendorType { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Contains)]
        public String Address { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Contains)]
        public String Phone { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Contains)]
        public String Email { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Contains)]
        public String Website { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Contains)]
        public String Services { get; set; }
    }
}