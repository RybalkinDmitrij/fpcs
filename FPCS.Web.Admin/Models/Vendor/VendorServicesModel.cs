﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace FPCS.Web.Admin.Models.Vendor
{
    public class VendorServicesModel
    {
        [Display(Name = "Vendor services")]
        public IEnumerable<LightServiceModel> VendorServices { get; set; }

        public SelectList Services { get; set; }
    }
}