﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FPCS.Web.Admin.Models.Vendor
{
	public class GoodServiceVendorPrintModel
	{
		public String Title { get; set; }

		public String Name { get; set; }

		public String GuardianName { get; set; }

		public String Email { get; set; }

		public String Phone { get; set; }

		public String SponsorTeacher { get; set; }

		public String CourseName { get; set; }

		public Decimal? Budget { get; set; }

		public String BudgetStr
		{
			get { return this.Budget.HasValue ? this.Budget.Value.ToString("C") : (0).ToString("C"); }
		}

		public Decimal? Balance { get; set; }

		public String BalanceStr
		{
			get { return this.Balance.HasValue ? this.Balance.Value.ToString("C") : (0).ToString("C"); }
		}
	}
}