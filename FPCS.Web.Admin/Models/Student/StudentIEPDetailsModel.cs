﻿using FPCS.Data.Enums;
using DEB = FPCS.Data.Entities;
using FPCS.Core.Extensions;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FPCS.Web.Admin.Models.Student
{
    public class StudentIEPDetailsModel
    {
        public StudentIEPDetailsModel()
        {
        }

        public StudentIEPDetailsModel(DEB.Student dbEntity)
        {
            DbUserId = dbEntity.DbUserId;
            IEPHasChildEligible = dbEntity.IEPHasChildEligible.GetValueOrDefault(false);
            IEPGiftedTalented = dbEntity.IEPGiftedTalented.GetValueOrDefault(false);
            IEPDeafBlindness = dbEntity.IEPDeafBlindness.GetValueOrDefault(false);
            IEPHearingImpairment = dbEntity.IEPHearingImpairment.GetValueOrDefault(false);
            IEPMultipleDisability = dbEntity.IEPMultipleDisability.GetValueOrDefault(false);
            IEPTraumaticBrainInjury = dbEntity.IEPTraumaticBrainInjury.GetValueOrDefault(false);
            IEPAutismAsbergerSyndrome = dbEntity.IEPAutismAsbergerSyndrome.GetValueOrDefault(false);
            IEPEarlyChildhoodDevelopmentalDelay = dbEntity.IEPEarlyChildhoodDevelopmentalDelay.GetValueOrDefault(false);
            IEPSpecificLearningDisability = dbEntity.IEPSpecificLearningDisability.GetValueOrDefault(false);
            IEPOrthopedicImpairment = dbEntity.IEPOrthopedicImpairment.GetValueOrDefault(false);
            IEPSpeechLanguageImpairment = dbEntity.IEPSpeechLanguageImpairment.GetValueOrDefault(false);
            IEPDeafness = dbEntity.IEPDeafness.GetValueOrDefault(false);
            IEPEmotionalDisturbance = dbEntity.IEPEmotionalDisturbance.GetValueOrDefault(false);
            IEPMentalRetardation = dbEntity.IEPMentalRetardation.GetValueOrDefault(false);
            IEPOtherHealthImpairment = dbEntity.IEPOtherHealthImpairment.GetValueOrDefault(false);
            IEPVisualImpairment = dbEntity.IEPVisualImpairment.GetValueOrDefault(false);
            IEPHasChildReceiving = dbEntity.IEPHasChildReceiving.GetValueOrDefault(false);
            IEPHasChildFormally = dbEntity.IEPHasChildFormally.GetValueOrDefault(false);
            IEPIsExpirationDate = dbEntity.IEPIsExpirationDate;
            IEPIsNextEligibilityEvaluation = dbEntity.IEPIsNextEligibilityEvaluation;
            IEPHasChildLearnedAnotherLanguage = dbEntity.IEPHasChildLearnedAnotherLanguage.GetValueOrDefault(false);
            OtherLanguage = dbEntity.IEPHasChildExperiencedLearned;
            IEPIsChildEligibleBilingualProgram = dbEntity.IEPIsChildEligibleBilingualProgram.GetValueOrDefault(false);
            IEPHasChildCurrentlyReceivingServicesBilingualProgram = dbEntity.IEPHasChildCurrentlyReceivingServicesBilingualProgram.GetValueOrDefault(false);
            IEPHasChildReceivingServicesMigrantEducation = dbEntity.IEPHasChildReceivingServicesMigrantEducation.GetValueOrDefault(false);
        }

        public Guid DbUserId { get; set; }

        [Display(Name = "Has your child ever been eligible for Special Education Services?")]
        public Boolean IEPHasChildEligible { get; set; }

        [Display(Name = "Gifted/Talented")]
        public Boolean IEPGiftedTalented { get; set; }

        [Display(Name = "Deaf-Blindness")]
        public Boolean IEPDeafBlindness { get; set; }

        [Display(Name = "Hearing Impairment")]
        public Boolean IEPHearingImpairment { get; set; }

        [Display(Name = "Multiple Disability")]
        public Boolean IEPMultipleDisability { get; set; }

        [Display(Name = "Traumatic Brain Injury")]
        public Boolean IEPTraumaticBrainInjury { get; set; }

        [Display(Name = "Autism/Asberger's Syndrome")]
        public Boolean IEPAutismAsbergerSyndrome { get; set; }

        [Display(Name = "Early Childhood Developmental Delay")]
        public Boolean IEPEarlyChildhoodDevelopmentalDelay { get; set; }

        [Display(Name = "Specific Learning Disability")]
        public Boolean IEPSpecificLearningDisability { get; set; }

        [Display(Name = "Orthopedic Impairment")]
        public Boolean IEPOrthopedicImpairment { get; set; }

        [Display(Name = "Speech or Language Impairment")]
        public Boolean IEPSpeechLanguageImpairment { get; set; }

        [Display(Name = "Deafness")]
        public Boolean IEPDeafness { get; set; }

        [Display(Name = "Emotional Disturbance")]
        public Boolean IEPEmotionalDisturbance { get; set; }

        [Display(Name = "Mental Retardation")]
        public Boolean IEPMentalRetardation { get; set; }

        [Display(Name = "Other Health Impairment")]
        public Boolean IEPOtherHealthImpairment { get; set; }

        [Display(Name = "Visual Impairment")]
        public Boolean IEPVisualImpairment { get; set; }

        [Display(Name = "Has your child been receiving special education services?")]
        public Boolean IEPHasChildReceiving { get; set; }

        [Display(Name = "Has your child been formally exited from special education?")]
        public Boolean IEPHasChildFormally { get; set; }

        [Display(Name = "What is the expiration date of the most current IEP?")]
        public DateTime? IEPIsExpirationDate { get; set; }

        [Display(Name = "When is the next eligibility evaluation due?")]
        public DateTime? IEPIsNextEligibilityEvaluation { get; set; }

        [Display(Name = "Has your child ever learned another language besides English?")]
        public Boolean IEPHasChildLearnedAnotherLanguage { get; set; }

        [Display(Name = "If so, what language or culture has your child experienced or learned?")]
        public OtherLanguage? OtherLanguage { get; set; }

        [Display(Name = "Is your child eligible for the Bilingual Program?")]
        public Boolean IEPIsChildEligibleBilingualProgram { get; set; }

        [Display(Name = "Has your child currently been receiving services in the Bilingual Program?")]
        public Boolean IEPHasChildCurrentlyReceivingServicesBilingualProgram { get; set; }

        [Display(Name = "Has your child been receiving services under the Migrant Education Program?")]
        public Boolean IEPHasChildReceivingServicesMigrantEducation { get; set; }

        public SelectList OtherLanguages { get; set; }

        public void Init()
        {
            OtherLanguage ol = Data.Enums.OtherLanguage.English;
            OtherLanguages = ol.ToSelectListUsingDescWithNull();
        }
    }
}