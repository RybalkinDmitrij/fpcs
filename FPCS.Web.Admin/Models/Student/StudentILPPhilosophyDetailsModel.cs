﻿using FPCS.Data.Enums;
using DEB = FPCS.Data.Entities;
using FPCS.Core.Extensions;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FPCS.Core.Unity;
using FPCS.Data;
using FPCS.Data.Repo;

namespace FPCS.Web.Admin.Models.Student
{
    public class StudentILPPhilosophyDetailModel
    {
        public StudentILPPhilosophyDetailModel()
        {
        }

        public StudentILPPhilosophyDetailModel(DEB.Student dbEntity)
        {
            DbUserId = dbEntity.DbUserId;
            Value = dbEntity.ILPPhilosophy;

            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IFamilyRepo>();

                Students = repo.GetBrotherAndSisterByStudent(DbUserId)
                    .Select(x => new StudentILPPhilosophyFamilyModel(x, dbEntity.ILPPhilosophy))
                    .ToList();
            }
        }

        public Guid DbUserId { get; set; }

        [Display(Name = "An Course Description educational philosophy statement is required in your Student Packet. This philosophical statement will be used to represent your educational beliefs as reflected in the development of your child’s Student Packet. Below enter the educational philosophy that you would like included in the Student Packet for this child.")]
        public String Value { get; set; }

        public List<StudentILPPhilosophyFamilyModel> Students { get; set; }
    }
}