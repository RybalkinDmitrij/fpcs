﻿using System;

using FPCS.Core.jqGrid;
using FPCS.Data.Enums;

namespace FPCS.Web.Admin.Models.Student
{
    public class StudentsListOptions
    {
        [GridProperty(ExtensionType.All, true)]
        public Guid? DbUserId { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Contains)]
        public String FullName { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Contains)]
        public String Email { get; set; }

        [GridProperty(ExtensionType.Filter, false)]
        [GridProperty(ExtensionType.All, true)]
        public String IsLocked { get; set; }

        [GridProperty(ExtensionType.Filter, false)]
        public Sex? Sex { get; set; }

        [GridProperty(ExtensionType.Filter, false)]
        public Grade? Grade { get; set; }

        [GridProperty(ExtensionType.Filter, false)]
        public EnrollmentStatus? EnrollmentStatus { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Contains)]
        public String Guardians { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Equal)]
        public String ZangleID { get; set; }
    }
}