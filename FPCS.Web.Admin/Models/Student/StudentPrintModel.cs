﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FPCS.Web.Admin.Models.Student
{
    public class StudentPrintModel
    {
        public Guid DbUserId { get; set; }

        public String FullName { get; set; }

        public String Email { get; set; }

        public String IsLocked { get; set; }

        public String Sex { get; set; }

        public String Grade { get; set; }

        public String EnrollmentStatus { get; set; }

        public String EnrollmentDate { get; set; }

        public String WithdrawalDate { get; set; }

        public String Guardians { get; set; }

        public String ZangleID { get; set; }
    }
}