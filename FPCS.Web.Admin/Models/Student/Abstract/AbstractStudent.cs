﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DEB = FPCS.Data.Entities;

namespace FPCS.Web.Admin.Models.Student.Abstract
{
    public class AbstractStudent
    {
        public Boolean IsWithdrawal { get; set; }
    }
}