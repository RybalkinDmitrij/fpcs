﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FPCS.Web.Admin.Models.Student
{
    public class StudentMiniModel
    {
        public Guid DbUserId { get; set; }

        [Display(Name = "Full name")]
        public String FullName { get; set; }

        [Display(Name = "Email Address")]
        public String Email { get; set; }

        public String IsLocked { get; set; }

        [Display(Name = "Sex")]
        public String Sex { get; set; }

        [Display(Name = "Grade")]
        public String Grade { get; set; }

        [Display(Name = "Enrollment Status")]
        public String EnrollmentStatus { get; set; }

        [Display(Name = "Withdrawal Date")]
        public String WithdrawalDate { get; set; }

        [Display(Name = "Enrollment Date")]
        public String EnrollmentDate { get; set; }

        [Display(Name = "Guardians")]
        public String Guardians { get; set; }

        [Display(Name = "Phone")]
        public String Phone { get; set; }

        [Display(Name = "Zangle ID")]
        public String ZangleID { get; set; }
    }
}