﻿using FPCS.Core.Unity;
using FPCS.Data;
using FPCS.Data.Enums;
using FPCS.Data.Repo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FPCS.Web.Admin.Models.Student
{
    public class StudentIndexModel
    {
        public String Sex { get; set; }

        public String Grade { get; set; }

        public String EnrollmentStatus { get; set; }

        public SelectList Students { get; set; }

        public SelectList Teachers { get; set; }

        public SelectList FPCSCourses { get; set; }

        public void Init(Int32 schoolYear)
        {
            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                var repoStudent = uow.GetRepo<IDbUserRepo>();
                var repoTeacher = uow.GetRepo<IDbUserRepo>();
                var repoFPCSCourse = uow.GetRepo<IFPCSCourseRepo>();

                var students = repoStudent.GetByRoles(schoolYear, Role.Student)
                    .Select(x => new
                    {
                        LastName = x.LastName,
                        FirstName = x.FirstName,
                        DbUserId = x.DbUserId
                    })
                    .ToList()
                    .Select(x => new SelectListItem()
                    {
                        Text = x.LastName + ", " + x.FirstName,
                        Value = x.DbUserId.ToString()
                    })
                    .ToList();

                students.Add(new SelectListItem() { Text = "", Value = "0" });

                Students = new SelectList(students.OrderBy(x => x.Text).ToList(), "Value", "Text");

                var teachers = repoTeacher.GetByRoles(schoolYear, Role.Teacher)
                    .Select(x => new
                    {
                        LastName = x.LastName,
                        FirstName = x.FirstName,
                        DbUserId = x.DbUserId
                    })
                    .ToList()
                    .Select(x => new SelectListItem()
                    {
                        Text = x.LastName + ", " + x.FirstName,
                        Value = x.DbUserId.ToString()
                    })
                    .ToList();

                teachers.Add(new SelectListItem() { Text = "", Value = "0" });

                Teachers = new SelectList(teachers.OrderBy(x => x.Text).ToList(), "Value", "Text");

                var fpcsCourses = repoFPCSCourse.GetAll(schoolYear)
                    .Select(x => new
                    {
                        Name = x.Name,
                        TeacherLastName = x.Teacher != null ? x.Teacher.LastName : x.Guardian.LastName,
                        TeacherFirstName = x.Teacher != null ? x.Teacher.FirstName : x.Guardian.FirstName,
                        FPCSCourseId = x.FPCSCourseId
                    })
                    .ToList()
                    .Select(x => new SelectListItem()
                    {
                        Text = x.Name + " - " + x.TeacherLastName + ", " + x.TeacherFirstName,
                        Value = x.FPCSCourseId.ToString()
                    })
                    .ToList();

                fpcsCourses.Add(new SelectListItem() { Text = "", Value = "0" });

                FPCSCourses = new SelectList(fpcsCourses.OrderBy(x => x.Text).ToList(), "Value", "Text");
            }
        }
    }
}