﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

using FPCS.Core.Extensions;
using FPCS.Data.Enums;

namespace FPCS.Web.Admin.Models.Student
{
    public class StudentDetailsModel
    {
        public Guid DbUserId { get; set; }

        [Display(Name = "First name")]
        public String FirstName { get; set; }

        [Display(Name = "Last name")]
        public String LastName { get; set; }

        [Display(Name = "MI")]
        public String MiddleInitial { get; set; }

        [Display(Name = "Email Address")]
        public String Email { get; set; }

        [Display(Name = "Sex")]
        public Sex Sex { get; set; }

        [Display(Name = "Grade Year")]
        public Int32 GradYear { get; set; }

        [Display(Name = "Date of Birth")]
        public DateTimeOffset? DateOfBirth { get; set; }

        [Display(Name = "Withdrawl Date")]
        public DateTimeOffset? WithdrawalDate { get; set; }

        [Display(Name = "Enrollment Date")]
        public DateTimeOffset? EnrollmentDate { get; set; }

        [Display(Name = "If this student will be enrolled in a private school for this school year what is the name of that school?")]
        public String PrivateSchoolName { get; set; }

        [Display(Name = "If this student will be enrolled in another school district what percentage will this student be enrolled in that school district?")]
        public Int32 PercentInSchoolDistrict { get; set; }

        [Display(Name = "Do you plan for this student to graduate from FPCS?")]
        public Boolean IsGraduateFromFPCS { get; set; }

        [Display(Name = "Does this student qualify for an ASD Contract Hours Exemption?")]
        public Boolean IsASDContractHoursExemption { get; set; }

        [Display(Name = "If yes please give reason")]
        public String ReasonASDContractHoursExemption { get; set; }

        [Display(Name = "Grade")]
        public Grade Grade { get; set; }

        [Display(Name = "Enrollment Status")]
        public EnrollmentStatus? EnrollmentStatus { get; set; }

        [Display(Name = "Birth Certificate")]
        public Boolean IsBirthCertificate { get; set; }

        [Display(Name = "Grades Not Submitted")]
        public Boolean IsGradesNotSubmitted { get; set; }

        [Display(Name = "ILP Philosophy")]
        public Boolean IsILPPhilosophy { get; set; }

        [Display(Name = "Medical Release")]
        public Boolean IsMedicalRelease { get; set; }

        [Display(Name = "Progress Report Signature")]
        public Boolean IsProgressReportSignature { get; set; }

        [Display(Name = "Shot Records")]
        public Boolean IsShotRecords { get; set; }

        [Display(Name = "Testing Agreement")]
        public Boolean IsTestingAgreement { get; set; }

        [Display(Name = "Other")]
        public Boolean IsOther { get; set; }

        [Display(Name = "Percentage Enrolled")]
        public Int32 PercentagePlanningEnroll { get; set; }
        
        [Display(Name = "Core Credit Exemption")]
        public Int32 CoreCreditExemption { get; set; }

        [Display(Name = "Core Credit Exemption Reason")]
        public String CoreCreditExemptionReason { get; set; }

        [Display(Name = "Elective Credit Exemption")]
        public Int32 ElectiveCreditExemption { get; set; }

        [Display(Name = "Elective Credit Exemption Reason")]
        public String ElectiveCreditExemptionReason { get; set; }

        [Display(Name = "Zangle ID")]
        public String ZangleID { get; set; }

        public SelectList Grades { get; set; }

        public SelectList Sexes { get; set; }

        public SelectList EnrollmentStatuses { get; set; }

        public SelectList GradYears { get; set; }

        public SelectList PercentsInSchoolDistrict { get; set; }

        public SelectList PercentsInPercentagePlanningEnroll { get; set; }

        public void Init()
        {
            Grades = Grade.ToSelectListUsingDesc();
            Sexes = Sex.ToSelectList();
            EnrollmentStatuses = FPCS.Data.Enums.EnrollmentStatus.Enrolled.ToSelectListUsingDescWithoutActive();

            var years = new List<Lookup<Int32>>();
            for (int i = DateTime.Today.Year; i < DateTime.Today.Year + 14; i++)
                years.Add(new Lookup<Int32> { Value = i, Text = i.ToString().Replace("G", "") });
            GradYears = new SelectList(years, "Value", "Text");

            var percents = new List<Lookup<Int32>>();
            percents.Add(new Lookup<Int32> { Value = 0, Text = "0%" });
            percents.Add(new Lookup<Int32> { Value = 25, Text = "25%" });
            percents.Add(new Lookup<Int32> { Value = 50, Text = "50%" });
            percents.Add(new Lookup<Int32> { Value = 75, Text = "75%" });
            percents.Add(new Lookup<Int32> { Value = 100, Text = "100%" });

            PercentsInSchoolDistrict = new SelectList(percents, "Value", "Text");
            PercentsInPercentagePlanningEnroll = new SelectList(percents, "Value", "Text");
        }
    }
}