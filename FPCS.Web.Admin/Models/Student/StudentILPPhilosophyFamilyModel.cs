﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DEB = FPCS.Data.Entities;

namespace FPCS.Web.Admin.Models.Student
{
    public class StudentILPPhilosophyFamilyModel
    {
        public StudentILPPhilosophyFamilyModel()
        {
        }

        public StudentILPPhilosophyFamilyModel(DEB.Student dbEntity, String ilpPhilosophy)
        {
            StudentId = dbEntity.DbUserId;
            Name = String.Format(@"{0}, {1}", dbEntity.LastName, dbEntity.FirstName);
            IsChecked = dbEntity.ILPPhilosophy == ilpPhilosophy;
        }

        public Guid StudentId { get; set; }

        public Boolean IsChecked { get; set; }

        public String Name { get; set; }
    }
}