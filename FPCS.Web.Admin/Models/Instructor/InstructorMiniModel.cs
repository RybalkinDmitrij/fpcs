﻿using System;

using FPCS.Core.jqGrid;

namespace FPCS.Web.Admin.Models.Instructor
{
    public class InstructorMiniModel
    {
        public Int64 StudentPacketCourseId { get; set; }

        public String ClassName { get; set; }

        public String StudentName { get; set; }

        public String Guardians { get; set; }

        public String Email { get; set; }

        public String FamilyPhone { get; set; }

        public String GuardSign { get; set; }

        public String SponsorSign { get; set; }

        public String InstructSign { get; set; }

        public String AdminSign { get; set; }

        public String Comments { get; set; }

        //public String GuardianName { get; set; }

        //public String SponsorName { get; set; }

        //public String InstructName { get; set; }

        //public String AdminName { get; set; }
    }
}