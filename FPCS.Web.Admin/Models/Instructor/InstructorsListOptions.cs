﻿using System;

using FPCS.Core.jqGrid;

namespace FPCS.Web.Admin.Models.Instructor
{
    public class InstructorsListOptions
    {
        [GridProperty(ExtensionType.All, true)]
        public Int64? EntityId { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Contains)]
        public String ClassName { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Contains)]
        public String StudentName { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Contains)]
        public String Guardians { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Contains)]
        public String Email { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Contains)]
        public String FamilyPhone { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Contains)]
        public String GuardSign { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Contains)]
        public String SponsorSign { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Contains)]
        public String InstructSign { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Contains)]
        public String AdminSign { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Contains)]
        public String Comments { get; set; }
    }
}