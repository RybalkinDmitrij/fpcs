﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace FPCS.Web.Admin.Models.SchoolYear
{
	public class SchoolYearEditModel
	{
		[Required]
		public Int32 SchoolYearId { get; set; }

		[Required]
		[Display(Name = "Year")]
		public Int32 Year { get; set; }

		[Required]
		[Display(Name = "Name")]
		public String Name { get; set; }

		[Required]
		[Display(Name = "First Day of School")]
		public DateTimeOffset StartDate { get; set; }

		[Required]
		[Display(Name = "Last Day of School")]
		public DateTimeOffset EndDate { get; set; }

		[Required]
		[Display(Name = "Half Year of School")]
		public DateTimeOffset HalfDate { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "Course Description Hours Limit")]
		public Int32 ILPHoursLimit { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "Number of Hours Per Day")]
		public Decimal CountHoursWorkDay { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "Plus Budget Limit")]
		public Decimal PlusBudgetLimit { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "Book/Material fee")]
		public Decimal BookMaterialFee { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "Group Life Insurance")]
		public Decimal GroupLifeInsurance { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "Group Medical Insurance")]
		public Decimal GroupMedicalInsurance { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "Workers' Comp")]
		public Decimal WorkersComp { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "Unemployment")]
		public Decimal Unemployment { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "FICA")]
		public Decimal FICA { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "FICA Cap")]
		public Decimal FICACap { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "Medicare")]
		public Decimal Medicare { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "TERS Base Contract")]
		public Decimal TERSBaseContract { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "PERS Base Contract")]
		public Decimal PERSBaseContract { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "TERS Retirement")]
		public Decimal TERSRetirement { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "PERS Retirement")]
		public Decimal PERSRetirement { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "General Supplies")]
		public Decimal GeneralSupplies { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "K")]
		public Int32? FundingAmountGradeK { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "1")]
		public Int32? FundingAmountGrade1 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "2")]
		public Int32? FundingAmountGrade2 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "3")]
		public Int32? FundingAmountGrade3 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "4")]
		public Int32? FundingAmountGrade4 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "5")]
		public Int32? FundingAmountGrade5 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "6")]
		public Int32? FundingAmountGrade6 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "7")]
		public Int32? FundingAmountGrade7 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "8")]
		public Int32? FundingAmountGrade8 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "9")]
		public Int32? FundingAmountGrade9 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "10")]
		public Int32? FundingAmountGrade10 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "11")]
		public Int32? FundingAmountGrade11 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "12")]
		public Int32? FundingAmountGrade12 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "G")]
		public Int32? FundingAmountGradeG { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "K")]
		public Int32? CoreUnitGradeK { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "1")]
		public Int32? CoreUnitGrade1 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "2")]
		public Int32? CoreUnitGrade2 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "3")]
		public Int32? CoreUnitGrade3 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "4")]
		public Int32? CoreUnitGrade4 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "5")]
		public Int32? CoreUnitGrade5 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "6")]
		public Int32? CoreUnitGrade6 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "7")]
		public Int32? CoreUnitGrade7 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "8")]
		public Int32? CoreUnitGrade8 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "9")]
		public Int32? CoreUnitGrade9 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "10")]
		public Int32? CoreUnitGrade10 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "11")]
		public Int32? CoreUnitGrade11 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "12")]
		public Int32? CoreUnitGrade12 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "G")]
		public Int32? CoreUnitGradeG { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "K")]
		public Int32? ElectiveUnitGradeK { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "1")]
		public Int32? ElectiveUnitGrade1 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "2")]
		public Int32? ElectiveUnitGrade2 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "3")]
		public Int32? ElectiveUnitGrade3 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "4")]
		public Int32? ElectiveUnitGrade4 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "5")]
		public Int32? ElectiveUnitGrade5 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "6")]
		public Int32? ElectiveUnitGrade6 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "7")]
		public Int32? ElectiveUnitGrade7 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "8")]
		public Int32? ElectiveUnitGrade8 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "9")]
		public Int32? ElectiveUnitGrade9 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "10")]
		public Int32? ElectiveUnitGrade10 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "11")]
		public Int32? ElectiveUnitGrade11 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "12")]
		public Int32? ElectiveUnitGrade12 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "G")]
		public Int32? ElectiveUnitGradeG { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "Disable access for parents")]
		public Boolean DisableAccessForParents { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "Disable access for teachers")]
		public Boolean DisableAccessForTeachers { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "Disable message")]
		public String DisableAccessMessage { get; set; }
	}
}