﻿using System;
using System.Web.Mvc;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FPCS.Web.Admin.Models.SchoolYear
{
	public class SchoolYearDetailsModel
	{
		public Int32 SchoolYearId { get; set; }

		[Display(Name = "Year")]
		public Int32 Year { get; set; }

		[Display(Name = "Name")]
		public String Name { get; set; }

		[Display(Name = "First Day of School")]
		public DateTimeOffset StartDate { get; set; }

		[Display(Name = "Last Day of School")]
		public DateTimeOffset EndDate { get; set; }

		[Display(Name = "Half Year of School")]
		public DateTimeOffset HalfDate { get; set; }

		[Display(Name = "Updated Date")]
		public DateTimeOffset UpdatedDate { get; set; }

		[Display(Name = "Course Description Hours Limit")]
		public Int32 ILPHoursLimit { get; set; }

		[Display(Name = "Number of Hours Per Day")]
		public Decimal CountHoursWorkDay { get; set; }

		[Display(Name = "Plus Budget Limit")]
		public Decimal PlusBudgetLimit { get; set; }

		[Display(Name = "Book/Material fee")]
		public Decimal BookMaterialFee { get; set; }

		[Display(Name = "Group Life Insurance")]
		public Decimal GroupLifeInsurance { get; set; }

		[Display(Name = "Group Medical Insurance")]
		public Decimal GroupMedicalInsurance { get; set; }

		[Display(Name = "Workers' Comp")]
		public Decimal WorkersComp { get; set; }

		[Display(Name = "Unemployment")]
		public Decimal Unemployment { get; set; }

		[Display(Name = "FICA")]
		public Decimal FICA { get; set; }

		[Display(Name = "FICA Cap")]
		public Decimal FICACap { get; set; }

		[Display(Name = "Medicare")]
		public Decimal Medicare { get; set; }

		[Display(Name = "TERS Base Contract")]
		public Decimal TERSBaseContract { get; set; }

		[Display(Name = "PERS Base Contract")]
		public Decimal PERSBaseContract { get; set; }

		[Display(Name = "TERS Retirement")]
		public Decimal TERSRetirement { get; set; }

		[Display(Name = "PERS Retirement")]
		public Decimal PERSRetirement { get; set; }

		[Display(Name = "General Supplies")]
		public Decimal GeneralSupplies { get; set; }

		[Display(Name = "K")]
		public Int32 FundingAmountGradeK { get; set; }

		[Display(Name = "1")]
		public Int32 FundingAmountGrade1 { get; set; }

		[Display(Name = "2")]
		public Int32 FundingAmountGrade2 { get; set; }

		[Display(Name = "3")]
		public Int32 FundingAmountGrade3 { get; set; }

		[Display(Name = "4")]
		public Int32 FundingAmountGrade4 { get; set; }

		[Display(Name = "5")]
		public Int32 FundingAmountGrade5 { get; set; }

		[Display(Name = "6")]
		public Int32 FundingAmountGrade6 { get; set; }

		[Display(Name = "7")]
		public Int32 FundingAmountGrade7 { get; set; }

		[Display(Name = "8")]
		public Int32 FundingAmountGrade8 { get; set; }

		[Display(Name = "9")]
		public Int32 FundingAmountGrade9 { get; set; }

		[Display(Name = "10")]
		public Int32 FundingAmountGrade10 { get; set; }

		[Display(Name = "11")]
		public Int32 FundingAmountGrade11 { get; set; }

		[Display(Name = "12")]
		public Int32 FundingAmountGrade12 { get; set; }

		[Display(Name = "G")]
		public Int32 FundingAmountGradeG { get; set; }

		[Display(Name = "K")]
		public Int32? CoreUnitGradeK { get; set; }

		[Display(Name = "1")]
		public Int32? CoreUnitGrade1 { get; set; }

		[Display(Name = "2")]
		public Int32? CoreUnitGrade2 { get; set; }

		[Display(Name = "3")]
		public Int32? CoreUnitGrade3 { get; set; }

		[Display(Name = "4")]
		public Int32? CoreUnitGrade4 { get; set; }

		[Display(Name = "5")]
		public Int32? CoreUnitGrade5 { get; set; }

		[Display(Name = "6")]
		public Int32? CoreUnitGrade6 { get; set; }

		[Display(Name = "7")]
		public Int32? CoreUnitGrade7 { get; set; }

		[Display(Name = "8")]
		public Int32? CoreUnitGrade8 { get; set; }

		[Display(Name = "9")]
		public Int32? CoreUnitGrade9 { get; set; }

		[Display(Name = "10")]
		public Int32? CoreUnitGrade10 { get; set; }

		[Display(Name = "11")]
		public Int32? CoreUnitGrade11 { get; set; }

		[Display(Name = "12")]
		public Int32? CoreUnitGrade12 { get; set; }

		[Display(Name = "G")]
		public Int32? CoreUnitGradeG { get; set; }

		[Display(Name = "K")]
		public Int32? ElectiveUnitGradeK { get; set; }

		[Display(Name = "1")]
		public Int32? ElectiveUnitGrade1 { get; set; }

		[Display(Name = "2")]
		public Int32? ElectiveUnitGrade2 { get; set; }

		[Display(Name = "3")]
		public Int32? ElectiveUnitGrade3 { get; set; }

		[Display(Name = "4")]
		public Int32? ElectiveUnitGrade4 { get; set; }

		[Display(Name = "5")]
		public Int32? ElectiveUnitGrade5 { get; set; }

		[Display(Name = "6")]
		public Int32? ElectiveUnitGrade6 { get; set; }

		[Display(Name = "7")]
		public Int32? ElectiveUnitGrade7 { get; set; }

		[Display(Name = "8")]
		public Int32? ElectiveUnitGrade8 { get; set; }

		[Display(Name = "9")]
		public Int32? ElectiveUnitGrade9 { get; set; }

		[Display(Name = "10")]
		public Int32? ElectiveUnitGrade10 { get; set; }

		[Display(Name = "11")]
		public Int32? ElectiveUnitGrade11 { get; set; }

		[Display(Name = "12")]
		public Int32? ElectiveUnitGrade12 { get; set; }

		[Display(Name = "G")]
		public Int32? ElectiveUnitGradeG { get; set; }

		[Display(Name = "Disable access for parents")]
		public Boolean DisableAccessForParents { get; set; }

		[Display(Name = "Disable access for teachers")]
		public Boolean DisableAccessForTeachers { get; set; }

		[Display(Name = "Disable message")]
		public String DisableAccessMessage { get; set; }

		public SelectList Years { get; set; }

		public void Init()
		{
			var years = new List<Lookup<Int32, String>>();
			var endYear = DateTime.Today.Year + 4;
			for (int i = 2001; i < endYear; i++)
			{
				years.Add(new Lookup<Int32, String> {Value = i, Text = (i - 1).ToString() + " - " + i.ToString()});
			}
			Years = new SelectList(years, "Value", "Text");
		}
	}
}