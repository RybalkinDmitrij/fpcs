﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using FPCS.Core.Unity;
using FPCS.Data;
using FPCS.Data.Repo;

namespace FPCS.Web.Admin.Models.SchoolYear
{
	public class SchoolYearCreateModel
	{
		/// <summary>
		/// Default constructor
		/// </summary>
		public SchoolYearCreateModel()
		{
			ILPHoursLimit = 1440;
			CountHoursWorkDay = 7.5m;

			GroupLifeInsurance = 78;
			GroupMedicalInsurance = 17580;
			WorkersComp = 0.00739m;
			Unemployment = 0.001445m;
			FICA = 0.062m;
			FICACap = 94200;
			Medicare = 0.019m;
			TERSBaseContract = 0.5m;
			PERSBaseContract = 0.4m;
			TERSRetirement = 0.1256m;
			PERSRetirement = 0.22m;

			GeneralSupplies = 150m;

			CoreUnitGradeK = 4;
			CoreUnitGrade1 = 4;
			CoreUnitGrade2 = 4;
			CoreUnitGrade3 = 4;
			CoreUnitGrade4 = 4;
			CoreUnitGrade5 = 4;
			CoreUnitGrade6 = 4;
			CoreUnitGrade7 = 4;
			CoreUnitGrade8 = 4;
			CoreUnitGrade9 = 4;
			CoreUnitGrade10 = 4;
			CoreUnitGrade11 = 4;
			CoreUnitGrade12 = 4;
			CoreUnitGradeG = 4;

			ElectiveUnitGradeK = 4;
			ElectiveUnitGrade1 = 4;
			ElectiveUnitGrade2 = 4;
			ElectiveUnitGrade3 = 4;
			ElectiveUnitGrade4 = 4;
			ElectiveUnitGrade5 = 4;
			ElectiveUnitGrade6 = 4;
			ElectiveUnitGrade7 = 4;
			ElectiveUnitGrade8 = 4;
			ElectiveUnitGrade9 = 4;
			ElectiveUnitGrade10 = 4;
			ElectiveUnitGrade11 = 4;
			ElectiveUnitGrade12 = 4;
			ElectiveUnitGradeG = 4;
		}

		[Required]
		[Display(Name = "Year")]
		public Int32 Year { get; set; }

		[Required]
		[Display(Name = "Name")]
		public String Name { get; set; }

		[Required]
		[Display(Name = "First Day of School")]
		public DateTimeOffset StartDate { get; set; }

		[Required]
		[Display(Name = "Last Day of School")]
		public DateTimeOffset EndDate { get; set; }

		[Required]
		[Display(Name = "Half Year of School")]
		public DateTimeOffset HalfDate { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "Course Description Hours Limit")]
		public Int32 ILPHoursLimit { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "Number of Hours Per Day")]
		public Decimal CountHoursWorkDay { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "Plus Budget Limit")]
		public Decimal PlusBudgetLimit { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "Book/Material fee")]
		public Decimal BookMaterialFee { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "Group Life Insurance")]
		public Decimal GroupLifeInsurance { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "Group Medical Insurance")]
		public Decimal GroupMedicalInsurance { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "Workers' Comp")]
		public Decimal WorkersComp { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "Unemployment")]
		public Decimal Unemployment { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "FICA")]
		public Decimal FICA { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "FICA Cap")]
		public Decimal FICACap { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "Medicare")]
		public Decimal Medicare { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "TERS Base Contract %")]
		public Decimal TERSBaseContract { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "PERS Base Contract %")]
		public Decimal PERSBaseContract { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "TERS Retirement")]
		public Decimal TERSRetirement { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "PERS Retirement")]
		public Decimal PERSRetirement { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "General Supplies")]
		public Decimal GeneralSupplies { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "K")]
		public Int32? FundingAmountGradeK { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "1")]
		public Int32? FundingAmountGrade1 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "2")]
		public Int32? FundingAmountGrade2 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "3")]
		public Int32? FundingAmountGrade3 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "4")]
		public Int32? FundingAmountGrade4 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "5")]
		public Int32? FundingAmountGrade5 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "6")]
		public Int32? FundingAmountGrade6 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "7")]
		public Int32? FundingAmountGrade7 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "8")]
		public Int32? FundingAmountGrade8 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "9")]
		public Int32? FundingAmountGrade9 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "10")]
		public Int32? FundingAmountGrade10 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "11")]
		public Int32? FundingAmountGrade11 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "12")]
		public Int32? FundingAmountGrade12 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "G")]
		public Int32? FundingAmountGradeG { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "K")]
		public Int32? CoreUnitGradeK { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "1")]
		public Int32? CoreUnitGrade1 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "2")]
		public Int32? CoreUnitGrade2 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "3")]
		public Int32? CoreUnitGrade3 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "4")]
		public Int32? CoreUnitGrade4 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "5")]
		public Int32? CoreUnitGrade5 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "6")]
		public Int32? CoreUnitGrade6 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "7")]
		public Int32? CoreUnitGrade7 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "8")]
		public Int32? CoreUnitGrade8 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "9")]
		public Int32? CoreUnitGrade9 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "10")]
		public Int32? CoreUnitGrade10 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "11")]
		public Int32? CoreUnitGrade11 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "12")]
		public Int32? CoreUnitGrade12 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "G")]
		public Int32? CoreUnitGradeG { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "K")]
		public Int32? ElectiveUnitGradeK { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "1")]
		public Int32? ElectiveUnitGrade1 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "2")]
		public Int32? ElectiveUnitGrade2 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "3")]
		public Int32? ElectiveUnitGrade3 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "4")]
		public Int32? ElectiveUnitGrade4 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "5")]
		public Int32? ElectiveUnitGrade5 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "6")]
		public Int32? ElectiveUnitGrade6 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "7")]
		public Int32? ElectiveUnitGrade7 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "8")]
		public Int32? ElectiveUnitGrade8 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "9")]
		public Int32? ElectiveUnitGrade9 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "10")]
		public Int32? ElectiveUnitGrade10 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "11")]
		public Int32? ElectiveUnitGrade11 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "12")]
		public Int32? ElectiveUnitGrade12 { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "G")]
		public Int32? ElectiveUnitGradeG { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "Disable access for parents")]
		public Boolean DisableAccessForParents { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "Disable access for teachers")]
		public Boolean DisableAccessForTeachers { get; set; }

		[Required(ErrorMessage = "Required field")]
		[Display(Name = "Disable message")]
		public String DisableAccessMessage { get; set; }
	}
}