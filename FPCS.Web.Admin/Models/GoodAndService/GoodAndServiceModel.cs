﻿using FPCS.Data.Entities;
using FPCS.Data.Enums;
using FPCS.Core.Extensions;
using System;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using FPCS.Web.Admin.Helpers.GoodAndService;
using FPCS.Web.Admin.Helpers.GoodAndService.Models;

namespace FPCS.Web.Admin.Models.GoodAndService
{
    public class GoodAndServiceModel
    {
        public GoodAndServiceModel()
        {
        }

        public GoodAndServiceModel(GoodAndServiceHelperModel model)
        {
            Init(model);
        }
        
        public GoodAndServiceModel(GoodService dbEntity, List<GoodServiceBalance> balances)
        {
            Init(GoodAndServiceHelper.GetGoodAndService(dbEntity, balances));
        }

        public GoodAndServiceModel(GoodServiceBalance dbEntity)
        {
            Init(GoodAndServiceHelper.GetGoodAndService(dbEntity));
        }

        public Int64? StudentPacketCourseID { get; set; }

        public Int64? GoodAndServiceID { get; set; }

        public Int64? GoodAndServiceBalanceID { get; set; }

        [Display(Name = "Line Items")]
        public String LineItems { get; set; }

        [Display(Name = "Budget Item")]
        public String BudgetItem { get; set; }

        [Display(Name = "Vendor")]
        public String Vendor { get; set; }

        [Display(Name = "Status")]
        public String Status { get; set; }

        [Display(Name = "Description")]
        public String Description { get; set; }

        [Display(Name = "QTY")]
        public Decimal QTY { get; set; }

        [Display(Name = "Unit Cost")]
        public Decimal UnitCost { get; set; }

        [Display(Name = "S/H")]
        public Decimal SH { get; set; }

        [Display(Name = "Budget Total")]
        public Decimal BudgetTotal { get; set; }

        [Display(Name = "Actual Charges")]
        public Decimal ActualCharges { get; set; }

        [Display(Name = "Budget Adjust")]
        public Decimal BudgetAdjust { get; set; }

        [Display(Name = "Budget Balance")]
        public Decimal BudgetBalance { get; set; }

        [Display(Name = "Total Planning Budget")]
        public Decimal TotalPlanningBudget { get; set; }

        [Display(Name = "Total Actual Spending")]
        public Decimal TotalActualSpending { get; set; }

        public Boolean IsInstructor { get; set; }

        public Boolean IsShow { get; set; }

        public Boolean IsClosed { get; set; }

        public String ItemName { get; set; }

        public String Dates { get; set; }

        public String Consumable { get; set; }

        public Boolean HasBalance { get; set; }

        private void Init(GoodAndServiceHelperModel model)
        {
            StudentPacketCourseID = model.StudentPacketCourseID;
            GoodAndServiceID = model.GoodAndServiceID;
            GoodAndServiceBalanceID = model.GoodAndServiceBalanceID;
            LineItems = model.LineItems;
            BudgetItem = model.BudgetItem;
            Vendor = model.Vendor;
            Status = model.Status;
            Description = model.Description;
            QTY = model.QTY;
            UnitCost = model.UnitCost;
            SH = model.SH;
            BudgetTotal = model.BudgetTotal;
            ActualCharges = model.ActualCharges;
            BudgetAdjust = model.BudgetAdjust;
            BudgetBalance = model.BudgetBalance;
            TotalPlanningBudget = -1 * model.BudgetTotal;
            TotalActualSpending = -1 * model.ActualCharges;
            IsInstructor = model.IsInstructor;
            IsShow = model.IsShow;
            IsClosed = model.IsClosed;

            ItemName = model.ItemName;
            Dates = model.Dates;
            Consumable = model.Consumable;

            HasBalance = model.HasBalance;
        }
    }
}