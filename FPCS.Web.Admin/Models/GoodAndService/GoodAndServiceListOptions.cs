﻿using System;

using FPCS.Core.jqGrid;

namespace FPCS.Web.Admin.Models.GoodAndService
{
    public class GoodAndServiceListOptions
    {
        [GridProperty(ExtensionType.All, true)]
        public Int64? GoodAndServiceID { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Contains)]
        public String LineItems { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Contains)]
        public String BudgetItem { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Contains)]
        public String Vendor { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Contains)]
        public String Status { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Contains)]
        public String Desciption { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Equal)]
        public Decimal QTY { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Equal)]
        public Decimal UnitCost { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Equal)]
        public Decimal SH { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Equal)]
        public Decimal BudgetTotal { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Equal)]
        public Decimal ActualCharges { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Equal)]
        public Decimal BudgetAdjust { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Equal)]
        public Decimal BudgetBalance { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Equal)]
        public Decimal TotalPlanningBudget { get; set; }
    }
}