﻿using FPCS.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FPCS.Web.Admin.Models.GoodAndService
{
	public class TableGoodAndServiceModel
	{
		public TableGoodAndServiceModel()
		{
			this.GoodsAndServices = new List<GoodAndServiceModel>();
		}

		public List<GoodAndServiceModel> GoodsAndServices { get; set; }

		public Decimal PlanningBudgetTotal { get; set; }

		public Decimal ActualSpendingTotal { get; set; }
	}
}