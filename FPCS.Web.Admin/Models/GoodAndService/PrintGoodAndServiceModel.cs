﻿using FPCS.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FPCS.Web.Admin.Models.GoodAndService
{
    public class PrintGoodAndServiceModel
    {
        public String PrintAddress { get; set; }

        public String PrintState { get; set; }

        public String PrintPhone { get; set; }

        public String PrintFax { get; set; }

        public String PrintDateNow { get; set; }

        public String ClassName { get; set; }

        public String StudentName { get; set; }

        public Decimal GrandTotal { get; set; }

        public List<GoodAndServiceModel> GoodsAndServices { get; set; }

        
    }
}