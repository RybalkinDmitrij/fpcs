﻿using FPCS.Data.Enums;
using FPCS.Core.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FPCS.Core.Unity;
using FPCS.Data;
using FPCS.Data.Repo;
using FPCS.Data.Entities;

namespace FPCS.Web.Admin.Models.GoodAndService
{
    public class GoodSuppliesModel : AbstractGoodServiceModel
    {
        public GoodSuppliesModel()
        {
        }

        public GoodSuppliesModel(GoodOrService goodOrService, RequisitionOrReimbursement requisitionOrReimbursement, TypeRequisitionReimbursement typeRequisitionReimbursement, Int32 vendor, Int32 studentPacketCourseId, Int32 courseId, Boolean isReadOnly, Int32? id)
		  : base(goodOrService, requisitionOrReimbursement, typeRequisitionReimbursement, vendor, 0, studentPacketCourseId, courseId, null, isReadOnly)
        {
            if (id.HasValue && id.Value != 0)
            {
                using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
                {
                    var repo = uow.GetRepo<IGoodServiceRepo>();

                    GoodService dbEntity = repo.Get(id.Value);

                    Id = dbEntity.GoodServiceId;
                    SupplyType = dbEntity.SupplyType;
                    ItemName = dbEntity.LineItemName;
                    Description = dbEntity.Description;
                    Count = dbEntity.Count;
                    CatalogMFG = dbEntity.CatalogMFG;
                    CatalogIssuePG = dbEntity.CatalogIssuePG;
                    UnitType = dbEntity.UnitType;
                    BarCode = dbEntity.BarCode;
                    Consumable = dbEntity.Consumable;
                    AdminComments = dbEntity.AdminComments;
                    ApprovalGivenBy = dbEntity.ApprovalGivenBy;

                    Aprv = dbEntity.GoodOrServiceApproved.HasValue ? dbEntity.GoodOrServiceApproved.Value : GoodOrServiceApproved.b_pend;
                    IsClosed = dbEntity.IsClosed.HasValue ? dbEntity.IsClosed.Value : false;

                    NumberOfUnits = dbEntity.NumberOfUnits;
                    UnitPrice = dbEntity.UnitPrice;
                    ShippingHandlingFees = dbEntity.ShippingHandlingFees;

                    Total = NumberOfUnits * UnitPrice + ShippingHandlingFees;
                }
            }
        }

        //[Required]
        [Display(Name = "Supply Type")]
        public SupplyType? SupplyType { get; set; }

        [Display(Name = "Item Name")]
        public String ItemName { get; set; }

        [Display(Name = "Description")]
        public String Description { get; set; }

        [Display(Name = "Count")]
        public Int32 Count { get; set; }

        [Display(Name = "Catalog/MFG #")]
        public String CatalogMFG { get; set; }

        [Display(Name = "Catalog Issue PG #")]
        public String CatalogIssuePG { get; set; }

        //[Required]
        [Display(Name = "Unit Type")]
        public UnitType? UnitType { get; set; }

        [Display(Name = "Bar Code")]
        public String BarCode { get; set; }

        //[Required]
        [Display(Name = "Consumable")]
        public Consumable? Consumable { get; set; }

        [Display(Name = "Admin Comments")]
        public String AdminComments { get; set; }

        //[Required]
        [Display(Name = "Approval Given By")]
        public ApprovalGivenBy? ApprovalGivenBy { get; set; }

        [Required]
        [Display(Name = "Number of Units")]
        public Int32 NumberOfUnits { get; set; }

        [Required]
        [Display(Name = "Unit Price")]
        public Decimal UnitPrice { get; set; }

        [Required]
        [Display(Name = "Shipping/Handling/Fees")]
        public Decimal ShippingHandlingFees { get; set; }

        public SelectList SupplyTypes { get; set; }

        public SelectList UnitTypes { get; set; }

        public SelectList Consumables { get; set; }

        public SelectList ApprovalGivenBys { get; set; }

        public override void Init()
        {
            SupplyTypes = Data.Enums.SupplyType.ArtSupplies.ToSelectListUsingDescWithoutActive();
            UnitTypes = Data.Enums.UnitType.box.ToSelectListUsingDescWithoutActive();
            Consumables = Data.Enums.Consumable.No.ToSelectListUsingDescWithoutActive();
            ApprovalGivenBys = Data.Enums.ApprovalGivenBy.email.ToSelectListUsingDescWithoutActive();
        }
    }
}