﻿using FPCS.Data.Enums;
using FPCS.Core.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FPCS.Core.Unity;
using FPCS.Data;
using FPCS.Data.Repo;
using FPCS.Data.Entities;

namespace FPCS.Web.Admin.Models.GoodAndService
{
    public class ServiceComputerLeaseModel : AbstractGoodServiceModel
    {
        public ServiceComputerLeaseModel()
        {
        }

        public ServiceComputerLeaseModel(GoodOrService goodOrService, RequisitionOrReimbursement requisitionOrReimbursement, TypeRequisitionReimbursement typeRequisitionReimbursement, Int32 vendor, Int32 studentPacketCourseId, Int32 courseId, Boolean isReadOnly, Int32? id)
		  : base(goodOrService, requisitionOrReimbursement, typeRequisitionReimbursement, vendor, 0, studentPacketCourseId, courseId, null, isReadOnly)
        {
            if (id.HasValue && id.Value != 0)
            {
                using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
                {
                    var repo = uow.GetRepo<IGoodServiceRepo>();

                    GoodService dbEntity = repo.Get(id.Value);

                    Id = dbEntity.GoodServiceId;
                    ComputerLeaseItemType = dbEntity.ComputerLeaseItemType;
                    ComputerLeaseItemDesc = dbEntity.ComputerLeaseItemDesc;
                    UnitType = dbEntity.UnitType;
                    BarCode = dbEntity.BarCode;
                    AdminComments = dbEntity.AdminComments;
                    ApprovalGivenBy = dbEntity.ApprovalGivenBy;
                    DetailedItemDesc = dbEntity.DetailedItemDesc;

                    Aprv = dbEntity.GoodOrServiceApproved.HasValue ? dbEntity.GoodOrServiceApproved.Value : GoodOrServiceApproved.b_pend;
                    IsClosed = dbEntity.IsClosed.HasValue ? dbEntity.IsClosed.Value : false;

                    NumberOfUnits = dbEntity.NumberOfUnits;
                    UnitPrice = dbEntity.UnitPrice;

                    Total = NumberOfUnits * UnitPrice;
                }
            }
        }

        //[Required]
        [Display(Name = "Item Type")]
        public ComputerLeaseItemType? ComputerLeaseItemType { get; set; }

        //[Required]
        [Display(Name = "Item Desc")]
        public ComputerLeaseItemDesc? ComputerLeaseItemDesc { get; set; }

        //[Required]
        [Display(Name = "Unit Type")]
        public UnitType? UnitType { get; set; }

        [Display(Name = "Bar Code")]
        public String BarCode { get; set; }

        [Display(Name = "Admin Comments")]
        public String AdminComments { get; set; }

        //[Required]
        [Display(Name = "Approval Given By")]
        public ApprovalGivenBy? ApprovalGivenBy { get; set; }

        [Display(Name = "Detailed Item Desc")]
        public String DetailedItemDesc { get; set; }

        [Required]
        [Display(Name = "Number of Units")]
        public Int32 NumberOfUnits { get; set; }

        [Required]
        [Display(Name = "Unit Price")]
        public Decimal UnitPrice { get; set; }

        public SelectList ComputerLeaseItemTypes { get; set; }

        public SelectList ComputerLeaseItemDescs { get; set; }

        public SelectList UnitTypes { get; set; }

        public SelectList ApprovalGivenBys { get; set; }

        public override void Init()
        {
            ComputerLeaseItemTypes = Data.Enums.ComputerLeaseItemType.AllYear.ToSelectListUsingDescWithoutActive();
            ComputerLeaseItemDescs = Data.Enums.ComputerLeaseItemDesc.Laptop.ToSelectListUsingDescWithoutActive();
            UnitTypes = Data.Enums.UnitType.box.ToSelectListUsingDescWithoutActive();
            ApprovalGivenBys = Data.Enums.ApprovalGivenBy.email.ToSelectListUsingDescWithoutActive();
        }
    }
}