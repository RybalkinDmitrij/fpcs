﻿using FPCS.Data.Enums;
using FPCS.Core.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FPCS.Core.Unity;
using FPCS.Data;
using FPCS.Data.Repo;
using FPCS.Data.Entities;

namespace FPCS.Web.Admin.Models.GoodAndService
{
    public class ServiceUniversityCourseModel : AbstractGoodServiceModel
    {
        public ServiceUniversityCourseModel()
        {
        }

        public ServiceUniversityCourseModel(GoodOrService goodOrService, RequisitionOrReimbursement requisitionOrReimbursement, TypeRequisitionReimbursement typeRequisitionReimbursement, Int32 vendor, Int32 studentPacketCourseId, Int32 courseId, Boolean isReadOnly, Int32? id)
		  : base(goodOrService, requisitionOrReimbursement, typeRequisitionReimbursement, vendor, 0, studentPacketCourseId, courseId, null, isReadOnly)
        {
            if (id.HasValue && id.Value != 0)
            {
                using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
                {
                    var repo = uow.GetRepo<IGoodServiceRepo>();

                    GoodService dbEntity = repo.Get(id.Value);

                    Id = dbEntity.GoodServiceId;
                    ClassType = dbEntity.ClassType;
                    CourseTitle = dbEntity.CourseTitle;
                    CourseNumber = dbEntity.CourseNumber;
                    DateBirth = dbEntity.DateBirth.GetValueOrDefault(DateTime.Now);
                    Credits = dbEntity.Credits;
                    Semester = dbEntity.Semester;
                    AdminComments = dbEntity.AdminComments;
                    ApprovalGivenBy = dbEntity.ApprovalGivenBy;

                    Aprv = dbEntity.GoodOrServiceApproved.HasValue ? dbEntity.GoodOrServiceApproved.Value : GoodOrServiceApproved.b_pend;
                    IsClosed = dbEntity.IsClosed.HasValue ? dbEntity.IsClosed.Value : false;

                    NumberOfUnits = dbEntity.NumberOfUnits;
                    UnitPrice = dbEntity.UnitPrice;

                    Total = NumberOfUnits * UnitPrice;
                }
            }
        }

        //[Required]
        [Display(Name = "Class Type")]
        public ClassType? ClassType { get; set; }

        [Display(Name = "Course Title")]
        public String CourseTitle { get; set; }

        [Display(Name = "Course Number")]
        public String CourseNumber { get; set; }

        [Display(Name = "Date Of Birth")]
        public DateTime DateBirth { get; set; }

        //[Required]
        [Display(Name = "Credits")]
        public Credits? Credits { get; set; }

        //[Required]
        [Display(Name = "Semester")]
        public Semester2? Semester { get; set; }

        [Display(Name = "Admin Comments")]
        public String AdminComments { get; set; }

        //[Required]
        [Display(Name = "Approval Given By")]
        public ApprovalGivenBy? ApprovalGivenBy { get; set; }

        [Required]
        [Display(Name = "Number of Units")]
        public Int32 NumberOfUnits { get; set; }

        [Required]
        [Display(Name = "Unit Price")]
        public Decimal UnitPrice { get; set; }

        public SelectList ClassTypes { get; set; }

        public SelectList CreditsList { get; set; }

        public SelectList Semesters { get; set; }

        public SelectList ApprovalGivenBys { get; set; }

        public override void Init()
        {
            DateBirth = DateTime.Now;
            ClassTypes = Data.Enums.ClassType.APU.ToSelectListUsingDescWithoutActive();
            CreditsList = Data.Enums.Credits.n1.ToSelectListUsingDescWithoutActive();
            Semesters = Data.Enums.Semester2.Fall.ToSelectListUsingDescWithoutActive();
            ApprovalGivenBys = Data.Enums.ApprovalGivenBy.email.ToSelectListUsingDescWithoutActive();
            Aprvs = Data.Enums.GoodOrServiceApproved.b_appr.ToSelectListUsingDescWithoutActive();
        }
    }
}