﻿using FPCS.Data.Enums;
using FPCS.Core.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FPCS.Core.Unity;
using FPCS.Data;
using FPCS.Data.Repo;
using FPCS.Data.Entities;

namespace FPCS.Web.Admin.Models.GoodAndService
{
    public class ServiceVendorServiceModel : AbstractGoodServiceModel
    {
        public ServiceVendorServiceModel()
        {
        }

        public ServiceVendorServiceModel(GoodOrService goodOrService, RequisitionOrReimbursement requisitionOrReimbursement, TypeRequisitionReimbursement typeRequisitionReimbursement, Int32 vendor, Int32 studentPacketCourseId, Int32 courseId, Boolean isReadOnly, Int32? id)
            : base(goodOrService, requisitionOrReimbursement, typeRequisitionReimbursement, vendor, 0, studentPacketCourseId, courseId, null, isReadOnly)
        {
            if (id.HasValue && id.Value != 0)
            {
                using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
                {
                    var repo = uow.GetRepo<IGoodServiceRepo>();

                    GoodService dbEntity = repo.Get(id.Value);

                    Id = dbEntity.GoodServiceId;
                    TypeService = dbEntity.TypeService;
                    Description = dbEntity.Description;
                    BeginDate = dbEntity.BeginDate.GetValueOrDefault(DateTime.Now);
                    EndDate = dbEntity.EndDate.GetValueOrDefault(DateTime.Now);
                    VendorUnitType = dbEntity.VendorUnitType;
                    AdminComments = dbEntity.AdminComments;
                    ApprovalGivenBy = dbEntity.ApprovalGivenBy;

                    Aprv = dbEntity.GoodOrServiceApproved.HasValue ? dbEntity.GoodOrServiceApproved.Value : GoodOrServiceApproved.b_pend;
                    IsClosed = dbEntity.IsClosed.HasValue ? dbEntity.IsClosed.Value : false;

                    NumberOfUnits = dbEntity.NumberOfUnits;
                    UnitPrice = dbEntity.UnitPrice;

                    Total = NumberOfUnits * UnitPrice;
                }
            }
        }

        //[Required]
        [Display(Name = "Type Of Service")]
        public TypeService? TypeService { get; set; }

        [Display(Name = "Description")]
        public String Description { get; set; }

        [Display(Name = "Begin Date")]
        public DateTime BeginDate { get; set; }

        [Display(Name = "End Date")]
        public DateTime EndDate { get; set; }

        //[Required]
        [Display(Name = "Vendor Unit Type")]
        public VendorUnitType? VendorUnitType { get; set; }

        [Display(Name = "Admin Comments")]
        public String AdminComments { get; set; }

        //[Required]
        [Display(Name = "Approval Given By")]
        public ApprovalGivenBy? ApprovalGivenBy { get; set; }

        [Required]
        [Display(Name = "Number of Units")]
        public Int32 NumberOfUnits { get; set; }

        [Required]
        [Display(Name = "Unit Price")]
        public Decimal UnitPrice { get; set; }

        public SelectList TypeServices { get; set; }

        public SelectList VendorUnitTypes { get; set; }

        public SelectList ApprovalGivenBys { get; set; }

        public override void Init()
        {
            BeginDate = DateTime.Now;
            EndDate = DateTime.Now;
            TypeServices = Data.Enums.TypeService.AcademicTutoring.ToSelectListUsingDescWithoutActive();
            VendorUnitTypes = Data.Enums.VendorUnitType.Course.ToSelectListUsingDescWithoutActive();
            ApprovalGivenBys = Data.Enums.ApprovalGivenBy.email.ToSelectListUsingDescWithoutActive();
        }
    }
}