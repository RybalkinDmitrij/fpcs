﻿using FPCS.Data.Enums;
using FPCS.Core.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FPCS.Web.Admin.Models.GoodAndService
{
	public abstract class AbstractGoodServiceModel
	{
		public AbstractGoodServiceModel()
		{
		}

		public AbstractGoodServiceModel(GoodOrService goodOrService, RequisitionOrReimbursement requisitionOrReimbursement,
			TypeRequisitionReimbursement typeRequisitionReimbursement, Int32 vendor, Int64 studentPacketId,
			Int32 studentPacketCourseId, Int32 courseId, Guid? tempId, Boolean isReadOnly)
		{
			StudentPacketId = studentPacketId;
			StudentPacketCourseId = studentPacketCourseId;
			CourseId = courseId;
			TempId = tempId.HasValue ? tempId.Value.ToString() : Guid.Empty.ToString();

			GoodOrService = goodOrService;
			RequisitionOrReimbursement = requisitionOrReimbursement;
			TypeRequisitionReimbursement = typeRequisitionReimbursement;
			VendorId = vendor;

			ReadOnly = isReadOnly;

			Aprvs = Data.Enums.GoodOrServiceApproved.b_appr.ToSelectListUsingDescWithoutActive();

			Init();
		}

		public Int64 Id { get; set; }

		[Required]
		public Int64 StudentPacketId { get; set; }

		[Required]
		public Int32 StudentPacketCourseId { get; set; }

		[Required]
		public Int32 CourseId { get; set; }

		public String TempId { get; set; }

		[Required]
		public GoodOrService GoodOrService { get; set; }

		[Required]
		[Display(Name = "Requisition Or Reimbursement")]
		public RequisitionOrReimbursement RequisitionOrReimbursement { get; set; }

		[Required]
		[Display(Name = "Type of Requisition or Reimbursement")]
		public TypeRequisitionReimbursement TypeRequisitionReimbursement { get; set; }

		[Required]
		[Display(Name = "Vendor")]
		public Int32 VendorId { get; set; }

		[Display(Name = "Total")]
		public Decimal Total { get; set; }

		public Boolean ReadOnly { get; set; }

		[Display(Name = "Is Closed")]
		public Boolean IsClosed { get; set; }

		//[Required]
		[Display(Name = "Approval")]
		public GoodOrServiceApproved? Aprv { get; set; }

		public SelectList Aprvs { get; set; }

		public abstract void Init();
	}
}