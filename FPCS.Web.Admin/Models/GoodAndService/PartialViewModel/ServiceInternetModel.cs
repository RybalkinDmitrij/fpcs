﻿using FPCS.Data.Enums;
using FPCS.Core.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FPCS.Core.Unity;
using FPCS.Data;
using FPCS.Data.Repo;
using FPCS.Data.Entities;

namespace FPCS.Web.Admin.Models.GoodAndService
{
    public class ServiceInternetModel : AbstractGoodServiceModel
    {
        public ServiceInternetModel()
        {
        }

        public ServiceInternetModel(GoodOrService goodOrService, RequisitionOrReimbursement requisitionOrReimbursement, TypeRequisitionReimbursement typeRequisitionReimbursement, Int32 vendor, Int32 studentPacketCourseId, Int32 courseId, Boolean isReadOnly, Int32? id)
		  : base(goodOrService, requisitionOrReimbursement, typeRequisitionReimbursement, vendor, 0, studentPacketCourseId, courseId, null, isReadOnly)
        {
            if (id.HasValue && id.Value != 0)
            {
                using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
                {
                    var repo = uow.GetRepo<IGoodServiceRepo>();

                    GoodService dbEntity = repo.Get(id.Value);

                    Id = dbEntity.GoodServiceId;
                    InternetItemType = dbEntity.InternetItemType;
                    Description = dbEntity.Description;

                    Aprv = dbEntity.GoodOrServiceApproved.HasValue ? dbEntity.GoodOrServiceApproved.Value : GoodOrServiceApproved.b_pend;
                    IsClosed = dbEntity.IsClosed.HasValue ? dbEntity.IsClosed.Value : false;

                    NumberOfUnits = dbEntity.NumberOfUnits;
                    UnitPrice = dbEntity.UnitPrice;

                    Total = NumberOfUnits * UnitPrice;
                }
            }
        }

        //[Required]
        [Display(Name = "Item Type")]
        public InternetItemType? InternetItemType { get; set; }

        [Display(Name = "Description")]
        public String Description { get; set; }

        [Required]
        [Display(Name = "Number of Units")]
        public Int32 NumberOfUnits { get; set; }

        [Required]
        [Display(Name = "Unit Price")]
        public Decimal UnitPrice { get; set; }

        public SelectList InternetItemTypes { get; set; }

        public override void Init()
        {
            InternetItemTypes = Data.Enums.InternetItemType.MonthByMonth.ToSelectListUsingDescWithoutActive();
        }
    }
}