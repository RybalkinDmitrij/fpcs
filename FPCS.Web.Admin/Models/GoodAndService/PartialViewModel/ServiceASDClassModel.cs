﻿using FPCS.Data.Enums;
using FPCS.Core.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FPCS.Core.Unity;
using FPCS.Data;
using FPCS.Data.Repo;
using FPCS.Data.Entities;

namespace FPCS.Web.Admin.Models.GoodAndService
{
    public class ServiceASDClassModel : AbstractGoodServiceModel
    {
        public ServiceASDClassModel()
        {
        }

        public ServiceASDClassModel(GoodOrService goodOrService, RequisitionOrReimbursement requisitionOrReimbursement, TypeRequisitionReimbursement typeRequisitionReimbursement, Int32 vendor, Int32 studentPacketCourseId, Int32 courseId, Boolean isReadOnly, Int32? id)
		  : base(goodOrService, requisitionOrReimbursement, typeRequisitionReimbursement, vendor, 0, studentPacketCourseId, courseId, null, isReadOnly)
        {
            if (id.HasValue && id.Value != 0)
            {
                using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
                {
                    var repo = uow.GetRepo<IGoodServiceRepo>();

                    GoodService dbEntity = repo.Get(id.Value);

                    Id = dbEntity.GoodServiceId;
                    HighSchool = dbEntity.HighSchool;
                    AdminComments = dbEntity.AdminComments;
                    ApprovalGivenBy = dbEntity.ApprovalGivenBy;

                    Aprv = dbEntity.GoodOrServiceApproved.HasValue ? dbEntity.GoodOrServiceApproved.Value : GoodOrServiceApproved.b_pend;
                    IsClosed = dbEntity.IsClosed.HasValue ? dbEntity.IsClosed.Value : false;

                    NumberOfSemesters = dbEntity.NumberSemesters;
                    CostPerSemester = dbEntity.CostPerSemester;

                    Total = NumberOfSemesters * CostPerSemester;
                }
            }
        }

        //[Required]
        [Display(Name = "High School")]
        public HighSchool? HighSchool { get; set; }

        [Display(Name = "Admin Comments")]
        public String AdminComments { get; set; }

        //[Required]
        [Display(Name = "Approval Given By")]
        public ApprovalGivenBy? ApprovalGivenBy { get; set; }

        [Required]
        [Display(Name = "# of Semesters")]
        public Int32 NumberOfSemesters { get; set; }

        [Required]
        [Display(Name = "Cost per Semester")]
        public Decimal CostPerSemester { get; set; }

        public SelectList HighSchools { get; set; }

        public SelectList ApprovalGivenBys { get; set; }

        public override void Init()
        {
            HighSchools = Data.Enums.HighSchool.Bartlett_High.ToSelectListUsingDescWithoutActive();
            ApprovalGivenBys = Data.Enums.ApprovalGivenBy.email.ToSelectListUsingDescWithoutActive();
        }
    }
}