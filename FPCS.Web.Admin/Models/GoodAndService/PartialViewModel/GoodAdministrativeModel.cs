﻿using FPCS.Core.Unity;
using FPCS.Data;
using FPCS.Data.Enums;
using FPCS.Core.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FPCS.Data.Entities;
using FPCS.Data.Repo;

namespace FPCS.Web.Admin.Models.GoodAndService
{
    public class GoodAdministrativeModel : AbstractGoodServiceModel
    {
        public GoodAdministrativeModel()
        {
        }

        public GoodAdministrativeModel(GoodOrService goodOrService, RequisitionOrReimbursement requisitionOrReimbursement, TypeRequisitionReimbursement typeRequisitionReimbursement, Int32 vendor, Int32 studentPacketCourseId, Int32 courseId, Boolean isReadOnly, Int32? id)
		  : base(goodOrService, requisitionOrReimbursement, typeRequisitionReimbursement, vendor, 0, studentPacketCourseId, courseId, null, isReadOnly)
        {
            if (id.HasValue && id.Value != 0)
            {
                using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
                {
                    var repo = uow.GetRepo<IGoodServiceRepo>();

                    GoodService dbEntity = repo.Get(id.Value);

                    Id = dbEntity.GoodServiceId;
                    LineItemName = dbEntity.LineItemName;
                    LineItemDesc = dbEntity.LineItemDesc;
                    AdminComments = dbEntity.AdminComments;
                    ApprovalGivenBy = dbEntity.ApprovalGivenBy;

                    Aprv = dbEntity.GoodOrServiceApproved.HasValue ? dbEntity.GoodOrServiceApproved.Value : GoodOrServiceApproved.b_pend;
                    IsClosed = dbEntity.IsClosed.HasValue ? dbEntity.IsClosed.Value : false;

                    NumberOfUnits = dbEntity.NumberOfUnits;
                    UnitPrice = dbEntity.UnitPrice;
                    ShippingHandlingFees = dbEntity.ShippingHandlingFees;

                    Total = NumberOfUnits * UnitPrice + ShippingHandlingFees;
                }
            }
        }

        [Display(Name = "Line Item Name")]
        public String LineItemName { get; set; }

        [Display(Name = "Line Item Desc")]
        public String LineItemDesc { get; set; }

        [Display(Name = "Admin Comments")]
        public String AdminComments { get; set; }

        //[Required]
        [Display(Name = "Approval Given By")]
        public ApprovalGivenBy? ApprovalGivenBy { get; set; }

        [Required]
        [Display(Name = "Number of Units")]
        public Int32 NumberOfUnits { get; set; }

        [Required]
        [Display(Name = "Unit Price")]
        public Decimal UnitPrice { get; set; }

        [Required]
        [Display(Name = "Shipping/Handling/Fees")]
        public Decimal ShippingHandlingFees { get; set; }

        public SelectList ApprovalGivenBys { get; set; }

        public override void Init()
        {
            ApprovalGivenBys = Data.Enums.ApprovalGivenBy.email.ToSelectListUsingDescWithoutActive();
        }
    }
}