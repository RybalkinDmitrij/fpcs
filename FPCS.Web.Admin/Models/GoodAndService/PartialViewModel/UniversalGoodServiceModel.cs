﻿using FPCS.Core.Unity;
using FPCS.Data;
using FPCS.Data.Enums;
using FPCS.Core.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FPCS.Data.Entities;
using FPCS.Data.Repo;

namespace FPCS.Web.Admin.Models.GoodAndService
{
	public class UniversalGoodServiceModel : AbstractGoodServiceModel
	{
		public UniversalGoodServiceModel()
		{
			base.GoodOrService = Data.Enums.GoodOrService.Good;
		}

		public UniversalGoodServiceModel(RequisitionOrReimbursement requisitionOrReimbursement,
			TypeRequisitionReimbursement typeRequisitionReimbursement, Int64 studentPacketId, Int32 studentPacketCourseId,
			Int32 courseId, Guid? tempId, Boolean isReadOnly, Int32? id, Int32 typeCall)
			: base(
				GoodOrService.Good, requisitionOrReimbursement, typeRequisitionReimbursement, 0, studentPacketId,
				studentPacketCourseId, courseId, tempId, isReadOnly)
		{
			if (id.HasValue && id.Value != 0)
			{
				using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
				{
					var repo = uow.GetRepo<IGoodServiceRepo>();

					GoodService dbEntity = repo.Get(id.Value);

					Id = dbEntity.GoodServiceId;
					Guid tmpId;
					if (!Guid.TryParse(dbEntity.TempId, out tmpId)) tmpId = Guid.Empty;
					TempId = tmpId.ToString();
					Title = dbEntity.Title;
					Description = dbEntity.Description;
					PublisherISBN = dbEntity.PublisherISBN;
					Comments = dbEntity.AdminComments;

					TypeCall = typeCall;

					GoodOrService = dbEntity.GoodOrService;
					RequisitionOrReimbursement = dbEntity.RequisitionOrReimbursement;
					TypeRequisitionReimbursement = dbEntity.TypeRequisitionReimbursement;
					VendorId = dbEntity.VendorId;
				    FileStoreId = dbEntity.Vendor.FileStoreId.HasValue ? dbEntity.Vendor.FileStoreId.Value : 0;

					NumberOfUnits = dbEntity.NumberOfUnits;
					UnitPrice = dbEntity.UnitPrice;
					ShippingHandlingFees = dbEntity.ShippingHandlingFees;

					Total = NumberOfUnits*UnitPrice + ShippingHandlingFees;

					FamilyEmail = dbEntity.StudentPacketCourseId.HasValue
						? dbEntity.StudentPacketCourse
							.StudentPacket
							.Student
							.Families
							.Where(t => !t.IsDeleted)
							.Select(t => t.Email)
							.FirstOrDefault()
						: dbEntity.StudentPacketId.HasValue
							? dbEntity.StudentPacket
								.Student
								.Families
								.Where(t => !t.IsDeleted)
								.Select(t => t.Email)
								.FirstOrDefault()
							: String.Empty;

					SponsorTeacherEmail = dbEntity.StudentPacketCourseId.HasValue
						? dbEntity.StudentPacketCourse
							.StudentPacket
							.SponsorTeacher
							.Email
						: dbEntity.StudentPacketId.HasValue
							? dbEntity.StudentPacket
								.SponsorTeacher
								.Email
							: String.Empty;
				}
			}
			else
			{
				using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
				{
					var repoStudentPacketCourse = uow.GetRepo<IStudentPacketCourseRepo>();

					StudentPacketCourse dbEntity = repoStudentPacketCourse.GetByStudentPacket(studentPacketId)
						.FirstOrDefault();

					if (dbEntity != null)
					{
						FamilyEmail = dbEntity.StudentPacket
							.Student
							.Families
							.Where(t => !t.IsDeleted)
							.Select(t => t.Email)
							.FirstOrDefault();

						SponsorTeacherEmail = dbEntity.StudentPacket
							.SponsorTeacher
							.Email;
					}

					TypeCall = typeCall;

					NumberOfUnits = 1;

					var repo = uow.GetRepo<IVendorRepo>();

					var vendor = repo.GetAll().FirstOrDefault(x => x.BusinessName == "FPCS Service Vendor");

					if (vendor != null)
					{
						VendorId = vendor.VendorID;
                        FileStoreId = vendor.FileStoreId.HasValue ? vendor.FileStoreId.Value : 0;
					}
					else
					{
						VendorId = 0;
					    FileStoreId = 0;
					    //vendor = repo.GetAll().FirstOrDefault();
					    //if (vendor != null)
					    //{
					    //	VendorId = vendor.VendorID;
					    //}
					}
				}
			}
		}

		[Display(Name = "Title/Name")]
		public String Title { get; set; }

		[Display(Name = "Description")]
		public String Description { get; set; }

		[Display(Name = "Publisher ISBN")]
		public String PublisherISBN { get; set; }

		[Display(Name = "Comments")]
		public String Comments { get; set; }

		[Required]
		[Display(Name = "Number of Units")]
		public Int32 NumberOfUnits { get; set; }

		[Required]
		[Display(Name = "Unit Price")]
		public Decimal UnitPrice { get; set; }

		[Required]
		[Display(Name = "Shipping/Handling/Fees")]
		public Decimal ShippingHandlingFees { get; set; }

		public SelectList RequisitionOrReimbursements { get; set; }

		public SelectList TypeRequisitionReimbursements { get; set; }

		public Int32 TypeCall { get; set; }

		public SelectList Vendors { get; set; }

		public String FamilyEmail { get; set; }

		public String SponsorTeacherEmail { get; set; }

        public Int64 FileStoreId { get; set; }

		public override void Init()
		{
		}

		public void Init(Int32 schoolYearId, Boolean isGeneralExpenses = false)
		{
			using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
			{
				RequisitionOrReimbursements = RequisitionOrReimbursement.ToSelectListUsingDesc();
				TypeRequisitionReimbursements = TypeRequisitionReimbursement.ToSelectListUsingDesc();

				if (!isGeneralExpenses)
				{
					var vendors = uow.GetRepo<IVendorRepo>()
						.GetWithoutGeneralExpenses(schoolYearId)
						.Select(x => new Lookup<Int32> {Value = x.VendorID, Text = x.BusinessName})
						.OrderBy(x => x.Text)
						.ToList();

					Vendors = new SelectList(vendors, "Value", "Text");
				}
				else
				{
					var repo = uow.GetRepo<IVendorRepo>();
					var vendors = repo.GetForGeneralExpenses(schoolYearId)
						.Select(x => new Lookup<Int32> {Value = x.VendorID, Text = x.BusinessName})
						.OrderBy(x => x.Text)
						.ToList();

					vendors.ForEach(x =>
					{
						x.Text = x.Text == "FPCS ADDITIONAL COMPUTER"
							? "FPCS ADDITIONAL COMPUTER ( MAX: $200)"
							: x.Text == "FPCS IPAD/TABLET"
								? "FPCS IPAD/TABLET ( MAX: $200)"
								: x.Text == "FPCS PRINTER"
									? "FPCS PRINTER ( MAX: $200)"
									: x.Text == "FPCS SCHOOL SUPPLIES"
										? "FPCS SCHOOL SUPPLIES ( MAX: $150)"
										: x.Text == "FPCS INTERNET"
											? "FPCS INTERNET ( MAX: $840)"
											: x.Text == "FPCS COMPUTER LEASE"
												? "FPCS COMPUTER LEASE ( MAX: $480)"
												: "";
					});

					Vendors = new SelectList(vendors, "Value", "Text");
				}
			}
		}
	}
}