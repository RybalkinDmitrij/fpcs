﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;

using FPCS.Core.Unity;
using FPCS.Data;
using FPCS.Data.Repo;
using FPCS.Data.Enums;
using FPCS.Core.Extensions;
using DE = FPCS.Data.Entities;

namespace FPCS.Web.Admin.Models.GoodAndService
{
    public class GoodAndServiceCreateModel
    {
        [Required]
        [Display(Name = "Good or Service")]
        public GoodOrService GoodOrService { get; set; }

        [Required]
        [Display(Name = "Requisition Or Reimbursement")]
        public RequisitionOrReimbursement RequisitionOrReimbursement { get; set; }

        [Required]
        [Display(Name = "Type of Requisition or Reimbursement")]
        public TypeRequisitionReimbursement TypeRequisitionReimbursement { get; set; }

        [Required]
        [Display(Name = "Vendor")]
        public DE.Vendor Vendor { get; set; }

        public SelectList GoodsOrServices { get; set; }

        public SelectList RequisitionsOrReimbursements { get; set; }

        public SelectList TypesRequisitionReimbursement { get; set; }

        public SelectList Vendors { get; set; }

        public void Init(Int32 schoolYearId)
        {
            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                GoodsOrServices = GoodOrService.ToSelectListUsingDesc();
                RequisitionsOrReimbursements = RequisitionOrReimbursement.ToSelectListUsingDesc();
                TypesRequisitionReimbursement = TypeRequisitionReimbursement.ToSelectListUsingDesc();

                var vendors = uow.GetRepo<IVendorRepo>()
                                            .GetAll(schoolYearId)
                                            .Select(x => new Lookup<Int32> { Value = x.VendorID, Text = x.BusinessName })
                                            .OrderBy(x=>x.Text)
                                            .ToList();

                Vendors = new SelectList(vendors, "Value", "Text");
            }
        }
    }
}