﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FPCS.Web.Admin.Models.GoodAndService
{
    public class GoodsAndServicesModel
    {
        public Int64? EntityId { get; set; }

        public String BudgetItem { get; set; }

        public String Status { get; set; }

        public String Desciption { get; set; }

        public Decimal QTY { get; set; }

        public Decimal UnitCost { get; set; }

        public Decimal SH { get; set; }

        public Decimal BudgetTotal { get; set; }

        public Decimal ActualCharges { get; set; }

        public Decimal BudgetAdjust { get; set; }

        public Decimal BudgetBalance { get; set; }
    }
}