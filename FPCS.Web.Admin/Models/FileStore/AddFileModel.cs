﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace FPCS.Web.Admin.Models.StoredFile
{
    public class AddFileModel
    {
        public String Name { get; set; }

        public String URL { get; set; }

        public Int64 ContentSize { get; set; }

        public DateTime? DateCreated { get; set; }

        public String MimeType { get; set; }
    }
}