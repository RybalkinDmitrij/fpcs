﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace FPCS.Web.Admin.Models.StoredFile
{
    public class PrintFileStoreModel
    {
        public String Guid { get; set; }

        public String Name { get; set; }
    }
}