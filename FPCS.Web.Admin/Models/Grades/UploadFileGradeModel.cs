﻿using FPCS.Data.Entities;
using FPCS.Data.Enums;
using FPCS.Core.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FPCS.Web.Admin.Models.Grades
{
    public class UploadFileGradeModel
    {
        public UploadFileGradeModel()
        {
        }

        public UploadFileGradeModel(StudentPacketCourse dbEntity, String q)
        {
            StudentPacketCourseId = dbEntity.StudentPacketCourseId;
            QNumber = q;

            Init();
        }

        public Int64 StudentPacketCourseId { get; set; }

        public String QNumber { get; set; }

        public void Init()
        {
        }
    }
}