﻿using FPCS.Data.Entities;
using FPCS.Data.Enums;
using FPCS.Core.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FPCS.Web.Admin.Models.Grades
{
    public class EditGradeModel
    {
        public EditGradeModel()
        {
        }

        public EditGradeModel(StudentPacketCourse dbEntity)
        {
            StudentPacketCourseId = dbEntity.StudentPacketCourseId;
            StudentName = String.Format(@"{0}, {1}", dbEntity.StudentPacket.Student.LastName,
                dbEntity.StudentPacket.Student.FirstName);
            ASDCode = dbEntity.FPCSCourse.ASDCourse.ExternalASDCourseId;
            CourseTitle = dbEntity.FPCSCourse.Name;
            TeacherName = dbEntity.FPCSCourse.Teacher != null ?
                                String.Format(@"{0}, {1}", 
                                    dbEntity.FPCSCourse.Teacher.LastName,
                                    dbEntity.FPCSCourse.Teacher.FirstName) :
                                String.Empty;
            GuardianName = dbEntity.FPCSCourse.Guardian != null ?
                                String.Format(@"{0}, {1}",
                                    dbEntity.FPCSCourse.Guardian.LastName,
                                    dbEntity.FPCSCourse.Guardian.FirstName) :
                                String.Empty;
            Q11 = dbEntity.Q11;
            Q11GuidFile = dbEntity.Q11GuidFile;
            Q12 = dbEntity.Q12;
            Q12GuidFile = dbEntity.Q12GuidFile;
            Q13 = dbEntity.Q13;
            Q13GuidFile = dbEntity.Q13GuidFile;
            Q1 = dbEntity.Q1;
            Q21 = dbEntity.Q21;
            Q21GuidFile = dbEntity.Q21GuidFile;
            Q22 = dbEntity.Q22;
            Q22GuidFile = dbEntity.Q22GuidFile;
            Q23 = dbEntity.Q23;
            Q23GuidFile = dbEntity.Q23GuidFile;
            Q2 = dbEntity.Q2;
            Sem1 = dbEntity.Sem1;
            Q31 = dbEntity.Q31;
            Q31GuidFile = dbEntity.Q31GuidFile;
            Q32 = dbEntity.Q32;
            Q32GuidFile = dbEntity.Q32GuidFile;
            Q33 = dbEntity.Q33;
            Q33GuidFile = dbEntity.Q33GuidFile;
            Q3 = dbEntity.Q3;
            Q41 = dbEntity.Q41;
            Q41GuidFile = dbEntity.Q41GuidFile;
            Q42 = dbEntity.Q42;
            Q42GuidFile = dbEntity.Q42GuidFile;
            Q43 = dbEntity.Q43;
            Q43GuidFile = dbEntity.Q43GuidFile;
            Q4 = dbEntity.Q4;
            Sem2 = dbEntity.Sem2;
            Credits = dbEntity.Credit.HasValue && dbEntity.Credit.Value == 0
                ? GradeCredits.n000
                : dbEntity.Credit.HasValue && dbEntity.Credit.Value == 0.25m
                    ? GradeCredits.n025
                    : dbEntity.Credit.HasValue && dbEntity.Credit.Value == 0.50m
                        ? GradeCredits.n050
                        : dbEntity.Credit.HasValue && dbEntity.Credit.Value == 1.00m
                            ? GradeCredits.n100
                            : GradeCredits.n000;
            Comment = dbEntity.Comment;
            IsInSystem = dbEntity.IsInSystem;
            IsLock = dbEntity.IsLock;

            Init();
        }

        public Int64 StudentPacketCourseId { get; set; }

        [Display(Name = "Student Name")]
        public String StudentName { get; set; }

        [Display(Name = "ASD Code")]
        public String ASDCode { get; set; }

        [Display(Name = "Course Title")]
        public String CourseTitle { get; set; }

        [Display(Name = "Teacher Name")]
        public String TeacherName { get; set; }

        [Display(Name = "Guardian Name")]
        public String GuardianName { get; set; }

        [Display(Name = "Q1 (1)")]
        public String Q11 { get; set; }

        public String Q11GuidFile { get; set; }

        public Boolean HasQ11File
        {
            get
            {
                return !String.IsNullOrEmpty(Q11GuidFile);
            }
        }

        [Display(Name = "Q1 (2)")]
        public String Q12 { get; set; }

        public String Q12GuidFile { get; set; }

        public Boolean HasQ12File
        {
            get
            {
                return !String.IsNullOrEmpty(Q12GuidFile);
            }
        }

        [Display(Name = "Q1 (3)")]
        public String Q13 { get; set; }

        public String Q13GuidFile { get; set; }

        public Boolean HasQ13File
        {
            get
            {
                return !String.IsNullOrEmpty(Q13GuidFile);
            }
        }

        [Display(Name = "Q1")]
        public String Q1 { get; set; }

        [Display(Name = "Q2 (1)")]
        public String Q21 { get; set; }

        public String Q21GuidFile { get; set; }

        public Boolean HasQ21File
        {
            get
            {
                return !String.IsNullOrEmpty(Q21GuidFile);
            }
        }

        [Display(Name = "Q2 (2)")]
        public String Q22 { get; set; }

        public String Q22GuidFile { get; set; }

        public Boolean HasQ22File
        {
            get
            {
                return !String.IsNullOrEmpty(Q22GuidFile);
            }
        }

        [Display(Name = "Q2 (3)")]
        public String Q23 { get; set; }

        public String Q23GuidFile { get; set; }

        public Boolean HasQ23File
        {
            get
            {
                return !String.IsNullOrEmpty(Q23GuidFile);
            }
        }

        [Display(Name = "Q2")]
        public String Q2 { get; set; }

        [Display(Name = "Sem 1")]
        public String Sem1 { get; set; }

        [Display(Name = "Q3 (1)")]
        public String Q31 { get; set; }

        public String Q31GuidFile { get; set; }

        public Boolean HasQ31File
        {
            get
            {
                return !String.IsNullOrEmpty(Q31GuidFile);
            }
        }

        [Display(Name = "Q3 (2)")]
        public String Q32 { get; set; }

        public String Q32GuidFile { get; set; }

        public Boolean HasQ32File
        {
            get
            {
                return !String.IsNullOrEmpty(Q32GuidFile);
            }
        }

        [Display(Name = "Q3 (3)")]
        public String Q33 { get; set; }

        public String Q33GuidFile { get; set; }

        public Boolean HasQ33File
        {
            get
            {
                return !String.IsNullOrEmpty(Q33GuidFile);
            }
        }

        [Display(Name = "Q3")]
        public String Q3 { get; set; }

        [Display(Name = "Q4 (1)")]
        public String Q41 { get; set; }

        public String Q41GuidFile { get; set; }

        public Boolean HasQ41File
        {
            get
            {
                return !String.IsNullOrEmpty(Q41GuidFile);
            }
        }

        [Display(Name = "Q4 (2)")]
        public String Q42 { get; set; }

        public String Q42GuidFile { get; set; }

        public Boolean HasQ42File
        {
            get
            {
                return !String.IsNullOrEmpty(Q42GuidFile);
            }
        }

        [Display(Name = "Q4 (3)")]
        public String Q43 { get; set; }

        public String Q43GuidFile { get; set; }

        public Boolean HasQ43File
        {
            get
            {
                return !String.IsNullOrEmpty(Q43GuidFile);
            }
        }

        [Display(Name = "Q4")]
        public String Q4 { get; set; }

        [Display(Name = "Sem 2")]
        public String Sem2 { get; set; }

        [Display(Name = "Credits")]
        public GradeCredits Credits { get; set; }

        [Display(Name = "Comment")]
        public String Comment { get; set; }

        [Display(Name = "In Zangle")]
        public Boolean IsInSystem { get; set; }

        [Display(Name = "Lock")]
        public Boolean IsLock { get; set; }

        public SelectList Grades { get; set; }

        public SelectList SampleGrades { get; set; }

        public void Init()
        {
            Grades = Credits.ToSelectListUsingDesc();

            List<Lookup<String>> values = new List<Lookup<String>>();
            values.Add(new Lookup<String>() { Value = null, Text = "" });
            values.Add(new Lookup<String>() { Value = "A", Text = "A" });
            values.Add(new Lookup<String>() { Value = "B", Text = "B" });
            values.Add(new Lookup<String>() { Value = "C", Text = "C" });
            values.Add(new Lookup<String>() { Value = "D", Text = "D" });

            SampleGrades = new SelectList(values, "Value", "Text");
        }
    }
}