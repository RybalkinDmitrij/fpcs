﻿using FPCS.Core.jqGrid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FPCS.Web.Admin.Models.Grades
{
    public class GradeListOptions
    {
        [GridProperty(ExtensionType.All, true, "StudentLastName", FilterOperation.Contains)]
        public String StudentName { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Contains)]
        public String ASDCode { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Contains)]
        public String CourseTitle { get; set; }

        [GridProperty(ExtensionType.All, true, "TeacherLastName", FilterOperation.Contains)]
        public String TeacherName { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Equal)]
        public String Q1 { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Equal)]
        public String Q2 { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Equal)]
        public String Sem1 { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Equal)]
        public String Q3 { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Equal)]
        public String Q4 { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Equal)]
        public String Sem2 { get; set; }

        [GridProperty(ExtensionType.All, true, "Credit", FilterOperation.Equal)]
        public Decimal? Credits { get; set; }

        [GridProperty(ExtensionType.All, true)]
        public String Comment { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Equal)]
        public Boolean? IsInSystem { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Equal)]
        public Boolean? IsLock { get; set; }
    }
}