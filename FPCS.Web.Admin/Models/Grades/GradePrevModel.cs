﻿using FPCS.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FPCS.Web.Admin.Models.Grades
{
    public class GradePrevModel
    {
        public Int64 StudentPacketCourseId { get; set; }

        public String StudentLastName { get; set; }

        public String StudentFirstName { get; set; }

        public String ASDCode { get; set; }

        public String CourseTitle { get; set; }

        public CourseClassType? ClassType { get; set; }

        public Guid? TeacherId { get; set; }

        public String TeacherLastName { get; set; }

        public String TeacherFirstName { get; set; }

        public Guid? GuardianId { get; set; }

        public String GuardianLastName { get; set; }

        public String GuardianFirstName { get; set; }

        public String Q11 { get; set; }

        public String Q12 { get; set; }

        public String Q13 { get; set; }

        public String Q1 { get; set; }

        public String Q21 { get; set; }

        public String Q22 { get; set; }

        public String Q23 { get; set; }

        public String Q2 { get; set; }

        public String Sem1 { get; set; }

        public String Q31 { get; set; }

        public String Q32 { get; set; }

        public String Q33 { get; set; }

        public String Q3 { get; set; }

        public String Q41 { get; set; }

        public String Q42 { get; set; }

        public String Q43 { get; set; }

        public String Q4 { get; set; }

        public String Sem2 { get; set; }

        public Decimal Credit { get; set; }

        public String Comment { get; set; }

        public Boolean IsInSystem { get; set; }

        public Boolean IsLock { get; set; }
    }
}