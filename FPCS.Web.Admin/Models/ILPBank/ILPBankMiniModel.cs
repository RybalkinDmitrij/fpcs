﻿using System;

namespace FPCS.Web.Admin.Models.ILPBank
{
    public class ILPBankMiniModel
    {
        public Int32 ILPBankId { get; set; }

        public String Name { get; set; }

        public Guid? TeacherId { get; set; }

        public String Teacher { get; set; }

        public Guid? GuardianId { get; set; }

        public String Guardian { get; set; }

        public String Subject { get; set; }

        public Int32 Year { get; set; }

        public String IsPublic { get; set; }
    }
}