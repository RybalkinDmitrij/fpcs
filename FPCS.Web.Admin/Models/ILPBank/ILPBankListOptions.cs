﻿using System;

using FPCS.Core.jqGrid;

namespace FPCS.Web.Admin.Models.ILPBank
{
    public class ILPBankListOptions
    {
        [GridProperty(ExtensionType.All, true)]
        public Int32? ILPBankId { get; set; }

        [GridProperty(ExtensionType.All, true, "ILPName", FilterOperation.Contains)]
        public String Name { get; set; }

        [GridProperty(ExtensionType.All, true, "Teacher.FullName", FilterOperation.Contains)]
        public String Teacher { get; set; }

        [GridProperty(ExtensionType.All, true, "Guardian.FullName", FilterOperation.Contains)]
        public String Guardian { get; set; }

        [GridProperty(ExtensionType.All, true, "Subject.Name", FilterOperation.Contains)]
        public String Subject { get; set; }

        [GridProperty(ExtensionType.All, true, "SchoolYear.Year", FilterOperation.Equal)]
        public Int32? Year { get; set; }

        [GridProperty(ExtensionType.All, false)]
        public Boolean? IsPublic { get; set; }
    }
}