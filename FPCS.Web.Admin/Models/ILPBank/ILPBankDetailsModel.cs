﻿using FPCS.Core.Unity;
using FPCS.Data;
using FPCS.Data.Entities;
using DBEntities = FPCS.Data.Entities;
using FPCS.Data.Repo;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using DBE = FPCS.Data.Entities;

namespace FPCS.Web.Admin.Models.ILPBank
{
    public class ILPBankDetailsModel
    {
        public ILPBankDetailsModel()
        {
        }

        public Int32 ILPBankId { get; set; }

        public Int32 ILPId { get; set; }

        [Display(Name = "Instructor")]
        public String Instructor { get; set; }

        [Display(Name = "Class Name")]
        public String ClassName { get; set; }

        [Display(Name = "School Year")]
        public Int32 SchoolYear { get; set; }

        [Display(Name = "Course Description")]
        public String ILPName { get; set; }

        [Display(Name = "Teacher Name")]
        public String TeacherName { get; set; }

        [Display(Name = "Guardian Name")]
        public String GuardianName { get; set; }

        [Display(Name = "Public Or Private")]
        public String PublicOrPrivate { get; set; }

        [Display(Name = "Course Hrs")]
        public Int32 CourseHrs { get; set; }

        /// <summary>
        /// Description of the course including methods needed ...
        /// </summary>
        [Display(Name = "Description of the course including methods, curriculum, and assignments")]
        public String DescriptionCourse { get; set; }

        /// <summary>
        /// Standards: Common core or GLE
        /// </summary>
        [Display(Name = "Standards: Common core or GLE")]
        public String Standards { get; set; }

        /// <summary>
        /// Student Activities
        /// </summary>
        [Display(Name = "Student Activities")]
        public String StudentActivities { get; set; }

        /// <summary>
        /// Materials, Resources
        /// </summary>
        [Display(Name = "Materials, Resources")]
        public String MaterialsResources { get; set; }

        /// <summary>
        /// Role of Parent/Teacher/Vendor/any additional responsibilities of the student
        /// </summary>
        [Display(Name = "Role of Parent/Teacher/Vendor/any additional responsibilities of the student")]
        public String RoleAnyPeople { get; set; }

        /// <summary>
        /// Evaluation and Grading (Pass/Fail)
        /// </summary>
        [Display(Name = "Pass/Fail")]
        public Boolean EvaluationGradingPassFail { get; set; }

        /// <summary>
        /// Evaluation and Grading (Grading Scale)
        /// </summary>
        [Display(Name = "Grading Scale")]
        public Boolean EvaluationGradingGradingScale { get; set; }

        /// <summary>
        /// Evaluation and Grading (O/S/N)
        /// </summary>
        [Display(Name = "O/S/N")]
        public Boolean EvaluationGradingOSN { get; set; }

        /// <summary>
        /// Evaluation and Grading (Other)
        /// </summary>
        [Display(Name = "Other (if checked you MUST explain why in box)")]
        public Boolean EvaluationGradingOther { get; set; }

        /// <summary>
        /// Evaluation and Grading (Other Explain)
        /// </summary>
        [Display(Name = "Explain")]
        public String EvaluationGradingOtherExplain { get; set; }

        /// <summary>
        /// What will be evaluated? What will be the measurable outcomes?
        /// </summary>
        [Display(Name = "What will be evaluated? What will be the measurable outcomes?")]
        public String EvaluatedMeasurableOutcomes { get; set; }

        /// <summary>
        /// Course Syllabus: work out a timeline (scope and sequence) of all major topics to be covered.
        /// </summary>
        [Display(Name = "Course Syllabus: work out a timeline (scope and sequence) of all major topics to be covered.")]
        public String CourseSyllabus { get; set; }

        /// <summary>
        /// Guardian ILP Modifications
        /// </summary>
        [Display(Name = "Guardian Course Description Modifications")]
        public String GuardianILPModifications { get; set; }

        /// <summary>
        /// Instructor ILP Modifications
        /// </summary>
        [Display(Name = "Instructor Course Description Modifications")]
        public String InstructorILPModifications { get; set; }

        /// <summary>
        /// Add ILP to Bank?
        /// </summary>
        [Display(Name = "Add Course Description to Bank?")]
        public Boolean AddILPToBank { get; set; }
    }
}