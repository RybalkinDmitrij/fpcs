﻿using FPCS.Core.Unity;
using FPCS.Data;
using FPCS.Data.Entities;
using DBEntities = FPCS.Data.Entities;
using FPCS.Data.Repo;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using DBE = FPCS.Data.Entities;

namespace FPCS.Web.Admin.Models.ILP
{
	public class ILPPrintModel
	{
		public ILPPrintModel()
		{
		}

		public ILPPrintModel(Int32 id)
		{
			using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
			{
				DBE.ILP dbEntity = uow.GetRepo<IILPRepo>().Get(id);

				StudentPacketCourseId = dbEntity.StudentPacketCourseId.GetValueOrDefault(0);
				FPCSCourseId = dbEntity.FPCSCourseId.GetValueOrDefault(0);

				var repoSchoolYear = uow.GetRepo<ISchoolYearRepo>();

				if (StudentPacketCourseId != 0)
				{
					var repoStudentPacketCourse = uow.GetRepo<IStudentPacketCourseRepo>();

					StudentPacketCourse dbStudentPacketCourse = repoStudentPacketCourse.Get(StudentPacketCourseId);

					StudentName = dbStudentPacketCourse.StudentPacket.Student.LastName + ", " +
					              dbStudentPacketCourse.StudentPacket.Student.FirstName;

					if (dbStudentPacketCourse.FPCSCourse.Teacher != null)
					{
						Instructor = String.Format(@"{0} {1}", dbStudentPacketCourse.FPCSCourse.Teacher.LastName,
							dbStudentPacketCourse.FPCSCourse.Teacher.FirstName);
						ClassName = String.Format(@"{0}: {1}", dbStudentPacketCourse.FPCSCourse.Teacher.LastName,
							dbStudentPacketCourse.FPCSCourse.Name);
					}
					else
					{
						Instructor = String.Format(@"{0} {1}", dbStudentPacketCourse.FPCSCourse.Guardian.LastName,
							dbStudentPacketCourse.FPCSCourse.Guardian.FirstName);
						ClassName = String.Format(@"{0}: {1}", dbStudentPacketCourse.FPCSCourse.Guardian.LastName,
							dbStudentPacketCourse.FPCSCourse.Name);
					}

					SchoolYear = repoSchoolYear.GetByYear(dbStudentPacketCourse.FPCSCourse.ClassEndDate.Year).Name;

					dbEntity = uow.GetRepo<IILPRepo>()
						.GetByStudentPacketCourse(dbStudentPacketCourse.StudentPacketCourseId);
				}
				else if (FPCSCourseId != 0)
				{
					StudentName = String.Empty;

					var repoFPCSCourse = uow.GetRepo<IFPCSCourseRepo>();

					DBEntities.FPCSCourse dbFPCSCourse = repoFPCSCourse.Get(FPCSCourseId);

					if (dbFPCSCourse.Teacher != null)
					{
						Instructor = String.Format(@"{0} {1}", dbFPCSCourse.Teacher.LastName, dbFPCSCourse.Teacher.FirstName);
						ClassName = String.Format(@"{0}: {1}", dbFPCSCourse.Teacher.LastName, dbFPCSCourse.Name);
					}
					else
					{
						Instructor = String.Format(@"{0} {1}", dbFPCSCourse.Guardian.LastName, dbFPCSCourse.Guardian.FirstName);
						ClassName = String.Format(@"{0}: {1}", dbFPCSCourse.Guardian.LastName, dbFPCSCourse.Name);
					}

					SchoolYear = repoSchoolYear.GetByYear(dbFPCSCourse.ClassEndDate.Year).Name;

					dbEntity = uow.GetRepo<IILPRepo>()
						.GetByFPCSCourse(dbFPCSCourse.FPCSCourseId);
				}

				if (dbEntity != null)
				{
					ILPId = dbEntity.ILPId;
					ILPName = dbEntity.ILPName;
					StudentPacketCourseId = dbEntity.StudentPacketCourseId.GetValueOrDefault(0);
					FPCSCourseId = dbEntity.FPCSCourseId.GetValueOrDefault(0);
					CourseHrs = dbEntity.CourseHrs;
					DescriptionCourse = dbEntity.DescriptionCourse;
					Standards = dbEntity.Standards;
					StudentActivities = dbEntity.StudentActivities;
					MaterialsResources = dbEntity.MaterialsResources;
					RoleAnyPeople = dbEntity.RoleAnyPeople;
					EvaluationGradingPassFail = dbEntity.EvaluationGradingPassFail;
					EvaluationGradingGradingScale = dbEntity.EvaluationGradingGradingScale;
					EvaluationGradingOSN = dbEntity.EvaluationGradingOSN;
					EvaluationGradingOther = dbEntity.EvaluationGradingOther;
					EvaluationGradingOtherExplain = dbEntity.EvaluationGradingOtherExplain;
					EvaluatedMeasurableOutcomes = dbEntity.EvaluatedMeasurableOutcomes;
					CourseSyllabus = dbEntity.CourseSyllabus;
					GuardianILPModifications = dbEntity.GuardianILPModifications;
					InstructorILPModifications = dbEntity.InstructorILPModifications;
				}
			}
		}

		public Int32 ILPId { get; set; }

		[Display(Name = "Instructor")]
		public String Instructor { get; set; }

		[Display(Name = "Class Name")]
		public String ClassName { get; set; }

		[Display(Name = "School Year")]
		public String SchoolYear { get; set; }

		[Required]
		[Display(Name = "Course Description")]
		public String ILPName { get; set; }

		public Int64 StudentPacketCourseId { get; set; }

		public Int64 FPCSCourseId { get; set; }

		[Display(Name = "Course Hrs")]
		public Int32 CourseHrs { get; set; }

		/// <summary>
		/// Description of the course including methods needed ...
		/// </summary>
		[Display(Name = "Description of the course including methods, curriculum, and assignments")]
		public String DescriptionCourse { get; set; }

		/// <summary>
		/// Standards: Common core or GLE
		/// </summary>
		[Display(Name = "Standards: Common core or GLE")]
		public String Standards { get; set; }

		/// <summary>
		/// Student Activities
		/// </summary>
		[Display(Name = "Student Activities")]
		public String StudentActivities { get; set; }

		/// <summary>
		/// Materials, Resources
		/// </summary>
		[Display(Name = "Materials, Resources")]
		public String MaterialsResources { get; set; }

		/// <summary>
		/// Role of Parent/Teacher/Vendor/any additional responsibilities of the student
		/// </summary>
		[Display(Name = "Role of Parent/Teacher/Vendor/any additional responsibilities of the student")]
		public String RoleAnyPeople { get; set; }

		/// <summary>
		/// Evaluation and Grading (Pass/Fail)
		/// </summary>
		[Display(Name = "Pass/Fail")]
		public Boolean EvaluationGradingPassFail { get; set; }

		/// <summary>
		/// Evaluation and Grading (Grading Scale)
		/// </summary>
		[Display(Name = "Grading Scale")]
		public Boolean EvaluationGradingGradingScale { get; set; }

		/// <summary>
		/// Evaluation and Grading (O/S/N)
		/// </summary>
		[Display(Name = "O/S/N")]
		public Boolean EvaluationGradingOSN { get; set; }

		/// <summary>
		/// Evaluation and Grading (Other)
		/// </summary>
		[Display(Name = "Other (if checked you MUST explain why in box)")]
		public Boolean EvaluationGradingOther { get; set; }

		/// <summary>
		/// Evaluation and Grading (Other Explain)
		/// </summary>
		[Display(Name = "Explain")]
		public String EvaluationGradingOtherExplain { get; set; }

		/// <summary>
		/// What will be evaluated? What will be the measurable outcomes?
		/// </summary>
		[Display(Name = "What will be evaluated? What will be the measurable outcomes?")]
		public String EvaluatedMeasurableOutcomes { get; set; }

		/// <summary>
		/// Course Syllabus: work out a timeline (scope and sequence) of all major topics to be covered.
		/// </summary>
		[Display(Name = "Course Syllabus: work out a timeline (scope and sequence) of all major topics to be covered.")]
		public String CourseSyllabus { get; set; }

		/// <summary>
		/// Guardian ILP Modifications
		/// </summary>
		[Display(Name = "Guardian Course Description Modifications")]
		public String GuardianILPModifications { get; set; }

		/// <summary>
		/// Instructor ILP Modifications
		/// </summary>
		[Display(Name = "Instructor Course Description Modifications")]
		public String InstructorILPModifications { get; set; }

		/// <summary>
		/// Add ILP to Bank?
		/// </summary>
		[Display(Name = "Add Course Description to Bank?")]
		public Boolean AddILPToBank { get; set; }

		public String PrintAddress { get; set; }

		public String PrintState { get; set; }

		public String PrintPhone { get; set; }

		public String PrintFax { get; set; }

		public String PrintDateNow { get; set; }

		public String StudentName { get; set; }
	}
}