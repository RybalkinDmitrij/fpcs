﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FPCS.Web.Admin.Models.Student.Abstract;

namespace FPCS.Web.Admin.Models.Family
{
    public class LightStudentModel : AbstractStudent
    {
        public Guid DbUserId { get; set; }

        public String FullName { get; set; }

        public Int32 GradYear { get; set; }
    }
}