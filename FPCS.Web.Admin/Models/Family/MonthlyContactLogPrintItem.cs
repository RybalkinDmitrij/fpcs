﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FPCS.Web.Admin.Models.Family
{
    public class MonthlyContactLogPrintItem
    {
        public String TeacherName { get; set; }

        public String Date { get; set; }

        public String Comment { get; set; }

        public String FamilyName { get; set;  }
    }
}