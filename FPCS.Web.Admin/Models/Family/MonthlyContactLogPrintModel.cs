﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FPCS.Web.Admin.Models.Family
{
    public class MonthlyContactLogPrintModel
    {
        public List<MonthlyContactLogPrintItem> Data { get; set; }

        public String PrintAddress { get; set; }

        public String PrintState { get; set; }

        public String PrintPhone { get; set; }

        public String PrintFax { get; set; }

        public String PrintDateNow { get; set; }
    }
}