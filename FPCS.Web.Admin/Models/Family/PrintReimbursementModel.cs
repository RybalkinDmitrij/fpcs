﻿using FPCS.Data.Enums;
using FPCS.Core.Extensions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace FPCS.Web.Admin.Models.Family
{
    public class PrintReimbursementModel
    {
        public PrintReimbursementModel()
        {
        }

        public List<ReimbursementModel> Reimbursements { get; set; }

        public String VendorName { get; set; }

        public String PrintAddress { get; set; }

        public String PrintState { get; set; }

        public String PrintPhone { get; set; }

        public String PrintFax { get; set; }

        public String PrintDateNow { get; set; }

        public String Title { get; set; }

        public Decimal Total { get; set; }
    }
}