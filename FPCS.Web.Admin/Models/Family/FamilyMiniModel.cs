﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FPCS.Web.Admin.Models.Family
{
    public class FamilyMiniModel
    {
        [Display(Name = "Family Id")]
        public Int64 FamilyId { get; set; }

        [Display(Name = "Family Name")]
        public String FamilyName { get; set; }

        [Display(Name = "First Names")]
        public String FirstNames { get; set; }

        [Display(Name = "Family Email Address")]
        public String Email { get; set; }

        [Display(Name = "Home Phone")]
        public String Telephone { get; set; }
    }
}