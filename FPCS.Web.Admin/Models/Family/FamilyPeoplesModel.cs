﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace FPCS.Web.Admin.Models.Family
{
    public class FamilyPeoplesModel
    {
        [Display(Name = "Family guardians")]
        public IEnumerable<LightGuardianModel> FamilyGuardians { get; set; }

        [Display(Name = "Family students")]
        public IEnumerable<LightStudentModel> FamilyStudents { get; set; }

        public SelectList Guardians { get; set; }

        public SelectList Students { get; set; }
    }
}