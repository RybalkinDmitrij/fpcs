﻿using System;

using FPCS.Core.jqGrid;

namespace FPCS.Web.Admin.Models.Family
{
    public class FamilyListOptions
    {
        [GridProperty(ExtensionType.All, true)]
        public Int64? FamilyId { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Contains)]
        public String Name { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Contains)]
        public String Email { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Contains)]
        public String Telephone { get; set; }
    }
}