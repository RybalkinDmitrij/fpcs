﻿using System;

using FPCS.Core.jqGrid;

namespace FPCS.Web.Admin.Models.Family
{
    public class MonthlyContactLogListOptions
    {
        [GridProperty(ExtensionType.All, true, FilterOperation.Contains)]
        public String TeacherName { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Contains)]
        public String Date { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Contains)]
        public String Comment { get; set; }
    }
}