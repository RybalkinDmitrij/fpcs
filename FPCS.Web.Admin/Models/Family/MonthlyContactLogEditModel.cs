﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FPCS.Web.Admin.Models.Family
{
    public class MonthlyContactLogEditModel
    {
        public Int32 MonthlyContactLogId { get; set; }

        public Int64 FamilyId { get; set; }
        
        public Guid TeacherId { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "Date record")]
        public DateTime Date { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "Comment")]
        public String Comment { get; set; }

        public void Init()
        {
        }
    }
}