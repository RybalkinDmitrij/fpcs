﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FPCS.Web.Admin.Models.Family
{
    public class MonthlyContactLogIndexModel
    {
        public Int64 FamilyId { get; set; }

        public String FamilyName { get; set; }

        public String StudentName { get; set; }
    }
}