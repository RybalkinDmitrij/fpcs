﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FPCS.Web.Admin.Models.Family
{
    public class LightGuardianModel
    {
        public Guid DbUserId { get; set; }

        public String FullName { get; set; }

        public String CellPhone { get; set; }
    }
}