﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FPCS.Web.Admin.Models.Family
{
    public class MonthlyContactLogMiniModel
    {
        public Int32 MonthlyContactLogId { get; set; }

        public Int64 FamilyId { get; set; }

        [Display(Name = "Family Name")]
        public String FamilyName { get; set; }

        public Guid TeacherId { get; set; }

        [Display(Name = "Teacher Name")]
        public String TeacherName { get; set; }

        [Display(Name = "Date record")]
        public String Date { get; set; }

        [Display(Name = "Comment")]
        public String Comment { get; set; }
    }
}