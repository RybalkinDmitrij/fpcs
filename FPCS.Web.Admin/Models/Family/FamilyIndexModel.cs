﻿using FPCS.Core.Unity;
using FPCS.Data;
using FPCS.Data.Repo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FPCS.Web.Admin.Models.Family
{
    public class FamilyIndexModel
    {
        public SelectList Families { get; set; }

        public void Init(Int32 schoolYearId)
        {
            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IFamilyRepo>();
                var dbList = repo.GetAll(schoolYearId);

                var result = dbList.Select(x => new FamilyMiniModel
                {
                    FamilyId = x.FamilyId,
                    FamilyName = x.Name,
                    FirstNames = String.Empty,
                    Telephone = x.Telephone,
                    Email = x.Email,
                })
                .ToList();

                var guardians = uow.GetRepo<IGuardianRepo>()
                                              .GetByFamilies(schoolYearId, result.Select(t => t.FamilyId).ToList())
                                              .Select(x => new
                                              {
                                                  GuardianId = x.DbUserId,
                                                  GuardianFirstName = x.FirstName,
                                                  GuardianLastName = x.LastName,
                                                  FamilyIds = x.Families.Where(t => !t.IsDeleted).Select(t => t.FamilyId)
                                              })
                                              .ToList();

                foreach (var item in result)
                {
                    var names = guardians.Where(x => x.FamilyIds.Contains(item.FamilyId)).Select(x => new { x.GuardianLastName, x.GuardianFirstName }).ToList();
                    var lastNames = names.Select(x => x.GuardianLastName).Distinct();

                    if (lastNames.Count() == 1)
                    {
                        item.FirstNames = names[0].GuardianLastName + ", " + String.Join(" & ", names.Select(x => x.GuardianFirstName));
                    }
                    else
                    {
                        item.FirstNames = String.Join(" - ", lastNames) + ", " + String.Join(" & ", names.Select(x => x.GuardianFirstName));
                    }
                }

                var resultSLI = result.Select(x => new SelectListItem()
                    {
                        Text = x.FirstNames,
                        Value = x.FamilyId.ToString()
                    })
                    .ToList();

                resultSLI.Add(new SelectListItem() { Text = "", Value = "0" });

                Families = new SelectList(resultSLI.OrderBy(x => x.Text).ToList(), "Value", "Text");
            }
        }
    }
}