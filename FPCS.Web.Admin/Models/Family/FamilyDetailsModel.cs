﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;

using FPCS.Core.Unity;
using FPCS.Data;
using FPCS.Data.Repo;

namespace FPCS.Web.Admin.Models.Family
{
    public class FamilyDetailsModel
    {
        [Required]
        [Display(Name = "Family Id")]
        public Int64 FamilyId { get; set; }

        [Display(Name = "Family Name")]
        public String Name { get; set; }

        [Display(Name = "Description")]
        public String Description { get; set; }

        [Display(Name = "Home address")]
        public String Address { get; set; }

        [Display(Name = "Mailing address")]
        public String MailingAddress { get; set; }

        [Display(Name = "Home Phone")]
        public String Telephone { get; set; }

        [Display(Name = "Cell Phone")]
        public String CellPhone { get; set; }

        [Display(Name = "City")]
        public String City { get; set; }

        [Display(Name = "Country")]
        public String Country { get; set; }

        [Display(Name = "Zip")]
        public String Zip { get; set; }

        [EmailAddress]
        [Display(Name = "Family Email Address")]
        public String Email { get; set; }

        [Required]
        [Display(Name = "Add Family to Directory")]
        public Boolean IsUseDirectory { get; set; }

        [Required]
        [Display(Name = "State")]
        public Int64 StateId { get; set; }

        [Display(Name = "Family guardians")]
        public IEnumerable<LightGuardianModel> FamilyGuardians { get; set; }

        [Display(Name = "Family students")]
        public IEnumerable<LightStudentModel> FamilyStudents { get; set; }

        public SelectList States { get; set; }

        public SelectList Guardians { get; set; }

        public SelectList Students { get; set; }

        public void Init()
        {
            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                var states = uow.GetRepo<IStateRepo>().GetAll().Select(x => new Lookup<Int64> { Value = x.StateId, Text = x.Code }).ToList();
                var guardians = uow.GetRepo<IGuardianRepo>().GetAll()
                    .Select(x => new Lookup<Guid> { Value = x.DbUserId, Text = x.FullName }).ToList()
                    .Where(x => !FamilyGuardians.Any(x2 => x2.DbUserId == x.Value)).ToList();
                var students = uow.GetRepo<IStudentRepo>().GetAll()
                    .Select(x => new Lookup<Guid> { Value = x.DbUserId, Text = x.FullName }).ToList()
                    .Where(x => !FamilyStudents.Any(x2 => x2.DbUserId == x.Value)).ToList();
                States = new SelectList(states, "Value", "Text");
                Guardians = new SelectList(guardians, "Value", "Text");
                Students = new SelectList(students, "Value", "Text");
            }
        }
    }
}