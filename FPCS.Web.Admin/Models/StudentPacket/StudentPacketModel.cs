﻿using FPCS.Data.Enums;
using FPCS.Core.Extensions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace FPCS.Web.Admin.Models.StudentPacket
{
    public class StudentPacketModel
    {
        public StudentPacketModel()
        {
            Init();
        }

        public String SubjectSearchOptions { get; set; }

        public Int64 StudentPacketId { get; set; }

        public Guid StudentId { get; set; }

        public String StudentName { get; set; }

        public Grade StudentGrade { get; set; }

        public String StudentGradeStr { get; set; }

        public Guid? SponsorTeacherId { get; set; }

        [Display(Name = "Sponsor Teacher")]
        public String SponsorTeacherName { get; set; }

        public Boolean IsASDTASigned { get; set; }

        public Boolean IsPRASigned { get; set; }

        public String ILPPhilosophy { get; set; }

        public IEnumerable<StudentPacketCourseModel> StudentPacketCourseModels { get; set; }

        public Boolean IsCompletePacketCourse { get; set; }

        public List<String> ItemsNeedComplete { get; set; }

        #region Progress Chart & Individual Elective Spending Limits

        public GoalEnrollmentPercent GoalEnrollmentPercent { get; set; }

        public Decimal GoalCoreUnits { get; set; }

        public Decimal GoalElectiveUnits { get; set; }

        public Decimal GoalTotalUnits { get; set; }

        public Decimal GoalILPHrs { get; set; }

        public Decimal GoalContractHrs { get; set; }

        public Decimal AchievedEnrollmentPercent { get; set; }

        public Decimal AchievedCoreUnits { get; set; }

        public Decimal AchievedElectiveUnits { get; set; }

        public Decimal AchievedTotalUnits { get; set; }

        public Decimal AchievedILPHrs { get; set; }

        public Decimal AchievedContractHrs { get; set; }

        public Decimal IndividualBudgetBudgetLimit { get; set; }

        public Decimal IndividualBudgetAmountBudgeted { get; set; }

        public Decimal IndividualBudgetElectiveBalance { get; set; }

        public Decimal IndividualBudgetGlobalReserve { get; set; }

        public SelectList GoalEnrollmentPercents { get; set; }

        #endregion

        #region Planning Budget & Actual Spending

        public Decimal PBBeginningBalance { get; set; }

        public Decimal PBBudgetTransferDeposits { get; set; }

        public Decimal PBBudgetTransferWithdrawals { get; set; }

        public Decimal PBAvailableRemainingFunds { get; set; }

        public Decimal PBAvailableRemainingFundsTotal { get; set; }

        public Decimal ASBeginningBalance { get; set; }

        public Decimal ASBudgetTransferDeposits { get; set; }

        public Decimal ASBudgetTransferWithdrawals { get; set; }

        public Decimal ASAvailableRemainingFunds { get; set; }

        public Decimal ASAvailableRemainingFundsTotal { get; set; }

        public Boolean IsLocked { get; set; }

        #endregion

        public void Init()
        {
            #region default info for demo

            GoalContractHrs = 2;
            
            #endregion

            GoalEnrollmentPercents = GoalEnrollmentPercent.ToSelectListUsingDesc();
        }
    }
}