﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using FPCS.Data.Enums;
using FPCS.Web.Admin.Models.GoodAndService;

namespace FPCS.Web.Admin.Models.StudentPacket
{
    public class StudentPacketCourseModel
    {
        [Display(Name = "Student Packet Course Id")]
        public Int64 StudentPacketCourseId { get; set; }

        [Display(Name = "FPCS Course Id")]
        public Int64 FPCSCourseId { get; set; }

        [Display(Name = "Name of course")]
        public String FPCSCourseName { get; set; }

        [Display(Name = "Name of student packet course")]
        public String StudentPacketCourseName { get; set; }

        [Display(Name = "Teacher Id")]
        public Guid? TeacherId { get; set; }

        [Display(Name = "Teacher")]
        public String TeacherName { get; set; }

        [Display(Name = "Guardian Id")]
        public Guid? GuardianId { get; set; }

        [Display(Name = "Guardian")]
        public String GuardianName { get; set; }

        public Int32 SubjectId { get; set; }

        [Display(Name = "Subject")]
        public String Subject { get; set; }

        public Boolean SubjectIsElective { get; set; }

        [Display(Name = "Semester")]
        public Semester Semester { get; set; }

        public Guid? SponsorSignatureGuid { get; set; }

        [Display(Name = "Sponsor")]
        public String SponsorSignature { get; set; }

        public Guid? InstructorSignatureGuid { get; set; }

        [Display(Name = "Instructor")]
        public String InstructorSignature { get; set; }

        public Guid? AdminSignatureGuid { get; set; }

        [Display(Name = "Admin")]
        public String AdminSignature { get; set; }

        public Int32 AdminSignatureInt { get; set; }

        public Guid? GuardiansSignatureGuid { get; set; }

        [Display(Name = "Guardians")]
        public String GuardiansSignature { get; set; }

        public Boolean IsFullSignature { get; set; }

        [Display(Name = "Course Description Hrs")]
        public Int32 ILPHours { get; set; }

        [Display(Name = "Contract Hrs")]
        public Decimal ContractHrs { get; set; }

        public TableGoodAndServiceModel TableGoodAndService { get; set; }

        public Boolean IsInstructorSponsor { get; set; }

        public Boolean IsInstructorGuardian { get; set; }

        public StudentPacketCourseStatus Status { get; set; }

        public String CourseHelper { get; set; }
    }
}