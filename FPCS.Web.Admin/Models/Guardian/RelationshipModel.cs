﻿using FPCS.Data.Enums;
using System;

namespace FPCS.Web.Admin.Models.Guardian
{
    public class RelationshipModel
    {
        public Guid DbUserId { get; set; }

        public String FullName { get; set; }

        public FamilyRelationshipType FamilyRelationshipType { get; set; }
    }
}