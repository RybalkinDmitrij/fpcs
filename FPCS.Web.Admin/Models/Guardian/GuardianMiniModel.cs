﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FPCS.Web.Admin.Models.Guardian
{
    public class GuardianMiniModel
    {
        public Guid DbUserId { get; set; }

        [Display(Name = "Full name")]
        public String FullName { get; set; }

        [Display(Name = "Email Address")]
        public String Email { get; set; }

        public String IsLocked { get; set; }
    }
}