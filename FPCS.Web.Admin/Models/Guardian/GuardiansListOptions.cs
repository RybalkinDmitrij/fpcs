﻿using System;

using FPCS.Core.jqGrid;

namespace FPCS.Web.Admin.Models.Guardian
{
    public class GuardiansListOptions
    {
        [GridProperty(ExtensionType.All, true)]
        public Guid? DbUserId { get; set; }

        [GridProperty(ExtensionType.All, true, "LastName", FilterOperation.Contains)]
        public String FullName { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Contains)]
        public String Email { get; set; }

        [GridProperty(ExtensionType.Filter, false)]
        [GridProperty(ExtensionType.All, true)]
        public String IsLocked { get; set; }
    }
}