﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;

using FPCS.Core.Unity;
using FPCS.Core.Extensions;
using FPCS.Data;
using FPCS.Data.Repo;

namespace FPCS.Web.Admin.Models.Guardian
{
    public class GuardianEditModel
    {
        [Required]
        public Guid DbUserId { get; set; }

        [Required]
        [Display(Name = "First name")]
        public String FirstName { get; set; }

        [Required]
        [Display(Name = "Last name")]
        public String LastName { get; set; }

        [Display(Name = "MI")]
        public String MiddleInitial { get; set; }

        [Display(Name = "Employer")]
        public String Employer { get; set; }

        [Display(Name = "Active Military")]
        public Int32 IsActiveMilitaryInt { get; set; }

        public Boolean? IsActiveMilitary
        {
            get
            {
                if (IsActiveMilitaryInt == 1) return true;
                else if (IsActiveMilitaryInt == 2) return false;
                else return null;
            }
            set
            {
                if (value == true) IsActiveMilitaryInt = 1;
                else if (value == false) IsActiveMilitaryInt = 2;
                else IsActiveMilitaryInt = 0;
            }
        }

        [Display(Name = "Rank")]
        public String Rank { get; set; }

        [Display(Name = "Pager")]
        public String Pager { get; set; }

        [Display(Name = "Business Phone ")]
        public String BusinessPhone { get; set; }

        [Display(Name = "Ext.")]
        public String Ext { get; set; }

        [Display(Name = "Cell Phone")]
        public String CellPhone { get; set; }

        [EmailAddress]
        [Display(Name = "Email Address")]
        public String Email { get; set; }

        [Display(Name = "Address (if different)")]
        public String Address { get; set; }

        [Display(Name = "City (if different)")]
        public String City { get; set; }

        [Display(Name = "Country (if different)")]
        public String Country { get; set; }

        [Display(Name = "Zip (if different)")]
        public String Zip { get; set; }

        [Display(Name = "State (if different)")]
        public Int64? StateId { get; set; }

        public IEnumerable<RelationshipModel> GuardianRelationships { get; set; }

        public SelectList States { get; set; }

        public SelectList ActiveMilitaries { get; set; }

        public SelectList Relationships { get; set; }

        public void Init()
        {
            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                var states = uow.GetRepo<IStateRepo>().GetAll().Select(x => new Lookup<Int64> { Value = x.StateId, Text = x.Code }).ToList();
                States = new SelectList(states, "Value", "Text");

                var activeMilitaries = new List<Lookup<Int32>>();
                activeMilitaries.Add(new Lookup<Int32> { Value = 0, Text = "Not Military" });
                activeMilitaries.Add(new Lookup<Int32> { Value = 1, Text = "Yes" });
                activeMilitaries.Add(new Lookup<Int32> { Value = 2, Text = "No" });
                ActiveMilitaries = new SelectList(activeMilitaries, "Value", "Text");

                Relationships = FPCS.Data.Enums.FamilyRelationshipType.NotSpecified.ToSelectListUsingDescWithoutActive();
            }
        }
    }
}