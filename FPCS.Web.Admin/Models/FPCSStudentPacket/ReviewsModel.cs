﻿using FPCS.Data.Entities;
using FPCS.Web.Admin.Models.GoodAndService;
using FPCS.Web.Admin.Models.ILP;
using FPCS.Web.Admin.Models.StudentPacketCourseAlert;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FPCS.Web.Admin.Models.FPCSStudentPacket
{
    public class ReviewsModel
    {
        public ReviewsModel(Int32? studentPacketCourseId, Int32? courseId)
        {
            StudentPacketCourseId = studentPacketCourseId.GetValueOrDefault(0);
            CourseId = courseId.GetValueOrDefault(0);
            TableGoodAndServiceModel = new TableGoodAndServiceModel();
            ILPModel = new ILPModel(StudentPacketCourseId, CourseId);
            EnrollmentListModel = new EnrollmentListModel(StudentPacketCourseId);
            TableAlertModel = new TableAlertModel();
            SignModel = new SignModel();
        }

        public Boolean IsCanEditILP { get; set; }

        public Boolean IsCanEditGoodService { get; set; }

        public Int32 StudentPacketCourseId { get; set; }
        public Int32 CourseId { get; set; }

        public TableGoodAndServiceModel TableGoodAndServiceModel { get; set; }
        public ILPModel ILPModel { get; set; }
        public EnrollmentListModel EnrollmentListModel { get; set; }
        public TableAlertModel TableAlertModel { get; set; }
        public SignModel SignModel { get; set; }
    }
}