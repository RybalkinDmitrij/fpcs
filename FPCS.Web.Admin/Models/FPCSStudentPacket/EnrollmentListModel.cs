﻿using FPCS.Core.Unity;
using FPCS.Core.Extensions;
using FPCS.Data;
using FPCS.Data.Entities;
using FPCS.Data.Repo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DBE = FPCS.Data.Entities;
using System.ComponentModel.DataAnnotations;

namespace FPCS.Web.Admin.Models.FPCSStudentPacket
{
	public class EnrollmentListModel
	{
		public EnrollmentListModel()
		{
		}

		public EnrollmentListModel(Int32? studentPacketCourseId)
		{
			StudentPacketCourseId = studentPacketCourseId.GetValueOrDefault(0);

			using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
			{
				if (StudentPacketCourseId != 0)
				{
					var repoStudentPacketCourse = uow.GetRepo<IStudentPacketCourseRepo>();

					StudentPacketCourse dbStudentPacketCourse = repoStudentPacketCourse.Get(StudentPacketCourseId);

					if (dbStudentPacketCourse.FPCSCourse.Teacher != null)
					{
						InstructedBy = String.Format(@"{0}, {1}", dbStudentPacketCourse.FPCSCourse.Teacher.LastName,
							dbStudentPacketCourse.FPCSCourse.Teacher.FirstName);
						InstructedByHomePhone = dbStudentPacketCourse.FPCSCourse.Teacher.HomePhone;
						InstructedByWorkPhone = dbStudentPacketCourse.FPCSCourse.Teacher.CellPhone;
						InstructedByEmail = dbStudentPacketCourse.FPCSCourse.Teacher.Email;
					}
					else if (dbStudentPacketCourse.FPCSCourse.Guardian != null)
					{
						InstructedBy = String.Format(@"{0}, {1}", dbStudentPacketCourse.FPCSCourse.Guardian.LastName,
							dbStudentPacketCourse.FPCSCourse.Guardian.FirstName);
						InstructedByHomePhone = dbStudentPacketCourse.FPCSCourse.Guardian.BusinessPhone;
						InstructedByWorkPhone = dbStudentPacketCourse.FPCSCourse.Guardian.CellPhone;
						InstructedByEmail = dbStudentPacketCourse.FPCSCourse.Guardian.Email;
					}
					else if (dbStudentPacketCourse.FPCSCourse.Vendor != null)
					{
						InstructedBy = String.Format(@"{0}, {1}", dbStudentPacketCourse.FPCSCourse.Vendor.LastName,
							dbStudentPacketCourse.FPCSCourse.Vendor.FirstName);
						InstructedByHomePhone = dbStudentPacketCourse.FPCSCourse.Vendor.Phone;
						InstructedByWorkPhone = dbStudentPacketCourse.FPCSCourse.Vendor.Phone;
						InstructedByEmail = dbStudentPacketCourse.FPCSCourse.Vendor.Email;
					}

					StartEndDates = String.Format(@"{0} - {1}", dbStudentPacketCourse.FPCSCourse.ClassStartDate.ToString("MM/dd/yyyy"),
						dbStudentPacketCourse.FPCSCourse.ClassEndDate.ToString("MM/dd/yyyy"));

					MeetsOn = String.Empty;
					MinStudents = 0;
					MaxStudents = 0;

					List<DBE.Student> students =
						repoStudentPacketCourse.GetStudentsByFPCSCourse(dbStudentPacketCourse.FPCSCourseId,
							dbStudentPacketCourse.StudentPacket.SchoolYearId).ToList();

					NumberStudentsInClass = students.Count();

					Students = students.Select(x => new EnrolledStudentModel(x)).ToList();
				}
			}
		}

		public Int32 StudentPacketCourseId { get; set; }

		[Display(Name = "Instructed By")]
		public String InstructedBy { get; set; }

		[Display(Name = "Instructed's Home Phone")]
		public String InstructedByHomePhone { get; set; }

		[Display(Name = "Instructed's Work Phone")]
		public String InstructedByWorkPhone { get; set; }

		[Display(Name = "Instructed's Email")]
		public String InstructedByEmail { get; set; }

		[Display(Name = "Start & End Dates")]
		public String StartEndDates { get; set; }

		[Display(Name = "Meets On")]
		public String MeetsOn { get; set; }

		[Display(Name = "Min # of Students")]
		public Int32 MinStudents { get; set; }

		[Display(Name = "Max # of Students")]
		public Int32 MaxStudents { get; set; }

		[Display(Name = "# of Students in Class")]
		public Int32 NumberStudentsInClass { get; set; }

		public List<EnrolledStudentModel> Students { get; set; }
	}
}