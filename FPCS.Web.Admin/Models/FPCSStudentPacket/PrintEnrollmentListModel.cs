﻿using FPCS.Core.Unity;
using FPCS.Core.Extensions;
using FPCS.Data;
using FPCS.Data.Entities;
using FPCS.Data.Repo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DBE = FPCS.Data.Entities;
using System.ComponentModel.DataAnnotations;

namespace FPCS.Web.Admin.Models.FPCSStudentPacket
{
    public class PrintEnrollmentListModel
    {
        public PrintEnrollmentListModel()
        {
        }

        public EnrollmentListModel EnrollmentListModel { get; set; }

        public String PrintAddress { get; set; }

        public String PrintState { get; set; }

        public String PrintPhone { get; set; }

        public String PrintFax { get; set; }

        public String PrintDateNow { get; set; }
    }
}