﻿using DBE = FPCS.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FPCS.Core.Unity;
using FPCS.Data;
using FPCS.Data.Repo;
using System.ComponentModel.DataAnnotations;

namespace FPCS.Web.Admin.Models.FPCSStudentPacket
{
    public class EnrolledStudentModel
    {
        public EnrolledStudentModel()
        {
        }

        public EnrolledStudentModel(DBE.Student dbEntity)
        {
            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                StudentId = dbEntity.DbUserId.ToString();
                StudentsEnrolled = String.Format(@"{0}, {1}", dbEntity.LastName, dbEntity.FirstName);
                StudentsSponsor = dbEntity.StudentPackets != null && dbEntity.StudentPackets.FirstOrDefault() != null && dbEntity.StudentPackets.FirstOrDefault().SponsorTeacher != null ?
                    String.Format(@"{0}, {1}", dbEntity.StudentPackets.FirstOrDefault().SponsorTeacher.LastName, dbEntity.StudentPackets.FirstOrDefault().SponsorTeacher.FirstName) :
                    String.Empty;

                DBE.Family family = dbEntity.Families != null && dbEntity.Families.FirstOrDefault() != null ? dbEntity.Families.FirstOrDefault() : null;

                if (family != null)
                {
                    var guardians = family.Guardians.ToList();

                    StudentsGuardians = String.Join(" & ", guardians.Select(x => String.Format(@"{0} {1}", x.LastName, x.FirstName)));
                    StudentsGuardiansEmail = family.Email;
                    HomePhone = family.Telephone;
                }
            }
        }

        public String StudentId { get; set; }

        [Display(Name = "Student's Enrolled")]
        public String StudentsEnrolled { get; set; }

        [Display(Name = "Student's Guardians")]
        public String StudentsGuardians { get; set; }

        [Display(Name = "Student's Family Email")]
        public String StudentsGuardiansEmail { get; set; }

        [Display(Name = "Home Phone")]
        public String HomePhone { get; set; }

        [Display(Name = "Student's Sponsor")]
        public String StudentsSponsor { get; set; }
    }
}