﻿using FPCS.Data.Enums;
using FPCS.Core.Extensions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using FPCS.Data.Entities;
using FPCS.Web.Admin.Models.SessionModels;

namespace FPCS.Web.Admin.Models.FPCSStudentPacket
{
    public class SignModel
    {
        public SignModel()
        {
            Init();
        }

        public SignModel(StudentPacketCourse dbEntity, String returnHref, SessionUserModel currentUser)
        {
            StudentPacketCourseId = dbEntity.StudentPacketCourseId;

            GuardiansGuids = dbEntity.StudentPacket.Student.Families.FirstOrDefault() != null ?
                dbEntity.StudentPacket.Student.Families.FirstOrDefault().Guardians.Select(x => x.DbUserId).ToList() : new List<Guid>();
            GuardiansSign = dbEntity.GuardianSignature;
            GuardiansComment = dbEntity.GuardianComment;
            SponsorGuid = dbEntity.StudentPacket.SponsorTeacherId;
            SponsorSign = dbEntity.SponsorSignature;
            SponsorComment = dbEntity.SponsorComment;
            InstructorGuid = dbEntity.FPCSCourse.TeacherId.HasValue ?
                                dbEntity.FPCSCourse.TeacherId.Value :
                                    dbEntity.FPCSCourse.GuardianId.HasValue ?
                                        dbEntity.FPCSCourse.GuardianId.Value : 
                                        Guid.Empty;
            InstructorSign = dbEntity.InstructorSignature;
            InstructorComment = dbEntity.InstructorComment;
            AdminSign = dbEntity.AdminSignature;
            AdminComment = dbEntity.AdminComment;

            IsCanChangeGuardiansSign = GuardiansGuids.Contains(currentUser.UserId);
            IsCanChangeSponsorSign = SponsorGuid == currentUser.UserId;
            IsCanChangeInstructorSign = InstructorGuid == currentUser.UserId;
            IsCanChangeAdminSign = currentUser.Role == Role.Admin;

            ReturnHref = returnHref;

            Init();
        }

        public Int64 StudentPacketCourseId { get; set; }

        public Boolean IsCanChangeGuardiansSign { get; private set; }

        public List<Guid> GuardiansGuids { get; set; }

        [Display(Name = "Guardians")]
        public Sign? GuardiansSign { get; set; }

        [Display(Name = "Guardians comment")]
        public String GuardiansComment { get; set; }

        public Boolean IsCanChangeSponsorSign { get; private set; }

        public Guid? SponsorGuid { get; set; }

        [Display(Name = "Sponsor")]
        public Sign? SponsorSign { get; set; }

        [Display(Name = "Sponsor comment")]
        public String SponsorComment { get; set; }

        public Boolean IsCanChangeInstructorSign { get; private set; }

        public Guid? InstructorGuid { get; set; }

        [Display(Name = "Instructor")]
        public Sign? InstructorSign { get; set; }

        [Display(Name = "Instructor comment")]
        public String InstructorComment { get; set; }

        public Boolean IsCanChangeAdminSign { get; private set; }

        [Display(Name = "Admin")]
        public Sign? AdminSign { get; set; }

        [Display(Name = "Admin comment")]
        public String AdminComment { get; set; }

        public SelectList Signs { get; set; }

        public Boolean IsInstructorSponsor
        {
            get
            {
                return InstructorGuid.HasValue && SponsorGuid.HasValue && InstructorGuid == SponsorGuid;
            }
        }

        public Boolean IsInstructorGuardian
        {
            get
            {
                return InstructorGuid.HasValue && GuardiansGuids != null && GuardiansGuids.Contains(InstructorGuid.Value);
            }
        }

        public String ReturnHref { get; set; }

        public void Init()
        {
            Signs = Sign.MustAmend.ToSelectListUsingDescWithoutActive();
        }
    }
}