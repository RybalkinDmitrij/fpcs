﻿using FPCS.Data.Enums;
using FPCS.Core.Extensions;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using FPCS.Core.Unity;
using FPCS.Data;
using FPCS.Data.Repo;
using FDE = FPCS.Data.Entities;

namespace FPCS.Web.Admin.Models.Teacher
{
    public class TeacherEditModel
    {
        public TeacherEditModel() 
        {
            Init();
        }

        public TeacherEditModel(FDE.Teacher dbEntity)
        {
            DbUserId = dbEntity.DbUserId;
            FirstName = dbEntity.FirstName;
            LastName = dbEntity.LastName;
            MiddleInitial = dbEntity.MiddleInitial;
            Email = dbEntity.Email;
            TitlePerson = dbEntity.Title;
            DateBirth = dbEntity.DateBirth;
            IsActive = dbEntity.IsActive;
            MailingAddress = dbEntity.MailingAddress;
            City = dbEntity.City;
            StateId = dbEntity.StateId;
            ZipCode = dbEntity.ZipCode;
            HomePhone = dbEntity.HomePhone;
            BusPhone = dbEntity.BusPhone;
            Ext = dbEntity.Ext;
            CellPhone = dbEntity.CellPhone;
            MastersDegree = dbEntity.MastersDegree;
            SSN = dbEntity.SSN;
            //EMailAddress = dbEntity.EMailAddress;
            SecondEMailAddress = dbEntity.SecondEMailAddress;
            DistrictCode = dbEntity.DistrictCode;
            IsBenefitPaidASDSchool = dbEntity.IsBenefitPaidASDSchool;
            ASDFTE = dbEntity.ASDFTE;
            IsNotIncludeBenefits = dbEntity.IsNotIncludeBenefits;
            FTCSFTE = dbEntity.FTCSFTE;
            NameSchool = dbEntity.NameSchool;
            IsLeaveASD = dbEntity.IsLeaveASD;
            IsSubtitleTeacher = dbEntity.IsSubtitleTeacher;
            IsOtherASDEmployee = dbEntity.IsOtherASDEmployee;
            IsOnASDEligibleToHire = dbEntity.IsOnASDEligibleToHire;
            IsRetiredASDTeacher = dbEntity.IsRetiredASDTeacher;
            FPCSFTE = dbEntity.FPCSFTE;
            IsGroupInstruction = dbEntity.IsGroupInstruction;
            IsIndividualInstruction = dbEntity.IsIndividualInstruction;
            YearsTeachingExperience = dbEntity.YearsTeachingExperience;
            TeachingCertificateExpirationDate = dbEntity.TeachingCertificateExpirationDate;
            TeachingSalaryPlacement = dbEntity.TeachingSalaryPlacement;
            IsAlaskaCertificationK8 = dbEntity.IsAlaskaCertificationK8;
            IsAlaskaCertificationK12 = dbEntity.IsAlaskaCertificationK12;
            IsAlaskaCertificationSpecialEducation = dbEntity.IsAlaskaCertificationSpecialEducation;
            IsAlaskaCertificationSecondary = dbEntity.IsAlaskaCertificationSecondary;
            AlaskaCertificationSubjectGrades = dbEntity.AlaskaCertificationSubjectGrades;
            IsInMyClassroom = dbEntity.IsInMyClassroom;
            IsAtMyHome = dbEntity.IsAtMyHome;
            IsAsStudentsHome = dbEntity.IsAsStudentsHome;
            IsAtFPCSClassroom = dbEntity.IsAtFPCSClassroom;
            IsOtherAvailableTeach = dbEntity.IsOtherAvailableTeach;
            OtherAvailableTeach = dbEntity.OtherAvailableTeach;
            IsWeekdays = dbEntity.IsWeekdays;
            IsWeekdayAfternoons = dbEntity.IsWeekdayAfternoons;
            IsWeekdayEvenings = dbEntity.IsWeekdayEvenings;
            IsWeekends = dbEntity.IsWeekends;
            IsSummers = dbEntity.IsSummers;
            FlatRateHour = dbEntity.FlatRateHour;
            BasePayHour = dbEntity.BasePayHour;
            PayHourwBenefits = dbEntity.PayHourwBenefits;
            PayType = dbEntity.PayType;
            PerDeimRate = dbEntity.PerDeimRate;

            Init();
        }

        [Required]
        public Guid DbUserId { get; set; }

        [Required]
        [Display(Name = "First name")]
        public String FirstName { get; set; }

        [Required]
        [Display(Name = "Last name")]
        public String LastName { get; set; }

        [Display(Name = "MI")]
        public String MiddleInitial { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email Address")]
        public String Email { get; set; }

        [Required]
        [Display(Name = "Title")]
        public TitlePerson TitlePerson { get; set; }

        [Required]
        [Display(Name = "Date of Birth")]
        public DateTime DateBirth { get; set; }

        [Required]
        [Display(Name = "Is Active")]
        public Boolean IsActive { get; set; }

        [Required]
        [Display(Name = "Mailing Address")]
        public String MailingAddress { get; set; }

        [Required]
        [Display(Name = "City")]
        public String City { get; set; }

        [Required]
        [Display(Name = "State")]
        public Int64 StateId { get; set; }

        [Required]
        [Display(Name = "Zip Code")]
        public String ZipCode { get; set; }

        [Display(Name = "Home Phone")]
        public String HomePhone { get; set; }

        [Display(Name = "Bus. Phone")]
        public String BusPhone { get; set; }

        [Required]
        [Display(Name = "Ext")]
        public Int32 Ext { get; set; }

        [Display(Name = "Cell Phone")]
        public String CellPhone { get; set; }

        [Required]
        [Display(Name = "Masters Degree")]
        public Boolean MastersDegree { get; set; }

        [Required]
        [Display(Name = "SSN")]
        public String SSN { get; set; }

        //[Required]
        //[Display(Name = "Email Address")]
        //public String EMailAddress { get; set; }

        [Display(Name = "Second Email Address")]
        public String SecondEMailAddress { get; set; }

        [Display(Name = "District Code")]
        public String DistrictCode { get; set; }

        [Required]
        [Display(Name = "Benefit paid by other ASD school")]
        public Boolean IsBenefitPaidASDSchool { get; set; }

        [Required]
        [Display(Name = "% (ASD FTE)")]
        public Int32 ASDFTE { get; set; }

        [Required]
        [Display(Name = "Check only if less than 50% (Will not include benefits in calculations)")]
        public Boolean IsNotIncludeBenefits { get; set; }

        [Required]
        [Display(Name = "% (FTCS FTE)")]
        public Int32 FTCSFTE { get; set; }

        [Display(Name = "Name of School")]
        public String NameSchool { get; set; }

        [Required]
        [Display(Name = "On leave from ASD")]
        public Boolean IsLeaveASD { get; set; }

        [Required]
        [Display(Name = "Substitute Teacher")]
        public Boolean IsSubtitleTeacher { get; set; }

        [Required]
        [Display(Name = "Other ASD employee (not teaching)")]
        public Boolean IsOtherASDEmployee { get; set; }

        [Required]
        [Display(Name = "On ASD eligible-to-hire list")]
        public Boolean IsOnASDEligibleToHire { get; set; }

        [Required]
        [Display(Name = "Retired ASD Teacher")]
        public Boolean IsRetiredASDTeacher { get; set; }

        [Required]
        [Display(Name = "FPCS FTE Percentage")]
        public Int32 FPCSFTE { get; set; }

        [Required]
        [Display(Name = "Group Instruction")]
        public Boolean IsGroupInstruction { get; set; }

        [Required]
        [Display(Name = "Individual Instruction (one on one)")]
        public Boolean IsIndividualInstruction { get; set; }

        [Required]
        [Display(Name = "Years of Teaching Experience")]
        public Int32 YearsTeachingExperience { get; set; }

        [Required]
        [Display(Name = "Teaching Certificate Expiration Date")]
        public DateTime TeachingCertificateExpirationDate { get; set; }

        [Required]
        [Display(Name = "Teaching Salary Placement")]
        public String TeachingSalaryPlacement { get; set; }

        [Required]
        [Display(Name = "K-8")]
        public Boolean IsAlaskaCertificationK8 { get; set; }

        [Required]
        [Display(Name = "K-12")]
        public Boolean IsAlaskaCertificationK12 { get; set; }

        [Required]
        [Display(Name = "Special Education")]
        public Boolean IsAlaskaCertificationSpecialEducation { get; set; }

        [Required]
        [Display(Name = "Secondary (list subject and grades)")]
        public Boolean IsAlaskaCertificationSecondary { get; set; }

        [Display(Name = "")]
        public String AlaskaCertificationSubjectGrades { get; set; }

        [Required]
        [Display(Name = "in my classroom")]
        public Boolean IsInMyClassroom { get; set; }

        [Required]
        [Display(Name = "at my home")]
        public Boolean IsAtMyHome { get; set; }

        [Required]
        [Display(Name = "at student's home")]
        public Boolean IsAsStudentsHome { get; set; }

        [Required]
        [Display(Name = "at FPCS classroom")]
        public Boolean IsAtFPCSClassroom { get; set; }

        [Required]
        [Display(Name = "Other")]
        public Boolean IsOtherAvailableTeach { get; set; }

        [Display(Name = "")]
        public String OtherAvailableTeach { get; set; }

        [Required]
        [Display(Name = "weekdays")]
        public Boolean IsWeekdays { get; set; }

        [Required]
        [Display(Name = "weekday afternoons")]
        public Boolean IsWeekdayAfternoons { get; set; }

        [Required]
        [Display(Name = "weekday evenings")]
        public Boolean IsWeekdayEvenings { get; set; }

        [Required]
        [Display(Name = "weekends")]
        public Boolean IsWeekends { get; set; }

        [Required]
        [Display(Name = "summers")]
        public Boolean IsSummers { get; set; }

        [Required]
        [Display(Name = "Flat Rate per Hour")]
        public Decimal FlatRateHour { get; set; }

        [Required]
        [Display(Name = "Base Pay per Hour")]
        public Decimal BasePayHour { get; set; }

        [Required]
        [Display(Name = "Pay per Hour w/Benefits")]
        public Decimal PayHourwBenefits { get; set; }

        [Required]
        [Display(Name = "Pay Type")]
        public PayType PayType { get; set; }

        [Required]
        [Display(Name = "Per Deim Rate")]
        public Decimal PerDeimRate { get; set; }

        public SelectList Titles { get; set; }

        public SelectList States { get; set; }

        public SelectList PayTypes { get; set; }

        public void Init()
        {
            Titles = TitlePerson.ToSelectListUsingDesc();
            PayTypes = PayType.ToSelectListUsingDesc();

            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                var states = uow.GetRepo<IStateRepo>()
                                    .GetAll()
                                    .Select(x => new Lookup<Int64> { Value = x.StateId, Text = x.Code })
                                    .ToList();

                States = new SelectList(states, "Value", "Text");
            }
        }
    }
}