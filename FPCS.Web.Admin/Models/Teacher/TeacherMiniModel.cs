﻿using System;

namespace FPCS.Web.Admin.Models.Teacher
{
    public class TeacherMiniModel
    {
        public Guid DbUserId { get; set; }

        public String FullName { get; set; }

        public String Email { get; set; }

        public Boolean IsActive { get; set; }
    }
}