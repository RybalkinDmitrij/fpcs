﻿using FPCS.Core.jqGrid;
using System;

namespace FPCS.Web.Admin.Models.Teacher
{
    public class TeachersListOptions
    {
        [GridProperty(ExtensionType.All, true)]
        public Guid? DbUserId { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Contains)]
        public String FullName { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Contains)]
        public String Email { get; set; }
    }
}