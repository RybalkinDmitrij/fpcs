﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FPCS.Web.Admin.Models.SignedCourse
{
    public class SignedCourseMiniModel
    {
        public Guid StudentGuid { get; set; }

        public Int64 StudentPacketId { get; set; }

        public String StudentName { get; set; }

        public String Grade { get; set; }

        public String HomePhone { get; set; }

        public Int32 GNS { get; set; }

        public Int32 SNS { get; set; }

        public Int32 INS { get; set; }

        public Int32 ANS { get; set; }

        public Int32 AMA { get; set; }

        public Int32 SA { get; set; }

        public Int32 GA { get; set; }

        public String EmailFamily { get; set; }
    }
}