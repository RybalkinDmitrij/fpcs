﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;

using FPCS.Core.Unity;
using FPCS.Data;
using FPCS.Data.Repo;

namespace FPCS.Web.Admin.Models.Subject
{
    public class SubjectCreateModel
    {
        [Required]
        [Display(Name = "Name")]
        public String Name { get; set; }

        [Display(Name = "Description")]
        public String Description { get; set; }
        
        [Required]
        [Display(Name = "Subject is elective")]
        public Boolean IsElective { get; set; }

        public void Init()
        {
        }
    }
}