﻿using System;

namespace FPCS.Web.Admin.Models.Subject
{
    public class SubjectMiniModel
    {
        public Int32 SubjectId { get; set; }

        public String Name { get; set; }

        public String IsElective { get; set; }
    }
}