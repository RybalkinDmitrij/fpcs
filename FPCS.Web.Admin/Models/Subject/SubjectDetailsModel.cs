﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FPCS.Web.Admin.Models.Subject
{
    public class SubjectDetailsModel
    {
        [Display(Name = "SubjectId")]
        public Int32 SubjectId { get; set; }

        [Display(Name = "Name")]
        public String Name { get; set; }

        [Display(Name = "Description")]
        public String Description { get; set; }

        [Display(Name = "Subject is elective")]
        public Boolean IsElective { get; set; }
    }
}