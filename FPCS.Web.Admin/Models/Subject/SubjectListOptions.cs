﻿using System;

using FPCS.Core.jqGrid;

namespace FPCS.Web.Admin.Models.Subject
{
    public class SubjectListOptions
    {
        [GridProperty(ExtensionType.All, true)]
        public Int32? SubjectId { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Contains)]
        public String Name { get; set; }

        [GridProperty(ExtensionType.Filter, false)]
        [GridProperty(ExtensionType.All, true)]
        public String IsElective { get; set; }
    }
}