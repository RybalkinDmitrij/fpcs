﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

using FPCS.Core.Unity;
using FPCS.Core.Extensions;
using FPCS.Data;
using FPCS.Data.Repo;
using FPCS.Data.Enums;

namespace FPCS.Web.Admin.Models.StudentPacket2
{
    public class SponsorSignatureModel
    {
        [Required]
        [Display(Name = "Student Packet Id")]
        public Int64 StudentPacketId { get; set; }

        [Required]
        [Display(Name = "Teacher Id")]
        public Guid? TeacherId { get; set; }

        [Display(Name = "Sponsor Signature")]
        public Sign? Signature { get; set; }

        public SelectList Signs { get; set; }

        public void Init()
        {
            Signs = Sign.MustAmend.ToSelectListUsingDescWithNull();
        }
    }
}