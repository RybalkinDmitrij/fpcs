﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

using FPCS.Core.Unity;
using FPCS.Core.Extensions;
using FPCS.Data;
using FPCS.Data.Repo;
using FPCS.Data.Enums;

namespace FPCS.Web.Admin.Models.StudentPacket2
{
    public class AmpMathModel
    {
        [Required]
        [Display(Name = "Student Packet Id")]
        public Int64 StudentPacketId { get; set; }

        [Required]
        [Display(Name = "Student Id")]
        public Guid StudentId { get; set; }

        [Display(Name = "Amp Math")]
        public AmpMath AmpMath { get; set; }

        public SelectList AmpMaths { get; set; }

        public void Init()
        {
            AmpMaths = AmpMath.Advanced.ToSelectListUsingDesc();
        }
    }
}