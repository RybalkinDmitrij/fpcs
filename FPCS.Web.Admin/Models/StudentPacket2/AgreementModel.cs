﻿using FPCS.Core.Unity;
using FPCS.Data;
using DEB = FPCS.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FPCS.Data.Repo;

namespace FPCS.Web.Admin.Models.StudentPacket2
{
    public class AgreementModel
    {
        public Guid StudentId { get; set; }

        public String StudentName {get;set;}

        public Boolean IsASDTASigned { get; set; }

        public Boolean IsPRASigned { get; set; }

        public String ASDTAGuardianSigner { get; set; }

        public String PRAGuardianSigner { get; set; }

        public List<AgreementModel> Students { get; set; }

        public void Init(Int64 studentPacketId)
        {
            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                DEB.StudentPacket packet = uow.GetRepo<IStudentPacketRepo>().Get(studentPacketId);

                if (packet == null) { throw new Exception("Student packet not found"); }

                StudentId = packet.StudentId;
                StudentName = String.Format(@"{0}, {1}", packet.Student.LastName, packet.Student.FirstName);
                IsASDTASigned = packet.IsASDTASigned;
                IsPRASigned = packet.IsPRASigned;
                ASDTAGuardianSigner = packet.ASDTAGuardian != null ? String.Format(@"{0}, {1}", packet.ASDTAGuardian.LastName, packet.ASDTAGuardian.FirstName) : String.Empty;
                PRAGuardianSigner = packet.PRAGuardian != null ? String.Format(@"{0}, {1}", packet.PRAGuardian.LastName, packet.PRAGuardian.FirstName) : String.Empty;

                Int64 familyId = 0;
                if (packet.Student.Families.FirstOrDefault() == null) { return; }

                familyId = packet.Student.Families.FirstOrDefault().FamilyId;

                var studentIds = uow.GetRepo<IStudentRepo>()
                                    .GetByFamily(familyId, packet.SchoolYearId)
                                    .Select(x => x.DbUserId)
                                    .ToList();

                Students = uow.GetRepo<IStudentPacketRepo>()
                              .GetAll()
                              .Where(x => studentIds.Contains(x.StudentId))
                              .Select(x => new AgreementModel()
                              {
                                  StudentId = x.StudentId,
                                  StudentName = x.Student.LastName + ", " + x.Student.FirstName,
                                  IsASDTASigned = x.IsASDTASigned,
                                  IsPRASigned = x.IsPRASigned,
                                  ASDTAGuardianSigner = x.ASDTAGuardian != null ?
                                                          x.ASDTAGuardian.LastName + ", " + x.ASDTAGuardian.FirstName :
                                                          String.Empty,
                                  PRAGuardianSigner = x.PRAGuardian != null ?
                                                          x.PRAGuardian.LastName + ", " + x.PRAGuardian.FirstName :
                                                          String.Empty
                              })
                              .ToList();
            }
        }
    }
}