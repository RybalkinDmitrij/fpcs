﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

using FPCS.Core.Unity;
using FPCS.Data;
using FPCS.Data.Repo;

namespace FPCS.Web.Admin.Models.StudentPacket2
{
    public class SponsorTeacherModel
    {
        [Required]
        [Display(Name = "Student Packet Id")]
        public Int64 StudentPacketId { get; set; }

        [Required]
        [Display(Name = "Teacher Id")]
        public Guid? TeacherId { get; set; }

        [Display(Name = "Sponsor Teacher")]
        public String Name { get; set; }

        public SelectList Teachers { get; set; }

        public void Init()
        {
            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                var teachers = uow.GetRepo<ITeacherRepo>().GetAll().Select(x => new Lookup<Guid> { Value = x.DbUserId, Text = x.FullName }).ToList();
                Teachers = new SelectList(teachers, "Value", "Text");
            }
        }
    }
}