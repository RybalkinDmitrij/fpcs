﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FPCS.Web.Admin.Models.Student.Abstract;

namespace FPCS.Web.Admin.Models.StudentPacket2
{
    public class ReimbursementModel: AbstractStudent
    {
        public Int64 GoodServiceId { get; set; }
        
        public String Item { get; set; }
        
        public Decimal BudgetedAmount { get; set; }

        public Decimal RequestedAmount { get; set; }

        public String Course { get; set; }

        public String StudentName { get; set; }

        public String ObjCD { get; set; }
    }
}