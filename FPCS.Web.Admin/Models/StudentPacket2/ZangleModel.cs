﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FPCS.Web.Admin.Models.StudentPacket2
{
    public class ZangleModel
    {
        public Guid StudentId { get; set; }

        public Int64 StudentPacketId { get; set; }

        [Display(Name = "Zangle ID")]
        public String Name { get; set; }

        public void Init()
        {
        }
    }
}