﻿using FPCS.Data.Enums;
using FPCS.Core.Extensions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using FPCS.Web.Admin.Models.Student.Abstract;

namespace FPCS.Web.Admin.Models.StudentPacket2
{
    public class StudentPacketModel: AbstractStudent
    {
        public StudentPacketModel()
        {
            Init();
        }

        public Semester? Semester { get; set; }

        public Guid CurrentUserId { get; set; }
        
        public String SubjectSearchOptions { get; set; }

        public Int64 StudentPacketId { get; set; }

        public Guid StudentId { get; set; }

        public String StudentName { get; set; }
        
        public Int64 FamilyId { get; set; }

        public String FamilyName { get; set; }

        public String FamilyPhone { get; set; }

        public String ZangleId { get; set; }

        public String StateId { get; set; }

        public AmpEla AmpEla { get; set; }

        public String AmpElaComment { get; set; }

        public AmpMath AmpMath { get; set; }

        public String AmpMathComment { get; set; }

        public String A504 { get; set; }

        public String SWD { get; set; }

        public Grade StudentGrade { get; set; }

        public String StudentGradeStr { get; set; }

        public Guid? SponsorTeacherId { get; set; }

        [Display(Name = "Sponsor Teacher")]
        public String SponsorTeacherName { get; set; }

        public Sign? SponsorSignatureSem1 { get; set; }
	  public DateTime? DateSponsorSignatureSem1 { get; set; }
	  public String DateSponsorSignatureSem1Str {
		  get
		  {
			  return this.DateSponsorSignatureSem1.HasValue
				  ? this.DateSponsorSignatureSem1.Value.ToString("MM/dd/yyyy")
				  : String.Empty;
		  }
	  }

        public Sign? GuardianSignatureSem1 { get; set; }
	  public DateTime? DateGuardianSignatureSem1 { get; set; }
	  public String DateGuardianSignatureSem1Str
	  {
		  get
		  {
			  return this.DateGuardianSignatureSem1.HasValue
				  ? this.DateGuardianSignatureSem1.Value.ToString("MM/dd/yyyy")
				  : String.Empty;
		  }
	  }

        public Sign? SponsorSignatureSem2 { get; set; }
	  public DateTime? DateSponsorSignatureSem2 { get; set; }
	  public String DateSponsorSignatureSem2Str
	  {
		  get
		  {
			  return this.DateSponsorSignatureSem2.HasValue
				  ? this.DateSponsorSignatureSem2.Value.ToString("MM/dd/yyyy")
				  : String.Empty;
		  }
	  }

        public Sign? GuardianSignatureSem2 { get; set; }
	  public DateTime? DateGuardianSignatureSem2 { get; set; }
	  public String DateGuardianSignatureSem2Str
	  {
		  get
		  {
			  return this.DateGuardianSignatureSem2.HasValue
				  ? this.DateGuardianSignatureSem2.Value.ToString("MM/dd/yyyy")
				  : String.Empty;
		  }
	  }

        public Sign? SponsorSignatureSummer { get; set; }
	  public DateTime? DateSponsorSignatureSummer { get; set; }
	  public String DateSponsorSignatureSummerStr
	  {
		  get
		  {
			  return this.DateSponsorSignatureSummer.HasValue
				  ? this.DateSponsorSignatureSummer.Value.ToString("MM/dd/yyyy")
				  : String.Empty;
		  }
	  }

        public Sign? GuardianSignatureSummer { get; set; }
	  public DateTime? DateGuardianSignatureSummer { get; set; }
	  public String DateGuardianSignatureSummerStr
	  {
		  get
		  {
			  return this.DateGuardianSignatureSummer.HasValue
				  ? this.DateGuardianSignatureSummer.Value.ToString("MM/dd/yyyy")
				  : String.Empty;
		  }
	  }

        public Boolean IsASDTASigned { get; set; }

        public Boolean IsPRASigned { get; set; }

        public String ILPPhilosophy { get; set; }

        public Decimal Sponsorship { get; set; }

        public Decimal SponsorshipOut { get; set; }

        public IEnumerable<StudentPacketCourseModel> StudentPacketCourseModels { get; set; }

        public Boolean IsCompletePacketCourse { get; set; }

        public List<String> ItemsNeedComplete { get; set; }

        public String JsonSponsorTeachers { get; set; }

        public String JsonSponsorSignatures { get; set; }

        public String JsonAmpElas { get; set; }

        public String JsonAmpMaths { get; set; }

        #region Progress Chart & Individual Elective Spending Limits

        public GoalEnrollmentPercent GoalEnrollmentPercent { get; set; }

        public Decimal GoalCoreUnits { get; set; }

        public Decimal GoalElectiveUnits { get; set; }

        public Decimal GoalTotalUnits { get; set; }

        public Decimal GoalILPHrs { get; set; }

        public Decimal GoalContractHrs { get; set; }

        public Decimal AchievedEnrollmentPercent { get; set; }

        public Decimal AchievedCoreUnits { get; set; }

        public Decimal AchievedElectiveUnits { get; set; }

        public Decimal AchievedTotalUnits { get; set; }

        public Decimal AchievedILPHrs { get; set; }

        public Decimal AchievedContractHrs { get; set; }

        public Decimal IndividualBudgetBudgetLimit { get; set; }

        public Decimal IndividualBudgetAmountBudgeted { get; set; }

        public Decimal IndividualBudgetElectiveBalance { get; set; }

        public Decimal IndividualBudgetGlobalReserve { get; set; }

        public SelectList GoalEnrollmentPercents { get; set; }

        #endregion

        #region Planning Budget & Actual Spending

        public Decimal PBBeginningBalance { get; set; }

        public Decimal PBBudgetTransferDeposits { get; set; }

        public Decimal PBBudgetTransferWithdrawals { get; set; }

        public Decimal PBAvailableRemainingFunds { get; set; }

        public Decimal PBAvailableRemainingFundsTotal { get; set; }

        public Decimal ASBeginningBalance { get; set; }

        public Decimal ASBudgetTransferDeposits { get; set; }

        public Decimal ASBudgetTransferWithdrawals { get; set; }

        public Decimal ASAvailableRemainingFunds { get; set; }

        public Decimal ASAvailableRemainingFundsTotal { get; set; }

        public Boolean IsLocked { get; set; }

        #endregion

        public String FamilyEmail { get; set; }

        public String SponsorTeacherEmail { get; set; }

        public void Init()
        {
            #region default info for demo

            GoalContractHrs = 2;
            
            #endregion

            GoalEnrollmentPercents = GoalEnrollmentPercent.ToSelectListUsingDesc();
        }
    }
}