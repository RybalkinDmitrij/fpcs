﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using FPCS.Data.Enums;
using FPCS.Web.Admin.Models.GoodAndService;

namespace FPCS.Web.Admin.Models.StudentPacket2
{
	public class StudentPacketCourseModel
	{
		[Display(Name = "Student Packet Course Id")]
		public Int64 StudentPacketCourseId { get; set; }

		[Display(Name = "FPCS Course Id")]
		public Int64 FPCSCourseId { get; set; }

		[Display(Name = "Name of course")]
		public String FPCSCourseName { get; set; }

		[Display(Name = "Course description")]
		public String ILPName { get; set; }

		[Display(Name = "Course description")]
		public String ASDCourseDescription { get; set; }

		[Display(Name = "Name of student packet course")]
		public String StudentPacketCourseName { get; set; }

		[Display(Name = "Teacher Id")]
		public Guid? TeacherId { get; set; }

		[Display(Name = "Teacher")]
		public String TeacherName { get; set; }

		[Display(Name = "Guardian Id")]
		public Guid? GuardianId { get; set; }

		[Display(Name = "Guardian")]
		public String GuardianName { get; set; }

		[Display(Name = "Vendor Id")]
		public Int32? VendorId { get; set; }

		[Display(Name = "Vendor")]
		public String VendorName { get; set; }

		public Int32 SubjectId { get; set; }

		[Display(Name = "Subject")]
		public String Subject { get; set; }

		public Boolean SubjectIsElective { get; set; }

		[Display(Name = "Semester")]
		public Semester Semester { get; set; }

		[Display(Name = "Repeated")]
		public Repeated Repeat { get; set; }

		[Display(Name = "Contract Hrs")]
		public Decimal ContractHrs { get; set; }

		[Display(Name = "Grade Unit")]
		public Decimal GradeCredit { get; set; }

		public String Qtr11 { get; set; }

		public String Qtr12 { get; set; }

		public String Qtr13 { get; set; }

		public String Q1 { get; set; }

		public String Q1Comment { get; set; }

		public Boolean Q1IsLock { get; set; }

		public String Qtr21 { get; set; }

		public String Qtr22 { get; set; }

		public String Qtr23 { get; set; }

		public String Q2 { get; set; }

		public String Q2Comment { get; set; }

		public Boolean Q2IsLock { get; set; }

		public String Sem1 { get; set; }

		public Decimal? Sem1Credit { get; set; }

		public String Sem1Comment { get; set; }

		public Boolean Sem1IsLock { get; set; }

		public String Qtr31 { get; set; }

		public String Qtr32 { get; set; }

		public String Qtr33 { get; set; }

		public String Q3 { get; set; }

		public String Q3Comment { get; set; }

		public Boolean Q3IsLock { get; set; }

		public String Qtr41 { get; set; }

		public String Qtr42 { get; set; }

		public String Qtr43 { get; set; }

		public String Q4 { get; set; }

		public String Q4Comment { get; set; }

		public Boolean Q4IsLock { get; set; }

		public String Sem2 { get; set; }

		public Decimal? Sem2Credit { get; set; }

		public String Sem2Comment { get; set; }

		public Boolean Sem2IsLock { get; set; }

		public String Qtr51 { get; set; }

		public String Qtr52 { get; set; }

		public String Qtr53 { get; set; }

		public String Q5 { get; set; }

		public String Q5Comment { get; set; }

		public Boolean Q5IsLock { get; set; }

		public String Qtr61 { get; set; }

		public String Qtr62 { get; set; }

		public String Qtr63 { get; set; }

		public String Q6 { get; set; }

		public String Q6Comment { get; set; }

		public Boolean Q6IsLock { get; set; }

		public String Sem3 { get; set; }

		public Decimal? Sem3Credit { get; set; }

		public String Sem3Comment { get; set; }

		public Boolean Sem3IsLock { get; set; }

		public String IsInSystem { get; set; }

		public TableGoodAndServiceModel TableGoodAndService { get; set; }
	}
}