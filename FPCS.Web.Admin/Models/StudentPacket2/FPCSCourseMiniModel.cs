﻿using System;

using FPCS.Data.Enums;
using FPCS.Core.Extensions;

namespace FPCS.Web.Admin.Models.StudentPacket2
{
    public class FPCSCourseMiniModel
    {
        public Int64? FPCSCourseId { get; set; }

        public String Name { get; set; }

        public String Teacher { get; set; }

        public String Guardian { get; set; }

        public String Vendor { get; set; }

        public String Subject { get; set; }

        public String Cost { get; set; }

        public Semester Semester { get; set; }

        public String SemesterStr { get { return Semester.GetDescription(); } }
    }
}