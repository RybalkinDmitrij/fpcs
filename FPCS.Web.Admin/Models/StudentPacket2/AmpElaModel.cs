﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

using FPCS.Core.Unity;
using FPCS.Core.Extensions;
using FPCS.Data;
using FPCS.Data.Repo;
using FPCS.Data.Enums;

namespace FPCS.Web.Admin.Models.StudentPacket2
{
    public class AmpElaModel
    {
        [Required]
        [Display(Name = "Student Packet Id")]
        public Int64 StudentPacketId { get; set; }

        [Required]
        [Display(Name = "Student Id")]
        public Guid StudentId { get; set; }

        [Display(Name = "Amp ELA")]
        public AmpEla AmpEla { get; set; }

        public SelectList AmpElas { get; set; }

        public void Init()
        {
            AmpElas = AmpEla.Advanced.ToSelectListUsingDesc();
        }
    }
}