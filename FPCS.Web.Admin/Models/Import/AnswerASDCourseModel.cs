﻿using ImpState = FPCS.Import.State;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FPCS.Web.Admin.Models.Import
{
    public class AnswerASDCourseModel
    {
        public AnswerASDCourseModel() { }
        
        public AnswerASDCourseModel(String message, String title, Int32 countInsert, Int32 countUpdate, Int32 countDelete)
        {
            Message = message;
            Title = title;
            CountInsert = countInsert;
            CountUpdate = countUpdate;
            CountDelete = countDelete;
        }

        public String Message { get; set; }
        public String Title { get; set; }
        public Int32 CountInsert { get; set; }
        public Int32 CountUpdate { get; set; }
        public Int32 CountDelete { get; set; }
    }
}