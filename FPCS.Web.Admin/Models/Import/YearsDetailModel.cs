﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FPCS.Web.Admin.Models.Import
{
    public class YearsDetailModel
    {
        public YearsDetailModel() { }

        public YearsDetailModel(String message, String title, Int32 countStudent, Int32 countTeacher, Int32 countGuardian, Int32 countFamily, Int32 countASDCourse, Int32 countVendor)
        {
            Message = message;
            Title = title;
            CountStudent = countStudent;
            CountTeacher = countTeacher;
            CountGuardian = countGuardian;
            CountFamily = countFamily;
            CountASDCourse = countASDCourse;
            CountVendor = countVendor;
        }

        public String Message { get; set; }
        public String Title { get; set; }
        public Int32 CountStudent { get; set; }
        public Int32 CountTeacher { get; set; }
        public Int32 CountGuardian { get; set; }
        public Int32 CountFamily { get; set; }
        public Int32 CountASDCourse { get; set; }
        public Int32 CountVendor { get; set; }
    }
}