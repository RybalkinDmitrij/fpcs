﻿using FPCS.Core.Unity;
using FPCS.Data;
using FPCS.Data.Repo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FPCS.Web.Admin.Models.Import
{
    public class ImportDetailsModel
    {
        public Int32 FromSchoolYearId { get; set; }

        public Int32 ToSchoolYearId { get; set; }

        public SelectList SchoolYears { get; set; }

        public void Init()
        {
            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                var schoolYears = uow.GetRepo<ISchoolYearRepo>().GetAll().Select(x => new Lookup<Int32> { Value = x.SchoolYearId, Text = x.Name }).ToList();
                SchoolYears = new SelectList(schoolYears, "Value", "Text");
            }
        }
    }
}