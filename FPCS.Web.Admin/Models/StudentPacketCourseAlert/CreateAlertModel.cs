﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using DBE = FPCS.Data.Entities;

namespace FPCS.Web.Admin.Models.StudentPacketCourseAlert
{
    public class CreateAlertModel
    {
        public CreateAlertModel()
        {
        }

        public CreateAlertModel(Int64 studentPacketCourseId)
        {
            StudentPacketCourseId = studentPacketCourseId;
        }

        public Int64 StudentPacketCourseAlertId { get; set; }

        public Int64 StudentPacketCourseId { get; set; }

        public Guid DbUserId { get; set; }

        [Display(Name="Name of Send User")]
        public String NameSendUser { get; set; }

        [Display(Name = "Alert")]
        public String Value { get; set; }
    }
}