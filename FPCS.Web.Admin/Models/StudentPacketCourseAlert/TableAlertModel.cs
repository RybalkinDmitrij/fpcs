﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FPCS.Web.Admin.Models.StudentPacketCourseAlert
{
    public class TableAlertModel
    {
        public List<AlertModel> Alerts { get; set; }
    }
}