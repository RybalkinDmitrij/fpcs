﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FPCS.Web.Admin.Models.SignedPacket
{
    public class SignedPacketPrintModel
    {
        //public List<GoodAndServiceApprovalModel> Data { get; set; }

        public List<SignedPacketMiniModel> Data { get; set; }

        public String PrintAddress { get; set; }

        public String PrintState { get; set; }

        public String PrintPhone { get; set; }

        public String PrintFax { get; set; }

        public String PrintDateNow { get; set; }
    }
}