﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FPCS.Web.Admin.Models.Student.Abstract;

namespace FPCS.Web.Admin.Models.SignedPacket
{
	public class SignedPacketMiniModel: AbstractStudent
	{
		public Guid StudentGuid { get; set; }

		public Int64 StudentPacketId { get; set; }

		public String StudentName { get; set; }

        public String DateOfBirth { get; set; }

		public String Grade { get; set; }

		public String HomePhone { get; set; }

		public String EmailFamily { get; set; }

        public String DateSponsorSignature { get; set; }

        public String DateGuardianSignature { get; set; }
	}
}