﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FPCS.Core.jqGrid;

namespace FPCS.Web.Admin.Models.SignedPacket
{
	public class SignedPacketListOptions
	{
		[GridProperty(ExtensionType.All, true, FilterOperation.StartsWith)]
		public String StudentName { get; set; }
	}
}