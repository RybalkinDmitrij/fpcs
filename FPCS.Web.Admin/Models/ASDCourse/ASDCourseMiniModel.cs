﻿using System;

namespace FPCS.Web.Admin.Models.ASDCourse
{
    public class ASDCourseMiniModel
    {
        public Int32 ASDCourseId { get; set; }

        public String ExternalASDCourseId { get; set; }

        public String Subject { get; set; }

        public String Name { get; set; }

        public Decimal GradCredit { get; set; }

        public String IsActivated { get; set; }

        public String IsFree { get; set; }
    }
}