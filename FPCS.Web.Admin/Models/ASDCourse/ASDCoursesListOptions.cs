﻿using System;

using FPCS.Core.jqGrid;

namespace FPCS.Web.Admin.Models.ASDCourse
{
    public class ASDCoursesListOptions
    {
        [GridProperty(ExtensionType.All, true)]
        public Int32? ASDCourseId { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Contains)]
        public String ExternalASDCourseId { get; set; }

        [GridProperty(ExtensionType.All, true, "Subject.Name", FilterOperation.Contains)]
        public String Subject { get; set; }

        [GridProperty(ExtensionType.All, true, FilterOperation.Contains)]
        public String Name { get; set; }

        [GridProperty(ExtensionType.Filter, false)]
        [GridProperty(ExtensionType.All, true)]
        public String IsActivated { get; set; }
    }
}