﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;

using FPCS.Core.Unity;
using FPCS.Data;
using FPCS.Data.Repo;

namespace FPCS.Web.Admin.Models.ASDCourse
{
    public class ASDCourseEditModel
    {
        [Required]
        [Display(Name = "ASDCourseId")]
        public Int32 ASDCourseId { get; set; }

        [Required]
        [Display(Name = "ASD Course Id")]
        public String ExternalASDCourseId { get; set; }

        [Display(Name = "Description")]
        public String Description { get; set; }

        [Display(Name = "Name")]
        public String Name { get; set; }

        [Required]
        [Display(Name = "Grad Unit")]
        public Decimal GradCredit { get; set; }

        [Required]
        [Display(Name = "Subject")]
        public Int32 SubjectId { get; set; }

        [Required]
        [Display(Name = "Activate ASD course. (Please note, ASD course will be accessed for creating FPCS courses)")]
        public Boolean IsActivated { get; set; }

        [Required]
        [Display(Name = "Free ASD course")]
        public Boolean IsFree { get; set; }

        public Boolean IsCreatedFPCSCourses { get; set; }

        public SelectList Subjects { get; set; }

        public void Init()
        {
            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                var subjects = uow.GetRepo<ISubjectRepo>()
                                  .GetAll()
                                  .OrderBy(x => x.Code)
                                  .Select(x => new Lookup<Int32>
                                  {
                                      Value = x.SubjectId,
                                      Text = x.Name
                                  })
                                  .ToList();

                Subjects = new SelectList(subjects, "Value", "Text");
            }
        }
    }
}