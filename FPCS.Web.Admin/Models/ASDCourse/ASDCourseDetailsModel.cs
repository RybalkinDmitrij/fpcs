﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FPCS.Web.Admin.Models.ASDCourse
{
    public class ASDCourseDetailsModel
    {
        [Display(Name = "ASDCourseId")]
        public Int32 ASDCourseId { get; set; }

        [Display(Name = "ASD Course Id")]
        public String ExternalASDCourseId { get; set; }

        [Display(Name = "Description")]
        public String Description { get; set; }

        [Display(Name = "Name")]
        public String Name { get; set; }

        [Display(Name = "Grad Unit")]
        public Decimal GradCredit { get; set; }

        [Display(Name = "Subject")]
        public String Subject { get; set; }

        [Display(Name = "Activate ASD course. (Please note, ASD course will be accessed for creating FPCS courses)")]
        public Boolean IsActivated { get; set; }

        [Display(Name = "Free ASD course")]
        public Boolean IsFree { get; set; }
    }
}