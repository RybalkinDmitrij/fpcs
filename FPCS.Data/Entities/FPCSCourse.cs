﻿using System;
using System.Collections.Generic;
using FPCS.Data.Enums;

namespace FPCS.Data.Entities
{
	public class FPCSCourse
	{
		public Int64 FPCSCourseId { get; set; }

		public String Name { get; set; }

		public Int32 GradeLevel { get; set; }

		public Int32 CourseSubjectId { get; set; }

		public CourseClassType? ClassType { get; set; }

		public String Location { get; set; }

		public String ScheduleComments { get; set; }

		public DateTimeOffset RegistrationDeadline { get; set; }

		public DateTimeOffset ClassStartDate { get; set; }

		public DateTimeOffset ClassEndDate { get; set; }

		#region Course cost

		public Int32? MinStudentsCount { get; set; }

		public Int32? MaxStudentsCount { get; set; }

		public Decimal? HoursWithAllStudents { get; set; }

		public Decimal? HoursTeacherPlanning { get; set; }

		public Decimal? TotalHours { get; set; }

		public Int32? EnrolledCount { get; set; }

		public Decimal? HoursChargedPerStudent { get; set; }

		public Decimal? TeacherCostPerStudent { get; set; }

		public Decimal? TotalCourseCost { get; set; }

		#endregion Course cost

		public Boolean IsActivated { get; set; }

		public Boolean IsDeleted { get; set; }

		public DateTimeOffset CreatedDate { get; set; }

		public DateTimeOffset UpdatedDate { get; set; }

		public Grade? MinGrade { get; set; }

		public Grade? MaxGrade { get; set; }

		public Semester Semester { get; set; }

		public ScheduleRepetition? ScheduleRepetition { get; set; }

		public Int32 ASDCourseId { get; set; }
		public virtual ASDCourse ASDCourse { get; set; }

		public Guid? TeacherId { get; set; }
		public virtual Teacher Teacher { get; set; }

		public Guid? GuardianId { get; set; }
		public virtual Guardian Guardian { get; set; }

		public Int32? VendorId { get; set; }
		public virtual Vendor Vendor { get; set; }

		public Int32? SubjectId { get; set; }
		public virtual Subject Subject { get; set; }

		public virtual ICollection<CourseScheduleOnDay> CourseScheduleOnDays { get; set; }

		public virtual ICollection<Family> FPCSCoursesRestrictions { get; set; }

		public virtual ICollection<StudentPacketCourse> StudentPacketCourses { get; set; }

		public virtual ICollection<GoodService> GoodServices { get; set; }

		public virtual ICollection<ILP> ILPs { get; set; }
	}
}
