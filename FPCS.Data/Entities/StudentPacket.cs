﻿using FPCS.Data.Enums;
using System;
using System.Collections.Generic;

namespace FPCS.Data.Entities
{
	public class StudentPacket
	{
		public Int64 StudentPacketId { get; set; }

		public Boolean IsDeleted { get; set; }

		public DateTimeOffset CreatedDate { get; set; }

		public DateTimeOffset UpdatedDate { get; set; }

		public Guid StudentId { get; set; }
		public virtual Student Student { get; set; }

		public Guid? SponsorTeacherId { get; set; }
		public virtual Teacher SponsorTeacher { get; set; }

		public Sign? SponsorSignatureSem1 { get; set; }
		public DateTime? DateSponsorSignatureSem1 { get; set; }

		public Sign? GuardianSignatureSem1 { get; set; }
		public DateTime? DateGuardianSignatureSem1 { get; set; }

		public Sign? SponsorSignatureSem2 { get; set; }
		public DateTime? DateSponsorSignatureSem2 { get; set; }

		public Sign? GuardianSignatureSem2 { get; set; }
		public DateTime? DateGuardianSignatureSem2 { get; set; }

		public Sign? SponsorSignatureSummer { get; set; }
		public DateTime? DateSponsorSignatureSummer { get; set; }

		public Sign? GuardianSignatureSummer { get; set; }
		public DateTime? DateGuardianSignatureSummer { get; set; }

		public Int32 SchoolYearId { get; set; }
		public virtual SchoolYear SchoolYear { get; set; }

		public Boolean IsLocked { get; set; }

		/// <summary>
		/// ASD Testing Agreement
		/// </summary>
		public Boolean IsASDTASigned { get; set; }

		public Guid? ASDTAGuardianId { get; set; }
		public Guardian ASDTAGuardian { get; set; }

		/// <summary>
		/// Progress Report Agreement
		/// </summary>
		public Boolean IsPRASigned { get; set; }

		public Guid? PRAGuardianId { get; set; }
		public Guardian PRAGuardian { get; set; }

		public AmpEla AmpELA { get; set; }
		public String AmpELAComment { get; set; }

		public AmpMath AmpMath { get; set; }
		public String AmpMathComment { get; set; }

		public Boolean A504 { get; set; }

		public Boolean SWD { get; set; }

		public virtual ICollection<GoodService> GoodServices { get; set; }

		public virtual ICollection<StudentPacketCourse> StudentPacketCourses { get; set; }
	}
}
