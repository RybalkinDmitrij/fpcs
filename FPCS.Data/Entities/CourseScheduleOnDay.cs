﻿using System;

namespace FPCS.Data.Entities
{
    public class CourseScheduleOnDay
    {
        public Int64 CourseScheduleOnDayId { get; set; }

        public TimeSpan StartTime { get; set; }

        public TimeSpan EndTime { get; set; }

        public DayOfWeek DayOfWeek { get; set; }

        public Int64 FPCSCourseId { get; set; }

        public virtual FPCSCourse FPCSCourse { get; set; }
    }
}
