﻿using System;

using FPCS.Data.Enums;

namespace FPCS.Data.Entities
{
    public class FamilyRelationship
    {
        public Int64 FamilyRelationshipId { get; set; }

        public FamilyRelationshipType FamilyRelationshipType { get; set; }

        public Guid StudentId { get; set; }

        public virtual Student Student { get; set; }

        public Guid GuardianId { get; set; }

        public virtual Guardian Guardian { get; set; }
    }
}
