﻿using FPCS.Data.Enums;
using System;
using System.Collections.Generic;

namespace FPCS.Data.Entities
{
    public class Teacher : DbUser
    {
        public TitlePerson Title { get; set; }

        public DateTime DateBirth { get; set; }

        public Boolean IsActive { get; set; }

        public String MailingAddress { get; set; }

        public String City { get; set; }

        public Int64 StateId { get; set; }
        public virtual State State { get; set; }

        public String ZipCode { get; set; }

        public String HomePhone { get; set; }

        public String BusPhone { get; set; }

        public Int32 Ext { get; set; }

        public String CellPhone { get; set; }

        public Boolean MastersDegree { get; set; }

        public String SSN { get; set; }

        //public String EMailAddress { get; set; }

        public String SecondEMailAddress { get; set; }

        public String DistrictCode { get; set; }

        public Boolean IsBenefitPaidASDSchool { get; set; }

        public Int32 ASDFTE { get; set; }

        public Boolean IsNotIncludeBenefits { get; set; }

        public Int32 FTCSFTE { get; set; }

        public String NameSchool { get; set; }

        public Boolean IsLeaveASD { get; set; }

        public Boolean IsSubtitleTeacher { get; set; }

        public Boolean IsOtherASDEmployee { get; set; }

        public Boolean IsOnASDEligibleToHire { get; set; }

        public Boolean IsRetiredASDTeacher { get; set; }

        public Int32 FPCSFTE { get; set; }

        public Boolean IsGroupInstruction { get; set; }

        public Boolean IsIndividualInstruction { get; set; }

        public Int32 YearsTeachingExperience { get; set; }

        public DateTime TeachingCertificateExpirationDate { get; set; }

        public String TeachingSalaryPlacement { get; set; }

        public Boolean IsAlaskaCertificationK8 { get; set; }

        public Boolean IsAlaskaCertificationK12 { get; set; }

        public Boolean IsAlaskaCertificationSpecialEducation { get; set; }

        public Boolean IsAlaskaCertificationSecondary { get; set; }

        public String AlaskaCertificationSubjectGrades { get; set; }

        public Boolean IsInMyClassroom { get; set; }

        public Boolean IsAtMyHome { get; set; }

        public Boolean IsAsStudentsHome { get; set; }

        public Boolean IsAtFPCSClassroom { get; set; }

        public Boolean IsOtherAvailableTeach { get; set; }

        public String OtherAvailableTeach { get; set; }

        public Boolean IsWeekdays { get; set; }

        public Boolean IsWeekdayAfternoons { get; set; }

        public Boolean IsWeekdayEvenings { get; set; }

        public Boolean IsWeekends { get; set; }

        public Boolean IsSummers { get; set; }

        public Decimal FlatRateHour { get; set; }

        public Decimal BasePayHour { get; set; }

        public Decimal PayHourwBenefits { get; set; }

        public PayType PayType { get; set; }

        public Decimal PerDeimRate { get; set; }

        public Boolean IsGuardian { get; set; }

        public virtual ICollection<ILPBank> ILPBanks { get; set; }

        public virtual ICollection<FPCSCourse> FPCSCourses { get; set; }

        public virtual ICollection<StudentPacket> SponsorStudentPackets { get; set; }

        public virtual ICollection<StudentPacketCourse> SponsorStudentPacketCoursesSignatures { get; set; }

        public virtual ICollection<StudentPacketCourse> InstructorStudentPacketCoursesSignatures { get; set; }
    }
}
