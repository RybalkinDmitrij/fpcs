﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Data.Entities
{
    public class MonthlyContactLog
    {
        public Int32 MonthlyContactLogId { get; set; }

        public Int64 FamilyId { get; set; }

        public virtual Family Family { get; set; }

        public Guid TeacherId { get; set; }

        public virtual DbUser Teacher { get; set; }

        public DateTime Date { get; set; }

        public String Comment { get; set; }
    }
}
