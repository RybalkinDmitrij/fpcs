﻿using System;
using System.Collections.Generic;

namespace FPCS.Data.Entities
{
	public class SchoolYear
	{
		public Int32 SchoolYearId { get; set; }

		public Int32 Year { get; set; }

		public String Name { get; set; }

		public DateTimeOffset StartDate { get; set; }

		public DateTimeOffset EndDate { get; set; }

		public DateTimeOffset HalfDate { get; set; }

		public Int32 ILPHoursLimit { get; set; }

		public Decimal CountHoursWorkDay { get; set; }

		public Decimal PlusBudgetLimit { get; set; }

		public Decimal BookMaterialFee { get; set; }

		public Decimal GroupLifeInsurance { get; set; }

		public Decimal GroupMedicalInsurance { get; set; }

		public Decimal WorkersComp { get; set; }

		public Decimal Unemployment { get; set; }

		public Decimal FICA { get; set; }

		public Decimal FICACap { get; set; }

		public Decimal Medicare { get; set; }

		public Decimal TERSBaseContract { get; set; }

		public Decimal PERSBaseContract { get; set; }

		public Decimal TERSRetirement { get; set; }

		public Decimal PERSRetirement { get; set; }

		public Decimal GeneralSupplies { get; set; }

		public Int32 FundingAmountGradeK { get; set; }

		public Int32 FundingAmountGrade1 { get; set; }

		public Int32 FundingAmountGrade2 { get; set; }

		public Int32 FundingAmountGrade3 { get; set; }

		public Int32 FundingAmountGrade4 { get; set; }

		public Int32 FundingAmountGrade5 { get; set; }

		public Int32 FundingAmountGrade6 { get; set; }

		public Int32 FundingAmountGrade7 { get; set; }

		public Int32 FundingAmountGrade8 { get; set; }

		public Int32 FundingAmountGrade9 { get; set; }

		public Int32 FundingAmountGrade10 { get; set; }

		public Int32 FundingAmountGrade11 { get; set; }

		public Int32 FundingAmountGrade12 { get; set; }

		public Int32 FundingAmountGradeG { get; set; }

		public Int32 CoreUnitGradeK { get; set; }

		public Int32 CoreUnitGrade1 { get; set; }

		public Int32 CoreUnitGrade2 { get; set; }

		public Int32 CoreUnitGrade3 { get; set; }

		public Int32 CoreUnitGrade4 { get; set; }

		public Int32 CoreUnitGrade5 { get; set; }

		public Int32 CoreUnitGrade6 { get; set; }

		public Int32 CoreUnitGrade7 { get; set; }

		public Int32 CoreUnitGrade8 { get; set; }

		public Int32 CoreUnitGrade9 { get; set; }

		public Int32 CoreUnitGrade10 { get; set; }

		public Int32 CoreUnitGrade11 { get; set; }

		public Int32 CoreUnitGrade12 { get; set; }

		public Int32 CoreUnitGradeG { get; set; }

		public Int32 ElectiveUnitGradeK { get; set; }

		public Int32 ElectiveUnitGrade1 { get; set; }

		public Int32 ElectiveUnitGrade2 { get; set; }

		public Int32 ElectiveUnitGrade3 { get; set; }

		public Int32 ElectiveUnitGrade4 { get; set; }

		public Int32 ElectiveUnitGrade5 { get; set; }

		public Int32 ElectiveUnitGrade6 { get; set; }

		public Int32 ElectiveUnitGrade7 { get; set; }

		public Int32 ElectiveUnitGrade8 { get; set; }

		public Int32 ElectiveUnitGrade9 { get; set; }

		public Int32 ElectiveUnitGrade10 { get; set; }

		public Int32 ElectiveUnitGrade11 { get; set; }

		public Int32 ElectiveUnitGrade12 { get; set; }

		public Int32 ElectiveUnitGradeG { get; set; }

		public Boolean DisableAccessForParents { get; set; }

		public Boolean DisableAccessForTeachers { get; set; }

		public String DisableMessage { get; set; }

		public DateTimeOffset CreatedDate { get; set; }

		public DateTimeOffset UpdatedDate { get; set; }

		public virtual ICollection<ASDCourse> ASDCourses { get; set; }

		public virtual ICollection<StudentPacket> StudentPackets { get; set; }

		public virtual ICollection<Vendor> Vendors { get; set; }

		public virtual ICollection<DbUser> DbUsers { get; set; }

		public virtual ICollection<Family> Families { get; set; }

		public virtual ICollection<ILPBank> ILPBanks { get; set; }
	}
}
