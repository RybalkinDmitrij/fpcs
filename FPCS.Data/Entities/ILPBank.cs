﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Data.Entities
{
    /// <summary>
    /// Individual Learning Plan Bank
    /// </summary>
    public class ILPBank
    {
        public Int32 ILPBankId { get; set; }

        public Int32 ILPId { get; set; }

        public String ILPName { get; set; }

        public Int32? SubjectId { get; set; }
        public virtual Subject Subject { get; set; }

        public Guid? TeacherId { get; set; }
        public virtual Teacher Teacher { get; set; }

        public Guid? GuardianId { get; set; }
        public virtual Guardian Guardian { get; set; }

        public Int32 SchoolYearId { get; set; }
        public SchoolYear SchoolYear { get; set; }

        public Boolean IsPublic { get; set; }

        public Int32 CourseHrs { get; set; }

        /// <summary>
        /// Description of the course including methods needed ...
        /// </summary>
        public String DescriptionCourse { get; set; }

        /// <summary>
        /// Standards: Common core or GLE
        /// </summary>
        public String Standards { get; set; }
        
        /// <summary>
        /// Student Activities
        /// </summary>
        public String StudentActivities { get; set; }

        /// <summary>
        /// Materials, Resources
        /// </summary>
        public String MaterialsResources { get; set; }

        /// <summary>
        /// Role of Parent/Teacher/Vendor/any additional responsibilities of the student
        /// </summary>
        public String RoleAnyPeople { get; set; }

        /// <summary>
        /// Evaluation and Grading (Pass/Fail)
        /// </summary>
        public Boolean EvaluationGradingPassFail { get; set; }

        /// <summary>
        /// Evaluation and Grading (Grading Scale)
        /// </summary>
        public Boolean EvaluationGradingGradingScale { get; set; }

        /// <summary>
        /// Evaluation and Grading (O/S/N)
        /// </summary>
        public Boolean EvaluationGradingOSN { get; set; }

        /// <summary>
        /// Evaluation and Grading (Other)
        /// </summary>
        public Boolean EvaluationGradingOther { get; set; }

        /// <summary>
        /// Evaluation and Grading (Other Explain)
        /// </summary>
        public String EvaluationGradingOtherExplain { get; set; }

        /// <summary>
        /// What will be evaluated? What will be the measurable outcomes?
        /// </summary>
        public String EvaluatedMeasurableOutcomes { get; set; }

        /// <summary>
        /// Course Syllabus: work out a timeline (scope and sequence) of all major topics to be covered.
        /// </summary>
        public String CourseSyllabus { get; set; }

        /// <summary>
        /// Guardian ILP Modifications
        /// </summary>
        public String GuardianILPModifications { get; set; }

        /// <summary>
        /// Instructor ILP Modifications
        /// </summary>
        public String InstructorILPModifications { get; set; }
    }
}
