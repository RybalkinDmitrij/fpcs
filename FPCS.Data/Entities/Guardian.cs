﻿using System;
using System.Collections.Generic;

namespace FPCS.Data.Entities
{
    public class Guardian : DbUser
    {
        public Int64? StateId { get; set; }

        public String Employer { get; set; }

        public Boolean? IsActiveMilitary { get; set; }

        public String Rank { get; set; }

        public String Pager { get; set; }

        public String BusinessPhone { get; set; }

        public String Ext { get; set; }

        public String CellPhone { get; set; }

        public String Address { get; set; }

        public String City { get; set; }

        public String Country { get; set; }

        public String Zip { get; set; }

        public virtual State State { get; set; }

        public virtual ICollection<ILPBank> ILPBanks { get; set; }

        public virtual ICollection<FPCSCourse> FPCSCourses { get; set; }

        public virtual ICollection<Family> Families { get; set; }

        public virtual ICollection<FamilyRelationship> Relationships { get; set; }

        public virtual ICollection<StudentPacketCourse> StudentPacketCoursesSignatures { get; set; }

        public virtual ICollection<StudentPacket> ASDTAStudentPackets { get; set; }

        public virtual ICollection<StudentPacket> PRAStudentPackets { get; set; }
    }
}
