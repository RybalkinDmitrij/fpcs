﻿using FPCS.Data.Enums;
using System;
using System.Collections.Generic;

namespace FPCS.Data.Entities
{
    public class ASDCourse
    {
        public Int32 ASDCourseId { get; set; }

        public Int32 UnqASDCourseId { get; set; }

        public String ExternalASDCourseId { get; set; }

        public String Description { get; set; }

        public Decimal GradCredit { get; set; }

        public String Name { get; set; }

        public TypeGrade TypeGrade { get; set; }

        public DateTimeOffset CreatedDate { get; set; }

        public DateTimeOffset UpdatedDate { get; set; }

        public Boolean IsActivated { get; set; }

        public Boolean IsDeleted { get; set; }

        public Boolean IsFree { get; set; }

        public Int32 SubjectId { get; set; }

        public virtual Subject Subject { get; set; }

        public Int32 SchoolYearId { get; set; }

        public virtual SchoolYear SchoolYear { get; set; }

        public virtual ICollection<FPCSCourse> FPCSCourses { get; set; }
    }
}
