﻿using FPCS.Data.Enums;
using System;
using System.Collections.Generic;

namespace FPCS.Data.Entities
{
	/// <summary>
	/// Good or Service
	/// </summary>
	public class GoodService
	{
		public Int64 GoodServiceId { get; set; }

		public Boolean IsDeleted { get; set; }

		public DateTimeOffset CreatedDate { get; set; }

		public DateTimeOffset UpdatedDate { get; set; }

		public Int64? StudentPacketCourseId { get; set; }
		public virtual StudentPacketCourse StudentPacketCourse { get; set; }

		public Int64? FPCSCourseId { get; set; }
		public virtual FPCSCourse FPCSCourse { get; set; }

		public Int64? StudentPacketId { get; set; }
		public virtual StudentPacket StudentPacket { get; set; }

		public String TempId { get; set; }

		public virtual ICollection<GoodServiceBalance> GoodServiceBalances { get; set; }

		/// <summary>
		/// Good or Service
		/// </summary>
		public GoodOrService GoodOrService { get; set; }

		/// <summary>
		/// Requisition Or Reimbursement
		/// </summary>
		public RequisitionOrReimbursement RequisitionOrReimbursement { get; set; }

		/// <summary>
		/// Type of Requisition or Reimbursement
		/// </summary>
		public TypeRequisitionReimbursement TypeRequisitionReimbursement { get; set; }

		public Int32 VendorId { get; set; }
		public virtual Vendor Vendor { get; set; }

		/// <summary>
		/// Admin comments
		/// </summary>
		public String AdminComments { get; set; }

		/// <summary>
		/// Approval given by
		/// </summary>
		public ApprovalGivenBy? ApprovalGivenBy { get; set; }

		/// <summary>
		/// Number of units
		/// </summary>
		public Int32 NumberOfUnits { get; set; }

		/// <summary>
		/// Unit price
		/// </summary>
		public Decimal UnitPrice { get; set; }

		/// <summary>
		/// Shipping / Handling / Fees
		/// </summary>
		public Decimal ShippingHandlingFees { get; set; }

		// Administrative

		/// <summary>
		/// Line item name
		/// </summary>
		public String LineItemName { get; set; }

		/// <summary>
		/// Line item desc
		/// </summary>
		public String LineItemDesc { get; set; }

		// Supplies

		/// <summary>
		/// Supply type
		/// </summary>
		public SupplyType? SupplyType { get; set; }

		/// <summary>
		/// Count (Requisition -> Supplies)
		/// </summary>
		public Int32 Count { get; set; }

		/// <summary>
		/// Catalog/MFG#
		/// </summary>
		public String CatalogMFG { get; set; }

		/// <summary>
		/// Catalog Issue PG#
		/// </summary>
		public String CatalogIssuePG { get; set; }

		/// <summary>
		/// Unit type
		/// </summary>
		public UnitType? UnitType { get; set; }

		/// <summary>
		/// Bar code
		/// </summary>
		public String BarCode { get; set; }

		/// <summary>
		/// Consumable
		/// </summary>
		public Consumable? Consumable { get; set; }

		// Textbook/Curriculum

		/// <summary>
		/// Type goods of textbook/curriculum
		/// </summary>
		public TypeTextbookCurriculum? TypeTextbookCurriculum { get; set; }

		/// <summary>
		/// Title
		/// </summary>
		public String Title { get; set; }

		/// <summary>
		/// Author
		/// </summary>
		public String Author { get; set; }

		/// <summary>
		/// Publisher ISBN
		/// </summary>
		public String PublisherISBN { get; set; }

		/// <summary>
		/// Publisher name
		/// </summary>
		public String PublisherName { get; set; }

		/// <summary>
		/// Edition
		/// </summary>
		public String Edition { get; set; }

		// ASD class

		/// <summary>
		/// High school
		/// </summary>
		public HighSchool? HighSchool { get; set; }

		/// <summary>
		/// # of Semesters
		/// </summary>
		public Int32 NumberSemesters { get; set; }

		/// <summary>
		/// Cost per semester
		/// </summary>
		public Decimal CostPerSemester { get; set; }

		// University Course

		/// <summary>
		/// Class type
		/// </summary>
		public ClassType? ClassType { get; set; }

		/// <summary>
		/// Course title
		/// </summary>
		public String CourseTitle { get; set; }

		/// <summary>
		/// Course number
		/// </summary>
		public String CourseNumber { get; set; }

		/// <summary>
		/// Date of birth
		/// </summary>
		public DateTime? DateBirth { get; set; }

		/// <summary>
		/// Credits
		/// </summary>
		public Credits? Credits { get; set; }

		/// <summary>
		/// Semester
		/// </summary>
		public Semester2? Semester { get; set; }

		// Vendor service

		/// <summary>
		/// Type of service
		/// </summary>
		public TypeService? TypeService { get; set; }

		/// <summary>
		/// Description
		/// </summary>
		public String Description { get; set; }

		/// <summary>
		/// Begin date
		/// </summary>
		public DateTime? BeginDate { get; set; }

		/// <summary>
		/// End date
		/// </summary>
		public DateTime? EndDate { get; set; }

		/// <summary>
		/// Vendor unit type
		/// </summary>
		public VendorUnitType? VendorUnitType { get; set; }

		// Computer Lease

		public ComputerLeaseItemType? ComputerLeaseItemType { get; set; }

		public ComputerLeaseItemDesc? ComputerLeaseItemDesc { get; set; }

		public String DetailedItemDesc { get; set; }

		// Internet

		public InternetItemType? InternetItemType { get; set; }

		// GOOD SERVICE APPROVAL

		public GoodOrServiceApproved? GoodOrServiceApproved { get; set; }

		public String CommentsRejectReason { get; set; }

		public Boolean? IsClosed { get; set; }

		public Guid? UserRequestId { get; set; }

		public virtual DbUser UserRequest { get; set; }

		public DateTime? DateRequest { get; set; }
	}
}