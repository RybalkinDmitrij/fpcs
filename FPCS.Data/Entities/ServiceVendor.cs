﻿using FPCS.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Data.Entities
{
    public class ServiceVendor
    {
        public Int32 ServiceVendorId { get; set; }

        //public TypeServiceVendor Type { get; set; }

        public Int32 SubjectId { get; set; }
        public Subject Subject { get; set; }

        public String Name { get; set; }

        public Boolean IsActive { get; set; }

        public virtual ICollection<Vendor> Vendors { get; set; }
    }
}
