﻿using System;
using System.Collections.Generic;

namespace FPCS.Data.Entities
{
    public class Family
    {
        public Int64 FamilyId { get; set; }

        public Int64 StateId { get; set; }

        public String Name { get; set; }

        public String Description { get; set; }

        public String Address { get; set; }

        public String MailingAddress { get; set; }

        public String Telephone { get; set; }

        public String CellPhone { get; set; }

        public String City { get; set; }

        public String Country { get; set; }

        public String Zip { get; set; }

        public String Email { get; set; }

        public String Password { get; set; }

        public Boolean IsUseDirectory { get; set; }

        public Boolean IsDeleted { get; set; }

        public DateTimeOffset CreatedDate { get; set; }

        public DateTimeOffset UpdatedDate { get; set; }

        public virtual State State { get; set; }

        public virtual Int32 SchoolYearId { get; set; }
        public SchoolYear SchoolYear { get; set; }

        public virtual ICollection<Guardian> Guardians { get; set; }

        public virtual ICollection<Student> Students { get; set; }

        public virtual ICollection<FPCSCourse> FPCSCoursesRestrictions { get; set; }

        public virtual ICollection<MonthlyContactLog> MonthlyContactLogs { get; set; }
    }
}
