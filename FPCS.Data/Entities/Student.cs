﻿using System;
using System.Collections.Generic;

using FPCS.Data.Enums;

namespace FPCS.Data.Entities
{
    public class Student : DbUser
    {
        public Int32 GradYear { get; set; }

        public DateTimeOffset DateOfBirth { get; set; }

        public DateTimeOffset? EnrollmentDate { get; set; }

        public DateTimeOffset? WithdrawalDate { get; set; }

        public String PrivateSchoolName { get; set; }

        public Int32 PercentInSchoolDistrict { get; set; }

        public Boolean IsGraduateFromFPCS { get; set; }

        public Boolean IsASDContractHoursExemption { get; set; }

        public String ReasonASDContractHoursExemption { get; set; }

        public Sex Sex { get; set; }

        public Grade Grade { get; set; }

        public String ZangleID { get; set; }

        public String StateID { get; set; }

        public EnrollmentStatus? EnrollmentStatus { get; set; }

        public Boolean? IsBirthCertificate { get; set; }

        public Boolean? IsGradesNotSubmitted { get; set; }

        public Boolean? IsILPPhilosophy { get; set; }

        public Boolean? IsMedicalRelease { get; set; }

        public Boolean? IsProgressReportSignature { get; set; }

        public Boolean? IsShotRecords { get; set; }

        public Boolean? IsTestingAgreement { get; set; }

        public Boolean? IsOther { get; set; }

        public Int32? PercentagePlanningEnroll { get; set; }

        public Boolean? IsDoYouPlanGraduate { get; set; }

        public Int32? CoreCreditExemption { get; set; }

        public String CoreCreditExemptionReason { get; set; }

        public Int32? ElectiveCreditExemption { get; set; }

        public String ElectiveCreditExemptionReason { get; set; }

        public Boolean? IEPHasChildEligible { get; set; }

        public Boolean? IEPGiftedTalented { get; set; }

        public Boolean? IEPDeafBlindness { get; set; }

        public Boolean? IEPHearingImpairment { get; set; }

        public Boolean? IEPMultipleDisability { get; set; }

        public Boolean? IEPTraumaticBrainInjury { get; set; }

        public Boolean? IEPAutismAsbergerSyndrome { get; set; }

        public Boolean? IEPEarlyChildhoodDevelopmentalDelay { get; set; }

        public Boolean? IEPSpecificLearningDisability { get; set; }

        public Boolean? IEPOrthopedicImpairment { get; set; }

        public Boolean? IEPSpeechLanguageImpairment { get; set; }

        public Boolean? IEPDeafness { get; set; }

        public Boolean? IEPEmotionalDisturbance { get; set; }

        public Boolean? IEPMentalRetardation { get; set; }

        public Boolean? IEPOtherHealthImpairment { get; set; }

        public Boolean? IEPVisualImpairment { get; set; }

        public Boolean? IEPHasChildReceiving { get; set; }

        public Boolean? IEPHasChildFormally { get; set; }

        public DateTime? IEPIsExpirationDate { get; set; }

        public DateTime? IEPIsNextEligibilityEvaluation { get; set; }

        public Boolean? IEPHasChildLearnedAnotherLanguage { get; set; }

        public OtherLanguage? IEPHasChildExperiencedLearned { get; set; }

        public Boolean? IEPIsChildEligibleBilingualProgram { get; set; }

        public Boolean? IEPHasChildCurrentlyReceivingServicesBilingualProgram { get; set; }

        public Boolean? IEPHasChildReceivingServicesMigrantEducation { get; set; }

        public String ILPPhilosophy { get; set; }

        public virtual ICollection<Family> Families { get; set; }

        public virtual ICollection<FamilyRelationship> Relationships { get; set; }

        public virtual ICollection<StudentPacket> StudentPackets { get; set; }

        public virtual ICollection<Transfer> FromTransfers { get; set; }

        public virtual ICollection<Transfer> ToTransfers { get; set; }
    }
}
