﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Data.Entities
{
	/// <summary>
	/// Individual Learning Plan
	/// </summary>
	public class ILP
	{
        public ILP()
		{
		}

        public ILP(String instructor)
        {
            Instructor = "";
            CourseHrs = 0;
            EvaluationGradingGradingScale = false;
            EvaluationGradingPassFail = false;
            EvaluationGradingOther = false;
            EvaluationGradingOSN = false;
        }

		public Int32 ILPId { get; set; }

		public String ILPName { get; set; }

		public String Instructor { get; set; }

		public Int64? StudentPacketCourseId { get; set; }
		public virtual StudentPacketCourse StudentPacketCourse { get; set; }

		public Int64? FPCSCourseId { get; set; }
		public virtual FPCSCourse FPCSCourse { get; set; }

		public Int32 CourseHrs { get; set; }

		/// <summary>
		/// Description of the course including methods needed ...
		/// </summary>
		public String DescriptionCourse { get; set; }

		/// <summary>
		/// Standards: Common core or GLE
		/// </summary>
		public String Standards { get; set; }

		/// <summary>
		/// Student Activities
		/// </summary>
		public String StudentActivities { get; set; }

		/// <summary>
		/// Materials, Resources
		/// </summary>
		public String MaterialsResources { get; set; }

		/// <summary>
		/// Role of Parent/Teacher/Vendor/any additional responsibilities of the student
		/// </summary>
		public String RoleAnyPeople { get; set; }

		/// <summary>
		/// Evaluation and Grading (Pass/Fail)
		/// </summary>
		public Boolean EvaluationGradingPassFail { get; set; }

		/// <summary>
		/// Evaluation and Grading (Grading Scale)
		/// </summary>
		public Boolean EvaluationGradingGradingScale { get; set; }

		/// <summary>
		/// Evaluation and Grading (O/S/N)
		/// </summary>
		public Boolean EvaluationGradingOSN { get; set; }

		/// <summary>
		/// Evaluation and Grading (Other)
		/// </summary>
		public Boolean EvaluationGradingOther { get; set; }

		/// <summary>
		/// Evaluation and Grading (Other Explain)
		/// </summary>
		public String EvaluationGradingOtherExplain { get; set; }

		/// <summary>
		/// What will be evaluated? What will be the measurable outcomes?
		/// </summary>
		public String EvaluatedMeasurableOutcomes { get; set; }

		/// <summary>
		/// Course Syllabus: work out a timeline (scope and sequence) of all major topics to be covered.
		/// </summary>
		public String CourseSyllabus { get; set; }

		/// <summary>
		/// Guardian ILP Modifications
		/// </summary>
		public String GuardianILPModifications { get; set; }

		/// <summary>
		/// Instructor ILP Modifications
		/// </summary>
		public String InstructorILPModifications { get; set; }
	}
}
