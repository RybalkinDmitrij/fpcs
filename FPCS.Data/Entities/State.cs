﻿using System;
using System.Collections.Generic;

namespace FPCS.Data.Entities
{
    public class State
    {
        public Int64 StateId { get; set; }

        public String Code { get; set; }

        public String Name { get; set; }

        public virtual ICollection<Family> Families { get; set; }

        public virtual ICollection<Guardian> Guardians { get; set; }

        public virtual ICollection<Vendor> StreetAddressVendors { get; set; }

        public virtual ICollection<Vendor> MailingAddressVendors { get; set; }

        public virtual ICollection<Teacher> Teachers { get; set; }
    }
}
