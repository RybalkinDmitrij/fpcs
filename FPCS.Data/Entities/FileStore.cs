﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Data.Entities
{
    public class FileStore
    {
        public Int64 FileStoreId { get; set; }

        public String Name { get; set; }

        public String URL { get; set; }

        public Int64 ContentSize { get; set; }

        public DateTime? DateCreated { get; set; }

        public String MimeType { get; set; }

        public virtual ICollection<Vendor> Vendor { get; set; }
    }
}
