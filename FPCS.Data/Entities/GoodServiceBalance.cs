﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Data.Entities
{
    public class GoodServiceBalance
    {
        public Int64 GoodServiceBalanceId { get; set; }

        public Int64? GoodServiceId { get; set; }
        public virtual GoodService GoodService { get; set; }

        public String PO { get; set; }

        public String Invoice { get; set; }

        public String Check { get; set; }

        public DateTime? CheckDate { get; set; }

        public String Payee { get; set; }

        public DateTime? ReceiptDate { get; set; }

        public String Description { get; set; }

        public Decimal UnitPrice { get; set; }

        public Decimal QTY { get; set; }

        public Decimal Shipping { get; set; }

        public DateTime? DateEntered { get; set; }
    }
}
