﻿using FPCS.Data.Enums;
using System;
using System.Collections.Generic;

namespace FPCS.Data.Entities
{
    public class StudentPacketCourse
    {
        public Int64 StudentPacketCourseId { get; set; }

        public Boolean IsDeleted { get; set; }

        public DateTimeOffset CreatedDate { get; set; }

        public DateTimeOffset UpdatedDate { get; set; }

        public Int64 StudentPacketId { get; set; }
        public virtual StudentPacket StudentPacket { get; set; }

        public Int64 FPCSCourseId { get; set; }
        public virtual FPCSCourse FPCSCourse { get; set; }

        public Repeated Repeat { get; set; }

        public DateTime? DateSponsorSignature { get; set; }

        public Sign? SponsorSignature { get; set; }

        public String SponsorComment { get; set; }

        public Guid? SponsorId { get; set; }
        public virtual Teacher Sponsor { get; set; }

        public DateTime? DateInstructorSignature { get; set; }

        public Sign? InstructorSignature { get; set; }

        public String InstructorComment { get; set; }

        public Guid? InstructorId { get; set; }
        public virtual Teacher Instructor { get; set; }

        public DateTime? DateAdminSignature { get; set; }

        public Sign? AdminSignature { get; set; }

        public String AdminComment { get; set; }

        public Guid? AdminId { get; set; }
        public virtual Admin Admin { get; set; }

        public DateTime? DateGuardianSignature { get; set; }

        public Sign? GuardianSignature { get; set; }

        public String GuardianComment { get; set; }

        public Guid? GuardianId { get; set; }
        public virtual Guardian Guardian { get; set; }
        
        //public virtual ICollection<Guardian> GuardiansSignatures { get; set; }

        // Grades
        public String Q11 { get; set; }

        public String Q11GuidFile { get; set; }

        public String Q12 { get; set; }

        public String Q12GuidFile { get; set; }

        public String Q13 { get; set; }

        public String Q13GuidFile { get; set; }

        public String Q1 { get; set; }

        public String Q1Comment { get; set; }

        public Boolean Q1IsLock { get; set; }

        public String Q21 { get; set; }

        public String Q21GuidFile { get; set; }

        public String Q22 { get; set; }

        public String Q22GuidFile { get; set; }

        public String Q23 { get; set; }

        public String Q23GuidFile { get; set; }

        public String Q2 { get; set; }

        public String Q2Comment { get; set; }

        public Boolean Q2IsLock { get; set; }

        public String Sem1 { get; set; }

        public String Sem1Comment { get; set; }

        public Decimal? Sem1Credit { get; set; }

        public Boolean Sem1IsLock { get; set; }

        public String Q31 { get; set; }

        public String Q31GuidFile { get; set; }

        public String Q32 { get; set; }

        public String Q32GuidFile { get; set; }

        public String Q33 { get; set; }

        public String Q33GuidFile { get; set; }

        public String Q3 { get; set; }

        public String Q3Comment { get; set; }

        public Boolean Q3IsLock { get; set; }

        public String Q41 { get; set; }

        public String Q41GuidFile { get; set; }

        public String Q42 { get; set; }

        public String Q42GuidFile { get; set; }

        public String Q43 { get; set; }

        public String Q43GuidFile { get; set; }

        public String Q4 { get; set; }

        public String Q4Comment { get; set; }

        public Boolean Q4IsLock { get; set; }

        public String Sem2 { get; set; }

        public String Sem2Comment { get; set; }

        public Decimal? Sem2Credit { get; set; }

        public Boolean Sem2IsLock { get; set; }

        public String Q51 { get; set; }

        public String Q51GuidFile { get; set; }

        public String Q52 { get; set; }

        public String Q52GuidFile { get; set; }

        public String Q53 { get; set; }

        public String Q53GuidFile { get; set; }

        public String Q5 { get; set; }

        public String Q5Comment { get; set; }

        public Boolean Q5IsLock { get; set; }

        public String Q61 { get; set; }

        public String Q61GuidFile { get; set; }

        public String Q62 { get; set; }

        public String Q62GuidFile { get; set; }

        public String Q63 { get; set; }

        public String Q63GuidFile { get; set; }

        public String Q6 { get; set; }

        public String Q6Comment { get; set; }

        public Boolean Q6IsLock { get; set; }

        public String Sem3 { get; set; }

        public String Sem3Comment { get; set; }

        public Decimal? Sem3Credit { get; set; }

        public Boolean Sem3IsLock { get; set; }

        public Decimal? Credit { get; set; }

        public String Comment { get; set; }

        public Boolean IsInSystem { get; set; }

        public Boolean IsLock { get; set; }

        public virtual ICollection<GoodService> GoodServices { get; set; }

        public virtual ICollection<ILP> ILPs { get; set; }

        public virtual ICollection<StudentPacketCourseAlert> StudentPacketCourseAlerts { get; set; }
    }
}