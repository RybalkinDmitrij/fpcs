﻿using System;
using System.Collections.Generic;
using FPCS.Data.Enums;

namespace FPCS.Data.Entities
{
	public class Vendor
	{
		public int VendorID { get; set; }

		public int UnqVendorID { get; set; }

		public bool IsDeleted { get; set; }

		public int SchoolYearId { get; set; }
		public SchoolYear SchoolYear { get; set; }

		public VendorType Type { get; set; }

		public string BusinessName { get; set; }

		public string FirstName { get; set; }

		public string LastName { get; set; }

		public VendorStatus? Status { get; set; }

		public string StatusComments { get; set; }

		public string StreetAddress { get; set; }

		public string StreetAddressCity { get; set; }

		public long? StreetAddressStateId { get; set; }
		public State StreetAddressState { get; set; }

		public string StreetAddressZipCode { get; set; }

		public string MailingAddress { get; set; }

		public string MailingAddressCity { get; set; }

		public long? MailingAddressStateId { get; set; }
		public State MailingAddressState { get; set; }

		public string MailingAddressZipCode { get; set; }

		public string Phone { get; set; }

		public string Fax { get; set; }

		public string Email { get; set; }

		public Location? Location { get; set; }

		public string BusinessWebsite { get; set; }

		public string EmployerIdentification { get; set; }

		public string AKBusinessLicense { get; set; }

		public DateTime? LicenseExpiration { get; set; }

		public DateTime? InsuranceExpiration { get; set; }

		public string TrainingEducationExperience { get; set; }

		public string CommentsAboutServices { get; set; }

		public VendorUnitType? VendorUnitType { get; set; }

		public string OtherChargeMethod { get; set; }

		public decimal? Price { get; set; }

		public DateTime? ContractStartingDate { get; set; }

		/// <summary>
		///       Are you Non-Profit?
		/// </summary>
		public bool IsNonProfit { get; set; }

		/// <summary>
		///       Have you ever been convicted of a misdemeanor or felony?
		/// </summary>
		public bool IsMisdemeanorOrFelony { get; set; }

		/// <summary>
		///       Would you consent to a background check and submit to fingerprinting?
		/// </summary>
		public bool IsBackgroundCheckFingerprinting { get; set; }

		/// <summary>
		///       Do you have children who are currently enrolled in FPCS?
		/// </summary>
		public bool IsHaveChildrenCurrentlyEnrolled { get; set; }

		/// <summary>
		///       Are you a Retired Certificated ASD Teacher?
		/// </summary>
		public bool IsRetiredCertificatedASDTeacher { get; set; }

		/// <summary>
		///       Are you, or a member of your immediate family, an ASD employee or on the ASD eligible for hire list?
		/// </summary>
		public bool IsASDEmployeeEligibleHireList { get; set; }

		/// <summary>
		///       Are you currently available to provide services?
		/// </summary>
		public bool IsCurrentlyAvailableProvideServices { get; set; }

		/// <summary>
		///       Do you provide online Curriculum services?
		/// </summary>
		public bool IsProvideOnlineCurriculumServices { get; set; }

		public bool IsActive { get; set; }

		public bool IsCourseProvider { get; set; }

		public decimal CostCourseProvider { get; set; }

        public Int64? FileStoreId { get; set; }
        public virtual FileStore FileStore { get; set; }

		public virtual ICollection<ServiceVendor> Services { get; set; }

		public virtual ICollection<GoodService> GoodServices { get; set; }

		public virtual ICollection<FPCSCourse> FPCSCourses { get; set; }
	}
}