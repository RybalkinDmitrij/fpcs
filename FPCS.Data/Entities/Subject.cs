﻿using System;
using System.Collections.Generic;

namespace FPCS.Data.Entities
{
    public class Subject
    {
        public Int32 SubjectId { get; set; }

        public Int32 Code { get; set; }

        public String Name { get; set; }

        public String Description { get; set; }

        public Boolean IsDeleted { get; set; }

        public Boolean IsElective { get; set; }

        public virtual ICollection<ILPBank> ILPBanks { get; set; }
        public virtual ICollection<ASDCourse> ASDCourses { get; set; }

        public virtual ICollection<FPCSCourse> FPCSCourses { get; set; }

        public virtual ICollection<ServiceVendor> ServiceVendors { get; set; }
    }
}
