﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Data.Entities
{
    public class StudentPacketCourseAlert
    {
        public Int64 StudentPacketCourseAlertId { get; set; }

        public Int64 StudentPacketCourseId { get; set; }
        public virtual StudentPacketCourse StudentPacketCourse { get; set; }

        public Guid DbUserId { get; set; }
        public virtual DbUser User { get; set; }

        public String Value { get; set; }
    }
}
