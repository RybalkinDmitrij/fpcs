﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Data.Entities
{
    public class Transfer
    {
        public Int64 TransferId { get; set; }

        public Guid? FromStudentId { get; set; }

        public virtual Student FromStudent { get; set; }

        public Guid? ToStudentId { get; set; }

        public virtual Student ToStudent { get; set; }

        public Decimal TransferAmount { get; set; }

        public String Comment { get; set; }

        public DateTime DateCreate { get; set; }

        public String UserCreateName { get; set; }
    }
}
