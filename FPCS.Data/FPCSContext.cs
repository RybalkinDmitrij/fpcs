﻿using System.Data.Entity;

using FPCS.Data.Cofigs;

namespace FPCS.Data
{
    internal class StudentManagementContext : DbContext
    {
        public StudentManagementContext()
            : base("ConnectionString")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new DbUserConfig());
            modelBuilder.Configurations.Add(new AdminConfig());
            modelBuilder.Configurations.Add(new TeacherConfig());
            modelBuilder.Configurations.Add(new GuardianConfig());
            modelBuilder.Configurations.Add(new StudentConfig());
            modelBuilder.Configurations.Add(new FamilyConfig());
            modelBuilder.Configurations.Add(new FamilyRelationshipConfig());
            modelBuilder.Configurations.Add(new StateConfig());
            modelBuilder.Configurations.Add(new SchoolYearConfig());
            modelBuilder.Configurations.Add(new SubjectConfig());
            modelBuilder.Configurations.Add(new ASDCourseConfig());
            modelBuilder.Configurations.Add(new FPCSCourseConfig());
            modelBuilder.Configurations.Add(new CourseScheduleOnDayConfig());
            modelBuilder.Configurations.Add(new StudentPacketConfig());
            modelBuilder.Configurations.Add(new StudentPacketCourseConfig());
            modelBuilder.Configurations.Add(new GoodServiceConfig());
            modelBuilder.Configurations.Add(new GoodServiceBalanceConfig());
            modelBuilder.Configurations.Add(new VendorConfig());
            modelBuilder.Configurations.Add(new ServiceVendorConfig());
            modelBuilder.Configurations.Add(new ILPConfig());
            modelBuilder.Configurations.Add(new ILPBankConfig());
            modelBuilder.Configurations.Add(new StudentPacketCourseAlertConfig());
            modelBuilder.Configurations.Add(new TransferConfig());
            modelBuilder.Configurations.Add(new FileStoreConfig());

            modelBuilder.Configurations.Add(new MonthlyContactLogConfig());
        }
    }
}
