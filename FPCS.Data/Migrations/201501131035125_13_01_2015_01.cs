namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _13_01_2015_01 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.StudentPacketCourse", "Q11GuidFile", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "Q12GuidFile", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "Q13GuidFile", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "Q21GuidFile", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "Q22GuidFile", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "Q23GuidFile", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "Q31GuidFile", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "Q32GuidFile", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "Q33GuidFile", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "Q41GuidFile", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "Q42GuidFile", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "Q43GuidFile", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.StudentPacketCourse", "Q43GuidFile");
            DropColumn("dbo.StudentPacketCourse", "Q42GuidFile");
            DropColumn("dbo.StudentPacketCourse", "Q41GuidFile");
            DropColumn("dbo.StudentPacketCourse", "Q33GuidFile");
            DropColumn("dbo.StudentPacketCourse", "Q32GuidFile");
            DropColumn("dbo.StudentPacketCourse", "Q31GuidFile");
            DropColumn("dbo.StudentPacketCourse", "Q23GuidFile");
            DropColumn("dbo.StudentPacketCourse", "Q22GuidFile");
            DropColumn("dbo.StudentPacketCourse", "Q21GuidFile");
            DropColumn("dbo.StudentPacketCourse", "Q13GuidFile");
            DropColumn("dbo.StudentPacketCourse", "Q12GuidFile");
            DropColumn("dbo.StudentPacketCourse", "Q11GuidFile");
        }
    }
}
