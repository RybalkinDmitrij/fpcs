namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _14_01_2015_00 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FPCSCourse", "ClassType", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.FPCSCourse", "ClassType");
        }
    }
}
