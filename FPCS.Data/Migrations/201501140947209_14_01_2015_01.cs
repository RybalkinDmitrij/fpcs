namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _14_01_2015_01 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Family", "CellPhone", c => c.String(maxLength: 50));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Family", "CellPhone");
        }
    }
}
