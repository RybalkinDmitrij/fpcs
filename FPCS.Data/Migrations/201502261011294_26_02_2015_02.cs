namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _26_02_2015_02 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.StudentPacketCourse", "Sem1IsLock", c => c.Boolean(nullable: false, defaultValue: false));
            AddColumn("dbo.StudentPacketCourse", "Sem2IsLock", c => c.Boolean(nullable: false, defaultValue: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.StudentPacketCourse", "Sem2IsLock");
            DropColumn("dbo.StudentPacketCourse", "Sem1IsLock");
        }
    }
}
