namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CommentsSign : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.StudentPacketCourse", "SponsorComment", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "InstructorComment", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "AdminComment", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "GuardianComment", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.StudentPacketCourse", "GuardianComment");
            DropColumn("dbo.StudentPacketCourse", "AdminComment");
            DropColumn("dbo.StudentPacketCourse", "InstructorComment");
            DropColumn("dbo.StudentPacketCourse", "SponsorComment");
        }
    }
}
