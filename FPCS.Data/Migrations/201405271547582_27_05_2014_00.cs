namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _27_05_2014_00 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ASDCourse", "UnqASDCourseId", c => c.Int(nullable: false, defaultValue: 0));

            Sql("UPDATE [dbo].[ASDCourse] SET [UnqASDCourseId] = [ASDCourseId]");
        }
        
        public override void Down()
        {
            DropColumn("dbo.ASDCourse", "UnqASDCourseId");
        }
    }
}
