namespace FPCS.Data.Migrations
{
    using FPCS.Data.Enums;
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _10_02_2015_02 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.StudentPacketCourse", "Repeat", c => c.Int(nullable: false, defaultValue: (Int32)Repeated.NoRepeated));
        }
        
        public override void Down()
        {
            DropColumn("dbo.StudentPacketCourse", "Repeat");
        }
    }
}
