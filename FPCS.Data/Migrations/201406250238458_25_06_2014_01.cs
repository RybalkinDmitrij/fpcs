namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _25_06_2014_01 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ILPBank", "ILPId", c => c.Int(nullable: false));
            AddColumn("dbo.ILPBank", "SchoolYearId", c => c.Int(nullable: false));
            AddForeignKey("dbo.ILPBank", "SchoolYearId", "dbo.SchoolYear", "SchoolYearId");
            CreateIndex("dbo.ILPBank", "SchoolYearId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.ILPBank", new[] { "SchoolYearId" });
            DropForeignKey("dbo.ILPBank", "SchoolYearId", "dbo.SchoolYear");
            DropColumn("dbo.ILPBank", "SchoolYearId");
            DropColumn("dbo.ILPBank", "ILPId");
        }
    }
}
