namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class GradeStudentPacketCourse : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.StudentPacketCourse", "Q1", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "Q2", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "Sem1", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "Q3", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "Q4", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "Sem2", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "Credit", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.StudentPacketCourse", "Comment", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "IsInSystem", c => c.Boolean(nullable: false, defaultValue: false));
            AddColumn("dbo.StudentPacketCourse", "IsLock", c => c.Boolean(nullable: false, defaultValue: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.StudentPacketCourse", "IsLock");
            DropColumn("dbo.StudentPacketCourse", "IsInSystem");
            DropColumn("dbo.StudentPacketCourse", "Comment");
            DropColumn("dbo.StudentPacketCourse", "Credit");
            DropColumn("dbo.StudentPacketCourse", "Sem2");
            DropColumn("dbo.StudentPacketCourse", "Q4");
            DropColumn("dbo.StudentPacketCourse", "Q3");
            DropColumn("dbo.StudentPacketCourse", "Sem1");
            DropColumn("dbo.StudentPacketCourse", "Q2");
            DropColumn("dbo.StudentPacketCourse", "Q1");
        }
    }
}
