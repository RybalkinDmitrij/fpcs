// <auto-generated />
namespace FPCS.Data.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class ILP : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(ILP));
        
        string IMigrationMetadata.Id
        {
            get { return "201404010308529_ILP"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
