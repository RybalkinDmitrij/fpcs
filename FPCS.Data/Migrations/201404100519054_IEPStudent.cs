namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IEPStudent : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Student", "IEPHasChildEligible", c => c.Boolean());
            AddColumn("dbo.Student", "IEPGiftedTalented", c => c.Boolean());
            AddColumn("dbo.Student", "IEPDeafBlindness", c => c.Boolean());
            AddColumn("dbo.Student", "IEPHearingImpairment", c => c.Boolean());
            AddColumn("dbo.Student", "IEPMultipleDisability", c => c.Boolean());
            AddColumn("dbo.Student", "IEPTraumaticBrainInjury", c => c.Boolean());
            AddColumn("dbo.Student", "IEPAutismAsbergerSyndrome", c => c.Boolean());
            AddColumn("dbo.Student", "IEPEarlyChildhoodDevelopmentalDelay", c => c.Boolean());
            AddColumn("dbo.Student", "IEPSpecificLearningDisability", c => c.Boolean());
            AddColumn("dbo.Student", "IEPOrthopedicImpairment", c => c.Boolean());
            AddColumn("dbo.Student", "IEPSpeechLanguageImpairment", c => c.Boolean());
            AddColumn("dbo.Student", "IEPDeafness", c => c.Boolean());
            AddColumn("dbo.Student", "IEPEmotionalDisturbance", c => c.Boolean());
            AddColumn("dbo.Student", "IEPMentalRetardation", c => c.Boolean());
            AddColumn("dbo.Student", "IEPOtherHealthImpairment", c => c.Boolean());
            AddColumn("dbo.Student", "IEPVisualImpairment", c => c.Boolean());
            AddColumn("dbo.Student", "IEPHasChildReceiving", c => c.Boolean());
            AddColumn("dbo.Student", "IEPHasChildFormally", c => c.Boolean());
            AddColumn("dbo.Student", "IEPIsExpirationDate", c => c.DateTime());
            AddColumn("dbo.Student", "IEPIsNextEligibilityEvaluation", c => c.DateTime());
            AddColumn("dbo.Student", "IEPHasChildLearnedAnotherLanguage", c => c.Boolean());
            AddColumn("dbo.Student", "IEPHasChildExperiencedLearned", c => c.Int());
            AddColumn("dbo.Student", "IEPIsChildEligibleBilingualProgram", c => c.Boolean());
            AddColumn("dbo.Student", "IEPHasChildCurrentlyReceivingServicesBilingualProgram", c => c.Boolean());
            AddColumn("dbo.Student", "IEPHasChildReceivingServicesMigrantEducation", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Student", "IEPHasChildReceivingServicesMigrantEducation");
            DropColumn("dbo.Student", "IEPHasChildCurrentlyReceivingServicesBilingualProgram");
            DropColumn("dbo.Student", "IEPIsChildEligibleBilingualProgram");
            DropColumn("dbo.Student", "IEPHasChildExperiencedLearned");
            DropColumn("dbo.Student", "IEPHasChildLearnedAnotherLanguage");
            DropColumn("dbo.Student", "IEPIsNextEligibilityEvaluation");
            DropColumn("dbo.Student", "IEPIsExpirationDate");
            DropColumn("dbo.Student", "IEPHasChildFormally");
            DropColumn("dbo.Student", "IEPHasChildReceiving");
            DropColumn("dbo.Student", "IEPVisualImpairment");
            DropColumn("dbo.Student", "IEPOtherHealthImpairment");
            DropColumn("dbo.Student", "IEPMentalRetardation");
            DropColumn("dbo.Student", "IEPEmotionalDisturbance");
            DropColumn("dbo.Student", "IEPDeafness");
            DropColumn("dbo.Student", "IEPSpeechLanguageImpairment");
            DropColumn("dbo.Student", "IEPOrthopedicImpairment");
            DropColumn("dbo.Student", "IEPSpecificLearningDisability");
            DropColumn("dbo.Student", "IEPEarlyChildhoodDevelopmentalDelay");
            DropColumn("dbo.Student", "IEPAutismAsbergerSyndrome");
            DropColumn("dbo.Student", "IEPTraumaticBrainInjury");
            DropColumn("dbo.Student", "IEPMultipleDisability");
            DropColumn("dbo.Student", "IEPHearingImpairment");
            DropColumn("dbo.Student", "IEPDeafBlindness");
            DropColumn("dbo.Student", "IEPGiftedTalented");
            DropColumn("dbo.Student", "IEPHasChildEligible");
        }
    }
}
