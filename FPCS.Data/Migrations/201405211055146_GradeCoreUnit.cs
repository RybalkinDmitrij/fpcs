namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class GradeCoreUnit : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SchoolYear", "PlusBudgetLimit", c => c.Decimal(nullable: false, precision: 18, scale: 2, defaultValue: 0));
            AddColumn("dbo.SchoolYear", "CoreUnitGradeK", c => c.Int(nullable: false, defaultValue: 4));
            AddColumn("dbo.SchoolYear", "CoreUnitGrade1", c => c.Int(nullable: false, defaultValue: 4));
            AddColumn("dbo.SchoolYear", "CoreUnitGrade2", c => c.Int(nullable: false, defaultValue: 4));
            AddColumn("dbo.SchoolYear", "CoreUnitGrade3", c => c.Int(nullable: false, defaultValue: 4));
            AddColumn("dbo.SchoolYear", "CoreUnitGrade4", c => c.Int(nullable: false, defaultValue: 4));
            AddColumn("dbo.SchoolYear", "CoreUnitGrade5", c => c.Int(nullable: false, defaultValue: 4));
            AddColumn("dbo.SchoolYear", "CoreUnitGrade6", c => c.Int(nullable: false, defaultValue: 4));
            AddColumn("dbo.SchoolYear", "CoreUnitGrade7", c => c.Int(nullable: false, defaultValue: 4));
            AddColumn("dbo.SchoolYear", "CoreUnitGrade8", c => c.Int(nullable: false, defaultValue: 4));
            AddColumn("dbo.SchoolYear", "CoreUnitGrade9", c => c.Int(nullable: false, defaultValue: 4));
            AddColumn("dbo.SchoolYear", "CoreUnitGrade10", c => c.Int(nullable: false, defaultValue: 4));
            AddColumn("dbo.SchoolYear", "CoreUnitGrade11", c => c.Int(nullable: false, defaultValue: 4));
            AddColumn("dbo.SchoolYear", "CoreUnitGrade12", c => c.Int(nullable: false, defaultValue: 4));
            AddColumn("dbo.SchoolYear", "CoreUnitGradeG", c => c.Int(nullable: false, defaultValue: 4));
            AddColumn("dbo.SchoolYear", "ElectiveUnitGradeK", c => c.Int(nullable: false, defaultValue: 4));
            AddColumn("dbo.SchoolYear", "ElectiveUnitGrade1", c => c.Int(nullable: false, defaultValue: 4));
            AddColumn("dbo.SchoolYear", "ElectiveUnitGrade2", c => c.Int(nullable: false, defaultValue: 4));
            AddColumn("dbo.SchoolYear", "ElectiveUnitGrade3", c => c.Int(nullable: false, defaultValue: 4));
            AddColumn("dbo.SchoolYear", "ElectiveUnitGrade4", c => c.Int(nullable: false, defaultValue: 4));
            AddColumn("dbo.SchoolYear", "ElectiveUnitGrade5", c => c.Int(nullable: false, defaultValue: 4));
            AddColumn("dbo.SchoolYear", "ElectiveUnitGrade6", c => c.Int(nullable: false, defaultValue: 4));
            AddColumn("dbo.SchoolYear", "ElectiveUnitGrade7", c => c.Int(nullable: false, defaultValue: 4));
            AddColumn("dbo.SchoolYear", "ElectiveUnitGrade8", c => c.Int(nullable: false, defaultValue: 4));
            AddColumn("dbo.SchoolYear", "ElectiveUnitGrade9", c => c.Int(nullable: false, defaultValue: 4));
            AddColumn("dbo.SchoolYear", "ElectiveUnitGrade10", c => c.Int(nullable: false, defaultValue: 4));
            AddColumn("dbo.SchoolYear", "ElectiveUnitGrade11", c => c.Int(nullable: false, defaultValue: 4));
            AddColumn("dbo.SchoolYear", "ElectiveUnitGrade12", c => c.Int(nullable: false, defaultValue: 4));
            AddColumn("dbo.SchoolYear", "ElectiveUnitGradeG", c => c.Int(nullable: false, defaultValue: 4));
        }
        
        public override void Down()
        {
            DropColumn("dbo.SchoolYear", "ElectiveUnitGradeG");
            DropColumn("dbo.SchoolYear", "ElectiveUnitGrade12");
            DropColumn("dbo.SchoolYear", "ElectiveUnitGrade11");
            DropColumn("dbo.SchoolYear", "ElectiveUnitGrade10");
            DropColumn("dbo.SchoolYear", "ElectiveUnitGrade9");
            DropColumn("dbo.SchoolYear", "ElectiveUnitGrade8");
            DropColumn("dbo.SchoolYear", "ElectiveUnitGrade7");
            DropColumn("dbo.SchoolYear", "ElectiveUnitGrade6");
            DropColumn("dbo.SchoolYear", "ElectiveUnitGrade5");
            DropColumn("dbo.SchoolYear", "ElectiveUnitGrade4");
            DropColumn("dbo.SchoolYear", "ElectiveUnitGrade3");
            DropColumn("dbo.SchoolYear", "ElectiveUnitGrade2");
            DropColumn("dbo.SchoolYear", "ElectiveUnitGrade1");
            DropColumn("dbo.SchoolYear", "ElectiveUnitGradeK");
            DropColumn("dbo.SchoolYear", "CoreUnitGradeG");
            DropColumn("dbo.SchoolYear", "CoreUnitGrade12");
            DropColumn("dbo.SchoolYear", "CoreUnitGrade11");
            DropColumn("dbo.SchoolYear", "CoreUnitGrade10");
            DropColumn("dbo.SchoolYear", "CoreUnitGrade9");
            DropColumn("dbo.SchoolYear", "CoreUnitGrade8");
            DropColumn("dbo.SchoolYear", "CoreUnitGrade7");
            DropColumn("dbo.SchoolYear", "CoreUnitGrade6");
            DropColumn("dbo.SchoolYear", "CoreUnitGrade5");
            DropColumn("dbo.SchoolYear", "CoreUnitGrade4");
            DropColumn("dbo.SchoolYear", "CoreUnitGrade3");
            DropColumn("dbo.SchoolYear", "CoreUnitGrade2");
            DropColumn("dbo.SchoolYear", "CoreUnitGrade1");
            DropColumn("dbo.SchoolYear", "CoreUnitGradeK");
            DropColumn("dbo.SchoolYear", "PlusBudgetLimit");
        }
    }
}
