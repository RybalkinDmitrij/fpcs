namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _02_03_2015_00 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SchoolYear", "GeneralSupplies", c => c.Decimal(nullable: false, precision: 18, scale: 7, defaultValue: 0));
        }
        
        public override void Down()
        {
            DropColumn("dbo.SchoolYear", "GeneralSupplies");
        }
    }
}
