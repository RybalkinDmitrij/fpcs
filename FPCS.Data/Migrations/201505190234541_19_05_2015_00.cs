namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _19_05_2015_00 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ASDCourse", "TypeGrade", c => c.Int(nullable: false, defaultValue: 0));

            Sql("UPDATE [dbo].[ASDCourse] SET TypeGrade = 0 WHERE Name = 'Elementary'");
            Sql("UPDATE [dbo].[ASDCourse] SET TypeGrade = 1 WHERE Name = 'Middle'");
            Sql("UPDATE [dbo].[ASDCourse] SET TypeGrade = 2 WHERE Name = 'High School'");
        }
        
        public override void Down()
        {
            DropColumn("dbo.ASDCourse", "TypeGrade");
        }
    }
}
