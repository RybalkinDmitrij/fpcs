namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _26_05_2014_00 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Family", "SchoolYearId", c => c.Int(nullable: false, defaultValue: 1));
            AddColumn("dbo.Admin", "UnqDbUserId", c => c.Guid(nullable: false, defaultValue: Guid.NewGuid()));
            AddColumn("dbo.Admin", "SchoolYearId", c => c.Int(defaultValue: null));
            AddColumn("dbo.Teacher", "UnqDbUserId", c => c.Guid(nullable: false, defaultValue: Guid.NewGuid()));
            AddColumn("dbo.Guardian", "UnqDbUserId", c => c.Guid(nullable: false, defaultValue: Guid.NewGuid()));
            AddColumn("dbo.Guardian", "SchoolYearId", c => c.Int(defaultValue: 1));
            AddColumn("dbo.Student", "UnqDbUserId", c => c.Guid(nullable: false, defaultValue: Guid.NewGuid()));
            AddColumn("dbo.Student", "SchoolYearId", c => c.Int(defaultValue: 1));
            DropIndex("dbo.Teacher", new[] { "SchoolYearId" });
            DropForeignKey("dbo.Teacher", "SchoolYearId", "dbo.SchoolYear");
            DropColumn("dbo.Teacher", "SchoolYearId");
            AddColumn("dbo.Teacher", "SchoolYearId", c => c.Int());
            AddForeignKey("dbo.Family", "SchoolYearId", "dbo.SchoolYear", "SchoolYearId");
            AddForeignKey("dbo.Teacher", "SchoolYearId", "dbo.SchoolYear", "SchoolYearId");
            AddForeignKey("dbo.Admin", "SchoolYearId", "dbo.SchoolYear", "SchoolYearId");
            AddForeignKey("dbo.Guardian", "SchoolYearId", "dbo.SchoolYear", "SchoolYearId");
            AddForeignKey("dbo.Student", "SchoolYearId", "dbo.SchoolYear", "SchoolYearId");
            CreateIndex("dbo.Family", "SchoolYearId");
            CreateIndex("dbo.Teacher", "SchoolYearId");
            CreateIndex("dbo.Admin", "SchoolYearId");
            CreateIndex("dbo.Guardian", "SchoolYearId");
            CreateIndex("dbo.Student", "SchoolYearId");

            Sql("UPDATE [dbo].[Admin] SET [UnqDbUserId] = [DbUserId]");
            Sql("UPDATE [dbo].[Teacher] SET [UnqDbUserId] = [DbUserId]");
            Sql("UPDATE [dbo].[Guardian] SET [UnqDbUserId] = [DbUserId]");
            Sql("UPDATE [dbo].[Student] SET [UnqDbUserId] = [DbUserId]");

            Sql("UPDATE [dbo].[Teacher] SET [SchoolYearId] = 1");
            Sql("UPDATE [dbo].[Guardian] SET [SchoolYearId] = 1");
            Sql("UPDATE [dbo].[Student] SET [SchoolYearId] = 1");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Student", new[] { "SchoolYearId" });
            DropIndex("dbo.Guardian", new[] { "SchoolYearId" });
            DropIndex("dbo.Admin", new[] { "SchoolYearId" });
            DropIndex("dbo.Teacher", new[] { "SchoolYearId" });
            DropIndex("dbo.Family", new[] { "SchoolYearId" });
            DropForeignKey("dbo.Student", "SchoolYearId", "dbo.SchoolYear");
            DropForeignKey("dbo.Guardian", "SchoolYearId", "dbo.SchoolYear");
            DropForeignKey("dbo.Admin", "SchoolYearId", "dbo.SchoolYear");
            DropForeignKey("dbo.Teacher", "SchoolYearId", "dbo.SchoolYear");
            DropForeignKey("dbo.Family", "SchoolYearId", "dbo.SchoolYear");
            DropColumn("dbo.Teacher", "SchoolYearId");
            AddColumn("dbo.Teacher", "SchoolYearId", c => c.Int(nullable: false, defaultValue: 1));
            AddForeignKey("dbo.Teacher", "SchoolYearId", "dbo.SchoolYear", "SchoolYearId");
            CreateIndex("dbo.Teacher", "SchoolYearId");
            DropColumn("dbo.Student", "SchoolYearId");
            DropColumn("dbo.Student", "UnqDbUserId");
            DropColumn("dbo.Guardian", "SchoolYearId");
            DropColumn("dbo.Guardian", "UnqDbUserId");
            DropColumn("dbo.Teacher", "UnqDbUserId");
            DropColumn("dbo.Admin", "SchoolYearId");
            DropColumn("dbo.Admin", "UnqDbUserId");
            DropColumn("dbo.Family", "SchoolYearId");
        }
    }
}
