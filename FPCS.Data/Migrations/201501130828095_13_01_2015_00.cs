namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _13_01_2015_00 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.StudentPacketCourse", "Q11", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "Q12", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "Q13", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "Q21", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "Q22", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "Q23", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "Q31", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "Q32", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "Q33", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "Q41", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "Q42", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "Q43", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.StudentPacketCourse", "Q43");
            DropColumn("dbo.StudentPacketCourse", "Q42");
            DropColumn("dbo.StudentPacketCourse", "Q41");
            DropColumn("dbo.StudentPacketCourse", "Q33");
            DropColumn("dbo.StudentPacketCourse", "Q32");
            DropColumn("dbo.StudentPacketCourse", "Q31");
            DropColumn("dbo.StudentPacketCourse", "Q23");
            DropColumn("dbo.StudentPacketCourse", "Q22");
            DropColumn("dbo.StudentPacketCourse", "Q21");
            DropColumn("dbo.StudentPacketCourse", "Q13");
            DropColumn("dbo.StudentPacketCourse", "Q12");
            DropColumn("dbo.StudentPacketCourse", "Q11");
        }
    }
}
