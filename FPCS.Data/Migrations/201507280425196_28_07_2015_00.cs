namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _28_07_2015_00 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Vendor", "IsDeleted", c => c.Boolean(nullable: false, defaultValue: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Vendor", "IsDeleted");
        }
    }
}
