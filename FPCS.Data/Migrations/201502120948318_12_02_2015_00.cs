namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _12_02_2015_00 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.FPCSCourse", "Location", c => c.String(maxLength: 150));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.FPCSCourse", "Location", c => c.String(nullable: false, maxLength: 150));
        }
    }
}
