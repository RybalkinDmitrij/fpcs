namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ASDCourse_GradCredit : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ASDCourse", "Description", c => c.String(maxLength: 1000));
            AddColumn("dbo.ASDCourse", "GradCredit", c => c.Decimal(nullable: false, precision: 18, scale: 2, defaultValue: 0));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ASDCourse", "GradCredit");
            DropColumn("dbo.ASDCourse", "Description");
        }
    }
}
