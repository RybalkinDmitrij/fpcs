namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _18_05_2015_00 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FPCSCourse", "GradeLevel", c => c.Int(nullable: false, defaultValue: 0));
            AddColumn("dbo.FPCSCourse", "CourseSubjectId", c => c.Int(nullable: false, defaultValue: 0));
        }
        
        public override void Down()
        {
            DropColumn("dbo.FPCSCourse", "CourseSubjectId");
            DropColumn("dbo.FPCSCourse", "GradeLevel");
        }
    }
}
