// <auto-generated />
namespace FPCS.Data.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class GoodServiceApproval2 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(GoodServiceApproval2));
        
        string IMigrationMetadata.Id
        {
            get { return "201404080531091_GoodServiceApproval2"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
