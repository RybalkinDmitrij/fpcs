namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _30_06_2015_00 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.GoodService", "UserRequestId", c => c.Guid());
            AddColumn("dbo.GoodService", "DateRequest", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.GoodService", "DateRequest");
            DropColumn("dbo.GoodService", "UserRequestId");
        }
    }
}
