namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _31_07_2015_01 : DbMigration
    {
        public override void Up()
        {
		  AddColumn("dbo.SchoolYear", "DisableMessage", c => c.String(nullable: false, defaultValue: "Our website is currently under maintenance. We will be back before 08/03/2015"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.SchoolYear", "DisableMessage");
        }
    }
}
