namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ASDCourse_Name : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ASDCourse", "Name", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ASDCourse", "Name");
        }
    }
}
