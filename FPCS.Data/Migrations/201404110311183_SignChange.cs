namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SignChange : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.StudentPacketCourse", "SponsorSignatureId", "dbo.Teacher");
            DropForeignKey("dbo.StudentPacketCourse", "InstructorSignatureId", "dbo.Teacher");
            DropForeignKey("dbo.StudentPacketCourse", "AdminSignatureId", "dbo.Admin");
            DropForeignKey("dbo.GuardiansSignatures", "StudentPacketCourseId", "dbo.StudentPacketCourse");
            DropForeignKey("dbo.GuardiansSignatures", "GuardianId", "dbo.Guardian");
            DropIndex("dbo.StudentPacketCourse", new[] { "SponsorSignatureId" });
            DropIndex("dbo.StudentPacketCourse", new[] { "InstructorSignatureId" });
            DropIndex("dbo.StudentPacketCourse", new[] { "AdminSignatureId" });
            DropIndex("dbo.GuardiansSignatures", new[] { "StudentPacketCourseId" });
            DropIndex("dbo.GuardiansSignatures", new[] { "GuardianId" });
            AddColumn("dbo.StudentPacketCourse", "DateSponsorSignature", c => c.DateTime());
            AddColumn("dbo.StudentPacketCourse", "SponsorSignature", c => c.Int());
            AddColumn("dbo.StudentPacketCourse", "DateInstructorSignature", c => c.DateTime());
            AddColumn("dbo.StudentPacketCourse", "InstructorSignature", c => c.Int());
            AddColumn("dbo.StudentPacketCourse", "DateAdminSignature", c => c.DateTime());
            AddColumn("dbo.StudentPacketCourse", "AdminSignature", c => c.Int());
            AddColumn("dbo.StudentPacketCourse", "DateGuardianSignature", c => c.DateTime());
            AddColumn("dbo.StudentPacketCourse", "GuardianSignature", c => c.Int());
            AddColumn("dbo.StudentPacketCourse", "Guardian_DbUserId", c => c.Guid());
            AddColumn("dbo.StudentPacketCourse", "Teacher_DbUserId", c => c.Guid());
            AddColumn("dbo.StudentPacketCourse", "Teacher_DbUserId1", c => c.Guid());
            AddColumn("dbo.StudentPacketCourse", "Admin_DbUserId", c => c.Guid());
            AddForeignKey("dbo.StudentPacketCourse", "Guardian_DbUserId", "dbo.Guardian", "DbUserId");
            AddForeignKey("dbo.StudentPacketCourse", "Teacher_DbUserId", "dbo.Teacher", "DbUserId");
            AddForeignKey("dbo.StudentPacketCourse", "Teacher_DbUserId1", "dbo.Teacher", "DbUserId");
            AddForeignKey("dbo.StudentPacketCourse", "Admin_DbUserId", "dbo.Admin", "DbUserId");
            CreateIndex("dbo.StudentPacketCourse", "Guardian_DbUserId");
            CreateIndex("dbo.StudentPacketCourse", "Teacher_DbUserId");
            CreateIndex("dbo.StudentPacketCourse", "Teacher_DbUserId1");
            CreateIndex("dbo.StudentPacketCourse", "Admin_DbUserId");
            DropColumn("dbo.StudentPacketCourse", "SponsorSignatureId");
            DropColumn("dbo.StudentPacketCourse", "InstructorSignatureId");
            DropColumn("dbo.StudentPacketCourse", "AdminSignatureId");
            DropTable("dbo.GuardiansSignatures");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.GuardiansSignatures",
                c => new
                    {
                        StudentPacketCourseId = c.Long(nullable: false),
                        GuardianId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.StudentPacketCourseId, t.GuardianId });
            
            AddColumn("dbo.StudentPacketCourse", "AdminSignatureId", c => c.Guid());
            AddColumn("dbo.StudentPacketCourse", "InstructorSignatureId", c => c.Guid());
            AddColumn("dbo.StudentPacketCourse", "SponsorSignatureId", c => c.Guid());
            DropIndex("dbo.StudentPacketCourse", new[] { "Admin_DbUserId" });
            DropIndex("dbo.StudentPacketCourse", new[] { "Teacher_DbUserId1" });
            DropIndex("dbo.StudentPacketCourse", new[] { "Teacher_DbUserId" });
            DropIndex("dbo.StudentPacketCourse", new[] { "Guardian_DbUserId" });
            DropForeignKey("dbo.StudentPacketCourse", "Admin_DbUserId", "dbo.Admin");
            DropForeignKey("dbo.StudentPacketCourse", "Teacher_DbUserId1", "dbo.Teacher");
            DropForeignKey("dbo.StudentPacketCourse", "Teacher_DbUserId", "dbo.Teacher");
            DropForeignKey("dbo.StudentPacketCourse", "Guardian_DbUserId", "dbo.Guardian");
            DropColumn("dbo.StudentPacketCourse", "Admin_DbUserId");
            DropColumn("dbo.StudentPacketCourse", "Teacher_DbUserId1");
            DropColumn("dbo.StudentPacketCourse", "Teacher_DbUserId");
            DropColumn("dbo.StudentPacketCourse", "Guardian_DbUserId");
            DropColumn("dbo.StudentPacketCourse", "GuardianSignature");
            DropColumn("dbo.StudentPacketCourse", "DateGuardianSignature");
            DropColumn("dbo.StudentPacketCourse", "AdminSignature");
            DropColumn("dbo.StudentPacketCourse", "DateAdminSignature");
            DropColumn("dbo.StudentPacketCourse", "InstructorSignature");
            DropColumn("dbo.StudentPacketCourse", "DateInstructorSignature");
            DropColumn("dbo.StudentPacketCourse", "SponsorSignature");
            DropColumn("dbo.StudentPacketCourse", "DateSponsorSignature");
            CreateIndex("dbo.GuardiansSignatures", "GuardianId");
            CreateIndex("dbo.GuardiansSignatures", "StudentPacketCourseId");
            CreateIndex("dbo.StudentPacketCourse", "AdminSignatureId");
            CreateIndex("dbo.StudentPacketCourse", "InstructorSignatureId");
            CreateIndex("dbo.StudentPacketCourse", "SponsorSignatureId");
            AddForeignKey("dbo.GuardiansSignatures", "GuardianId", "dbo.Guardian", "DbUserId", cascadeDelete: true);
            AddForeignKey("dbo.GuardiansSignatures", "StudentPacketCourseId", "dbo.StudentPacketCourse", "StudentPacketCourseId", cascadeDelete: true);
            AddForeignKey("dbo.StudentPacketCourse", "AdminSignatureId", "dbo.Admin", "DbUserId");
            AddForeignKey("dbo.StudentPacketCourse", "InstructorSignatureId", "dbo.Teacher", "DbUserId");
            AddForeignKey("dbo.StudentPacketCourse", "SponsorSignatureId", "dbo.Teacher", "DbUserId");
        }
    }
}
