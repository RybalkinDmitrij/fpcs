namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _26_02_2015_00 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.StudentPacket", "AmpELAComment", c => c.String(maxLength: 500));
            AddColumn("dbo.StudentPacket", "AmpMathComment", c => c.String(maxLength: 500));
        }
        
        public override void Down()
        {
            DropColumn("dbo.StudentPacket", "AmpMathComment");
            DropColumn("dbo.StudentPacket", "AmpELAComment");
        }
    }
}
