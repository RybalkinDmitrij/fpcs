namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _30_05_2014_00 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Vendor", "UnqVendorID", c => c.Int(nullable: false, defaultValue: 0));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Vendor", "UnqVendorID");
        }
    }
}
