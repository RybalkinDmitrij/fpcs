namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _06_08_2015_01 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.FPCSCourse", "ScheduleRepetition", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.FPCSCourse", "ScheduleRepetition", c => c.Int(nullable: false));
        }
    }
}
