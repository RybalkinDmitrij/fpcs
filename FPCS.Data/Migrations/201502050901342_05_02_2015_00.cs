namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _05_02_2015_00 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MonthlyContactLog",
                c => new
                    {
                        MonthlyContactLogId = c.Int(nullable: false, identity: true),
                        FamilyId = c.Long(nullable: false),
                        TeacherId = c.Guid(nullable: false),
                        Date = c.DateTime(nullable: false),
                        Comment = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.MonthlyContactLogId)
                .ForeignKey("dbo.Family", t => t.FamilyId)
                .Index(t => t.FamilyId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.MonthlyContactLog", new[] { "FamilyId" });
            DropForeignKey("dbo.MonthlyContactLog", "FamilyId", "dbo.Family");
            DropTable("dbo.MonthlyContactLog");
        }
    }
}
