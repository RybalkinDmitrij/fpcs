namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _02_06_2014_00 : DbMigration
    {
        public override void Up()
        {
            Sql("DELETE FROM dbo.ServiceVendor");

            AddColumn("dbo.ServiceVendor", "SubjectId", c => c.Int(nullable: false));
            AddColumn("dbo.ServiceVendor", "IsActive", c => c.Boolean(nullable: false));
            AddForeignKey("dbo.ServiceVendor", "SubjectId", "dbo.Subject", "SubjectId");
            CreateIndex("dbo.ServiceVendor", "SubjectId");
            DropColumn("dbo.ServiceVendor", "Type");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ServiceVendor", "Type", c => c.Int(nullable: false));
            DropIndex("dbo.ServiceVendor", new[] { "SubjectId" });
            DropForeignKey("dbo.ServiceVendor", "SubjectId", "dbo.Subject");
            DropColumn("dbo.ServiceVendor", "IsActive");
            DropColumn("dbo.ServiceVendor", "SubjectId");
        }
    }
}
