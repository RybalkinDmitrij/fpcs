namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TeacherSchoolYear : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Teacher", "SchoolYearId", c => c.Int(nullable: false, defaultValue: 1));
            AddColumn("dbo.Teacher", "PerDeimRate", c => c.Decimal(nullable: false, precision: 19, scale: 3));
            AddForeignKey("dbo.Teacher", "SchoolYearId", "dbo.SchoolYear", "SchoolYearId");
            CreateIndex("dbo.Teacher", "SchoolYearId");
            DropColumn("dbo.Teacher", "HourlyRate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Teacher", "HourlyRate", c => c.Decimal(nullable: false, precision: 19, scale: 3));
            DropIndex("dbo.Teacher", new[] { "SchoolYearId" });
            DropForeignKey("dbo.Teacher", "SchoolYearId", "dbo.SchoolYear");
            DropColumn("dbo.Teacher", "PerDeimRate");
            DropColumn("dbo.Teacher", "SchoolYearId");
        }
    }
}
