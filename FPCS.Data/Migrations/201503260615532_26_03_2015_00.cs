namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _26_03_2015_00 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.StudentPacket", "SponsorSignatureSem1", c => c.Int());
            AddColumn("dbo.StudentPacket", "GuardianSignatureSem1", c => c.Int());
            AddColumn("dbo.StudentPacket", "SponsorSignatureSem2", c => c.Int());
            AddColumn("dbo.StudentPacket", "GuardianSignatureSem2", c => c.Int());
            DropColumn("dbo.StudentPacket", "SponsorSignature");
            DropColumn("dbo.StudentPacket", "GuardianSignature");
        }
        
        public override void Down()
        {
            AddColumn("dbo.StudentPacket", "GuardianSignature", c => c.Int());
            AddColumn("dbo.StudentPacket", "SponsorSignature", c => c.Int());
            DropColumn("dbo.StudentPacket", "GuardianSignatureSem2");
            DropColumn("dbo.StudentPacket", "SponsorSignatureSem2");
            DropColumn("dbo.StudentPacket", "GuardianSignatureSem1");
            DropColumn("dbo.StudentPacket", "SponsorSignatureSem1");
        }
    }
}
