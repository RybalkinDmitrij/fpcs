namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixGoodAndServiceApproval : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.GoodServiceBalance", "DateEntered", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.GoodServiceBalance", "DateEntered");
        }
    }
}
