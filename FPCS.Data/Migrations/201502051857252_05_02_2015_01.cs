namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _05_02_2015_01 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Family", "Password", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Family", "Password");
        }
    }
}
