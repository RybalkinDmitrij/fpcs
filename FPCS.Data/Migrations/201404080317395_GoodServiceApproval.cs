namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class GoodServiceApproval : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.GoodServiceApproval",
                c => new
                    {
                        GoodServiceApprovalId = c.Long(nullable: false, identity: true),
                        GoodServiceId = c.Long(nullable: false),
                        GoodOrServiceApproved = c.Int(nullable: false),
                        CommentsRejectReason = c.String(nullable: false),
                        IsClosed = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.GoodServiceApprovalId)
                .ForeignKey("dbo.GoodService", t => t.GoodServiceId)
                .Index(t => t.GoodServiceId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.GoodServiceApproval", new[] { "GoodServiceId" });
            DropForeignKey("dbo.GoodServiceApproval", "GoodServiceId", "dbo.GoodService");
            DropTable("dbo.GoodServiceApproval");
        }
    }
}
