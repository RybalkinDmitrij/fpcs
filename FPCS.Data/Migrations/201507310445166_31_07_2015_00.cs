namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _31_07_2015_00 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SchoolYear", "DisableAccessForParents", c => c.Boolean(nullable: false, defaultValue: true));
		AddColumn("dbo.SchoolYear", "DisableAccessForTeachers", c => c.Boolean(nullable: false, defaultValue: true));
        }
        
        public override void Down()
        {
            DropColumn("dbo.SchoolYear", "DisableAccessForTeachers");
            DropColumn("dbo.SchoolYear", "DisableAccessForParents");
        }
    }
}
