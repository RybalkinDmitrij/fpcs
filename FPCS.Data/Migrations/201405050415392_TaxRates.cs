namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TaxRates : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SchoolYear", "ILPHoursLimit", c => c.Int(nullable: false));
            AddColumn("dbo.SchoolYear", "CountHoursWorkDay", c => c.Decimal(nullable: false, precision: 18, scale: 1));
            AddColumn("dbo.SchoolYear", "GroupLifeInsurance", c => c.Decimal(nullable: false, precision: 18, scale: 1));
            AddColumn("dbo.SchoolYear", "GroupMedicalInsurance", c => c.Decimal(nullable: false, precision: 18, scale: 1));
            AddColumn("dbo.SchoolYear", "WorkersComp", c => c.Decimal(nullable: false, precision: 18, scale: 7));
            AddColumn("dbo.SchoolYear", "Unemployment", c => c.Decimal(nullable: false, precision: 18, scale: 7));
            AddColumn("dbo.SchoolYear", "FICA", c => c.Decimal(nullable: false, precision: 18, scale: 7));
            AddColumn("dbo.SchoolYear", "FICACap", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.SchoolYear", "Medicare", c => c.Decimal(nullable: false, precision: 18, scale: 7));
            AddColumn("dbo.SchoolYear", "TERSBaseContract", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.SchoolYear", "PERSBaseContract", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.SchoolYear", "TERSRetirement", c => c.Decimal(nullable: false, precision: 18, scale: 7));
            AddColumn("dbo.SchoolYear", "PERSRetirement", c => c.Decimal(nullable: false, precision: 18, scale: 7));
        }
        
        public override void Down()
        {
            DropColumn("dbo.SchoolYear", "PERSRetirement");
            DropColumn("dbo.SchoolYear", "TERSRetirement");
            DropColumn("dbo.SchoolYear", "PERSBaseContract");
            DropColumn("dbo.SchoolYear", "TERSBaseContract");
            DropColumn("dbo.SchoolYear", "Medicare");
            DropColumn("dbo.SchoolYear", "FICACap");
            DropColumn("dbo.SchoolYear", "FICA");
            DropColumn("dbo.SchoolYear", "Unemployment");
            DropColumn("dbo.SchoolYear", "WorkersComp");
            DropColumn("dbo.SchoolYear", "GroupMedicalInsurance");
            DropColumn("dbo.SchoolYear", "GroupLifeInsurance");
            DropColumn("dbo.SchoolYear", "CountHoursWorkDay");
            DropColumn("dbo.SchoolYear", "ILPHoursLimit");
        }
    }
}
