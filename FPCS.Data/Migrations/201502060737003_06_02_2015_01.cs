namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _06_02_2015_01 : DbMigration
    {
        public override void Up()
        {
            Sql("UPDATE dbo.StudentPacket SET AmpELA = 0");
            Sql("UPDATE dbo.StudentPacket SET AmpMath = 0");

            AlterColumn("dbo.StudentPacket", "AmpELA", c => c.Int(nullable: false, defaultValue: 0));
            AlterColumn("dbo.StudentPacket", "AmpMath", c => c.Int(nullable: false, defaultValue: 0));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.StudentPacket", "AmpMath", c => c.Int());
            AlterColumn("dbo.StudentPacket", "AmpELA", c => c.Int());

            Sql("UPDATE dbo.StudentPacket SET AmpELA = NULL");
            Sql("UPDATE dbo.StudentPacket SET AmpMath = NULL");
        }
    }
}
