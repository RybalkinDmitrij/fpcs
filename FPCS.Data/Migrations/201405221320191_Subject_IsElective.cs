namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Subject_IsElective : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Subject", "IsElective", c => c.Boolean(nullable: false, defaultValue: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Subject", "IsElective");
        }
    }
}
