namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _11_02_2015_00 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SchoolYear", "BookMaterialFee", c => c.Decimal(nullable: false, defaultValue: 0, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.SchoolYear", "BookMaterialFee");
        }
    }
}
