namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class GuardiansInFPCSCourse : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FPCSCourse", "GuardianId", c => c.Guid());
            AddColumn("dbo.Teacher", "IsGuardian", c => c.Boolean(nullable: false, defaultValue: false));
            AlterColumn("dbo.FPCSCourse", "TeacherId", c => c.Guid());
            AddForeignKey("dbo.FPCSCourse", "GuardianId", "dbo.Guardian", "DbUserId");
            CreateIndex("dbo.FPCSCourse", "GuardianId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.FPCSCourse", new[] { "GuardianId" });
            DropForeignKey("dbo.FPCSCourse", "GuardianId", "dbo.Guardian");
            AlterColumn("dbo.FPCSCourse", "TeacherId", c => c.Guid(nullable: false));
            DropColumn("dbo.Teacher", "IsGuardian");
            DropColumn("dbo.FPCSCourse", "GuardianId");
        }
    }
}
