namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _10_02_2015_01 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FPCSCourse", "VendorId", c => c.Int());
            AddForeignKey("dbo.FPCSCourse", "VendorId", "dbo.Vendor", "VendorID");
            CreateIndex("dbo.FPCSCourse", "VendorId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.FPCSCourse", new[] { "VendorId" });
            DropForeignKey("dbo.FPCSCourse", "VendorId", "dbo.Vendor");
            DropColumn("dbo.FPCSCourse", "VendorId");
        }
    }
}
