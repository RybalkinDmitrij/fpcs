namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _25_06_2015_00 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Vendor", "StreetAddress", c => c.String());
            AlterColumn("dbo.Vendor", "StreetAddressCity", c => c.String());
            AlterColumn("dbo.Vendor", "StreetAddressStateId", c => c.Long());
            AlterColumn("dbo.Vendor", "StreetAddressZipCode", c => c.String());
            AlterColumn("dbo.Vendor", "MailingAddressStateId", c => c.Long());
            AlterColumn("dbo.Vendor", "Phone", c => c.String());
            AlterColumn("dbo.Vendor", "Email", c => c.String());
            AlterColumn("dbo.Vendor", "EmployerIdentification", c => c.String());
            AlterColumn("dbo.Vendor", "VendorUnitType", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Vendor", "VendorUnitType", c => c.Int(nullable: false));
            AlterColumn("dbo.Vendor", "EmployerIdentification", c => c.String(nullable: false));
            AlterColumn("dbo.Vendor", "Email", c => c.String(nullable: false));
            AlterColumn("dbo.Vendor", "Phone", c => c.String(nullable: false));
            AlterColumn("dbo.Vendor", "MailingAddressStateId", c => c.Long(nullable: false));
            AlterColumn("dbo.Vendor", "StreetAddressZipCode", c => c.String(nullable: false));
            AlterColumn("dbo.Vendor", "StreetAddressStateId", c => c.Long(nullable: false));
            AlterColumn("dbo.Vendor", "StreetAddressCity", c => c.String(nullable: false));
            AlterColumn("dbo.Vendor", "StreetAddress", c => c.String(nullable: false));
        }
    }
}
