namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _30_05_2014_01 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Vendor", "IsActive", c => c.Boolean(nullable: false, defaultValue: true));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Vendor", "IsActive");
        }
    }
}
