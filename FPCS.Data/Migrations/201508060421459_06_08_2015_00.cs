namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _06_08_2015_00 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.FPCSCourse", "MinStudentsCount", c => c.Int());
            AlterColumn("dbo.FPCSCourse", "MaxStudentsCount", c => c.Int());
            AlterColumn("dbo.FPCSCourse", "HoursWithAllStudents", c => c.Decimal(precision: 19, scale: 3));
            AlterColumn("dbo.FPCSCourse", "HoursTeacherPlanning", c => c.Decimal(precision: 19, scale: 3));
            AlterColumn("dbo.FPCSCourse", "TotalHours", c => c.Decimal(precision: 19, scale: 3));
            AlterColumn("dbo.FPCSCourse", "EnrolledCount", c => c.Int());
            AlterColumn("dbo.FPCSCourse", "HoursChargedPerStudent", c => c.Decimal(precision: 19, scale: 5));
            AlterColumn("dbo.FPCSCourse", "TeacherCostPerStudent", c => c.Decimal(precision: 19, scale: 5));
            AlterColumn("dbo.FPCSCourse", "TotalCourseCost", c => c.Decimal(precision: 19, scale: 3));
            AlterColumn("dbo.FPCSCourse", "MinGrade", c => c.Int());
            AlterColumn("dbo.FPCSCourse", "MaxGrade", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.FPCSCourse", "MaxGrade", c => c.Int(nullable: false));
            AlterColumn("dbo.FPCSCourse", "MinGrade", c => c.Int(nullable: false));
            AlterColumn("dbo.FPCSCourse", "TotalCourseCost", c => c.Decimal(nullable: false, precision: 19, scale: 3));
            AlterColumn("dbo.FPCSCourse", "TeacherCostPerStudent", c => c.Decimal(nullable: false, precision: 19, scale: 5));
            AlterColumn("dbo.FPCSCourse", "HoursChargedPerStudent", c => c.Decimal(nullable: false, precision: 19, scale: 5));
            AlterColumn("dbo.FPCSCourse", "EnrolledCount", c => c.Int(nullable: false));
            AlterColumn("dbo.FPCSCourse", "TotalHours", c => c.Decimal(nullable: false, precision: 19, scale: 3));
            AlterColumn("dbo.FPCSCourse", "HoursTeacherPlanning", c => c.Decimal(nullable: false, precision: 19, scale: 3));
            AlterColumn("dbo.FPCSCourse", "HoursWithAllStudents", c => c.Decimal(nullable: false, precision: 19, scale: 3));
            AlterColumn("dbo.FPCSCourse", "MaxStudentsCount", c => c.Int(nullable: false));
            AlterColumn("dbo.FPCSCourse", "MinStudentsCount", c => c.Int(nullable: false));
        }
    }
}
