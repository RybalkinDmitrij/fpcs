namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _08_02_2015_00 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.StudentPacket", "SponsorSignature", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.StudentPacket", "SponsorSignature");
        }
    }
}
