namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _26_02_2015_01 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Student", "StateID", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Student", "StateID");
        }
    }
}
