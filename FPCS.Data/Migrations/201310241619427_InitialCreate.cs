namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.StudentPacketCourse",
                c => new
                    {
                        StudentPacketCourseId = c.Long(nullable: false, identity: true),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedDate = c.DateTimeOffset(nullable: false),
                        UpdatedDate = c.DateTimeOffset(nullable: false),
                        StudentPacketId = c.Long(nullable: false),
                        FPCSCourseId = c.Long(nullable: false),
                        SponsorSignatureId = c.Guid(),
                        InstructorSignatureId = c.Guid(),
                        AdminSignatureId = c.Guid(),
                    })
                .PrimaryKey(t => t.StudentPacketCourseId)
                .ForeignKey("dbo.StudentPacket", t => t.StudentPacketId)
                .ForeignKey("dbo.FPCSCourse", t => t.FPCSCourseId)
                .ForeignKey("dbo.Teacher", t => t.SponsorSignatureId)
                .ForeignKey("dbo.Teacher", t => t.InstructorSignatureId)
                .ForeignKey("dbo.Admin", t => t.AdminSignatureId)
                .Index(t => t.StudentPacketId)
                .Index(t => t.FPCSCourseId)
                .Index(t => t.SponsorSignatureId)
                .Index(t => t.InstructorSignatureId)
                .Index(t => t.AdminSignatureId);
            
            CreateTable(
                "dbo.StudentPacket",
                c => new
                    {
                        StudentPacketId = c.Long(nullable: false, identity: true),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedDate = c.DateTimeOffset(nullable: false),
                        UpdatedDate = c.DateTimeOffset(nullable: false),
                        StudentId = c.Guid(nullable: false),
                        SponsorTeacherId = c.Guid(),
                        SchoolYearId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.StudentPacketId)
                .ForeignKey("dbo.Student", t => t.StudentId)
                .ForeignKey("dbo.Teacher", t => t.SponsorTeacherId)
                .ForeignKey("dbo.SchoolYear", t => t.SchoolYearId)
                .Index(t => t.StudentId)
                .Index(t => t.SponsorTeacherId)
                .Index(t => t.SchoolYearId);
            
            CreateTable(
                "dbo.Family",
                c => new
                    {
                        FamilyId = c.Long(nullable: false, identity: true),
                        StateId = c.Long(nullable: false),
                        Name = c.String(nullable: false, maxLength: 150),
                        Description = c.String(),
                        Address = c.String(maxLength: 200),
                        Telephone = c.String(maxLength: 50),
                        City = c.String(maxLength: 70),
                        Country = c.String(maxLength: 70),
                        Zip = c.String(maxLength: 20),
                        Email = c.String(maxLength: 100),
                        IsUseDirectory = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedDate = c.DateTimeOffset(nullable: false),
                        UpdatedDate = c.DateTimeOffset(nullable: false),
                    })
                .PrimaryKey(t => t.FamilyId)
                .ForeignKey("dbo.State", t => t.StateId)
                .Index(t => t.StateId);
            
            CreateTable(
                "dbo.State",
                c => new
                    {
                        StateId = c.Long(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 20),
                        Name = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.StateId);
            
            CreateTable(
                "dbo.FamilyRelationship",
                c => new
                    {
                        FamilyRelationshipId = c.Long(nullable: false, identity: true),
                        FamilyRelationshipType = c.Int(nullable: false),
                        StudentId = c.Guid(nullable: false),
                        GuardianId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.FamilyRelationshipId)
                .ForeignKey("dbo.Guardian", t => t.GuardianId)
                .ForeignKey("dbo.Student", t => t.StudentId)
                .Index(t => t.GuardianId)
                .Index(t => t.StudentId);
            
            CreateTable(
                "dbo.FPCSCourse",
                c => new
                    {
                        FPCSCourseId = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 150),
                        Location = c.String(nullable: false, maxLength: 150),
                        ScheduleComments = c.String(maxLength: 250),
                        RegistrationDeadline = c.DateTimeOffset(nullable: false),
                        ClassStartDate = c.DateTimeOffset(nullable: false),
                        ClassEndDate = c.DateTimeOffset(nullable: false),
                        MinStudentsCount = c.Int(nullable: false),
                        MaxStudentsCount = c.Int(nullable: false),
                        NumberHoursWithStudent = c.Int(nullable: false),
                        NumberHoursTeacherPlanning = c.Int(nullable: false),
                        IsActivated = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedDate = c.DateTimeOffset(nullable: false),
                        UpdatedDate = c.DateTimeOffset(nullable: false),
                        MinGrade = c.Int(nullable: false),
                        MaxGrade = c.Int(nullable: false),
                        ScheduleRepetition = c.Int(nullable: false),
                        ASDCourseId = c.Int(nullable: false),
                        TeacherId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.FPCSCourseId)
                .ForeignKey("dbo.ASDCourse", t => t.ASDCourseId)
                .ForeignKey("dbo.Teacher", t => t.TeacherId)
                .Index(t => t.ASDCourseId)
                .Index(t => t.TeacherId);
            
            CreateTable(
                "dbo.ASDCourse",
                c => new
                    {
                        ASDCourseId = c.Int(nullable: false, identity: true),
                        ExternalASDCourseId = c.String(nullable: false, maxLength: 50),
                        CreatedDate = c.DateTimeOffset(nullable: false),
                        UpdatedDate = c.DateTimeOffset(nullable: false),
                        IsActivated = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        SubjectId = c.Int(nullable: false),
                        SchoolYearId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ASDCourseId)
                .ForeignKey("dbo.Subject", t => t.SubjectId)
                .ForeignKey("dbo.SchoolYear", t => t.SchoolYearId)
                .Index(t => t.SubjectId)
                .Index(t => t.SchoolYearId);
            
            CreateTable(
                "dbo.Subject",
                c => new
                    {
                        SubjectId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Description = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.SubjectId);
            
            CreateTable(
                "dbo.SchoolYear",
                c => new
                    {
                        SchoolYearId = c.Int(nullable: false, identity: true),
                        Year = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 50),
                        StartDate = c.DateTimeOffset(nullable: false),
                        EndDate = c.DateTimeOffset(nullable: false),
                        HalfDate = c.DateTimeOffset(nullable: false),
                        FundingAmountGradeK = c.Int(nullable: false),
                        FundingAmountGrade1 = c.Int(nullable: false),
                        FundingAmountGrade2 = c.Int(nullable: false),
                        FundingAmountGrade3 = c.Int(nullable: false),
                        FundingAmountGrade4 = c.Int(nullable: false),
                        FundingAmountGrade5 = c.Int(nullable: false),
                        FundingAmountGrade6 = c.Int(nullable: false),
                        FundingAmountGrade7 = c.Int(nullable: false),
                        FundingAmountGrade8 = c.Int(nullable: false),
                        FundingAmountGrade9 = c.Int(nullable: false),
                        FundingAmountGrade10 = c.Int(nullable: false),
                        FundingAmountGrade11 = c.Int(nullable: false),
                        FundingAmountGrade12 = c.Int(nullable: false),
                        FundingAmountGradeG = c.Int(nullable: false),
                        CreatedDate = c.DateTimeOffset(nullable: false),
                        UpdatedDate = c.DateTimeOffset(nullable: false),
                    })
                .PrimaryKey(t => t.SchoolYearId);
            
            CreateTable(
                "dbo.CourseScheduleOnDay",
                c => new
                    {
                        CourseScheduleOnDayId = c.Long(nullable: false, identity: true),
                        StartTime = c.Time(nullable: false),
                        EndTime = c.Time(nullable: false),
                        DayOfWeek = c.Int(nullable: false),
                        FPCSCourseId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.CourseScheduleOnDayId)
                .ForeignKey("dbo.FPCSCourse", t => t.FPCSCourseId)
                .Index(t => t.FPCSCourseId);
            
            CreateTable(
                "dbo.FamilyGuardian",
                c => new
                    {
                        GuardianId = c.Guid(nullable: false),
                        FamilyId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.GuardianId, t.FamilyId })
                .ForeignKey("dbo.Guardian", t => t.GuardianId, cascadeDelete: true)
                .ForeignKey("dbo.Family", t => t.FamilyId, cascadeDelete: true)
                .Index(t => t.GuardianId)
                .Index(t => t.FamilyId);
            
            CreateTable(
                "dbo.FPCSCoursesRestrictions",
                c => new
                    {
                        FPCSCourseId = c.Long(nullable: false),
                        FamilyId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.FPCSCourseId, t.FamilyId })
                .ForeignKey("dbo.FPCSCourse", t => t.FPCSCourseId, cascadeDelete: true)
                .ForeignKey("dbo.Family", t => t.FamilyId, cascadeDelete: true)
                .Index(t => t.FPCSCourseId)
                .Index(t => t.FamilyId);
            
            CreateTable(
                "dbo.FamilyStudent",
                c => new
                    {
                        StudentId = c.Guid(nullable: false),
                        FamilyId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.StudentId, t.FamilyId })
                .ForeignKey("dbo.Student", t => t.StudentId, cascadeDelete: true)
                .ForeignKey("dbo.Family", t => t.FamilyId, cascadeDelete: true)
                .Index(t => t.StudentId)
                .Index(t => t.FamilyId);
            
            CreateTable(
                "dbo.GuardiansSignatures",
                c => new
                    {
                        StudentPacketCourseId = c.Long(nullable: false),
                        GuardianId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.StudentPacketCourseId, t.GuardianId })
                .ForeignKey("dbo.StudentPacketCourse", t => t.StudentPacketCourseId, cascadeDelete: true)
                .ForeignKey("dbo.Guardian", t => t.GuardianId, cascadeDelete: true)
                .Index(t => t.StudentPacketCourseId)
                .Index(t => t.GuardianId);
            
            CreateTable(
                "dbo.Admin",
                c => new
                    {
                        DbUserId = c.Guid(nullable: false, identity: true),
                        Login = c.String(nullable: false, maxLength: 100),
                        Password = c.String(nullable: false, maxLength: 100),
                        Email = c.String(maxLength: 100),
                        FirstName = c.String(nullable: false, maxLength: 100),
                        LastName = c.String(nullable: false, maxLength: 100),
                        FullName = c.String(nullable: false, maxLength: 200),
                        MiddleInitial = c.String(maxLength: 50),
                        IsLocked = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedDate = c.DateTimeOffset(nullable: false),
                        UpdatedDate = c.DateTimeOffset(nullable: false),
                        Role = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.DbUserId);
            
            CreateTable(
                "dbo.Teacher",
                c => new
                    {
                        DbUserId = c.Guid(nullable: false, identity: true),
                        Login = c.String(nullable: false, maxLength: 100),
                        Password = c.String(nullable: false, maxLength: 100),
                        Email = c.String(maxLength: 100),
                        FirstName = c.String(nullable: false, maxLength: 100),
                        LastName = c.String(nullable: false, maxLength: 100),
                        FullName = c.String(nullable: false, maxLength: 200),
                        MiddleInitial = c.String(maxLength: 50),
                        IsLocked = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedDate = c.DateTimeOffset(nullable: false),
                        UpdatedDate = c.DateTimeOffset(nullable: false),
                        Role = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.DbUserId);
            
            CreateTable(
                "dbo.Guardian",
                c => new
                    {
                        DbUserId = c.Guid(nullable: false, identity: true),
                        Login = c.String(nullable: false, maxLength: 100),
                        Password = c.String(nullable: false, maxLength: 100),
                        Email = c.String(maxLength: 100),
                        FirstName = c.String(nullable: false, maxLength: 100),
                        LastName = c.String(nullable: false, maxLength: 100),
                        FullName = c.String(nullable: false, maxLength: 200),
                        MiddleInitial = c.String(maxLength: 50),
                        IsLocked = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedDate = c.DateTimeOffset(nullable: false),
                        UpdatedDate = c.DateTimeOffset(nullable: false),
                        Role = c.Int(nullable: false),
                        StateId = c.Long(),
                        Employer = c.String(maxLength: 100),
                        IsActiveMilitary = c.Boolean(),
                        Rank = c.String(maxLength: 100),
                        Pager = c.String(maxLength: 100),
                        BusinessPhone = c.String(maxLength: 50),
                        Ext = c.String(maxLength: 20),
                        CellPhone = c.String(maxLength: 50),
                        Address = c.String(maxLength: 200),
                        City = c.String(maxLength: 70),
                        Country = c.String(maxLength: 70),
                        Zip = c.String(maxLength: 20),
                    })
                .PrimaryKey(t => t.DbUserId)
                .ForeignKey("dbo.State", t => t.StateId)
                .Index(t => t.StateId);
            
            CreateTable(
                "dbo.Student",
                c => new
                    {
                        DbUserId = c.Guid(nullable: false, identity: true),
                        Login = c.String(nullable: false, maxLength: 100),
                        Password = c.String(nullable: false, maxLength: 100),
                        Email = c.String(maxLength: 100),
                        FirstName = c.String(nullable: false, maxLength: 100),
                        LastName = c.String(nullable: false, maxLength: 100),
                        FullName = c.String(nullable: false, maxLength: 200),
                        MiddleInitial = c.String(maxLength: 50),
                        IsLocked = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedDate = c.DateTimeOffset(nullable: false),
                        UpdatedDate = c.DateTimeOffset(nullable: false),
                        Role = c.Int(nullable: false),
                        GradYear = c.Int(nullable: false),
                        DateOfBirth = c.DateTimeOffset(nullable: false),
                        WithdrawalDate = c.DateTimeOffset(),
                        PrivateSchoolName = c.String(maxLength: 150),
                        PercentInSchoolDistrict = c.Int(nullable: false),
                        IsGraduateFromFPCS = c.Boolean(nullable: false),
                        IsASDContractHoursExemption = c.Boolean(nullable: false),
                        ReasonASDContractHoursExemption = c.String(maxLength: 250),
                        Sex = c.Int(nullable: false),
                        Grade = c.Int(nullable: false),
                        EnrollmentStatus = c.Int(),
                    })
                .PrimaryKey(t => t.DbUserId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Guardian", new[] { "StateId" });
            DropIndex("dbo.GuardiansSignatures", new[] { "GuardianId" });
            DropIndex("dbo.GuardiansSignatures", new[] { "StudentPacketCourseId" });
            DropIndex("dbo.FamilyStudent", new[] { "FamilyId" });
            DropIndex("dbo.FamilyStudent", new[] { "StudentId" });
            DropIndex("dbo.FPCSCoursesRestrictions", new[] { "FamilyId" });
            DropIndex("dbo.FPCSCoursesRestrictions", new[] { "FPCSCourseId" });
            DropIndex("dbo.FamilyGuardian", new[] { "FamilyId" });
            DropIndex("dbo.FamilyGuardian", new[] { "GuardianId" });
            DropIndex("dbo.CourseScheduleOnDay", new[] { "FPCSCourseId" });
            DropIndex("dbo.ASDCourse", new[] { "SchoolYearId" });
            DropIndex("dbo.ASDCourse", new[] { "SubjectId" });
            DropIndex("dbo.FPCSCourse", new[] { "TeacherId" });
            DropIndex("dbo.FPCSCourse", new[] { "ASDCourseId" });
            DropIndex("dbo.FamilyRelationship", new[] { "StudentId" });
            DropIndex("dbo.FamilyRelationship", new[] { "GuardianId" });
            DropIndex("dbo.Family", new[] { "StateId" });
            DropIndex("dbo.StudentPacket", new[] { "SchoolYearId" });
            DropIndex("dbo.StudentPacket", new[] { "SponsorTeacherId" });
            DropIndex("dbo.StudentPacket", new[] { "StudentId" });
            DropIndex("dbo.StudentPacketCourse", new[] { "AdminSignatureId" });
            DropIndex("dbo.StudentPacketCourse", new[] { "InstructorSignatureId" });
            DropIndex("dbo.StudentPacketCourse", new[] { "SponsorSignatureId" });
            DropIndex("dbo.StudentPacketCourse", new[] { "FPCSCourseId" });
            DropIndex("dbo.StudentPacketCourse", new[] { "StudentPacketId" });
            DropForeignKey("dbo.Guardian", "StateId", "dbo.State");
            DropForeignKey("dbo.GuardiansSignatures", "GuardianId", "dbo.Guardian");
            DropForeignKey("dbo.GuardiansSignatures", "StudentPacketCourseId", "dbo.StudentPacketCourse");
            DropForeignKey("dbo.FamilyStudent", "FamilyId", "dbo.Family");
            DropForeignKey("dbo.FamilyStudent", "StudentId", "dbo.Student");
            DropForeignKey("dbo.FPCSCoursesRestrictions", "FamilyId", "dbo.Family");
            DropForeignKey("dbo.FPCSCoursesRestrictions", "FPCSCourseId", "dbo.FPCSCourse");
            DropForeignKey("dbo.FamilyGuardian", "FamilyId", "dbo.Family");
            DropForeignKey("dbo.FamilyGuardian", "GuardianId", "dbo.Guardian");
            DropForeignKey("dbo.CourseScheduleOnDay", "FPCSCourseId", "dbo.FPCSCourse");
            DropForeignKey("dbo.ASDCourse", "SchoolYearId", "dbo.SchoolYear");
            DropForeignKey("dbo.ASDCourse", "SubjectId", "dbo.Subject");
            DropForeignKey("dbo.FPCSCourse", "TeacherId", "dbo.Teacher");
            DropForeignKey("dbo.FPCSCourse", "ASDCourseId", "dbo.ASDCourse");
            DropForeignKey("dbo.FamilyRelationship", "StudentId", "dbo.Student");
            DropForeignKey("dbo.FamilyRelationship", "GuardianId", "dbo.Guardian");
            DropForeignKey("dbo.Family", "StateId", "dbo.State");
            DropForeignKey("dbo.StudentPacket", "SchoolYearId", "dbo.SchoolYear");
            DropForeignKey("dbo.StudentPacket", "SponsorTeacherId", "dbo.Teacher");
            DropForeignKey("dbo.StudentPacket", "StudentId", "dbo.Student");
            DropForeignKey("dbo.StudentPacketCourse", "AdminSignatureId", "dbo.Admin");
            DropForeignKey("dbo.StudentPacketCourse", "InstructorSignatureId", "dbo.Teacher");
            DropForeignKey("dbo.StudentPacketCourse", "SponsorSignatureId", "dbo.Teacher");
            DropForeignKey("dbo.StudentPacketCourse", "FPCSCourseId", "dbo.FPCSCourse");
            DropForeignKey("dbo.StudentPacketCourse", "StudentPacketId", "dbo.StudentPacket");
            DropTable("dbo.Student");
            DropTable("dbo.Guardian");
            DropTable("dbo.Teacher");
            DropTable("dbo.Admin");
            DropTable("dbo.GuardiansSignatures");
            DropTable("dbo.FamilyStudent");
            DropTable("dbo.FPCSCoursesRestrictions");
            DropTable("dbo.FamilyGuardian");
            DropTable("dbo.CourseScheduleOnDay");
            DropTable("dbo.SchoolYear");
            DropTable("dbo.Subject");
            DropTable("dbo.ASDCourse");
            DropTable("dbo.FPCSCourse");
            DropTable("dbo.FamilyRelationship");
            DropTable("dbo.State");
            DropTable("dbo.Family");
            DropTable("dbo.StudentPacket");
            DropTable("dbo.StudentPacketCourse");
        }
    }
}
