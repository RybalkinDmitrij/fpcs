namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _17_02_2015_00 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Vendor", "FirstName", c => c.String(maxLength: 150));
            AlterColumn("dbo.Vendor", "LastName", c => c.String(maxLength: 150));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Vendor", "LastName", c => c.String(nullable: false, maxLength: 150));
            AlterColumn("dbo.Vendor", "FirstName", c => c.String(nullable: false, maxLength: 150));
        }
    }
}
