namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSemester : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FPCSCourse", "Semester", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.FPCSCourse", "Semester");
        }
    }
}
