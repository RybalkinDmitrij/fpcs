namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _15_07_2015_00 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.GoodService", "TempId", c => c.Guid());
        }
        
        public override void Down()
        {
            DropColumn("dbo.GoodService", "TempId");
        }
    }
}
