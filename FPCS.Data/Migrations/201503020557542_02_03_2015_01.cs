namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _02_03_2015_01 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.GoodService", "StudentPacketId", c => c.Long());
            AddForeignKey("dbo.GoodService", "StudentPacketId", "dbo.StudentPacket", "StudentPacketId");
            CreateIndex("dbo.GoodService", "StudentPacketId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.GoodService", new[] { "StudentPacketId" });
            DropForeignKey("dbo.GoodService", "StudentPacketId", "dbo.StudentPacket");
            DropColumn("dbo.GoodService", "StudentPacketId");
        }
    }
}
