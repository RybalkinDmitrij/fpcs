namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _07_08_2015_01 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Vendor", "CourseDescription", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Vendor", "CourseDescription");
        }
    }
}
