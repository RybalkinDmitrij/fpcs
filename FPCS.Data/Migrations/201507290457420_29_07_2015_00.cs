namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _29_07_2015_00 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.StudentPacket", "DateSponsorSignatureSem1", c => c.DateTime());
            AddColumn("dbo.StudentPacket", "DateGuardianSignatureSem1", c => c.DateTime());
            AddColumn("dbo.StudentPacket", "DateSponsorSignatureSem2", c => c.DateTime());
            AddColumn("dbo.StudentPacket", "DateGuardianSignatureSem2", c => c.DateTime());
            AddColumn("dbo.StudentPacket", "DateSponsorSignatureSummer", c => c.DateTime());
            AddColumn("dbo.StudentPacket", "DateGuardianSignatureSummer", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.StudentPacket", "DateGuardianSignatureSummer");
            DropColumn("dbo.StudentPacket", "DateSponsorSignatureSummer");
            DropColumn("dbo.StudentPacket", "DateGuardianSignatureSem2");
            DropColumn("dbo.StudentPacket", "DateSponsorSignatureSem2");
            DropColumn("dbo.StudentPacket", "DateGuardianSignatureSem1");
            DropColumn("dbo.StudentPacket", "DateSponsorSignatureSem1");
        }
    }
}
