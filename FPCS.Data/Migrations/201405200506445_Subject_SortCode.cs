namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Subject_SortCode : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Subject", "Code", c => c.Int(nullable: false, defaultValue: 0));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Subject", "Code");
        }
    }
}
