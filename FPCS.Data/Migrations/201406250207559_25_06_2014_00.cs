namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _25_06_2014_00 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ILPBank",
                c => new
                    {
                        ILPBankId = c.Int(nullable: false, identity: true),
                        ILPName = c.String(nullable: false),
                        SubjectId = c.Int(nullable: false),
                        TeacherId = c.Guid(),
                        GuardianId = c.Guid(),
                        IsPublic = c.Boolean(nullable: false),
                        CourseHrs = c.Int(nullable: false),
                        DescriptionCourse = c.String(maxLength: 700),
                        Standards = c.String(maxLength: 700),
                        StudentActivities = c.String(maxLength: 700),
                        MaterialsResources = c.String(maxLength: 700),
                        RoleAnyPeople = c.String(maxLength: 700),
                        EvaluationGradingPassFail = c.Boolean(nullable: false),
                        EvaluationGradingGradingScale = c.Boolean(nullable: false),
                        EvaluationGradingOther = c.Boolean(nullable: false),
                        EvaluationGradingOtherExplain = c.String(maxLength: 700),
                        EvaluatedMeasurableOutcomes = c.String(maxLength: 700),
                        CourseSyllabus = c.String(maxLength: 700),
                        GuardianILPModifications = c.String(maxLength: 700),
                        InstructorILPModifications = c.String(maxLength: 700),
                    })
                .PrimaryKey(t => t.ILPBankId)
                .ForeignKey("dbo.Subject", t => t.SubjectId)
                .ForeignKey("dbo.Teacher", t => t.TeacherId)
                .ForeignKey("dbo.Guardian", t => t.GuardianId)
                .Index(t => t.SubjectId)
                .Index(t => t.TeacherId)
                .Index(t => t.GuardianId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.ILPBank", new[] { "GuardianId" });
            DropIndex("dbo.ILPBank", new[] { "TeacherId" });
            DropIndex("dbo.ILPBank", new[] { "SubjectId" });
            DropForeignKey("dbo.ILPBank", "GuardianId", "dbo.Guardian");
            DropForeignKey("dbo.ILPBank", "TeacherId", "dbo.Teacher");
            DropForeignKey("dbo.ILPBank", "SubjectId", "dbo.Subject");
            DropTable("dbo.ILPBank");
        }
    }
}
