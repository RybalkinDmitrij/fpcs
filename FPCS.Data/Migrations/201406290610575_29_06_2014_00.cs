namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _29_06_2014_00 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ILPBank", "DescriptionCourse", c => c.String());
            AlterColumn("dbo.ILPBank", "Standards", c => c.String());
            AlterColumn("dbo.ILPBank", "StudentActivities", c => c.String());
            AlterColumn("dbo.ILPBank", "MaterialsResources", c => c.String());
            AlterColumn("dbo.ILPBank", "RoleAnyPeople", c => c.String());
            AlterColumn("dbo.ILPBank", "EvaluationGradingOtherExplain", c => c.String());
            AlterColumn("dbo.ILPBank", "EvaluatedMeasurableOutcomes", c => c.String());
            AlterColumn("dbo.ILPBank", "CourseSyllabus", c => c.String());
            AlterColumn("dbo.ILPBank", "GuardianILPModifications", c => c.String());
            AlterColumn("dbo.ILPBank", "InstructorILPModifications", c => c.String());
            AlterColumn("dbo.ILP", "DescriptionCourse", c => c.String());
            AlterColumn("dbo.ILP", "Standards", c => c.String());
            AlterColumn("dbo.ILP", "StudentActivities", c => c.String());
            AlterColumn("dbo.ILP", "MaterialsResources", c => c.String());
            AlterColumn("dbo.ILP", "RoleAnyPeople", c => c.String());
            AlterColumn("dbo.ILP", "EvaluationGradingOtherExplain", c => c.String());
            AlterColumn("dbo.ILP", "EvaluatedMeasurableOutcomes", c => c.String());
            AlterColumn("dbo.ILP", "CourseSyllabus", c => c.String());
            AlterColumn("dbo.ILP", "GuardianILPModifications", c => c.String());
            AlterColumn("dbo.ILP", "InstructorILPModifications", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ILP", "InstructorILPModifications", c => c.String(maxLength: 700));
            AlterColumn("dbo.ILP", "GuardianILPModifications", c => c.String(maxLength: 700));
            AlterColumn("dbo.ILP", "CourseSyllabus", c => c.String(maxLength: 700));
            AlterColumn("dbo.ILP", "EvaluatedMeasurableOutcomes", c => c.String(maxLength: 700));
            AlterColumn("dbo.ILP", "EvaluationGradingOtherExplain", c => c.String(maxLength: 700));
            AlterColumn("dbo.ILP", "RoleAnyPeople", c => c.String(maxLength: 700));
            AlterColumn("dbo.ILP", "MaterialsResources", c => c.String(maxLength: 700));
            AlterColumn("dbo.ILP", "StudentActivities", c => c.String(maxLength: 700));
            AlterColumn("dbo.ILP", "Standards", c => c.String(maxLength: 700));
            AlterColumn("dbo.ILP", "DescriptionCourse", c => c.String(maxLength: 700));
            AlterColumn("dbo.ILPBank", "InstructorILPModifications", c => c.String(maxLength: 700));
            AlterColumn("dbo.ILPBank", "GuardianILPModifications", c => c.String(maxLength: 700));
            AlterColumn("dbo.ILPBank", "CourseSyllabus", c => c.String(maxLength: 700));
            AlterColumn("dbo.ILPBank", "EvaluatedMeasurableOutcomes", c => c.String(maxLength: 700));
            AlterColumn("dbo.ILPBank", "EvaluationGradingOtherExplain", c => c.String(maxLength: 700));
            AlterColumn("dbo.ILPBank", "RoleAnyPeople", c => c.String(maxLength: 700));
            AlterColumn("dbo.ILPBank", "MaterialsResources", c => c.String(maxLength: 700));
            AlterColumn("dbo.ILPBank", "StudentActivities", c => c.String(maxLength: 700));
            AlterColumn("dbo.ILPBank", "Standards", c => c.String(maxLength: 700));
            AlterColumn("dbo.ILPBank", "DescriptionCourse", c => c.String(maxLength: 700));
        }
    }
}
