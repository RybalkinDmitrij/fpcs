namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class StudentPacketCourse_IsLocked : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.StudentPacket", "IsLocked", c => c.Boolean(nullable: false, defaultValue: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.StudentPacket", "IsLocked");
        }
    }
}
