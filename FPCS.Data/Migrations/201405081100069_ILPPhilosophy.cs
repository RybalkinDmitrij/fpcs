namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ILPPhilosophy : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Student", "ILPPhilosophy", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Student", "ILPPhilosophy");
        }
    }
}
