namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _28_05_2014_01 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.StudentPacket", "IsASDTASigned", c => c.Boolean(nullable: false, defaultValue: false));
            AddColumn("dbo.StudentPacket", "ASDTAGuardianId", c => c.Guid());
            AddColumn("dbo.StudentPacket", "IsPRASigned", c => c.Boolean(nullable: false, defaultValue: false));
            AddColumn("dbo.StudentPacket", "PRAGuardianId", c => c.Guid());
            AddForeignKey("dbo.StudentPacket", "ASDTAGuardianId", "dbo.Guardian", "DbUserId");
            AddForeignKey("dbo.StudentPacket", "PRAGuardianId", "dbo.Guardian", "DbUserId");
            CreateIndex("dbo.StudentPacket", "ASDTAGuardianId");
            CreateIndex("dbo.StudentPacket", "PRAGuardianId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.StudentPacket", new[] { "PRAGuardianId" });
            DropIndex("dbo.StudentPacket", new[] { "ASDTAGuardianId" });
            DropForeignKey("dbo.StudentPacket", "PRAGuardianId", "dbo.Guardian");
            DropForeignKey("dbo.StudentPacket", "ASDTAGuardianId", "dbo.Guardian");
            DropColumn("dbo.StudentPacket", "PRAGuardianId");
            DropColumn("dbo.StudentPacket", "IsPRASigned");
            DropColumn("dbo.StudentPacket", "ASDTAGuardianId");
            DropColumn("dbo.StudentPacket", "IsASDTASigned");
        }
    }
}
