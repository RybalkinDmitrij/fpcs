namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _09_03_2015_00 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Student", "EnrollmentDate", c => c.DateTimeOffset());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Student", "EnrollmentDate");
        }
    }
}
