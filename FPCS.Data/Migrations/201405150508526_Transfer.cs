namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Transfer : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Transfer",
                c => new
                    {
                        TransferId = c.Long(nullable: false, identity: true),
                        FromStudentId = c.Guid(nullable: false),
                        ToStudentId = c.Guid(nullable: false),
                        TransferAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Comment = c.String(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        UserCreateName = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.TransferId)
                .ForeignKey("dbo.Student", t => t.FromStudentId)
                .ForeignKey("dbo.Student", t => t.ToStudentId)
                .Index(t => t.FromStudentId)
                .Index(t => t.ToStudentId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Transfer", new[] { "ToStudentId" });
            DropIndex("dbo.Transfer", new[] { "FromStudentId" });
            DropForeignKey("dbo.Transfer", "ToStudentId", "dbo.Student");
            DropForeignKey("dbo.Transfer", "FromStudentId", "dbo.Student");
            DropTable("dbo.Transfer");
        }
    }
}
