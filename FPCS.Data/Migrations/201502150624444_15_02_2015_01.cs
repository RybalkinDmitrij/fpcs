namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _15_02_2015_01 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ILPBank", "EvaluationGradingOSN", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ILPBank", "EvaluationGradingOSN");
        }
    }
}
