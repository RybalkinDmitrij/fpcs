namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Teacher : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Teacher", "Title", c => c.Int(nullable: false));
            AddColumn("dbo.Teacher", "DateBirth", c => c.DateTime(nullable: false));
            AddColumn("dbo.Teacher", "IsActive", c => c.Boolean(nullable: false));
            AddColumn("dbo.Teacher", "MailingAddress", c => c.String(nullable: false));
            AddColumn("dbo.Teacher", "City", c => c.String(nullable: false));
            AddColumn("dbo.Teacher", "StateId", c => c.Long(nullable: false));
            AddColumn("dbo.Teacher", "ZipCode", c => c.String(nullable: false));
            AddColumn("dbo.Teacher", "HomePhone", c => c.String());
            AddColumn("dbo.Teacher", "BusPhone", c => c.String());
            AddColumn("dbo.Teacher", "Ext", c => c.Int(nullable: false));
            AddColumn("dbo.Teacher", "CellPhone", c => c.String());
            AddColumn("dbo.Teacher", "MastersDegree", c => c.Boolean(nullable: false));
            AddColumn("dbo.Teacher", "SSN", c => c.String(nullable: false));
            AddColumn("dbo.Teacher", "SecondEMailAddress", c => c.String());
            AddColumn("dbo.Teacher", "DistrictCode", c => c.String());
            AddColumn("dbo.Teacher", "IsBenefitPaidASDSchool", c => c.Boolean(nullable: false));
            AddColumn("dbo.Teacher", "ASDFTE", c => c.Int(nullable: false));
            AddColumn("dbo.Teacher", "IsNotIncludeBenefits", c => c.Boolean(nullable: false));
            AddColumn("dbo.Teacher", "FTCSFTE", c => c.Int(nullable: false));
            AddColumn("dbo.Teacher", "NameSchool", c => c.String());
            AddColumn("dbo.Teacher", "IsLeaveASD", c => c.Boolean(nullable: false));
            AddColumn("dbo.Teacher", "IsSubtitleTeacher", c => c.Boolean(nullable: false));
            AddColumn("dbo.Teacher", "IsOtherASDEmployee", c => c.Boolean(nullable: false));
            AddColumn("dbo.Teacher", "IsOnASDEligibleToHire", c => c.Boolean(nullable: false));
            AddColumn("dbo.Teacher", "IsRetiredASDTeacher", c => c.Boolean(nullable: false));
            AddColumn("dbo.Teacher", "FPCSFTE", c => c.Int(nullable: false));
            AddColumn("dbo.Teacher", "IsGroupInstruction", c => c.Boolean(nullable: false));
            AddColumn("dbo.Teacher", "IsIndividualInstruction", c => c.Boolean(nullable: false));
            AddColumn("dbo.Teacher", "YearsTeachingExperience", c => c.Int(nullable: false));
            AddColumn("dbo.Teacher", "TeachingCertificateExpirationDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Teacher", "TeachingSalaryPlacement", c => c.String(nullable: false));
            AddColumn("dbo.Teacher", "IsAlaskaCertificationK8", c => c.Boolean(nullable: false));
            AddColumn("dbo.Teacher", "IsAlaskaCertificationK12", c => c.Boolean(nullable: false));
            AddColumn("dbo.Teacher", "IsAlaskaCertificationSpecialEducation", c => c.Boolean(nullable: false));
            AddColumn("dbo.Teacher", "IsAlaskaCertificationSecondary", c => c.Boolean(nullable: false));
            AddColumn("dbo.Teacher", "AlaskaCertificationSubjectGrades", c => c.String());
            AddColumn("dbo.Teacher", "IsInMyClassroom", c => c.Boolean(nullable: false));
            AddColumn("dbo.Teacher", "IsAtMyHome", c => c.Boolean(nullable: false));
            AddColumn("dbo.Teacher", "IsAsStudentsHome", c => c.Boolean(nullable: false));
            AddColumn("dbo.Teacher", "IsAtFPCSClassroom", c => c.Boolean(nullable: false));
            AddColumn("dbo.Teacher", "IsOtherAvailableTeach", c => c.Boolean(nullable: false));
            AddColumn("dbo.Teacher", "OtherAvailableTeach", c => c.String());
            AddColumn("dbo.Teacher", "IsWeekdays", c => c.Boolean(nullable: false));
            AddColumn("dbo.Teacher", "IsWeekdayAfternoons", c => c.Boolean(nullable: false));
            AddColumn("dbo.Teacher", "IsWeekdayEvenings", c => c.Boolean(nullable: false));
            AddColumn("dbo.Teacher", "IsWeekends", c => c.Boolean(nullable: false));
            AddColumn("dbo.Teacher", "IsSummers", c => c.Boolean(nullable: false));
            AddColumn("dbo.Teacher", "FlatRateHour", c => c.Decimal(nullable: false, precision: 19, scale: 3));
            AddColumn("dbo.Teacher", "BasePayHour", c => c.Decimal(nullable: false, precision: 19, scale: 3));
            AddColumn("dbo.Teacher", "PayHourwBenefits", c => c.Decimal(nullable: false, precision: 19, scale: 3));
            AddColumn("dbo.Teacher", "PayType", c => c.Int(nullable: false));
            AddForeignKey("dbo.Teacher", "StateId", "dbo.State", "StateId");
            CreateIndex("dbo.Teacher", "StateId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Teacher", new[] { "StateId" });
            DropForeignKey("dbo.Teacher", "StateId", "dbo.State");
            DropColumn("dbo.Teacher", "PayType");
            DropColumn("dbo.Teacher", "PayHourwBenefits");
            DropColumn("dbo.Teacher", "BasePayHour");
            DropColumn("dbo.Teacher", "FlatRateHour");
            DropColumn("dbo.Teacher", "IsSummers");
            DropColumn("dbo.Teacher", "IsWeekends");
            DropColumn("dbo.Teacher", "IsWeekdayEvenings");
            DropColumn("dbo.Teacher", "IsWeekdayAfternoons");
            DropColumn("dbo.Teacher", "IsWeekdays");
            DropColumn("dbo.Teacher", "OtherAvailableTeach");
            DropColumn("dbo.Teacher", "IsOtherAvailableTeach");
            DropColumn("dbo.Teacher", "IsAtFPCSClassroom");
            DropColumn("dbo.Teacher", "IsAsStudentsHome");
            DropColumn("dbo.Teacher", "IsAtMyHome");
            DropColumn("dbo.Teacher", "IsInMyClassroom");
            DropColumn("dbo.Teacher", "AlaskaCertificationSubjectGrades");
            DropColumn("dbo.Teacher", "IsAlaskaCertificationSecondary");
            DropColumn("dbo.Teacher", "IsAlaskaCertificationSpecialEducation");
            DropColumn("dbo.Teacher", "IsAlaskaCertificationK12");
            DropColumn("dbo.Teacher", "IsAlaskaCertificationK8");
            DropColumn("dbo.Teacher", "TeachingSalaryPlacement");
            DropColumn("dbo.Teacher", "TeachingCertificateExpirationDate");
            DropColumn("dbo.Teacher", "YearsTeachingExperience");
            DropColumn("dbo.Teacher", "IsIndividualInstruction");
            DropColumn("dbo.Teacher", "IsGroupInstruction");
            DropColumn("dbo.Teacher", "FPCSFTE");
            DropColumn("dbo.Teacher", "IsRetiredASDTeacher");
            DropColumn("dbo.Teacher", "IsOnASDEligibleToHire");
            DropColumn("dbo.Teacher", "IsOtherASDEmployee");
            DropColumn("dbo.Teacher", "IsSubtitleTeacher");
            DropColumn("dbo.Teacher", "IsLeaveASD");
            DropColumn("dbo.Teacher", "NameSchool");
            DropColumn("dbo.Teacher", "FTCSFTE");
            DropColumn("dbo.Teacher", "IsNotIncludeBenefits");
            DropColumn("dbo.Teacher", "ASDFTE");
            DropColumn("dbo.Teacher", "IsBenefitPaidASDSchool");
            DropColumn("dbo.Teacher", "DistrictCode");
            DropColumn("dbo.Teacher", "SecondEMailAddress");
            DropColumn("dbo.Teacher", "SSN");
            DropColumn("dbo.Teacher", "MastersDegree");
            DropColumn("dbo.Teacher", "CellPhone");
            DropColumn("dbo.Teacher", "Ext");
            DropColumn("dbo.Teacher", "BusPhone");
            DropColumn("dbo.Teacher", "HomePhone");
            DropColumn("dbo.Teacher", "ZipCode");
            DropColumn("dbo.Teacher", "StateId");
            DropColumn("dbo.Teacher", "City");
            DropColumn("dbo.Teacher", "MailingAddress");
            DropColumn("dbo.Teacher", "IsActive");
            DropColumn("dbo.Teacher", "DateBirth");
            DropColumn("dbo.Teacher", "Title");
        }
    }
}
