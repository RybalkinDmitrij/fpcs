namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _12_02_2015_02 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.StudentPacketCourse", "Sem1Credit", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.StudentPacketCourse", "Sem2Credit", c => c.Decimal(precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.StudentPacketCourse", "Sem2Credit");
            DropColumn("dbo.StudentPacketCourse", "Sem1Credit");
        }
    }
}
