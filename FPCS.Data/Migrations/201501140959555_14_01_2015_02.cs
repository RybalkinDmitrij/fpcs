namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _14_01_2015_02 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Family", "MailingAddress", c => c.String(maxLength: 200));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Family", "MailingAddress");
        }
    }
}
