namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _21_07_2014_00 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ASDCourse", "IsFree", c => c.Boolean(nullable: false, defaultValue: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ASDCourse", "IsFree");
        }
    }
}
