using System;
using System.Linq;
using System.Data.Entity.Migrations;

using FPCS.Data.Entities;
using FPCS.Core.Unity;
using FPCS.Data.Repo;

namespace FPCS.Data.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<StudentManagementContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(StudentManagementContext context)
        {
            UnityManager.Instance.RegisterAllUnitySetups();
            UpdateStates(context);
            UpdateUsers(context);
            UpdateSubject(context);
            //UpdateServiceVendor(context);
        }

        private void UpdateStates(StudentManagementContext context)
        {
            context.Set<State>().AddOrUpdate(
                x => x.Code,
                new State { Code = "AK", Name = "AK" },
                new State { Code = "AL", Name = "AL" },
                new State { Code = "AR", Name = "AR" },
                new State { Code = "AZ", Name = "AZ" },
                new State { Code = "CA", Name = "CA" },
                new State { Code = "CO", Name = "CO" },
                new State { Code = "CT", Name = "CT" },
                new State { Code = "DC", Name = "DC" },
                new State { Code = "DE", Name = "DE" },
                new State { Code = "FL", Name = "FL" });
        }

        private void UpdateUsers(StudentManagementContext context)
        {
            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IAdminRepo>();
                if (!repo.IsExistLogin("fpcs$admin"))
                {
                    repo.Add("fpcs$admin", "admin$fpcs", null, "admin", "admin", null);
                    uow.Commit();
                }
            }
        }

        private void UpdateSubject(StudentManagementContext context)
        {
            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<ISubjectRepo>();
                if (repo.GetAll().Count() == 0)
                {
                    repo.Add("Art", null, 1);
                    repo.Add("Career Tech", null, 2);
                    repo.Add("Computers", null, 3);
                    repo.Add("Elementary", null, 4);
                    repo.Add("KCC", null, 5);
                    repo.Add("Language Arts", null, 6);
                    repo.Add("Language Arts Electives", null, 7);
                    repo.Add("Mathematics", null, 8);
                    repo.Add("Miscellaneous", null, 9);
                    repo.Add("Music", null, 10);
                    repo.Add("Physical Education/ Health", null, 11);
                    repo.Add("ROTC", null, 12);
                    repo.Add("Science", null, 13);
                    repo.Add("Social Studies", null, 14);
                    repo.Add("Sponsorship Plus", null, 15);
                    repo.Add("World Language", null, 16);

                    uow.Commit();
                }
            }
        }

        private void UpdateServiceVendor(StudentManagementContext context)
        {
            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<IServiceVendorRepo>();
                if (repo.GetAll().Count() == 0)
                {
                    #region data

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 1,
                        Name = "CULINARY",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 1,
                        Name = "DANCE",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 1,
                        Name = "DRAMA",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 1,
                        Name = "DRAWING",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 1,
                        Name = "DRILL TEAM",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 1,
                        Name = "JEWELRY",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 1,
                        Name = "OTHER ART",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 1,
                        Name = "PAINTING",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 1,
                        Name = "SCULPTURE",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 1,
                        Name = "THEATER",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 3,
                        Name = "INTERNET",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 7,
                        Name = "ACADEMIC-LANGUAGEARTS ELECT",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 7,
                        Name = "INTERNET",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 6,
                        Name = "ACADEMIC-LANGUAGE ARTS",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 6,
                        Name = "INTERNET",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 8,
                        Name = "ACADEMIC-MATH",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 8,
                        Name = "INTERNET",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 9,
                        Name = "INTERNET",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 10,
                        Name = "CLARINET",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 10,
                        Name = "FLUTE",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 10,
                        Name = "GUITAR",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 10,
                        Name = "OTHER MUSIC",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 10,
                        Name = "PERCUSSION",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 10,
                        Name = "PIANO",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 10,
                        Name = "STUDY OF",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 10,
                        Name = "VIOLIN",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 10,
                        Name = "VOCAL",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 11,
                        Name = "BALLET",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 11,
                        Name = "BASEBALL",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 11,
                        Name = "BASKETBALL",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 11,
                        Name = "BOWLING",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 11,
                        Name = "DIVING",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 11,
                        Name = "EQUESTRIAN",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 11,
                        Name = "FOOTBALL",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 11,
                        Name = "GYMNASTICS",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 11,
                        Name = "HOCKEY",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 11,
                        Name = "ICE SKATING",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 11,
                        Name = "KARATE",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 11,
                        Name = "OTHER PE",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 11,
                        Name = "RACQUETBALL",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 11,
                        Name = "SKING",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 11,
                        Name = "SNOWBOARDING",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 11,
                        Name = "SOCCER",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 11,
                        Name = "SWIMMING",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 11,
                        Name = "TENNIS",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 11,
                        Name = "TIA KWAN DO"
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 11,
                        Name = "TRACK",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 11,
                        Name = "VOLLEYBALL",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 11,
                        Name = "WINTER SPORTS",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 11,
                        Name = "WRESTLING",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 13,
                        Name = "ACADEMIC-SCIENCE",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 13,
                        Name = "INTERNET",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 14,
                        Name = "ACADEMIC-SOCIAL STUDIES",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 14,
                        Name = "GASR (CATEGORY A): ACADEMIC-SOCIAL STUDIES",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 14,
                        Name = "GASR (CATEGORY A): INTERNET",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 14,
                        Name = "INTERNET",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 14,
                        Name = "HSSR (CATEGORY B): ACADEMIC-SOCIAL STUDIES",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 14,
                        Name = "HSSR (CATEGORY B): INTERNET",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 15,
                        Name = "INTERNET",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 16,
                        Name = "CHINESE",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 16,
                        Name = "FRENCH",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 16,
                        Name = "GERMAN",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 16,
                        Name = "INTERNET",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 16,
                        Name = "ITALIAN",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 16,
                        Name = "LATIN",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 16,
                        Name = "SIGN LANGUAGE",
                        IsActive = true
                    });

                    repo.Add(new ServiceVendor()
                    {
                        SubjectId = 16,
                        Name = "SPANISH",
                        IsActive = true
                    });

                    #endregion data

                    uow.Commit();
                }
            }
        }
    }
}
