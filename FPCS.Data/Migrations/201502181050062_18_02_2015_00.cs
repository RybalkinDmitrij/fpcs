namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _18_02_2015_00 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FPCSCourse", "SubjectId", c => c.Int());
            AddForeignKey("dbo.FPCSCourse", "SubjectId", "dbo.Subject", "SubjectId");
            CreateIndex("dbo.FPCSCourse", "SubjectId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.FPCSCourse", new[] { "SubjectId" });
            DropForeignKey("dbo.FPCSCourse", "SubjectId", "dbo.Subject");
            DropColumn("dbo.FPCSCourse", "SubjectId");
        }
    }
}
