namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _07_08_2015_00 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ILP", "Instructor", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ILP", "Instructor");
        }
    }
}
