namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateFPCSCourseCost : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FPCSCourse", "HoursWithAllStudents", c => c.Decimal(nullable: false, precision: 19, scale: 3));
            AddColumn("dbo.FPCSCourse", "HoursTeacherPlanning", c => c.Decimal(nullable: false, precision: 19, scale: 3));
            AddColumn("dbo.FPCSCourse", "TotalHours", c => c.Decimal(nullable: false, precision: 19, scale: 3));
            AddColumn("dbo.FPCSCourse", "EnrolledCount", c => c.Int(nullable: false));
            AddColumn("dbo.FPCSCourse", "HoursChargedPerStudent", c => c.Decimal(nullable: false, precision: 19, scale: 5));
            AddColumn("dbo.FPCSCourse", "TeacherCostPerStudent", c => c.Decimal(nullable: false, precision: 19, scale: 5));
            AddColumn("dbo.FPCSCourse", "TotalCourseCost", c => c.Decimal(nullable: false, precision: 19, scale: 3));
            AddColumn("dbo.Teacher", "HourlyRate", c => c.Decimal(nullable: false, precision: 19, scale: 3));
            DropColumn("dbo.FPCSCourse", "NumberHoursWithStudent");
            DropColumn("dbo.FPCSCourse", "NumberHoursTeacherPlanning");
        }
        
        public override void Down()
        {
            AddColumn("dbo.FPCSCourse", "NumberHoursTeacherPlanning", c => c.Int(nullable: false));
            AddColumn("dbo.FPCSCourse", "NumberHoursWithStudent", c => c.Int(nullable: false));
            DropColumn("dbo.Teacher", "HourlyRate");
            DropColumn("dbo.FPCSCourse", "TotalCourseCost");
            DropColumn("dbo.FPCSCourse", "TeacherCostPerStudent");
            DropColumn("dbo.FPCSCourse", "HoursChargedPerStudent");
            DropColumn("dbo.FPCSCourse", "EnrolledCount");
            DropColumn("dbo.FPCSCourse", "TotalHours");
            DropColumn("dbo.FPCSCourse", "HoursTeacherPlanning");
            DropColumn("dbo.FPCSCourse", "HoursWithAllStudents");
        }
    }
}
