namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class GoodsAndServices : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Vendor",
                c => new
                    {
                        VendorID = c.Int(nullable: false, identity: true),
                        Type = c.Int(nullable: false),
                        BusinessName = c.String(nullable: false, maxLength: 150),
                        FirstName = c.String(nullable: false, maxLength: 150),
                        LastName = c.String(nullable: false, maxLength: 150),
                        Status = c.Int(),
                        StatusComments = c.String(),
                        StreetAddress = c.String(nullable: false),
                        StreetAddressCity = c.String(nullable: false),
                        StreetAddressStateId = c.Long(nullable: false),
                        StreetAddressZipCode = c.String(nullable: false),
                        MailingAddress = c.String(),
                        MailingAddressCity = c.String(),
                        MailingAddressStateId = c.Long(nullable: false),
                        MailingAddressZipCode = c.String(),
                        Phone = c.String(nullable: false),
                        Fax = c.String(),
                        Email = c.String(nullable: false),
                        Location = c.Int(),
                        BusinessWebsite = c.String(),
                        EmployerIdentification = c.String(nullable: false),
                        AKBusinessLicense = c.String(),
                        LicenseExpiration = c.DateTime(),
                        InsuranceExpiration = c.DateTime(),
                        TrainingEducationExperience = c.String(),
                        CommentsAboutServices = c.String(),
                        VendorUnitType = c.Int(nullable: false),
                        OtherChargeMethod = c.String(),
                        Price = c.Decimal(precision: 18, scale: 2),
                        ContractStartingDate = c.DateTime(),
                        IsNonProfit = c.Boolean(nullable: false),
                        IsMisdemeanorOrFelony = c.Boolean(nullable: false),
                        IsBackgroundCheckFingerprinting = c.Boolean(nullable: false),
                        IsHaveChildrenCurrentlyEnrolled = c.Boolean(nullable: false),
                        IsRetiredCertificatedASDTeacher = c.Boolean(nullable: false),
                        IsASDEmployeeEligibleHireList = c.Boolean(nullable: false),
                        IsCurrentlyAvailableProvideServices = c.Boolean(nullable: false),
                        IsProvideOnlineCurriculumServices = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.VendorID)
                .ForeignKey("dbo.State", t => t.StreetAddressStateId)
                .ForeignKey("dbo.State", t => t.MailingAddressStateId)
                .Index(t => t.StreetAddressStateId)
                .Index(t => t.MailingAddressStateId);
            
            CreateTable(
                "dbo.ServiceVendor",
                c => new
                    {
                        ServiceVendorId = c.Int(nullable: false, identity: true),
                        Type = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 150),
                    })
                .PrimaryKey(t => t.ServiceVendorId);
            
            CreateTable(
                "dbo.GoodService",
                c => new
                    {
                        GoodServiceId = c.Long(nullable: false, identity: true),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedDate = c.DateTimeOffset(nullable: false),
                        UpdatedDate = c.DateTimeOffset(nullable: false),
                        StudentPacketCourseId = c.Long(),
                        FPCSCourseId = c.Long(),
                        GoodOrService = c.Int(nullable: false),
                        RequisitionOrReimbursement = c.Int(nullable: false),
                        TypeRequisitionReimbursement = c.Int(nullable: false),
                        VendorId = c.Int(nullable: false),
                        AdminComments = c.String(),
                        ApprovalGivenBy = c.Int(),
                        NumberOfUnits = c.Int(),
                        UnitPrice = c.Decimal(precision: 18, scale: 2),
                        ShippingHandlingFees = c.Decimal(precision: 18, scale: 2),
                        LineItemName = c.String(),
                        LineItemDesc = c.String(),
                        SupplyType = c.Int(),
                        Count = c.Int(),
                        CatalogMFG = c.String(),
                        CatalogIssuePG = c.String(),
                        UnitType = c.Int(),
                        BarCode = c.String(),
                        Consumable = c.Int(),
                        TypeTextbookCurriculum = c.Int(),
                        Title = c.String(),
                        Author = c.String(),
                        PublisherISBN = c.String(),
                        PublisherName = c.String(),
                        Edition = c.String(),
                        HighSchool = c.Int(),
                        NumberSemesters = c.Int(),
                        CostPerSemester = c.Decimal(precision: 18, scale: 2),
                        ClassType = c.Int(),
                        CourseTitle = c.String(),
                        CourseNumber = c.String(),
                        DateBirth = c.DateTime(),
                        Credits = c.Int(),
                        Semester = c.Int(),
                        TypeService = c.Int(),
                        Description = c.String(),
                        BeginDate = c.DateTime(),
                        EndDate = c.DateTime(),
                        VendorUnitType = c.Int(),
                        ComputerLeaseItemType = c.Int(),
                        ComputerLeaseItemDesc = c.Int(),
                        DetailedItemDesc = c.String(),
                        InternetItemType = c.Int(),
                    })
                .PrimaryKey(t => t.GoodServiceId)
                .ForeignKey("dbo.StudentPacketCourse", t => t.StudentPacketCourseId)
                .ForeignKey("dbo.FPCSCourse", t => t.FPCSCourseId)
                .ForeignKey("dbo.Vendor", t => t.VendorId)
                .Index(t => t.StudentPacketCourseId)
                .Index(t => t.FPCSCourseId)
                .Index(t => t.VendorId);
            
            CreateTable(
                "dbo.ServiceBelongVendor",
                c => new
                    {
                        VendorId = c.Int(nullable: false),
                        ServiceVendorId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.VendorId, t.ServiceVendorId })
                .ForeignKey("dbo.Vendor", t => t.VendorId, cascadeDelete: true)
                .ForeignKey("dbo.ServiceVendor", t => t.ServiceVendorId, cascadeDelete: true)
                .Index(t => t.VendorId)
                .Index(t => t.ServiceVendorId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.ServiceBelongVendor", new[] { "ServiceVendorId" });
            DropIndex("dbo.ServiceBelongVendor", new[] { "VendorId" });
            DropIndex("dbo.GoodService", new[] { "VendorId" });
            DropIndex("dbo.GoodService", new[] { "FPCSCourseId" });
            DropIndex("dbo.GoodService", new[] { "StudentPacketCourseId" });
            DropIndex("dbo.Vendor", new[] { "MailingAddressStateId" });
            DropIndex("dbo.Vendor", new[] { "StreetAddressStateId" });
            DropForeignKey("dbo.ServiceBelongVendor", "ServiceVendorId", "dbo.ServiceVendor");
            DropForeignKey("dbo.ServiceBelongVendor", "VendorId", "dbo.Vendor");
            DropForeignKey("dbo.GoodService", "VendorId", "dbo.Vendor");
            DropForeignKey("dbo.GoodService", "FPCSCourseId", "dbo.FPCSCourse");
            DropForeignKey("dbo.GoodService", "StudentPacketCourseId", "dbo.StudentPacketCourse");
            DropForeignKey("dbo.Vendor", "MailingAddressStateId", "dbo.State");
            DropForeignKey("dbo.Vendor", "StreetAddressStateId", "dbo.State");
            DropTable("dbo.ServiceBelongVendor");
            DropTable("dbo.GoodService");
            DropTable("dbo.ServiceVendor");
            DropTable("dbo.Vendor");
        }
    }
}
