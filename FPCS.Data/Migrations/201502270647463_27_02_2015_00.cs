namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _27_02_2015_00 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.StudentPacket", "A504", c => c.Boolean(nullable: false, defaultValue: false));
            AddColumn("dbo.StudentPacket", "SWD", c => c.Boolean(nullable: false, defaultValue: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.StudentPacket", "SWD");
            DropColumn("dbo.StudentPacket", "A504");
        }
    }
}
