namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class GoodServiceApproval2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.GoodServiceApproval", "GoodServiceId", "dbo.GoodService");
            DropIndex("dbo.GoodServiceApproval", new[] { "GoodServiceId" });
            CreateTable(
                "dbo.GoodServiceBalance",
                c => new
                    {
                        GoodServiceBalanceId = c.Long(nullable: false, identity: true),
                        GoodServiceId = c.Long(nullable: false),
                        PO = c.String(),
                        Invoice = c.String(),
                        Check = c.String(),
                        CheckDate = c.DateTime(),
                        Payee = c.String(),
                        ReceiptDate = c.DateTime(),
                        Description = c.String(),
                        UnitPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        QTY = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Shipping = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.GoodServiceBalanceId)
                .ForeignKey("dbo.GoodService", t => t.GoodServiceId)
                .Index(t => t.GoodServiceId);
            
            AddColumn("dbo.GoodService", "GoodOrServiceApproved", c => c.Int());
            AddColumn("dbo.GoodService", "CommentsRejectReason", c => c.String());
            AddColumn("dbo.GoodService", "IsClosed", c => c.Boolean());
            DropTable("dbo.GoodServiceApproval");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.GoodServiceApproval",
                c => new
                    {
                        GoodServiceApprovalId = c.Long(nullable: false, identity: true),
                        GoodServiceId = c.Long(nullable: false),
                        GoodOrServiceApproved = c.Int(nullable: false),
                        CommentsRejectReason = c.String(nullable: false),
                        IsClosed = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.GoodServiceApprovalId);
            
            DropIndex("dbo.GoodServiceBalance", new[] { "GoodServiceId" });
            DropForeignKey("dbo.GoodServiceBalance", "GoodServiceId", "dbo.GoodService");
            DropColumn("dbo.GoodService", "IsClosed");
            DropColumn("dbo.GoodService", "CommentsRejectReason");
            DropColumn("dbo.GoodService", "GoodOrServiceApproved");
            DropTable("dbo.GoodServiceBalance");
            CreateIndex("dbo.GoodServiceApproval", "GoodServiceId");
            AddForeignKey("dbo.GoodServiceApproval", "GoodServiceId", "dbo.GoodService", "GoodServiceId");
        }
    }
}
