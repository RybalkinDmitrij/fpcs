namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class StudentPacketCourseAlert : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.StudentPacketCourseAlert",
                c => new
                    {
                        StudentPacketCourseAlertId = c.Long(nullable: false, identity: true),
                        StudentPacketCourseId = c.Long(nullable: false),
                        DbUserId = c.Guid(nullable: false),
                        Value = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.StudentPacketCourseAlertId)
                .ForeignKey("dbo.StudentPacketCourse", t => t.StudentPacketCourseId)
                .Index(t => t.StudentPacketCourseId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.StudentPacketCourseAlert", new[] { "StudentPacketCourseId" });
            DropForeignKey("dbo.StudentPacketCourseAlert", "StudentPacketCourseId", "dbo.StudentPacketCourse");
            DropTable("dbo.StudentPacketCourseAlert");
        }
    }
}
