namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _22_05_2015_00 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.StudentPacket", "SponsorSignatureSummer", c => c.Int());
            AddColumn("dbo.StudentPacket", "GuardianSignatureSummer", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.StudentPacket", "GuardianSignatureSummer");
            DropColumn("dbo.StudentPacket", "SponsorSignatureSummer");
        }
    }
}
