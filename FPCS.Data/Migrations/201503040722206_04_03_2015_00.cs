namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _04_03_2015_00 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.StudentPacket", "GuardianSignature", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.StudentPacket", "GuardianSignature");
        }
    }
}
