namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Signaturer : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.StudentPacketCourse", "Guardian_DbUserId", "dbo.Guardian");
            DropForeignKey("dbo.StudentPacketCourse", "Teacher_DbUserId", "dbo.Teacher");
            DropForeignKey("dbo.StudentPacketCourse", "Teacher_DbUserId1", "dbo.Teacher");
            DropForeignKey("dbo.StudentPacketCourse", "Admin_DbUserId", "dbo.Admin");
            DropIndex("dbo.StudentPacketCourse", new[] { "Guardian_DbUserId" });
            DropIndex("dbo.StudentPacketCourse", new[] { "Teacher_DbUserId" });
            DropIndex("dbo.StudentPacketCourse", new[] { "Teacher_DbUserId1" });
            DropIndex("dbo.StudentPacketCourse", new[] { "Admin_DbUserId" });
            AddColumn("dbo.StudentPacketCourse", "SponsorId", c => c.Guid());
            AddColumn("dbo.StudentPacketCourse", "InstructorId", c => c.Guid());
            AddColumn("dbo.StudentPacketCourse", "AdminId", c => c.Guid());
            AddColumn("dbo.StudentPacketCourse", "GuardianId", c => c.Guid());
            AddForeignKey("dbo.StudentPacketCourse", "SponsorId", "dbo.Teacher", "DbUserId");
            AddForeignKey("dbo.StudentPacketCourse", "InstructorId", "dbo.Teacher", "DbUserId");
            AddForeignKey("dbo.StudentPacketCourse", "AdminId", "dbo.Admin", "DbUserId");
            AddForeignKey("dbo.StudentPacketCourse", "GuardianId", "dbo.Guardian", "DbUserId");
            CreateIndex("dbo.StudentPacketCourse", "SponsorId");
            CreateIndex("dbo.StudentPacketCourse", "InstructorId");
            CreateIndex("dbo.StudentPacketCourse", "AdminId");
            CreateIndex("dbo.StudentPacketCourse", "GuardianId");
            DropColumn("dbo.StudentPacketCourse", "Guardian_DbUserId");
            DropColumn("dbo.StudentPacketCourse", "Teacher_DbUserId");
            DropColumn("dbo.StudentPacketCourse", "Teacher_DbUserId1");
            DropColumn("dbo.StudentPacketCourse", "Admin_DbUserId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.StudentPacketCourse", "Admin_DbUserId", c => c.Guid());
            AddColumn("dbo.StudentPacketCourse", "Teacher_DbUserId1", c => c.Guid());
            AddColumn("dbo.StudentPacketCourse", "Teacher_DbUserId", c => c.Guid());
            AddColumn("dbo.StudentPacketCourse", "Guardian_DbUserId", c => c.Guid());
            DropIndex("dbo.StudentPacketCourse", new[] { "GuardianId" });
            DropIndex("dbo.StudentPacketCourse", new[] { "AdminId" });
            DropIndex("dbo.StudentPacketCourse", new[] { "InstructorId" });
            DropIndex("dbo.StudentPacketCourse", new[] { "SponsorId" });
            DropForeignKey("dbo.StudentPacketCourse", "GuardianId", "dbo.Guardian");
            DropForeignKey("dbo.StudentPacketCourse", "AdminId", "dbo.Admin");
            DropForeignKey("dbo.StudentPacketCourse", "InstructorId", "dbo.Teacher");
            DropForeignKey("dbo.StudentPacketCourse", "SponsorId", "dbo.Teacher");
            DropColumn("dbo.StudentPacketCourse", "GuardianId");
            DropColumn("dbo.StudentPacketCourse", "AdminId");
            DropColumn("dbo.StudentPacketCourse", "InstructorId");
            DropColumn("dbo.StudentPacketCourse", "SponsorId");
            CreateIndex("dbo.StudentPacketCourse", "Admin_DbUserId");
            CreateIndex("dbo.StudentPacketCourse", "Teacher_DbUserId1");
            CreateIndex("dbo.StudentPacketCourse", "Teacher_DbUserId");
            CreateIndex("dbo.StudentPacketCourse", "Guardian_DbUserId");
            AddForeignKey("dbo.StudentPacketCourse", "Admin_DbUserId", "dbo.Admin", "DbUserId");
            AddForeignKey("dbo.StudentPacketCourse", "Teacher_DbUserId1", "dbo.Teacher", "DbUserId");
            AddForeignKey("dbo.StudentPacketCourse", "Teacher_DbUserId", "dbo.Teacher", "DbUserId");
            AddForeignKey("dbo.StudentPacketCourse", "Guardian_DbUserId", "dbo.Guardian", "DbUserId");
        }
    }
}
