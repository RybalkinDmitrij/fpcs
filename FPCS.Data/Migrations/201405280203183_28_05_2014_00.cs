namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _28_05_2014_00 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Transfer", "FromStudentId", c => c.Guid());
            AlterColumn("dbo.Transfer", "ToStudentId", c => c.Guid());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Transfer", "ToStudentId", c => c.Guid(nullable: false));
            AlterColumn("dbo.Transfer", "FromStudentId", c => c.Guid(nullable: false));
        }
    }
}
