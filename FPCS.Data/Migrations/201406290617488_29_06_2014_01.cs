namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _29_06_2014_01 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ILPBank", "SubjectId", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ILPBank", "SubjectId", c => c.Int(nullable: false));
        }
    }
}
