namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _15_07_2015_01 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.GoodService", "TempId", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.GoodService", "TempId", c => c.Guid());
        }
    }
}
