namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _15_02_2015_00 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ILP", "EvaluationGradingOSN", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ILP", "EvaluationGradingOSN");
        }
    }
}
