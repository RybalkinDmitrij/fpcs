// <auto-generated />
namespace FPCS.Data.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class _22_05_2015_01 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(_22_05_2015_01));
        
        string IMigrationMetadata.Id
        {
            get { return "201505220610576_22_05_2015_01"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
