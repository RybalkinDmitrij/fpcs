namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _18_08_2015_00 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FileStore",
                c => new
                    {
                        FileStoreId = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        URL = c.String(nullable: false),
                        ContentSize = c.Long(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        MimeType = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.FileStoreId);
            
            AddColumn("dbo.Vendor", "FileStoreId", c => c.Long());
            AddForeignKey("dbo.Vendor", "FileStoreId", "dbo.FileStore", "FileStoreId");
            CreateIndex("dbo.Vendor", "FileStoreId");
            DropColumn("dbo.Vendor", "CourseDescription");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Vendor", "CourseDescription", c => c.String());
            DropIndex("dbo.Vendor", new[] { "FileStoreId" });
            DropForeignKey("dbo.Vendor", "FileStoreId", "dbo.FileStore");
            DropColumn("dbo.Vendor", "FileStoreId");
            DropTable("dbo.FileStore");
        }
    }
}
