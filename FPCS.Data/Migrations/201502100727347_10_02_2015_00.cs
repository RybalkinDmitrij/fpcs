namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _10_02_2015_00 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Vendor", "IsCourseProvider", c => c.Boolean(nullable: false, defaultValue: false));
            AddColumn("dbo.Vendor", "CostCourseProvider", c => c.Decimal(nullable: false, defaultValue: 0, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Vendor", "CostCourseProvider");
            DropColumn("dbo.Vendor", "IsCourseProvider");
        }
    }
}
