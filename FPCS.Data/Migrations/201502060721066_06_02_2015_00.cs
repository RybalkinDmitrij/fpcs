namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _06_02_2015_00 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.StudentPacket", "AmpELA", c => c.Int());
            AddColumn("dbo.StudentPacket", "AmpMath", c => c.Int());
            AddColumn("dbo.Student", "ZangleID", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Student", "ZangleID");
            DropColumn("dbo.StudentPacket", "AmpMath");
            DropColumn("dbo.StudentPacket", "AmpELA");
        }
    }
}
