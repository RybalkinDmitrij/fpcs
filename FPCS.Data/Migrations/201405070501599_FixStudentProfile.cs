namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixStudentProfile : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Student", "IsBirthCertificate", c => c.Boolean());
            AddColumn("dbo.Student", "IsGradesNotSubmitted", c => c.Boolean());
            AddColumn("dbo.Student", "IsILPPhilosophy", c => c.Boolean());
            AddColumn("dbo.Student", "IsMedicalRelease", c => c.Boolean());
            AddColumn("dbo.Student", "IsProgressReportSignature", c => c.Boolean());
            AddColumn("dbo.Student", "IsShotRecords", c => c.Boolean());
            AddColumn("dbo.Student", "IsTestingAgreement", c => c.Boolean());
            AddColumn("dbo.Student", "IsOther", c => c.Boolean());
            AddColumn("dbo.Student", "PercentagePlanningEnroll", c => c.Int());
            AddColumn("dbo.Student", "IsDoYouPlanGraduate", c => c.Boolean());
            AddColumn("dbo.Student", "CoreCreditExemption", c => c.Int());
            AddColumn("dbo.Student", "CoreCreditExemptionReason", c => c.String());
            AddColumn("dbo.Student", "ElectiveCreditExemption", c => c.Int());
            AddColumn("dbo.Student", "ElectiveCreditExemptionReason", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Student", "ElectiveCreditExemptionReason");
            DropColumn("dbo.Student", "ElectiveCreditExemption");
            DropColumn("dbo.Student", "CoreCreditExemptionReason");
            DropColumn("dbo.Student", "CoreCreditExemption");
            DropColumn("dbo.Student", "IsDoYouPlanGraduate");
            DropColumn("dbo.Student", "PercentagePlanningEnroll");
            DropColumn("dbo.Student", "IsOther");
            DropColumn("dbo.Student", "IsTestingAgreement");
            DropColumn("dbo.Student", "IsShotRecords");
            DropColumn("dbo.Student", "IsProgressReportSignature");
            DropColumn("dbo.Student", "IsMedicalRelease");
            DropColumn("dbo.Student", "IsILPPhilosophy");
            DropColumn("dbo.Student", "IsGradesNotSubmitted");
            DropColumn("dbo.Student", "IsBirthCertificate");
        }
    }
}
