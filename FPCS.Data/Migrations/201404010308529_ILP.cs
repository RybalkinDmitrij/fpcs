namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ILP : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ILP",
                c => new
                    {
                        ILPId = c.Int(nullable: false, identity: true),
                        ILPName = c.String(nullable: false),
                        StudentPacketCourseId = c.Long(),
                        FPCSCourseId = c.Long(),
                        CourseHrs = c.Int(nullable: false),
                        DescriptionCourse = c.String(maxLength: 700),
                        Standards = c.String(maxLength: 700),
                        StudentActivities = c.String(maxLength: 700),
                        MaterialsResources = c.String(maxLength: 700),
                        RoleAnyPeople = c.String(maxLength: 700),
                        EvaluationGradingPassFail = c.Boolean(nullable: false),
                        EvaluationGradingGradingScale = c.Boolean(nullable: false),
                        EvaluationGradingOther = c.Boolean(nullable: false),
                        EvaluationGradingOtherExplain = c.String(maxLength: 700),
                        EvaluatedMeasurableOutcomes = c.String(maxLength: 700),
                        CourseSyllabus = c.String(maxLength: 700),
                        GuardianILPModifications = c.String(maxLength: 700),
                        InstructorILPModifications = c.String(maxLength: 700),
                    })
                .PrimaryKey(t => t.ILPId)
                .ForeignKey("dbo.StudentPacketCourse", t => t.StudentPacketCourseId)
                .ForeignKey("dbo.FPCSCourse", t => t.FPCSCourseId)
                .Index(t => t.StudentPacketCourseId)
                .Index(t => t.FPCSCourseId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.ILP", new[] { "FPCSCourseId" });
            DropIndex("dbo.ILP", new[] { "StudentPacketCourseId" });
            DropForeignKey("dbo.ILP", "FPCSCourseId", "dbo.FPCSCourse");
            DropForeignKey("dbo.ILP", "StudentPacketCourseId", "dbo.StudentPacketCourse");
            DropTable("dbo.ILP");
        }
    }
}
