namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _22_05_2015_01 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.StudentPacketCourse", "Q1IsLock", c => c.Boolean(nullable: false, defaultValue: false));
            AddColumn("dbo.StudentPacketCourse", "Q2IsLock", c => c.Boolean(nullable: false, defaultValue: false));
            AddColumn("dbo.StudentPacketCourse", "Q3IsLock", c => c.Boolean(nullable: false, defaultValue: false));
            AddColumn("dbo.StudentPacketCourse", "Q4IsLock", c => c.Boolean(nullable: false, defaultValue: false));
            AddColumn("dbo.StudentPacketCourse", "Q51", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "Q51GuidFile", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "Q52", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "Q52GuidFile", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "Q53", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "Q53GuidFile", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "Q5", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "Q5Comment", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "Q5IsLock", c => c.Boolean(nullable: false, defaultValue: false));
            AddColumn("dbo.StudentPacketCourse", "Q61", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "Q61GuidFile", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "Q62", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "Q62GuidFile", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "Q63", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "Q63GuidFile", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "Q6", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "Q6Comment", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "Q6IsLock", c => c.Boolean(nullable: false, defaultValue: false));
            AddColumn("dbo.StudentPacketCourse", "Sem3", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "Sem3Comment", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "Sem3Credit", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.StudentPacketCourse", "Sem3IsLock", c => c.Boolean(nullable: false, defaultValue: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.StudentPacketCourse", "Sem3IsLock");
            DropColumn("dbo.StudentPacketCourse", "Sem3Credit");
            DropColumn("dbo.StudentPacketCourse", "Sem3Comment");
            DropColumn("dbo.StudentPacketCourse", "Sem3");
            DropColumn("dbo.StudentPacketCourse", "Q6IsLock");
            DropColumn("dbo.StudentPacketCourse", "Q6Comment");
            DropColumn("dbo.StudentPacketCourse", "Q6");
            DropColumn("dbo.StudentPacketCourse", "Q63GuidFile");
            DropColumn("dbo.StudentPacketCourse", "Q63");
            DropColumn("dbo.StudentPacketCourse", "Q62GuidFile");
            DropColumn("dbo.StudentPacketCourse", "Q62");
            DropColumn("dbo.StudentPacketCourse", "Q61GuidFile");
            DropColumn("dbo.StudentPacketCourse", "Q61");
            DropColumn("dbo.StudentPacketCourse", "Q5IsLock");
            DropColumn("dbo.StudentPacketCourse", "Q5Comment");
            DropColumn("dbo.StudentPacketCourse", "Q5");
            DropColumn("dbo.StudentPacketCourse", "Q53GuidFile");
            DropColumn("dbo.StudentPacketCourse", "Q53");
            DropColumn("dbo.StudentPacketCourse", "Q52GuidFile");
            DropColumn("dbo.StudentPacketCourse", "Q52");
            DropColumn("dbo.StudentPacketCourse", "Q51GuidFile");
            DropColumn("dbo.StudentPacketCourse", "Q51");
            DropColumn("dbo.StudentPacketCourse", "Q4IsLock");
            DropColumn("dbo.StudentPacketCourse", "Q3IsLock");
            DropColumn("dbo.StudentPacketCourse", "Q2IsLock");
            DropColumn("dbo.StudentPacketCourse", "Q1IsLock");
        }
    }
}
