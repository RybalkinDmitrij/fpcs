namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SchoolYearByVendor : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Vendor", "SchoolYearId", c => c.Int(nullable: false, defaultValue: 1));
            AddForeignKey("dbo.Vendor", "SchoolYearId", "dbo.SchoolYear", "SchoolYearId");
            CreateIndex("dbo.Vendor", "SchoolYearId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Vendor", new[] { "SchoolYearId" });
            DropForeignKey("dbo.Vendor", "SchoolYearId", "dbo.SchoolYear");
            DropColumn("dbo.Vendor", "SchoolYearId");
        }
    }
}
