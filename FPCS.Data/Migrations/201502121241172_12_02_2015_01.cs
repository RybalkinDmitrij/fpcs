namespace FPCS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _12_02_2015_01 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.StudentPacketCourse", "Q1Comment", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "Q2Comment", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "Sem1Comment", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "Q3Comment", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "Q4Comment", c => c.String());
            AddColumn("dbo.StudentPacketCourse", "Sem2Comment", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.StudentPacketCourse", "Sem2Comment");
            DropColumn("dbo.StudentPacketCourse", "Q4Comment");
            DropColumn("dbo.StudentPacketCourse", "Q3Comment");
            DropColumn("dbo.StudentPacketCourse", "Sem1Comment");
            DropColumn("dbo.StudentPacketCourse", "Q2Comment");
            DropColumn("dbo.StudentPacketCourse", "Q1Comment");
        }
    }
}
