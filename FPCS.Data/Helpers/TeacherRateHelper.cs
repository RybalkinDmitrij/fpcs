﻿using FPCS.Core.Unity;
using FPCS.Data;
using FPCS.Data.Entities;
using FPCS.Data.Enums;
using FPCS.Data.Helpers.HelperModels;
using FPCS.Data.Repo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FPCS.Data.Helpers
{
    public static class TeacherRateHelper
    {
        public static TaxRate CalcTaxRate(Teacher teacher)
        {
            TaxRate taxRate = new TaxRate();

            Decimal basePayHour = 0;
            Decimal trs = 0;
            Decimal pers = 0;
            Decimal fica = 0;
            Decimal medicare = 0;
            Decimal healthInsurance = 0;
            Decimal workmansComp = 0;
            Decimal lifeInsurance = 0;
            Decimal unemployment = 0;
            Decimal hourlyRateBenefit = 0;
            Decimal perDeimRate = 0;

            using (var uow = UnityManager.Instance.Resolve<IUnitOfWork>())
            {
                var repo = uow.GetRepo<ITeacherRepo>();
                var repoSchoolYear = uow.GetRepo<ISchoolYearRepo>();

                //Teacher teacher = repo.Get(teacherGuid);
                SchoolYear schoolYear = repoSchoolYear.Get(teacher.SchoolYearId);

                basePayHour = Math.Round(schoolYear.CountHoursWorkDay != 0 ? teacher.PerDeimRate / schoolYear.CountHoursWorkDay : 0, 2);
                trs = schoolYear.TERSRetirement * basePayHour;
                pers = schoolYear.PERSRetirement * basePayHour;
                fica = schoolYear.FICA * basePayHour;
                medicare = schoolYear.Medicare * basePayHour;
                healthInsurance = teacher.FPCSFTE != 0 ? 100 * (schoolYear.GroupMedicalInsurance / (1410 * teacher.FPCSFTE)) : 0;
                workmansComp = schoolYear.WorkersComp * basePayHour;
                unemployment = schoolYear.Unemployment * basePayHour;

                if (teacher.PayType == PayType.ADDContract || teacher.PayType == PayType.Contract)
                {
                    lifeInsurance = teacher.FPCSFTE != 0 ? 100 * (schoolYear.GroupLifeInsurance / (1410 * teacher.FPCSFTE)) : 0;
                }

                if (teacher.FTCSFTE >= schoolYear.TERSBaseContract || teacher.ASDFTE >= schoolYear.TERSBaseContract)
                {
                    hourlyRateBenefit = basePayHour + trs + medicare + workmansComp + lifeInsurance + unemployment;

                    taxRate = new TaxRate(basePayHour, trs, 0, 0, medicare, 0, workmansComp, lifeInsurance, unemployment,
                        hourlyRateBenefit, perDeimRate);
                }
                else if ((teacher.FTCSFTE + teacher.FPCSFTE) >= schoolYear.TERSBaseContract && (teacher.FPCSFTE >= 20) &&
                         (teacher.FTCSFTE < schoolYear.TERSBaseContract))
                {
                    hourlyRateBenefit = basePayHour + trs + medicare + healthInsurance + workmansComp + lifeInsurance +
                                        unemployment;

                    taxRate = new TaxRate(basePayHour, trs, 0, 0, medicare, healthInsurance, workmansComp, lifeInsurance,
                        unemployment, hourlyRateBenefit, perDeimRate);
                }
                else if ((teacher.ASDFTE + teacher.FPCSFTE) >= schoolYear.TERSBaseContract && (teacher.FPCSFTE >= 20) &&
                         (teacher.ASDFTE < schoolYear.TERSBaseContract))
                {
                    hourlyRateBenefit = basePayHour + trs + medicare + healthInsurance + workmansComp + lifeInsurance +
                                        unemployment;

                    taxRate = new TaxRate(basePayHour, trs, 0, 0, medicare, healthInsurance, workmansComp, lifeInsurance,
                        unemployment, hourlyRateBenefit, perDeimRate);
                }
                else if (teacher.FPCSFTE < schoolYear.PERSBaseContract)
                {
                    hourlyRateBenefit = basePayHour + fica + medicare + workmansComp + lifeInsurance + unemployment;

                    taxRate = new TaxRate(basePayHour, 0, 0, fica, medicare, 0, workmansComp, lifeInsurance,
                        unemployment, hourlyRateBenefit, perDeimRate);
                }
                else if (teacher.FPCSFTE >= schoolYear.PERSBaseContract && teacher.FPCSFTE < schoolYear.TERSBaseContract)
                {
                    hourlyRateBenefit = basePayHour + pers + fica + medicare + workmansComp + lifeInsurance + unemployment;

                    taxRate = new TaxRate(basePayHour, 0, pers, fica, medicare, 0, workmansComp, lifeInsurance,
                        unemployment, hourlyRateBenefit, perDeimRate);
                }
            }

            return taxRate;
        }
    }
}