﻿using System.Data.Entity;
using FPCS.Data.Migrations;
using Microsoft.Practices.Unity;

using FPCS.Core.Unity;
using FPCS.Data.Repo;
using FPCS.Data.Repo.Impl;

namespace FPCS.Data
{
    public class UnitySetup : IUnitySetup
    {
        public IUnityContainer RegisterTypes(IUnityContainer container)
        {
            container.RegisterType<IUnitOfWork, UnitOfWork<StudentManagementContext>>();

            container.RegisterType<IDbUserRepo, DbUserRepo>();
            container.RegisterType<IAdminRepo, AdminRepo>();
            container.RegisterType<ITeacherRepo, TeacherRepo>();
            container.RegisterType<IGuardianRepo, GuardianRepo>();
            container.RegisterType<IStudentRepo, StudentRepo>();
            container.RegisterType<IFamilyRepo, FamilyRepo>();
            container.RegisterType<IFamilyRelationshipRepo, FamilyRelationshipRepo>(); 
            container.RegisterType<IStateRepo, StateRepo>();
            container.RegisterType<ISchoolYearRepo, SchoolYearRepo>();
            container.RegisterType<ISubjectRepo, SubjectRepo>();
            container.RegisterType<IASDCourseRepo, ASDCourseRepo>();
            container.RegisterType<IFPCSCourseRepo, FPCSCourseRepo>();
            container.RegisterType<ICourseScheduleOnDayRepo, CourseScheduleOnDayRepo>();
            container.RegisterType<IStudentPacketRepo, StudentPacketRepo>();
            container.RegisterType<IStudentPacketCourseRepo, StudentPacketCourseRepo>();
            container.RegisterType<IGoodServiceRepo, GoodServiceRepo>();
            container.RegisterType<IGoodServiceBalanceRepo, GoodServiceBalanceRepo>();
            container.RegisterType<IVendorRepo, VendorRepo>();
            container.RegisterType<IServiceVendorRepo, ServiceVendorRepo>();
            container.RegisterType<IILPRepo, ILPRepo>();
            container.RegisterType<IILPBankRepo, ILPBankRepo>();
            container.RegisterType<IStudentPacketCourseAlertRepo, StudentPacketCourseAlertRepo>();
            container.RegisterType<ITransferRepo, TransferRepo>();
            container.RegisterType<IFileStoreRepo, FileStoreRepo>();

            container.RegisterType<IMonthlyContactLogRepo, MonthlyContactLogRepo>();
            
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<StudentManagementContext, Configuration>());

            return container;
        }
    }
}
