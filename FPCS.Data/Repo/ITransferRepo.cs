﻿using System;
using System.Linq;

using FPCS.Data.Entities;
using System.Linq.Expressions;

namespace FPCS.Data.Repo
{
    public interface ITransferRepo : IRepoBase<Transfer>
    {
        Decimal GetDeposits(Guid studentId);

        Decimal GetWithdrawals(Guid studentId);

        IQueryable<Transfer> GetByFromStudent(Guid studentId);

        IQueryable<Transfer> GetByStudent(Guid studentId);
    }
}
