﻿using System;
using System.Linq;
using System.Collections.Generic;

using FPCS.Data.Entities;

namespace FPCS.Data.Repo
{
    public interface IFamilyRepo : IRepoBase<Family>
    {
        IQueryable<Family> GetAll(Int32 schoolYearId);

        Family Get(Int64 familyId);

        IQueryable<Family> GetAsQuery(Int64 familyId);

        IQueryable<Family> GetFamiliesByGuardian(Guid guardianId);

        IQueryable<Family> GetFamiliesByStudent(Guid studentId);

        ICollection<Guardian> GetGuardiansByStudent(Guid studentId);

        ICollection<Guardian> GetGuardiansByFamily(Int64 familyId);

        List<Guardian> GetRelativesByDbUserId(Guid dbUserId);

        ICollection<Student> GetStudentsByFamily(Int64 familyId);

        List<Student> GetBrotherAndSisterByStudent(Guid studentId);

        Family Add(String name, String description, String address, String mailingAddress, String telephone, String cellPhone, String city, String country,
         String zip, String email, String password, Boolean isUseDirectory, Int64 stateId, Int32 schoolYearId);

        Family Update(Int64 FamilyId, String name, String description, String address, String mailingAddress, String telephone, String cellPhone, String city, String country,
         String zip, String email, String password, Boolean isUseDirectory, Int64 stateId);

        Family Update(Int64 FamilyId, String address, String mailingAddress, String telephone, String cellPhone, String city, String country,
         String zip, Boolean isUseDirectory, Int64 stateId);

        Family UpdateGuardians(Family family, IEnumerable<Guid> dbUserIds);

        Family UpdateStudents(Family family, IEnumerable<Guid> dbUserIds);
    }
}
