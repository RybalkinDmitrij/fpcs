﻿using System;
using System.Linq;
using FPCS.Data.Entities;
using FPCS.Data.Enums;
using System.Collections.Generic;

namespace FPCS.Data.Repo
{
	public interface IVendorRepo : IRepoBase<Vendor>
	{
		Vendor Add(
			VendorType Type,
			String BusinessName,
			String FirstName,
			String LastName,
			VendorStatus? Status,
			String StatusComments,
			String StreetAddress,
			String StreetAddressCity,
			Int64? StreetAddressStateId,
			String StreetAddressZipCode,
			String MailingAddress,
			String MailingAddressCity,
			Int64? MailingAddressStateId,
			String MailingAddressZipCode,
			String Phone,
			String Fax,
			String Email,
			Location? Location,
			String BusinessWebsite,
			String EmployerIdentification,
			String AKBusinessLicense,
			DateTime? LicenseExpiration,
			DateTime? InsuranceExpiration,
			String TrainingEducationExperience,
			String CommentsAboutServices,
			VendorUnitType? VendorUnitType,
			String OtherChargeMethod,
			Decimal? Price,
			DateTime? ContractStartingDate,
			Boolean IsNonProfit,
			Boolean IsMisdemeanorOrFelony,
			Boolean IsBackgroundCheckFingerprinting,
			Boolean IsHaveChildrenCurrentlyEnrolled,
			Boolean IsRetiredCertificatedASDTeacher,
			Boolean IsASDEmployeeEligibleHireList,
			Boolean IsCurrentlyAvailableProvideServices,
			Boolean IsProvideOnlineCurriculumServices,
			Boolean IsCourseProvider,
			Decimal CostCourseProvider,
			Int32 SchoolYearId
			);

		Vendor Update(
			Int32 VendorID,
			VendorType Type,
			String BusinessName,
			String FirstName,
			String LastName,
			VendorStatus? Status,
			String StatusComments,
			String StreetAddress,
			String StreetAddressCity,
			Int64? StreetAddressStateId,
			String StreetAddressZipCode,
			String MailingAddress,
			String MailingAddressCity,
			Int64? MailingAddressStateId,
			String MailingAddressZipCode,
			String Phone,
			String Fax,
			String Email,
			Location? Location,
			String BusinessWebsite,
			String EmployerIdentification,
			String AKBusinessLicense,
			DateTime? LicenseExpiration,
			DateTime? InsuranceExpiration,
			String TrainingEducationExperience,
			String CommentsAboutServices,
			VendorUnitType? VendorUnitType,
			String OtherChargeMethod,
			Decimal? Price,
			DateTime? ContractStartingDate,
			Boolean IsNonProfit,
			Boolean IsMisdemeanorOrFelony,
			Boolean IsBackgroundCheckFingerprinting,
			Boolean IsHaveChildrenCurrentlyEnrolled,
			Boolean IsRetiredCertificatedASDTeacher,
			Boolean IsASDEmployeeEligibleHireList,
			Boolean IsCurrentlyAvailableProvideServices,
			Boolean IsProvideOnlineCurriculumServices,
			Boolean IsCourseProvider,
			Decimal CostCourseProvider,
			Int32 SchoolYearId
			);

		Vendor UpdateServices(Vendor vendor, IEnumerable<Int32> serviceIds);

		IQueryable<Vendor> GetAsQuery(Int64 vendorId);

		IQueryable<Vendor> GetAll(Int32 schoolYearId);

		IQueryable<Vendor> GetCourseProvider(Int32 schoolYearId);

		IQueryable<Vendor> GetForGeneralExpenses(Int32 schoolYearId);

        IQueryable<Vendor> GetWithoutGeneralExpenses(Int32 schoolYearId);

        Decimal GetCourseCost(Int64 vendorId);

        Int64? GetVendorFileStoreId(Int64 vendorId);
	}
}
