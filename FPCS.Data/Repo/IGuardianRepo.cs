﻿using System;
using System.Linq;

using FPCS.Data.Entities;
using System.Collections.Generic;

namespace FPCS.Data.Repo
{
    public interface IGuardianRepo : IRepoBase<Guardian>
    {
        IQueryable<Guardian> GetAll(Int32 schoolYearId);

        Guardian Get(Guid id);

        IQueryable<Guardian> GetWithoutFamily(Int64 familyId, Int32 schoolYearId);

        IQueryable<Guardian> GetByStudent(Guid studentId);

        IQueryable<Guardian> GetByFamilies(Int32 schoolYearID, List<Int64> familyIds);

        Guardian Add(String firstName, String lastName, String middleInitial, String employer,
            Boolean? isActiveMilitary, String rank, String pager, String businessPhone, String ext, String cellPhone,
            String email, String address, String city, String country, String zip, Int64? stateId, Int32 schoolYearId);

        Guardian Update(Guid dbUserId, String firstName, String lastName, String middleInitial, String employer,
            Boolean? isActiveMilitary, String rank, String pager, String businessPhone, String ext, String cellPhone,
            String email, String address, String city, String country, String zip, Int64? stateId);
    }
}
