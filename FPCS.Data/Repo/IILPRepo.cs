﻿using System;

using FPCS.Data.Entities;

namespace FPCS.Data.Repo
{
    public interface IILPRepo : IRepoBase<ILP>
    {
        ILP GetByStudentPacketCourse(Int64 studentPacketCourseId);

        ILP GetByFPCSCourse(Int64 fpcsCourseId);
    }
}
