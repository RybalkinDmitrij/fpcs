﻿using System;
using System.Linq;

using FPCS.Data.Entities;
using FPCS.Data.Enums;

namespace FPCS.Data.Repo
{
    public interface IStudentRepo : IRepoBase<Student>
    {
        IQueryable<Student> GetAll(Int32 schoolYearId);

        Student Get(Guid id);

        IQueryable<Student> GetWithoutFamily(Int64 familyId, Int32 schoolYearId);

        IQueryable<Student> GetByFamily(Int64 familyId, Int32 schoolYearId);

        IQueryable<Student> GetByUser(Guid dbUserId, Role role, Int32 schoolYearId);

        Student Add(String firstName, String lastName, String middleInitial, String email, Int32 gradYear, DateTimeOffset dateOfBirth, DateTimeOffset? withdrawalDate, DateTimeOffset? enrollmentDate,
            String privateSchoolName, Int32 percentInSchoolDistrict, Boolean isGraduateFromFPCS, Boolean isASDContractHoursExemption,
            String reasonASDContractHoursExemption, Sex sex, Grade grade, EnrollmentStatus? enrollmentStatus, String zangleId, Int32? percentagePlanningEnroll,
            Int32? coreCreditExemption, String coreCreditExemptionReason, Int32? electiveCreditExemption, String electiveCreditExemptionReason,
            Boolean isBirthCertificate, Boolean isGradesNotSubmitted, Boolean isILPPhilosophy, Boolean isMedicalRelease, Boolean isProgressReportSignature,
            Boolean isShotRecords, Boolean isTestingAgreement, Boolean isOther, Int32 schoolYearId);

        Student Update(Guid dbUserId, String firstName, String lastName, String middleInitial, String email, Int32 gradYear, DateTimeOffset dateOfBirth,
            DateTimeOffset? withdrawalDate, DateTimeOffset? enrollmentDate, String privateSchoolName, Int32 percentInSchoolDistrict, Boolean isGraduateFromFPCS, Boolean isASDContractHoursExemption,
            String reasonASDContractHoursExemption, Sex sex, Grade grade, EnrollmentStatus? enrollmentStatus, String zangleId, Int32? percentagePlanningEnroll,
            Int32? coreCreditExemption, String coreCreditExemptionReason, Int32? electiveCreditExemption, String electiveCreditExemptionReason,
            Boolean isBirthCertificate, Boolean isGradesNotSubmitted, Boolean isILPPhilosophy, Boolean isMedicalRelease, Boolean isProgressReportSignature,
            Boolean isShotRecords, Boolean isTestingAgreement, Boolean isOther);
    }
}
