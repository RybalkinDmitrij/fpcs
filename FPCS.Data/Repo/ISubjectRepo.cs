﻿using System;

using FPCS.Data.Entities;
using System.Linq;

namespace FPCS.Data.Repo
{
    public interface ISubjectRepo : IRepoBase<Subject>
    {
        Subject Add(String name, String description, Int32 code);

        Subject GetByName(String name);
    }
}
