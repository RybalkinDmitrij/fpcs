﻿using System;
using System.Linq;
using System.Collections.Generic;

using FPCS.Data.Entities;

namespace FPCS.Data.Repo
{
    public interface ICourseScheduleOnDayRepo : IRepoBase<CourseScheduleOnDay>
    {
        IQueryable<CourseScheduleOnDay> GetByFPCSCourse(Int64 fpcsCourseId);

        void UpdateFPCSCourseSchedule(Int64 fpcsCourseId, IEnumerable<CourseScheduleOnDay> courseScheduleOnDays);
    }
}
