﻿using System;

using FPCS.Data.Entities;
using System.Linq;
using FPCS.Data.Enums;

namespace FPCS.Data.Repo
{
    public interface IDbUserRepo : IRepoBase<DbUser>
    {
        IQueryable<DbUser> GetAll(Int32? schoolYearId);

        IQueryable<DbUser> GetByRoles(Int32? schoolYearId, params Role[] roles);

        DbUser Get(Guid id);

        Boolean IsExistLogin(String login, Int32? schoolYearId);

        DbUser GetByLogin(String login, Int32? schoolYearId);

        DbUser GetNotLockedByLogin(String login, Int32? schoolYearId);

        DbUser GetByLoginAndPass(String login, String password, Int32? schoolYearId);

        DbUser GetByEmail(String email, Int32? schoolYearId);

        DbUser Update(Guid dbUserId, String email, String login, String password, Boolean isLocked);
    }
}
