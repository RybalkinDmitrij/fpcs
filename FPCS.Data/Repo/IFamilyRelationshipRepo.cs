﻿using System;
using System.Collections.Generic;

using FPCS.Data.Entities;
using FPCS.Data.Enums;

namespace FPCS.Data.Repo
{
    public interface IFamilyRelationshipRepo : IRepoBase<FamilyRelationship>
    {
        FamilyRelationship Get(Guid guardianDbUserId, Guid studentDbUserId);

        FamilyRelationship Add(Guid guardianDbUserId, Guid studentDbUserId, FamilyRelationshipType familyRelationshipType);

        FamilyRelationship Add(Guardian guardian, Student student, FamilyRelationshipType familyRelationshipType);

        IEnumerable<FamilyRelationship> AddGuardian(Guardian guardian, IEnumerable<Student> students, FamilyRelationshipType familyRelationshipType);

        IEnumerable<FamilyRelationship> AddStudent(Student student, IEnumerable<Guardian> guardians, FamilyRelationshipType familyRelationshipType);

        FamilyRelationship Update(Guid guardianDbUserId, Guid studentDbUserId, FamilyRelationshipType familyRelationshipType);

        FamilyRelationship Update(Int64 studentGuardianId, FamilyRelationshipType familyRelationshipType);

        void RemoveGuardian(Guid dbUserId);

        void RemoveStudent(Guid dbUserId);

        void RemoveGuardian(Guid dbUserId, IEnumerable<Guid> studentDbUserIds);

        void RemoveStudent(Guid dbUserId, IEnumerable<Guid> guardianDbUserIds);

        void RemoveGuardiansStudents(IEnumerable<Guid> guardianDbUserIds, IEnumerable<Guid> studentDbUserIds);
    }
}
