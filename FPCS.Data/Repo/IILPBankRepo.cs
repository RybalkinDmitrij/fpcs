﻿using System;

using FPCS.Data.Entities;
using System.Linq;

namespace FPCS.Data.Repo
{
    public interface IILPBankRepo : IRepoBase<ILPBank>
    {
        ILPBank GetByILP(Int32 ilpId);

        IQueryable<ILPBank> GetAsQuery(Int32 ilpBankId);
    }
}
