﻿using System;
using System.Linq;
using FPCS.Data.Entities;
using FPCS.Data.Enums;

namespace FPCS.Data.Repo
{
	public interface IStudentPacketRepo : IRepoBase<StudentPacket>
	{
		StudentPacket Get(long studentPacketId);

		IQueryable<StudentPacket> GetAsQuery(long studentPacketId);

		IQueryable<StudentPacket> GetBySchoolYear(int schoolYearId);

		StudentPacket Get(Guid studentId, int schoolYearId);

		IQueryable<StudentPacket> GetAsQuery(Guid studentId, int schoolYearId);

		IQueryable<Student> GetBySponsor(Guid sponsorId, int schoolYearId);

		bool IsExist(Guid studentId, int schoolYearId);

		IQueryable<StudentPacket> GetSignedPackets(int schoolYearId);

		StudentPacket AddIsNotExist(Guid studentId, int schoolYearId);

		StudentPacket GetOrAddIsNotExist(Guid studentId, int schoolYearId);

		StudentPacket UpdateSponsorTeacher(long studentPacketId, Guid teacherId);

		StudentPacket UpdateSponsorSignature(long studentPacketId, Sign? signature);
	}
}
