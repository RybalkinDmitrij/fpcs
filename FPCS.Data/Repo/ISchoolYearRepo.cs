﻿using System;

using FPCS.Data.Entities;

namespace FPCS.Data.Repo
{
    public interface ISchoolYearRepo : IRepoBase<SchoolYear>
    {
        SchoolYear GetByYear(Int32 year);

        SchoolYear GetByDate(DateTimeOffset date, Boolean nearestIfNotFound = false);

        SchoolYear GetByASDCourse(Int32 asdCourseId);

        SchoolYear AddCurrentYear();

        SchoolYear AddByYear(Int32 year);

        SchoolYear Add(DateTimeOffset startDate, DateTimeOffset endDate, DateTimeOffset halfDate,
            Int32 fundingAmountGradeK, Int32 fundingAmountGrade1, Int32 fundingAmountGrade2, Int32 fundingAmountGrade3, Int32 fundingAmountGrade4,
            Int32 fundingAmountGrade5, Int32 fundingAmountGrade6, Int32 fundingAmountGrade7, Int32 fundingAmountGrade8, Int32 fundingAmountGrade9,
            Int32 fundingAmountGrade10, Int32 fundingAmountGrade11, Int32 fundingAmountGrade12, Int32 fundingAmountGradeG,
            Int32 coreUnitGradeK, Int32 coreUnitGrade1, Int32 coreUnitGrade2, Int32 coreUnitGrade3, Int32 coreUnitGrade4,
            Int32 coreUnitGrade5, Int32 coreUnitGrade6, Int32 coreUnitGrade7, Int32 coreUnitGrade8, Int32 coreUnitGrade9,
            Int32 coreUnitGrade10, Int32 coreUnitGrade11, Int32 coreUnitGrade12, Int32 coreUnitGradeG,
            Int32 electiveUnitGradeK, Int32 electiveUnitGrade1, Int32 electiveUnitGrade2, Int32 electiveUnitGrade3, Int32 electiveUnitGrade4,
            Int32 electiveUnitGrade5, Int32 electiveUnitGrade6, Int32 electiveUnitGrade7, Int32 electiveUnitGrade8, Int32 electiveUnitGrade9,
            Int32 electiveUnitGrade10, Int32 electiveUnitGrade11, Int32 electiveUnitGrade12, Int32 electiveUnitGradeG,
            Int32 ilpHoursLimit, Decimal countHoursWorkDay, Decimal plusBudgetLimit, Decimal bookMaterialFee, Decimal groupLifeInsurance, Decimal groupMedicalInsurance, Decimal workersComp,
            Decimal unemployment, Decimal fica, Decimal ficaCap, Decimal medicare, Decimal tersBaseContract, Decimal persBaseContract,
		Decimal tersRetirement, Decimal persRetirement, Decimal generalSupplies, Boolean disableAccessForParents, Boolean disableAccessForTeachers, String disableMessage);

        SchoolYear Update(Int32 schoolYearId, DateTimeOffset startDate, DateTimeOffset endDate, DateTimeOffset halfDate,
            Int32 fundingAmountGradeK, Int32 fundingAmountGrade1, Int32 fundingAmountGrade2, Int32 fundingAmountGrade3, Int32 fundingAmountGrade4,
            Int32 fundingAmountGrade5, Int32 fundingAmountGrade6, Int32 fundingAmountGrade7, Int32 fundingAmountGrade8, Int32 fundingAmountGrade9,
            Int32 fundingAmountGrade10, Int32 fundingAmountGrade11, Int32 fundingAmountGrade12, Int32 fundingAmountGradeG,
            Int32 coreUnitGradeK, Int32 coreUnitGrade1, Int32 coreUnitGrade2, Int32 coreUnitGrade3, Int32 coreUnitGrade4,
            Int32 coreUnitGrade5, Int32 coreUnitGrade6, Int32 coreUnitGrade7, Int32 coreUnitGrade8, Int32 coreUnitGrade9,
            Int32 coreUnitGrade10, Int32 coreUnitGrade11, Int32 coreUnitGrade12, Int32 coreUnitGradeG,
            Int32 electiveUnitGradeK, Int32 electiveUnitGrade1, Int32 electiveUnitGrade2, Int32 electiveUnitGrade3, Int32 electiveUnitGrade4,
            Int32 electiveUnitGrade5, Int32 electiveUnitGrade6, Int32 electiveUnitGrade7, Int32 electiveUnitGrade8, Int32 electiveUnitGrade9,
            Int32 electiveUnitGrade10, Int32 electiveUnitGrade11, Int32 electiveUnitGrade12, Int32 electiveUnitGradeG,
            Int32 ilpHoursLimit, Decimal countHoursWorkDay, Decimal plusBudgetLimit, Decimal bookMaterialFee, Decimal groupLifeInsurance, Decimal groupMedicalInsurance, Decimal workersComp,
            Decimal unemployment, Decimal fica, Decimal ficaCap, Decimal medicare, Decimal tersBaseContract, Decimal persBaseContract,
		Decimal tersRetirement, Decimal persRetirement, Decimal generalSupplies, Boolean disableAccessForParents, Boolean disableAccessForTeachers, String disableMessage);
    }
}
