﻿using System;
using System.Linq;
using System.Collections.Generic;

using FPCS.Data.Entities;
using FPCS.Data.Enums;

namespace FPCS.Data.Repo
{
    public interface IFPCSCourseRepo : IRepoBase<FPCSCourse>
    {
        IQueryable<FPCSCourse> GetAll(Int32 schoolYearId);
        
        FPCSCourse Get(Int64 Id);

        IQueryable<FPCSCourse> GetAsQuery(Int64 fpcsCourseId);

        IQueryable<FPCSCourse> GetByIds(List<Int64> fpcsCourseIds);

        IQueryable<FPCSCourse> GetBySchoolYear(Int32 schoolYearId);

        IQueryable<FPCSCourse> GetBySchoolYearAndUser(Int32 schoolYearId, Guid userId);

        IQueryable<FPCSCourse> GetByASDCourse(Int32 asdCourseId);

        IQueryable<FPCSCourse> GetNotSelectedInPacketCourses(Guid studentId, Int64 studentPacketId, Int32 schoolYearId);

        IQueryable<FPCSCourse> GetNotSelectedInPacketCourses(Guid studentId, Int64 studentPacketId, Int32 schoolYearId, Semester semester);

        FPCSCourse Add(Int32 asdCourseId, Guid? teacherId, Guid? guardianId, Int32? vendorId, String name, Int32? gradeLevel, Int32? courseSubjectId, String location, CourseClassType? classType, Int32? subjectId, Boolean isActivated, Semester semester);

        FPCSCourse Update(Int64 fpcsCourseId, Int32 asdCourseId, Guid? teacherId, Guid? guardianId, Int32? vendorId, String name, Int32? gradeLevel, Int32? courseSubjectId, String location, CourseClassType? classType, Int32? subjectId,
             Boolean isActivated, Semester semester);

        FPCSCourse UpdateCourseRestrictions(FPCSCourse course, IEnumerable<Int64> families);

        FPCSCourse UpdateClassCost(Int64 courseId, Int32 schoolYearId);

        FPCSCourse UpdateClassCost(FPCSCourse course, Int32 schoolYearId);

        FPCSCourse UpdateClassCost(FPCSCourse course, Decimal teacherHourlyRate, Int32 schoolYearId);
    }
}
