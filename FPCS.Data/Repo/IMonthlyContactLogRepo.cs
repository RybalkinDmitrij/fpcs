﻿using System;
using System.Linq;
using System.Collections.Generic;

using FPCS.Data.Entities;

namespace FPCS.Data.Repo
{
    public interface IMonthlyContactLogRepo : IRepoBase<MonthlyContactLog>
    {
        IQueryable<MonthlyContactLog> GetByFamily(Int64 familyId);
    }
}
