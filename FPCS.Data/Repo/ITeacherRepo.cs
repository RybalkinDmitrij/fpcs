﻿using System;

using FPCS.Data.Entities;
using System.Linq;

namespace FPCS.Data.Repo
{
    public interface ITeacherRepo : IRepoBase<Teacher>
    {
        IQueryable<Teacher> GetAll(Int32 schoolYearId);

        Teacher Add(Teacher dbEntity);

        Teacher Update(Teacher dbEntity);

        Teacher Get(Guid Id);

        Decimal GetHourlyRate(Guid? Id);

        Decimal GetHourlyRateWithBenefits(Guid? Id);
    }
}
