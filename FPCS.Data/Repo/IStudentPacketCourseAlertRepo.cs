﻿using System;
using System.Linq;

using FPCS.Data.Entities;

namespace FPCS.Data.Repo
{
    public interface IStudentPacketCourseAlertRepo : IRepoBase<StudentPacketCourseAlert>
    {
    }
}
