﻿using System;

using FPCS.Data.Entities;

namespace FPCS.Data.Repo
{
    public interface IFileStoreRepo : IRepoBase<FileStore>
    {
        FileStore Add(String name, String URL, Int64 contentSize, DateTime? dateCreated, String mimeType);

        FileStore Get(Int64 fileStoreId);
    }
}