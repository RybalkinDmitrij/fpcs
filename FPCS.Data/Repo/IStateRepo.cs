﻿using FPCS.Data.Entities;

namespace FPCS.Data.Repo
{
    public interface IStateRepo : IRepoBase<State>
    {
        
    }
}
