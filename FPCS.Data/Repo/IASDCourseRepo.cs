﻿using System;
using System.Linq;

using FPCS.Data.Entities;

namespace FPCS.Data.Repo
{
    public interface IASDCourseRepo : IRepoBase<ASDCourse>
    {
        IQueryable<ASDCourse> GetAll(Int32 schoolYearId);

        ASDCourse Get(Int32 Id);

        IQueryable<ASDCourse> GetAsQuery(Int64 asdCourseId, Int32 schoolYearId);

        ASDCourse GetByExternalASDCourseId(String externalASDCourseId, Int32 schoolYearId);

        IQueryable<ASDCourse> GetBySchoolYear(Int32 schoolYearId);

        IQueryable<ASDCourse> GetActivatedBySchoolYear(Int32 schoolYearId);

        Boolean IsExistExternalASDCourseId(String externalASDCourseId, Int32 schoolYearId);

        ASDCourse Add(String externalASDCourseId, String description, String name, Decimal gradCredit, Int32 subjectId, Int32 schoolYearId, Boolean isActivated, Boolean isFree);

        ASDCourse Update(Int32 asdCourseId, String externalASDCourseId, String description, String name, Decimal gradCredit, Int32 subjectId, Boolean isActivated, Int32 schoolYearId, Boolean isFree);
    }
}
