﻿using System;
using System.Linq;
using System.Collections.Generic;
using FPCS.Data.Entities;

namespace FPCS.Data.Repo
{
	public interface IGoodServiceRepo : IRepoBase<GoodService>
	{
		IQueryable<GoodService> GetByTemp(Guid tempId);

		IQueryable<GoodService> GetByGeneralExpenses(Int64 studentPacketId);

		IQueryable<GoodService> GetByStudentPacket(Int64 studentPacketId);

		IQueryable<GoodService> GetByStudentPacketCourse(Int64 studentPacketCourseId);

		IQueryable<GoodService> GetByFPCSCourse(Int64 fpcsCourseId);

		IQueryable<GoodService> GetByFPCSCourse(List<Int64> fpcsCourseIds);

		IQueryable<GoodService> GetByStudentPacketCourseOrFPCSCourse(Int64 studentPacketCourseId, Int64 fpcsCourseId);

		IQueryable<GoodService> GetByStudentPacketCourseOrFPCSCourseOrStudentPacket(Int64 studentPacketCourseId,
			Int64 fpcsCourseId, Int64 studentPacketId, Guid tempId);

		IQueryable<GoodService> GetByVendorHasStudentPacketCourse(Int64 vendorId);

        IQueryable<GoodService> GetReimbursements(Guid studentId, Int64 studentPacketId);

		IQueryable<GoodService> GetReimbursements(List<Guid> studentIds);

		IQueryable<GoodService> GetByPacket(Int64 packetId);

		IQueryable<GoodService> GetByIds(List<Int64> ids);

		IQueryable<GoodService> GetAsQuery(Int64 id, Boolean isStudentPacketCourse = false);
	}
}
