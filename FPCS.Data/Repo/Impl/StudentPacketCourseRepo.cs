﻿using System;
using System.Linq;
using FPCS.Data.Entities;
using FPCS.Data.Exceptions;
using System.Collections.Generic;
using FPCS.Data.Enums;

namespace FPCS.Data.Repo.Impl
{
	internal class StudentPacketCourseRepo : RepoBase<StudentPacketCourse, StudentManagementContext>,
		IStudentPacketCourseRepo
	{
		public StudentPacketCourseRepo(UnitOfWork<StudentManagementContext> unitOfWork) : base(unitOfWork)
		{
		}

		#region override methods

		public override IQueryable<StudentPacketCourse> GetAll()
		{
			return DbSet.Where(x => !x.IsDeleted &&
							!x.FPCSCourse.IsDeleted &&
							!x.StudentPacket.IsDeleted &&
							!x.StudentPacket.Student.IsDeleted);
		}

		public override void Remove(StudentPacketCourse entity)
		{
			entity.IsDeleted = true;
			entity.UpdatedDate = DateTimeOffset.Now;
			UnitOfWork.Commit();
			UnitOfWork.GetRepo<IFPCSCourseRepo>().UpdateClassCost(entity.FPCSCourseId, entity.StudentPacket.SchoolYearId);
		}

		public override StudentPacketCourse Add(StudentPacketCourse entity)
		{
			var dbEntity = base.Add(entity);
			UnitOfWork.Commit();
			UnitOfWork.GetRepo<IFPCSCourseRepo>().UpdateClassCost(dbEntity.FPCSCourseId, entity.StudentPacket.SchoolYearId);
			return dbEntity;
		}

		#endregion override methods

		public IQueryable<StudentPacketCourse> GetAll(Int32 schoolYearId)
		{
			return GetAll().Where(x => x.StudentPacket.SchoolYearId == schoolYearId &&
			                           x.StudentPacket.Student.SchoolYearId == schoolYearId);
		}

		public StudentPacketCourse Get(Int64 studentPacketCourseId)
		{
			return GetAll().FirstOrDefault(x => x.StudentPacketCourseId == studentPacketCourseId);
		}

		public IQueryable<StudentPacketCourse> GetByStudentPacket(Int64 studentPacketId)
		{
			return GetAll().Where(x => x.StudentPacketId == studentPacketId);
		}

		public IQueryable<StudentPacketCourse> GetByStudent(Guid studentId, Int32 schoolYearId)
		{
			return GetAll().Where(x => x.StudentPacket.StudentId == studentId && x.StudentPacket.SchoolYearId == schoolYearId);
		}

		public IQueryable<StudentPacketCourse> GetByFPCSCourse(Int64 fpcsCourseId, Int32 schoolYearId)
		{
			return GetAll(schoolYearId).Where(x => x.FPCSCourseId == fpcsCourseId);
		}

		public IQueryable<StudentPacketCourse> GetByFPCSCourse(List<Int64> fpcsCourseIds, Int32 schoolYearId)
		{
			return GetAll(schoolYearId).Where(x => fpcsCourseIds.Contains(x.FPCSCourseId));
		}

		public IQueryable<Student> GetStudentsByFPCSCourse(Int64 fpcsCourseId, Int32 schoolYearId)
		{
			return GetAll(schoolYearId).Where(x => x.FPCSCourseId == fpcsCourseId).Select(x => x.StudentPacket.Student);
		}

		public IQueryable<Student> GetStudentsByInstructor(Guid instructorId, Int32 schoolYearId)
		{
			return
				GetAll(schoolYearId)
					.Where(x => x.FPCSCourse.TeacherId == instructorId || x.FPCSCourse.GuardianId == instructorId)
					.Select(x => x.StudentPacket.Student);
		}

		public IQueryable<Student> GetStudentsByInstructorAndFPCSCourse(Guid? instructorId, Int64? fpcsCourseId,
			Int32 schoolYearId)
		{
			if (fpcsCourseId.HasValue && fpcsCourseId.Value == 0)
				fpcsCourseId = null;

			var data = GetAll(schoolYearId)
				.Where(
					x =>
						(!instructorId.HasValue ||
						 (instructorId.HasValue &&
						  (x.FPCSCourse.TeacherId == instructorId || x.FPCSCourse.GuardianId == instructorId))) &&
						(!fpcsCourseId.HasValue || (fpcsCourseId.HasValue && x.FPCSCourseId == fpcsCourseId)))
				.Select(x => x.StudentPacket.Student)
				.ToList();

			if (instructorId.HasValue)
			{
				data.AddRange(UnitOfWork.GetRepo<IStudentPacketRepo>().GetBySponsor(instructorId.Value, schoolYearId).ToList());
			}

			return data.AsQueryable();
		}

		public IQueryable<StudentPacketCourse> GetByInstructor(Guid instructorId, Int32 schoolYearId)
		{
			return GetAll(schoolYearId).Where(x => x.FPCSCourse.TeacherId == instructorId || instructorId == Guid.Empty);
		}

		public IQueryable<StudentPacketCourse> GetByUser(Guid userId, Role role, Int32 schoolYearId)
		{
			return GetAll(schoolYearId)
				.Where(x => role == Role.Admin ||
				            (role == Role.Teacher &&
				             (x.StudentPacket.SponsorTeacherId == userId || x.FPCSCourse.TeacherId == userId)) ||
				            (role == Role.Guardian &&
				             x.StudentPacket.Student.Families.FirstOrDefault() != null &&
				             x.StudentPacket.Student.Families.FirstOrDefault()
					             .Guardians.Select(t => t.DbUserId)
					             .Contains(userId)));
		}

		public StudentPacketCourse Add(Int64 studentPacketId, Int64 fpcsCourseId)
		{
			if (this.IsExist(x => x.StudentPacketId == studentPacketId && x.FPCSCourseId == fpcsCourseId))
				throw new DuplicateEntityException("The Student Packet Course already exist");

			var dbEntity = new StudentPacketCourse
			{
				StudentPacketId = studentPacketId,
				StudentPacket = UnitOfWork.GetRepo<IStudentPacketRepo>().Get(studentPacketId),
				FPCSCourseId = fpcsCourseId,
				FPCSCourse = UnitOfWork.GetRepo<IFPCSCourseRepo>().Get(fpcsCourseId),
				Repeat = Repeated.NoRepeated,
				GuardianSignature = null,
				SponsorSignature = null,
				InstructorSignature = null,
				AdminSignature = null,
				CreatedDate = DateTimeOffset.Now,
				UpdatedDate = DateTimeOffset.Now,
				IsDeleted = false
			};
			return this.Add(dbEntity);
		}
	}
}
