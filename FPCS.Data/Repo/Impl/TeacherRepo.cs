﻿using System;
using System.Linq;

using FPCS.Data.Entities;
using FPCS.Data.Enums;
using FPCS.Data.Exceptions;
using FPCS.Data.Helpers.HelperModels;
using FPCS.Data.Helpers;

namespace FPCS.Data.Repo.Impl
{
    internal class TeacherRepo : RepoBase<Teacher, StudentManagementContext>, ITeacherRepo
    {
        public TeacherRepo(UnitOfWork<StudentManagementContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        public override IQueryable<Teacher> GetAll()
        {
            return DbSet.Where(x => !x.IsDeleted && !x.IsGuardian);
        }

        public override void Remove(Teacher entity)
        {
            entity.IsDeleted = true;
            entity.UpdatedDate = DateTimeOffset.Now;
        }

        #endregion override methods

        public Teacher Add(Teacher dbEntity)
        {
            dbEntity.Login = dbEntity.Email; // INFO: 17.05.2014 was solved to make login = email on Create
            dbEntity.Password = this.GetSimpleKey();
            dbEntity.CreatedDate = DateTimeOffset.Now;
            dbEntity.UpdatedDate = DateTimeOffset.Now;
            dbEntity.IsDeleted = false;
            dbEntity.IsLocked = false; // INFO: 17.05.2014 was solved to make isLocked = false on Create
            dbEntity.Role = Role.Teacher;

            TaxRate taxRate = TeacherRateHelper.CalcTaxRate(dbEntity);
            dbEntity.BasePayHour = taxRate.BaseHourlyRate;
            dbEntity.PayHourwBenefits = taxRate.HourlyRateTaxesBenefits;

            dbEntity = base.Add(dbEntity);

            return dbEntity;
        }

        public Teacher Update(Teacher dbEntity)
        {
            dbEntity.UpdatedDate = DateTimeOffset.Now;

            TaxRate taxRate = TeacherRateHelper.CalcTaxRate(dbEntity);
            dbEntity.BasePayHour = taxRate.BaseHourlyRate;
            dbEntity.PayHourwBenefits = taxRate.HourlyRateTaxesBenefits;

            return dbEntity;
        }

        public IQueryable<Teacher> GetAll(Boolean isOnlyGuardian, Int32 schoolYearId)
        {
            return DbSet.Where(x => (isOnlyGuardian ? x.IsGuardian : true) && !x.IsDeleted && x.SchoolYearId == schoolYearId);
        }

        public IQueryable<Teacher> GetAll(Int32 schoolYearId)
        {
            return DbSet.Where(x => !x.IsDeleted && x.SchoolYearId == schoolYearId && !x.IsGuardian);
        }

        public Teacher Get(Guid Id)
        {
            return GetAll().FirstOrDefault(x => x.DbUserId == Id);
        }

        public Decimal GetHourlyRate(Guid? Id)
        {
            if (Id.HasValue)
            {
                return GetAll().Where(x => x.DbUserId == Id).Select(x => x.BasePayHour).FirstOrDefault();
            }
            else
            {
                // So its guardian
                return 0m;
            }
        }

        public Decimal GetHourlyRateWithBenefits(Guid? Id)
        {
            if (Id.HasValue)
            {
                return GetAll().Where(x => x.DbUserId == Id).Select(x => x.PayHourwBenefits).FirstOrDefault();
            }
            else
            {
                // So its guardian
                return 0m;
            }
        }
    }
}
