﻿using System;
using System.Linq;
using System.Collections.Generic;

using FPCS.Data.Entities;
using FPCS.Data.Enums;
using FPCS.Data.Exceptions;

namespace FPCS.Data.Repo.Impl
{
    internal class MonthlyContactLogRepo : RepoBase<MonthlyContactLog, StudentManagementContext>, IMonthlyContactLogRepo
    {
        public MonthlyContactLogRepo(UnitOfWork<StudentManagementContext> unitOfWork) : base(unitOfWork) { }

        public IQueryable<MonthlyContactLog> GetByFamily(Int64 familyId)
        {
            return GetAll().Where(x => x.FamilyId == familyId);
        }
    }
}
