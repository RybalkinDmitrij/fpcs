﻿using System;
using System.Linq;
using FPCS.Data.Entities;
using FPCS.Data.Enums;
using FPCS.Data.Exceptions;
using System.Collections.Generic;

namespace FPCS.Data.Repo.Impl
{
	internal class GoodServiceRepo : RepoBase<GoodService, StudentManagementContext>, IGoodServiceRepo
	{
		public GoodServiceRepo(UnitOfWork<StudentManagementContext> unitOfWork) : base(unitOfWork)
		{
		}

		public override IQueryable<GoodService> GetAll()
		{
			return base.GetAll().Where(x => !x.IsDeleted &&
			                                ((((
				                                x.StudentPacketCourse != null &&
				                                !x.StudentPacketCourse.IsDeleted &&
				                                !x.StudentPacketCourse.StudentPacket.IsDeleted &&
				                                !x.StudentPacketCourse.StudentPacket.Student.IsDeleted &&
				                                (x.StudentPacketCourse.FPCSCourse.Semester == Semester.Semester1 ||
				                                 x.StudentPacketCourse.FPCSCourse.Semester == Semester.Semester2 ||
				                                 x.StudentPacketCourse.FPCSCourse.Semester == Semester.Summer)
				                                ) ||
			                                 (
				                                 x.FPCSCourse != null &&
				                                 !x.FPCSCourse.IsDeleted &&
				                                 (x.FPCSCourse.Semester == Semester.Semester1 ||
				                                  x.FPCSCourse.Semester == Semester.Semester2 ||
				                                  x.FPCSCourse.Semester == Semester.Summer)
				                                 ) ||
			                                 (
				                                 x.StudentPacket != null &&
				                                 !x.StudentPacket.IsDeleted &&
				                                 !x.StudentPacket.Student.IsDeleted
				                                 ))) ||
								   x.TempId != null));
		}

		public override void Remove(GoodService entity)
		{
			entity.IsDeleted = true;
			base.Update(entity);
		}

		public IQueryable<GoodService> GetByTemp(Guid tempId)
		{
			var strTempId = tempId.ToString();
			return GetAll().Where(x => x.TempId == strTempId);
		}

		public IQueryable<GoodService> GetByGeneralExpenses(Int64 studentPacketId)
		{
			return GetAll().Where(x => x.StudentPacketId == studentPacketId);
		}

		public IQueryable<GoodService> GetByStudentPacket(Int64 studentPacketId)
		{
			return GetAll().Where(x => x.StudentPacketCourse.StudentPacketId == studentPacketId);
		}

		public IQueryable<GoodService> GetByStudentPacketCourse(Int64 studentPacketCourseId)
		{
			return GetAll().Where(x => x.StudentPacketCourseId == studentPacketCourseId);
		}

		public IQueryable<GoodService> GetByFPCSCourse(Int64 fpcsCourseId)
		{
			return GetAll().Where(x => x.FPCSCourseId == fpcsCourseId);
		}

		public IQueryable<GoodService> GetByFPCSCourse(List<Int64> fpcsCourseIds)
		{
			return GetAll().Where(x => x.FPCSCourseId.HasValue && fpcsCourseIds.Contains(x.FPCSCourseId.Value));
		}

		public IQueryable<GoodService> GetByStudentPacketCourseOrFPCSCourse(Int64 studentPacketCourseId, Int64 fpcsCourseId)
		{
			return GetAll().Where(x => x.StudentPacketCourseId == studentPacketCourseId ||
			                           x.FPCSCourseId == fpcsCourseId);
		}

		public IQueryable<GoodService> GetByStudentPacketCourseOrFPCSCourseOrStudentPacket(Int64 studentPacketCourseId,
			Int64 fpcsCourseId, Int64 studentPacketId, Guid tempId)
		{
			String strTempId = tempId.ToString();
			return GetAll().Where(x => x.StudentPacketCourseId == studentPacketCourseId ||
			                           x.FPCSCourseId == fpcsCourseId ||
			                           (studentPacketCourseId == 0 && fpcsCourseId == 0 && studentPacketId != 0 && x.StudentPacketId == studentPacketId) ||
							   (studentPacketCourseId == 0 && fpcsCourseId == 0 && studentPacketId == 0 && x.TempId == strTempId));
		}

		public IQueryable<GoodService> GetByVendorHasStudentPacketCourse(Int64 vendorId)
		{
			return GetAll().Where(x => x.VendorId == vendorId &&
			                           x.StudentPacketCourseId.HasValue
				);
		}

        public IQueryable<GoodService> GetReimbursements(Guid studentId, Int64 studentPacketId)
		{
			return GetAll().Where(x => ((x.StudentPacketCourse != null &&
			                           x.StudentPacketCourse.StudentPacket != null &&
			                           x.StudentPacketCourse.StudentPacket.StudentId == studentId) ||
                                       (x.StudentPacketId == studentPacketId)) &&
			                           x.RequisitionOrReimbursement == RequisitionOrReimbursement.Reimbursement);
		}

		public IQueryable<GoodService> GetReimbursements(List<Guid> studentIds)
		{
			return GetAll().Where(x => x.StudentPacketCourse != null &&
			                           x.StudentPacketCourse.StudentPacket != null &&
			                           studentIds.Contains(x.StudentPacketCourse.StudentPacket.StudentId) &&
			                           x.RequisitionOrReimbursement == RequisitionOrReimbursement.Reimbursement);
		}

		public IQueryable<GoodService> GetByPacket(Int64 packetId)
		{
			return GetAll().Where(x => x.StudentPacketId == packetId && x.GoodOrServiceApproved != GoodOrServiceApproved.b_rejc);
		}

		public IQueryable<GoodService> GetByIds(List<Int64> ids)
		{
			return GetAll().Where(x => ids.Contains(x.GoodServiceId));
		}

		public IQueryable<GoodService> GetAsQuery(Int64 id, Boolean isStudentPacketCourse = false)
		{
			if (isStudentPacketCourse)
			{
				return GetAll().Where(x => x.GoodServiceId == id && x.StudentPacketCourseId.HasValue);
			}
			else
			{
				return GetAll().Where(x => x.GoodServiceId == id);
			}
		}
	}
}
