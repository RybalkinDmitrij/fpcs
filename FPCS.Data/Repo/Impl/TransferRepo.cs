﻿using System;
using System.Linq;

using FPCS.Data.Entities;
using FPCS.Data.Enums;
using FPCS.Data.Exceptions;

namespace FPCS.Data.Repo.Impl
{
    internal class TransferRepo : RepoBase<Transfer, StudentManagementContext>, ITransferRepo
    {
        public TransferRepo(UnitOfWork<StudentManagementContext> unitOfWork) : base(unitOfWork) { }

        public Decimal GetDeposits(Guid studentId)
        {
            return GetAll().Where(x => x.ToStudentId == studentId)
                           .Select(x => x.TransferAmount)
                           .ToList()
                           .Sum(x => x);
        }

        public Decimal GetWithdrawals(Guid studentId)
        {
            return GetAll().Where(x => x.FromStudentId == studentId)
                           .Select(x => x.TransferAmount)
                           .ToList()
                           .Sum(x => x);
        }

        public IQueryable<Transfer> GetByFromStudent(Guid studentId)
        {
            return GetAll().Where(x => x.FromStudentId == studentId);
        }

        public IQueryable<Transfer> GetByStudent(Guid studentId)
        {
            return GetAll().Where(x => x.FromStudentId == studentId || 
                                       x.ToStudentId == studentId);
        }
    }
}
