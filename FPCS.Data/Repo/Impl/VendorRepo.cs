﻿using System;
using System.Linq;
using FPCS.Data.Entities;
using FPCS.Data.Enums;
using FPCS.Data.Exceptions;
using System.Collections.Generic;

namespace FPCS.Data.Repo.Impl
{
	internal class VendorRepo : RepoBase<Vendor, StudentManagementContext>, IVendorRepo
	{
		public VendorRepo(UnitOfWork<StudentManagementContext> unitOfWork) : base(unitOfWork)
		{
		}

		public override void Remove(Vendor entity)
		{
			entity.IsDeleted = true;
		}

		public override IQueryable<Vendor> GetAll()
		{
			return base.GetAll().Where(x => !x.IsDeleted);
		}

		public IQueryable<Vendor> GetAll(Int32 schoolYearId)
		{
			return GetAll().Where(x => x.SchoolYearId == schoolYearId);
		}

		public IQueryable<Vendor> GetCourseProvider(Int32 schoolYearId)
		{
			return GetAll(schoolYearId).Where(x => x.IsCourseProvider);
		}

		public Vendor Add(
			VendorType type,
			String businessName,
			String firstName,
			String lastName,
			VendorStatus? status,
			String statusComments,
			String streetAddress,
			String streetAddressCity,
			Int64? streetAddressStateId,
			String streetAddressZipCode,
			String mailingAddress,
			String mailingAddressCity,
			Int64? mailingAddressStateId,
			String mailingAddressZipCode,
			String phone,
			String fax,
			String email,
			Location? location,
			String businessWebsite,
			String employerIdentification,
			String akBusinessLicense,
			DateTime? licenseExpiration,
			DateTime? insuranceExpiration,
			String trainingEducationExperience,
			String commentsAboutServices,
			VendorUnitType? vendorUnitType,
			String otherChargeMethod,
			Decimal? price,
			DateTime? contractStartingDate,
			Boolean isNonProfit,
			Boolean isMisdemeanorOrFelony,
			Boolean isBackgroundCheckFingerprinting,
			Boolean isHaveChildrenCurrentlyEnrolled,
			Boolean isRetiredCertificatedASDTeacher,
			Boolean isASDEmployeeEligibleHireList,
			Boolean isCurrentlyAvailableProvideServices,
			Boolean isProvideOnlineCurriculumServices,
			Boolean isCourseProvider,
			Decimal costCourseProvider,
			Int32 schoolYearId
			)
		{
			Vendor vendor = base.Add(new Vendor
			{
				Type = type,
				BusinessName = businessName,
				FirstName = firstName,
				LastName = lastName,
				Status = status,
				StatusComments = statusComments,
				StreetAddress = streetAddress,
				StreetAddressCity = streetAddressCity,
				StreetAddressStateId = streetAddressStateId,
				StreetAddressZipCode = streetAddressZipCode,
				MailingAddress = mailingAddress,
				MailingAddressCity = mailingAddressCity,
				MailingAddressStateId = mailingAddressStateId,
				MailingAddressZipCode = mailingAddressZipCode,
				Phone = phone,
				Fax = fax,
				Email = email,
				Location = location,
				BusinessWebsite = businessWebsite,
				EmployerIdentification = employerIdentification,
				AKBusinessLicense = akBusinessLicense,
				LicenseExpiration = licenseExpiration,
				InsuranceExpiration = insuranceExpiration,
				TrainingEducationExperience = trainingEducationExperience,
				CommentsAboutServices = commentsAboutServices,
				VendorUnitType = vendorUnitType,
				OtherChargeMethod = otherChargeMethod,
				Price = price,
				ContractStartingDate = contractStartingDate,
				IsNonProfit = isNonProfit,
				IsMisdemeanorOrFelony = isMisdemeanorOrFelony,
				IsBackgroundCheckFingerprinting = isBackgroundCheckFingerprinting,
				IsHaveChildrenCurrentlyEnrolled = isHaveChildrenCurrentlyEnrolled,
				IsRetiredCertificatedASDTeacher = isRetiredCertificatedASDTeacher,
				IsASDEmployeeEligibleHireList = isASDEmployeeEligibleHireList,
				IsCurrentlyAvailableProvideServices = isCurrentlyAvailableProvideServices,
				IsProvideOnlineCurriculumServices = isProvideOnlineCurriculumServices,
				IsCourseProvider = isCourseProvider,
				CostCourseProvider = costCourseProvider,
				SchoolYearId = schoolYearId,
				Services = new List<ServiceVendor>()
			});

			if (vendor.UnqVendorID == 0)
			{
				vendor.UnqVendorID = vendor.VendorID;
			}

			return vendor;
		}

		public Vendor Update(
			Int32 vendorId,
			VendorType type,
			String businessName,
			String firstName,
			String lastName,
			VendorStatus? status,
			String statusComments,
			String streetAddress,
			String streetAddressCity,
			Int64? streetAddressStateId,
			String streetAddressZipCode,
			String mailingAddress,
			String mailingAddressCity,
			Int64? mailingAddressStateId,
			String mailingAddressZipCode,
			String phone,
			String fax,
			String email,
			Location? location,
			String businessWebsite,
			String employerIdentification,
			String akBusinessLicense,
			DateTime? licenseExpiration,
			DateTime? insuranceExpiration,
			String trainingEducationExperience,
			String commentsAboutServices,
			VendorUnitType? vendorUnitType,
			String otherChargeMethod,
			Decimal? price,
			DateTime? contractStartingDate,
			Boolean isNonProfit,
			Boolean isMisdemeanorOrFelony,
			Boolean isBackgroundCheckFingerprinting,
			Boolean isHaveChildrenCurrentlyEnrolled,
			Boolean isRetiredCertificatedASDTeacher,
			Boolean isASDEmployeeEligibleHireList,
			Boolean isCurrentlyAvailableProvideServices,
			Boolean isProvideOnlineCurriculumServices,
			Boolean isCourseProvider,
			Decimal costCourseProvider,
			Int32 schoolYearId
			)
		{
			var dbVendor = Get(vendorId);
			if (dbVendor == null) throw new NotFoundEntityException("Vendor {0} not found", vendorId);

			dbVendor.Type = type;
			dbVendor.BusinessName = businessName;
			dbVendor.FirstName = firstName;
			dbVendor.LastName = lastName;
			dbVendor.Status = status;
			dbVendor.StatusComments = statusComments;
			dbVendor.StreetAddress = streetAddress;
			dbVendor.StreetAddressCity = streetAddressCity;
			dbVendor.StreetAddressStateId = streetAddressStateId;
			dbVendor.StreetAddressZipCode = streetAddressZipCode;
			dbVendor.MailingAddress = mailingAddress;
			dbVendor.MailingAddressCity = mailingAddressCity;
			dbVendor.MailingAddressStateId = mailingAddressStateId;
			dbVendor.MailingAddressZipCode = mailingAddressZipCode;
			dbVendor.Phone = phone;
			dbVendor.Fax = fax;
			dbVendor.Email = email;
			dbVendor.Location = location;
			dbVendor.BusinessWebsite = businessWebsite;
			dbVendor.EmployerIdentification = employerIdentification;
			dbVendor.AKBusinessLicense = akBusinessLicense;
			dbVendor.LicenseExpiration = licenseExpiration;
			dbVendor.InsuranceExpiration = insuranceExpiration;
			dbVendor.TrainingEducationExperience = trainingEducationExperience;
			dbVendor.CommentsAboutServices = commentsAboutServices;
			dbVendor.VendorUnitType = vendorUnitType;
			dbVendor.OtherChargeMethod = otherChargeMethod;
			dbVendor.Price = price;
			dbVendor.ContractStartingDate = contractStartingDate;
			dbVendor.IsNonProfit = isNonProfit;
			dbVendor.IsMisdemeanorOrFelony = isMisdemeanorOrFelony;
			dbVendor.IsBackgroundCheckFingerprinting = isBackgroundCheckFingerprinting;
			dbVendor.IsHaveChildrenCurrentlyEnrolled = isHaveChildrenCurrentlyEnrolled;
			dbVendor.IsRetiredCertificatedASDTeacher = isRetiredCertificatedASDTeacher;
			dbVendor.IsASDEmployeeEligibleHireList = isASDEmployeeEligibleHireList;
			dbVendor.IsCurrentlyAvailableProvideServices = isCurrentlyAvailableProvideServices;
			dbVendor.IsProvideOnlineCurriculumServices = isProvideOnlineCurriculumServices;
			dbVendor.IsCourseProvider = isCourseProvider;
			dbVendor.CostCourseProvider = costCourseProvider;
			dbVendor.SchoolYearId = schoolYearId;

			return dbVendor;
		}

		public Vendor UpdateServices(Vendor vendor, IEnumerable<Int32> serviceIds)
		{
			if (serviceIds == null)
			{
				serviceIds = new List<Int32>();
			}
			if (vendor.Services == null)
			{
				vendor.Services = new List<ServiceVendor>();
			}

			var repo = UnitOfWork.GetRepo<IServiceVendorRepo>();

			var added = serviceIds.Where(x => !vendor.Services.Any(x2 => x2.ServiceVendorId == x));
			foreach (var item in added)
			{
				vendor.Services.Add(repo.Get(item));
			}
			;

			if (vendor.Services.Count > serviceIds.Count())
			{
				var removed = vendor.Services.Where(x => !serviceIds.Any(x2 => x2 == x.ServiceVendorId)).ToList();
				foreach (var item in removed)
				{
					vendor.Services.Remove(item);
				}
			}

			return vendor;
		}

		public IQueryable<Vendor> GetAsQuery(Int64 vendorId)
		{
			return GetAll().Where(x => x.VendorID == vendorId);
		}

		public IQueryable<Vendor> GetForGeneralExpenses(Int32 schoolYearId)
		{
			return GetAll(schoolYearId)
				.Where(x => x.BusinessName == "FPCS COMPUTER LEASE" ||
				            x.BusinessName == "FPCS ADDITIONAL COMPUTER" ||
				            x.BusinessName == "FPCS IPAD/TABLET" ||
				            x.BusinessName == "FPCS PRINTER" ||
				            x.BusinessName == "FPCS SCHOOL SUPPLIES" ||
				            x.BusinessName == "FPCS INTERNET");

		}

        public IQueryable<Vendor> GetWithoutGeneralExpenses(Int32 schoolYearId)
        {
            return GetAll(schoolYearId)
                .Where(x => x.BusinessName != "FPCS COMPUTER LEASE" &&
                            x.BusinessName != "FPCS ADDITIONAL COMPUTER" &&
                            x.BusinessName != "FPCS IPAD/TABLET" &&
                            x.BusinessName != "FPCS PRINTER" &&
                            x.BusinessName != "FPCS SCHOOL SUPPLIES" &&
                            x.BusinessName != "FPCS INTERNET");

        }

        public Decimal GetCourseCost(Int64 vendorId)
        {
            return GetAll().Where(x => x.VendorID == vendorId)
                           .Select(x => x.CostCourseProvider)
                           .FirstOrDefault();
        }

        public Int64? GetVendorFileStoreId(Int64 vendorId)
        {
            return GetAll().Where(x => x.VendorID == vendorId)
                           .Select(x => x.FileStoreId)
                           .FirstOrDefault();
        }
	}
}
