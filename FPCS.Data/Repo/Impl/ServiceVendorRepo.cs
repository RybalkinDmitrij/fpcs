﻿using System;
using System.Linq;

using FPCS.Data.Entities;
using FPCS.Data.Enums;
using FPCS.Data.Exceptions;

namespace FPCS.Data.Repo.Impl
{
    internal class ServiceVendorRepo : RepoBase<ServiceVendor, StudentManagementContext>, IServiceVendorRepo
    {
        public ServiceVendorRepo(UnitOfWork<StudentManagementContext> unitOfWork) : base(unitOfWork) { }
    }
}
