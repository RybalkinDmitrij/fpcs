﻿using System;
using System.Linq;

using FPCS.Data.Entities;
using FPCS.Data.Enums;
using FPCS.Data.Exceptions;

namespace FPCS.Data.Repo.Impl
{
    internal class ILPRepo : RepoBase<ILP, StudentManagementContext>, IILPRepo
    {
        public ILPRepo(UnitOfWork<StudentManagementContext> unitOfWork) : base(unitOfWork) { }

        #region override methods
        
        #endregion override methods

        public ILP GetByStudentPacketCourse(Int64 studentPacketCourseId)
        {
            return GetAll().FirstOrDefault(x => x.StudentPacketCourseId == studentPacketCourseId);
        }

        public ILP GetByFPCSCourse(Int64 fpcsCourseId)
        {
            return GetAll().FirstOrDefault(x => x.FPCSCourseId == fpcsCourseId);
        }
    }
}
