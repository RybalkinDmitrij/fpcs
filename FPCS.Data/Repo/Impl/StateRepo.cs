﻿using System;
using System.Linq;

using FPCS.Data.Entities;

namespace FPCS.Data.Repo.Impl
{
    internal class StateRepo : RepoBase<State, StudentManagementContext>, IStateRepo
    {
        public StateRepo(UnitOfWork<StudentManagementContext> unitOfWork) : base(unitOfWork) { }
    }
}
