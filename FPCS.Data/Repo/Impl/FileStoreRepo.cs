﻿using System;
using System.Linq;

using FPCS.Data.Entities;
using FPCS.Data.Enums;
using FPCS.Data.Exceptions;

namespace FPCS.Data.Repo.Impl
{
    internal class FileStoreRepo : RepoBase<FileStore, StudentManagementContext>, IFileStoreRepo
    {
        public FileStoreRepo(UnitOfWork<StudentManagementContext> unitOfWork) : base(unitOfWork) { }

        public FileStore Add(String name, String URL, Int64 contentSize, DateTime? dateCreated, String mimeType)
        {
            return this.Add(new FileStore
            {
                Name = name,
                URL = URL,
                ContentSize = contentSize,
                DateCreated = dateCreated,
                MimeType = mimeType
            });
        }

        public FileStore Get(Int64 fileStoreId)
        {
            return GetAll().FirstOrDefault(x => x.FileStoreId == fileStoreId);
        }
    }
}
