﻿using System;
using System.Linq;

using FPCS.Data.Entities;
using FPCS.Data.Exceptions;
using System.Collections.Generic;

namespace FPCS.Data.Repo.Impl
{
    internal class CourseScheduleOnDayRepo : RepoBase<CourseScheduleOnDay, StudentManagementContext>, ICourseScheduleOnDayRepo
    {
        public CourseScheduleOnDayRepo(UnitOfWork<StudentManagementContext> unitOfWork) : base(unitOfWork) { }

        public IQueryable<CourseScheduleOnDay> GetByFPCSCourse(Int64 fpcsCourseId)
        {
            return DbSet.Where(x => x.FPCSCourseId == fpcsCourseId);
        }

        public void UpdateFPCSCourseSchedule(Int64 fpcsCourseId, IEnumerable<CourseScheduleOnDay> courseScheduleOnDays)
        {
            if (courseScheduleOnDays == null) courseScheduleOnDays = new List<CourseScheduleOnDay>();

            var entities = GetByFPCSCourse(fpcsCourseId).ToList();

            foreach (var item in entities)
            {
                var entity = courseScheduleOnDays.FirstOrDefault(x => x.CourseScheduleOnDayId == item.CourseScheduleOnDayId &&
                            (x.DayOfWeek != item.DayOfWeek || x.StartTime != item.StartTime || x.EndTime != item.EndTime));
                if (entity != null)
                {
                    item.DayOfWeek = entity.DayOfWeek;
                    item.StartTime = entity.StartTime;
                    item.EndTime = entity.EndTime;
                }
            }

            var added = courseScheduleOnDays.Where(x => !entities.Any(x2 => x2.DayOfWeek == x.DayOfWeek));
            foreach (var item in added)
            {
                entities.Add(item);
                DbSet.Add(item);
            }

            if (entities.Count > courseScheduleOnDays.Count())
            {
                var removed = entities.Where(x => !courseScheduleOnDays.Any(x2 => x2.DayOfWeek == x.DayOfWeek));
                foreach (var item in removed) DbSet.Remove(item);
            }
        }
    }
}
