﻿using System;
using System.Linq;
using System.Collections.Generic;

using FPCS.Data.Entities;
using FPCS.Data.Enums;
using FPCS.Data.Exceptions;

using FPCS.Core.Extensions;

namespace FPCS.Data.Repo.Impl
{
    internal class GuardianRepo : RepoBase<Guardian, StudentManagementContext>, IGuardianRepo
    {
        public GuardianRepo(UnitOfWork<StudentManagementContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        public override IQueryable<Guardian> GetAll()
        {
            return DbSet.Where(x => !x.IsDeleted);
        }

        public override void Remove(Guardian entity)
        {
            entity.IsDeleted = true;
            entity.UpdatedDate = DateTimeOffset.Now;
            entity.Families.Clear();
            UnitOfWork.GetRepo<IFamilyRelationshipRepo>().RemoveGuardian(entity.DbUserId);
        }

        #endregion override methods

        public IQueryable<Guardian> GetAll(Int32 schoolYearId)
        {
            return GetAll().Where(x => x.SchoolYearId == schoolYearId);
        }

        public Guardian Get(Guid Id)
        {
            return GetAll().FirstOrDefault(x => x.DbUserId == Id);
        }

        public IQueryable<Guardian> GetWithoutFamily(Int64 familyId, Int32 schoolYearId)
        {
            return GetAll(schoolYearId).Where(x => !x.Families.Any(x2 => x2.FamilyId == familyId));
        }

        public IQueryable<Guardian> GetByStudent(Guid studentId)
        {
            var family = UnitOfWork.GetRepo<IStudentRepo>().Get(studentId).Families.FirstOrDefault();

            return family != null ? family.Guardians.AsQueryable() : null;
        }

        public IQueryable<Guardian> GetByFamilies(Int32 schoolYearID, List<Int64> familyIds)
        {
            return GetAll(schoolYearID).Where(x => x.Families.Any(t => familyIds.Contains(t.FamilyId)));
        }

        public Guardian Add(String firstName, String lastName, String middleInitial, String employer, 
            Boolean? isActiveMilitary, String rank, String pager, String businessPhone, String ext, String cellPhone, 
            String email, String address, String city, String country, String zip, Int64? stateId, Int32 schoolYearId)
        {
            var guardian =
                Add(
                new Guardian
                {
                    Login = this.GetSimpleKey(),
                    Password = this.GetSimpleKey(),
                    FirstName = firstName.TrimAndReduce(),
                    LastName = lastName.TrimAndReduce(),
                    FullName = lastName.TrimAndReduce() + " " + firstName.TrimAndReduce(),
                    MiddleInitial = middleInitial.TrimAndReduce(),
                    Employer = employer.TrimAndReduce(),
                    IsActiveMilitary = isActiveMilitary,
                    Rank = rank.TrimAndReduce(),
                    Pager = pager.TrimAndReduce(),
                    BusinessPhone = businessPhone.TrimAndReduce(),
                    Ext = ext.TrimAndReduce(),
                    CellPhone = cellPhone.TrimAndReduce(),
                    Email = email.TrimAndReduce(),
                    Address = address.TrimAndReduce(),
                    City = city.TrimAndReduce(),
                    Country = country.TrimAndReduce(),
                    Zip = zip.TrimAndReduce(),
                    CreatedDate = DateTimeOffset.Now,
                    UpdatedDate = DateTimeOffset.Now,
                    IsDeleted = false,
                    IsLocked = false,
                    StateId = stateId,
                    Role = Role.Guardian,
                    SchoolYearId = schoolYearId
                });
            
            return guardian;
        }

        public Guardian Update(Guid dbUserId, String firstName, String lastName, String middleInitial, String employer,
            Boolean? isActiveMilitary, String rank, String pager, String businessPhone, String ext, String cellPhone,
            String email, String address, String city, String country, String zip, Int64? stateId)
        {
            var dbUser = this.Get(dbUserId);
            if (dbUser == null) throw new NotFoundEntityException("Guardian not found");

            dbUser.FirstName = firstName.TrimAndReduce();
            dbUser.LastName = lastName.TrimAndReduce();
            dbUser.FullName = lastName.TrimAndReduce() + " " + firstName.TrimAndReduce();
            dbUser.MiddleInitial = middleInitial.TrimAndReduce();
            dbUser.Employer = employer.TrimAndReduce();
            dbUser.IsActiveMilitary = isActiveMilitary;
            dbUser.Rank = rank.TrimAndReduce();
            dbUser.Pager = pager.TrimAndReduce();
            dbUser.BusinessPhone = businessPhone.TrimAndReduce();
            dbUser.Ext = ext.TrimAndReduce();
            dbUser.CellPhone = cellPhone.TrimAndReduce();
            dbUser.Email = email.TrimAndReduce();
            dbUser.Address = address.TrimAndReduce();
            dbUser.City = city.TrimAndReduce();
            dbUser.Country = country.TrimAndReduce();
            dbUser.Zip = zip.TrimAndReduce();
            dbUser.UpdatedDate = DateTimeOffset.Now;
            dbUser.StateId = stateId;

            return dbUser;
        }

        
    }
}
