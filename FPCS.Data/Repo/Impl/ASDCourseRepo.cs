﻿using System;
using System.Linq;
using FPCS.Data.Entities;
using FPCS.Data.Exceptions;
using FPCS.Core.Extensions;
using FPCS.Data.Enums;

namespace FPCS.Data.Repo.Impl
{
	internal class ASDCourseRepo : RepoBase<ASDCourse, StudentManagementContext>, IASDCourseRepo
	{
		public ASDCourseRepo(UnitOfWork<StudentManagementContext> unitOfWork) : base(unitOfWork)
		{
		}

		#region override methods

		public override IQueryable<ASDCourse> GetAll()
		{
			return DbSet.Where(x => !x.IsDeleted);
		}

		public override void Remove(ASDCourse entity)
		{
			if (UnitOfWork.GetRepo<IFPCSCourseRepo>().GetByASDCourse(entity.ASDCourseId).Count() > 0)
				throw new ChildEntityRelationException(String.Format(@"ASD Course {0} has FPCS courses so can not be removed",
					entity.ExternalASDCourseId));

			entity.IsDeleted = true;
			entity.UpdatedDate = DateTimeOffset.Now;
		}

		#endregion override methods

		public IQueryable<ASDCourse> GetAll(Int32 schoolYearId)
		{
			return GetAll().Where(x => x.SchoolYearId == schoolYearId);
		}

		public ASDCourse Get(Int32 Id)
		{
			return GetAll().FirstOrDefault(x => x.ASDCourseId == Id);
		}

		public IQueryable<ASDCourse> GetAsQuery(Int64 asdCourseId, Int32 schoolYearId)
		{
			return GetAll(schoolYearId).Where(x => x.ASDCourseId == asdCourseId);
		}

		public ASDCourse GetByExternalASDCourseId(String externalASDCourseId, Int32 schoolYearId)
		{
			return GetAll(schoolYearId).FirstOrDefault(x => x.ExternalASDCourseId == externalASDCourseId);
		}

		public IQueryable<ASDCourse> GetBySchoolYear(Int32 schoolYearId)
		{
			return GetAll().Where(x => x.SchoolYearId == schoolYearId);
		}

		public IQueryable<ASDCourse> GetActivatedBySchoolYear(Int32 schoolYearId)
		{
			return GetAll(schoolYearId).Where(x => x.IsActivated);
		}

		public Boolean IsExistExternalASDCourseId(String externalASDCourseId, Int32 schoolYearId)
		{
			return GetAll(schoolYearId).Any(x => x.ExternalASDCourseId == externalASDCourseId);
		}

		public ASDCourse Add(String externalASDCourseId, String description, String name, Decimal gradCredit, Int32 subjectId,
			Int32 schoolYearId, Boolean isActivated, Boolean isFree)
		{
			if (IsExistExternalASDCourseId(externalASDCourseId, schoolYearId))
				throw new DuplicateEntityException("This ASD Course Id already exist");

			externalASDCourseId = externalASDCourseId.TrimAndReduce();
			description = description.TrimAndReduce();
			name = name.TrimAndReduce();

			var asdCourse =
				Add(
					new ASDCourse
					{
						UnqASDCourseId = 0,
						ExternalASDCourseId = externalASDCourseId,
						SubjectId = subjectId,
						SchoolYearId = schoolYearId,
						IsActivated = isActivated,
						CreatedDate = DateTimeOffset.Now,
						UpdatedDate = DateTimeOffset.Now,
						IsDeleted = false,
						IsFree = isFree,
						Description = description,
						Name = name,
						GradCredit = gradCredit,
						TypeGrade = name.Contains("High")
							? TypeGrade.High
							: name.Contains("Middle")
								? TypeGrade.Middle
								: name.Contains("University") ? TypeGrade.University : TypeGrade.Elementary
					});

			asdCourse.UnqASDCourseId = asdCourse.ASDCourseId;

			return asdCourse;
		}

		public ASDCourse Update(Int32 asdCourseId, String externalASDCourseId, String description, String name,
			Decimal gradCredit, Int32 subjectId, Boolean isActivated, Int32 schoolYearId, Boolean isFree)
		{
			var dbEntity = GetByExternalASDCourseId(externalASDCourseId, schoolYearId);
			if (dbEntity != null && dbEntity.ASDCourseId != asdCourseId)
				throw new DuplicateEntityException("This ASD Course Id already exist");

			dbEntity = Get(asdCourseId);
			if (dbEntity == null) throw new NotFoundEntityException("ASD Course not found");

			if (UnitOfWork.GetRepo<IFPCSCourseRepo>().GetByASDCourse(asdCourseId).Count() == 0)
				dbEntity.IsActivated = isActivated;

			dbEntity.ExternalASDCourseId = externalASDCourseId.TrimAndReduce();
			dbEntity.SubjectId = subjectId;
			dbEntity.UpdatedDate = DateTimeOffset.Now;
			dbEntity.Description = description.TrimAndReduce();
			dbEntity.Name = name.TrimAndReduce();
			dbEntity.GradCredit = gradCredit;
			dbEntity.TypeGrade = name.Contains("High")
				? TypeGrade.High
				: name.Contains("Middle")
					? TypeGrade.Middle
					: name.Contains("University") ? TypeGrade.University : TypeGrade.Elementary;
			dbEntity.IsFree = isFree;

			return dbEntity;
		}
	}
}
