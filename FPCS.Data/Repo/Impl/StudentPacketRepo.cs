﻿using System;
using System.Linq;
using System.Collections.Generic;
using FPCS.Data.Entities;
using FPCS.Data.Exceptions;
using FPCS.Data.Enums;

namespace FPCS.Data.Repo.Impl
{
	internal class StudentPacketRepo : RepoBase<StudentPacket, StudentManagementContext>, IStudentPacketRepo
	{
		public StudentPacketRepo(UnitOfWork<StudentManagementContext> unitOfWork) : base(unitOfWork)
		{
		}

		#region override methods

		public override IQueryable<StudentPacket> GetAll()
		{
			return DbSet.Where(x => !x.IsDeleted && !x.Student.IsDeleted);
		}

		public override void Remove(StudentPacket entity)
		{
			entity.IsDeleted = true;
			entity.UpdatedDate = DateTimeOffset.Now;
		}

		#endregion override methods

		public StudentPacket Get(Int64 studentPacketId)
		{
			return GetAll().FirstOrDefault(x => x.StudentPacketId == studentPacketId);
		}

		public IQueryable<StudentPacket> GetAsQuery(Int64 studentPacketId)
		{
			return GetAll().Where(x => x.StudentPacketId == studentPacketId);
		}

		public IQueryable<Student> GetBySponsor(Guid sponsorId, int schoolYearId)
		{
			return GetBySchoolYear(schoolYearId).Where(x => x.SponsorTeacherId == sponsorId).Select(x => x.Student);
		}

		public IQueryable<StudentPacket> GetBySchoolYear(Int32 schoolYearId)
		{
			// INFO: 26.05.2014 Solve make relation Student and School Year
			return GetAll().Where(x => x.Student.SchoolYearId == schoolYearId);
		}

		public StudentPacket Get(Guid studentId, Int32 schoolYearId)
		{
			return GetAll().FirstOrDefault(x => x.StudentId == studentId && x.SchoolYearId == schoolYearId);
		}

		public IQueryable<StudentPacket> GetAsQuery(Guid studentId, Int32 schoolYearId)
		{
			return GetAll().Where(x => x.StudentId == studentId && x.SchoolYearId == schoolYearId);
		}

		public Boolean IsExist(Guid studentId, Int32 schoolYearId)
		{
			return IsExist(x => x.StudentId == studentId && x.SchoolYearId == schoolYearId);
		}

		public IQueryable<StudentPacket> GetSignedPackets(Int32 schoolYearId)
		{
			return
				GetBySchoolYear(schoolYearId)
					.Where(x => x.SponsorSignatureSem1 == Sign.Sign && x.GuardianSignatureSem1 == Sign.Sign);
		}

		public StudentPacket AddIsNotExist(Guid studentId, Int32 schoolYearId)
		{
			if (IsExist(studentId, schoolYearId)) return null;

			var dbEntity = new StudentPacket
			{
				StudentId = studentId,
				SchoolYearId = schoolYearId,
				SponsorTeacherId = null,
				IsDeleted = false,
				IsLocked = false,
				CreatedDate = DateTimeOffset.Now,
				UpdatedDate = DateTimeOffset.Now,
				StudentPacketCourses = new List<StudentPacketCourse>()
			};

			return this.Add(dbEntity);
		}

		public StudentPacket GetOrAddIsNotExist(Guid studentId, Int32 schoolYearId)
		{
			var dbEntity = Get(studentId, schoolYearId);
			if (dbEntity != null) return dbEntity;

			dbEntity = new StudentPacket
			{
				StudentId = studentId,
				SchoolYearId = schoolYearId,
				SponsorTeacherId = null,
				IsDeleted = false,
				IsLocked = false,
				CreatedDate = DateTimeOffset.Now,
				UpdatedDate = DateTimeOffset.Now,
				StudentPacketCourses = new List<StudentPacketCourse>()
			};

			return this.Add(dbEntity);
		}

		public StudentPacket UpdateSponsorTeacher(Int64 studentPacketId, Guid teacherId)
		{
			var dbEntity = this.Get(studentPacketId);
			if (dbEntity == null) throw new NotFoundEntityException("Student packet {0} not found", studentPacketId);

			dbEntity.SponsorTeacherId = teacherId;
			dbEntity.UpdatedDate = DateTimeOffset.Now;

			return dbEntity;
		}

		public StudentPacket UpdateSponsorSignature(Int64 studentPacketId, Sign? signature)
		{
			var dbEntity = this.Get(studentPacketId);
			if (dbEntity == null) throw new NotFoundEntityException("Student packet {0} not found", studentPacketId);

			dbEntity.SponsorSignatureSem1 = signature;
			dbEntity.UpdatedDate = DateTimeOffset.Now;

			return dbEntity;
		}
	}
}
