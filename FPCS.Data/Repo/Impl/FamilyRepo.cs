﻿using System;
using System.Linq;
using System.Collections.Generic;

using FPCS.Data.Entities;
using FPCS.Data.Enums;
using FPCS.Data.Exceptions;

namespace FPCS.Data.Repo.Impl
{
    internal class FamilyRepo : RepoBase<Family, StudentManagementContext>, IFamilyRepo
    {
        public FamilyRepo(UnitOfWork<StudentManagementContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        public override IQueryable<Family> GetAll()
        {
            return DbSet.Where(x => !x.IsDeleted);
        }

        public override void Remove(Family entity)
        {
            entity.IsDeleted = true;
            entity.UpdatedDate = DateTimeOffset.Now;
            var guardiansIds = entity.Guardians.Select(x => x.DbUserId).ToList();
            var studentsIds = entity.Students.Select(x => x.DbUserId).ToList();
            UnitOfWork.GetRepo<IFamilyRelationshipRepo>().RemoveGuardiansStudents(guardiansIds, studentsIds);
            entity.Guardians.Clear();
            entity.Students.Clear();
        }

        #endregion override methods

        public IQueryable<Family> GetAll(Int32 schoolYearId)
        {
            return GetAll().Where(x => x.SchoolYearId == schoolYearId);
        }

        public Family Get(Int64 familyId)
        {
            return GetAll().FirstOrDefault(x => x.FamilyId == familyId);
        }

        public IQueryable<Family> GetAsQuery(Int64 familyId)
        {
            return GetAll().Where(x => x.FamilyId == familyId);
        }

        public ICollection<Guardian> GetGuardiansByStudent(Guid studentId)
        {
            return GetAll().Where(x => x.Students.Select(y => y.DbUserId).Contains(studentId)).Select(x => x.Guardians).FirstOrDefault().Where(x => !x.IsDeleted).ToList();
        }

        public ICollection<Guardian> GetGuardiansByFamily(Int64 familyId)
        {
            return GetAll().Where(x => x.FamilyId == familyId).FirstOrDefault().Guardians.Where(x => !x.IsDeleted).ToList();
        }

        public List<Guardian> GetRelativesByDbUserId(Guid dbUserId)
        {
            var repo = this.UnitOfWork.GetRepo<IGuardianRepo>();
            var family = repo.GetAll().Where(x => x.DbUserId == dbUserId).Select(x => x.Families.FirstOrDefault());

            return GetAll().Where(x => x.FamilyId == family.Select(y => y.FamilyId).FirstOrDefault()).Select(x => x.Guardians).FirstOrDefault().ToList();
        }

        public ICollection<Student> GetStudentsByFamily(Int64 familyId)
        {
            return GetAll().Where(x => x.FamilyId == familyId).FirstOrDefault().Students.Where(x => !x.IsDeleted).ToList();
        }

        public IQueryable<Family> GetFamiliesByGuardian(Guid guardianId)
        {
            return GetAll().Where(x => x.Guardians.Select(t => t.DbUserId).Contains(guardianId));
        }

        public IQueryable<Family> GetFamiliesByStudent(Guid studentId)
        {
            return GetAll().Where(x => x.Students.Select(t => t.DbUserId).Contains(studentId));
        }

        public List<Student> GetBrotherAndSisterByStudent(Guid studentId)
        {
            var family = GetAll().Where(x => x.Students.Select(y => y.DbUserId).Contains(studentId)).FirstOrDefault();

            if (family != null && family.Students != null)
            {
                return family.Students.Where(x => x.DbUserId != studentId).ToList();
            }
            else
            {
                return new List<Student>();
            }
        }

        public Family Add(String name, String description, String address, String mailingAddress, String telephone, String cellPhone, String city, String country,
         String zip, String email, String password, Boolean isUseDirectory, Int64 stateId, Int32 schoolYearId)
        {
            return this.Add(new Family
            {
                Name = name,
                Description = description,
                Address = address,
                MailingAddress = mailingAddress,
                Telephone = telephone,
                CellPhone = cellPhone,
                City = city,
                Country = country,
                Zip = zip,
                Email = email,
                Password = password,
                IsUseDirectory = isUseDirectory,
                IsDeleted = false,
                CreatedDate = DateTimeOffset.Now,
                UpdatedDate = DateTimeOffset.Now,
                StateId = stateId,
                SchoolYearId = schoolYearId,
                Guardians = new List<Guardian>(),
                Students = new List<Student>()
            });
        }

        public Family Update(Int64 FamilyId, String name, String description, String address, String mailingAddress, String telephone, String cellPhone, String city, String country,
         String zip, String email, String password, Boolean isUseDirectory, Int64 stateId)
        {
            var dbFamily = Get(FamilyId);
            if (dbFamily == null) throw new NotFoundEntityException("Family {0} not found", FamilyId);

            dbFamily.Name = name;
            dbFamily.Description = description;
            dbFamily.Address = address;
            dbFamily.MailingAddress = mailingAddress;
            dbFamily.Telephone = telephone;
            dbFamily.CellPhone = cellPhone;
            dbFamily.City = city;
            dbFamily.Country = country;
            dbFamily.Zip = zip;
            dbFamily.Email = email;
            dbFamily.Password = password;
            dbFamily.IsUseDirectory = isUseDirectory;
            dbFamily.StateId = stateId;
            dbFamily.UpdatedDate = DateTimeOffset.Now;
            return dbFamily;
        }

        public Family Update(Int64 familyId, String address, String mailingAddress, String telephone, String cellPhone, String city, String country,
         String zip, Boolean isUseDirectory, Int64 stateId)
        {
            var dbFamily = Get(familyId);
            if (dbFamily == null) throw new NotFoundEntityException("Family {0} not found", familyId);

            dbFamily.Address = address;
            dbFamily.MailingAddress = mailingAddress;
            dbFamily.Telephone = telephone;
            dbFamily.CellPhone = cellPhone;
            dbFamily.City = city;
            dbFamily.Country = country;
            dbFamily.Zip = zip;
            dbFamily.IsUseDirectory = isUseDirectory;
            dbFamily.StateId = stateId;
            dbFamily.UpdatedDate = DateTimeOffset.Now;
            return dbFamily;
        }

        public Family UpdateGuardians(Family family, IEnumerable<Guid> dbUserIds)
        {
            var repo = this.UnitOfWork.GetRepo<IGuardianRepo>();
            var sgRepo = this.UnitOfWork.GetRepo<IFamilyRelationshipRepo>();

            var students = family.Students.ToList();
            var studentsIds = students.Select(x => x.DbUserId).ToList();

            var deleting = family.Guardians.ToList().Where(x => !dbUserIds.Contains(x.DbUserId));
            foreach (var item in deleting)
            {
                family.Guardians.Remove(item);
                sgRepo.RemoveGuardian(item.DbUserId, studentsIds);
                UnitOfWork.Commit();
            }

            foreach (var id in dbUserIds)
            {
                if (!family.Guardians.Any(x => x.DbUserId == id))
                {
                    var guardian = repo.Get(id);

                    var oldFamilies = GetFamiliesByGuardian(id).ToList();
                    foreach (var item in oldFamilies)
                    {
                        item.Guardians.Remove(guardian);
                        UnitOfWork.Commit();
                        sgRepo.RemoveGuardian(guardian.DbUserId, item.Students.Select(x => x.DbUserId).ToList());
                        UnitOfWork.Commit();
                        
                        item.Name = GetFamilyName(item.Guardians);
                    }

                    family.Guardians.Add(guardian);
                    sgRepo.AddGuardian(guardian, students, FamilyRelationshipType.NotSpecified);
                }
            }
            return family;
        }

        public Family UpdateStudents(Family family, IEnumerable<Guid> dbUserIds)
        {
            var repo = this.UnitOfWork.GetRepo<IStudentRepo>();
            var sgRepo = UnitOfWork.GetRepo<IFamilyRelationshipRepo>();

            var guardians = family.Guardians.ToList();
            var guardiansIds = guardians.Select(x => x.DbUserId).ToList();

            var deleting = family.Students.ToList().Where(x => !dbUserIds.Contains(x.DbUserId));
            foreach (var item in deleting)
            {
                family.Students.Remove(item);
                sgRepo.RemoveStudent(item.DbUserId, guardiansIds);
                UnitOfWork.Commit();
            }

            foreach (var id in dbUserIds)
            {
                if (!family.Students.Any(x => x.DbUserId == id))
                {
                    var student = repo.Get(id);

                    var oldFamilies = GetFamiliesByStudent(id).ToList();
                    foreach (var item in oldFamilies)
                    {
                        item.Students.Remove(student);
                        UnitOfWork.Commit();
                        sgRepo.RemoveStudent(student.DbUserId, item.Guardians.Select(x => x.DbUserId).ToList());
                        UnitOfWork.Commit();
                    }

                    family.Students.Add(student);
                    sgRepo.AddStudent(student, guardians, FamilyRelationshipType.NotSpecified);
                    UnitOfWork.Commit();
                }
            }
            return family;
        }

        private String GetFamilyName(Family family)
        {
            return GetFamilyName(family.Guardians);
        }

        private String GetFamilyName(ICollection<Guardian> guardians)
        {
            var familyName = String.Empty;

            if (guardians.Count() != 0)
            {
                var repoDbUser = UnitOfWork.GetRepo<IDbUserRepo>();

                var names = guardians.Select(x => new { x.LastName, x.FirstName }).ToList();
                var lastNames = names.Select(x => x.LastName).Distinct();

                if (lastNames.Count() == 1) familyName = names[0].LastName;
                else familyName = String.Join(" - ", lastNames);
            }

            return familyName;
        }
    }
}
