﻿using System;
using System.Linq;

using FPCS.Data.Entities;

namespace FPCS.Data.Repo.Impl
{
    internal class SubjectRepo : RepoBase<Subject, StudentManagementContext>, ISubjectRepo
    {
        public SubjectRepo(UnitOfWork<StudentManagementContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        public override IQueryable<Subject> GetAll()
        {
            return DbSet.Where(x => !x.IsDeleted);
        }

        public override void Remove(Subject entity)
        {
            entity.IsDeleted = true;
        }

        #endregion override methods

        public Subject Get(Int32 Id)
        {
            return GetAll().FirstOrDefault(x => x.SubjectId == Id);
        }

        public Subject Add(String name, String description, Int32 code)
        {
            return this.Add(new Subject
            {
                Name = name,
                Description = description,
                IsDeleted = false,
                Code = code
            });
        }

        public Subject GetByName(String name)
        {
            return GetAll().FirstOrDefault(x => x.Name.Trim().ToLower() == name.Trim().ToLower());
        }
    }
}
