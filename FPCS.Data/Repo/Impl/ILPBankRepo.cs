﻿using System;
using System.Linq;

using FPCS.Data.Entities;
using FPCS.Data.Enums;
using FPCS.Data.Exceptions;

namespace FPCS.Data.Repo.Impl
{
    internal class ILPBankRepo : RepoBase<ILPBank, StudentManagementContext>, IILPBankRepo
    {
        public ILPBankRepo(UnitOfWork<StudentManagementContext> unitOfWork) : base(unitOfWork) { }

        #region override methods
        
        #endregion override methods

        public ILPBank GetByILP(Int32 ilpId)
        {
            return GetAll().FirstOrDefault(x => x.ILPId == ilpId);
        }

        public IQueryable<ILPBank> GetAsQuery(Int32 ilpBankId)
        {
            return GetAll().Where(x => x.ILPBankId == ilpBankId);
        }
    }
}
