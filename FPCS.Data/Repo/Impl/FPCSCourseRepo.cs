﻿using System;
using System.Linq;
using System.Data.Entity;
using System.Collections.Generic;

using FPCS.Data.Entities;
using FPCS.Data.Exceptions;
using FPCS.Data.Enums;

using FPCS.Core.Extensions;

namespace FPCS.Data.Repo.Impl
{
    internal class FPCSCourseRepo : RepoBase<FPCSCourse, StudentManagementContext>, IFPCSCourseRepo
    {
        public FPCSCourseRepo(UnitOfWork<StudentManagementContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        public override IQueryable<FPCSCourse> GetAll()
        {
            return DbSet.Where(x => !x.IsDeleted);
        }
        
        public override void Remove(FPCSCourse entity)
        {
            entity.IsDeleted = true;
            entity.FPCSCoursesRestrictions.Clear();
            entity.UpdatedDate = DateTimeOffset.Now;
        }

        #endregion override methods

        public IQueryable<FPCSCourse> GetAll(Int32 schoolYearId)
        {
            return GetAll().Where(x => x.ASDCourse.SchoolYearId == schoolYearId);
        }
        
        public FPCSCourse Get(Int64 Id)
        {
            return GetAll().FirstOrDefault(x => x.FPCSCourseId == Id);
        }

        public IQueryable<FPCSCourse> GetAsQuery(Int64 fpcsCourseId)
        {
            return GetAll().Where(x => x.FPCSCourseId == fpcsCourseId);
        }

        public IQueryable<FPCSCourse> GetByIds(List<Int64> fpcsCourseIds)
        {
            return GetAll().Where(x => fpcsCourseIds.Contains(x.FPCSCourseId));
        }

        public IQueryable<FPCSCourse> GetBySchoolYear(Int32 schoolYearId)
        {
            return GetAll().Where(x => x.ASDCourse.SchoolYearId == schoolYearId);
        }

        public IQueryable<FPCSCourse> GetBySchoolYearAndUser(Int32 schoolYearId, Guid userId)
        {
            DbUser user = UnitOfWork.GetRepo<IDbUserRepo>().Get(userId);

            if (user.Role == Role.Admin)
            {
                return GetAll(schoolYearId);
            }
            else if (user.Role == Role.Teacher)
            {
                return GetAll(schoolYearId).Where(x => x.TeacherId == user.DbUserId/* || x.GuardianId.HasValue*/);
            }
            else if (user.Role == Role.Guardian)
            {
                return GetAll(schoolYearId).Where(x => x.GuardianId == user.DbUserId/* || x.TeacherId.HasValue*/);
            }
            else
            {
                throw new Exception("It's user don't have permission!");
            }
        }

        public IQueryable<FPCSCourse> GetByASDCourse(Int32 asdCourseId)
        {
            return GetAll().Where(x => x.ASDCourseId == asdCourseId);
        }

        public IQueryable<FPCSCourse> GetNotSelectedInPacketCourses(Guid studentId, Int64 studentPacketId, Int32 schoolYearId)
        {
            var existIds = UnitOfWork.GetRepo<IStudentPacketCourseRepo>().GetByStudentPacket(studentPacketId).Select(x => x.FPCSCourseId);

            var guardianIds = UnitOfWork.GetRepo<IGuardianRepo>().GetByStudent(studentId).Select(x => x.DbUserId);

            var result =
                    GetAll(schoolYearId)
                           .Where(x => x.IsActivated &&
                                       !existIds.Contains(x.FPCSCourseId) &&
                                       //(x.FPCSCoursesRestrictions.Count() == 0 ||
                                        //x.FPCSCoursesRestrictions.Any(x2 => x2.Students.Any(x3 => x3.DbUserId == studentId))) &&
                                       (x.GuardianId != null && guardianIds.Contains(x.GuardianId.Value) ||
                                        x.GuardianId == null)
                                        );

            return result;
        }

        public IQueryable<FPCSCourse> GetNotSelectedInPacketCourses(Guid studentId, Int64 studentPacketId, Int32 schoolYearId, Semester semester)
        {
            return GetNotSelectedInPacketCourses(studentId, studentPacketId, schoolYearId).Where(x => x.Semester == semester/* || x.Semester == Semester.AllYear*/);
        }

        public FPCSCourse Add(Int32 asdCourseId, Guid? teacherId, Guid? guardianId, Int32? vendorId, String name, Int32? gradeLevel, Int32? courseSubjectId, String location, CourseClassType? classType, Int32? subjectId,
            Boolean isActivated, Semester semester)
        {
            var schoolYear = UnitOfWork.GetRepo<ISchoolYearRepo>().GetByASDCourse(asdCourseId);
            var classStartDate = GetStartSemester(schoolYear, semester);
            var classEndDate = GetEndSemester(schoolYear, semester);

            var asdCourse = UnitOfWork.GetRepo<IASDCourseRepo>().Get(asdCourseId);

            var dbEntity = new FPCSCourse
            {
                ASDCourseId = asdCourseId,
                TeacherId = teacherId,
                GuardianId = guardianId,
                VendorId = vendorId,
                Name = name.TrimAndReduce(),
                Location = location.TrimAndReduce(),
                ClassType = classType,
                ClassStartDate = classStartDate,
                ClassEndDate = classEndDate,
                Semester = semester,
                IsActivated = isActivated,

                SubjectId = subjectId,
                GradeLevel = (Int32)asdCourse.TypeGrade,
                CourseSubjectId = asdCourse.SubjectId,

                CreatedDate = DateTimeOffset.Now,
                UpdatedDate = DateTimeOffset.Now,
                IsDeleted = false
            };

            UpdateClassCost(dbEntity, schoolYear.SchoolYearId);
            return Add(dbEntity);
        }

        public FPCSCourse Update(Int64 fpcsCourseId, Int32 asdCourseId, Guid? teacherId, Guid? guardianId, Int32? vendorId, String name, Int32? gradeLevel, Int32? courseSubjectId, String location, CourseClassType? classType, Int32? subjectId, Boolean isActivated, Semester semester)
        {
            var dbEntity = Get(fpcsCourseId);
            if (dbEntity == null) throw new NotFoundEntityException("FPCS Course not found");

            var schoolYear = UnitOfWork.GetRepo<ISchoolYearRepo>().GetByASDCourse(dbEntity.ASDCourseId);
            var classStartDate = GetStartSemester(schoolYear, semester);
            var classEndDate = GetEndSemester(schoolYear, semester);

            var asdCourse = UnitOfWork.GetRepo<IASDCourseRepo>().Get(asdCourseId);

            dbEntity.ASDCourseId = asdCourseId;
            dbEntity.TeacherId = teacherId;
            dbEntity.GuardianId = guardianId;
            dbEntity.VendorId = vendorId;
            dbEntity.Name = name.TrimAndReduce();
            dbEntity.Location = location.TrimAndReduce();
            dbEntity.ClassType = classType;
            dbEntity.ClassStartDate = classStartDate;
            dbEntity.ClassEndDate = classEndDate;
            dbEntity.Semester = semester;
            dbEntity.IsActivated = isActivated;
            dbEntity.UpdatedDate = DateTimeOffset.Now;

            dbEntity.SubjectId = subjectId;
            dbEntity.GradeLevel = (Int32)asdCourse.TypeGrade;
            dbEntity.CourseSubjectId = asdCourse.SubjectId;

            UpdateClassCost(dbEntity, schoolYear.SchoolYearId);
            return dbEntity;
        }

        public FPCSCourse UpdateCourseRestrictions(FPCSCourse course, IEnumerable<Int64> families)
        {
            if (families == null) families = new List<Int64>();
            if (course.FPCSCoursesRestrictions == null) course.FPCSCoursesRestrictions = new List<Family>();

            var repo = UnitOfWork.GetRepo<IFamilyRepo>();

            var added = families.Where(x => !course.FPCSCoursesRestrictions.Any(x2 => x2.FamilyId == x));
            foreach (var item in added) course.FPCSCoursesRestrictions.Add(repo.Get(item));

            if (course.FPCSCoursesRestrictions.Count > families.Count())
            {
                var removed = course.FPCSCoursesRestrictions.Where(x => !families.Any(x2 => x2 == x.FamilyId)).ToList();
                foreach (var item in removed) course.FPCSCoursesRestrictions.Remove(item);
            }

            return course;
        }

        public FPCSCourse UpdateClassCost(Int64 courseId, Int32 schoolYearId)
        {
            var course = GetAsQuery(courseId).Include(x => x.Teacher).FirstOrDefault();
            if (course == null) throw new NotFoundEntityException("FPCS Course not found");
            return UpdateClassCost(course, course.Teacher != null ? course.Teacher.BasePayHour : 0, schoolYearId);
        }

        public FPCSCourse UpdateClassCost(FPCSCourse course, Int32 schoolYearId)
        {
            var rate = UnitOfWork.GetRepo<ITeacherRepo>().GetHourlyRate(course.TeacherId);
            return UpdateClassCost(course, rate, schoolYearId);
        }

        public FPCSCourse UpdateClassCost(FPCSCourse course, Decimal teacherHourlyRate, Int32 schoolYearId)
        {
            course.TotalHours = course.HoursTeacherPlanning + course.HoursWithAllStudents;
            course.EnrolledCount = UnitOfWork.GetRepo<IStudentPacketCourseRepo>().GetByFPCSCourse(course.FPCSCourseId, schoolYearId).Count();
            course.HoursChargedPerStudent = course.EnrolledCount > 0
                ? course.TotalHours/course.EnrolledCount
                : course.TotalHours;
            course.TeacherCostPerStudent = course.HoursChargedPerStudent*teacherHourlyRate;
            course.TotalCourseCost = course.TotalHours*teacherHourlyRate;
            course.UpdatedDate = DateTimeOffset.Now;

            return course;
        }

        private void ValidateDates(SchoolYear schoolYear, DateTimeOffset classStartDate, DateTimeOffset classEndDate)
        {
            if (classStartDate >= classEndDate) throw new ParamsEntityException("Class Start Date greater than Class End Date");
            if (classStartDate < schoolYear.StartDate) throw new ParamsEntityException("Class Start Date less than {0}", schoolYear.StartDate);
            if (classEndDate > schoolYear.EndDate) throw new ParamsEntityException("Class End Date greater than {0}", schoolYear.EndDate);
        }

        private Semester GetSemester(SchoolYear schoolYear, DateTimeOffset classStartDate, DateTimeOffset classEndDate)
        {
            if (classStartDate >= schoolYear.StartDate && classStartDate <= schoolYear.HalfDate &&
                classEndDate >= schoolYear.StartDate && classEndDate <= schoolYear.HalfDate) return Semester.Semester1;

            if (classStartDate >= schoolYear.HalfDate && classStartDate <= schoolYear.EndDate &&
                classEndDate >= schoolYear.HalfDate && classEndDate <= schoolYear.EndDate) return Semester.Semester2;

            return Semester.Semester1; // must be AllYear
        }

        private DateTimeOffset GetStartSemester(SchoolYear schoolYear, Semester semester)
        {
            if (semester == Semester.Semester1)
            {
                return schoolYear.StartDate;
            }
            else // if (semester == Semester.Semester2)
            {
                return schoolYear.HalfDate.AddDays(-1);
            }
        }

        private DateTimeOffset GetEndSemester(SchoolYear schoolYear, Semester semester)
        {
            if (semester == Semester.Semester1)
            {
                return schoolYear.HalfDate;
            }
            else // if (semester == Semester.Semester2)
            {
                return schoolYear.EndDate;
            }
        }
    }
}
