﻿using System;
using System.Linq;

using FPCS.Data.Entities;
using FPCS.Data.Enums;
using FPCS.Data.Exceptions;

namespace FPCS.Data.Repo.Impl
{
    internal class GoodServiceBalanceRepo : RepoBase<GoodServiceBalance, StudentManagementContext>, IGoodServiceBalanceRepo
    {
        public GoodServiceBalanceRepo(UnitOfWork<StudentManagementContext> unitOfWork) : base(unitOfWork) { }

        public override IQueryable<GoodServiceBalance> GetAll()
        {
            return base.GetAll().Where(x => !x.GoodService.IsDeleted &&
                                            ((
                                                x.GoodService.StudentPacketCourse != null &&
                                                !x.GoodService.StudentPacketCourse.IsDeleted &&
                                                !x.GoodService.StudentPacketCourse.StudentPacket.IsDeleted &&
                                                !x.GoodService.StudentPacketCourse.StudentPacket.Student.IsDeleted &&
                                                (x.GoodService.StudentPacketCourse.FPCSCourse.Semester == Semester.Semester1 ||
								x.GoodService.StudentPacketCourse.FPCSCourse.Semester == Semester.Semester2 ||
								x.GoodService.StudentPacketCourse.FPCSCourse.Semester == Semester.Summer)
                                            ) ||
                                            (
                                                x.GoodService.FPCSCourse != null &&
                                                !x.GoodService.FPCSCourse.IsDeleted &&
                                                (x.GoodService.FPCSCourse.Semester == Semester.Semester1 ||
								x.GoodService.FPCSCourse.Semester == Semester.Semester2 ||
								x.GoodService.FPCSCourse.Semester == Semester.Summer)
                                            ) ||
                                            (
                                                x.GoodService.StudentPacket != null &&
                                                !x.GoodService.StudentPacket.IsDeleted &&
                                                !x.GoodService.StudentPacket.Student.IsDeleted
                                            )));
        }

        public IQueryable<GoodServiceBalance> GetByGoodService(Int64 goodServiceId)
        {
            return GetAll().Where(x => x.GoodService.GoodServiceId == goodServiceId);
        }

        public IQueryable<GoodServiceBalance> GetByStudentPacket(Int64 studentPacketId)
        {
            return GetAll().Where(x => x.GoodService.StudentPacketCourse.StudentPacketId == studentPacketId &&
                                       x.GoodService.IsDeleted == false);
        }

        public IQueryable<GoodServiceBalance> GetByStudentPacketCourseOrFPCSCourse(Int64 studentPacketCourseId, Int64 fpcsCourseId)
        {
            return GetAll().Where(x => (x.GoodService.StudentPacketCourseId == studentPacketCourseId ||
                                             x.GoodService.FPCSCourseId == fpcsCourseId) && x.GoodService.IsDeleted == false);
        }

	  public IQueryable<GoodServiceBalance> GetByStudentPacketCourseOrFPCSCourseOrStudentPacket(Int64 studentPacketCourseId, Int64 fpcsCourseId, Int64 studentPacketId, Guid tempId)
	  {
		  var strTempId = tempId.ToString();
		  return GetAll().Where(x => (x.GoodService.StudentPacketCourseId == studentPacketCourseId ||
							     x.GoodService.FPCSCourseId == fpcsCourseId ||
							     (studentPacketCourseId == 0 && fpcsCourseId == 0 && x.GoodService.StudentPacketId == studentPacketId) ||
							     (studentPacketCourseId == 0 && fpcsCourseId == 0 && studentPacketId == 0 && x.GoodService.TempId == strTempId)) 
								&& x.GoodService.IsDeleted == false);
        }

        public IQueryable<GoodServiceBalance> GetByPacket(Int64 studentPacketId)
        {
            return GetAll().Where(x => x.GoodService.StudentPacketId == studentPacketId &&
                                       x.GoodService.IsDeleted == false);
        }
    }
}
