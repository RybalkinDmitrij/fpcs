﻿using System;
using System.Linq;

using FPCS.Data.Entities;
using FPCS.Data.Exceptions;
using System.Collections.Generic;

namespace FPCS.Data.Repo.Impl
{
    internal class StudentPacketCourseAlertRepo : RepoBase<StudentPacketCourseAlert, StudentManagementContext>, IStudentPacketCourseAlertRepo
    {
        public StudentPacketCourseAlertRepo(UnitOfWork<StudentManagementContext> unitOfWork) : base(unitOfWork) { }

    }
}
