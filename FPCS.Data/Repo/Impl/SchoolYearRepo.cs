﻿using System;
using System.Linq;
using FPCS.Data.Entities;
using FPCS.Data.Exceptions;

namespace FPCS.Data.Repo.Impl
{
	internal class SchoolYearRepo : RepoBase<SchoolYear, StudentManagementContext>, ISchoolYearRepo
	{
		public SchoolYearRepo(UnitOfWork<StudentManagementContext> unitOfWork) : base(unitOfWork)
		{
		}

		public SchoolYear GetByYear(Int32 year)
		{
			return DbSet.FirstOrDefault(x => x.Year == year);
		}

		public SchoolYear GetByDate(DateTimeOffset date, Boolean nearestIfNotFound = false)
		{
			var entity = DbSet.FirstOrDefault(x => x.StartDate <= date && date <= x.EndDate);
			if (entity == null && nearestIfNotFound)
			{
				return DbSet.LastOrDefault(x => x.StartDate <= date);
			}
			return entity;
		}

		public SchoolYear GetByASDCourse(Int32 asdCourseId)
		{
			return GetAll().FirstOrDefault(x => x.ASDCourses.Any(x2 => x2.ASDCourseId == asdCourseId));
		}

		public SchoolYear AddCurrentYear()
		{
			var curDate = DateTimeOffset.UtcNow;
			var curYear = curDate.Month > 5 ? curDate.Year + 1 : curDate.Year;
			return AddByYear(curYear);
		}

		public SchoolYear AddByYear(Int32 year)
		{
			var startDate = new DateTimeOffset(year - 1, 6, 1, 0, 0, 0, TimeSpan.Zero);
			var endDate = new DateTimeOffset(year, 6, 1, 0, 0, 0, TimeSpan.Zero);
			var halfDate = new DateTimeOffset(year, 1, 1, 0, 0, 0, TimeSpan.Zero);

			return Add(startDate, endDate, halfDate, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
				4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 1440, 7.5m, 0m, 78, 17580, 0.00739m, 0.001445m, 0.062m, 94200, 0m,
				0.019m, 0.5m, 0.4m, 0.1256m, 0.22m, 0, false, false, "");
		}

		public SchoolYear Add(DateTimeOffset startDate, DateTimeOffset endDate, DateTimeOffset halfDate,
			Int32 fundingAmountGradeK, Int32 fundingAmountGrade1, Int32 fundingAmountGrade2, Int32 fundingAmountGrade3,
			Int32 fundingAmountGrade4,
			Int32 fundingAmountGrade5, Int32 fundingAmountGrade6, Int32 fundingAmountGrade7, Int32 fundingAmountGrade8,
			Int32 fundingAmountGrade9,
			Int32 fundingAmountGrade10, Int32 fundingAmountGrade11, Int32 fundingAmountGrade12, Int32 fundingAmountGradeG,
			Int32 coreUnitGradeK, Int32 coreUnitGrade1, Int32 coreUnitGrade2, Int32 coreUnitGrade3, Int32 coreUnitGrade4,
			Int32 coreUnitGrade5, Int32 coreUnitGrade6, Int32 coreUnitGrade7, Int32 coreUnitGrade8, Int32 coreUnitGrade9,
			Int32 coreUnitGrade10, Int32 coreUnitGrade11, Int32 coreUnitGrade12, Int32 coreUnitGradeG,
			Int32 electiveUnitGradeK, Int32 electiveUnitGrade1, Int32 electiveUnitGrade2, Int32 electiveUnitGrade3,
			Int32 electiveUnitGrade4,
			Int32 electiveUnitGrade5, Int32 electiveUnitGrade6, Int32 electiveUnitGrade7, Int32 electiveUnitGrade8,
			Int32 electiveUnitGrade9,
			Int32 electiveUnitGrade10, Int32 electiveUnitGrade11, Int32 electiveUnitGrade12, Int32 electiveUnitGradeG,
			Int32 ilpHoursLimit, Decimal countHoursWorkDay, Decimal plusBudgetLimit, Decimal bookMaterialFee,
			Decimal groupLifeInsurance, Decimal groupMedicalInsurance, Decimal workersComp,
			Decimal unemployment, Decimal fica, Decimal ficaCap, Decimal medicare, Decimal tersBaseContract,
			Decimal persBaseContract,
			Decimal tersRetirement, Decimal persRetirement, Decimal generalSupplies, Boolean disableAccessForParents,
			Boolean disableAccessForTeachers, String disableMessage)
		{
			if (IsExistYear(endDate.Year)) throw new DuplicateEntityException("The year already exist");
			if (!IsValidNewDateRange(startDate, endDate)) throw new InvalidRangeException("Invalid date range");

			return Add(new SchoolYear
			{
				StartDate = new DateTimeOffset(startDate.Year, startDate.Month, startDate.Day, 0, 0, 0, 0, TimeSpan.Zero),
				EndDate = new DateTimeOffset(endDate.Year, endDate.Month, endDate.Day, 23, 59, 59, 999, TimeSpan.Zero),
				HalfDate = new DateTimeOffset(halfDate.Year, halfDate.Month, halfDate.Day, 0, 0, 0, 0, TimeSpan.Zero),
				ILPHoursLimit = ilpHoursLimit,
				CountHoursWorkDay = countHoursWorkDay,
				PlusBudgetLimit = plusBudgetLimit,
				BookMaterialFee = bookMaterialFee,
				GroupLifeInsurance = groupLifeInsurance,
				GroupMedicalInsurance = groupMedicalInsurance,
				WorkersComp = workersComp,
				Unemployment = unemployment,
				FICA = fica,
				FICACap = ficaCap,
				Medicare = medicare,
				TERSBaseContract = tersBaseContract,
				PERSBaseContract = persBaseContract,
				TERSRetirement = tersRetirement,
				PERSRetirement = persRetirement,
				GeneralSupplies = generalSupplies,
				FundingAmountGradeK = fundingAmountGradeK,
				FundingAmountGrade1 = fundingAmountGrade1,
				FundingAmountGrade2 = fundingAmountGrade2,
				FundingAmountGrade3 = fundingAmountGrade3,
				FundingAmountGrade4 = fundingAmountGrade4,
				FundingAmountGrade5 = fundingAmountGrade5,
				FundingAmountGrade6 = fundingAmountGrade6,
				FundingAmountGrade7 = fundingAmountGrade7,
				FundingAmountGrade8 = fundingAmountGrade8,
				FundingAmountGrade9 = fundingAmountGrade9,
				FundingAmountGrade10 = fundingAmountGrade10,
				FundingAmountGrade11 = fundingAmountGrade11,
				FundingAmountGrade12 = fundingAmountGrade12,
				FundingAmountGradeG = fundingAmountGradeG,
				CoreUnitGradeK = coreUnitGradeK,
				CoreUnitGrade1 = coreUnitGrade1,
				CoreUnitGrade2 = coreUnitGrade2,
				CoreUnitGrade3 = coreUnitGrade3,
				CoreUnitGrade4 = coreUnitGrade4,
				CoreUnitGrade5 = coreUnitGrade5,
				CoreUnitGrade6 = coreUnitGrade6,
				CoreUnitGrade7 = coreUnitGrade7,
				CoreUnitGrade8 = coreUnitGrade8,
				CoreUnitGrade9 = coreUnitGrade9,
				CoreUnitGrade10 = coreUnitGrade10,
				CoreUnitGrade11 = coreUnitGrade11,
				CoreUnitGrade12 = coreUnitGrade12,
				CoreUnitGradeG = coreUnitGradeG,
				ElectiveUnitGradeK = electiveUnitGradeK,
				ElectiveUnitGrade1 = electiveUnitGrade1,
				ElectiveUnitGrade2 = electiveUnitGrade2,
				ElectiveUnitGrade3 = electiveUnitGrade3,
				ElectiveUnitGrade4 = electiveUnitGrade4,
				ElectiveUnitGrade5 = electiveUnitGrade5,
				ElectiveUnitGrade6 = electiveUnitGrade6,
				ElectiveUnitGrade7 = electiveUnitGrade7,
				ElectiveUnitGrade8 = electiveUnitGrade8,
				ElectiveUnitGrade9 = electiveUnitGrade9,
				ElectiveUnitGrade10 = electiveUnitGrade10,
				ElectiveUnitGrade11 = electiveUnitGrade11,
				ElectiveUnitGrade12 = electiveUnitGrade12,
				ElectiveUnitGradeG = electiveUnitGradeG,
				DisableAccessForParents = disableAccessForParents,
				DisableAccessForTeachers = disableAccessForTeachers,
				DisableMessage = disableMessage,
				CreatedDate = DateTimeOffset.Now,
				UpdatedDate = DateTimeOffset.Now,
				Year = endDate.Year,
				Name = String.Format(@"{0} - {1}", startDate.Year.ToString(), endDate.Year.ToString())
			});
		}

		public SchoolYear Update(Int32 schoolYearId, DateTimeOffset startDate, DateTimeOffset endDate, DateTimeOffset halfDate,
			Int32 fundingAmountGradeK, Int32 fundingAmountGrade1, Int32 fundingAmountGrade2, Int32 fundingAmountGrade3,
			Int32 fundingAmountGrade4,
			Int32 fundingAmountGrade5, Int32 fundingAmountGrade6, Int32 fundingAmountGrade7, Int32 fundingAmountGrade8,
			Int32 fundingAmountGrade9,
			Int32 fundingAmountGrade10, Int32 fundingAmountGrade11, Int32 fundingAmountGrade12, Int32 fundingAmountGradeG,
			Int32 coreUnitGradeK, Int32 coreUnitGrade1, Int32 coreUnitGrade2, Int32 coreUnitGrade3, Int32 coreUnitGrade4,
			Int32 coreUnitGrade5, Int32 coreUnitGrade6, Int32 coreUnitGrade7, Int32 coreUnitGrade8, Int32 coreUnitGrade9,
			Int32 coreUnitGrade10, Int32 coreUnitGrade11, Int32 coreUnitGrade12, Int32 coreUnitGradeG,
			Int32 electiveUnitGradeK, Int32 electiveUnitGrade1, Int32 electiveUnitGrade2, Int32 electiveUnitGrade3,
			Int32 electiveUnitGrade4,
			Int32 electiveUnitGrade5, Int32 electiveUnitGrade6, Int32 electiveUnitGrade7, Int32 electiveUnitGrade8,
			Int32 electiveUnitGrade9,
			Int32 electiveUnitGrade10, Int32 electiveUnitGrade11, Int32 electiveUnitGrade12, Int32 electiveUnitGradeG,
			Int32 ilpHoursLimit, Decimal countHoursWorkDay, Decimal plusBudgetLimit, Decimal bookMaterialFee,
			Decimal groupLifeInsurance, Decimal groupMedicalInsurance, Decimal workersComp,
			Decimal unemployment, Decimal fica, Decimal ficaCap, Decimal medicare, Decimal tersBaseContract,
			Decimal persBaseContract,
			Decimal tersRetirement, Decimal persRetirement, Decimal generalSupplies, Boolean disableAccessForParents,
			Boolean disableAccessForTeachers, String disableMessage)
		{
			if (IsExistYear(schoolYearId, endDate.Year)) throw new DuplicateEntityException("The year already exist");
			if (!IsValidNewDateRange(schoolYearId, startDate, endDate)) throw new InvalidRangeException("Invalid date range");

			var entity = Get(schoolYearId);
			if (entity == null) throw new NotFoundEntityException("SchoolYear not found");

			entity.StartDate = new DateTimeOffset(startDate.Year, startDate.Month, startDate.Day, 0, 0, 0, 0, TimeSpan.Zero);
			entity.EndDate = new DateTimeOffset(endDate.Year, endDate.Month, endDate.Day, 23, 59, 59, 999, TimeSpan.Zero);
			entity.HalfDate = new DateTimeOffset(halfDate.Year, halfDate.Month, halfDate.Day, 0, 0, 0, 0, TimeSpan.Zero);

			entity.ILPHoursLimit = ilpHoursLimit;
			entity.CountHoursWorkDay = countHoursWorkDay;
			entity.PlusBudgetLimit = plusBudgetLimit;
			entity.BookMaterialFee = bookMaterialFee;
			entity.GroupLifeInsurance = groupLifeInsurance;
			entity.GroupMedicalInsurance = groupMedicalInsurance;
			entity.WorkersComp = workersComp;
			entity.Unemployment = unemployment;
			entity.FICA = fica;
			entity.FICACap = ficaCap;
			entity.Medicare = medicare;
			entity.TERSBaseContract = tersBaseContract;
			entity.PERSBaseContract = persBaseContract;
			entity.TERSRetirement = tersRetirement;
			entity.PERSRetirement = persRetirement;

			entity.GeneralSupplies = generalSupplies;

			entity.FundingAmountGradeK = fundingAmountGradeK;
			entity.FundingAmountGrade1 = fundingAmountGrade1;
			entity.FundingAmountGrade2 = fundingAmountGrade2;
			entity.FundingAmountGrade3 = fundingAmountGrade3;
			entity.FundingAmountGrade4 = fundingAmountGrade4;
			entity.FundingAmountGrade5 = fundingAmountGrade5;
			entity.FundingAmountGrade6 = fundingAmountGrade6;
			entity.FundingAmountGrade7 = fundingAmountGrade7;
			entity.FundingAmountGrade8 = fundingAmountGrade8;
			entity.FundingAmountGrade9 = fundingAmountGrade9;
			entity.FundingAmountGrade10 = fundingAmountGrade10;
			entity.FundingAmountGrade11 = fundingAmountGrade11;
			entity.FundingAmountGrade12 = fundingAmountGrade12;
			entity.FundingAmountGradeG = fundingAmountGradeG;

			entity.CoreUnitGradeK = coreUnitGradeK;
			entity.CoreUnitGrade1 = coreUnitGrade1;
			entity.CoreUnitGrade2 = coreUnitGrade2;
			entity.CoreUnitGrade3 = coreUnitGrade3;
			entity.CoreUnitGrade4 = coreUnitGrade4;
			entity.CoreUnitGrade5 = coreUnitGrade5;
			entity.CoreUnitGrade6 = coreUnitGrade6;
			entity.CoreUnitGrade7 = coreUnitGrade7;
			entity.CoreUnitGrade8 = coreUnitGrade8;
			entity.CoreUnitGrade9 = coreUnitGrade9;
			entity.CoreUnitGrade10 = coreUnitGrade10;
			entity.CoreUnitGrade11 = coreUnitGrade11;
			entity.CoreUnitGrade12 = coreUnitGrade12;
			entity.CoreUnitGradeG = coreUnitGradeG;

			entity.ElectiveUnitGradeK = electiveUnitGradeK;
			entity.ElectiveUnitGrade1 = electiveUnitGrade1;
			entity.ElectiveUnitGrade2 = electiveUnitGrade2;
			entity.ElectiveUnitGrade3 = electiveUnitGrade3;
			entity.ElectiveUnitGrade4 = electiveUnitGrade4;
			entity.ElectiveUnitGrade5 = electiveUnitGrade5;
			entity.ElectiveUnitGrade6 = electiveUnitGrade6;
			entity.ElectiveUnitGrade7 = electiveUnitGrade7;
			entity.ElectiveUnitGrade8 = electiveUnitGrade8;
			entity.ElectiveUnitGrade9 = electiveUnitGrade9;
			entity.ElectiveUnitGrade10 = electiveUnitGrade10;
			entity.ElectiveUnitGrade11 = electiveUnitGrade11;
			entity.ElectiveUnitGrade12 = electiveUnitGrade12;
			entity.ElectiveUnitGradeG = electiveUnitGradeG;

			entity.DisableAccessForParents = disableAccessForParents;
			entity.DisableAccessForTeachers = disableAccessForTeachers;
			entity.DisableMessage = disableMessage;

			entity.Year = endDate.Year;
			entity.UpdatedDate = DateTimeOffset.Now;
			entity.Name = startDate.Year.ToString() + " - " + endDate.Year.ToString();

			return entity;
		}

		#region private methods

		private Boolean IsExistYear(Int32 year)
		{
			return this.IsExist(x => x.Year == year);
		}

		private Boolean IsExistYear(Int32 currentSchoolYearId, Int32 year)
		{
			return this.IsExist(x => x.SchoolYearId != currentSchoolYearId && x.Year == year);
		}

		private Boolean IsValidNewDateRange(DateTimeOffset startDate, DateTimeOffset endDate)
		{
			return GetAll()
				.ToList()
				.Count(x => x.StartDate.DateTime <= startDate.DateTime && startDate.DateTime <= x.EndDate.DateTime ||
				            x.StartDate.DateTime <= endDate.DateTime && endDate.DateTime <= x.EndDate.DateTime) == 0;
		}

		private Boolean IsValidNewDateRange(Int32 currentSchoolYearId, DateTimeOffset startDate, DateTimeOffset endDate)
		{
			return GetAll()
				.ToList()
				.Count(x => x.SchoolYearId != currentSchoolYearId &&
				            (x.StartDate.DateTime <= startDate.DateTime && startDate.DateTime <= x.EndDate.DateTime ||
				             x.StartDate.DateTime <= endDate.DateTime && endDate.DateTime <= x.EndDate.DateTime)) == 0;
		}

		#endregion private methods
	}
}
