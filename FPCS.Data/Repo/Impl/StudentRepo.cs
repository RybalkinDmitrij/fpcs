﻿using System;
using System.Linq;
using System.Collections.Generic;

using FPCS.Data.Entities;
using FPCS.Data.Enums;
using FPCS.Data.Exceptions;

using FPCS.Core.Extensions;

namespace FPCS.Data.Repo.Impl
{
    internal class StudentRepo : RepoBase<Student, StudentManagementContext>, IStudentRepo
    {
        public StudentRepo(UnitOfWork<StudentManagementContext> unitOfWork) : base(unitOfWork) { }

        #region override methods

        public override IQueryable<Student> GetAll()
        {
            return DbSet.Where(x => !x.IsDeleted);
        }

        public override void Remove(Student entity)
        {
            entity.IsDeleted = true;
            entity.UpdatedDate = DateTimeOffset.Now;
            entity.Families.Clear();
            UnitOfWork.GetRepo<IFamilyRelationshipRepo>().RemoveStudent(entity.DbUserId);

            entity.FromTransfers.Clear();
            entity.ToTransfers.Clear();
            
            var transfers = UnitOfWork.GetRepo<ITransferRepo>()
                                      .GetByStudent(entity.DbUserId)
                                      .ToList();

            foreach (var item in transfers)
            {
                UnitOfWork.GetRepo<ITransferRepo>().Remove(item.TransferId);
            }
        }

        #endregion override methods

        public IQueryable<Student> GetAll(Int32 schoolYearId)
        {
            return GetAll().Where(x => x.SchoolYearId == schoolYearId);
        }

        public Student Get(Guid Id)
        {
            return GetAll().FirstOrDefault(x => x.DbUserId == Id);
        }

        public IQueryable<Student> GetWithoutFamily(Int64 familyId, Int32 schoolYearId)
        {
            return GetAll(schoolYearId).Where(x => !x.Families.Any(x2 => x2.FamilyId == familyId));
        }

        public IQueryable<Student> GetByFamily(Int64 familyId, Int32 schoolYearId)
        {
            return GetAll(schoolYearId).Where(x => x.Families.Any(x2 => x2.FamilyId == familyId));
        }

        public IQueryable<Student> GetByUser(Guid dbUserId, Role role, Int32 schoolYearId)
        {
            if (role == Role.Guardian)
            {
                Guardian guardian = UnitOfWork.GetRepo<IGuardianRepo>().Get(dbUserId);
                Int64 familyId = guardian.Families.FirstOrDefault(x => !x.IsDeleted) != null ?
                    guardian.Families.FirstOrDefault(x => !x.IsDeleted).FamilyId : 0;

                return GetByFamily(familyId, schoolYearId);
            }
            else if (role == Role.Teacher)
            {
                return UnitOfWork.GetRepo<IStudentPacketRepo>()
                            .GetBySchoolYear(schoolYearId)
                            .Where(x => x.SponsorTeacherId == dbUserId ||
                                        x.StudentPacketCourses.Select(t => t.FPCSCourse.TeacherId).Contains(dbUserId))
                            .Select(x => x.Student);
            }
            else
            {
                return GetAll(schoolYearId);
            }
        }

        public Student Add(String firstName, String lastName, String middleInitial, String email, Int32 gradYear, DateTimeOffset dateOfBirth, DateTimeOffset? withdrawalDate,
            DateTimeOffset? enrollmentDate, String privateSchoolName, Int32 percentInSchoolDistrict, Boolean isGraduateFromFPCS, Boolean isASDContractHoursExemption,
            String reasonASDContractHoursExemption, Sex sex, Grade grade, EnrollmentStatus? enrollmentStatus, String zangleId, Int32? percentagePlanningEnroll,
            Int32? coreCreditExemption, String coreCreditExemptionReason, Int32? electiveCreditExemption, String electiveCreditExemptionReason,
            Boolean isBirthCertificate, Boolean isGradesNotSubmitted, Boolean isILPPhilosophy, Boolean isMedicalRelease, Boolean isProgressReportSignature,
            Boolean isShotRecords, Boolean isTestingAgreement, Boolean isOther, Int32 schoolYearId
        )
        {
            var student =
                Add(
                new Student
                    {
                        Login = this.GetSimpleKey(),
                        Password = this.GetSimpleKey(),
                        FirstName = firstName.TrimAndReduce(),
                        LastName = lastName.TrimAndReduce(),
                        FullName = lastName.TrimAndReduce() + " " + firstName.TrimAndReduce(),
                        MiddleInitial = middleInitial.TrimAndReduce(),
                        Email = email.TrimAndReduce(),
                        GradYear = gradYear,
                        DateOfBirth = dateOfBirth,
                        WithdrawalDate = withdrawalDate,
                        EnrollmentDate = enrollmentDate,
                        PrivateSchoolName = privateSchoolName.TrimAndReduce(),
                        PercentInSchoolDistrict = percentInSchoolDistrict,
                        IsGraduateFromFPCS = isGraduateFromFPCS,
                        IsASDContractHoursExemption = isASDContractHoursExemption,
                        ReasonASDContractHoursExemption = reasonASDContractHoursExemption.TrimAndReduce(),
                        CreatedDate = DateTimeOffset.Now,
                        UpdatedDate = DateTimeOffset.Now,
                        IsDeleted = false,
                        IsLocked = true,
                        Sex = sex,
                        Grade = grade,
                        EnrollmentStatus = enrollmentStatus,
                        ZangleID = zangleId,
                        Role = Role.Student,
                        PercentagePlanningEnroll = percentagePlanningEnroll,
                        CoreCreditExemption = coreCreditExemption,
                        CoreCreditExemptionReason = coreCreditExemptionReason.TrimAndReduce(),
                        ElectiveCreditExemption = electiveCreditExemption,
                        ElectiveCreditExemptionReason = electiveCreditExemptionReason.TrimAndReduce(),
                        IsBirthCertificate = isBirthCertificate,
                        IsGradesNotSubmitted = isGradesNotSubmitted,
                        IsILPPhilosophy = isILPPhilosophy,
                        IsMedicalRelease = isMedicalRelease,
                        IsProgressReportSignature = isProgressReportSignature,
                        IsShotRecords = isShotRecords,
                        IsTestingAgreement = isTestingAgreement,
                        IsOther = isOther,
                        SchoolYearId = schoolYearId
                    });

            return student;
        }

        public Student Update(Guid dbUserId, String firstName, String lastName, String middleInitial, String email, Int32 gradYear, DateTimeOffset dateOfBirth,
            DateTimeOffset? withdrawalDate, DateTimeOffset? enrollmentDate, String privateSchoolName, Int32 percentInSchoolDistrict, Boolean isGraduateFromFPCS, Boolean isASDContractHoursExemption,
            String reasonASDContractHoursExemption, Sex sex, Grade grade, EnrollmentStatus? enrollmentStatus, String zangleId, Int32? percentagePlanningEnroll,
            Int32? coreCreditExemption, String coreCreditExemptionReason, Int32? electiveCreditExemption, String electiveCreditExemptionReason,
            Boolean isBirthCertificate, Boolean isGradesNotSubmitted, Boolean isILPPhilosophy, Boolean isMedicalRelease, Boolean isProgressReportSignature,
            Boolean isShotRecords, Boolean isTestingAgreement, Boolean isOther)
        {
            var dbUser = this.Get(dbUserId);
            if (dbUser == null) throw new NotFoundEntityException("Student not found");

            dbUser.FirstName = firstName.TrimAndReduce();
            dbUser.LastName = lastName.TrimAndReduce();
            dbUser.FullName = lastName.TrimAndReduce() + " " + firstName.TrimAndReduce();
            dbUser.MiddleInitial = middleInitial.TrimAndReduce();
            dbUser.Email = email.TrimAndReduce();
            dbUser.GradYear = gradYear;
            dbUser.DateOfBirth = dateOfBirth;
            dbUser.WithdrawalDate = withdrawalDate;
            dbUser.EnrollmentDate = enrollmentDate;
            dbUser.PrivateSchoolName = privateSchoolName.TrimAndReduce();
            dbUser.PercentInSchoolDistrict = percentInSchoolDistrict;
            dbUser.IsGraduateFromFPCS = isGraduateFromFPCS;
            dbUser.IsASDContractHoursExemption = isASDContractHoursExemption;
            dbUser.ReasonASDContractHoursExemption = reasonASDContractHoursExemption.TrimAndReduce();
            dbUser.UpdatedDate = DateTimeOffset.Now;
            dbUser.Sex = sex;
            dbUser.Grade = grade;
            dbUser.EnrollmentStatus = enrollmentStatus;
            dbUser.ZangleID = zangleId;
            if (percentagePlanningEnroll.HasValue)
            {
                dbUser.PercentagePlanningEnroll = percentagePlanningEnroll;
            }
            dbUser.CoreCreditExemption = coreCreditExemption;
            dbUser.CoreCreditExemptionReason = coreCreditExemptionReason.TrimAndReduce();
            dbUser.ElectiveCreditExemption = electiveCreditExemption;
            dbUser.ElectiveCreditExemptionReason = electiveCreditExemptionReason.TrimAndReduce();
            dbUser.IsBirthCertificate = isBirthCertificate;
            dbUser.IsGradesNotSubmitted = isGradesNotSubmitted;
            dbUser.IsILPPhilosophy = isILPPhilosophy;
            dbUser.IsMedicalRelease = isMedicalRelease;
            dbUser.IsProgressReportSignature = isProgressReportSignature;
            dbUser.IsShotRecords = isShotRecords;
            dbUser.IsTestingAgreement = isTestingAgreement;
            dbUser.IsOther = isOther;

            return dbUser;
        }
    }
}
