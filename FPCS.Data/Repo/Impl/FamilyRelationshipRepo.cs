﻿using System;
using System.Linq;
using System.Collections.Generic;

using FPCS.Data.Entities;
using FPCS.Data.Enums;
using FPCS.Data.Exceptions;

namespace FPCS.Data.Repo.Impl
{
    internal class FamilyRelationshipRepo : RepoBase<FamilyRelationship, StudentManagementContext>, IFamilyRelationshipRepo
    {
        public FamilyRelationshipRepo(UnitOfWork<StudentManagementContext> unitOfWork) : base(unitOfWork) { }

        public FamilyRelationship Get(Guid guardianDbUserId, Guid studentDbUserId)
        {
            return DbSet.FirstOrDefault(x => x.Guardian.DbUserId == guardianDbUserId && x.Student.DbUserId == studentDbUserId);
        }

        public FamilyRelationship Add(Guid guardianDbUserId, Guid studentDbUserId, FamilyRelationshipType familyRelationshipType)
        {
            var guardian = UnitOfWork.GetRepo<IGuardianRepo>().Get(guardianDbUserId);
            var student = UnitOfWork.GetRepo<IStudentRepo>().Get(studentDbUserId);
            if (guardian == null) throw new NotFoundEntityException("Guardian {0} not found", guardianDbUserId);
            if (student == null) throw new NotFoundEntityException("Student {0} not found", studentDbUserId);

            return this.Add(guardian, student, familyRelationshipType);
        }

        public FamilyRelationship Add(Guardian guardian, Student student, FamilyRelationshipType familyRelationshipType)
        {
            return this.Add(new FamilyRelationship
            {
                Guardian = guardian,
                Student = student,
                FamilyRelationshipType = familyRelationshipType
            });
        }

        public IEnumerable<FamilyRelationship> AddGuardian(Guardian guardian, IEnumerable<Student> students, FamilyRelationshipType familyRelationshipType)
        {
            var list = new List<FamilyRelationship>();
            foreach (var student in students)
            {
                var dbEntity = Get(guardian.DbUserId, student.DbUserId);
                if (dbEntity == null)
                {
                    var entity = Add(guardian, student, familyRelationshipType);
                    list.Add(entity);
                }
            }
            return list;
        }

        public IEnumerable<FamilyRelationship> AddStudent(Student student, IEnumerable<Guardian> guardians, FamilyRelationshipType familyRelationshipType)
        {
            var list = new List<FamilyRelationship>();
            foreach (var guardian in guardians)
            {
                var dbEntity = Get(guardian.DbUserId, student.DbUserId);
                if (dbEntity == null)
                {
                    var entity = Add(guardian, student, familyRelationshipType);
                    list.Add(entity);
                }
            }
            return list;
        }

        public FamilyRelationship Update(Guid guardianDbUserId, Guid studentDbUserId, FamilyRelationshipType familyRelationshipType)
        {
            var entity = Get(guardianDbUserId, studentDbUserId);
            if (entity == null) throw new NotFoundEntityException("Family not found. Arguments {0} {1}", guardianDbUserId, studentDbUserId);

            entity.FamilyRelationshipType = familyRelationshipType;
            return entity;
        }

        public FamilyRelationship Update(Int64 studentGuardianId, FamilyRelationshipType familyRelationshipType)
        {
            var entity = Get(studentGuardianId);
            if (entity == null) throw new NotFoundEntityException("Family {0} not found", studentGuardianId);

            entity.FamilyRelationshipType = familyRelationshipType;
            return entity;
        }

        public void RemoveGuardian(Guid dbUserId)
        {
            var list = DbSet.Where(x => x.Guardian.DbUserId == dbUserId).ToList();
            Remove(list);
        }

        public void RemoveStudent(Guid dbUserId)
        {
            var list = DbSet.Where(x => x.Student.DbUserId == dbUserId).ToList();
            Remove(list);
        }

        public void RemoveGuardian(Guid dbUserId, IEnumerable<Guid> studentDbUserIds)
        {
            var list = DbSet.Where(x => x.Guardian.DbUserId == dbUserId && studentDbUserIds.Contains(x.Student.DbUserId)).ToList();
            Remove(list);
        }

        public void RemoveStudent(Guid dbUserId, IEnumerable<Guid> guardianDbUserIds)
        {
            var list = DbSet.Where(x => x.Student.DbUserId == dbUserId && guardianDbUserIds.Contains(x.Guardian.DbUserId)).ToList();
            Remove(list);
        }

        public void RemoveGuardiansStudents(IEnumerable<Guid> guardianDbUserIds, IEnumerable<Guid> studentDbUserIds)
        {
            var list = DbSet.Where(x => guardianDbUserIds.Contains(x.Guardian.DbUserId) && studentDbUserIds.Contains(x.Student.DbUserId)).ToList();
            Remove(list);
        }
    }
}
