﻿using System;
using System.Linq;
using FPCS.Data.Entities;
using FPCS.Data.Enums;
using System.Collections.Generic;

namespace FPCS.Data.Repo
{
	public interface IStudentPacketCourseRepo : IRepoBase<StudentPacketCourse>
	{
		IQueryable<StudentPacketCourse> GetAll(Int32 schoolYearId);

		StudentPacketCourse Get(Int64 studentPacketCourseId);

		IQueryable<StudentPacketCourse> GetByStudentPacket(Int64 studentPacketId);

		IQueryable<StudentPacketCourse> GetByStudent(Guid studentId, Int32 schoolYearId);

		StudentPacketCourse Add(Int64 studentPacketId, Int64 fpcsCourseId);

		IQueryable<StudentPacketCourse> GetByFPCSCourse(Int64 fpcsCourseId, Int32 schoolYearId);

		IQueryable<StudentPacketCourse> GetByFPCSCourse(List<Int64> fpcsCourseIds, Int32 schoolYearId);

		IQueryable<StudentPacketCourse> GetByInstructor(Guid instructorId, Int32 schoolYearId);

		IQueryable<StudentPacketCourse> GetByUser(Guid userId, Role role, Int32 schoolYearId);

		IQueryable<Student> GetStudentsByFPCSCourse(Int64 fpcsCourseId, Int32 schoolYearId);

		IQueryable<Student> GetStudentsByInstructor(Guid instructorId, Int32 schoolYearId);

		IQueryable<Student> GetStudentsByInstructorAndFPCSCourse(Guid? instructorId, Int64? fpcsCourse, Int32 schoolYearId);
	}
}
