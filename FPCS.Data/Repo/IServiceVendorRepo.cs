﻿using System;

using FPCS.Data.Entities;

namespace FPCS.Data.Repo
{
    public interface IServiceVendorRepo : IRepoBase<ServiceVendor>
    {
    }
}
