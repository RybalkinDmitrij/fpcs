﻿using System;

using FPCS.Data.Entities;
using System.Linq;

namespace FPCS.Data.Repo
{
    public interface IGoodServiceBalanceRepo : IRepoBase<GoodServiceBalance>
    {
        IQueryable<GoodServiceBalance> GetByGoodService(Int64 goodServiceId);

        IQueryable<GoodServiceBalance> GetByStudentPacket(Int64 studentPacketId);

        IQueryable<GoodServiceBalance> GetByStudentPacketCourseOrFPCSCourse(Int64 studentPacketCourseId, Int64 fpcsCourseId);

	  IQueryable<GoodServiceBalance> GetByStudentPacketCourseOrFPCSCourseOrStudentPacket(Int64 studentPacketCourseId, Int64 fpcsCourseId, Int64 studentPacketId, Guid tempId);

        IQueryable<GoodServiceBalance> GetByPacket(Int64 packetId);
    }
}
