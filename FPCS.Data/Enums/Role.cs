﻿
namespace FPCS.Data.Enums
{
    public enum Role
    {
        Admin = 1,
        Teacher = 2,
        Guardian = 3,
        Student = 4,
        Vendor = 5
    }
}
