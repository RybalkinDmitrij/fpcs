﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Data.Enums
{
    public enum HighSchool
    {
         KCC = 10,
         MyHigh = 20,
         Bartlett_High = 30,
         Chugiak_High = 40,
         Dimond_High = 50,
         Eagle_River_High = 60,
         East_High = 70,
         Service_High = 80,
         South_High = 90,
         West_High = 100,
         Middle_School = 110,
         Elementary_School = 120
    }
}
