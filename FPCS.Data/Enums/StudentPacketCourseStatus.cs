﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Data.Enums
{
    public enum StudentPacketCourseStatus
    {
        [DescriptionAttribute("Not Fully Signed")]
        NotFullySigned = 10,

        [DescriptionAttribute("Rejected")]
        Rejected = 20,

        [DescriptionAttribute("Must Amend")]
        MustAmend = 30,

        [DescriptionAttribute("Fully Signed")]
        FullySigned = 40,

        [DescriptionAttribute("Sponsor Alert")]
        SponsorAlert = 50,

        [DescriptionAttribute("Parent Alert")]
        ParentAlert = 60
    }
}
