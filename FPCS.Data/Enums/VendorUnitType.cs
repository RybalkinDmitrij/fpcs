﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Data.Enums
{
    public enum VendorUnitType
    {
        [DescriptionAttribute("Course(s)")]
        Course = 10,

        [DescriptionAttribute("Hour(s)")]
        Hour = 20,

        [DescriptionAttribute("Lesson(s)")]
        Lesson = 30,

        [DescriptionAttribute("Month(s)")]
        Month = 40,

        [DescriptionAttribute("Other")]
        Other = 50,

        [DescriptionAttribute("Semester(s)")]
        Semester = 60
    }
}
