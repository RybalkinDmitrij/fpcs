﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Data.Enums
{
    public enum InternetItemType
    {
        [DescriptionAttribute("Semester 1")]
        Semester1 = 10,

        [DescriptionAttribute("Semester 2")]
        Semester2 = 20,

        [DescriptionAttribute("Month By Month")]
        MonthByMonth = 30
    }
}
