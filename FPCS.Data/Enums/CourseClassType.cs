﻿using FPCS.Core.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Data.Enums
{
    public enum CourseClassType
    {
        /// <summary>
        /// Parent taught
        /// </summary>
        [DescriptionAttribute2("PT - parent taught", "PT")]
        PT = 1,

        /// <summary>
        /// Small group class
        /// </summary>
        [DescriptionAttribute2("SMG - small group class", "SMG")]
        SMG = 2,

        /// <summary>
        /// Single-class enrollment
        /// </summary>
        [DescriptionAttribute2("SCE - single-class enrollment", "SCE")]
        SCE = 3,

        /// <summary>
        /// Online class
        /// </summary>
        [DescriptionAttribute2("OLC - online class", "OLC")]
        OLC = 4,

        /// <summary>
        /// Home designed class
        /// </summary>
        [DescriptionAttribute2("HD - home designed class", "HD")]
        HD = 5,

        /// <summary>
        /// University course
        /// </summary>
        [DescriptionAttribute2("UC - university course", "UC")]
        UC = 6,

        /// <summary>
        /// University course
        /// </summary>
        [DescriptionAttribute2("PTV - parent taught (vendor)", "PTV")]
        PTV = 7,

        /// <summary>
        /// On track
        /// </summary>
        [DescriptionAttribute2("OT - on track", "OT")]
        OT = 8
    }
}
