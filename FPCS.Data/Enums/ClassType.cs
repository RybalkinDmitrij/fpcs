﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Data.Enums
{
    public enum ClassType
    {
        PartneringUniversityCourse = 10,
        IndependentStudy = 20,
        UAA = 30,
        APU = 40
    }
}
