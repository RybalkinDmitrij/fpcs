﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Data.Enums
{
    public enum Credits
    {
        [DescriptionAttribute("1")]
        n1 = 10,

        [DescriptionAttribute("2")]
        n2 = 20,

        [DescriptionAttribute("3")]
        n3 = 30,

        [DescriptionAttribute("4")]
        n4 = 40,

        [DescriptionAttribute("5")]
        n5 = 50
    }
}
