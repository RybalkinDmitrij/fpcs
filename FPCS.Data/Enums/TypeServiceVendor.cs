﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Data.Enums
{
    public enum TypeServiceVendor
    {
        ART = 10,
        COMPUTERS = 20,
        LANGUAGE_ARTS_ELECTIVES = 30,
        LANGUAGE_ARTS = 40,
        MATHEMATICS = 50,
        MISCELLANEOUS = 60,
        MUSIC = 70,
        PHYSICAL_EDUCATION_HEALTH = 80,
        SCIENCE = 90,
        SOCIAL_STUDIES = 100,
        SPONSORSHIP_PLUS = 110,
        WORLD_LANGUAGE = 120
    }
}
