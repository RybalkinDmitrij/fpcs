﻿using System.ComponentModel;

namespace FPCS.Data.Enums
{
    public enum EnrollmentStatus
    {
        [DescriptionAttribute("Conditionally enrolled")]
        ConditionallyEnrolled = 1,

        [DescriptionAttribute("Early graduate")]
        EarlyGraduate = 2,

        [DescriptionAttribute("Enrolled")]
        Enrolled = 3,

        //[DescriptionAttribute("Lottery candidate")]
        //LotteryCandidate = 4,

        [DescriptionAttribute("Graduated")]
        Graduated = 4,

        [DescriptionAttribute("Not re enroll")]
        NotReEnroll = 5,

        [DescriptionAttribute("Withdraw")]
        Withdraw = 6

        //[DescriptionAttribute("Withdraw lottery app")]
        //WithdrawLotteryApp = 7
    }
}
