﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Data.Enums
{
    public enum UnitType
    {
        [DescriptionAttribute("Box")]
        box = 10,

        [DescriptionAttribute("Bundle")]
        bundle = 20,

        [DescriptionAttribute("Carton")]
        carton = 30,

        [DescriptionAttribute("Case")]
        fcase = 40,

        [DescriptionAttribute("Each")]
        each = 50,

        [DescriptionAttribute("Invoice")]
        invoice = 60,

        [DescriptionAttribute("Item")]
        item = 70,

        [DescriptionAttribute("Kit")]
        kit = 80,

        [DescriptionAttribute("Pad")]
        pad = 90,

        [DescriptionAttribute("Pkg")]
        pkg = 100,

        [DescriptionAttribute("Ream")]
        ream = 110,

        [DescriptionAttribute("Roll")]
        roll = 120,

        [DescriptionAttribute("Series")]
        series = 130,

        [DescriptionAttribute("Set")]
        set = 140
    }
}
