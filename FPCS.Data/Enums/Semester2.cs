﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Data.Enums
{
    public enum Semester2
    {
        [DescriptionAttribute("Fall")]
        Fall = 10,

        [DescriptionAttribute("Spring")]
        Spring = 20,

        [DescriptionAttribute("Summer")]
        Summer = 30,

        [DescriptionAttribute("Special")]
        Special = 40
    }
}
