﻿using System.ComponentModel;

namespace FPCS.Data.Enums
{
    public enum ScheduleRepetition
    {
        [DescriptionAttribute("Daily")]
        Daily = 1,
        [DescriptionAttribute("Monday-Friday")]
        MonFri = 2,
        [DescriptionAttribute("Weekly")]
        Weekly = 3,
        [DescriptionAttribute("Bi-Weekly")]
        BiWeekly = 4,
        [DescriptionAttribute("Monthly")]
        Monthly = 5,
        [DescriptionAttribute("Bi-Monthly")]
        BiMonthly = 6,
        [DescriptionAttribute("Quarterly")]
        Quarterly = 7,
        [DescriptionAttribute("Semester")]
        Semester = 8
    }
}
