﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Data.Enums
{
    public enum TitlePerson
    {
        [DescriptionAttribute("Mr")]
        Mr = 10,

        [DescriptionAttribute("Mrs")]
        Mrs = 20,

        [DescriptionAttribute("Ms")]
        Ms = 30,

        [DescriptionAttribute("Dr")]
        Dr = 40
    }
}
