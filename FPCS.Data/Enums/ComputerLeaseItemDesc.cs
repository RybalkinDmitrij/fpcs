﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Data.Enums
{
    public enum ComputerLeaseItemDesc
    {
        [DescriptionAttribute("PC Computer")]
        PCComputer = 10,

        [DescriptionAttribute("Mac Computer")]
        MacComputer = 20,

        [DescriptionAttribute("Laptop")]
        Laptop = 30
    }
}
