﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Data.Enums
{
    public enum TypeGrade
    {
        [DescriptionAttribute("K-6")]
        Elementary = 1,

        [DescriptionAttribute("7-8")]
        Middle = 2,

        [DescriptionAttribute("9-12")]
        High = 3,

	  [DescriptionAttribute("University")]
	  University = 4
    }
}
