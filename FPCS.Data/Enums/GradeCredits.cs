﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Data.Enums
{
    public enum GradeCredits
    {
        [DescriptionAttribute("0")]
        n000 = 10,

        [DescriptionAttribute("0.25")]
        n025 = 20,

        [DescriptionAttribute("0.50")]
        n050 = 30,

        [DescriptionAttribute("1.00")]
        n100 = 40
    }
}