﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Data.Enums
{
    public enum ApprovalGivenBy
    {
        [DescriptionAttribute("Phone")]
        phone = 10,

        [DescriptionAttribute("Fax")]
        fax = 20,

        [DescriptionAttribute("Email")]
        email = 30,

        [DescriptionAttribute("In-person")]
        in_person = 40
    }
}
