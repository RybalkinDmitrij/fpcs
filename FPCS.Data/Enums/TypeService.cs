﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Data.Enums
{
    public enum TypeService
    {
        [DescriptionAttribute("Academic tutoring")]
        AcademicTutoring = 10,

        [DescriptionAttribute("PE")]
        PE = 20,

        [DescriptionAttribute("Music")]
        Music = 30,

        [DescriptionAttribute("Art")]
        Art = 40,

        [DescriptionAttribute("Driver education")]
        DriverEducation = 50,

        [DescriptionAttribute("Building rental")]
        BuildingRental = 60
    }
}
