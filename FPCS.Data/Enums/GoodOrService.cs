﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Data.Enums
{
    public enum GoodOrService
    {
        [DescriptionAttribute("Good")]
        Good = 10,

        [DescriptionAttribute("Service")]
        Service = 20
    }
}
