﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Data.Enums
{
    public enum VendorType
    {
        [DescriptionAttribute("Service Vendor")]
        Service = 10,

        [DescriptionAttribute("Goods Vendor")]
        Goods = 20,

        [DescriptionAttribute("Both")]
        Both = 30,

        [DescriptionAttribute("Non-Profit")]
        NonProfit = 40
    }
}
