﻿using System.ComponentModel;

namespace FPCS.Data.Enums
{
    public enum Semester
    {
        [DescriptionAttribute("Semester 1")]
        Semester1 = 1,

        [DescriptionAttribute("Semester 2")]
        Semester2 = 2,

        [DescriptionAttribute("Summer")]
        Summer = 3//,

        //[DescriptionAttribute("All Year")]
        //AllYear = 9
    }
}
