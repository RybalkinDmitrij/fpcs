﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Data.Enums
{
    public enum Location
    {
        [DescriptionAttribute("Downtown")]
        Downtown = 10,

        [DescriptionAttribute("Eagle River")]
        EagleRiver = 20,

        [DescriptionAttribute("Eastside")]
        Eastside = 30,

        [DescriptionAttribute("Hillside")]
        Hillside = 40,

        [DescriptionAttribute("Midtown")]
        Midtown = 50,

        [DescriptionAttribute("Southside")]
        Southside = 60,

        [DescriptionAttribute("Westside")]
        Westside = 70
    }
}
