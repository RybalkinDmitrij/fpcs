﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Data.Enums
{
    public enum SupplyType
    {
        [DescriptionAttribute("Art supplies")]
        ArtSupplies = 10,

        [DescriptionAttribute("General school supply")]
        GeneralSchoolSupply = 20,

        [DescriptionAttribute("Music supplies")]
        MusicSupplies = 30,

        [DescriptionAttribute("Other class supply")]
        OtherClassSupply = 40,

        [DescriptionAttribute("Science supplies")]
        ScienceSupplies = 50
    }
}
