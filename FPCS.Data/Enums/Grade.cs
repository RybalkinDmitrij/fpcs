﻿using System;
using System.ComponentModel;

namespace FPCS.Data.Enums
{
    public enum Grade
    {
        [DescriptionAttribute("K")]
        K = 0,
        [DescriptionAttribute("G1")]
        G1 = 1,
        [DescriptionAttribute("G2")]
        G2 = 2,
        [DescriptionAttribute("G3")]
        G3 = 3,
        [DescriptionAttribute("G4")]
        G4 = 4,
        [DescriptionAttribute("G5")]
        G5 = 5,
        [DescriptionAttribute("G6")]
        G6 = 6,
        [DescriptionAttribute("G7")]
        G7 = 7,
        [DescriptionAttribute("G8")]
        G8 = 8,
        [DescriptionAttribute("G9")]
        G9 = 9,
        [DescriptionAttribute("G10")]
        G10 = 10,
        [DescriptionAttribute("G11")]
        G11 = 11,
        [DescriptionAttribute("G12")]
        G12 = 12,
        [DescriptionAttribute("G")]
        G = 13
    }

	public static class GradeExtension
	{
		public static String ConvertToDescription(this Grade grade)
		{
			switch (grade)
			{
				case Grade.K:
					return "K";
				case Grade.G1:
					return "G1";
				case Grade.G2:
					return "G2";
				case Grade.G3:
					return "G3";
				case Grade.G4:
					return "G4";
				case Grade.G5:
					return "G5";
				case Grade.G6:
					return "G6";
				case Grade.G7:
					return "G7";
				case Grade.G8:
					return "G8";
				case Grade.G9:
					return "G9";
				case Grade.G10:
					return "G10";
				case Grade.G11:
					return "G11";
				case Grade.G12:
					return "G12";
				case Grade.G:
					return "G";
				default:
					return "K";
			}
		}
	}
}
