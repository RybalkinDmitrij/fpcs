﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Data.Enums
{
    public enum GoalEnrollmentPercent
    {
        [DescriptionAttribute("0%")]
        _0Percent = 10,

        [DescriptionAttribute("25%")]
        _25Percent = 20,

        [DescriptionAttribute("50%")]
        _50Percent = 30,

        [DescriptionAttribute("75%")]
        _75Percent = 40,

        [DescriptionAttribute("100%")]
        _100Percent = 50
    }
}
