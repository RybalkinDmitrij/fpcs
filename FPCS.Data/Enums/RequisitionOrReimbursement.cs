﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Data.Enums
{
    public enum RequisitionOrReimbursement
    {
        [DescriptionAttribute("Requisition")]
        Requisition = 10,

        [DescriptionAttribute("Reimbursement")]
        Reimbursement = 20
    }
}
