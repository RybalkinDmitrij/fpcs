﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Data.Enums
{
    public enum VendorStatus
    {
        [DescriptionAttribute("Approved")]
        Approved = 10,

        [DescriptionAttribute("Pending")]
        Pending = 20,

        [DescriptionAttribute("Rejected")]
        Rejected = 30,

        [DescriptionAttribute("Remove")]
        Remove = 40
    }
}
