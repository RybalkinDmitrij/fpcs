﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Data.Enums
{
    public enum Repeated
    {
        [DescriptionAttribute("Yes")]
        Repeated = 1,

        [DescriptionAttribute("No")]
        NoRepeated = 2
    }
}
