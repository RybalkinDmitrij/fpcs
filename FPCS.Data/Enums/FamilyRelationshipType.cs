﻿using System.ComponentModel;

namespace FPCS.Data.Enums
{
    public enum FamilyRelationshipType
    {
        [DescriptionAttribute("Father")]
        Father = 1,

        [DescriptionAttribute("Mother")]
        Mother = 2,

        [DescriptionAttribute("Foster Father")]
        FosterFather = 3,

        [DescriptionAttribute("Foster Mother")]
        FosterMother = 4,

        [DescriptionAttribute("Legal Guardian")]
        LegalGuardian = 5,

        [DescriptionAttribute("Grand Parent")]
        GrandParent = 6,

        [DescriptionAttribute("Step Father")]
        StepFather = 7,

        [DescriptionAttribute("Step Mother")]
        StepMother = 8,

        [DescriptionAttribute("Uncle")]
        Uncle = 9,

        [DescriptionAttribute("Aunt")]
        Aunt = 10,

        [DescriptionAttribute("Other")]
        Other = 11,

        [DescriptionAttribute("Grand Mother")]
        GrandMother = 12,

        [DescriptionAttribute("Grand Father")]
        GrandFather = 13,

        [DescriptionAttribute("Sibling")]
        Sibling = 14,

        [DescriptionAttribute("Family Friend")]
        FamilyFriend = 15,

        [DescriptionAttribute("Doctor")]
        Doctor = 16,

        [DescriptionAttribute("Neighbor")]
        Neighbor = 17,

        [DescriptionAttribute("Agency Representative")]
        AgencyRepresentative = 18,

        [DescriptionAttribute("Caregiver")]
        Caregiver = 19,

        [DescriptionAttribute("Self")]
        Self = 20,

        [DescriptionAttribute("Not Specified")]
        NotSpecified = 99
    }
}
