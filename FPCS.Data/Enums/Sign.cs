﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Data.Enums
{
    public enum Sign
    {
        [DescriptionAttribute("Must Amend")]
        MustAmend = 10,

        [DescriptionAttribute("Rejected")]
        Rejected = 20,

        [DescriptionAttribute("Signed")]
        Sign = 30
    }
}
