﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Data.Enums
{
    public enum PayType
    {
        [DescriptionAttribute("ADD")]
        ADD = 10,

        [DescriptionAttribute("ADD & Contract")]
        ADDContract = 20,

        [DescriptionAttribute("Contract")]
        Contract = 30,

        [DescriptionAttribute("Inactive")]
        Inactive = 40,

        [DescriptionAttribute("SAA")]
        SAA = 50
    }
}
