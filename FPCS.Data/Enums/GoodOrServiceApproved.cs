﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Data.Enums
{
    public enum GoodOrServiceApproved
    {
        [DescriptionAttribute("pend")]
        b_pend = 10,

        [DescriptionAttribute("appr")]
        b_appr = 20,

        [DescriptionAttribute("rejc")]
        b_rejc = 30
    }
}
