﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Data.Enums
{
    public enum TypeTextbookCurriculum
    {
        [DescriptionAttribute("Activity workbook")]
        Activity_Workbook = 10,

        [DescriptionAttribute("Almanac")]
        Almanac = 20,

        [DescriptionAttribute("CD/DVD")]
        CD_DVD = 30,

        [DescriptionAttribute("Curriculum set")]
        CurriculumSet = 40,

        [DescriptionAttribute("Dictionary collegiate")]
        DictionaryCollegiate = 50,

        [DescriptionAttribute("Dictionary elementary")]
        DictionaryElementary = 60,

        [DescriptionAttribute("Dictionary secondary")]
        DictionarySecondary = 70,

        [DescriptionAttribute("Educational kits")]
        EducationalKits = 80,

        [DescriptionAttribute("Flash cards")]
        FlashCards = 90,

        [DescriptionAttribute("Maps")]
        Maps = 100,

        [DescriptionAttribute("Reading books")]
        ReadingBooks = 110,

        [DescriptionAttribute("Student class textbook")]
        StudentClassTextbook = 120,

        [DescriptionAttribute("Teacher manuals")]
        TeacherManuals = 130,

        [DescriptionAttribute("Trade books")]
        TradeBooks = 140
    }
}
