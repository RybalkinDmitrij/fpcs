﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Data.Enums
{
    public enum Consumable
    {
        [DescriptionAttribute("No")]
        No = 10,

        [DescriptionAttribute("Yes")]
        Yes = 20
    }
}
