﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Data.Enums
{
	public enum AmpEla
	{
		[DescriptionAttribute("N/A")]
		NA = 0,

		[DescriptionAttribute("Advanced")]
		Advanced = 1,

		[DescriptionAttribute("Proficient")]
		Proficient = 2,

		[DescriptionAttribute("Below Proficient")]
		BelowProficient = 3,

		[DescriptionAttribute("Far Below Proficient")]
		FarBelowProficient = 4,

		[DescriptionAttribute("Parent Refusal")]
		ParentRefusal = 5,

		[DescriptionAttribute("Absent")]
		Absent = 6
	}
}
