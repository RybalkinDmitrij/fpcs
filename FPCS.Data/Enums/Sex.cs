﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Data.Enums
{
    public enum Sex
    {
        [DescriptionAttribute("Male")]
        Male = 1,

        [DescriptionAttribute("Female")]
        Female = 2
    }
}
