﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Data.Enums
{
    public enum OtherLanguage
    {
        [DescriptionAttribute("Aleute")]
        Aleute = 10,

        [DescriptionAttribute("English")]
        English = 20,

        [DescriptionAttribute("Eskimo")]
        Eskimo = 30,

        [DescriptionAttribute("French")]
        French = 40,

        [DescriptionAttribute("German")]
        German = 50,

        [DescriptionAttribute("Other")]
        Other = 60,

        [DescriptionAttribute("Russian")]
        Russian = 70,

        [DescriptionAttribute("Spanish")]
        Spanish = 80
    }
}
