﻿using System.ComponentModel;

namespace FPCS.Data.Enums
{
	public enum TypeRequisitionReimbursement
	{
		[DescriptionAttribute("Supplies")]
		Supplies = 20,

		[DescriptionAttribute("Textbook/Curriculum")]
		TextbookCurriculum = 30,

		[DescriptionAttribute("University Course/Ind. Study Class")]
		UniversityCourseIndStudyClass = 60,

		[DescriptionAttribute("Vendor Service")]
		VendorService = 70,

		[DescriptionAttribute("ASD Class")]
		ASDClass = 80,

		[DescriptionAttribute("FPCS Class")]
		FPCSClass = 90
	}
}
