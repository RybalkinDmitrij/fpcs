﻿using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;

using FPCS.Data.Entities;

namespace FPCS.Data.Cofigs
{
    internal class StudentPacketConfig : EntityTypeConfiguration<StudentPacket>
    {
        public StudentPacketConfig()
        {
            ToTable("StudentPacket");
            HasKey(x => x.StudentPacketId);

            Property(x => x.IsDeleted).IsRequired();
            Property(x => x.CreatedDate).IsRequired();
            Property(x => x.UpdatedDate).IsRequired();
            Property(x => x.IsLocked).IsRequired();

            Property(x => x.AmpELA).IsRequired();
            Property(x => x.AmpELAComment).HasMaxLength(500).IsOptional();
            Property(x => x.AmpMath).IsRequired();
            Property(x => x.AmpMathComment).HasMaxLength(500).IsOptional();

            Property(x => x.A504).IsRequired();
            Property(x => x.SWD).IsRequired();

            Property(x => x.IsASDTASigned).IsRequired();
            HasOptional(x => x.ASDTAGuardian).WithMany(x => x.ASDTAStudentPackets).HasForeignKey(x => x.ASDTAGuardianId).WillCascadeOnDelete(false);

            Property(x => x.IsPRASigned).IsRequired();
            HasOptional(x => x.PRAGuardian).WithMany(x => x.PRAStudentPackets).HasForeignKey(x => x.PRAGuardianId).WillCascadeOnDelete(false);

            Property(x => x.GuardianSignatureSem1).IsOptional();
		Property(x => x.DateGuardianSignatureSem1).IsOptional();
            Property(x => x.SponsorSignatureSem1).IsOptional();
		Property(x => x.DateSponsorSignatureSem1).IsOptional();

            Property(x => x.GuardianSignatureSem2).IsOptional();
		Property(x => x.DateGuardianSignatureSem2).IsOptional();
            Property(x => x.SponsorSignatureSem2).IsOptional();
		Property(x => x.DateSponsorSignatureSem2).IsOptional();

            Property(x => x.GuardianSignatureSummer).IsOptional();
		Property(x => x.DateGuardianSignatureSummer).IsOptional();
            Property(x => x.SponsorSignatureSummer).IsOptional();
		Property(x => x.DateSponsorSignatureSummer).IsOptional();

            HasOptional(x => x.SponsorTeacher).WithMany(x => x.SponsorStudentPackets).HasForeignKey(x => x.SponsorTeacherId).WillCascadeOnDelete(false);
            
            HasRequired(x => x.Student).WithMany(x => x.StudentPackets).HasForeignKey(x => x.StudentId).WillCascadeOnDelete(false);
            HasRequired(x => x.SchoolYear).WithMany(x => x.StudentPackets).HasForeignKey(x => x.SchoolYearId).WillCascadeOnDelete(false);
        }
    }
}
