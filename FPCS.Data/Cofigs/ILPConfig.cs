﻿using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;

using FPCS.Data.Entities;

namespace FPCS.Data.Cofigs
{
    internal class ILPConfig : EntityTypeConfiguration<ILP>
    {
        public ILPConfig()
        {
            ToTable("ILP");
            HasKey(x => x.ILPId);

            Property(x => x.ILPName).IsRequired();
		Property(x => x.Instructor).IsRequired();
 
            HasOptional(x => x.StudentPacketCourse).WithMany(x => x.ILPs).HasForeignKey(x => x.StudentPacketCourseId).WillCascadeOnDelete(false);
            HasOptional(x => x.FPCSCourse).WithMany(x => x.ILPs).HasForeignKey(x => x.FPCSCourseId).WillCascadeOnDelete(false);

            Property(x => x.CourseHrs).IsRequired();
            Property(x => x.DescriptionCourse).IsOptional().HasMaxLength(18000);
            Property(x => x.Standards).IsOptional().HasMaxLength(18000);
            Property(x => x.StudentActivities).IsOptional().HasMaxLength(18000);
            Property(x => x.MaterialsResources).IsOptional().HasMaxLength(18000);
            Property(x => x.RoleAnyPeople).IsOptional().HasMaxLength(18000);
            Property(x => x.EvaluationGradingPassFail).IsRequired();
            Property(x => x.EvaluationGradingGradingScale).IsRequired();
            Property(x => x.EvaluationGradingOSN).IsRequired();
            Property(x => x.EvaluationGradingOther).IsRequired();
            Property(x => x.EvaluationGradingOtherExplain).IsOptional().HasMaxLength(18000);
            Property(x => x.EvaluatedMeasurableOutcomes).IsOptional().HasMaxLength(18000);
            Property(x => x.CourseSyllabus).IsOptional().HasMaxLength(18000);
            Property(x => x.GuardianILPModifications).IsOptional().HasMaxLength(18000);
            Property(x => x.InstructorILPModifications).IsOptional().HasMaxLength(18000);
        }
    }
}
