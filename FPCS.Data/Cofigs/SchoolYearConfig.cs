﻿using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;
using FPCS.Data.Entities;

namespace FPCS.Data.Cofigs
{
	internal class SchoolYearConfig : EntityTypeConfiguration<SchoolYear>
	{
		public SchoolYearConfig()
		{
			ToTable("SchoolYear");
			HasKey(x => x.SchoolYearId);

			Property(x => x.Name).IsRequired().HasMaxLength(50);
			Property(x => x.Year).IsRequired();
			Property(x => x.StartDate).IsRequired();
			Property(x => x.EndDate).IsRequired();
			Property(x => x.HalfDate).IsRequired();

			Property(x => x.ILPHoursLimit).IsRequired();
			Property(x => x.CountHoursWorkDay).IsRequired().HasPrecision(18, 1);
			Property(x => x.PlusBudgetLimit).IsRequired().HasPrecision(18, 2);
			Property(x => x.BookMaterialFee).IsRequired().HasPrecision(18, 2);

			Property(x => x.GroupLifeInsurance).IsRequired().HasPrecision(18, 1);
			Property(x => x.GroupMedicalInsurance).IsRequired().HasPrecision(18, 1);
			Property(x => x.WorkersComp).IsRequired().HasPrecision(18, 7);
			Property(x => x.Unemployment).IsRequired().HasPrecision(18, 7);
			Property(x => x.FICA).IsRequired().HasPrecision(18, 7);
			Property(x => x.FICACap).IsRequired().HasPrecision(18, 2);
			Property(x => x.Medicare).IsRequired().HasPrecision(18, 7);
			Property(x => x.TERSBaseContract).IsRequired().HasPrecision(18, 2);
			Property(x => x.PERSBaseContract).IsRequired().HasPrecision(18, 2);
			Property(x => x.TERSRetirement).IsRequired().HasPrecision(18, 7);
			Property(x => x.PERSRetirement).IsRequired().HasPrecision(18, 7);

			Property(x => x.GeneralSupplies).IsRequired().HasPrecision(18, 7);

			Property(x => x.FundingAmountGradeK).IsRequired();
			Property(x => x.FundingAmountGrade1).IsRequired();
			Property(x => x.FundingAmountGrade2).IsRequired();
			Property(x => x.FundingAmountGrade3).IsRequired();
			Property(x => x.FundingAmountGrade4).IsRequired();
			Property(x => x.FundingAmountGrade5).IsRequired();
			Property(x => x.FundingAmountGrade6).IsRequired();
			Property(x => x.FundingAmountGrade7).IsRequired();
			Property(x => x.FundingAmountGrade8).IsRequired();
			Property(x => x.FundingAmountGrade9).IsRequired();
			Property(x => x.FundingAmountGrade10).IsRequired();
			Property(x => x.FundingAmountGrade11).IsRequired();
			Property(x => x.FundingAmountGrade12).IsRequired();
			Property(x => x.FundingAmountGradeG).IsRequired();

			Property(x => x.CoreUnitGradeK).IsRequired();
			Property(x => x.CoreUnitGrade1).IsRequired();
			Property(x => x.CoreUnitGrade2).IsRequired();
			Property(x => x.CoreUnitGrade3).IsRequired();
			Property(x => x.CoreUnitGrade4).IsRequired();
			Property(x => x.CoreUnitGrade5).IsRequired();
			Property(x => x.CoreUnitGrade6).IsRequired();
			Property(x => x.CoreUnitGrade7).IsRequired();
			Property(x => x.CoreUnitGrade8).IsRequired();
			Property(x => x.CoreUnitGrade9).IsRequired();
			Property(x => x.CoreUnitGrade10).IsRequired();
			Property(x => x.CoreUnitGrade11).IsRequired();
			Property(x => x.CoreUnitGrade12).IsRequired();
			Property(x => x.CoreUnitGradeG).IsRequired();

			Property(x => x.ElectiveUnitGradeK).IsRequired();
			Property(x => x.ElectiveUnitGrade1).IsRequired();
			Property(x => x.ElectiveUnitGrade2).IsRequired();
			Property(x => x.ElectiveUnitGrade3).IsRequired();
			Property(x => x.ElectiveUnitGrade4).IsRequired();
			Property(x => x.ElectiveUnitGrade5).IsRequired();
			Property(x => x.ElectiveUnitGrade6).IsRequired();
			Property(x => x.ElectiveUnitGrade7).IsRequired();
			Property(x => x.ElectiveUnitGrade8).IsRequired();
			Property(x => x.ElectiveUnitGrade9).IsRequired();
			Property(x => x.ElectiveUnitGrade10).IsRequired();
			Property(x => x.ElectiveUnitGrade11).IsRequired();
			Property(x => x.ElectiveUnitGrade12).IsRequired();
			Property(x => x.ElectiveUnitGradeG).IsRequired();

			Property(x => x.DisableAccessForParents).IsRequired();
			Property(x => x.DisableAccessForTeachers).IsRequired();
			Property(x => x.DisableMessage).IsRequired();

			Property(x => x.CreatedDate).IsRequired();
			Property(x => x.UpdatedDate).IsRequired();
		}
	}
}
