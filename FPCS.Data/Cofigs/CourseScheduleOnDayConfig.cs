﻿using System.Data.Entity.ModelConfiguration;

using FPCS.Data.Entities;

namespace FPCS.Data.Cofigs
{
    internal class CourseScheduleOnDayConfig : EntityTypeConfiguration<CourseScheduleOnDay>
    {
        public CourseScheduleOnDayConfig()
        {
            ToTable("CourseScheduleOnDay");
            HasKey(x => x.CourseScheduleOnDayId);

            Property(x => x.StartTime).IsRequired();
            Property(x => x.EndTime).IsRequired();
            Property(x => x.DayOfWeek).IsRequired();

            HasRequired(x => x.FPCSCourse).WithMany(x => x.CourseScheduleOnDays).HasForeignKey(x => x.FPCSCourseId).WillCascadeOnDelete(false);
        }
    }
}
