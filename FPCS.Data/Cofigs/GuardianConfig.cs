﻿using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;

using FPCS.Data.Entities;

namespace FPCS.Data.Cofigs
{
    internal class GuardianConfig : EntityTypeConfiguration<Guardian>
    {
        public GuardianConfig()
        {
            Map(x => { x.ToTable("Guardian"); x.MapInheritedProperties(); });

            Property(x => x.Employer).HasMaxLength(100);
            Property(x => x.IsActiveMilitary);
            Property(x => x.Rank).HasMaxLength(100);
            Property(x => x.Pager).HasMaxLength(100);
            Property(x => x.BusinessPhone).HasMaxLength(50);
            Property(x => x.Ext).HasMaxLength(20);
            Property(x => x.CellPhone).HasMaxLength(50);
            Property(x => x.Address).HasMaxLength(200);
            Property(x => x.City).HasMaxLength(70);
            Property(x => x.Country).HasMaxLength(70);
            Property(x => x.Zip).HasMaxLength(20);

            HasOptional(x => x.State).WithMany(x => x.Guardians).HasForeignKey(x => x.StateId).WillCascadeOnDelete(false);
            HasMany(x => x.Families).WithMany(x => x.Guardians)
                .Map(x => x.MapLeftKey("GuardianId").MapRightKey("FamilyId").ToTable("FamilyGuardian"));
            HasMany(x => x.Relationships).WithRequired(x => x.Guardian).HasForeignKey(x => x.GuardianId).WillCascadeOnDelete(false);
        }
    }
}
