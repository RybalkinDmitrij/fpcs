﻿using System.Data.Entity.ModelConfiguration;

using FPCS.Data.Entities;

namespace FPCS.Data.Cofigs
{
    internal class ServiceVendorConfig : EntityTypeConfiguration<ServiceVendor>
    {
        public ServiceVendorConfig()
        {
            ToTable("ServiceVendor");
            HasKey(x => x.ServiceVendorId);

            Property(x => x.Name).IsRequired().HasMaxLength(150);
            //Property(x => x.Type).IsRequired();
            Property(x => x.IsActive).IsRequired();

            HasRequired(x => x.Subject).WithMany(x => x.ServiceVendors).HasForeignKey(x => x.SubjectId).WillCascadeOnDelete(false);
        }
    }
}
