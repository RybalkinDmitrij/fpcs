﻿using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;
using FPCS.Data.Entities;

namespace FPCS.Data.Cofigs
{
	internal class GoodServiceConfig : EntityTypeConfiguration<GoodService>
	{
		public GoodServiceConfig()
		{
			ToTable("GoodService");
			HasKey(x => x.GoodServiceId);

			Property(x => x.IsDeleted).IsRequired();
			Property(x => x.CreatedDate).IsRequired();
			Property(x => x.UpdatedDate).IsRequired();

			HasOptional(x => x.StudentPacketCourse)
				.WithMany(x => x.GoodServices)
				.HasForeignKey(x => x.StudentPacketCourseId)
				.WillCascadeOnDelete(false);
			HasOptional(x => x.FPCSCourse)
				.WithMany(x => x.GoodServices)
				.HasForeignKey(x => x.FPCSCourseId)
				.WillCascadeOnDelete(false);
			HasOptional(x => x.StudentPacket)
				.WithMany(x => x.GoodServices)
				.HasForeignKey(x => x.StudentPacketId)
				.WillCascadeOnDelete(false);

			Property(x => x.TempId).IsOptional();

			HasRequired(x => x.Vendor).WithMany(x => x.GoodServices).HasForeignKey(x => x.VendorId).WillCascadeOnDelete(false);

			Property(x => x.GoodOrService).IsRequired();
			Property(x => x.RequisitionOrReimbursement).IsRequired();
			Property(x => x.TypeRequisitionReimbursement).IsRequired();

			Property(x => x.AdminComments).IsOptional();
			Property(x => x.ApprovalGivenBy).IsOptional();
			Property(x => x.NumberOfUnits).IsOptional();
			Property(x => x.UnitPrice).IsOptional();
			Property(x => x.ShippingHandlingFees).IsOptional();
			Property(x => x.LineItemName).IsOptional();
			Property(x => x.LineItemDesc).IsOptional();
			Property(x => x.SupplyType).IsOptional();
			Property(x => x.Count).IsOptional();
			Property(x => x.CatalogMFG).IsOptional();
			Property(x => x.CatalogIssuePG).IsOptional();
			Property(x => x.UnitType).IsOptional();
			Property(x => x.BarCode).IsOptional();
			Property(x => x.Consumable).IsOptional();
			Property(x => x.TypeTextbookCurriculum).IsOptional();
			Property(x => x.Title).IsOptional();
			Property(x => x.Author).IsOptional();
			Property(x => x.PublisherISBN).IsOptional();
			Property(x => x.PublisherName).IsOptional();
			Property(x => x.Edition).IsOptional();

			Property(x => x.HighSchool).IsOptional();
			Property(x => x.AdminComments).IsOptional();
			Property(x => x.ApprovalGivenBy).IsOptional();
			Property(x => x.NumberOfUnits).IsOptional();
			Property(x => x.UnitPrice).IsOptional();
			Property(x => x.NumberSemesters).IsOptional();
			Property(x => x.CostPerSemester).IsOptional();
			Property(x => x.ClassType).IsOptional();
			Property(x => x.CourseTitle).IsOptional();
			Property(x => x.CourseNumber).IsOptional();
			Property(x => x.DateBirth).IsOptional();
			Property(x => x.Credits).IsOptional();
			Property(x => x.Semester).IsOptional();
			Property(x => x.TypeService).IsOptional();
			Property(x => x.Description).IsOptional();
			Property(x => x.BeginDate).IsOptional();
			Property(x => x.EndDate).IsOptional();
			Property(x => x.VendorUnitType).IsOptional();

			Property(x => x.ComputerLeaseItemType).IsOptional();
			Property(x => x.ComputerLeaseItemDesc).IsOptional();
			Property(x => x.DetailedItemDesc).IsOptional();
			Property(x => x.InternetItemType).IsOptional();

			Property(x => x.GoodOrServiceApproved).IsOptional();
			Property(x => x.CommentsRejectReason).IsOptional();
			Property(x => x.IsClosed).IsOptional();

			HasOptional(x => x.UserRequest).WithMany(x => x.GoodServiceRequests).HasForeignKey(x => x.UserRequestId).WillCascadeOnDelete(false);
			Property(x => x.DateRequest).IsOptional();
		}
	}
}
