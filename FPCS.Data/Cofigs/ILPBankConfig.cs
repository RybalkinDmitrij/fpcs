﻿using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;

using FPCS.Data.Entities;

namespace FPCS.Data.Cofigs
{
    internal class ILPBankConfig : EntityTypeConfiguration<ILPBank>
    {
        public ILPBankConfig()
        {
            ToTable("ILPBank");
            HasKey(x => x.ILPBankId);

            Property(x => x.ILPId).IsRequired();
            Property(x => x.ILPName).IsRequired();
 
            HasOptional(x => x.Subject).WithMany(x => x.ILPBanks).HasForeignKey(x => x.SubjectId).WillCascadeOnDelete(false);
            HasRequired(x => x.SchoolYear).WithMany(x => x.ILPBanks).HasForeignKey(x => x.SchoolYearId).WillCascadeOnDelete(false);
            HasOptional(x => x.Teacher).WithMany(x => x.ILPBanks).HasForeignKey(x => x.TeacherId).WillCascadeOnDelete(false);
            HasOptional(x => x.Guardian).WithMany(x => x.ILPBanks).HasForeignKey(x => x.GuardianId).WillCascadeOnDelete(false);

            Property(x => x.CourseHrs).IsRequired();
            Property(x => x.DescriptionCourse).IsOptional().HasMaxLength(68000);
            Property(x => x.Standards).IsOptional().HasMaxLength(68000);
            Property(x => x.StudentActivities).IsOptional().HasMaxLength(68000);
            Property(x => x.MaterialsResources).IsOptional().HasMaxLength(68000);
            Property(x => x.RoleAnyPeople).IsOptional().HasMaxLength(68000);
            Property(x => x.EvaluationGradingPassFail).IsRequired();
            Property(x => x.EvaluationGradingGradingScale).IsRequired();
            Property(x => x.EvaluationGradingOSN).IsRequired();
            Property(x => x.EvaluationGradingOther).IsRequired();
            Property(x => x.EvaluationGradingOtherExplain).IsOptional().HasMaxLength(68000);
            Property(x => x.EvaluatedMeasurableOutcomes).IsOptional().HasMaxLength(68000);
            Property(x => x.CourseSyllabus).IsOptional().HasMaxLength(68000);
            Property(x => x.GuardianILPModifications).IsOptional().HasMaxLength(68000);
            Property(x => x.InstructorILPModifications).IsOptional().HasMaxLength(68000);
        }
    }
}
