﻿using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;

using FPCS.Data.Entities;

namespace FPCS.Data.Cofigs
{
    internal class TeacherConfig : EntityTypeConfiguration<Teacher>
    {
        public TeacherConfig()
        {
            Map(x => { x.ToTable("Teacher"); x.MapInheritedProperties(); });

            Property(x => x.Title).IsRequired();
            Property(x => x.DateBirth).IsRequired();
            Property(x => x.IsActive).IsRequired();
            Property(x => x.MailingAddress).IsRequired();
            Property(x => x.City).IsRequired();
            Property(x => x.ZipCode).IsRequired();
            Property(x => x.HomePhone).IsOptional();
            Property(x => x.BusPhone).IsOptional();
            Property(x => x.Ext).IsRequired();
            Property(x => x.CellPhone).IsOptional();
            Property(x => x.MastersDegree).IsRequired();
            Property(x => x.SSN).IsRequired();
            //Property(x => x.EMailAddress).IsRequired();
            Property(x => x.SecondEMailAddress).IsOptional();
            Property(x => x.DistrictCode).IsOptional();
            Property(x => x.IsBenefitPaidASDSchool).IsRequired();
            Property(x => x.ASDFTE).IsRequired();
            Property(x => x.IsNotIncludeBenefits).IsRequired();
            Property(x => x.FTCSFTE).IsRequired();
            Property(x => x.NameSchool).IsOptional();
            Property(x => x.IsLeaveASD).IsRequired();
            Property(x => x.IsSubtitleTeacher).IsRequired();
            Property(x => x.IsOtherASDEmployee).IsRequired();
            Property(x => x.IsOnASDEligibleToHire).IsRequired();
            Property(x => x.IsRetiredASDTeacher).IsRequired();
            Property(x => x.FPCSFTE).IsRequired();
            Property(x => x.IsGroupInstruction).IsRequired();
            Property(x => x.IsIndividualInstruction).IsRequired();
            Property(x => x.YearsTeachingExperience).IsRequired();
            Property(x => x.TeachingCertificateExpirationDate).IsRequired();
            Property(x => x.TeachingSalaryPlacement).IsRequired();
            Property(x => x.IsAlaskaCertificationK8).IsRequired();
            Property(x => x.IsAlaskaCertificationK12).IsRequired();
            Property(x => x.IsAlaskaCertificationSpecialEducation).IsRequired();
            Property(x => x.IsAlaskaCertificationSecondary).IsRequired();
            Property(x => x.AlaskaCertificationSubjectGrades).IsOptional();
            Property(x => x.IsInMyClassroom).IsRequired();
            Property(x => x.IsAtMyHome).IsRequired();
            Property(x => x.IsAsStudentsHome).IsRequired();
            Property(x => x.IsAtFPCSClassroom).IsRequired();
            Property(x => x.IsOtherAvailableTeach).IsRequired();
            Property(x => x.OtherAvailableTeach).IsOptional();
            Property(x => x.IsWeekdays).IsRequired();
            Property(x => x.IsWeekdayAfternoons).IsRequired();
            Property(x => x.IsWeekdayEvenings).IsRequired();
            Property(x => x.IsWeekends).IsRequired();
            Property(x => x.IsSummers).IsRequired();
            Property(x => x.FlatRateHour).IsRequired().HasPrecision(19, 3);
            Property(x => x.BasePayHour).IsRequired().HasPrecision(19, 3);
            Property(x => x.PayHourwBenefits).IsRequired().HasPrecision(19, 3);
            Property(x => x.PerDeimRate).IsRequired().HasPrecision(19, 3);
            Property(x => x.PayType).IsRequired();
            Property(x => x.IsGuardian).IsRequired();

            HasRequired(x => x.State).WithMany(x => x.Teachers).HasForeignKey(x => x.StateId).WillCascadeOnDelete(false);
        }
    }
}
