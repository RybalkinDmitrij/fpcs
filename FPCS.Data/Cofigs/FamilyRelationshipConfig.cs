﻿using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;

using FPCS.Data.Entities;

namespace FPCS.Data.Cofigs
{
    internal class FamilyRelationshipConfig : EntityTypeConfiguration<FamilyRelationship>
    {
        public FamilyRelationshipConfig()
        {
            ToTable("FamilyRelationship");
            HasKey(x => x.FamilyRelationshipId);

            Property(x => x.FamilyRelationshipType).IsRequired();
        }
    }
}
