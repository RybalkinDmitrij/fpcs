﻿using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;

using FPCS.Data.Entities;

namespace FPCS.Data.Cofigs
{
    internal class StudentPacketCourseConfig : EntityTypeConfiguration<StudentPacketCourse>
    {
        public StudentPacketCourseConfig()
        {
            ToTable("StudentPacketCourse");
            HasKey(x => x.StudentPacketCourseId);

            Property(x => x.IsDeleted).IsRequired();
            Property(x => x.CreatedDate).IsRequired();
            Property(x => x.UpdatedDate).IsRequired();

            HasRequired(x => x.StudentPacket).WithMany(x => x.StudentPacketCourses).HasForeignKey(x => x.StudentPacketId).WillCascadeOnDelete(false);
            HasRequired(x => x.FPCSCourse).WithMany(x => x.StudentPacketCourses).HasForeignKey(x => x.FPCSCourseId).WillCascadeOnDelete(false);

            Property(x => x.Repeat).IsRequired();

            Property(x => x.DateSponsorSignature).IsOptional();
            Property(x => x.SponsorSignature).IsOptional();
            Property(x => x.SponsorComment).IsOptional();

            Property(x => x.DateInstructorSignature).IsOptional();
            Property(x => x.InstructorSignature).IsOptional();
            Property(x => x.InstructorComment).IsOptional();

            Property(x => x.DateAdminSignature).IsOptional();
            Property(x => x.AdminSignature).IsOptional();
            Property(x => x.AdminComment).IsOptional();

            Property(x => x.DateGuardianSignature).IsOptional();
            Property(x => x.GuardianSignature).IsOptional();
            Property(x => x.GuardianComment).IsOptional();

            Property(x => x.Q11).IsOptional();
            Property(x => x.Q11GuidFile).IsOptional();
            Property(x => x.Q12).IsOptional();
            Property(x => x.Q12GuidFile).IsOptional();
            Property(x => x.Q13).IsOptional();
            Property(x => x.Q13GuidFile).IsOptional();
            Property(x => x.Q1).IsOptional();
            Property(x => x.Q1Comment).IsOptional();
            Property(x => x.Q1IsLock).IsRequired();
            Property(x => x.Q21).IsOptional();
            Property(x => x.Q21GuidFile).IsOptional();
            Property(x => x.Q22).IsOptional();
            Property(x => x.Q22GuidFile).IsOptional();
            Property(x => x.Q23).IsOptional();
            Property(x => x.Q23GuidFile).IsOptional();
            Property(x => x.Q2).IsOptional();
            Property(x => x.Q2Comment).IsOptional();
            Property(x => x.Q2IsLock).IsRequired();
            Property(x => x.Sem1).IsOptional();
            Property(x => x.Sem1Credit).IsOptional();
            Property(x => x.Sem1Comment).IsOptional();
            Property(x => x.Sem1IsLock).IsRequired();
            Property(x => x.Q31).IsOptional();
            Property(x => x.Q31GuidFile).IsOptional();
            Property(x => x.Q32).IsOptional();
            Property(x => x.Q32GuidFile).IsOptional();
            Property(x => x.Q33).IsOptional();
            Property(x => x.Q33GuidFile).IsOptional();
            Property(x => x.Q3).IsOptional();
            Property(x => x.Q3Comment).IsOptional();
            Property(x => x.Q3IsLock).IsRequired();
            Property(x => x.Q41).IsOptional();
            Property(x => x.Q41GuidFile).IsOptional();
            Property(x => x.Q42).IsOptional();
            Property(x => x.Q42GuidFile).IsOptional();
            Property(x => x.Q43).IsOptional();
            Property(x => x.Q43GuidFile).IsOptional();
            Property(x => x.Q4).IsOptional();
            Property(x => x.Q4Comment).IsOptional();
            Property(x => x.Q4IsLock).IsRequired();
            Property(x => x.Sem2).IsOptional();
            Property(x => x.Sem2Credit).IsOptional();
            Property(x => x.Sem2Comment).IsOptional();
            Property(x => x.Sem2IsLock).IsRequired();
            Property(x => x.Q51).IsOptional();
            Property(x => x.Q51GuidFile).IsOptional();
            Property(x => x.Q52).IsOptional();
            Property(x => x.Q52GuidFile).IsOptional();
            Property(x => x.Q53).IsOptional();
            Property(x => x.Q53GuidFile).IsOptional();
            Property(x => x.Q5).IsOptional();
            Property(x => x.Q5Comment).IsOptional();
            Property(x => x.Q5IsLock).IsRequired();
            Property(x => x.Q61).IsOptional();
            Property(x => x.Q61GuidFile).IsOptional();
            Property(x => x.Q62).IsOptional();
            Property(x => x.Q62GuidFile).IsOptional();
            Property(x => x.Q63).IsOptional();
            Property(x => x.Q63GuidFile).IsOptional();
            Property(x => x.Q6).IsOptional();
            Property(x => x.Q6Comment).IsOptional();
            Property(x => x.Q6IsLock).IsRequired();
            Property(x => x.Sem3).IsOptional();
            Property(x => x.Sem3Credit).IsOptional();
            Property(x => x.Sem3Comment).IsOptional();
            Property(x => x.Sem3IsLock).IsRequired();
            Property(x => x.Credit).IsOptional();
            Property(x => x.Comment).IsOptional();
            Property(x => x.IsInSystem).IsRequired();
            Property(x => x.IsLock).IsRequired();

            HasOptional(x => x.Sponsor).WithMany(x => x.SponsorStudentPacketCoursesSignatures).HasForeignKey(x => x.SponsorId).WillCascadeOnDelete(false);
            HasOptional(x => x.Instructor).WithMany(x => x.InstructorStudentPacketCoursesSignatures).HasForeignKey(x => x.InstructorId).WillCascadeOnDelete(false);
            HasOptional(x => x.Admin).WithMany(x => x.StudentPacketCoursesSignatures).HasForeignKey(x => x.AdminId).WillCascadeOnDelete(false);
            HasOptional(x => x.Guardian).WithMany(x => x.StudentPacketCoursesSignatures).HasForeignKey(x => x.GuardianId).WillCascadeOnDelete(false);

            //HasMany(x => x.GuardiansSignatures).WithMany(x => x.StudentPacketCoursesSignatures)
            //    .Map(x => x.MapLeftKey("StudentPacketCourseId").MapRightKey("GuardianId").ToTable("GuardiansSignatures"));
        }
    }
}
