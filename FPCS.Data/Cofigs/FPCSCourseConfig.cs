﻿using System.Data.Entity.ModelConfiguration;

using FPCS.Data.Entities;

namespace FPCS.Data.Cofigs
{
    internal class FPCSCourseConfig : EntityTypeConfiguration<FPCSCourse>
    {
        public FPCSCourseConfig()
        {
            ToTable("FPCSCourse");
            HasKey(x => x.FPCSCourseId);

            Property(x => x.Name).IsRequired().HasMaxLength(150);
            Property(x => x.Location).IsOptional().HasMaxLength(150);
            Property(x => x.ClassType).IsOptional();
            Property(x => x.ScheduleComments).HasMaxLength(250);
            Property(x => x.RegistrationDeadline).IsRequired();
            Property(x => x.ClassStartDate).IsRequired();
            Property(x => x.ClassEndDate).IsRequired();
		Property(x => x.MinStudentsCount).IsOptional();
		Property(x => x.MaxStudentsCount).IsOptional();
		Property(x => x.HoursWithAllStudents).IsOptional().HasPrecision(19, 3);
		Property(x => x.HoursTeacherPlanning).IsOptional().HasPrecision(19, 3);
		Property(x => x.TotalHours).IsOptional().HasPrecision(19, 3);
		Property(x => x.HoursChargedPerStudent).IsOptional().HasPrecision(19, 5);
		Property(x => x.TeacherCostPerStudent).IsOptional().HasPrecision(19, 5);
		Property(x => x.TotalCourseCost).IsOptional().HasPrecision(19, 3);
            Property(x => x.CreatedDate).IsRequired();
            Property(x => x.UpdatedDate).IsRequired();
            Property(x => x.IsActivated).IsRequired();
            Property(x => x.IsDeleted).IsRequired();

            Property(x => x.MinGrade).IsOptional();
		Property(x => x.MaxGrade).IsOptional();
            Property(x => x.Semester).IsRequired();
            Property(x => x.ScheduleRepetition).IsOptional();

            Property(x => x.CourseSubjectId).IsRequired();
            Property(x => x.GradeLevel).IsRequired();

            HasRequired(x => x.ASDCourse).WithMany(x => x.FPCSCourses).HasForeignKey(x => x.ASDCourseId).WillCascadeOnDelete(false);
            HasOptional(x => x.Teacher).WithMany(x => x.FPCSCourses).HasForeignKey(x => x.TeacherId).WillCascadeOnDelete(false);
            HasOptional(x => x.Guardian).WithMany(x => x.FPCSCourses).HasForeignKey(x => x.GuardianId).WillCascadeOnDelete(false);
            HasOptional(x => x.Vendor).WithMany(x => x.FPCSCourses).HasForeignKey(x => x.VendorId).WillCascadeOnDelete(false);
            HasMany(x => x.FPCSCoursesRestrictions).WithMany(x => x.FPCSCoursesRestrictions)
                .Map(x => x.MapLeftKey("FPCSCourseId").MapRightKey("FamilyId").ToTable("FPCSCoursesRestrictions"));

            HasOptional(x => x.Subject).WithMany(x => x.FPCSCourses).HasForeignKey(x => x.SubjectId).WillCascadeOnDelete(false);
        }
    }
}
