﻿using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;

using FPCS.Data.Entities;

namespace FPCS.Data.Cofigs
{
    internal class StudentConfig : EntityTypeConfiguration<Student>
    {
        public StudentConfig()
        {
            Map(x => { x.ToTable("Student"); x.MapInheritedProperties(); });

            Property(x => x.GradYear).IsRequired();
            Property(x => x.DateOfBirth).IsRequired();
            Property(x => x.EnrollmentDate);
            Property(x => x.WithdrawalDate);
            Property(x => x.PrivateSchoolName).HasMaxLength(150);
            Property(x => x.PercentInSchoolDistrict);
            Property(x => x.IsGraduateFromFPCS).IsRequired();
            Property(x => x.IsASDContractHoursExemption).IsRequired();
            Property(x => x.ReasonASDContractHoursExemption).HasMaxLength(250);

            Property(x => x.Sex).IsRequired();
            Property(x => x.Grade).IsRequired();
            Property(x => x.EnrollmentStatus).IsOptional();

            Property(x => x.ZangleID).IsOptional();
            Property(x => x.StateID).IsOptional();

            Property(x => x.IsBirthCertificate).IsOptional();
            Property(x => x.IsGradesNotSubmitted).IsOptional();
            Property(x => x.IsILPPhilosophy).IsOptional();
            Property(x => x.IsMedicalRelease).IsOptional();
            Property(x => x.IsProgressReportSignature).IsOptional();
            Property(x => x.IsShotRecords).IsOptional();
            Property(x => x.IsTestingAgreement).IsOptional();
            Property(x => x.IsOther).IsOptional();

            Property(x => x.PercentagePlanningEnroll).IsOptional();
            Property(x => x.IsDoYouPlanGraduate).IsOptional();
            Property(x => x.CoreCreditExemption).IsOptional();
            Property(x => x.CoreCreditExemptionReason).IsOptional();
            Property(x => x.ElectiveCreditExemption).IsOptional();
            Property(x => x.ElectiveCreditExemptionReason).IsOptional();

            Property(x => x.IEPHasChildEligible).IsOptional();
            Property(x => x.IEPGiftedTalented).IsOptional();
            Property(x => x.IEPDeafBlindness).IsOptional();
            Property(x => x.IEPHearingImpairment).IsOptional();
            Property(x => x.IEPMultipleDisability).IsOptional();
            Property(x => x.IEPTraumaticBrainInjury).IsOptional();
            Property(x => x.IEPAutismAsbergerSyndrome).IsOptional();
            Property(x => x.IEPEarlyChildhoodDevelopmentalDelay).IsOptional();
            Property(x => x.IEPSpecificLearningDisability).IsOptional();
            Property(x => x.IEPOrthopedicImpairment).IsOptional();
            Property(x => x.IEPSpeechLanguageImpairment).IsOptional();
            Property(x => x.IEPDeafness).IsOptional();
            Property(x => x.IEPEmotionalDisturbance).IsOptional();
            Property(x => x.IEPMentalRetardation).IsOptional();
            Property(x => x.IEPOtherHealthImpairment).IsOptional();
            Property(x => x.IEPVisualImpairment).IsOptional();
            Property(x => x.IEPHasChildReceiving).IsOptional();
            Property(x => x.IEPHasChildFormally).IsOptional();
            Property(x => x.IEPIsExpirationDate).IsOptional();
            Property(x => x.IEPIsNextEligibilityEvaluation).IsOptional();
            Property(x => x.IEPHasChildLearnedAnotherLanguage).IsOptional();
            Property(x => x.IEPHasChildExperiencedLearned).IsOptional();
            Property(x => x.IEPIsChildEligibleBilingualProgram).IsOptional();
            Property(x => x.IEPHasChildCurrentlyReceivingServicesBilingualProgram).IsOptional();
            Property(x => x.IEPHasChildReceivingServicesMigrantEducation).IsOptional();

            Property(x => x.ILPPhilosophy).IsOptional();

            HasMany(x => x.Families).WithMany(x => x.Students)
                .Map(x => x.MapLeftKey("StudentId").MapRightKey("FamilyId").ToTable("FamilyStudent"));
            HasMany(x => x.Relationships).WithRequired(x => x.Student).HasForeignKey(x => x.StudentId).WillCascadeOnDelete(false);
        }
    }
}
