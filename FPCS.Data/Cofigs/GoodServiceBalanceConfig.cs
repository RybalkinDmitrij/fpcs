﻿using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;

using FPCS.Data.Entities;

namespace FPCS.Data.Cofigs
{
    internal class GoodServiceBalanceConfig : EntityTypeConfiguration<GoodServiceBalance>
    {
        public GoodServiceBalanceConfig()
        {
            

            HasKey(x => x.GoodServiceBalanceId);

            HasRequired(x => x.GoodService).WithMany(x => x.GoodServiceBalances).HasForeignKey(x => x.GoodServiceId).WillCascadeOnDelete(false);

            Property(x => x.PO).IsOptional();
            Property(x => x.Invoice).IsOptional();
            Property(x => x.Check).IsOptional();
            Property(x => x.CheckDate).IsOptional();
            Property(x => x.Payee).IsOptional();
            Property(x => x.ReceiptDate).IsOptional();
            Property(x => x.Description).IsOptional();
            Property(x => x.DateEntered).IsOptional();

            Property(x => x.UnitPrice).IsRequired();
            Property(x => x.QTY).IsRequired();
            Property(x => x.Shipping).IsRequired();
        }
    }
}
