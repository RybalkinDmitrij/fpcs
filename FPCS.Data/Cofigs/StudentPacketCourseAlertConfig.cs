﻿using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;

using FPCS.Data.Entities;

namespace FPCS.Data.Cofigs
{
    internal class StudentPacketCourseAlertConfig : EntityTypeConfiguration<StudentPacketCourseAlert>
    {
        public StudentPacketCourseAlertConfig()
        {
            ToTable("StudentPacketCourseAlert");
            HasKey(x => x.StudentPacketCourseAlertId);

            Property(x => x.Value).IsRequired();

            HasRequired(x => x.StudentPacketCourse).WithMany(x => x.StudentPacketCourseAlerts).HasForeignKey(x => x.StudentPacketCourseId).WillCascadeOnDelete(false);
            HasRequired(x => x.User).WithMany(x => x.StudentPacketCourseAlerts).HasForeignKey(x => x.DbUserId).WillCascadeOnDelete(false);
        }
    }
}
