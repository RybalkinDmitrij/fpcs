﻿using FPCS.Data.Entities;
using System.Data.Entity.ModelConfiguration;

namespace FPCS.Data.Cofigs
{
    internal class SubjectConfig : EntityTypeConfiguration<Subject>
    {
        public SubjectConfig()
        {
            ToTable("Subject");
            HasKey(x => x.SubjectId);

            Property(x => x.Name).IsRequired().HasMaxLength(100);
            Property(x => x.Description).IsOptional().IsMaxLength();
            Property(x => x.IsDeleted).IsRequired();
            Property(x => x.IsElective).IsRequired();
            Property(x => x.Code).IsRequired();
        }
    }
}
