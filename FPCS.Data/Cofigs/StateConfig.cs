﻿using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;

using FPCS.Data.Entities;

namespace FPCS.Data.Cofigs
{
    internal class StateConfig : EntityTypeConfiguration<State>
    {
        public StateConfig()
        {
            ToTable("State");
            HasKey(x => x.StateId);

            Property(x => x.Code).IsRequired().HasMaxLength(20);
            Property(x => x.Name).IsRequired().HasMaxLength(100);
        }
    }
}
