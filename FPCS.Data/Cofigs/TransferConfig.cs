﻿using FPCS.Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Data.Cofigs
{
    internal class TransferConfig : EntityTypeConfiguration<Transfer>
    {
        public TransferConfig()
        {
            ToTable("Transfer");
            HasKey(x => x.TransferId);

            Property(x => x.TransferAmount).IsRequired();
            Property(x => x.Comment).IsRequired();
            Property(x => x.DateCreate).IsRequired();
            Property(x => x.UserCreateName).IsRequired();

            HasOptional(x => x.FromStudent).WithMany(x => x.FromTransfers).HasForeignKey(x => x.FromStudentId).WillCascadeOnDelete(false);
            HasOptional(x => x.ToStudent).WithMany(x => x.ToTransfers).HasForeignKey(x => x.ToStudentId).WillCascadeOnDelete(false);
        }
    }
}