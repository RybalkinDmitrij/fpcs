﻿using FPCS.Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPCS.Data.Cofigs
{
    internal class FileStoreConfig : EntityTypeConfiguration<FileStore>
    {
        public FileStoreConfig()
        {
            ToTable("FileStore");
            HasKey(x => x.FileStoreId);

            Property(x => x.Name).IsRequired();
            Property(x => x.URL).IsRequired();
            Property(x => x.ContentSize).IsRequired();
            Property(x => x.DateCreated).IsRequired();
            Property(x => x.MimeType).IsRequired();
        }
    }
}