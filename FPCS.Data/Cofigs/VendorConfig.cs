﻿using System.Data.Entity.ModelConfiguration;
using FPCS.Data.Entities;

namespace FPCS.Data.Cofigs
{
	internal class VendorConfig : EntityTypeConfiguration<Vendor>
	{
		public VendorConfig()
		{
			ToTable("Vendor");
			HasKey(x => x.VendorID);

			Property(x => x.UnqVendorID).IsRequired();
			Property(x => x.IsDeleted).IsRequired();

			Property(x => x.Type).IsRequired();

			Property(x => x.BusinessName).IsRequired().HasMaxLength(150);
			Property(x => x.FirstName).IsOptional().HasMaxLength(150);
			Property(x => x.LastName).IsOptional().HasMaxLength(150);
			Property(x => x.Status).IsOptional();
			Property(x => x.StatusComments).IsOptional();

			Property(x => x.StreetAddress).IsOptional();
			Property(x => x.StreetAddressCity).IsOptional();
			HasOptional(x => x.StreetAddressState)
				.WithMany(x => x.StreetAddressVendors)
				.HasForeignKey(x => x.StreetAddressStateId)
				.WillCascadeOnDelete(false);
			Property(x => x.StreetAddressZipCode).IsOptional();

			Property(x => x.MailingAddress).IsOptional();
			Property(x => x.MailingAddressCity).IsOptional();
			HasOptional(x => x.MailingAddressState)
				.WithMany(x => x.MailingAddressVendors)
				.HasForeignKey(x => x.MailingAddressStateId)
				.WillCascadeOnDelete(false);
			Property(x => x.MailingAddressZipCode).IsOptional();

			Property(x => x.Phone).IsOptional();
			Property(x => x.Fax).IsOptional();
			Property(x => x.Email).IsOptional();
			Property(x => x.Location).IsOptional();
			Property(x => x.BusinessWebsite).IsOptional();

			Property(x => x.EmployerIdentification).IsOptional();
			Property(x => x.AKBusinessLicense).IsOptional();
			Property(x => x.LicenseExpiration).IsOptional();
			Property(x => x.InsuranceExpiration).IsOptional();

			Property(x => x.TrainingEducationExperience).IsOptional();
			Property(x => x.CommentsAboutServices).IsOptional();
			Property(x => x.VendorUnitType).IsOptional();
			Property(x => x.OtherChargeMethod).IsOptional();
			Property(x => x.Price).IsOptional();
			Property(x => x.ContractStartingDate).IsOptional();

			Property(x => x.IsNonProfit).IsRequired();
			Property(x => x.IsMisdemeanorOrFelony).IsRequired();
			Property(x => x.IsBackgroundCheckFingerprinting).IsRequired();
			Property(x => x.IsHaveChildrenCurrentlyEnrolled).IsRequired();
			Property(x => x.IsRetiredCertificatedASDTeacher).IsRequired();
			Property(x => x.IsASDEmployeeEligibleHireList).IsRequired();
			Property(x => x.IsCurrentlyAvailableProvideServices).IsRequired();
			Property(x => x.IsProvideOnlineCurriculumServices).IsRequired();

			Property(x => x.IsActive).IsRequired();

			Property(x => x.IsCourseProvider).IsRequired();
			Property(x => x.CostCourseProvider).IsRequired();

            HasOptional(x => x.FileStore).WithMany(x => x.Vendor).HasForeignKey(x => x.FileStoreId).WillCascadeOnDelete(false);

			HasMany(x => x.Services).WithMany(x => x.Vendors)
				.Map(x => x.MapLeftKey("VendorId").MapRightKey("ServiceVendorId").ToTable("ServiceBelongVendor"));

			HasRequired(x => x.SchoolYear).WithMany(x => x.Vendors).HasForeignKey(x => x.SchoolYearId).WillCascadeOnDelete(false);
		}
	}
}
