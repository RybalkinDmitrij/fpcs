﻿using System.Data.Entity.ModelConfiguration;

using FPCS.Data.Entities;

namespace FPCS.Data.Cofigs
{
    internal class ASDCourseConfig : EntityTypeConfiguration<ASDCourse>
    {
        public ASDCourseConfig()
        {
            ToTable("ASDCourse");
            HasKey(x => x.ASDCourseId);

            Property(x => x.UnqASDCourseId).IsRequired();

            Property(x => x.ExternalASDCourseId).IsRequired().HasMaxLength(50);
            Property(x => x.Description).IsOptional().HasMaxLength(1000);
            Property(x => x.GradCredit).IsRequired().HasPrecision(18, 2);
            Property(x => x.CreatedDate).IsRequired();
            Property(x => x.UpdatedDate).IsRequired();
            Property(x => x.IsActivated).IsRequired();
            Property(x => x.IsDeleted).IsRequired();
            Property(x => x.IsFree).IsRequired();
            Property(x => x.Name).IsOptional();
            Property(x => x.TypeGrade).IsRequired();

            HasRequired(x => x.Subject).WithMany(x => x.ASDCourses).HasForeignKey(x => x.SubjectId).WillCascadeOnDelete(false);
            HasRequired(x => x.SchoolYear).WithMany(x => x.ASDCourses).HasForeignKey(x => x.SchoolYearId).WillCascadeOnDelete(false);
        }
    }
}
