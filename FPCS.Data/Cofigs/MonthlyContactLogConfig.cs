﻿using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;

using FPCS.Data.Entities;

namespace FPCS.Data.Cofigs
{
    internal class MonthlyContactLogConfig : EntityTypeConfiguration<MonthlyContactLog>
    {
        public MonthlyContactLogConfig()
        {
            ToTable("MonthlyContactLog");
            HasKey(x => x.MonthlyContactLogId);

            Property(x => x.Date).IsRequired();
            Property(x => x.Comment).IsRequired();

            HasRequired(x => x.Family).WithMany(x => x.MonthlyContactLogs).HasForeignKey(x => x.FamilyId).WillCascadeOnDelete(false);
            HasRequired(x => x.Teacher).WithMany(x => x.MonthlyContactLogs).HasForeignKey(x => x.TeacherId).WillCascadeOnDelete(false);
        }
    }
}
