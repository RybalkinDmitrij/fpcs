﻿using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;

using FPCS.Data.Entities;

namespace FPCS.Data.Cofigs
{
    internal class FamilyConfig : EntityTypeConfiguration<Family>
    {
        public FamilyConfig()
        {
            ToTable("Family");
            HasKey(x => x.FamilyId);

            Property(x => x.Name).IsRequired().HasMaxLength(150);
            Property(x => x.Description).IsMaxLength();
            Property(x => x.Address).HasMaxLength(200);
            Property(x => x.MailingAddress).HasMaxLength(200);
            Property(x => x.Telephone).HasMaxLength(50);
            Property(x => x.CellPhone).HasMaxLength(50);
            Property(x => x.City).HasMaxLength(70);
            Property(x => x.Country).HasMaxLength(70);
            Property(x => x.Zip).HasMaxLength(20);
            Property(x => x.Email).HasMaxLength(100);
            Property(x => x.Password).HasMaxLength(100);
            Property(x => x.IsUseDirectory).IsRequired();
            Property(x => x.IsDeleted).IsRequired();
            Property(x => x.CreatedDate).IsRequired();
            Property(x => x.UpdatedDate).IsRequired();

            HasRequired(x => x.State).WithMany(x => x.Families).HasForeignKey(x => x.StateId).WillCascadeOnDelete(false);
            HasRequired(x => x.SchoolYear).WithMany(x => x.Families).HasForeignKey(x => x.SchoolYearId).WillCascadeOnDelete(false);
        }
    }
}
